12 
bei der „Mittelstandspolitik" des Staates nur darum handelt, den un 
vermeidlichen Uebergang in neue Verhältnisse möglichst langsam, und damit 
möglichst wenig verderblich zu gestalten. 
So stellen sich die sozialen Aufgaben des modernen Staates, soweit 
sie aus dem Niedergang der Landwirtschaft und aus dem Niedergang des 
Handwerks hervorgehen, wesentlich als Aufgaben der Abwehr dar, und 
zwar einer Abwehr, von der wir wissen, daß ihr kein voller Erfolg be- 
schieden ist, daß sie nur zum Zweck haben kann, dem Untergang bestehen 
der Wirtschaftsformen die allzu verwüstende Kraft zu nehmen, den Ueber 
gang zur Zukunft in möglichst schonender Weise zu gestalten. Auf 
diesem ganzen Gebiet der Sozialpolitik, die sich schützend vor den land 
wirtschaftlichen Großbetrieb und vor das Handwerk stellt, hat die Staats 
gewalt den Wind der Weltgeschichte, diesen mächtigen, auf die Dauer un 
widerstehlichen Wind, der die Entwickelung der Menschheit vorwärts treibt, 
gegen sich. Hier giebt es nur ein Kreuzen und Lavieren, um nicht zu 
plötzlich steuerlos in neue Bahnen gerissen zu werden. Es ist damit zu 
gleich klar, daß die Aufgaben staatlicher Sozialpolitik mit dem Gebrauch 
jener Abwehrmittel nicht erschöpft sind. Dauernden Erfolg vermag 
nicht eine lediglich gegen die kommende Entwickelung sich sträubende, 
sondern nur eine schöpferische Politik zu ernten. Fahrt mit vollen 
Segeln vor dem Winde der Weltgeschichte, das ist das eigentliche Ziel 
wie der äußeren so der inneren Staatsleitung. Die Macht der Sozial 
politik des modernen Staates hat sich an vornehmster Stelle da zu ent 
falten, wo sie der aufsteigenden Entwickelung der Zukunft dient. Erst 
da treten wir in die vornehmsten sozialen Aufgaben des modernen 
Staates ein. 
II. 
Der gewerbliche Großbetrieb. 
Dem Niedergang des landwirtschaftlichen Großbetriebes und des 
gewerblichen Kleinbetriebes steht das Aufsteigen des mit dem Großhandel 
verbündeten gewerblichen Großbetriebs zur Seite. Der Weltmarkt hat sich 
aufgethan, dem Flügelschlag des Großkapitals den freien Luftraum zu 
gewähren. Die Intelligenz des Großgewerbetreibenden und des Groß 
kaufmanns hat ungeahnte Triumphe davongetragen. Sie macht die Welt 
sich unterthänig. Die Erzeugungskraft des Gewerbes ist eine ganz andere