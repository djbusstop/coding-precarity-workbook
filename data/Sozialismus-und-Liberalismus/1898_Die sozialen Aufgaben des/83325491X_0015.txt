9 
\ 
kraft des grundbesitzenden Adels teilte sich durch Bismarcks' gewaltige 
Persönlichkeit dem deutschen Staate, ja in gewissem Maße dem ganzen 
deutschen Volke mit. In der Macht des Adels liegt ein großer Teil der 
Macht des deutschen Volkes. Noch heute. Mit kleinen Leuten macht 
man keine Weltgeschichte. Das Königtum bedarf der adligen Herren 
geschlechter als seiner natürlichen Bundesgenossen. Die Macht des Adels 
aber wohnt auf den großen Gütern, insbesondere gerade auf den Ritter 
gütern Ostelbiens. Das Sinken des Großgrundbesitzes rührt an die 
Wurzeln unseres Staates. Was hat der Staat zu thun? 
Ein englisches Sprichwort sagt: Das Parlament kann alles, nur 
nicht aus einem Mann eine Frau machen. Ja, der Staat kann „alles"; 
aber über die thatsächlichen Verhältnisse hat er keinerlei Gewalt. 
Kann der Staat die Thatsache ändern, daß wir unaufhaltsam auf dem 
Wege zur Weltwirtschaft begriffen sind? Kann der Staat die Thatsache 
ändern, daß die Daseinsbedingungen für die großen Güter im Untergänge 
sich befinden? Nimmermehr! Der unbarmherzigen Thatsache gilt es ins 
Gesicht zu sehen. Es nützt nichts, das Auge bor ihr zu verschließen. 
Wir müssen wissen, daß die Tage des landwirtschaftlichen Großbetriebs 
gezählt sind. Die Mittel selber, welche der Großgrundbesitz zu seiner 
Erhaltung fordert, zeigen die Unmöglichkeit seines Begehrens. Sowohl 
die von ihm verlangte Beseitigung des freien Getreidehandels wie seine 
auf Unterdrückung der arbeitenden Volksmenge gerichteten Bestrebungen 
machen es klar (von dem Bimetallismus ganz abzusehen), daß er seine 
Erhaltung auf Kosten der Nation gesichert wissen will. Das aber ist 
das Unmögliche und unbedingt Abzulehnende. Die Nation ist niemals 
um eines Standes willen, sondern immer nur der einzelne Stand um der 
Nation willen da. So lange der Stand aus eigenen Kräften der Nation 
dient, so lange ist er zum Dasein, ja zur Herrschaft berechtigt. Sobald 
ein Stand, und sei er noch so bedeutsam, der Nation zur Last fällt, 
wird er unerbittlich von den Schultern des Volkes abgeschüttelt. Die 
geschichtliche Entwickelung kennt keine Rücksichtnahme. Es gilt der Satz: 
Der Mohr hat seine Schuldigkeit gethan, der Mohr kann gehen. Die 
„großen Mittel" zur Erhaltung des Großgrundbesitzes bedeuten, daß er 
nicht mehr aus eignen Kräften seine Pflichten zu erfüllen imstande ist, 
bedeuten seine Erhaltung auf Kosten der Gesamtheit, zu Lasten der Ge 
samtheit. Es ist damit gesagt, daß die „großen Mittel" für den Staat 
ausgeschlossen sind. 
Aber soll der Staat seinerseits mit Hand anlegen, den Großgrund- 
So hm, Soziale Aufgaben. 2