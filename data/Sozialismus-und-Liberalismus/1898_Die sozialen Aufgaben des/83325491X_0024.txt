18 
und Konsumvereine haben in den letzten Jahrzehnten den Stand der ge 
lernten Arbeiter Englands zu einer Großmacht des politischen und des 
wirtschaftlichen Lebens emporgehoben. Auch auf dem Kontinent ist die 
Arbeiterschaft in die Gewerkschafts- und Konsumvereinsbewegung ein 
getreten. 
IV. 
Die Arbeiterbewegung. 
In Deutschland hatte die norddeutsche Bundesverfassung schon 1867 
den Arbeitern das allgemeine gleiche Reichstagswahlrecht geschenkt. Bereits 
1869 fügte die Reichsgewerbe-Ordnung die Koalitions-Freiheit hinzu. 
Alle Bedingungen für das Emporsteigen des deutschen Arbeiterslandes 
schienen damit erfüllt. Dennoch ist gerade in Deutschland die Entwickelung 
eine besonders schwierige geworden. Nicht blos das Widerstreben der 
Unternehmerkreise, sondern in noch höherem Maße die innere Entwickelung, 
welche die Arbeiterbewegung nahm, trat dem Fortschreiten des Arbeiter 
standes hindernd in den Weg. 
Die deutsche Arbeiterbewegung fiel in die Hände der Sozialdemokratie. 
Das war nicht immer so. Unter dem Einfluß Lassalles (in den sech 
ziger Jahren) war die beginnende Arbeiterbewegung Deutschlands monarchisch 
und national. Erst im Laufe der siebziger Jahre zog durch Liebknecht 
und Bebel das von Karl Marx entwickelte Gedankensystem der inter 
nationalen Sozialdemokratie den deutschen Arbeiterstand in seine Bahnen. 
Die Sozialdemokratie ist der erklärte Feind der ganzen bestehenden 
Staats- und Rechtsordnung. Die naturnotwendige Folge des kapita 
listischen Privateigentums ist nach ihrer Lehre die Ausbeutung des 
Arbeiters durch den mit Kapital ausgerüsteten Unternehmer. Der Unter 
nehmer eignet sich den „Mehrwert" an. Der Unternehmer ist als solcher 
der Ausbeuter. Es giebt keine Nation. Es giebt nur zwei Klassen: 
Ausbeuter und Ausgebeutete. Der Staat ist kein Nationalstaat, sondern 
ein Klassenstaat, ein Ausbeuterstaat, ein Staat, welcher lediglich zum 
Schutze der Unternehmerklasse und der von ihr betriebenen Ausbeutung 
dient. Der Staat ist ein Unstaat. Das Recht ist Unrecht. Und solange 
die kapitalistische Produktionsform besteht, kann es nicht anders werden. 
Hilfe durch den Klassenstaat und innerhalb des Klassenstaates ist aus 
geschlossen. Das unerbittliche Naturgesetz der ökonomischen Entwickelung