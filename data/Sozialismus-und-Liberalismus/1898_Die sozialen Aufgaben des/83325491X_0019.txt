13 
geworden, als sie jemals war. Mit spielender Leichtigkeit bringt sie un 
geheure Gütermengen hervor, die Welt damit zu versorgen und den Reich 
tum der Welt dafür einzutauschen. Ruhte einst der nationale Reichtum 
an erster Stelle im Grund und Boden, heute beruht er zu einem großen, 
vielleicht bereits zum größten Teile in der Industrie. Für die Länder, 
die zu der „Stadt" des Weltmarkts gehören, ist es gar keine Frage mehr, 
ob sie Industriestaaten sein wollen oder nicht. In der Stadt blüht nicht 
die Landwirtschaft, sondern das Gewerbe. Die Stadt versorgt das Land 
mit den Hervorbringungen des Gewerbefleißes. Hier dröhnt der Lärm 
der Maschine, hier qualmt der Rauch der Esse, hier regen sich die Tausende 
von arbeitsamen Händen. Die Stadt des isolierten Staates ist die In 
dustriestadt. In dem Aufsteigen der Großindustrie liegt die Neubegründung 
der Herrschaft der abendländischen Völker über die umgebende Welt. Das 
Maß der Vorherrschaft auf dem Gebiet des großindustriellen Wettbewerbs 
ist heute mit dem Maß der Teilnahme an der wirtschaftlichen Weltherr 
schaft gleichbedeutend. Es ist damit klar, welche übermächtige Bedeutung 
die Blüte von Großindustrie und Großhandel für den modernen Staat 
besitzt. Teilnahme an der politischen Weltherrschaft hat die Teilnahme 
an der wirtschaftlichen Weltherrschaft zur Voraussetzung. Indem den 
Staaten der abendländischen Kulturwelt der Boden der Landwirtschaft, 
auf dem sie groß geworden, unter den Füßen weicht, thut sich ihnen in 
Großhandel und Großindustrie eine neue Grundlage ihrer Macht auf. 
Die Handels- und Gewerbepolitik ist zu einem maßgebenden Bestandteil 
zugleich der inneren und der äußeren Politik geworden. 
Indem der stolze Bau des modernen gewerblichen Großbetriebes zum 
Himmel steigt, das Gold, die Weltmacht der Gegenwart, in seinem Innern 
sammelnd, wirft er jedoch zugleich naturnotwendig auf die breite Masse 
der Bevölkerung tiefschwarzen Schatten. Der Großbetrieb bedarf der 
großen vermögenslosen arbeitenden Menge. Ja, er erzeugt, indem er für 
sich selber Schätze sammelt, die Armut der Masse, die er sodann in seinen 
Frohndienst zwingt. Das kleine Kapital des Handwerkers wird durch das 
Großkapital, der gewerbliche Kleinbetrieb durch den gewerblichen Groß 
betrieb in stetig steigendem Maß vernichtet. Auf offener Straße schreitet 
der Großbetrieb einher, einem reißenden Tiere gleich, sehend, wen er ver 
schlinge. Er kann nicht anders. Der Großbetrieb ist es, der den Mittel 
stand alter Art mit elementarer Gewalt vernichtet, eine immer größere 
Zahl aus ihrer überkommenen Selbständigkeit in die Masse der abhängigen 
Arbeiterbevölkerung herunterschleudernd. Ein ungeheurer Kampf ums