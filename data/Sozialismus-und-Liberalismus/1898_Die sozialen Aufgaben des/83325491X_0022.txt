16 
wenn er etwa durch einen Arbeitsunfall arbeitsunfähig, oder alt und 
schwach geworden war, vom Arbeitgeber gekündigt und er der Armenkasse 
überwiesen werden. Und so lange er in Arbeit stand, drückte das Heer 
der auf Arbeit Wartenden auf die Arbeitsbedingungen. Ja, die Frauen 
und Kinder der arbeitenden Bevölkerung traten im Wettbewerb auf, um 
den Lohn des Mannes auf Kinder und Frauenlohn, ja die ganze Stellung 
des arbeitenden Mannes auf Kinder- und Frauen-Stellung herabzudrücken. 
Der freie Arbeitsvertrag bedeutete thatsächlich die Unfreiheit des 
Arbeiters. Eine neue Sklaverei kam aus, schlimmer als die Sklaverei 
vergangener Jahrhunderte. Die Masse des Volkes kam in Gefahr, in ein 
Volk von Knechten sich zu verwandeln. Aber wehe dem Volk, das sich 
in Knechtschaft schlagen läßt! wehe den Knechten, wehe den Herren! 
Die große, ja die größte soziale Aufgabe des modernen Staates ist 
es, mit dem freien Arbeitsvertrag der Gegenwart die Freiheit des 
Arbeiters zu verbinden. 
In dieser Richtung geht die „Arbeiterschutzgesetzgebung" unserer Zeit. 
Der freie Arbeitsvertrag als solcher genügt zur Regelung des Arbeits 
verhältnisses nicht. Es genügt nicht, daß der Arbeitsvertrag den Parteien 
zu beliebiger Ausfüllung überlassen werde. Der Staat muß das Recht 
des Arbeitsvertrags durch seine Gesetzgebung entwickeln. Er muß in 
den Arbeitsvertrag selber Bestimmungen eintragen, die um des Gesamt 
wohls willen notwendig sind. Denn der Arbeitsvertrag ist heute von 
öffentlicher Bedeutung und geht nicht blos den Einzelnen, sondern die 
Gesamtheit an. Um der Freiheit des Arbeiters willen muß der Staat 
die Vertragsfreiheit einschränken. Er muß Kinder- und Frauenarbeit 
verbieten oder doch nur in gewissen Grenzen zulassen. Er muß Einfluß 
nehmen auf die Arbeitszeit. Ja, er muß in gewissem Maße, z. B. durch 
Verbot des Trucksystems, durch Vorschriften über die Arbeitsräume, über 
haupt auf den Inhalt der Arbeitsbedingungen einwirken. Er muß durch 
Ausgestaltung des Arbeitsrechts den Arbeiter hindern, daß er sich zum 
Sklaven des Arbeitgebers mache. Noch mehr. Die deutsche Gesetzgebung 
über Unfall-, Kranken-, Alters und Invaliditäts-Versicherung der Arbeiter 
bringt den Gedanken zum Ausdruck, daß das Arbeitsverhältnis trotz der 
„freien" Dienstmiete dennoch kein bloßes Verhältnis auf Zeit ist, das mit 
der Entlassung des Arbeiters lediglich endigt. Der durch einen Arbeits- 
Unfall arbeitsunfähig oder der alt und schwach gewordene Arbeiter soll 
nicht schlechthin der Armenkasse zufallen. Er soll in Ehren sein Brot 
essen können. Hat der Arbeitgeber den Nutzen der Arbeitskraft gehabt,