28 
"Een. Warum? — Weil die sozialdemokratische Lehre unwahr ist. 
Unter Führung der Gewerkschaften schreitet die Arbeiterbewegung aus 
dem Boden des bestehenden Staates voran, um mit jedem ihrer Fort 
schritte der sozialdemokratischen Bewegung als solcher den Boden unter 
den Füßen hinwegzuziehen. 
28ie bie enMcNung ber (^#50^, gan* gerate so, ja in nod, 
höherem Maße hat das Aufsteigen der sozialdemokratischen Partei zu 
politischer Macht ihr innerstes Wesen verändert. Karl Marx gab 
1848 in seinem „Kommunistischen Manifest" die Losung aus, daß die 
Ziele der Arbeiterbewegung nur „durch den gewaltsamen Umsturz 
aller bisherigen Gesellschaftsordnung" zu erreichen seien. „Mögen die 
herrschenden Klassen vor einer kommunistischen Revolution erzittern!" In 
diesem Sinne erhob er den Schlachtruf: „Proletarier aller Länder, ver 
einigt euch! Aber längst ist die Idee des gewaltsamen Umsturzes ab 
gethan worden. Längst haben die Führer der sozialdemokratischen Be 
wegung eingesehen, daß jeder Versuch eines „gewaltsamen" Umsturzes das 
Gegenteil einer Förderung der Arbeiterbewegung bedeuten müßte. Mit 
Vorliebe wird immer noch vor den sozialdemokratischen Scharen die rote 
Fahne geschwenkt, und immer noch dann und wann (z. B. von Bebel) 
der nun in nächster Zeit bevorstehende Zusammenbruch der heutigen Ge 
sellschaftsordnung angekündigt. Aber das alles geschieht nur, um die 
Leldenschaft der Masse durch äußeres Blendwerk anzustacheln. Selbst 
Liebknecht, der alte Revolutionär von 1848, sagte 1891 auf dem 
Erfurter Sozialdemokratischen Parteitage: „Das Revolutionäre liegt nicht 
in den Mitteln, sondern in dem Ziel; Gewalt ist seit Jahrtausenden ein 
reaktionärer Faktor." Mit anderen Worten: in richtiger Erkenntnis der 
Verhältnisse ist von den Führern die Revolution als Mittel der sozial 
demokratischen Bewegung aufgegeben worden. Man gebraucht das Wort 
„Revolution", „sozialrevolutionär" noch immer mit Vorliebe; aber das 
Wort ist zu einem bloßen Schmuck der Rede herabgesunken. Weshalb? 
Die bloße Thatsache, daß die Arbeiterbewegung durch das Mittel des 
allgemeinen gleichen Wahlrechts im deutschen Reichstag zu einer ge 
ordneten Vertretung und zu parlamentarischem Einfluß gelangte, hat 
die sozialdemokratische Partei, trotzdem sie es nicht eingestehen will, 
und trotzdem sie noch immer revolutionäre Manieren vor sich her 
trägt, thatsächlich, sie mag wollen oder nicht, in eine Ordnungs 
partei verwandelt. Nur daß sie das Arbeiterinteresse auf ihre 
<şahne geschrieben hat. Aber die Befriedigung dieses Arbeiterinteresses