﻿2». Bcdcntuug der städtische« Wohnungsfrage für die Landbcvöikernng usw. 123

sind Verhältnisse, die ihm das Gefühl der Heimat geben.
Daher neuerdings zahlreiche Bestrebungen nach innerer Kolo-
nisation durch Zerschlagung größerer Güter zur Ansiedlung
von landwirtschaftlichen Arbeitern in gemeinnütziger Weise.
Auch die ländlichen Dienstboten erstreben für die spätern Jahre
einen kleinen Grundbesitz. Für alle diese Schichten sind nicht
Wohnung, Kost und Lohn allein entscheidend, denn gerade
bei den Ttichtigsten lebt bewußt und unbewusst die Sehnsucht
nach einer, wenn auch bescheidenen eignen Scholle, damit,
wenn nicht sie selbst, doch ihre Kinder in den Stand der
Kleinbauens aufsteigen können.

Noch bedeutsamer wird diese Frage, wo die Industrie
auf das Land hinauswandert, was in wachsendem
Maße der Fall ist, besonders längs der Wasserstraße n.
Behörden und Untemehmer haben hier zu sorgen, daß
nicht Großhaus und Mietkaserne mit hin-
an s z i e h e n. Der Besitz eines wenn auch kleinen land-
wirtschaftlichen Grundeigentmns ist auch dadurch von großen!
Werte für den Arbeiter, daß er ihm leichter über Arbeits-
stockungen in der Industrie hinweghilft, besonders wenn in
der Gegend verschiedene Industriezweige ansässig sind. Die
Verbindung von Landwirtschaft und Industrie wirkt auf
konservative Anschauungen hin. Wo die Jndustriealisierung
des Landes sich in größern: Maße vollzieht, sind auch Ent-
eignung und Zwangsumlegung notwendig.

Die bayerische Landeskultnr-Rentenbank gibt (Gesetz
vom 24. März 1908), wie S. 110 erwähnt, auch Darlehen zur
Ansiedlung landwirtschaftlicher Arbeiter. Die Wohnungen
dürfen nicht mehr als drei Zimmer mit Küche und Zubehör
haben, im Zubehör sind „auch die zur Anlegung eines Haus-
nnd Nutzgartens erforderliche Grundfläche, ferner Acker- und
Wiesland bis zu 2 Hektar und die notwendigen Wirtschafts-
ränme (Stall und Scheune) inbegriffen". In Preußen
kann die Ansiedlung mit Hilfe des Rentengutgesetzes vom