﻿I. Die Wohnungsfrage im Altertum	17

denen die heimatlose Masse wehrlos gegenüberstand. Die-
selben Leute, welche draußen die Bauernhöfe zu Latifundien
zusammenlegten, kauften in Rom die alten Familienhäuser
des verarmenden und aussterbenden Mittelstandes zusammen
und schufen an deren Stelle Mietkasernen. Als einen
solchen besonders hervortretenden Baustellen- und Häuser-
wucherer in großem Stile schildert Plutarch uns Crassus,
der gewerbsmäßig Baustellen, die durch Feuersbrunst oder
Einsturz geschaffen waren, zusammenkaufte; fast die halbe
Stadt sei ihm allmählich zu eigen und deren Einwohnerschaft
zinspflichtig gewesen. Die großen vielstöckigen Mietkasemen
und Massenpferche hießen insulae. Die Zahl solcher insulae
wird für die spätere Kaiserzeit aus 46 602 angegeben, gegen-
über 1780 Patrizierhäusern (äomus). Die insulae waren durch-
weg aus Fachwerk und möglichst schlecht erbaut und brannten
daher oft ab. Der großkapitalistische Hausbesitzer (ckomiims)
hatte für jedes Miethaus einen Verwalterlprocurator insulae),
der entweder für Rechnung des Herrn oder als Zwischenwirt
auf eigne Rechnung gegen eine bestimmte Abgabe selbständig
vermietete. Wir hören von Wohnungen, die 200 Stufen hoch
lagen, was auf zehn Stockwerke schließen läßt, von einem
ägyptischen Könige, der abgesetzt als Staatspensionär in Rom
lebte und in solcher Höhe eine kleine, elende und teure Wohnung
besaß. Unter Augustus wird von Häusern mit 70 Fuß Höhe,
also sechs bis sieben Geschossen, berichtet, und in Konstantinopel
waren in der spätern Kaiserzeit Häuser von 100 Fuß Höhe er-
laubt. Das Gewerbe wurde vielfach in Kellern betrieben,
und auch die gewerbliche Bevölkerung hat dann so tief gewohnt.
Andere hausten massenhaft in Dachwohnungen, unter den
Ziegeln (sub tegulis), „wo die Tauben nisteten". Diese Zu-
stände nmßten in den breiten Volksmassen auch das Familien-
leben und das sittliche Gefühl vollständig vernichten.

Von der spätern republikanischen Zeit wird gemeldet,
daß 16 Angehörige der berühmten gens Aelia mit Frauen

Jacger, Gmndriß der Wohnungsfrage und Wohnungspolitik.

2