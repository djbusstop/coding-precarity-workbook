﻿t>2	Grundriß dcr Wohnungsfrage und WohnungSPolitik

völkerung; die Beseitigung des alten seßhaften Bürgertums,
besonders in den Großstädten, ist an ihnen spurlos vorüber-
gegangen, und daher kommt die Wohnungsreform in den meisten
großen Städten nicht vorwärts.

XIV.	Das Baugewerbe

leidet schwer unter der heutigen Form der städtischen Besied-
lung. Zunächst bewegt es sich, wie der ganze Wohnungs- und
Bodenmarkt, in Extremen; bald kommt eine Periode des Auf-
schwungs mit überhohen Materialpreisen und Löhnen, dann
wieder eine volle Stockung, bald eilt das Baugewerbe dem
Volkszuwachse voraus, bald kommt es ihm nicht nach, weil es
naturwidrig ganz von der Spekulation abhängig geworden
ist. Es kann nicht die sachgemäße Behausung des Volkes
vollziehen, sondern ist hier in die zweite Reihe gedrängt und
Werkzeug der Bodenspekulation geworden.
Diese benutzt es, um, wenn die Zeit gekommen ist, den Gewinn
an den Bauplätzen durch Bebauung zu verwirklichen. Diese
Bebauung geschieht, um die Verpflichtungen des Bauherrn
abzuladen, vielfach durch mittellose Strohmänner. Der Preis
der Baustelle wird ihnen mindestens so hoch angerechnet,
daß er der Rente entspricht, welche das Haus nach Abzug der
Baukosten abwerfen soll. Dadurch werden die spätern Hypo-
theken unsicher, und besonders schweben die Forderungen
der Bauhandwerker (Maurer, Zimmerer, Schreiner, Schlosser,
Glaser, Tapezierer, Tüncher usw.) stets in Gefahr, während
der Verkäufer der Baustelle für seine überhohen Forderungen
durch das starre Hypothekenrecht gesichert bleibt. Eine von
zahlreichen Innungen verfaßte Eingabe an das preußische
Abgeordnetenhaus von 1892 hat die Verluste der Bauhand-
werker in 731 Zwangsversteigerungen auf 75 Millionen Mark,
jedenfalls übertrieben, angegeben. Inwieweit das Reichs-
gesetz vom 1. Juni 1909 über die Sicherung der Bauforderungen
hier Abhilfe geschaffen hat, ist noch nicht zu erkennen.