﻿32. Die Wohnungsgcsctzgcbung in de» wichtigern AuKl-indsstantc» 14z

Alle diese Einrichtungen sind in verschiedenen Staaten und
Bezirken schon längst in Kraft, haben sich bewährt und wirkeil
ivohltätig. Ihre Aufnahme und Verallgemeinerung durch
das Reich bedeutet ein organisches Vorgehen zur Gesundung
des Wohnwesens im ganzen Reiche. Als weitere Forderung
ist beizufügen:

5. Errichtung einer staatlichen Kommission in jedem
größern Gebiete für städtische und industrielle Besiede-
lung nach dem Vorbilde der Rentengutskommissionen.

32.	Die Wohriungsgesetzgebung in den wichtigern
Auslandsstaaten

England hat, seitdem der öffentliche Geist durch die
.Mißstände in den Bergwerken und Fabriken vom Systein
des Gehenlassens abgedrängt wurde, und besonders seit Be-
ginn der Demokratisierung des Wahlrechts mit der Parlaments-
reform von 1832 eine Wohnungsgesetzgebung geschaffen,
die 1890 zusannnengestellt wurde. Sie umfaßt vorwiegend
bau- und gesundheitspolizeiliche Maßregeln. Mit dem Ein-
dringen der Arbeiter in die Gemeindevertretungen haben auch
die Gemeindeverwaltungen allmählich die Wohnungsreform
in die Hand genommen. Der Engländer legt weit mehr Wert
wie der Deutsche auf reinliches, angenehmes und gesundes
Wohnen. Der Londoner Grafschaftsrat und andere Gemeinden
haben hier viel geleistet durch Abbruch alter, ungesunder,
überfüllter und verrufener Quartiere, Erbauung neuer Woh-
nungen und großartiger Ledigenheime (S. 91). Das Wohnungs-
und Städtebaugesetz vom 3. Dezember 1909 gibt den städtischen
Behörden zwar nicht das Recht, die Baufluchten festzusetzen,
das taten in England von jeher die Privatgrundbesitzer, wohl
aber die ganze Plangestaltung sozialpolitisch zu leiten. In
England wohnt der einigermaßen besser bezahlte Arbeiter im
Einfamilienhaus (cottage), mit meistens 4 bis 5 Zimmern,
trefflichen Spül- und Abortanlagen, viel Licht und Luft