﻿8. Sie Tatsachen der Wohnungsnot in bezug auf die Wohnung zg

meist schlecht gereinigt und gelüftet, die körperliche Pflege
und Reinlichkeit wird sehr erschwert, das enge Zusammen-
leben mit vielen unseßhaften Persönlichkeiten von manchmal
schlechter und jedenfalls unkontrollierbarer Moral vermehrt
die sittlichen Gefahren, die das enge Zusammenleben an sich
schon bringt, besonders geht das Schamgefühl verloren.
Sittliche und physische Entartung ist die notwendige Folge.
Auch das Kostgängerwesen ist ein Krebsschaden des Arbeiter-
standes.

XII.	Die Mietpreise

haben in den Städten eine steigende Ten-
denz und erhöhen sich bei einem großen Teile der Be-
völkerung rascher als das Einkommen.

XIII.	Das eherne Wohrrgesctz-
ist die Folge, das heißt: der Mieter erhält für die höchst-
mögliche Mietleistung, die ihm abgenommen werden kann,
nur das Mindestmaß, das Existenzminimmn am Wohnungs-
bedürfnis.

XIV.	Feuchte Räume

sindum so häufiger, je kleiner die Wohnung,
Die Feuchtigkeit wird auch vielfach künstlich herbeigeführt durch
Kochen, Waschen und Wäschetrocknen in den Wohn-, Schlaf- und
Arbeitsräumen, statt in einein besonders dazu hergerichteten
Raume. So wird die Atemluft verschlechtert und die Wohnung
feucht. Feuchte Räume wirken ungünstig auf den Wärme-
haushalt der Bewohner; eine feuchte Wand ist wegen der
Wasserverdunstnng stets kalt, isoliert das Zimmer nicht gegen
Wärme und Kälte der Außenluft. Besonders schädlich ist das
Schlafen in solchen Räumen. Vielfach fehlt es an der Aufklärung
über die richtige Benutzung und Behandlung der Wohnungen.
Belehrungen durch die Wohnungsaufsicht, durch Merkblätter
und die Organisationen der Arbeiter sind sehr notwendig.

In Augsburg z. B. kochen das ganze Jahr hindurch in der
Küche 85 Prozent aller Haushaltungen, im Sommer sind