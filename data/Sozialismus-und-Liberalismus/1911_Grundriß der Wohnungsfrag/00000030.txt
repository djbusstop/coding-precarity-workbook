﻿32	Grundriß der Wohnungsfrage und WohnungsVolitil

auf drei mehr als 10 Bewohner kommen. Das ist viel zu weit
gegriffen; ein menschenwürdiges Wohnen verlangt für die
Nonnalfamilie von 4 bis 8 Personen als Mindestinaß zwei
Zimmer und Küche; die Grenze ist erreicht, wo auf ein Zimmer
mehr als 2 Personen entfallen. Das Mindesterfordernis einer
F a m i l i e n w o h n u n g sind eine Küche, Zimmer und
Kammer, also drei Räume.

Der richtige Maßstab ist der L u f t r a u m, der auf eine
erwachsene Person trifft. Hat eine solche nicht mehr als 10
Kubikmeter, so ist die Wohnung schon nach mäßigen Ansprüchen
überfüllt.

VIII.	Der jährliche Mictwechsel

ist um so stärker, je kleiner die Wohnungen sind (in Augsburg
z. B. trifft das jährlich ein Drittel aller Mietwohnungen).
Immer gilt die Regel, je größer die Wohnung, desto seltener,
je kleiner die Wohnung, desto häufiger der Wechsel.

IX.	Verhältnis von Einkommen und Wohnungsmiete

Die Ausgabe einer Haushaltung für die
W o h n u n g im Verhältnis zu i h r e m E i n k o m m e n ist uni
so größer, je kleiner das Einkommen ist: das S ch w a b e s ch e
Gesetz, wie der Statistiker Schwabe durch Untersuchungen
in Berlin 1867 es fand; es gilt allgemein. In den untersten
Einkommensstusen der städtischen Bevölkerung beträgt der
Mietaufwand 20 bis 25 Prozent, mitunter 33 Prozent und
noch mehr vom Jahreseinkommen der Familie. Als normaler
Aufwand gilt ein Fünftel bis ein Siebtel (20 bis 14 Prozent)
des Einkommens.

Die Meyersche Wohnungsstiftung in Leipzig, in
deren ausgedehnten Häuserbauten jetzt ungefähr 6000 Personen unter-
gebracht sind, nimmt grundsätzlich ein Siebtel des durch den Steuer-
zettel nachzuweisenden Familieneinkommens als Miete; der Mieter
muß seine Haushaltung danach einrichten, die Mieteausgabe wird
wie folgt berechnet: