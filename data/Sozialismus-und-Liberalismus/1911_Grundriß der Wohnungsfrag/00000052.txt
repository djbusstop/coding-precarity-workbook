﻿54	Grundriß der Wohnungsfrage und Wohnnngspolitik

VIII.	Landwirtschaftliche und städtische Bodenrente

Der große Unterschied zwischen landwirtschaftlichem und
städtischem Boden liegt darin, daß der erstere die unmittel-
bare Grundlage der Produktion ist, er wird bearbeitet oder
gepflegt, damit er Früchte trage; der städtische Boden aber ist
die mittelbare Unterlage der Produktion. Auf ihm werden
Wohnhäuser lind Werkstätten errichtet, um besonders jenen
Klassen, welche die Naturprodukte weiterverarbeiten, als
Wohn- und Arbeitsrämne zu dienen. In der Landwirtschaft
überwiegt der Naturfaktor, in der städtischen Produktion die
körperliche und geistige Arbeit des Menschen. Die städtischen
Tätigkeiten sind produktiver als die landwirtschaftlichen, die
städtische Bodenrente ist daher schon deswegen höher als die
ländliche; weiter wirkt die Anhäufung größerer Menschen-
massen auf kleine Räume steigernd auf die Bodenwerte. Diese
Menschenansammlung auf engem Raume bewirkt dann noch
weitere natürliche Unterschiede unter den städtischen Boden-
werten selbst.

IX.	Entstehung und Differenzierung der städtischen Boden-
preise, Hausplatz- und Kafernierungsrente

Der Boden ist auch in der Stadt ein Gebrauchsgegenstand,
den niemand entbehren kann. Auf dieser allgemeinen Grund-
lage wird seine Preisbildung noch von zahlreichen Einflüssen
bedingt. Dazu gehören: der landesübliche Kulturstand des
Volkes mit den Ansprüchen an die Wohnung, die Baukosten,
weit- oder engräumige Bebauung, weit- oder engräumiges
Zusammenleben in den Häusern und Wohnungen. Die wich-
tigsten Verwaltungs- und technischen Grundlagen, welche die
Preisbildung des Bodens maßgebend beeinflussen, werden
durch Bebauungspläne, Bodenaufteilung und Bauordnung
festgelegt, was in Deutschland amtlich geschieht. Sehr wichtig
für die Bildung der Bodenpreise ist die allgemeine wirtschaft-
liche Lage der breiten Volksmasse in Verbindung mit der