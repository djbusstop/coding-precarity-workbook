﻿21.	Steuererlcichtcrungc» und GetührcnermLtzignug für Klcinhiiuscr 11g

bayerisch eGesetz (siehe oben S. 109). In P r e u ß e n
erhalten gemeinnützige Baugesellschaften Befreinng von der
staatlichen Umsatzsteuer (Besitzwechselgebühr, Gesetz vom
31. Juli 1895). Das Oberverwaltungsgericht hat am 31. Ok-
tober 1905 ausgesprochen, daß der Begriff „unbemittel-
ten Familien" statutarisch festgesetzt sein muß und daß das
Steuerprivileg zu verweigern ist, wenn in der Satzung nur von
„Minderbemittelten" die Rede ist. Das Kammergericht hat
sich in der Entscheidung vom 14. März 1907 auf einen etwas
weitem Standpunkt gestellt und die Stempelsteuerfreiheit
einer Genossenschaft zugebilligt, die nach ihren: Statut be-
zweckt, „minderbenüttelten Familien" gesunde, billige und
zweckmäßig eingerichtete Wohnungen zu verschaffen. Hier
sind also u n bemittelte und m i n d e r bemittelte Familien
gleichgestellt. Die allgemeineSteuervergünstignng
von Häusern mit kleinen Wohnungen wurde von: preußischen
Oberverwaltungsgericht 1905 als den: Komnuinalabgabengesetze
widersprechend für ungültig erklärt.

Bayern gibt den Kleinwohnuugsbanten sechsjährige, den von
Gemeinden oder gemeinnützigen Vereinen hergestellten Kleinhänsern
mit nicht mehr als vier Wohnungen zwölfjährige Steuerfreiheit, auch
für die Gemeindesteuer wirksam. Die Bauvereine sind beim Erwerb
von Grundbesitz von der staatlichen und gemeindlichen Besitzwechsel-
gebühr befreit. Gut geleitete Banvereinc benützen diese Steuerfreiheit
zu rascherer Tilgung ihrer Schulden. In Württemberg un!>
Baden sind die Baugenossenschaften von der Umsatzsteuer, in H e s s e n
von den Stempeln und Gerichttgebühren befreit. Belgien hat in
dem bahnbrechenden Gesetze vom 9. August 1888 über die Arbeiter-
wohnungen auch verschiedene Steuervergünstigungen gewährt, nämlich
die Befreiung von den Personalsteuern, die auf Grund des Mietwerts,
der Fenster, der Türen und des Mobiliars erhoben werden sowie eine
Herabsetzung der Stempelgebühr bei Hausverkäufen, Hypotheken-
umschreibungen und Darlehnsverträgen. Frankreich ist in dem
Kleinwohnungsgesetz von 1894 dem Beispiele Belgiens gefolgt und
hat den Kleinwohnungen eine fünfjährige Befreiung von der Gebäude-
sowie von der Tür- und Fenstersteuer und ferner den Baugesellschaften
die Befreiung von der Gewerbesteuer und der Abgabe der toten Hand-

8»