﻿9ß	Grundriß dcr Wohnungsfrage und Wohnnngsholltlk

verzicht, die gemeinnützige Bautätigkeit von Privaten und
Gesellschaften und die Wirkung der Baugenossen-
schaften. Die Bedeutung dieser Art von Wohnungsher-
stellung liegt weniger in ihrem Einfluß auf den Wohnungsmarkt
und in Milderung der allgemeinen Wohnungsnot, denn hier
wird die Hauptaufgabe stets der gewerbsmäßigen Bautätigkeit
vorbehalten bleiben. Hauptaufgabe der gemein-
nützige nBantätigkeitund der organisierten
Selbsthilfe ist die Pionierarbeit: sie sollen vor-
bildlich wirken durch Herstellung gesundheitlich und sittlich
einwandfreier Wohnungen zu billigen Preisen, also nüt rich-
tigem Verhältnis zwischen Leistung und Gegenleistung. Alle
Fortschritte und Verbesserungen im Kleinwohnungswesen
sind einzig der Bautätigkeit niit Gewinnverzicht zu verdanken,
besonders hat die Baugenossenschaft das moderne A r-
b eiterhaus geschaffen und hat erzieherisch auf seine Be-
nutzung eingewirkt. Sie hat die Arbeiter ans der Hilfs- und
Hoffnungslosigkeit, in welche die spekulative Wohnungs-
versorgung sie getrieben, herausgeführt, ihnen wieder das
Gefühl der Verantwortlichkeit und der Freude an der Mit-
wirkung zur Besserung des eignen Loses gegeben. Die
Meyersche Stiftung in Leipzig hat durch strenges Festhalten
an dem Grundsätze, daß die Miete ein Siebtel vom Einkonnnen
nicht übersteigen darf, durch wöchentliches Einholen der Miete
und sonsüge erzieherische Maßregeln bewirkt, daß die durch
sparsame Wirtschaft emporgekommenen und nun größere
Wohnungen suchenden ehemaligen Mieter dieser Stiftung
jetzt von den privaten Leipziger Hausbesitzern gern auf-
genommen werden. Grund- und Hausbesitzerver-
e i ne übersehen bei ihrem Kampfe gegen die Baugenossen-
schaften die hohe sittliche Bedeutung derselben, sie übersehen
aber auch, daß die Summen, welche aus öffentlichen Mitteln
der gemeinnützigen Bautätigkeit in Deutschland gegeben
werden, fast vollständig verschwinden gegenüber der Milliarde,