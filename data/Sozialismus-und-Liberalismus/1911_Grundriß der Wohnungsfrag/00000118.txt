﻿120	Grundritz drr WnhnnngLfragc und Wohnungspoliiik

seinen wesentlichen Teilen nicht durch die Tätigkeit des
einzelnen Grundbesitzers, sondern durch die geistige, wirt-
schaftliche und politische Leistung der Gesamtheit der Nation
herbeigeführt wird. Grundlage ist der Aufschwung des deutschen
Wirtschaftslebens seit 1879 durch die Schaffung eines nationalen
Zoll- und Wirtschaftssystems. Fernere Grundlage ist die
Friedens- und Nechtsbürgschaft, die Reich und Staat dem
Wirtschaftsleben geben. Reich und Staat haben daher das
erste Recht auf Beteiligung au diesem Wertzuwachs. Aber
auch die Gemeinden haben ein Recht darauf, denn der
Aufschwung des Wirtschaftslebens kennzeichnet sich neben der
Steigerung der Bodenwerte auch durch das Wachstum der
Bevölkerung, und dieses legt den Gemeinden schwere wachsende
Lasten, besonders für Straßen, Beleuchtung, Schulen, Armen-
pflege und allgemeine soziale Zwecke auf. Die Besteuerung
des unverdienten Wertzuwachses für Reich, Staat und Ge-
meinde ist eine Forderung der ausgleichenden Gerechtigkeit
im Steuerwesen, nachdem die breite Masse des Volkes durch
indirekte Steuern stark belastet ist. Um das, was die Besteuerung
des unverdienten Wertzuwachses einträgt, können die andern
Steuern entlastet werden. Die Besteuerung des unverdienten
Wertzuwachses wurde zuerst durch einen Antrag Dr. Jäger
im bayerischen Landtage 1902 angeregt, mit Teilung des
Ertrags zwischen Staat und Gemeinde. F r a n k f u r t a. M.
führte die Steuer 1904 ein, 1905 folgte C ö l u, das Drei-
klassenwahlsysteni hemmte diese Bewegung, so daß,die
Steuer frei blieb für das Reich. Das Reichsgesetz vom
14. Februar 1911 weist 50 Prozent dem Reiche, 10 den
E i n z e l st a a t e n, 40 den Gemeinden zu. Wieweit
sich die Hoffnungen erfüllen werden, daß der Zwang,
einen Teil des unverdienten Wertzuwachses bar als
Steuer abliefern zu müssen, die Geländespekulatiou
mildern werde, ist bei der Kürze der Zeit noch nicht
abzusehen.