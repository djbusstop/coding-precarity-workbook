﻿SO. Finanzielle Organisation des Klciiiwohnuugsbaues	sllll

stellung die Darlehen gewährt sind, hypothekarisch verpfändet. All-
inählich ging die Verwaltung auch dazu über, mit den Geldern Haus-
grundstücke zu beleihen, die in das Eigentum einzelner
zwar übergehen, aber durch die Bestellung eines Wieder-
k a u f r e ch t s der Spekulation oder mißbräuchlichen Benutzung ent-
zogen werden. Neuerdings sucht die Verwaltung auch die Wohnungs-
verhältnisse von Staatsbediensteten durch Errichtung von Zwerg-
rentengütern in Verbindung mit Beleihung derselben aus den
staatlichen Mitteln zu verbessern.

Ähnlich verfahren B a y e r n, W ü r t t e m b e r g, B a d e n, S a ch-
s e n mit ihren Angestellten, besonders im Eisenbahn- und Verkehrs-
wesen?)

Eine allgemeine Geldquelle haben von den
deutschen Staaten für den Kleinwohnungsbau bis-
her nur zwei geschaffen: Hessen und Bayern.

Hessen hat mit Gesetz vom 6. August 1902, durch Gesetz vom
7. August weiter geregelt, seine Landeskreditkasse ermächtigt,
neben Geldern für allgenieine Kulturzwccke auch solche zuin Wohnungs-
bau für Minderbemittelte zu geben. Als zu solchen Wohnungen bestimmt
gelten Häuser, in denen Wohnungen mit nicht mehr als drei Zimmern
nebst Küche und Zubehör vorgesehen sind. Die Darlehen wurden an-
fangs nur an Gemeinden oder Gemeindeverbände gegeben,
und zwar bis zürn vollen Betrage der Kosten für Gelände und Bau,
in der Regel ohne dingliche Sicherung. Der ausbedungene Tilgungs-
betrag kann jedes fünfte Jahr unterbrochen werden, um größere Repara-
turen am Hause vorzunehmen oder Mittel dafür zu samrneln, bedürftigeir
Gemeinden kann der Zinsfuß auf zehn Jahre bis auf % Prozent er-
mäßigt werden. Der Fehlbetrag ist dann alsstaatlicherZuschuß
zurFörderungdesKleinivohnungswesensim Staats-
haushalt einzusetzen. Die Gemeinde kann mit dem Darlehn entweder
selbst bauen oder es an gemeinnützige Vereinigungen öffentlichen oder
privaten Rechtes zur Erbauung von Wohnungen für Minderbemittelte
weiter geben. Höher» Zins oder längere Tilgung darf sich die Gemeinde
dabei nicht ausbedingen. Im Falle eines in anderer Weise nicht zu
beseitigenden Mangels an solchen Wohnungen können die Ge-
meinden auf Antrag einer derartigen Bauvereinigung durch den

y Die bayerischen Bedingungen für Beleihung im Erbbaurecht siehe
Landtagsverhandlungen 1910/11, außerordentliches Budget Anlage 5,
S. 43.