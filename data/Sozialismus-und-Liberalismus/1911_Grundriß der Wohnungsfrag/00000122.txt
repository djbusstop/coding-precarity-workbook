﻿j 24	Grundriß der Wohnungsfrage und Wohnungspolitik

7. Juli 1891 geschehen, das zunächst für landwirtschaftliche
Arbeiter berechnet tvar, aber neuerdings anch auf Industrie-
arbeiter ausgedehnt wurde. Der Arbeiter leistet eine mäßige
Anzahlung, der Rest des Kaufpreises wird ihm von einer staat-
lichen Rentenbank zu 3% Prozent vorgeschossen und durch eine
4prozentige Jahresleistung mit Zins und Tilgung in 40 Jahren
heimgezahlt. Damit der Tod des Besitzers keine Störung
bringt, wird auch hier Verbindung mit der Lebensversicheruirg
empfohlen. Die Gründung muß ausgehen von einem Ge-
meindeverband, einem gemeinnützigen Verein oder einem
Arbeitgeber; spekulative Interessen müssen bei Aufteilung der
Grundstücke ausgeschlossen sein. Ein preußischer Ministerial-
erlaß vom 8. Januar 1907 regelt diese Verhältnisse: derartige
Rentengüter sind „Einfamilienhäuser nebst einem Garten von
chg Hektar (rund y2 Morgen) Größe, die nicht gegen Bar-
zahlung des Kaufpreises, sondern im Wesen gegen Übernahme
einer festen Geldrente bezahlt werden". Mindestgröße eines
derartigen Rentenguts ist 12,5 Ar = 1250 Quadratmeter;
der Käufer muß ein Zehntel bis ein Achtel vom Wert anzahlen.
Eine Schwierigkeit besteht darin, daß der Staat das Geld
in Rentenpfandbriefen gibt , die zu 3% Prozent stark unter
100 Prozent stehen, bei einem 3f4prozentigen Papier ein
Verlust von 9 bis 10 Prozent. Die Landesversicherungen
sollten hier den Nennwert auszahlen und ihre Darlehen um
den Betrag des Minderwerts erhöhen. Wegen der Gemein-
nützigkeit der Anlage könnte der Verlust auch anderweitig
übernonnnen werden. Bei derartigen Ansiedlnngcn ist niedriger
Zinsfuß eine Hanptbedingung.

26.	Freie Organisationen zur Wohnungsresorm
und Banberatungsstellen

Die älteste derartige Vereinigung (seit 1897) ist der Rh e i-
nische Verein für Kleinwohnungswesen,