﻿ktz	Grundriß der Wohnungsfrage «nd Wohnungspolitik

Hier wird übersehen, daß in höherm Maß als die soziale
Gesetzgebung die ungesunde Bodenaufteilung und die Miet-
kaserne der Großindustrie den Wettbewerb mit den Ländern
des Einfamlienhauses erschweren. Dabei macht die soziale
Gesetzgebung den Arbeiterstand körperlich und geistig rüstiger;
er wird auch gegenüber der Industrie kaufkräftiger, während
das Wohnungselend ihn körperlich und sittlich herabdrückt
und seine Kaufkraft gegenüber der Industrie sehr abschwächt.

1V. Kleinhaus oder Massenmiethaus

Eine kleine Anzahl von Wohnungspolitikern, besonders Voigt
und Adolf Weber, behaupten, das Großhaus verbillige die
Mietpreise, und überhaupt sei die Spekulation nicht verwerflich,
die ganze Frage sei mehr eine Frage der Baukosten als des
Bodens. Richtig ist daran nur, daß die allgemeinen und Ver-
waltungskosten, dann die Kosten des Bodens, des Fundaments
und der Bedachung durch Verteilung auf mehrere Geschosse
für die einzelne Wohnung sich herabmindern. Das gilt aber
nur bis zu einer gewissen Haushöhe; vom fünften bis sechsten
Geschoß ab wachsen die Baukosten sehr stark wegen der
Schwierigkeit, in der Höhe zu bauen. Dazu kommt bei dem
Großhause die Notwendigkeit größerer Hofflächen, stärkerer
Mauern, größerer Treppenhäuser und sonstiger verteuernder
Einrichtungen für Tragfähigkeit, Verkehr und Feuersicherheit
sowie höhere Ausgaben für Straßen und Entwässerung.

Eine sorgfältige Berechnung, wie der Bodenpreis auf die
Herstellungskosten einer Wohnung wirkt, hat B a n m e i st e r
angestellt für Kleinwohnungen von 90 Quadratmeter Wohn-
fläche in eingebauten Häusern amt verschiedenen Geschoß-
zahlen auf Grund der Karlsruher Bauordnung.

„Bei einem Bodenpreise von 20 der als Mittelwert auf dem für
Kleinwohnungen geeigneten städtischen Gelände vielfach vorkommt,
kostet eine Wohnung im dreistöckigen Hause durchschnittlich nur 29 .«
mehr als im fünfstöckigen. Ist dieser Zuschlag nicht unerheblich gegen-