﻿128	Grmldritz der W»huu»gsfr»gc und Wohiumgspolitik

wird stets auf dem Boden privater Besitzer ausgeübt, die durch
ihren ausgedehnten Grundbesitz vielfach ein Monopol auf
Wohnungsboden haben; das deutsche hat bei den Privat-
besitzern keinen Beifall gefunden. Es einpfiehlt sich in der
Tat nur für grundbesitzende Dauerkörperschaften; Reich, Staat,
Stiftungen und besonders Gemeinden können auf diese Weise
ihren Grundbesitz zu energischer Betreibung der Wohnungs-
reform verwenden. Je umfangreicher dieser Besitz ist, und be-
sonders je einflußreicher er im Stadterweiterungsgebiet zer-
streut liegt, desto mehr können derartige Grundbesitzer sich
zunächst eine sichere, dauernde und steigende Einnahme
durch Beteiligung an der natürlichen Steigerung des Boden-
werts (Zuwachsrente) verschaffen, statt sie durch Verkauf
des Geländes dauernd herzugeben; sie können ferner der
künstlichen Preistreiberei der Bodenwerte und besonders den
Ringbildungen privater Grundbesitzer entgegenwirken und zu-
gleich auf die Gesundung des Wohnwesens hinwirken. Es ist
daher ein schwerer Fehler des Staates, wenn er seinen Wald-
besitz, bzw. des Reiches, wenn es seine militärischen Gelände
an Spekulanten verkauft (z. B. das T e m p c l h o f e r Feld
bei Berlin) und dadurch mithilft, die Wehrfähigkeit unseres
Volkes noch weiter zu verringern.

Die Schwierigkeit der Geldbeschaffung
beim Erbbau führt zu verwickelten Darlehnsverträgen. Am
besten ist es, wenn der Grundeigentümer, der das Gelände
in Erbbau ausgibt, auch das Baugeld liefert. Eine Gemeinde
kann dabei leicht bis zu 80 Prozent, wenn sie ihre eignen
Bediensteten derartig ansetzt, bis zu 90 Prozent gehen. Die
Stadt Mannheim hat für solche Verträge folgende Bedingungen:

1.	Der Bau muß innerhalb 5 Jahren nach Abschluß des Vertrags
nach Plänen, die der Stadtrat genehmigte, beginnen; auch müssen
Änderungen vom Stadtrate geprüft und genehmigt werden.

2.	Bei Aufnahme von Hypotheken und Festsetzung der Tilgungspläne
muß der Stadtrat seine Genehmigung erteilen.