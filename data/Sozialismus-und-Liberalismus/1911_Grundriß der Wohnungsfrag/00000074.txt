﻿76	Grundriß der Wohnungsfrage und WohnungsPolittk

stücksmarkte; möglichste Vermehrung des kommunalen
Grundbesitzes, insbesondere durch solche Ländereien,
die bei Ausdehnung der Gemeinde voraussichtlich für Stadt-
erweiterungszwecke gebraucht werden, Zentralisierung dieser
Bestrebungen in eineni besondern Verwaltungszweige (Grund-
stücksfonds, Stadterweiterungsfouds usw.); soziale Verwertung
des gemeindlichen Grundbesitzes, sei es beim Verkauf, bei
Hergäbe im Erbbaurecht oder für gemeinnützige Zwecke, Fest-
setzung von Bedingungen beim freien Verkauf, wie z. B. Rück-
kaufsrecht, Teilnahme der Stadt an etwaigen Wertsteigerungen,
Festsetzung von Bebauungsfristen und der Folgen ihrer Nicht-
einhaltung, Anlage von Wohnhauskolonien in Form von
Gartenstädten oder auf sonstige, das Kleinhaus und das gute
und gesunde Wohnen fördernde Weise, Anlage von Garten-
kolonien (Schrebergärten, Spielplätzen, ErholungsParks usw.)
Die Wald- und Wiesengürtel dürfen nicht hemmen und müssen
daher durchbrochen sein, am besten schieben sie sich keilförnrig
in die Stadt hinein. Beim Ankauf von Gelände muß die Ge-
rneinde vorsichtig sein, damit sie nicht selbst die Preise steigert;
ist sie umsichtig dabei, so treibt sie auch gute Finanzpolitik.
Ein Grundgedanke ist dabei, in den äußern Ringen kaufen, in
den innern verkaufen. Hat die Gemeinde vielen Grundbesitz,
so kann sie nicht nur den Markt beherrschen, die Stadterweite-
rung maßgebend beeinflußen und Phantasiepreise der privaten
Grundbesitzer herabdrückcn, sondern auch die laufende Be-
bauung so regeln, daß nicht so viele unnütze Gelder für Her-
stellung der Straßen ausgegeben werden müssen, die erst in
der Zukunft bebaut werden.

Die Aufteilung des Baugeländes hat zu geschehen vom
Gesichtspunkte des Kampfes gegen die Miet-
kaserne und das Boden Monopol, also nach
allen Richtungen hin Vermehrung des Angebots von Land,
besonders für Kleinhäuser: kleineBaublöcke, schmale
Wohnstraßen mit rechtzeitiger Sorge für baureife