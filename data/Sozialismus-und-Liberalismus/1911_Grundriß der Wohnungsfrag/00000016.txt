﻿lg	Grundriß der Wohnungsfrage und Wohumigsholltik

lind Kindern in einem und dazu kleinem Hause wohnten. Am
schlechtesten wohnten die Proletarier und Sklaven. M a r t i a l
spricht im l. Jahrhundert der Kaiserzeit von finstern Proletarier-
wohnungen, in die mair nicht aufrecht eintreten sonnte und von
den engen Zellen, in welchen die unfreien Grundschichten der
Bevölkeruiig wohnten, und sagt, daß oft ein Krug, eine Matte,
eine Wanze, ein Haufen Stroh, ein Bettgestell das einzige
Mobilar gewesen nnb eine kurze Toga der einzige Schutz
gegen die Kälte. Die Forderung nach Aufhebuiig oder Er-
mäßigung der Miete war ebenso volkstümlich wie das Ver-
lairgen nach Aufhebung des Zinses. Beide Forderungen durch-
ziehen die letzten Jahrhunderte Roms. Cäsar und Oktavian
sahen sich daher genötigt, ihrer Volkstüuilichkeit wegen je ein-
ural einen einjährigen Mietnachlaß zu verordnen.

5.	Die städtische Wohmmgs- und Bodenpolitik
im deutschen Mittelalter

Das städtische Bürgertum bildete sich zunächst innerhalb
der Grundherrschafteu. Diese vollzogen die Aufteilung des
Bodens meist überlegt, planuiäßig und geschäftlich und gaben
die Parzellen in Leihe zur Bebauung. Die mittelalterliche
d e u t s ch r e ch t l i ch c Einrichtung der städtischen Boden-
leihe (Weichbildrecht) trennte grundsätzlich das
Eigentum an Boden und Bauwerk. Der Be-
liehene erhielt das selbständige Eigentumsrecht au der Benutzung
des Bodens und an der „Besserung", dem Bauwerke, beides
ohne Zeitbegreuzung, frei veräußerlich und vererblich gegen
Errichtung eines festen Erbzinses, den der Besitzer nicht er-
höhen und, solange die Rente (ewige Rente) bezahlt wurde,
auch nicht kündigen konnte. Dieses mittelalterliche deutsche
städtische Bodenrecht ging weiter wie die heutige englische
Bodenlcihe, die meist auf 99 Jahre erfolgt und nach Ablauf
dieser Zeit das Bauwerk dem Bodeneigentümer zufallen läßt;