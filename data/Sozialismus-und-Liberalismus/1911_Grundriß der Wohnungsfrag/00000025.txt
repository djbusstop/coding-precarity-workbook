﻿8. Dir Tatsachen der Wohmmgsiiot in bezug aus die Wohnung 27

ob die allgemeinen Bedingungen zur Wiedergesundung des
wirtschaftlichen Lebens günstig sind oder nicht, dann die bessere
oder schlechtere Regelung derBodeuaufteilung und besonders des
Baukredits. Die Zeit des Jndustrieaufschwungs bringt wegen
des Zudrangs zu den Arbeitsmittelpunkten meist empfind-
lichenWohnungsmangel,der in derZeit derStockllug sich mäßigt.
Sehr stark wird die Wohnungsproduktion durch ben Zinsfuß
beeinflußt; hoher Zins wirkt hemmend, niedriger erleichternd.
Maßstab des W o h n u n g s m a r k t e s ist die Zahl der
l e e r st e h e n d e n W o h u u n g e n. Das Verhältnis von
Angebot und Nachfrage auf dem Wohnungsmarkt ist erfahrungs-
gemäß nornial, wenn etwa 3 Prozent der brauchbaren Woh-
nungen leerstehen.

Dieser Prozentsatz betrug z. B.:

	Ende 1908	Ende 1909
in Mainz	2,7	4,1
„ Mannheini	3,2	2,6
„ Bremen	3,0	2,4
„ Königsberg	0,8	2,2
„ Breslan	3,8	4,6
„ Kiel	3,7	4,4
„ Frankfurt a. M.		4,3
„ Cöln		3,7
„ Straßburg		0,9
Die bayerischen	Städte haben vielfach	eine besonders
starke Wohnungsnot,	und M ü n ch e n steht	an der Spitze,

indem die Zahl der leerstehenden Wohnungen seit Jahren
nur etwa 0,6 Prozent, nach der Zählung vom 1. Dezember 1910
bei Kleinwohnungen nur 0,3 Prozent beträgt.

IV. Der Bau von Wohnungen für die minderbemittelte»
Klaffen

(etwa 85 Prozent der städtischen Bevölkerung) bleibt in
der Regel hinter dem Bedürfnis zurück und
besonders finden kinderreiche Familien sehr schwer Unter-
kommen. Die Vermietung zahlreicher Kleinwohnungen inacht