﻿

184	Grundriß der Wohnnngssragc und WohimngSpolitik

zugute kommeit. Diese Pachten können um die Hälfte und mehr hinter
der großstädtischen Grundrente zurückbleiben und gleichwohl die Ver-
zinsung und Rückzahlung des Gründungskapitals sowie einen gesunden
Haushalt für eine Stadt gewährleisten, die den höchsten Anforderungen
entspricht und durch ihre Privatgärten an jedem Hause und mit ihren
ausgedehnten öffentlichen Anlagen mehr einem Garten als einer Stadt
gleicht.

Die Verbindring zwischen Industrie und Landwirtschaft,
auch die Zufuhr der Nahrungsmittel in die Gartenstadt kann
in der neueir Gründung besser gelvahrt werden als in den
jetzigen Riesenstädten. Auch die Kunst kommt lvieder zu
ihrem Rechte. Die ganze Anlage soll sich dem Gelände an-
passen, mit schwach gekrümmten Straßen, wodurch die künst-
lerische Behandlung der Häuser zur Geltung kommt; Reihen-
häuser sind billiger und wärmer als einzelstehende, die Vor-
gärten nach Norden sind klein, iveil sie wegeit der Straße
doch kaum benutzt werden können, der eigentliche Hausgarten
liegt nach Westen und Süden. Die Bäume im Garten stehen
nicht in der Mitte, sondern an den Rändern, damit der Schatten
auf die Straße fällt. Wie im deutschen Mittelalter werden
nun lvieder Städte gegrüildet, in kurzer Zeit erbaut, und,
was das Mittelalter nicht kannte, feierlich eröffnet. Das
Endziel ist Durchsetzmlg des ganzen Landes mit derartigen
Gartenstädten, eine neue innere Kolonisation mit neuen
Boden-, Wohnungs- und Lebensverhältnissen. Die juristische
Fornmlierung der Gründung ist nicht leicht, damit nicht der
Wertzuwachs des Geländes schließlich doch die Selbstsucht
der Bürger erregt und der gemeinnützige Charakter der
Stadt dadurch gesprengt wird. Eine sichere Bürgschaft dafür,
daß die Bodenspekulation und damit der kapitalistische Druck
aus Verschlechterung der wirtschaftlichen, gesundheitlichen und
sittlichen Bedingungen des Wohnwesens dauernd verhindert
wird, besteht nur dann, wenn der Boden der Gartenstadt
öffentliches Eigentum ist, also in beu Händen von Stadt
oder Gemeinde bleibt und ein Erbbauzins erhoben wird.