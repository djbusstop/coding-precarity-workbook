﻿9. Die Tatsachen der Wohnungsnot in bezug auf Haus, Boden und Städtebau 4z
in Berlin	auf 42 Prozent aller Grundstücke

Schöneberg	,, 41	„
Charlottenburg	,, 27	
Posen	„ 15	„	
Hamburg	„ 10 „	
München	„ 8	„

in den größern Städten Westdeutschlands dagegen in der
Regel noch nicht 1/z Prozent aller bewohnten Grundstücke.

In Preußen kamen durchschnittlich auf ein Wohnhaus in:
Jahre 1890 9,13, in: Jahre 1895 9,38, im Jahre 1900 9,57,
im Jahre 1905 9,79 Köpfe. Die durchschnittliche Behausungs-
ziffer in den preußischen Städten ist 15,50. Schon im Jahre
1900 stand Deutschland mit seiner Durchschnittszahl von
1,82 Haushaltungen auf ein Wohnhaus an der Spitze der
Länder Europas. Die Familien- oder Haushal-
tung s z i f f e r, die Spitäler, Gefängnisse und Kasernen
niiteingerechnet, nimnit in Deutschland ab, entsprechend der
Abnahme der Geburtenziffer. Die Haushaltungsziffer betrug:

1871	4,70 Personen	1900	4,59 Personen

1880	4,69	„	1905	4,57

Die B e h a u s u n g s z i f f e r ist in den Industriestädten
Englands, selbst in Sheffield, Birmingham, Manchester
usw., und in den Industrie- und Handelsstädten Belgiens
etwa fünf die Ziffer des Einfamilienhauses; sie ist in
London 7, in Brüssel 9, in Lüttich 8, Gent 5, Antwerpen 7,
Verviers 11, Amsterdam 13. Das Beispiel dieser Länder
mit ihrer hochentwickelten Industrie und starken Bevölkerung
zeigt, daß die moderne industrielle und großstädtische Ent-
wicklung ganz wohl möglich ist ohne den Massenpferch.

I I. Die Wohnziffer,

d. i. die Zahl der in einem Gebäude untergebrachten Haus-
haltungen oder Familien hat, wie die Haushaltungs-
ziffer, noch eine steigende Tendenz.

l.