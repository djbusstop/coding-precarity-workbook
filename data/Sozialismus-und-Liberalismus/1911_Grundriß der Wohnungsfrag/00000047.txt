﻿9. Die Tatsachen der Wohnungsnot in bezug aus Haus, Boden und Städtebau 4g

Von je 1000 Bewohnern wohnten nach der Zählung vom
1. Dezember 1905 in

Met- Eigentümer-
Wohnungen

Berlin	939,8	23,1
Charlottenburg	923,0	34,2
Breslau	918,9	45,1
Königsberg	911,0	60,8
Leipzig	910,4	71,8
Altona	890,3	89,4
Chemnitz	878,6	98,1
Essen	709,4	115,1
Dortmund	844,0	136,5
Aachen	827,1	144,3
Mannheim	794,1	144,6
Straßbnrg	798,8	158,7
Freiburg Br.	791,4	167,1
Lübeck	627,8	346,4

Der Rest hatte Dienst- oder Freiwohnungen.

Die Tabelle zeigt ebenfalls den großen Unterschied zwischen
West- und Ostdeutschland; dieses hat in den großen Städten
hohe Behausungsziffern wegen des Vorwiegens des Groß-
hauses und der Mietkaserne. Die niedern Ziffern in West-
und Süddeutschland mit Ausnahme von München zeigen den
Einfluß des Klein- und Einfamilienhauses auf die Verteilung
des Boden- und Hausbesitzes. Die Ziffern zeigen aber auch,
wie sehr der Gedanke, fiir sich allein im eignen
Hause zu wohnen, nicht bloß dem städtischen Arbeiter-
stande, sondern auch den mittlern Schichten allmählich ver-
loren gegangen ist.

V.	Die Mietkaserne,

die Zusammenballung vieler Familien in kleinen Wohnungen
auf einem größern Grundstück ohne wirksame Hof- oder Garten-
nutzung am Hause, wobei viele Familien auf demselben Ge-
schoß untergebracht und alle auf dieselben Treppen und Gänge

Iaeger, Grundriß der Wohnungsfrage und Wohnungspolittk.

4