﻿Die gemeindlich: WohnungsPolitik	7g

hält zwei bis drei Zimmer mit allen erforderlichen Nebenräumen. Die
Verkaufsbedingungen sind so gestellt, daß die Stadtverwaltung in der
Lage ist, dem Häuserwucher zu steuern, eine Steigerung der Mietpreise
zu verhindern und einer Verschlechterung und Verwahrlosung der Ge-
bäude entgegenzuwirken: Rückkaufsrecht für 100 Jahre, Festsetzung
der Höchstbeträge der Mieten durch die Gemeindebehörde usw. Als
Käufer werden nur verheiratete oder verwitwete Personen mit
Kindern zugelassen; kinderreiche Familien werden bevorzugt. Der
Käufer muß das Haus selbst bewohnen, darf keinen Schlafgänger auf-
nehmen und die Aftermieter nicht im Preise steigern. Von dem auf den
Selbstkostenpreis begrenzten Kaufschilling hat der Käufer 10 Prozent
bar anzuzahlen, den Rest mit 8 Prozent zu verzinsen und mit 2% Prozent
(also in 23 Jahren) zu tilgen. Wenn 50 Prozent des Kaufschillings
gezahlt sind, kann der Schuldner die weitere Tilgung einstellen; anderseits
kann in Krankheitsfällen und bei sonstigen außerordentlichen Umständen
die Zahlung zeitweise gestundet werden, außerdem gewährt in diesen
Fällen die Hospitalverwaltung den Eigenhausbesitzern noch sogenannte
Unterstützungsdarlehn. Die Erfahrungen, welche die Stadt mit den
Käufern der Eigenhäuser gemacht hat, sind im allgemeinen recht erfreu-
lich. Die günstige Einwirkung des Besitzes eines eignen Heims ans die
Sparsamkeit, den Ordnungs- und Familiensinn ist uiiverkennbar zu-
tage getreten. Um die Spekulation zu verhindern, hat sich die Stadt
durch Vormerkung im Grundbuche statt Erbbau das Wiederkanfrecht
(§8 487 bis 503 BGB.) vorbehalten, und zwar auf 100 Jahre und
bei den Vorgärten auf 200 Jahre. Diesem Wiederkauf wird genau
jener Preis zugrunde gelegt, der zur Zeit der Erbauung des
Hauses für die Grundfläche und den Bau angerechnet wurde
mit Zurechnung jenes Betrags, um den das Gebäude durch Ver-
besserungen im Werte gestiegen ist, soweit dieser Mehrwert zur Zeit
des Wiederkaufs noch vorhanden ist, während derjenige Betrag vom
Kauf- und Bauschillinge wieder abgezogen wird, um den sich der Wert
der Liegenschaft durch die Benutzung verringert hat. An der festgestellten
Wiederkausssumme wird die Restschuld des Grundstückseigentümers
gegen die Stadt an Hauptsumme und Zinsen abgerechnet. Das Vor-
kaufsrecht für die Vorgärten gilt für den Fall, daß diese ganz oder teil-
weise für Straßenverbreiterung benutzt werden müssen. Auch hier ist
der Preis für den Wiederkauf von Wohnhäusern in Mark und Pfennig
für den Quadratmeter festgesetzt, wobei der Eigentümer für Aufwen-
dungen in dem Vorgarten keine Entschädigung beanspruchen, wohl aber
Einrichtungen, die eine Verbesserung herbeiführten, wieder beseitigen
kann.