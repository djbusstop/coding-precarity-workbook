﻿28	Grundriß der Wohnungsfrage und Wohnungspolitik

viel Mühe und Verdruß, man baut und veruüetet daher
lieber Wohnungen für wirtschaftlich bessergestellte Familien.
Dazn kommen noch drei wichtige Ursachen: die hohe, das
richtige Maß oft übersteigende Belastung des Grundbesitzes
durch öffentliche Abgaben, was die Bautätigkeit
herabdrückt; der Umstand, daß sich das Kapital, durch
unsere Rechtseinrichtungen und Kreditorganisation unterstützt,
lieber dem Boden als den Wohnungen zu-
wendet und beit eigentlichen Baukredit vernach-
lässigt; dann der weitere Umstand, daß unsere Verwal-
tungen noch vielfach der falschen Meinung sind, die minder-
bemittelten Stände niüßten in Groß- statt in Klein-
häusern untergebracht werden.

V.	Die Engriiumigkeit der Wohnungen
kennzeichnet die Lebensverhältnisse des weitaus größten
Teiles der städtischen und besonders der großstädtischen Be-
völkerung. Diese lebt zur Hälfte und oft noch mehr in Woh-
nungen von ein bis zwei nicht immer heizbaren Zimmern
mit oder ohne Küche. Alle die zahlreichen Untersuchungen
amtlicher und privater Art ergeben in ganz Deutschland stets
das selbe Bild knapper und oft auch schlechter Wohnverhält-
nisse, mit Bildern von tiefstem Wohnungselende durchsetzt,
wobei sich von Stadt zu Stadt kleine Verschiedenheiten zeigen,
die aber das trübe Gesamtbild nicht wesentlich verändern.

Von je 1000 Bewohnern wohnten nach der (letzten) Zählung
vom 1. Dezember 1905 in Wohnungen mit heizbaren Zimmern
oder Wohnräumen:

	0	1	2	3
Berlin		6,9	409,7	337,5	125,0
Charlottenburg		3,6	217,7	316,6	170,9
Breslau		0,1	374,9	341,6	148,6
Hamburg		4,1	166,3	309,2	277,4
Hannover		1,1	247,2	377,2	180,2
Karlsruhe		—	29,7	294,8	286,0
Straßburg		—	43,3	319,1	268,8