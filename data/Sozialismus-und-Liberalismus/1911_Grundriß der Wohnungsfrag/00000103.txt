﻿i«. Finanzielle Organisation des KlcinwohnungsbaucS	lyz

Vom Reich unterstützte Bau-

Stand am	Baugenossen-	genossenschaften	
31. Dezember	schaften	überhaupt	v. H. der Gesamtzahl
1801	396	10	2,6
1902	491	19	3,9
1903	537	36	6,7
1904	590	47	8,0
1905	642	54	8,4
1906	681	65	9,6
1907	747	72	9,6

20.	Finanzielle Organisation des
Kleinwohnungsbaues

Das ist neben der richtigen Bodenaufteilnng die wi ch-
t i g st e Seite der Wohnungsfrage. Den Jahres-
bedarf Deutschlands zur Herstellung von Kleinwohnungen
mit 800 Millionen Mark (S. 63) aufzubringen, begegnet
großen Schwierigkeiten; nicht, weil das Geld nicht vorhanden
wäre, sondern weil das Kapital sich vorwiegend dem Boden
zuwendet und dessen Preis in die Höhe zu treiben sucht, daher
dann beim Bau der Häuser größtenteils versagt. Hierin liegt
eine Hanptursache, warum der Bau von Kleinwohnungen
hinter dein Bedürfnisse zurückbleibt. Zur Linderung der Woh-
nungsnot muß daher mit großen Mitteln nachgeholfen werden.
Die Erfahrimg hat überall gezeigt, daß die vennehrte H e r-
stellitng voir Kleinwohnungen nicht in Fluß
kommen kann, ohne eine leicht zugängliche
Geldquelle. Nur unter dieser Voraussetzung kann auch
die Wohnnngsaufsicht wirken und auf Beseitigung alter un-
brauchbarer Wohnungen drängen. Die Geldquelle soll nicht
bloß der gemeinnützigen und besonders genossenschaftlichen
Bautätigkeit, sondern auch den Privatunternehmern zur
Verfügung stehen. Die Geldausgabe soll sich stets vorsichtig