﻿182	Grundriß der Wohnungsfrage und Wohnungspolitik

Lessingstraße 11), verlangt, „daß der Boden, diese Grundlage
aller nationalen Existenz, unter ein Recht gestellt werde, das
seinen Gebrauch als Werk- und Wohnstätte befördert, das jeden
Mißbrauch mit ihm ausschließt, und das die Wertsteigerung,
die er ohne die Arbeit des einzelnen erhält, möglichst dein
Ganzen nutzbar gemacht wird". Die nächstenZiele der deutschen
Bodenreformer siild die Besteuerung der städtischen Böden
nach dern gemeinen Wert in abgestufter Weise, die Steuer
auf den unverdienten Wertzuwachs, Vermehrung des in
öffentlichen Händen befindlichen Bodens niit sozialpolitischer
Verwendung, Erziehung der Behörden zu sozialpolitischer
Auffassung, lauter Forderungen, die mit der Wohnungsreform
zusammenfallen.

29.	Die Gartenstadtbewegung

Die Erkenntnis, daß die Anhäufllng der Bevölkerung in
den Großstädten gesundheitlich, wirtschaftlich, sittlich, politisch
und militärisch verwerflich ist, während anderseits das flache
Land sich entvölkert, hat zum Gedanken der Gartenstadt
geführt. Ihren Ursprung hat sie in England, mitgewirkt haben
dabei auch die Verhältnisse dieses Landes, wo der nationale
Boden auf dem Grabe des ehemaligen Bauernstandes in die
Hände einer kleinen Zahl mächtiger Landlords geraten ist,
denen die ganze übrige Bevölkerung einen jährlich steigenden
Tribut in Form der Bodenrente gibt. Alle Mittel zur Abhilfe
waren bisher vergeblich, auch die großen Gesundungsarbeiten
in den übervölkerten Stadtbezirken (Slums). Dazu kam die
Erscheinung, daß die Fabriken von dern teuern Boden der
Stadt auf den billigern des Landes hinauswandern. Das
führte Ebenezer Howard zu deni Gedanken, eine systematische
Organisation dieser Dezentralisationsbewegung zu schaffen,
und zwar auf gemeinnütziger Grundlage, also mit Gemein-
eigentum des Bodens in neuen Städten, die den verlorenen