﻿32. Die Wrchnmigsgcsctzkeruiig in de« wichtigern Auslandsswatcn >49

Achtung der Wohnungen in einem Gebäude, wie über die
zulässige Anzahl der Bewohner.

Alle Gemeinden sind verpflichtet, die Vermieter der Wohnungen
zur Anmeldung ihrer Mieter anzuhalten und die Zahl der Bewohner
der Miethäuser anzugeben. Dazu sollen Gesundheitskommissionen
errichtet werden, welche die Aufgabe der deutschen Wohnungsaufsicht
haben: sie sollen die Mißstände im Wohnungswesen beseitigen, können
ungeeignete Wohnungen schließen und den Abbruch solcher Häuser
anordnen. Damit diese Maßregeln wirksam seien, ist auch für Herstellung
neuer Wohnungen gesorgt. Alle Gemeinden mit über 10 000 Einwohnern
sowie alle diejenigen, deren Einlvohnerzahl in den letzten 3 Jahren
um mehr als ein Fünftel gestiegen ist, müssen einen Erweiterungsplan
aufstellen, worin der für die irächste Zeit zur Anlage von Straßen,
Kanälen und Plätzen bestimmte Boden festgelegt wird. Dieser Plan
muß lvenigstens alle 10 Jahre revidiert werden. Sodann wird der
Gemeinde die Befugitis erteilt, das für die Errichtung von Arbeiter-
wohnungen nötige Gelände zu kaufen, lvofür ihr ein ausgedehntes
Enteignungsrecht zusteht, das sie nicht nur für sich selbst, sondern
auch im Interesse der vom Staat als gemeinnützig anerkannten Bau-
vereine ausüben kann, auf deren Antrag sogar ausüben muß.

Das Gesetz ermächtigt die Gemeinde ferner, Gelder zu bewilligen
zur Enteignung und zum Ankauf von Gelände, uni Wohnungen zu bauen,
die den Ersatz für unbrauchbare Wohnungen bilden sollen. Die Ge-
meinde kann auch den Eigentümern verbesserungsbedürftiger Wohnungen
verzinsliche Vorschüsse geben unter hypothekarischer Haftung des Ge-
bäudes und Bodens. Diese Vorschüsse müssen in spätestens 20 Jahren
getilgt sein. Auch können den von der Regierung als gemeinnützig
anerkannten Bauvereinen Vorschüsse, die in spätestens 50 Jahren zu
tilgen sind, gewährt werden. Ferner sollen die Gemeinden solchen
Vereinen Grundstücke entweder als Eigentum überlassen oder in Erb-
pacht geben. Man hofft auf diese Weise den Baugesellschaften Gelegenheit
zu geben, recht bald rege Tätigkeit auf dem Gebiete des gemeinnützigen
Wohnungsbaues zu entfalten. Auch die Regierung stellt ihre finanzielle
Unterstützung in Aussicht, indem sie die allerdings sehr vorsichtige Er-
klärung abgibt: „Wir können den Genreinden aus Staatsmitteln zu
obigen Zwecken Vorschüsse gewähren." Diese Vorschüsse müssen eben-
falls in 50 Jahren getilgt sein.

Italien hat ein Kleinwohnungsgesetz vom 31. Mai
1903 (!e§§e solle case populari), das die Sparkassen,