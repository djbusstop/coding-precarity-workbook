﻿18. Die Bautätigkeit mit Gewinilverzicht und die gemeinnützige Bautätigkeit 97

die im Deutschen Reiche jährlich für neue Wohnungsbauten
ausgegeben werden muß (S. S. 63).

Größere Arbeitgeber, Fabrikanten und einzelne Staaten
als Inhaber von Großbetrieben, Eisenbahnen, Bergwerken
usw. (Preußen, Bayern, Sachsen, Württemberg, Baden usw.)
bauen schon längst regelmäßig Wohnungen für ihre Arbeiter
und Angestellten. Das tun auch viele Gemeinden. Alle diese
Unternehmungen geschehen nnt Gewinnverzicht; auch Privat-
gesellschaften wirken auf diesen! Gebiete wenn auch nicht
ohne Gewinn, so doch meist gemeinnützig. Wenn größere
Arbeitgeber solche Wohnungen errichten, um sich einen
Staunn tüchtiger Arbeiter zu erhalten, soll die Ver-
quickung der Wohnung mit dem Lohnvertrag unterbleiben.

Häuser, die durch private oder öffeutliche Hilfe mit Ge-
wiunverzicht hergestellt sind, sollen niemals in das
volle freie Eigentum übergehen, denn das frei
gewordene Privatinteresse treibt den Besitzer • früher oder
später dazu, die inzwischen eingetretene Steigerung des
Bvdenwerts sich nutzbar zu machen. Aus diesem Grunde
haben auch die Versuche, die Arbeiter auf dem Boden des
reinen Privateigentums zu Hausbesitzern zu machen, die
Bodenspekulation nie dauernd zurückgehalten, sondern oft
zu einen! Rückfall in finanziell, gesundheitlich und sittlich
schlechte Wohnverhältnisse geführt, wie das Schicksal der
Arbeiterstadt (citd ouvriere) in Mülhausen und anderweitige
Erfahrungen gezeigt haben; die Gebäude wurden mit großen
Opfern hergestellt, den Arbeitern in Privateigentum über-
lassen und dadurch von: gemeinuützigen Interesse losgelöst; die
Häuser kamen dann in die Hände von Spekulanten, und das
kaum vertriebene Wohnungselend kehrte wieder. Beini Über-
gang solcher Häuser in Privateigentmn muß daher Vorsorge
getroffen werden, daß sie ihre Eigenschaft als Arbeiterhäuser
dauernd behalten, sonst sind die Leistungen aus öffentlichen
Mitteln (niedriger Zinsfuß und sonsüge Unterstützungen) nur

I a e g e r, Grundriß der Wohnungsfrage und Wohnungspolitik.

7