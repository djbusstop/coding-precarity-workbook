﻿— 12 —

mehr zusammenschmilzt, sondern vielmehr als ein periodisches Ab-
mähen der Kleinkapitale, die dann immer wieder rasch aufkommen, um
von neuem durch die Sense der Großindustrie abgemäht zu werden.
Von den beiden Tendenzen, die mit dem kapitalistische-n Mittelstand
Fangball spielen, siegt in letzter Linie — im Gegensatz zu der Ent-
wicklung der Arbeiterklasse — die h e r abdrücke n d e Tendenz.
Dies braucht sich aber durchaus nicht in der absoluten zahlenmäßigen
Abnahme der Mittelbetriebe zu äußern, sondern erstens in dem all-
mählich steigenden Kapitalminimum, das zum existenzfähigen Betriebe
in den alten Branchen nötig ist, zweitens in der immer kürzeren Zeit-
spanne, während der sich Kleinkapitale der Exploitation neuer Bran-
chen auf eigene Hand ' erfreuen. Darauf folgt für das indivi-
duelle Kleinkapital eine immer kürzere Lebensfrist und ein immer
rascherer Wechsel der Produktionsmethoden wie der Anlagearten, und
für die Klasse im ganzen ein immer rascherer sozialer Stoff-
wechsel.

Letzteres weiß Bernstein sehr gut, und er stellt es selbst fest. Was
er aber zu vergessen scheint, ist, daß damit das Gesetz selbst der Be-
wegung der kapitalistischen Mittelbetriebe gegeben ist. Sind Die
Kleinkapitale einmal die Vorkämpfer des technischen Fortschrittes,
und ist der technische Fortschritt der Lebenspulsschlag der kapitalisti-
schen Wirtschaft, so bilden offenbar die Kleinkapitale eine unzertrenn-
liche Begleiterscheinung der kapitalistischen Entwicklung, die erst mit ihr
zusammen verschwinden kann. Das stufenweise Verschwinden der
Mittelbetriebe — im Sinne der absoluten summarischen Statistik, um
die es sich bei Bernstein handelt — würde bedeuten, nicht wie Bern-
stein meint, den revolutionären Entwicklungsgang des Kapitalismus,
sondern gerade umgekehrt eine Stockung, Einschlummerung des letzte-
ren. „Die Profitrate, d. h. der verhältnismäßige Kapitalzuwachs ist
vor allem wichtig für alle neuen, sich selbständig gruppierenden Kapi-
talableger. Und sobald die Kapitalbildung ausschließlich in die Hände
einiger wenigen fertigen Großkapitale siele, . . . wäre überhaupt das
belebende Feuer der Produktion erloschen. S ie würde ei n -
schlummer n." *

3.	Einführung des Sozialismus Lurch soziale Deformen.

Bernstein verwirft die „Zusammenbruchstheorie" als den histori-
schen Weg zur Verwirklichung der sozialistischen Gesellschaft. Welches
ist der Weg, der vom Standpunkte der „Anpasfungstheorie des Kapi-
talismus" dazu führt? Bernstein hat diese Frage nur andeutungs-
weise beantwortet, den Versuch, sie ausführlicher im Sinne Bern-
steins darzustellen, hat K o n r a d Schmidt gemacht.** Nach ihm
wird „der gewerkschaftliche Kampf und der politische Kampf um soziale

* K. Marx, Das Kapital. 3. Band, I, S. 241.

^Vorwärts vom 20. Februar 1898, Literarische Rundschau. Wir
glauben um so mehr die Ausführungen Konrad Schmidts im Zusammenhang
mit denjenigen von Bernstein betrachten zu dürfen, als Bernstein mit keinem
Wort die Kommentierung seiner Ansichten im Vorwärts ablehnte.