﻿— 32 —

Bernstein ein wenig Anarchie zu, so sorgt der Mechanismus der
Warenwirtschaft von selbst für die Steigerung dieser Anarchie ins
Ungeheuere — bis zum Zusammenbruch. Hofft Bernstein aber —
unter gleichzeitiger Beibehaltung der Warenproduktion — auch das
bißchen Anarchie allmählich in Ordnung und Harmonie aufzulösen, so
verfällt er wiederum in einen der fundamentalsten Fehler der bürger-
lichen Vulgär Ökonomie, indem er die Austauschweise von der Pro-
duktionsweise als unabhängig betrachtet.

Es ist nicht hier die entsprechende Gelegenheit, die überraschende
Verwirrung in bezug auf die elementarsten Grundsätze der politischen
Oekonomie, die Bernstein in seinem Buche an den Tag gelegt hat, in
ihrem Ganzen zu zeigen. Aber ein Punkt, auf den uns die Grundsätze
der kapitalistischen Anarchie führt, soll kurz beleuchtet werden.

Bernstein erklärt, das Marxische Arbeitswertgesetz sei
eine bloße Abstraktion, was nach ihm in der politischen Oekonomie
offenbar ein Schimpfwort ist. Ist aber der Arbeitswert bloß eine
Abstraktion, „ein Gedankenbild" (S. 44), dann hat jeder rechtschaffene
Bürger, der beim Militär gedient und seine Steuern entrichtet hat,
das gleiche Recht wie Karl Marx, sich beliebigen Unsinn zu einem
sokhem „Gedankenbild" , d. h. zum Wertgesetz, zurecht zu machen. „Von
Hause aus ist es Marx ebenso erlaubt, von den Eigenschaften der
Waren soweit abzusehen, daß sie schließlich nur noch Verkörperungen
von Mengen einfacher menschlicher Arbeit bleiben, wie es der Vöhm-
Jevonsschen Schule freisteht, von allen Eigenschaften der Waren außer
ihrer Nützlichkeit zu abstrahieren" (S. 42).

Also die Marxische gesellschaftliche Arbeit und die Mengersche
abstrafte Nützlichkeit, das ist ihm gehüpft wie gesprungen: alles bloß
Abstraktion. Bernstein hat somit ganz vergessen, baß die Marxische
Abstraktion nicht eine Erfindung, sondern eine Entdeckung ist, daß sie
nicht in Marxens Kopfe, sondern in der Warenwirtschaft existiert, nicht
ein reales gesellschaftliches Dasein führt, ein so reales Dasein, daß sie
geschnitten und gehämmert, gewogen und geprägt wird. Die von Marx
entdeckte abstrakt-menschliche Arbeit ist nämlich in ihrer entfalteten
Form nichts anderes, als — dasEeld. Und dies ist gerade eine der
genialsten ökonomischen Entdeckungen von Marx, während für die
ganze bürgerliche Oekonomie, vom ersten Merkantilisten bis auf den
letzten Klassiker, das mystische Wesen des Geldes ein Buch mit
sieben Siegeln geblieben ist.

Hingegen ist die Böhm-Jevonsche abstrakte Nützlichkeit tatsächlich
bloß ein Gedankenbild oder vielmehr ein Bild der Gedankenlosigkeit,
ein Privatblödsinn, für den weder die kapitalistische, noch eine andere
menschliche Gesellschaft, sondern einzig und allein die bürgerliche Vul-
gärökonomie verantwortlich gemacht werden kann. Mit diesem „Ge-
dankenbild" im Kopfe können Bernstein und Böhm und Jevons mit
der ganzen subjektiven Gemeinde vor dem Mysterium des Geldes noch
zwanzig Jahre stehen, ohne daß sie zu einer anderen Lösung kommen,
was jeder Schuster ohne sie schon wußte: daß das Geld auch eine „nütz-
liche" Sache ist.