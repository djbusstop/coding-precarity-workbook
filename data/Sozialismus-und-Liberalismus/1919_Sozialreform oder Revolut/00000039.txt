﻿34 -

sellschaft, die kapitalistischen Klassengegensätze leugnet, daß für ihn der
Sozialismus selbst zu einem „Ueberrest des Utopismus" geworden ist.
Der „Monismus," d. h. die Einheitlichkeit Bernsteins ist die Einheit-
lichkeit der verewigten kapitalistischen Ordnung, die Einheitlichkeit
des Sozialisten, der sein Endziel fallen gelassen hat, um dafür in der
einen und unwandelbaren bürgerlichen Gesellschaft das Ende der
menschlichen Entwicklung zu sehen.

Sieht aber Bernstein in der ökonomischen! Struktur des Kapitalis-
mus selbst den Zwiespalt, bie Entwicklung zum Sozialismus nicht, so
muß er, um das sozialistische Programm wenigstens in der Form zu
retten, zu einer außerhalb der ökonomischen Entwicklung liegenden, zu
einer idealistischen Konstruktion Zuflucht nehmen und den Sozialismus
selbst aus einer bestimmten geschichtlichen Phase der gesellschaftlichen
Entwicklung in ein abstraktes „Prinzip" verwandeln.

Das Bernsteinfche „Prinzip der Eenossenschaftlichkeit", mit dem
die kapitalistische Wirtschaft ausgeschmückt werden soll, dieser dünnste
„Abkläricht" des sozialistischen Endzieles, erscheint angesichts dessen
nicht als ein Zugeständnis seiner bürgerlichen Theorie an die sozia-
listische Zukunft der Gesellschaft, sondern an die sozialistische Vergangen-
heit — Bernsteins.

2.	Gewerkschaften, Genossenschaften und politische Demokratie.

Wir haben gesehen, der Bernsteinsche Sozialismus läuft auf den
Plan hinaus, die Arbeiter an dem gesellschaftlichen Reichtum teil-
nehmen zu lassen, die Armen in Reiche zu verwandeln. Wie soll das
bewerkstelligt werden? In seinen Aufsätzen „Probleme des Sozialis-
mus" in der Reuen Zeit liest Bernstein nur kaum verständliche Finger-
zeige durchblicken, in seinem Buche gibt er über diese Frage vollen
Aufschluß: sein Sozialismus soll auf zwei Wegen, durch Gewerkschaften
oder, wie Bernstein es nennt, wirtschaftliche Demokratie, und durch
Genossenschaften, verwirklicht werden. Durch die ersteren will er dem
industriellen, durch die letzteren dem kaufmännischen Profit an den
Kragen.

Was die Genossenschaften, und zwar vor allem die Produktiv-
genossenschaften betrifft, so stellen sie ihrem inneren Wesen nach in-
inmitten der kapitalistischen Wirtschaft ein Zwitterding dar:
eine im kleinen sozialisierte Produktion bei kapitalistischem Austausche.
In der kapitalistischen Wirtschaft beherrscht aber der Austausch die
Produktion und macht, angesichts der Konkurrenz, rücksichtslose Aus-
beutung, d. h. völlige Beherrschung des Produktionsprozesses durch die
Interessen des Kapitals zur Existenzbedingung der Unternehmung.
Praktisch äußert sich das in der Notwendigkeit, die Arbeit möglichst
intensiv zu machen, sie zu verkürzen oder zu verlängern, je nach der
Marktlage, die Arbeitskraft je nach den Anforderungen des Absatz-
marktes heranzuziehen oder sie abzustoßen und aufs Pflaster zu setzen,
mit einem Worte, all die bekannten Methoden zu praktizieren, die
eine kapitalistische Unternehmung konkurrenzfähig machen. Zn der