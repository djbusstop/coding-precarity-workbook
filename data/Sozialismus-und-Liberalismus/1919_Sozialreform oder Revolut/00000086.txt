﻿— 81 —

Lande genau den Rang bezeichnet, den dasselbe in
der Hierarchie des Weltmarktes einnimmt“ (1. c.
S. 161).

Das heißt: Marx verspottet und verhöhnt schon im Jahre 1847 bei
Owenisten und Fourieristen genau dieselbe Auffassung, die Herr Som-
bart heute als Marxens und der Marxisten Auffassung hinstellt. Und.
die „richtige," „realistische," „historische" Methode erweist sich diesmal
als eine Methode, die die reale Geschichte erst verfälscht, um sie dann
auf Grund der eigenen Fälschung zu verurteilen.

Sie bringt aber noch mehr fertig: sie führt auch noch logische
Gründe für diese „korrigierte" Geschichte an.

Die Sozialdemokratie, erklärt Herr Sombart, war nicht bloß tat-
sächlich seit jeher der Gewerkschaftsbewegung im Grunde ihres Herzens
abhold, sondern sie konnte und kann es gar nicht anders, so daß das
Gedeihen der Gewerkschaften sich direkt an dem Grade ihrer Befreiung
von der hemmenden „Vormundschaft" der Sozialdemokratie messen läßt.

Die Frage der sogenannten Neutralität der Gewerkschaften wird
auch in unseren eigenen Reihen seit einiger Zeit erörtert. Zum Aus-
gangspunkt bei den Befürwortern der Neutralität dienen aber bei uns
nur taktische Rücksichten, nämlich der Wunsch, Arbeiter, die ver-
schiedenen politischen Parteien angehören, zum einheitlichen wirtschaft-
lichen Kampfe zu sammeln. Diese gewerkschaftliche „Sammlungspolitik"
ist ein ganz analoger Gedanke zu der gleichfalls in den letzten Jahren
der Sozialdemokratie von verschiedenen Seiten empfohlenen
Politik der Sammlung. Wie hier durch Verschleierung der Endziele
die Werbekraft der Sozialdemokratie und damit ihre unmittelbaren
politischen Erfolge vergrößert werden sollten, so sollen dort durch
Abstreifung des sozialistischen Charakters die Werbekraft und die öko-
nomische Macht der Gewerkschaft potenziert werden.

Freilich formulieren die deutschen Gewerkschaften auch jetzt ihren
sozialistischen Charakter nicht offiziell und machen ihn den Mitgliedern
nicht zur Pflicht, aber ihre ganze Gegenwartsarbeit bewegt sich in sozia-
listischer Richtung.

Die Sozialdemokratie vertritt aber auch gegenüber einzelnen
Gruppen des kämpfenden Proletariats die Interessen der gesamten
Klasse und gegenüber einzelnen Augenblicksinteressen die
Interessen der ganzen Bewegung. Ersteres äußert sich sowohl in dem
politischen Kampfe der Sozialdemokratie um gesetzliche, d. h.
das ganze Proletariat in jedem Lande umfassende Maßnahmen zur
Hebung seiner Lage, wie in dem internationalen Charakter
ihrer Politik, letzteres in der Uebereinstimmung der Bestrebungen
der Sozialdemokratie mit dem Gange der gesellschaftlichen Entwick-
lung, das sozialistische Endziel als Richtschnur nehmend.

Die Gewerkschaften vertreten von vornherein — dies der Unter-
schied von der politischen Partei des Proletariats — nur unmittel-
bare Eegenwartsintereffen der Arbeiter. Aber in ihrer Entwicklung
werden sie durch diese selben Interessen dahin gedrängt, erstens ihren
Errungenschaften in jedem Lande durch gesetzliche Normen immer

6