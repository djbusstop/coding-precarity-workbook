﻿— 59 —

vom Standpunkte der vorherigen bürgerlichen Entwicklung, d. h.
von der historisch bedingten fortschrittlichen Seite und die
völlige Nichtbeachtung der weiteren, bevorstehenden Entwicklung und
im Zusammenhang damit auch der reaktionären Seite der be-
handelten Erscheinungen, Der Schutzzoll ist für Schippe! immer noch
das, was er zu Zeiten des seligen Friedrich List vor mehr als einem
halben Jahrhundert war: der große Fortschritt über die mittelalter-
lichfeudale wirtschaftliche Zersplitterung Deutschlands hinaus.. Das
heute bereits der allgemeine Freihandel denselben notwendigen
Schritt weiter über die innere wirtschaftliche Abgrenzung der eins
gewordenen Weltwirtschaft ist, daß daher die nationalen Zollschranken
heute eine Reaktion sind, das existiert für Schippe! nicht.

Dasselbe in der Frage des Militarismus.. Er betrachtet ihn
immer noch vom Standpunkte des großen Fortschritts, den die stehende
Armee auf Grund der allgemeinen Wehrpflicht gegen die ehemaligen
Werbeheere und feudalen Armeen bedeutete. Dabei bleibt aber die
Entwicklung für Schippe! stehen: über das stehende Heer nur mit
weiterer Verwirklichung der allgemeinen Wehrpflicht geht ihm die
Geschichte nicht hinaus.

Dasselbe in der Frage des Militarismus.. Er betrachtet ihn immer
noch vom Standpunkte des großen Fortschritts, den die stehende Armee
auf Grund der allgemeinen Wehrpflicht gegen die ehemaligen Werbe-
heere und feudalen Armeen bedeutete. Dabei bleibt aber die Ent-
wicklung für Schippe! stehen: über das stehende Heer nur mit weiterer
Verwirklichung der allgemeinen Wehrpflicht geht ihm die Geschichte
nicht hinaus.

Was bedeuten aber diese charakteristischen Standpunkte, die
Schippe! ebenso in der Zoll- wie in der Militärfrage einnimmt? Sie
bedeuten erstens eine Politk von Fall zu Fall anstatt einer
prinzipiellen Stellungnahme und zweitens im Zusammenhang damit
eine Bekämpfung des Systems selbst. Was ist diese Politik aber
anderes, als unser guter Bekannter aus der letzten Zeit oer Partei-
geschichte — der Opportunismus?

Es ist wieder die „praktische Politik", die in der Jsegrim-
Schippelschen offenen Absage an das Milizpostulat, einen der grund-
legenden Punkte unseres ganzen politischen Programms, ihre
Triumphe feiert, und darin liegt vom parteipolitischen Standpunkte
die eigentliche Bedeutung des Schippelschen Auftretens. Nur im Zu-
sammenhang mit dieser ganzen Strömung und vom Gesichtspunkte der
allgemeinen Grundlagen und Folgen des Opportunismus läßt sich
die neueste sozialdemokratische Kundgebung zugunsten des Militaris-
mus richtig beurteilen und abschätzen.

II.

Das wesentliche Merkmal der opportunistischen Politik ist,
daß sie folgerichtig stets dazu führt, die Endziele der Bewegung, die
Interessen der Befreiung der Arbeiterklasse ihren nächsten, und zwar
eingebildeten Interessen zum'Opfer zu bringen. Daß dieses Postulat