﻿— 22 —

sie also niedergerissen werden kann, ist einzig der Hammerschlag der
Revolution, d. h. die Eroberung der politischen Macht durch das
Proletariat.

5. Praktische Konsequenzen und allgemeiner Charakter
des Revisionismus.

Wir haben im ersten Kapitel darzutun gesucht, daß die Bern-
steinsche Theorie das sozialistische Programm vom materiellen Boden
aufhebt und auf.ein« idealistische Basis versetzt. Dies bezieht sich auf
die theoretische Begründung. Wie sieht nun aber die Theorie — in
die Praxis übersetzt —aus? Zunächst und formell unterscheidet sie sich
gar nicht von der bisher üblichen Praxis des sozialdemokratischen
Kampfes. Gewerkschaften, der Kampf um die Sozialreform und um
die Demokratisierung der politischen Einrichtungen, das ist das näm-
liche, was auch sonst den formellen Inhalt der sozialdemokratischen
Parteitätigkeit ausmacht. Der Unterschied liegt also nicht in dem
Was, wohl aber in dem Wie. Wie die Dinge jetzt liegen, werden
der gewerkschaftliche und der parlamentarische Kampf als Mittel auf-
gefaßt, das Proletariat allmählich zur Besitzergreifung der politischen
Gewalt zu führen und zu erziehen. Nach der revisionistischen Auf-
fassung sollen sie, angesichts der Unmöglichke-it und Zwecklosigkeit dieser
Besitzergreifung, bloß im Hinblick auf unmittelbare Resultate, d. h. die
Hebung der materiellen Lage der Arbeiter, und auf die stufenweise
Einschränkung ber kapitalistischen Ausbeutung und die Erweiterung
der gesellschaftlichen Kontorlle geführt werden. Wenn wir von dem
Zwecke der unmittelbaren Hebung der Lage der Arbeiter absehen, da
er beiden Auffassungen, der bisher in der Partei üblichen, wie der revi-
sionistischen, gemeinsam ist, so liegt der ganze Unterschied -kurz gefaßt
darin: nach der landläufigen Auffassung besteht die sozialistische Be-
deutung des gewerkschaftlichen und politischen Kampfes darin, daß er
das Proletariat, d. h. den subjektiven Faktor der sozialistischen
Umwälzung zu deren Durchführung vorbereitet. Rach Bernstein besteht
sie darin, daß der gewerkschaftliche und politische Kamps die kapitali-
stische Ausbeutung selbst stufenweise einschränken, der kapitalistischsn
Gesellschaft immer mehr ihren kapitalistischen Charakter nehmen und
den sozialistischen aufprägen, mit einem Worte, die sozialistische Um-
wälzung in objektivem Sinne herbeiführen soll. Sieht man die
Sache näher an, so- sind beide Auffassungen sogar gerade -entgegengesetzt.
In der parteiüblichen Auffassung gelangt das Proletariat durch den
gewerkschaftlichen und politischen Kampf zu der Ueberzeugung von der
Unmöglichkeit, seine Lage- von Grund aus durch diesen Kampf umzu-
gestalten und von der Unvermeidlichkeit -einer endgültigen Besitz-
ergreifung der politischen Machtmittel. In der Bernsteinschen Auf-
fassung geht man von der Unmöglichkeit der politischen Machtergreifung
als Voraussetzun aus, um durch bloßen gewerkschaftlichen und politi-
schen Kampf die- sozialistische Ordnung einzuführen.

Der sozialistische Charakter des gewerkschaftlichen und- parlamen-
tarischen Kampfes liegt also bei der Bernsteinschen Auffassung in dem