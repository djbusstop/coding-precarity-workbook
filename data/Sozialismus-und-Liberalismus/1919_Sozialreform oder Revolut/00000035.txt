﻿— 30 -

Wirtschaft zur gesellschaftlichen, daß sie vergesellschaftet
wurde.

Wie erklärt es sich aber angesichts dessen, daß Bernstein das
Phänomen der Aktiengesellschaften gerade umgekehrt als eine Zer-
splitterung und nicht als eine Zusammenfassung des Kapitals auffaßt,
daß er dort Verbreitung des Kapitaleigentums, wo Marx „Aufhebung
des Kapitaleig-entums" steht? Durch einen sehr einfachen vulgär-
ökonomischen Schnitzer: weil Bernstein unter Kapitalist nicht eine
Kategorie der Produktion, sondern des Eigentumsrechts, nicht eine
wirtschaftliche, sondern eine steuerpolitische Einheit, unter Kapital
nicht ein Produktionsganzes, sondern schlechthin Gelbvermögen ver-
steht. Deshalb steht er in seinem englischen Nähgarntrust nicht die
Zusammenschweißung von 12 300 Personen zu Einem, sondern ganze
12 300 Kapitalisten, deshalb ist ihm auch sein Ingenieur Schulze, der
als Mitgift für seine Frau vom Rentier Müller „eine größere Anzahl
Aktien" bekommen hat (S. bl), auch ein Kapitalist, deshalb wimmelt
ihm die ganze Welt von „Kapitalist« n“.*)

Aber hier wie sonst ist der vulgärökonomische Schnitzer bei Bern-
stein bloß der theoretische Boden für eine Vulgarisierung des Sozia-
lismus. Indem Bernstein den Begriff Kapitalist aus den Produk-
tionsverhältnissen in die Eigentumsverhältnisse überträgt und, „statt
von Unternehmern von Moschen spricht" (S. 53), überträgt er auch die
Frage des Sozialismus aus dem Gebiete der Produktion auf das Gebiet
der Vermögensverhältnisse, aus dem Verhältnis von Kapital und
A r b e i t in das Verhältnis von reich und arm.

Damit sind wir von Marx und Engels glücklich auf den Verfasser
des Evangeliums, des armen Sünders zurückgebracht, nur mit dem
Unterschiede, daß Weitling mit richtigem proletarischem Instinkt eben
in diesem Gegensatz von arm und reich in primitiver Form die Klassen-
gegensätze erkannte und zum Hebel der sozialistischen Bewegung
machen wollte, während Bernstein, umgekehrt, in der Verwandlung der
Armen in Reiche, d. h. in der Verwischung des Klassengegensatzes, also
im kleinbürgerlichen Verfahren die Aussichten des Sozialismus sieht.

Freilich beschränkt sich Bernstein nicht auf die Einkommens-
statistik. Er gibt uns auch Betriebsstatistik, und zwar aus mehreren

* Notabene! Bernstein sieht offenbar in der großen Verbreitung kleiner
Aktien einen Beweis, daß der gesellschaftliche Reichtum seinen Aktiensegen
bereits über ganz kleine Leute zu ergießen beginnt. In der Tat, wer würde
denn sonst als Kleinbürger oder gar Arbeiter 'z. B. Aktien für die Bagatelle
von 1 Dfd. oder 20 Mk. kaufenI Leider beruht diese Annahme auf einem ein-
fachen Rechenfehler: man operiert mit dem Nennwerte der Aktien, statt
mit ihrem Marktwerte, was aber zweierlei ist. Ein Beispiel! Auf dem
Minenmarkt werden u. a. die südafrikanischen Randmines gehandelt; die
Aktien sind, wie die meisten der Minenwerte, 1 Psd. = 20 Mark-Papiere. Ihr
Preis war aber schon 1899 43 Psd. (s. den Kurszettel Ende März), d" h.
nicht 20, sondern 860 Mk.! Und so steht es im Durchschnitt überall. Die
„kleinen" Aktien sind also, obwohl sie so demokratisch klingen, tatsächlich meistens
gutbürgerliche, und keineswegs kleinbürgerliche oder gar proletarische „An-
Weisungen aus den gesellschaftlichen Reichtum," denn zum Nennwert werden sie
von dem kleinsten Teil der Aktionäre erworben.