﻿— 19 -

schutzes nichk, wohl aber die Unternehmer zum Schutze ihres Absatzes.
Das heißt, die Zölle dienen heute nicht mehr als Schutzmittel einer
aufstrebenden kapitalistischen Produktion gegen eine reifere, sondern
als Kampfmittel einer nationalen Kapitalistengruppe gegen eine
andere. Die Zölle sind ferner nicht mehr nötig als Schutzmittel der
Industrie, um einen inländischen Markt zu bilden und zu erobern,
wohl aber als unentbehrliches Mittel zur Kartellierung der Industrie,
d. h. zum Kampfe der kapitalistischen Produzenten mit der konsu-
mierenden Gesellschaft. Endlich, was am grellsten den spezifischen Cha-
rakter der heutigen Zollpolitik markiert, ist die Tatsache, daß jetzt über-
all die ausschlaggebende Rolle darin überhaupt nicht die Industrie,
sondern die Landwirtschaft spielt, d. h. daß die Zollpolitik eigentlich
zu einem Mittel geworden ist, feudale Interessen in kapi-
talistischeFormzu gießen und zum Ausdruck zu
bringen.

Die gleiche Wandlung ist mit dem Militarismus vorgegangen.
Wenn wir die Geschichte betrachten, nicht wie sie hätte sein können
oder sollen, sondern wie sie tatsächlich war, so müssen wir konstatieren,
daß der Krieg den unentbehrlichen Faktor der kapitalistischen Ent-
wicklung bildete. Die Vereinigten Staaten Nordamerikas und Deutsch-
land, Italien und die Balkanstaaten, Rußland und Polen, sie alle
verdanken die Bedingungen oder den Anstoß zur kapitalistischen Ent-
wicklung den Kriegen, gleichviel ob dem Sieg oder der Niederlage.
So lange, als es Länder gab, deren innere Zersplitterung, oder deren
naturalwirtschaftliche Abgeschlossenheit zu überwinden war, spielte
auch der Militarismus eine revolutionäre Rolle im kapitalistischen
Sinne. Heute liegen auch hier die Dinge anders. Wenn die Welt-
politik zum Theater drohender Konflikte geworden ist, so handelt es
sich nicht sowohl um die Erschließung neuer Länder für den Kapitalis-
mus, als um fertige europäische Gegensätze, die sich nach den
anderen Weltteilen verpflanzt haben und dort zum Durchbruch kommen
Was heute gegeneinander mit der Waffe in der Hand auftritt, gleich-
viel ob in Europa oder in anderen Weltteilen, find nicht einerseits
kapitalistische, andererseits natural-wirtschaftliche Länder, sondern
Staaten, die gerade durch die Gleichartigkeit ihrer hohen kapitalistischen
Entwicklung zum Konflikt getrieben werden. Für diese Entwicklung
selbst kann freilich unter diesen Umständen der Konflikt, wenn er zum
Durchbruch kommt, nur von fataler Bedeutung sein, indem er die
tiefste Erschütterung und Umwälzung des wirtschaftlichen Lebens in
allen kapitalistischen Ländern herbeiführen wird. Anders sieht aber
die Sache aus vom Standpunkte der K a p i t a l i st e n k l a s s e. Für
sie ist heute der Militarismus in dreifacher Beziehung unentbehrlich
geworden: erstens als Kampfmittel für konkurrierende „nationale"
Interessen gegen andere nationale Gruppen, zweitens als wichtigste
Anlegeart ebenso für das finanzielle wie für das industrielle Kapital,
und drittens als Werkzeug der Klassenherrschaft im Jnlande gegenüber
dem arbeitenden Volke — alles Interessen, die mit dem Fortschritt
der kapitalistischen Produktionsweise an sich nichts gemein haben.