﻿— 89 —

Schwätzern" die jetzt noch in der Presse, in Volksversammlungen
und Vereinen vielfach den Ton angeben, von jenen faulen Kerls,
die zu nichts gut sind, als ein paar auswendig gelernte, unver-
standene Phrasen aus der Parteiliteratur papageienmäßig
nachzuplappern oder stiermäßig in die Menge hinein-
zubrüllen, die zu jeder Arbeit außer der „Parteiagitation" ver-
dorben sind — von diesen Zerrbildern politischer Agi-
tatoren" will Herr Professor Sombart die deutsche Arbeiterklasse
befreien . . . (S. 91).

In dem „Sozialismus und soziale Bewegung" (S. 99) wehklagte
Herr Sombart bitter über den Verfall der guten Sitten und feinen
Manieren in unserem Klaffenkampf. „Schon ganz äußerlich die Tonart
->er Meinungsäußerung, wie abstoßend, wie verletzend, wie roh ist sie
tui allzu oft! Und mutz das fein?"

Diese Worte waren uns, als wir sie lasen, direkt aus dem Herzen
gesprochen. Lange schon schmerzte uns die Verrohung des Tones und
der Sprache in unserer Partei, und wir waren herzlich froh, daß endlich
jemand eine ernste Mahnung an die Partei gerichtet hat. Profeffor
Sombart zeigt selbst am besten, wie man seine Gegner widerlegen und
doch in feinster, salonmäßiger Weise behandeln kann. Wir wollen des-
halb, um ja nicht etwa selbst in abstoßende, verletzende und rohe Ton-
art zu verfallen, der Sicherheit halber uns genau die Sprache des Herrn
Professors aneignen.

Also, Sie wollen die Arbeiterklaffe von den „Zerrbildern poli-
tischer Agitatoren" befreien, Herr außerordentlicher Profeffor? Ja,
wen meinen Sie eigentlich damit? Sind etwa jene zahllosen Agita-
toren der Sozialdemokratie, die unter dem Sozialistengesetz ein Jahr-
tausend hinter den Gefängnismauern verbracht haben, die faulen
Kerls? Sie nationalökonomischer Belletrist, der Sie sich Ihr Lebtag
auf dem sicheren Boden der akademischen und bürgerlichen Salons be-
wegt haben!

Sind etwa unsere bescheidenen Redakteure der kleinen Provinz-
blätter und unsere Versammlungsredner, die sich mit unsäglicher Mühe
aus ihrem proletarischen Dasein emporgearbeitet, sich jedes Körnchen
Bildung in zähem Ringen angeeignet und sich durch eigene Arbeit zu
Aposteln der großen -Befreiungslehre gemacht haben, sind das jene
„seichten, hirnlosen Schwätzer," von denen Sie sprechen? Sie seichter
Schwätzer, dem man von der Jugend an die abgestandenen Plattitüden
und Selbstverständlichkeiten der deutschen Nationalökonomie eintrich-
terte, um aus Ihnen, wenn Gott und die Weltpolitik hilft, einen
ordentlichen Professor zu machen!

Sind unsere zahllosen und namenlosen Agitatoren, die, jeden
Augenblick ihre und ihrer Familien Existenz aufs Spiel setzend, sich
nicht die saure Arbeit verleiden lassen, immer und immer wieder in
Versammlungen und Vereinen die Masse aufzurütteln und ihr hundert-,
tausendmal das alte und ewig neue Wort des sozialistischen Evan-
geliums zu wiederholen, jene „Zerrbilder politischer Agitatoren," die
„papageienmäßig" Phrasen aus der Parteiliteratur „nachplappern"