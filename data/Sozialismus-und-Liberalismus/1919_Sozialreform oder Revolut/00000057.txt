﻿— 52 —-

setzen der praktischen Tätigkeit ebenso in bezug auf die angestrebten
Ziel e, wie auf die anzuwendenden Kampsm itte l, wie endlich selbst
auf die Kampfw e i s e sehr feste Schranken. Daher zeigt sich bei den-
jenigen, die nur den praktischen Erfolgen nachjagen wollen, das natür-
liche Bestreben, sich die Hände frei zu machen, d. h. unsere Praxis
von der „Theorie" zu trennen, von ihr unabhängig zu machen.

Aber dieselbe Theorie schlug sie bei jedem praktischen Versuch auf
den Kopf: der Staatssozialismus, Agrarsozialismus, die Kompen-
sationspolitik, die Milizfrage sind ebensoviel Niederlagen für den
Opportunismus. Es ist klar, daß diese Strömung, wollte sie sich gegen
unsere Grundsätze behaupten, folgerichtig dazu kommen mußte, sich an
die Theorie selbst, an die Grundsätze heranzuwagen, statt sie zu ig-
norieren, sie zu erschüttern suchen und eine eigene Theorie zurechtzu-
machen. Ein dahingehender Versuch war eben die Vernsteinsche Theorie,
und daher sahen wir auf, dem Parteitag in Stuttgart alle opportu-
nistischen Elemente sich sofort um das Vernsteinsche Banner gruppieren.
Sind einerseits die opportunistischen Strömungen in der Praxis eine
ganz natürliche, aus den Bedingungen unseres Kampfes und seinem
Wachstum erklärliche Erscheinung, so ist andererseits die Vernsteinsche
Theorie ein nicht minder selbstverständlicher Versuch, diese Strömungen
in einem allgemeinen theoretischen Ausdruck zusammenzufassen, ihre
eigenen theoretischen Voraussetzungen herauszufinden und mit dem
wissenschaftlichen Sozialismus abzurechnen. Die Bernsteinfche Theorie
wär daher von vornherein die theoretische Feuerprobe für -den Oppor-
tunismus, feine erste wissenschaftliche Legitimation.

Wie ist nun diese Probe ausgefallen? Wir haben es gesehen. Der
Opportunismus ist nicht imstande, eine einigermaßen die Kritik aus-
haltende positive Theorie aufzustellen. Alles, was er kann, ist: die
Marxische Lehre zuerst in verschiedenen einzelnen Grundsätzen zu be-
kämpfen und zuletzt, da diese Lehre ein fest zusammengefügtes Gebäude
darstellt, das ganze System vom obersten Stockwerke bis zum Funda-
ment zu zerstören. Damit ist erwiesen, daß die opportunistische Praxis
in ihrem Wesen, in ihren Grundlagen mit dem Marxischen System
unvereinbar ist.

Aber damit ist ferner noch erwiesen, daß der Opportunismus auch
mit dem Sozialismus überhaupt unvereinbar ist, daß seine innere
Tendenz dahin geht, die Arbeiterbewegung in bürgerliche Bahnen hin-
überzudrängen, d. h. den proletarischen 'Klassenkamps völlig lahmzu-
legen. Freilich ist proletarischer Klassenkampf mit dem Marxischen
System — geschichtlich ausgenommen— nicht identisch. Auch vor Marx
und unabhängig von ihm hat es eine Arbeiterbewegung und ver-
schiedene sozialistische Systeme gegeben, die jedes in seiner Weise ein
den Zeitverhältnissen entsprechender theoretischer Ausdruck der Eman-
zipationsbestrebungen der Arbeiterklasse waren. Die Begründung des
Sozialismus durch moralische Gerechtigkeitsbegriffe, der Kampf gegen
die Verteilungsweise, statt gegen die Produktionsweise, die Auffassung
der Klassengegensätze als Gegensatz von arm und reich, die Bestrebung,
die „Eenossenschaftlichkeit" auf die kapitalistische Wirtschaft aufzu-