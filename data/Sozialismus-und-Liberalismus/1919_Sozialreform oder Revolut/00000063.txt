﻿- 58

E°i ist auffallend, wie der Schippelsche Standpunkt in der Frage
des Militarismus in allen Hauptpunkten mit seinem Standpunkt in
der aderen wichtigsten Frage des politischen Kampfes — in der Zoll-
politik übereinstimmt.

Vor allem haben wir hier wie dort das entschiedene Ablehnen,
diese oder jene Stellungnahme zur Frage mit Demokratie oder Reak-
lion zu verbinden. Die Behauptung, als wäre Freihandel mit Fort-
schritt und Schutzzöllnerei mit Reaktion identisch, hieß es im Schippsl-
fchen Referate auf dem Stuttgarter Parteitag — sei falsch. Lange
und breite geschichtliche Erinnerungen sollten beweisen, daß man sehr
gut ein Freihändler und zugleich Reaktionär, dagegen Schutzzöllner
und glühender Freund der Demokratie sein könne. Fast mit den
gleichen Worten hören wir jetzt: „Es gibt Milizschwärmer, die das
heutige Erwerbsleben mit endlos ewigen Störungen und Unter-
brechungen heimsuchen und Unteroffiziersgeist selbst bis in die letzten
Schulklassen unserer Knaben und Knäblein hinein verpflanzen wollen
— viel schlimmer wie der heutige Militarismus.
Es gibt Gegner der Miliz, die jeder und vollends einer derartigen
Ueberwucherung der militärischen Eingriffe und Anforderungen tod-
feind find."*

Aus der Tatsache, daß die bürgerlichen Politiker in diesen,
wie in allen Fragen keine prinzipielle Stellung einnehmen, daß
sie Gelegenheitspolitik treiben, folgert der Sozialdemokrat. Schippe!
auch für sich das Recht und die Notwendigkeit, den inneren reaktio-
nären Kern des Schutzzolles und des Militarismus, resp. die fort-
schrittliche Bedeutung des Freihandels und der Miliz, zu verkennen,
das heißt gleichfalls k e i n e p r i n z i p i e l l e Stellung zu den
b e id e n Fr a g e n e in zu n eh m e n.

Zweitens sehen wir hier wie dort gleichzeitig mit der Opposition
gegen einzelne Uebel der Schutzzollpoltik, resp. des Militarismus, die
entschiedene Weigerung, beide Erscheinungen als solche im Ganzen zu
bekämpfen. In Stuttgart hörten wir im Schippelschen Referat von
der Notwendigkeit, gegen einzelne übermäßige Zölle zu kämpfen, zu-
gleich aber die Warnung: sich ja nicht „festzulegen", sich nicht „die
Hände zu binden", d. h. den Schutzzoll nicht immer und überall zu be-
kämpfen. Jetzt hören wir, daß Schippe! wohl die „parlamentarische
und agitatorische Bekämpfung konkreter militärischer
Forderungen"** gelten läßt, daß er aber davor warnt, „rein
äußerliche Zufälligkeiten und sehr nebensächliche,
freilich auch sehr auffällige Rückwirkungen (des Militarismus)
auf die übrigen gesellschaftlichen Gebiete für sein Wesen und seinen
Kern zu nehmen."***

Endlich, drittens, dies die Grundlage der beiden obigen Stand-
punkte, hier wie dort die ausschließliche Abschätzung der Erscheinung

* Neue Jett, Nr 19, S. 589-581

** Soj. Morr, Novemberheft, 6. 495.

*** Neue Jett, Nr. 19, S 581.