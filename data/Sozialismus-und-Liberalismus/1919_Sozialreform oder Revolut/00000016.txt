﻿— II —

MUS und ihre tiefliegenden allgemeinen Ursachen aufdeckt,
mögen sich diese Krisen alle 10, alle 5, oder abwechselnd alle 20 Jahre
und alle 8 Jahre wiederholen. Was aber die Bernsteinscho Theorie am
schlagendsten ihrer Unzulänglichkeit überführt, ist die Tatsache, dah- die
jüngste Krise im Jahre 1907/08 am heftigsten gerade in jenem Lande
wütete, wo die famosen kapitalistischen „Anpassungsmittel": der Kre-
dit, der Nachrichtendienst und die Trusts am meisten ausgebildet sind.

Ueberhaupt setzt die Annahme, die kapitalistische Produktion
könnte sich dem Austausch „anpassen", eins von beiden voraus: ent-
weder, dah der Weltmarkt unumschränkt und ins Unendliche wächst,
oder umgekehrt, dah die Produktivkräfte in ihrem Wachstum gehemmt
werden, damit sie nicht über die Marktschranken hinauscilen. Ersteres
ist eine physische Unmöglichkeit, letzterem steht die Tatsache entgegen,
daß auf Schritt und Tritt technische Umwälzungen auf allen Gebieten
dcr Produktion vor sich gehen und jeden Tag neue Produktivkräfte
wachrufen.

Noch eine Erscheinung widerspricht nach Bernstein dem bezeichne-
ten Gang der kapitalistischen Dnge: die „schier unerschütterliche Pha-
lanx" der Mittelbetriebe, auf die er uns hinweist. Er sieht darin
ein Zeichen, datz die grohindustrielle Entwicklung nicht so revolutionie-
rend und konzentrierend wirkt, wie es nach der „Zusammenbruchs-
theorie" hätte erwartet werden müssen. Allein hietze es in der Tat
die Entwicklung der Großindustrie ganz falsch auffassen, wenn man
erwarten würde, es sollten dabei die Mittelbetriebe stufenweise von
der Oberfläche v e r s ch w i n d e n.

In dem allgemeinen Gange der kapitalistischen Entwicklung
spielen gerade nach der Annahme von Marx die Kleinkapitale die Rolle
der Pioniere -der technischen Revolution, und zwar in doppelter Hin-
sicht, ebenso -in bezug aus neue Produktionsmethoden in alten und- be-
lustigten, fest eingewurzelten Branchen, wie auch in bezug auf, Schaf-
fung neuer, von großen Kapitalen noch gar nicht exploitierter Produk-
tionszweige. Vollkommen falsch 'ist die Auffassung, als ginge die Ge-
schichte des kapitalistischen Mittelbetriebes in gerader Linie abwärts
zum stufenweisen Untergang. Dcr tatsächliche Verlauf der Entwicklung
ist vielmehr auch hier rein dialektisch und bewegt sich beständig zwischen
Gegensätzen. Der kapitalistische Mittelstand befindet sich ganz wie die
Arbeiterklasse unter dem Einfluh zweier entgegengesetzter Tendenzen,,
einer ihn erhebenden und einer ihn herabdrückenden Tendenz. Die
herabdrückende Tc-ndenz ist gegebenenfalls das beständige Steigen der
Stufenleiter der Produktion, welche den Umfang- der Mittelkap-itale
periodisch überholt und sie so immer wieder aus dem Wettkampf Her-
ausschleudert. Die hebende Tendenz ist die periodische Entwertung
des vorhandenen Kapitals, die die Stufenleiter der Produktion —
dem Werte des notwendigen Kapitalmin-imums nach — immer
wieder für eine Zeitlang senkt, sowie das Eindringen der kapitalisti-
schen Produktion in neue Sphären. Der Kampf des- Mittelbetriebes
mit dem Großkapital ist nicht als -eine regelmäßige S-chlacyr zu denken,
wo der Trupp des schwächeren Teiles direkt und quantitativ immer