﻿the scale towards

— 83 -

neu. Aber wenn die Gewerkschaften, wie bis jetzt in Deutschland
Fall, auf demselben Boden der allgemeinen sozialen Entwicklung
jen, deren Schlußergebnisse die Sozialdemokratie in ihrem Endziel
nnuliert, so kann sogar, vorausgesetzt. das; die Darstellung des
rrn Professors der Wahrheit entspricht, zwischen „Mittel" und
weck", zwischen Gewerkschaften und Sozialdemokratie keinerlei
gensatz bestehen. Im Gegenteil, am eifrigsten müßte dann die
zialdemokratie, selbst wenn ihr die unmittelbare Hebung der
beiterschaft an sich nicht teuer, sondern bloß Mittel zur Be-
leunigung der sozialistischen Umwälzung wäre, am Ausbau der
werkschaften arbeiten. Und sie müßte dazu „die innere Ruhe"
cade so gut finden, wie sie zur Teilnahme an dem bürgerlichen
irlamentarismus, zum Ausbau der Arbeiterschutzgesetzgebung, kurz
der ganzen Gegenwartsarbeit seit dreißig Jahren „die innere
rhe" hat. Zwischen der Sozialdemokratie, wie sie ist, und den Ge-
akschaften, wie sie sind, kann also unmöglich ein Gegensatz, sondern
iß vielmehr der innigste Zusammenhang bestehen.

Der Gegensatz ist nur in eine m Falle denkbar.. Wenn die Ge-
irkschaften etwa auf einem anderen Boden, wie gegenwärtig in
mtschland, ständen, wenn sie sich z. B. wie die englischen alten Trade
rions statt auf den Boden des Klassenkampfes auf den der Harmonie
r Interessen in der heutigen Gesellschaft stellen und an eine Möglich-
st der ausreichenden Wahrung der Arbeiterinteressen innerhalb
eser Gesellschaft glauben würden, mit einem Worte, wenn sie sich auf
n Boden der „richtigen," „realistischen," „historischen" Methode des
irrn Sombart stellen würden, wie wir sie oben kennen gelernt haben,
a n n würde allerdings zwischen der Sozialdemokratie und diesen
ewerkschaften ein schroffer Gegensatz bestehen. Denn den Glauben
t die Harmonie der Interessen in der kapitalistischen Gesellschaft, an
e Möglichkeit einer unbeschränkten Steigerung des Anteils der
rbeit cm dem Nationaleinkommen, alle die Illusionen der Vulgär-
onomie zerstört die Sozialdemokratie allerdings unbarmherzig. Das
ebeneinanderbestehen solcher Gewerkschaften mit der Sozialdemo-
atie könnte auch nur zu der Alternative führen: entweder daß
e Arbeiter, der Sozialdemokratie folgend, den Harmonie-
id Elückseligkeitsduseleien der „realistischen" Methode Adieu sagen,
>er aber daß sie, um den Illusionen dieser Methode treu zu
eiben, der Sozialdemokratie den Rücken kehren.

Und das ist des Pudels Kern, darin liegt die politische Be-
e u t u n g des Sombartschen Prophetentums in Gswerkschaftsfragen.
ie „realistische," Historische" Methode fängt damit an, den Eewerk-
iaften unumschränkte Perspektiven wirtschaftlichen Aufstiegs zu er-
fnen, um ihnen zum Schluffe die Sozialdemokratie als
as wahre Hindernis dieses Aufstiegs zu denunzieren.

Aber verwahrt sich Herr Sombart nicht mehrmals gegen die An-
rhme, als Hetze er die Gewerkschaften gegen die Sozialdemokratie?
chreibt er nicht ausdrücklich, sein Ideal eines Gewerkschaftlers könne
lebenbei auch überzeugter Sozialist, ehrlicher Sozialdemokrat fein"