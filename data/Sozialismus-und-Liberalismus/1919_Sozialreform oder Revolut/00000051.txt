﻿— 46 -

notwendig, wie auch einzig möglich macht. Wenn Engels die
Taktik der heutigen Arbeiterbewegung in seinem Vorwort zu den
Klassenkämpfen in Frankreich revidierte! und den Barrikaden den ge-
setzlichen Kampf entgegenstellte, so behandelte er — was aus jeder
Zeile des Vorwortes klar ist — nicht die Frage der end-
gültigen Eroberung der politischen Macht, sondern die des heutigen
alltäglichen Kampfes, nicht das Verhalten des Proletariats gegenüber
dem kapitalistischen Staate im Moment der Ergreifung! der Staats-
gewalt, sondern sein Verhalten im Rahmen des kapitalistischen
Staates. Mit einem Wort, Engels gab die Richtschnur dem be-
herrschten Proletariat und nicht dem siegreichen.

Umgekehrt bezieht sich der bekannte Ausspruch von Marx über die
Bodenfrage in England, aus den sich Bernstein gleichfalls beruft: „man
käme wahrscheinlich am billigsten fort, wenn man die Landlords aus-
kaufte", nicht auf das Verhalten des Proletariats vor feinem Siege,
sondern nach dem Siege. Denn von „Auskaufen" der herrschenden
Klassen kann offenbar nur dann die Rede fein, wenn die Arbeiterklasse
am Ruder ist. Was Marx somit hier als möglich in Erwägung zog,
ist die friedliche Ausübung der proletarischen Dik-
ta tu r und nicht die Ersetzung der Diktatur durch kapitalistische Sozial-
relformen.

Die Notwendigkeit selbst der Ergreifung der politischen Macht
durch das Proletariat war ebenso für Marx wie für Engels zu allen
Zeiten nutzer Zweifel. Und es blieb Bernstein vorbehalten, den
Hühnerstall des bürgerlichen Parlamentarismus für das berufene Or-
gan zu halten, wodurch die gewaltigste weltgeschichtliche Umwälzung:
die Ueberführung der Gesellschaft aus den 'kapitalistischen in
sozialistische Formen vollzogen werden soll.

Aber Bernstein hat ja seine Theorie bloß mit d'er Befürchtung' und
der Warnung angefangen, daß das Proletariat nicht zu früh' -ans
Ruder komme! In diesem Falle müßte es nämlich nach Bernstein die
bürgerlichen Zustände ganz so lassen, wie sie sind und selbst eine
furchtbare Niederlage erleiden. Was aus dieser Befürchtung vor allem
ersichtlich, ist, daß die Bernsteinsche Theorie für das Proletariat, falls
es durch die Verhältnisse ans Ruder gebracht wäre, nur Eine „prak-
tische" Anweisung hat: sich schlafen zu legen. Damit richtet sie sich aber
ohne weiteres selbst, als ein-e Auffassung, die das Proletariat in den
wichtigsten Fällen des Kampfes zur Untätigkeit, -also zum passiven
Verrate an der eigenen Sache verurteilt.

Tatsächlich wäre unser ganzes Programm ein elender Wisch
Papier, wenn es uns nicht für alle Eventualitäten und in allen
Momenten des Kampfes zu dienen, und zwar durch feine Ausübung
und nicht durch -seine Nichtausübung zu dien-en imstande wäre. Ist
unser Programm einmal die Formulierung der geschichtlichen Ent-
wicklung der Gesellschaft vom Kapitalismus zum Sozialismus, dann
muß es offenbar auch alle Uebergangsphäsen dieser Entwicklung for-
mulieren, in sich in den Erundzügen enthalten, also auch das ent-
sprechende Verhalten im Sinne der Annäherung zum Sozialismus in