﻿Wieso? Durch stufenweise Aufhebung im Weichbilde d-er Stadt aller
jener Splitterrechte: der Fronden, Kurmeden, des Eewandrechts, Best-
haupts, Kopfzinfes, Heiratszwanges, Erbteilungsrechts usw. usw., deren
Gesamtheit die Leibeigenschaft ausmachte.

Desgleichen arbeitete sich „der Kleinbürger zum Bourgeois unter
dem Joch des feudalistischen Absolutismus empor (a. a. O. <5. 17). Auf
welchem Wege? Durchs teilweise formelle Aufhebung oder tatsächliche
Lockerung der Zunftfesseln, durch allmähliche Ilmbildung der Verwal-
tung, des Finanz- und Wehrwesens in dem allernotwendigsten Um-
fange.

Will man also abstrakt, anstatt geschichtlich die Frage behandlen,
so läßt sich bei den früheren Klassenverhältnissen ein rein gesetzlich-
reformlerischer Uebergang von der feudalen zur bürgerlichen Gesell-
schaft wenigstens denken. Was sehen wir aber in der Tat? Daß auch
dort die gesetzlichen Reformen nicht dazu dienten, die Ergreifung der
politischen Macht durch das Bürgertum überflüssig zu machen, sondern
umgekehrt, sie vorzubereiten und herbeizuführen. Eine förmliche
politisch-soziale Umwälzung war unentbehrlich, ebenso zur Aufhebung
der Leibeigenschaft, wie zur Abschaffung des Feudalismus.

Ganz anders noch liegen aber die Dinge jetzt. Der Proletarier
wird durch kein Gesetz gezwungen, sich in das Joch des Kapitals zu
spannen, sondern durch die Not, durch den Mangel an Produktions-
mitteln. Kein Gesetz in der Welt kann ihm aber im Rahmen der
bürgerlichen Gesellschaft diese Mittel zudekretieren, weil er ihrer
nicht durch Gesetz, sondern durch ökonomische Entwicklung beraubt
wurde.

Ferner beruht die Ausbeutung innerhalb des Lohnverhält-
nisses gleichfalls nicht auf Gesetzen, denn die Höhe der Löhne wird
nicht auf gesetzlichem Wege, sondern durch ökonomische Faktoren be-
stimmt. Und die Tatsache selbst der Ausbeutung beruht nicht auf einer
gesetzlichen Bestimmung, sondern auf der ein wirtschaftlichen Tatsache,
daß die Arbeitskraft als Ware auftritt, die unter anderem die ange-
nehme Eigenart besitzt, Wert, und zwar mehr Wert zu produzieren,
als sie selbst in den Lebensmitteln des Arbeiters vertilgt. Mit einem
Worte, alle Erundverhältnisse der kapitalistischen Klassenherrschaft
lassen sich durch gesetzliche Reformen auf bürgerlicher Basis deshalb
nicht umgestalten, weil sie weder durch bürgerliche Gesetze herbeige-
führt, noch die Gestalt von solchen Gesetzen erhalten haben. Bernstein
weiß das nicht, wenn er eine sozialistische „Reform" plant, aber was
er nicht weiß, das sagt er, indem er auf S. 10 seines Buches schreibt, daß
„das ökonomische Motiv heute frei auftritt, wo es früher durch Herr-
schaftsverhältnisse und Ideologen aller Art verkleidet war."

Aber es kommt noch ein zweites hinzu. Es ist die andere Besonder-
heit der kapitalistischen Ordnung, daß in ihr alle Elemente der künf-
tigen Gesellschaft in ihrer Entwicklung vorerst eine Form annehmen,
in der sie sich dem Sozialismus nicht nähern, sondern von ihm ent-
fernen. In der Produktion wird immer mehr der gesellschaftliche