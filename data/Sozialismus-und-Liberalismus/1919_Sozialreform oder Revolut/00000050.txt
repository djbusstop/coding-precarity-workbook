﻿— 45 —

Charakter zum Ausdruck gebracht. Aber in welcher Form? Von Groß-
betrieb, Aktiengesellschaft, Kartell, wo die kapitalistischen Gegensätze,
die Ausbeutung, die Unterjochung der Arbeitskraft aufs höchste ge-
steigert werden.

Im Wehrwesen führt die Entwicklung die Verbreitung der allge-
meinen Wehrpflicht, die Verkürzung der Dienstzeit, also materiell die
Annäherung an das Volksheer herbei. Aber dies in der Form von
modernem Militarismus, wo die Beherrschung des Volkes durch den
Militärstaat, der Klassencharakter des Staates zum grellsten Ausdruck
kommt.

In den politischen Verhältnissen führt die Entwicklung der Demo-
fratte, insofern sie günstigen Boden hat, zur Beteiligung aller Volks-
schichten am politischen Leben, also gewissermaßen zum „Volksstaat."
Aber dies in der Form des bürgerlichen Parlamentarismus,' wo die
Klassengegensätze, die Klassenherrschaft nicht aufgehoben sind, sondern
vielmehr entfaltet und bloßgelegt werden. Weil sich die ganze kapita-
listische Entwicklung somit in Widersprüchen bewegt, so mutz, um den
Kern der sozalistischen Gesellschaft aus der ihm widersprechenden kapi-
talistischen Hülle herauszuschälen, auch aus diesem Grunde zur Erobe-
rung der politischen Macht durch das Proletariat und zur gänzlichen
Aufhebung des kapitalistischen Systems gegriffen werden.

Bernstein zieht freilich andere Schlüsse daraus: führte die Ent-
wicklung der Demokratie zur Verschärfung und nicht zur Abschwächung
der kapitalistischen Widersprüche, dann „müßte die Sozialdemokratie",
antwortet er uns, „wenn sie sich nicht selbst die Arbeit erschweren will,
Sozialreformen und die Erweiterung der demokratischen Einrichtungen
nach Wglichkeit zu vereiteln streben" (S. 71). Dies allerdings, wenn
die Sozialdemokratie noch kleinbürgerlicher Art an dem müßigen Ge-
schäft des Auswählens aller guten Seiten und des Wegwerfens schlech-
ter Seiten der Geschichte Geschmack -fände. Nur müßte sie dann folge-
richtig auch den ganzen Kapitalismus überhaupt „zu vereiteln streben",
denn er ist doch unstreitbar der Hauptbösewicht, der ihr alle Hinder-
nisse aus dem Wege zumSozialismus stellt. Tatsächlich gibt der Kapi-
talismus neben und zugleich mit Hindernissen auch die, einzigen
Möglichkeiten, das sozialistische Programm zu verwirklichen.
Dasselbe gilt aber vollkommen auch in bezug auf die Demokratie.

Ist die Demokratie für die Bourgeoisie teils überflüssig, teils
hinderlich geworden, so ist sie für die Arbeiterklasse dafür notwendig
und unentbehrlich. Sie ist erstens notwendig, weil sie politische Formen
(Selbstverwaltung, Wahlrecht u. dergl.) schafft, die als Ansätze und
Stützpunkte für das Proletariat bei seiner Umgestaltung der bürger-
lichen Gesellschaft dienen werden. Sie ist aber zweitens unentbehrlich,
weil nur in ihr, in dem Kampfe um die Demokratie, in der Ausübung
ihrer Rechte dasProletariat zum Bewußtsein seiner Klasseninteressen
und seiner geschichtlichen Aufgaben kommen kann.

Mit einem Worte, die Demokratie ist unentbehrlich, nicht weil
sie die Eroberung der politischen Macht durch das Proletariat über-
flüssig, sondern umgekehrt, weil sie diese Machtergreifung ebenso