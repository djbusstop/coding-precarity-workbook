﻿- 41 —

heutigen Gesellschaft widerspricht, wie die sozialistische Arbeiterbewe-
gung ein direktes Produkt dieser Tendenz ist.

Aber er beweist damit noch ein weiteres. Zudem er den Verzicht
auf das sozialistische Endziel seitens der Arbeiterklasse zur Voraus-
setzung und Bedingung des Wiederauflebens der bürgerlichen Demo-
kratie macht, zeigt er selbst, wie wenig, umgekehrt die bürgerliche
Demokratie eine notwendige Voraussetzung und Bedingung der sozia-
listischen Bewegung und des sozialistischen Sieges sein kann. Hier
schließt sich das Bernsteinsche Räsonnement zu einem fehlerhaften Kreis,
wobei die letzte Schlußfolgerung feine erste Voraussetzung „frißt."

Der Ausweg aus diesem Kreise ist ein sehr einfacher: aus der
Tatsache, daß der bürgerliche Liberalismus vor Schreck vor der auf-
strebenden Arbeiterbewegung und ihren Endzielen seine Seele aus-
gehaucht hat, folgt nur, daß die sozialistische Arbeiterbewegung eben
heute die ei nzige Stütze der Demokratie ist und sein kann, und daß
nicht die Schicksale der sozialistischen Bewegung an die bürgerliche
Demokratie, sondern umgekehrt die Schicksale der demokratischen Ent-
wicklung an die sozialistische Bewegung gebunden sind. Daß die Demo-
kratie nicht in dem Maße lebensfähig wird, als die Arbeiterklasse
ihren Emanzipationskampf aufgibt, sondern umgekehrt, in dem Maße,
als sozialistische Bewegung stark genug wird, gegen die reaktionären
Folgen der Weltpolitik und der bürgerlichen Fahnenflucht anzukämpfen.
Daß wer die Stärkung der Demokratie wünscht, auch Stärkung und
nicht Schwächung der sozialistischen Bewegung wünschen muß, und daß
mit dem Aufgeben der sozialistischen Bestrebungen ebenso die Arbeiter-
bewegung wie die Demokratie aufgegeben wird.

3.	Die Eroberung der politischen Macht.

Die Schicksale der Demokratie sind, wie! wir gesehen, an die Schick-
sale der Arbeiterbewegung gebunden. Aber macht denn die Entwicklung
der Demokratielauch im besten Falle eine proletarische Revolution im
Sinne der Ergreifung der Staatsgewalt, der Eroberung der politischen
Macht überflüssig oder unmöglich?

Bernstein entscheidet diese Frage auf dem Wege einer gründlichen
Abwägung der guten und schlechten Seiten der gesetzlichen Reform und
der Revolution, und zwar mit einer Behaglichkeit, die an das Abwägen
von Zimt und Pfeffer in einem Konsumverein erinnert. In dem gesetz-
lichen Gang der Entwicklung sieht er die Wirkung des Intellekts, in dem
revolutionären die des Gefühls, in der Reformarbeit eine langsame,
in der Revolution eine rasche Methode des geschichtlichen Fortschritts,
in der Gesetzgebung eine planmäßige, in dem Umsturz eine elemen-
tarische Gewalt. (S. 183.)

Es ist nun eine alte Geschichte, daß der kleinbürgerliche Reformer
in allen Dingen der Welt eine „gute!" und eine „schlechte" Seite sieht
und daß er von allen Blumenbeeten nascht. Eine ebenso alte Geschichte
ist es aber, daß der wirkliche Gang der Dinge sich um kleinbürgerliche
Kombinationen sehr wenig küuimert und das sorgfältigst zusammen-