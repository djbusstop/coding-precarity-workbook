﻿9

für sich zu finden, zieht jede private Kapitalportion vor, auf eigene
Faust das Glück zu probieren. Die Organisationen müssen dann wie
Seifenblasen platzen und wieder einer freien Konkurrenz, in poten-
zierter Form, Platz machen. *)

Im ganzen erscheinen also auch die Kartelle, ebenso wie der Kre-
dit, als bestimmte Entwicklungsphasen, die in letzter Linie die Anarchie
der kapitalistischen Welt nur noch vergrößern und alle ihre inneren
Widersprüche zum Ausdruck und zur Reife bringen. Sie verschärfen
den Widerspruch zwischen der Produktionsweise and der Austausch-
weise, indem sie den Kampf zwischen den Produzenten und den Konsu-
menten auf die Spitze treiben, wie wir dies besonders in den Ver-
einigten Staaten Amerikas erleben. Sie verschärfen ferner den Wider-
spruch zwischen der Produktions- und der Aneignungsweise, indem sie
der Arbeiterschaft die Uebermacht des organisierten Kapitals in brutal-
ster Form entgegenstellen und so den Gegensatz zwischen Kapital und
Arbeit aufs äußerste steigern.

Sie verschärfen endlich den Widerspruch zwischen dem internatio-
nalen Charakter der kapitalistischen Weltwirtschaft und dem nationalen
Charakter des kapitalistischen Staates, indem sie zur Begleiterscheinung
einen allgemeinen Zollkrieg haben und so die Gegensätze zwischen den
einzelnen kapitalistischen Staate aus die Spitze treiben. Dazu kommt
die direkte, höchst revolutionäre Wirkung der Kartelle auf die Kon-
zentration der Produkte, technische Vervollkommnung usw.

So erscheinen die Kartelle und Trusts in ihrer endgültigen Wir-
kung auf die kapitalistische Wirtschaft nicht nur als kein „Anpassungs-
mittel", das ihre Widersprüche verwischt, sondern geradezu als eines
der Mittel, die sie selbst zur Vergrößerung der eigenen Anarchie, zur
Austragung der in ihr enthaltenen Widersprüche, zur Beschleunigung
des eigenen Unterganges geschaffen hat.

Allein, wenn das Kreditwesen, die Kartelle und dergleichen die
Anarchie der kapitalistischen Wirtschaft nicht beseitigen, wie kommt es,
daß wir zwei Jahrzehnte lang — feit 1873 — keine allgemeine Han-

* In einer Fußnote zum 3. Band des Kapital schreibt Fr. Engels 1894:
„Seit obiges geschrieben wurde (1865), hat sich die Konkurrenz auf dem
Weltmarkt bedeutend gesteigert durch die rapide 'Entwicklung der Industrie in
allen Kulturländern, namentlich in Amerika und Deutschland. Die Tatsache,
daß die rasch und riesig anschwellenden modernen Produktivkräfte den Gesehen
des kapitalistischen Warenaustausches, innerhalb deren sie sich bewegen sollen,
täglich mehr über den Kopf wachsen, diese Tatsache drängt sich heute auch
dem Bewußtsein der Kapitalisten selbst mehr und mehr auf. Dies zeigt sich
namentlich in zwei Symptomen. Erstens in der neuen allgemeinen Schuhzoll-
Manie, die sich von der alten Schutzzöllnerei besonders dadurch unterscheidet,
daß sie gerade die exportfähigen Artikel am meisten schützt. Zweitens in
den Kartellen (Trusts) der Fabrikanten ganzer großer Produktionssphären zur
Regulierung/ der Produktion und damit der Preise und Prostte. Es ist selbst-
redend, daß diese Experimente nur bei relativ günstigem ökonomischem Wetter
durchführbar find. Der erste Sturm muß sie über den Hausen werfen und
beweisen, daß, wenn auch die Produktion einer Regulierung bedarf, es sicher
nicht die Kapitalistenklaffe ist, die dazu berufen ist. Inzwischen haben diese
Kartelle nur den Zweck, dafür zu sorgen, daß die Kleinen noch rascher von den
Großen verspeist werden als bisher."