﻿- 61 -

winnreichste und unentbehrlichste Anlageart schafft. Es ist zwar klar,
daß dieselben Mittel, die, durch Besteuerung in die Hände der Re-
gierung gelangt, zur Erhaltung des Militarismus dienen, wenn sie in
der Hand der Bevölkerung geblieben wären, eine gewachsene Nach-
frage nach Lebensmitteln darstellten, oder, vom Staate in größerem
Maßstabe zu Kulturzwecken angewandt, gleichfalls eine entsprechende
Nachfrage nach gesellschaftlicher Arbeit schaffen würden. Es ist zwar
klar, daß auf diese Weise für die -Gesellschaft im ganzen der Militaris-
mus durchaus keine „Entlastung" ist. Allein anders gestaltet sich die
Frage vom Standpunkte des kapitalistischen Profits, vom Unternehmer-
standpunkte. Für den Kapitalisten ist es gar nicht gleich, ob er eine
bestimmte Nachfrage nach Erzeugnissen auf seiten der zersplitterten
Privatkäufer oder auf seiten des Staates findet. Die Nachfrage des
Staates zeichnet sich durch eine Sicherheit, Mafsenhaftigkeit und gün-
stige, meistens monopolartige Gestaltung der Preise aus, die den
Staat zum vorteilhaftesten Abnehmer und die Lieferungen für ihn
zum glänzendsten Geschäft für das Kapital machen.

Was aber besonders bei militärischen Lieferungen als höchst wich-
tiger Vorteil zum Beispiel vor staatlichen Ausgaben für Kulturzwecke
(Schulen, Wege usw.) hinzukommt, sind die unaufhörlichen technischen
Umwälzungen und das unhaufhörliche Wachstum der Ausgaben, so daß
der Militarismus eine unerschöpfliche, ja immer ergiebigere Quelle
der kapitalistischen Gewinne darstellt und- das Kapital zu einer sozialen
Macht erhebt, wie- sie dem Arbeiter zum Beispiel in den Kruppschen
und Stummschen Unternehmungen entgegentritt. Der Militarismus,/:
der für die Gesellschaft im ganzen eine ökonomisch völlig absurde Ver-
geudung ungeheuerer Produktivkräfte darstellt, der für die Arbeiter- .
klaffe eine Herabsetzung ihrer wirtschaftlichen Lebenshaltung zum,
Zwecke ihrer sozialen Versklavung bedeutet, bildet für die Kapita-
l i st en klaffe ökonomisch die glänzendste, unersetzliche Anlagcart,
wie sozial und politisch die beste Stütze ihrer Klassenherrschaft. Wenn
daher Schippet denselben Militarismus kurzerhand für eine notwendige '
ökonomische „Entlastung" erklärt, so verwechselt er offenbar nicht nur
den Standpunkt der g e se l l s ch a f t l i ch e n I n t e r e s s e n mit dem
der K a p i t a l s i n ter e ss e n und stellt sich somit — wie wir ein-
gangs gesagt haben — auf bürgerlichen Standpunkt, sondern er geht
auch, indem er annimmt, jeder ökonomische Vorteil des Unternehmer-
tums fei notwendig auch ein Vorteil für die Arbeiterklasse, von dem
Grundsätze der I n t e r e s s e n h a r m o n i e zwischen Kapital
und Arbeit aus.

Es ist dies wiederum derselbe Standunkt, den wir bei Schippet
schon einmal kennen gelernt haben — in der Zollfrage. Auch hier
trat er, da er den Arbeiter als Produzenten vor dem verderb-
lichen Wettbewerbe der ausländischen Industrie schützen wollte, im
Prinzip für den Schutzzoll ein. Hier ganz wie in der Militärvorlage
sieht er nun unmitelbare wirtschaftliche Interessen des Arbeiters und
übersieht seine weiteren sozialen Interessen, die mit dem allgemeinen
gesellschaftlichen Fortschritt zum Freihandel oder zur Abschaffung