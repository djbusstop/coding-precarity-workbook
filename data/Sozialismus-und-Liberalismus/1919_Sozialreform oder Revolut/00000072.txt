﻿sind sie's beide. Denn was haben sie durch die ihnen von Schippe! ange-
ratene Operation erreicht? Sie haben freilich beide einander redlich
zur restlosen Zerstörung einer bestimmten Menge Güter oerholfen.
Aber ach! nicht die Zerstörung der materiellen Güter, sondern die Rea-
lisierung des Mehrwertes in blankem Gold ist der Zweck des Unter-
nehmertums. Und in dieser Beziehung läuft das witzige Geschäft auf
dasselbe hinaus, wie wenn jeder der beiden Unternehmer seinen
eigenen überflüssigen Mehrwert selber restlos verschluckt, konsumiert
hätte. Das ist das Schippelfche Mittel zur Abschwächung der Krisen.
Die westfälischen Kohlenbarone leiden an Ueberproduktion von Kohle?
Die Tölpel! Sie sollen nur in ihren Palästen stärker Heizen lassen, und
der Kohlenmarkt ist „entlastet." Die Besitzer der Marmorgruben in
Carrara klagen über Stockung im Handel? Sie sollen Loch.für ihre
Pferde Ställe aus Marmor errichten lasten, und das „Krisenfieber"
im Marmorgeschäft ist sofort gedämpft. Und zieht eine drohende Wolke
von allgemeiner Handelskrise herauf, so r-uft Schippe! dem Kapitalis-
mus zu: „Mehr Austern, mehr Champagner, mehr Livreebediente, mehr
Balletteusen, und ihr seid gerettet!" Wir fürchten nur, die alten durch-
triebenen Kerle werden ihm antworten: „Herr, Ihr haltet uns für
dümmer, als wir sind!"

.Diese geistreiche ökonomische- Theorie führt aber noch zu inter-
essanten sozialen und politischen Schlußfolgerungen. Bildet nämlich
bloß die unproduktive Konsumtion, d. h. die Konsumtion des Staates
und der bürgerlichen Klassen, eine wirtschaftliche Entlastung und ein
Gegenmittel zur Abschwächung der Krisen, dann erscheint es im Inter-
esse der Gesellschaft und des ruhigen Verlaufes des Produktionszyklus,
daß die unproduktive Konsumtion möglichst erweitert, die produktive
möglichst eingeschränkt, der von den Kapitalisten und dem Staate an-
geeignete Teil des gesellschaftlichen Reichtums möglichst groß, der für
das arbeitende Volk verbleibende möglichst gering, die Profit« und die
Steuern möglichst hoch, die Löhne möglichst niedrig sind. Der Arbeiter
— eine wirtschaftliche „Last" für die Gesellschaft, und die Hündchen
der Herzogin d'Uzös ein wirtschaftlicher Rettungsanker — das sind die
Konsequenzen der Schippelschen „Entlastungs"theorie.

Wir haben gesagt, sie sei auch unter den vulgärökonomischen
Theorien die trivialste. Was ist der Gradmesser der vulgärökonomischen
Trivialität? Das Wesen der Vulgärökonomie besteht darin, daß sie
die Vorgänge der kapitalistischen Wirtschaft nicht in ihrem tiefliegenden
Zusammenhang und in ihrem inneren Wesen, sondern in der ober-
flächlichen Zersplitterung durch die Gesetze der Konkurrenz, nicht durch
das Fernrohr der Wissenschaft, sondern durch die Brille des Einzel-
interessenten der bürgerlichen Gesellschaft betrachtet. Aber je nach dem
Standpunkt dieses Interessenten verschiebt sich auch das Bild der Gesell-
schaft und es kann sich mehr oder weniger schief im Hirn des Oekonomen
abspiegeln. Je näher der Standpunkt zum eigentlichen Produktions-
prozeß, um so näher steht die Auffassung zur Wahrheit. Und je weiter
sich der Forscher zum Austauschmarkt, zum Gebiet der vollen Herrschaft