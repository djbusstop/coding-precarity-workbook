﻿werksch-aftlichen Erfolge nach feinem Rezept zu sichern, für jedes reiche
Mädchen einen paffenden Bräutigam zu finden.

Doch fürchten wir, wäre selbst das verlorene Mühe. Denn kaum
hat Herr Sombart die, um im Stile des Herrn Professors zu sprechen.
„Jnbeziehungsetzung" und das „Sichtatsächlichdecken" von Dingen, die
sich weder aufeinander beziehen noch sich tatsächlich decken, fertig ge-
bracht, als fein Flickwerk durch „hier nicht näher darzulegende Ver-
umständungen" wieder an allen Ecken und Enden aus dem Leime
geht.

Der Unternehmer soll die von der gewerkschaftlichen Aktion durch-
gesetzte Lohnerhöhung mit einem Preisaufschlag auf feine Waren
decken. Meint aber der Herr Professor, daß die Warenpreise so mir
nichts dir nichts erhöht werden können, so hat er alle „Wesenheiten"
der Preisbildung vergessen. Ist der Preisausschlag ein allge-
meiner, dann hebt er sich in feiner Wirkung selbst auf. Erhöht aber
ein einzelner Unternehmer seine Preise, dann wird ihn die Kon-
kurrenz seiner Herren Kollegen in sehr kurzer Zeit Mores lehren.
Allerdings können auch einzelne llnternehmergruppen willkürlich die
Preise steigern, dies aber nur, wenn sie dem Publikum gegenüber eine
Machtstellung einnehmen, d. h. Ringe, Kartelle usw. bilden. Rur ist
in diesen die Machtstellung des Kapitals den Arbeitern gegenüber
eine noch viel größere und sie macht unglücklicherweise in der Regel
die gewerkschaftlichen Erfolge just dort unmöglich, wo die einzige
Voraussetzung der Sombartschen „Abwälzungstheorie" vorhanden.
Herr Sombart vergißt überhaupt, wo er von den Machtverhältnisten der
Gewerkschaften spricht, die Existenz der Unternehmerverbände gänzlich
und erinnert sich ihrer nur, wo er sie als eine angenehme Ergänzung
zu dem beliebten Einigungsverfahren bei Arbeitskonflikten braucht...

Oder: der Unternehmer soll die Lohnzuschläge, falls Preiser-
höhungen nicht angängig, durch Erweiterung der Produktion wett-
machen. Aber das üben die Unternehmer schon von selbst, ohne Herrn
Sombarts Ratschläge abzuwarten, seit undenklichen Zeiten, wo es
nur irgend möglich. Und freilich sind solche Perioden der Produktions-
erweiterungen, d. h. des industriellen Aufschwunges, die günstige Ge-
legenheit für Lohnforderungen. Rur ist hier die Erweiterung der
Produktion nicht etwa ein beliebig anwendbares Mittel zur Wett-
machung der Lohnerhöhungen, sondern umgekehrt eine Voraus-
setzung, bei der Lohnerhöhungen möglich sind, und die ihrerseits
an die Marktlage, d. h. wiederum an die eigenen Verwertungsinter-
essen des Kapitals gebunden ist!

Oder die Unternehmer sollen nun gar die Lohnzuschläge durch
— technische Verbesserungen decken! Ei, Herr Professor, das glaube
Ihnen Ihre „glückliche Braut!" Die technischen Verbesserungen werden
von den Unternehmer seit jeher angewendet, um die im Lohnkamps
stehenden Arbeiter lahmzulegen und nicht, um sie zu befriedigen.
Lassen Sie sich doch nur die Geschichte der Lohnkämpfe der Hamburger
Kohlenjumper vom Ende der 80er Jahre erzählen, die von den Unter-