﻿Die „deutsche Wissenschaft" hinter
den Arbeitern.*

Ein neuer Prophet ist der deutschen Arbeiterklasse erstanden. Der
außerordentliche Professor Werner Sombart in Breslau verkündet dem
deutschen Proletariat das Evangelium der Hoffnung und des Glaubens.
Er lehrt euch, „meine Herren Arbeiter," ganz wie ein Lasalle, „aus
der Vogelschau" einer neuen „richtigen," „realistischen," „historischen"
Methode das Gebiet der Arbeiterfrage erforschen, er versichert euch, daß
„die deutsche Wissenschaft" hinter euch stehe, und bittet euch, „gemein-
sam frohen Mutes weiter zu streben und weiter zu kämpfen, gemein-
sam die Sache sozialen Fortschritts zu vertreten und vorauszuschreiten
auf der Bahn der Kultur: zum Nutz und Frommen unseres geliebten
deutschen Vaterlandes, zum Stolze der Menschheit!"

„Weiter" — „gemeinsam" — klingt eigentlich etwas seltsam, denn
bis jetzt hat die deutsche Arbeiterklasse mit Herrn Sombart ziemlich
wenig gemeinsam zu streben und zu kämpfen das Vergnügen gehabt.
Sie kämpfte freilich, als Herr Sombart noch in seinen Windeln trocken
gelegt wurde, zum Nutz und Frommen des deutschen Vaterlandes und
zum Stolze der Menschheit, und sie vertritt die Sache des sozialen Fort-
schritts durch Streben und Kämpfen seit bald einem halben Jahr-
hundert, während das Streben und Kämpfen des Herrn Sombart
etwas jüngeren Datums ist.

Das sind aber schließlich kleine Ungenauigkeiten, die im feurigen
Redestrom wohl unterlaufen können. Schenken wir dem neuen Pro-
pheten seine rhetorischen Blüten und hören wir mit Andacht, was die
richtige, realistische, historische Methode, was die „deutsche Wissen-
schaft" hinter uns über die Aufgaben der Gewerkschaften und Sozial-
demokratie zu sagen hat.

I.

„Zunächst: eine Steigerung der Anteile der Arbeiter am National-
einkommen ist nicht in irgendwelche naturgesetzliche Schranken, deren
Erweiterung außer allem Machtbereich der Arbeiter selbst stände, ein-
geschlossen" („Dennoch!" S. 70). Die Wissenschaft hat zwar früher die

* Aus der Neuen Zeit 1899/1900, Nr. 51 und 62. Den Anlaß zu dem
folgenden Aufsatz bildete die Schrift des Professors Werner Sombart: Dennoch!
Aus Theorie und Geschichte der gewerkschaftlichen Arbeiterbewegung, Jena
1900. Der damals in Breslau tätige Professor versuchte eine Zeitlang auch
in den Kreisen der organisierten sozialdemokratischen Arbeiterschaft durch
populäre nationalökonomische und andere Vorträge in seinem Sinne zu
wirken.