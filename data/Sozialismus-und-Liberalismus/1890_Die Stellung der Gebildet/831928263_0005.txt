Liebe Kommilitone n! 
Das Interesse, welches die Arbeiterfrage heute erregt, und das Fach, das ich 
an unserer Universität zu vertreten die Ehre habe, hat einige unter Ihnen zu der 
Aufforderung veranlaßt, ich möge 51t Ihnen von der Aufgabe sprechen, welche nach 
meinem Erachten den Gebildeten in der heutigen sozialen Bewegung zufällt. Ich 
hätte es für eine Pflichtwidrigkeit gehalten, mich dieser Aufgabe zu entziehen. Aber 
bevor ich auf mein Thema eingehe, möchte ich auf seine Formuliernng Ihr besonderes 
Augenmerk lenken. 
Ich habe versprochen, über die Stellung der Gebildeten zur sozialen Frage 
zu sprechen — der Gebildeten, nicht der Besitzenden. 
Warum nicht der Besitzenden? 
Der Grund ist, weil sonst notwendig die Interessen derjenigen in den Vorder 
grund gerückt worden wären, die sich heute in einem Jntereffenkampfe mit den 
Arbeitern befinden; die Stellung der Arbeitgeber insbesondere zur Arbeiterfrage wäre 
damit der Gegenstand meiner Betrachtung geworden. 
. ... Nun sind diese Interessen gewiß 'von der höchsten Wichtigkeit und bedürfen 
,orgsaltiger Berücksichtigung. Auch begrüße ich es als erste glückliche Wirkung der 
erwarteten Beseirigung des Sozialistengesetzes, daß die Arbeitgeber sich heute zu 
organisieren beginnen, um, gleich den Arbeitern vereint, ihre Interessen zu wahren 
perni mir mis einer cbenbürtißen Orßonifation betber Interessen sann cine ac 
deihliche Entwicklung hervorgehen. 
9(bcr so lui^io bie ^niereffen ber ^06%^ stub, so finb fie boeß nießt 
'denti,ch mit dem Interesse des Ganzen. Sie sind dies selbstverständlich ebensowenig, 
wie die einseitigen Interessen der Arbeiter. Die Fürsorge für das Interesse des 
(Bmiaen, iiWid) für bie mißi'nnß be8 Beßanbes nnb bic ÄßeiterentiDidimia unserer 
nationalen Kultur nnb unserer nationalen Machtstellung ist es aber gerade, wozu 
diejenigen Gebildeten, die nicht persönlich auf einer der beiden Seiten mit ihren 
Interessen beteiligt sind, besonders berufen erscheinen. Denn das Fehlen einer un- 
im.yi. tu.umui Ulşşi. uno oer ueoervua, oen Oie Mvlloeten, ,oweit sie 
es wirklich sind, über die Entwicklung ber menschlichen Dinge erlangt haben, zeigt 
fon,oßi, lucrin bie luaßrc Wcfnßr in ber Wißen Wen miftd 311 Men, numidi, 
lucrin fie md)t 311 jileen i|t. ^^efer Itcbcrbiid seißt nämiid), baß biefe (Maßr (ctnc¿ 
'»'memimer 9^1^^11,^011 nnb beni %orbrmißeii neuer 
ed,4^1^10^11 mi fid, bestes, ^eißt er bod), baß bie %mte ber Wim nnb bic 
Niachtstellnug der verschiedenen Nationen keineswegs an das Vorherrschen sich ewia 
gleichbleibender Wirtschaftsznstäude und Gesellschaftsklassen gebunden sind — das 
Usolmehr die verschiedensten Wirtschaftsorganisationen sich im Laufe der Jahrhunderte 
¿ 7, " """ um »Iiuuimmiļļui ljuuutu, vie aïs onia) ote tonimeli 
Be lugnugeu gegeben erscheinen und die abzuändern außerhalb der Grenzen mensch- 
"dic'i ^1)^1^ ließt, ^aßeßcn 3cißt er, baß Wim nnb ^a^^t^^emInß ber mtionen 
nur bn gefährdet waren, wo diese Entwicklungen auftraten in Begleitung von