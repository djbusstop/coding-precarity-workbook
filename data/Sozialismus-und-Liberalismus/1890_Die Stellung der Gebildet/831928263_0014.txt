12 — 
Dabei ist auf den Ton dieses Umgangs zwischen Gebildeten und Arbeitern 
wohl zu achten. 
Vor allem Eins: Wie die Insassen selbst den verschiedensten religiösen, poli 
tischen, ökonomischen und sozialen Ueberzeugungen huldigen, so wird auch von dem 
Arbeiter, dem sie nahe treten, keinerlei Verzicht auf diese oder jene Meinung oder 
Parteistellnng verlangt. Es heißt da nicht, bist du Anglikaner oder Dissenter, 
Katholik oder Inde, Tory oder Whig, Radikaler oder Sozialdemokrat, sondern ein 
fach, womit kann ich dir helfend Ganz ebenso, wie sich bei der Eröffnung von 
Toynbee Hall Mitglieder aller religiösen und politischen Glaubensbekenntnisse, vom 
Sohne des Prinzen von Wales bis zum Radikalen Chamberlain beteiligten, ist die 
absoluteste religiöse und politische Toleranz auch das leitende Prinzip in der Praxis 
geblieben. ^ ^ 
Sodann: Die Erörterungen mit den Arbeitern entbehren aus Seite der Uni- 
versitätsleute jedweden Tones überlegener Lehrhaftigkeit. Kein süffisantes Abkanzeln 
derjenigen, die anderer Meinung sind; sondern ein Diskutieren jedweder Meinung als 
von etwas, was seine Berechtigung haben könne und erst geprüft werden müsse, lind 
durch strengstes Festhalten dieser beiden Prinzipien haben die jungen Universitätsleute 
nicht nur das Vertrauen der Arbeiter, sondern auch eine Autorität erlangt, die man 
ihnen, wenn sie beansprucht worden wäre, allzeit verweigert hätte. 
Was ich hier erzähle, ist nur auf London beschränkt; aber wenn nicht dieselben 
Niederlassungen, derselbe Geist, der ihre Insassen beseelt, findet sich heute bei ber 
gebildeten Jugend durch ganz England. Ueberall, um die Worte Carlyle's über 
Kingsley zu gebrauchen, ein Ueberschwang an großmütigem Eifer, ein wahres Unge 
stüm im Drange nach einer ehrenhaften männlichen Behandlung aller auf die unteren 
Klaffen bezüglichen Fragen. Es ist geradezu üblich geworden, daß der junge Mann 
der höheren Klassen einen Abend in der Woche in einem Arbeiterviertel zubringt in 
ähnlicher Thätigkeit, wie ich sie hier geschildert habe. Am großartigsten aber offen 
barte sich der eingetretene Umschwung bei dem Strike der Londoner Dockarbeiter im 
Herbste vorigen Jahres. Es war, als hätte ein wahrer Wetteifer zu helfen die 
höheren Klaffen erfaßt. Ihre ganze Presse war auf Seite der Arbeiter. An 50,000 
Pfd. Steri, wurden zu ihrer Unterstützung beigesteuert. Kein Zweifel, ohne deren 
Beihülfe hätten die armen ungelernten Arbeiter ihre Schlacht niemals gewonnen. 
Entsprechend diesem Eifer aber auch der Erfolg. 
Um zunächst von dem Erfolg der Bildnngsbestrebungen zu reden: Das eine, 
was den Kontinentalen, der zu englischen Arbeitern in Beziehung tritt, verblüfft, ist 
die Beobachtung, daß die geistige Nahrung der höheren und niederen Klassen dort ein 
und dieselbe ist. Die ernsten wissenschaftlichen Werke wie die Unterhaltungslitteratnr, 
welche der eine liest, liest auch der andere — eine Thatsache, die für uns Deutsche 
besonders viel Beschämendes hat. Aber ich möchte mich hier nicht bloß ans eigene 
Beobachtung berufen. Vielleicht daß Sie meinen, ich sähe mit voreingenommenem 
Blicke. Aber lesen Sie die Schrift der von den deutschen Großindustriellen im 
Herbste vorigen Jahres nach England entsendeten Kommission. Das sind gewiß 
unverdächtige Zeugen. Und selbst in dem beschnittenen Bericht, der an die Oeffent- 
lichkeit gelangt ist, finden Sie den Ansdruck des größten Erstaunens über die 
Bildung der 'englischen Arbeiter, mit denen sie in Berührung gekommen. , 
Aber noch größer ist ihr Erstaunen über die Sachkenntnis, welche die heutigen 
Mitglieder der lange so verpönten Gewerkvereine der gelernten Arbeiter in wirtschaft 
lichen und sozialen' Dingen bekunden, und über den Geist, in dem sie dieselben be 
handeln. Bei allen fanden sie Rücksichtnahme auf das, was die gegebenen Verhalt»,y e 
als erreichbar und unerreichbar erscheinen lassen. Ueberall die Anerkennung der 
Berechtigung des Arbeitgebers, diese Fragen auch von seinem Standpunkt zu betrachten 
und zu verfolgen. Und nur darin irrte die Kommission, daß sie meint, dies sei in 
England allezeit so gewesen, während es nichts anderes ist, als das Produkt der 
letzten à^^ahre. ^ ^ ^ ^ ^ Stellung des englischen Arbeiters zii seinem 
Vaterlanded Ich habe vorhin von der vaterlandslosen Gesinnung der Chartisten 
ber Diesiger 3#e gc^n#». öüren mir, mie 3. ». ein Mrer ber ®rnbenorbcitcr