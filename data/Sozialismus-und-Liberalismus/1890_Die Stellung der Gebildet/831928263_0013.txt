— 11 
neue Geist sich äußert, iu wenigen Worten gerecht zn werden. Sv kann ich der 
Universitätsausdehmingsbewegung nur erwähnen, jener merkwürdigen Bestrebungen, 
die Universitäten zum Volk zu bringen, da das Volk nicht zu den Universitäten zn 
kommen vermag. Es handelt sich da um ein großartiges System von Wander 
lehrern, die zumeist in Abendklassen weiten Kreisen des Volks die Gegenstände der 
Bildnng vermitteln, welche die Studenten unter günstigeren Umständen an den 
Universitäten sich aneignen. Ihre Vorlesnngen werden hente bereits von etwa 
30 000 Personen jährlich besucht, nnb, wie die Prüfungen zeigen, mit gutem Erfolg. 
Aber wichtiger für unsre heutige Betrachtung sind die bleibenden Nieder 
lassungen, welche die Universitäten inmitten der Centren des verkommensten Prole 
tariates gegründet haben. 
John Nnskin, der geistvolle Aesthetiker und lebhafte Sozialpolitiker, hatte vor 
Jahren der Geistlichkeit zugerufen: Ihr speist mit den Neichen und predigt den 
Armen; es wird nicht besser werden bis ibr mit den Armen speist nnb den Reichen 
predigt! Die englische Geistlichkeit hat sich diesen Ruf längst zn Herzen genommen. 
Nun waren es aber Laien, welche es als Mangel empfanden, daß ihre Zugehörig 
keit zur Gesellschaftsklasse der Arbeitgeber sie zwar völlig vertrant gemacht hatte 
mit der Art und Weise, wie diese die Arbeiteröerhältnisse und die sozialen Zustände 
überhaupt beurteilen, daß ihnen dagegen in Folge mangelnden Umganges eine Kennt 
nis des Arbeiters, wie er wirklich sei, und seiner Anschauungsweise fehle. Sie 
hatteil daher ihren Wohnsitz inmitten der Arbeiterviertel aufgeschlagen, um in ihrer 
freien Zeit mit der dort wohnenden Arbeiterbevölkerung in Berührung zn kommen, 
sie dabei in ihren Bedürfnisseil wie in ihren Bestrebungen zn studieren und dabei 
durch Teilnahme an ihren Versammlungen wie au ihren Vergnügungen dieselben ans 
eine höhere Gesittungsstufe herauszuziehen. 
Nach dem Tode eines dieser Männer, Arnold Toynbee's, beschlossen die 
Universitäten, das Andenken desselben dadurch zn ehren, daß sie dauernde Einrichtungen 
trafen, um das von ihm begonnene Werk fortzusetzen. So entstanden z. B. Toynbee 
Hall in Whitechapel, Oxford House in Bethnal Green. 
Ich bin vor wenigen Wochen wiederholt tu Toynbee Hall gewesen. Es besteht 
ans einem unregelmäßig gebauten Hanse, in dem 10 bis 12 junge Männer, die 
eben ihre Universitätsstudien vollendet haben, wohnen, um während der ersten Vor- 
bereitnngsjahre für ihren künftigeil Berits ihre Mußestunden, besonders des Abends, 
der Arbeiterbevölkernng zn widmen, bis sie selbst wieder andereit Neuankömmlingen 
von den Universitäten Platz machen. Sie haben kleine, zellenartige Zimmer, daneben 
große gemeinsame Empsangsränme und Speisezimmer, mit allem englischen Comfort 
ausgestattet. Es findet sich- da ferner eine Bibliothek, zumeist ans Büchern der 
Insassen bestehend, welche dieselben hier den Arbeitern zur Verfügung stellen. Jed 
weder, der Namen und Adresse in ein anfliegendes Buch einzeichnet, erhält damit 
das Recht, gu lesen. Es findet sich da des weiteren ein Hörsaal, ausgestattet auch 
mit naturwissenschaftlichen Apparaten, in welchem die Insassen abwechselnd jeden 
Abend Vorlesnngen ans den verschiedensten Wissenszweigen halten. Als ich dort 
war, fand ferner tu mehreren Räumen die alljährliche Gemäldeansstellung statt. Die 
Insassen wenden sich an ihre Freunde im ganzen Land, um durch Ueberlassnng von 
Bildern, die sich in deren Privatbesitz befinden, während ein paar Wochen die Kunst 
nnb ihre veredelnden Wirkungen auch denen zuteil werden zn lassen, die sonst nie 
etwas von ihr ahnen würde». Ich fand da Bilder ersten Ranges, wie von Josnah 
Reynolds nnb Anderen. Die Benutzung der Bibliothek ist eine befriedigende, der 
Besuch der Vorlesungen sehr stark, die Gemäldeansstellung war überfüllt. 
Aber darin erschöpft sich nicht die Thätigkeit der Insassen von Toynbee Hall. 
Ein Jeder ist allzeit jedwedem Arbeiter zugänglich, der Fragen an ihn zn richten 
wünscht, und auch diese Gelegenheit, sich aufzuklären, wird fleißig benutzt. Aber 
llicht nur das. Die Toynbee-Männer nehmen teil an den Vergnügungen der Be 
völkerung, in deren Mitte sie wohnen, beteiligen sich an den Erörterungen ihrer Ver- 
sammlnngen, werden Mitglieder ihres Klubs, beraten sie tu den Angelegenheiten 
ihrer Vereine, ja ich fand einen von ihnen als Schatzmeister eines Zweiges des 
Gewerkvereiues der Dockarbeiter.