4 
Revolutionen und Reaktionen, welche, wie bei uns der Bauernkrieg, die Kraft der 
Nationen zerrütteten und ihre Kultur und Machtstellung um Hunderte von Jahren 
zurückwarfen. Er zeigt also, daß es gilt, diese Revolutionen und Reaktionen zu 
vermeiden. 
Diese Erkenntnis, sowie jene Stellung fern von den streitenden Interessen 
macht die Gebildeten als besonders geeignet, in unsrer Frage zwischen Kapital und 
Arbeit als Vermittler zwischen den im Machtbesitz befindlichen und den anfstrebendeit 
Massen zu dienen. 
In welcher Weise aber hat man sich die Erfüllung dieser Vermittlerrolle zu 
denkend 
Bei Beantwortung dieser Frage besteht eine große Gefahr -- nämlich die, 
daß man, von idealen Gesichtspunkten geleitet, unausführbare Anforderungen stellt. 
Um sie zu vermeiden, erscheint es am besten, sich umzusehen, ob bereits irgend 
wo günstige Erfahrungen vorliegen, die zum Vorbild geeignet sind. Und solche 
Erfahrungen giebt es in einem Lande, das in wirtschaftlicher und sozialer Beziehung 
dem unseren um viele Jahrzehnte voraus ist, wo aber diese Entwicklung eben in 
Folge davon, daß die Gebildete,: ihre Funktionen richtig erfaßt und erfüllt haben, 
einen im ganzen glücklichen Verlauf zu nehmen scheint. Ich denke an England. 
Aber vielleicht erhebt sich der Einwand, daß ich, gerade wenn ich mich ans 
England berufe, auf Abwege gerate. Wie oft schon habe ich, wenn ich mich auf 
die vorgeschrittenere Entwicklung in England bezog, zu hören bekommen: ja so etwas 
paßt für England, aber nimmermehr für uns. Der englische Arbeiter nämlich sei 
so vernünftig, bei seinen Forderungen die Grenzen des Möglichen nie außer Acht 
zu lassen. Er sei voll Achtung vor dem Gesetz. Er sei voll nationaler Gesinnung. 
Das Glück von England sei eben dies, daß es niemals eine lediglich von gemeiner 
Gier erfüllte, gesellschaftlich und politisch sozialrevolutionäre Arbeiterpartei besessen 
habe. Ganz anders sei es mit unsrer die Arbeitermassen verwildernden Sozial 
demokratie, welche die Vaterlandslosigkeit auf ihre Fahne geschrieben habe. 
Dies ist die Meinung, welche heute gerade im Gespräche mit gebildeten 
Männern fortwährend zu Tage tritt. Vor acht Tagen erst vernahm ich sie ans dem 
Munde eines hervorragenden Parlamentariers, kurz vorher hatte einer der ersten 
Männer der Wissenschaft das Gleiche zu mir geäußert- Ja sie scheint fast ein Axiom 
unsrer gebildeten Klassen geworden zu sein. 
Ich weiß nun nicht, wie diejenigen, welche an diesen Sachverhalt glauben, sich 
denselben denken; wie sie sich erklären, daß gerade in dem Land, in dem die wirt 
schaftlichen Gegensätze die schärfste Ausbildung erfahren haben, niemals eine sozial- 
revolutionäre Arbeiterpartei entstanden sei. Fllst sollte man denken, daß sie die 
Engländer für eine besonders von Gott begnadete Race ansehen, womit dann freilich 
im Widerspruch steht, daß sie bei anderer Gelegenheit über die Erwerbsgier John 
Bulls und die besondere Rohheit seiner unteren Klassen nicht genug Worte des 
Tadels zu finden vermögen. 
Allein einerlei, wie sie sich alle diese Widersprüche erklären, die Bchanplnng, 
daß der englische Arbeiter, von jeher vernünftig, nur das Mögliche ins Auge gefaßt 
habe, allzeit dem Gesetze gehorchend gewesen sei, und daß es dort niemals eine 
sozialrevolutionäre Partei gegeben habe, ist einfach nicht wahr. Ganz im Gegen 
teil: Alles, was wir heute bei unseren Arbeitern beklagen, die sittliche Verwilderung, 
die Thorheit in den Forderungen, die sie stellen, die vaterlandslose und sozialrevo- 
lutionäre Gesinnung, all dies ist in dem England, wie es in der ersten Hälfte 
dieses Jahrhunderts gewesen ist, in noch weit stärkerem Maße vorhanden gewesen. 
Und was die sozialrevolutionäre Partei angeht, so ist unsre heutige Sozialdemo 
kratie in Zielen, Maßnahmen und bis auf die Schlagworte nur eine schwächere 
Nachahmung der englischen Chartisten. ^ ^ 
Diese Thatsache ist so wichtig und dabei so sehr im Widerspruch unt weit 
verbreiteten Vorurteilen, daß ich notwendig bei ihrer Darlegung etwas verweilen 
ķà' Bereits seit dem Schlüsse des 18. Jahrhunderts hatte der Großbetrieb die 
alte gewerbliche Ordnung aufzulösen begonnen, und damit hatte sich die Lage der