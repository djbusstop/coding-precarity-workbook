20 
dieser Angelegenheit bis zum Jahre 1869, als die neue Ge 
werbeordnung und mit ihr die Geltendmachung der gewerb 
lichen Freiheit in allen Richtungen und mit allenConsequenzen 
zur Herrschaft kam. Es war einer jener Wendepunkte im öffent 
lichen Leben, wo eine lange ungebührlich niedergehaltene Feder, 
endlich losgelassen, zunächst über die richtige Höhe hinausschnellt; 
man freute sich der endlich errungenen neuen Ordnung der Dinge, 
wie sie von der ökonomischen Wissenschaft seit Jahrzehnten gefor 
dert worden war, und richtete sich in dieser neuen Ordnung zu 
nächst mit möglichster doctrinärer Consequenz ein. Erst die Regel 
besiegelt, hieß es, die Ausnahmen wird uns, so weit sie wirklich 
Bedürfniß sind, schon die Erfahrung von selbst aufdrängen. Ein 
Politischer Grundsatz, der, beiläufig bemerkt, nur bei uns neu 
erscheint, während er in parlamentarisch regierten Ländern ein 
längst geläufiger ist. Man giebt dort häufig einem solchen ver 
suchsweise fortschreitenden Gange der Gesetzgebung den Vorzug und 
denkt nicht daran, es immer als einen Gang nach Canossa anzu 
sehen, wenn ein im Principe richtiges Gesetz später in Folge wei 
terer Erfahrungen Ausnahmebestimmungen erfährt. Die Ausnahme 
bedürfnisse haben denn auch gegenüber der deutschen Gewerbe 
ordnung nicht verfehlt, sich fühlbar zumachen auf verschiedenen 
Einzelgebieten, — so bekanntlich auf demjenigen der ärztlichen 
B e r u f s a u s ü b u n g, der Errichtung von Kranken- und 
Irrenanstalten, und namentlich auch bezüglich des Aus 
schanks gei st iger Getränke. Diesen Ausnahmen ist das Gesetz 
vom 23. Juli 1879 zum Theil gerecht geworden, aber auch nur 
z u rn T h e i le. Dasselbe bestimmt bekanntlich, daß die Erlaubniß 
zum Ausschänken von Branntwein oder zum Kleinhandel mit 
Branntwein oder Spiritus allgemein, die Erlaubniß 
zum Betriebe der Gastwirthschaft oder zum Ausschänken von Wein 
und Bier in Ortschaften mit weniger als 15,000 Ein 
wohnern , so wie in solchen Ortschaften mit einer größeren Ein 
wohnerzahl, für welche dies durch Ortsstatnt festgesetzt wird, von 
dem Nachweis eines vorhandenen Bedürfnisses abhängig 
sein solle. Vor Ertheilung der Erlaubniß ist die Ortspolizei und 
die Gemeindebehörde gutachtlich zu hören. 
Liegt in diesem Abänderungsgesetze ein sehr anerkennenswerther 
Fortschritt , namentlich auch durch die scharfe Unterscheidung der 
Branntwein-Schankcvncession von derjenigen für andere geistige Ge 
tränke, so ist dasselbe als genügend noch keineslvegs anzusehen. Das 
Maß d e r G e w ä h r u n g von Schankconcessionen entbehrt einer gesetz 
lichen Maxim al grenze, wie solche z. B. in Schweden festgesetzt 
und gegenwärtig in England und Holland eingeführt werden soll. 
Dem möglichen Einflüsse irreleitender Nebeninteressen auf die Ge 
meindebehörden muß staatlicherseits eine gewisse Schranke gesetzt 
werden. Eine solche würde nicht zu enge gezogen werden, lvenn 
man das Maximalverhältniß für Städte ans eine Concession für 
je 1000 Einwohner und für Landgemeinden auf eine Concession 
für je 500 Einwohner festsetzte. 'Außerdem aber sollte es den