18 
teste verneint. Er bezahlt seinen Wotka theurer und trinkt schlech 
teres Zeug, weiter ist kein Unterschied. Auch in Frankreich war 
nach der starken Erhöhung der Branntweinsteuer im Jahre 1873 
nur vorübergehend einige Abnahme des Consnms bemerkbar; nach 
zwei Jahren stieg er wieder über die frühere Höhe vor 1873. 
Eine andere Erwägung aber kommt hier noch in Betracht — 
nämlich die Gefahr, daß bei steigender Einnahme ans dem Brannt- 
weinconsum das sogenannte sis calis che Interesse sich in den 
Vordergrund dränge und auf die Behandlung besonders der Con 
cessionsfrage einen bedenklichen Einfluß übe. Ich will hier nicht 
auf Rußland verweisen, wo die Regierung der Bildung von Mäßig 
keitsvereinen unter den Bauern mit strengen Strafen entgegentrat, 
um einer für die Staatskasse so bedrohlichen Bewegung ein Ende 
zu machen, derartiges haben wir ja nicht zu fürchten; aber wenn 
ich in dem Budgetberichte des Finanzministers in unserem stamm 
verwandten constitutionellen Nachbarlande, in Holland, lese, wie der 
selbe die Hoffnung ausspricht, daß iin nächsten Jahre der Brannt- 
weinconsum wieder wie in den vorhergegangenen Jahren um etwa 
5000 Hectoliter steigen werde — wenn solche Hoffnung ausge 
sprochen wird, in demselben Augenblicke, wo Regierung und Volks 
vertretung mit einem Gesetzentwurf gegen die überhandnehmende 
Trunksucht beschäftigt sind — und wenn ich mir dann vergegen 
wärtige, daß es anderswo doch mindestens ebenso bedrängte und 
geplagte Finanzminister giebt wie in Holland, dann kann ich mich 
nicht eines Bedenkens dagegen erwehren, daß durch irgend welche 
Steuereinrichtungen das Interesse unseres Staatssäckels in eine 
Solidarität gerathe mit dem schwungvollen Fortgange unseres 
Branntweinconsums. 
Dieses Bedenken würde ich auch keineswegs beseitigt sehen 
durch eine Ueberweisung des Finanz-Emolumentes, welches aus 
einer hoher Concessionssteuer erflösse, an die Gemeindekassen. 
Wer Gelegenheit hatte, zu beobachten, wie gierig die Vertretungen 
besonders ländlicher Gemeinden nach jeder Hülfsquelle zur Er 
leichterung des Gemeindebudgets greifen, der kann nicht die Erwar 
tung hegen, daß diese Vertreter sich gegenüber Concessions- 
g esu ch en, die noch so weit über das Bedürfniß in der Gemeinde 
hinausgehen mögen, ablehnend verhalten werden, wenn mit der Ge 
währung jeder solcher Concession eine irgend erhebliche Ein 
nahme für die Gemeindekasse verbunden ist. 
Weit erfolgreicher als alle St eu er maßregeln sind denn auch 
nach den bisher vorliegenden Erfahrungen diejenigen Gesetzes 
einrichtungen gewesen, welche unmittelbar darauf abzielen, die 
Versuchung und Verführung, überhaupt die Gelegenheit 
zum Branntweintrinken möglichst zu beschränken. Gelegenheit 
macht bekanntlich nicht blos Diebe, sondern auch Spieler, Trinker 
und sonstige Excedenten aller Art. Wenn der Bürger und Arbeiter 
auf jedem Wege und Stege an Häusern vorbeikommt, aus denen ihm 
die herzwärmende Flasche winkt, wenn ihm beim Bäcker und Gewürz 
händler, beim Barbier und vielleicht sogar beim Schulzen und