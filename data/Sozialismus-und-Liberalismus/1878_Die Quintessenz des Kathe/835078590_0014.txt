6 
Menschen, der dabei nicht nur Interessen sondern zugleich auch 
Pflichten vor Augen haben muss. Das Prinzip der Wirthschaft- 
lichkeit, d. h. das Streben Güter mit möglichst wenig Opfer 
und Anstrengung zu erlangen, muss durch das Prinzip des Ge 
meinsinns eingeschränkt werden, demzufolge jeder seinen Mit 
menschen bereitwillig zugestehen soll, was er von ihnen verlangt«. 
Ist es nicht hübsch, in einem Lehrbuch der Nationalökonomie 
zu lesen: 
Was Du nicht willst, dass man Dir thu’ 
Das füg’ auch keinem andern zu. 
Das beweist, dass der Verfasser ein besseres Gedächtniss für 
seinen Katechismus als für die Lehren von Adam Smith, J. B. Say, 
Kau (den ächten, nicht den verfälschten) und anderen gehabt 
hat. Der gelehrte H Professor, fürchte ich, wird keine tüchtige 
Oekonomisten ausbilden, da dieselben vor lauter Moral keine 
Volkswirthschaft zu hören bekämen. Ich bin für die Arbeits- 
theilung: der eine forscht nach den Phänomenen der Volks 
wirthschaft und stellt fest, was er gründlich erforscht hat, der 
andere zeigt, wo das erlaubte volkswirthschaftliche Wirken 
aufhört und wo die Selbstverleugnung beginnen soll. Es ist 
übrigens keinem verboten zugleich Volkswirth und Moralist zu 
sein, aber dann muss er immer unterscheiden, was zum einen 
und was zum andern gehört. Unterscheidet er nicht, so macht 
er sinnlose Phrasen, wie die oben angeführte: yDas Streben 
Güter mit möglichst wenig Opfer und Anstrengung zu erlangen^ 
muss dureh das Prinzip des Gemeinsinns eingeschränkt werden.< 
Aus solchen vagen Sätzen wird absolut nichts gelernt werden 
können, allenfalls wird man folgendes daraus entwickeln können: 
ich erfinde eine Maschine mit der ich doppelt soviel produziren 
kann als meine Kollegen, der Gemeinsinn verbietet mir aber, 
die Maschine zu benutzen. H. Held ist wohl der einzige, der 
jenen so wichtigen Beobachtungssatz so bis in’s Nichtssagende 
verwässert. Es ist aber seine >Tendenz« so nichtssagend zu 
sein, wahrscheinlich damit man nicht seine Sätze zu »mehr 
oder minder fiktiven Prämissen« macht.