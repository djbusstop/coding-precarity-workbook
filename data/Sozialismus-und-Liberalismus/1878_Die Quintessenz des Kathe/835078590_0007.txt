Vorrede. 
\ T enn ich den Kathedersozialismus bekämpfe, so ist’s unter 
anderm um einem Gelehrten, von dem viel in diesen Blättern 
die Rede ist, zu beweisen, dass der Gemeinsinn nicht stets mit 
dem Egoismus wirksam ist, denn vorliegende Schrift ist ein 
Produkt des Gemeinsinnes allein; keine Spur von Egoismus ist 
dabei im Spiele. Ich trete für die ökonomische Wissenschaft ein, 
mit dem einzigen Zweck, zu derem Fortschritt beizutragen. 
Männer, wohlausgerüstet mit Talent und mit Gelehrsamkeit, 
die mit grossem Erfolg auf demselben Felde mitarbeiten könnten, 
sehe ich auf Irrwege gerathen, und die jüngere Generation zum 
Theil nach sich ziehen. Diese Männer möchte ich womöglich 
zurückrufen in den richtigen Weg, wenigstens jedoch die heran- 
wachsende Jugend warnen vor den Gefahren, welche die heut zu 
Tage vorgetragenen subjektiven Lehrsätze bergen. Dergleichen 
Lehrsätze legen ihren Anhängern eine Binde vor die Augen, 
zum allerwenigsten färben sie deren Brille; die ächte Wahrheit 
bleibt ihnen verborgen, und doch wäre es ihnen und der Mensch 
heit so nützlich, wenn sie sie sähen. 
Um sie aufzuklären, habe ich kein anderes Mittel als die 
falschen Lehrsätze zu bekämpfen. Dass ich den unparteiischen 
Leser überzeugen werde, daran zweifle ich nicht, denn ich wende 
mich an den schlichten, aber gesunden Menschenverstand. Auf 
den, dem der Irrthum Glaubenssache geworden ist, werde ich