47 
Wort gesucht werden ? Ich schlage Nebelthum vor. Dann werden 
künftige Nationalökonomen also definiren : Der Begriff des Privat 
besitzes zerfôllt in Eigenthum und Nebelthum; die politischen 
Oekonomisten und überhaupt alle vernünftigen Menschen kennen 
blos den klaren Begriff des > absoluten < Eigenthums, die Katheder 
sozialisten und andere Träumer ziehen das unklare, unbestimmte 
Nebelthum vor. Dies Nebelthum ist nämlich weder »beschränkte 
noch > unbeschränkt. < 
Ist es denn so unbedingt wahr, dass »das Maass dieser Be 
fugnisse wechselt?« Selbstverständlich kann hier nur Einzelbe 
sitz mit Einzelbesitz verglichen werden, und da möchte der 
Beweis schwerer fallen als man glaubt. Der Einzelbesitz be 
weglicher Güter, Waffen, Geräthe, Kleider, war von jeher so 
absolut wie es der Grad der Gesittung mit sich brachte — denn 
das Raub- und Mordrecht des wilden Häuptlings werden Sie 
doch nicht mitrechnen wollen, sonst müssten Sie auch das 
Menschenfressungsrecht in Anschlag bringen; — der Einzelbe 
sitz unbeweglicher Güter hat sich natürlich erst viel später 
eingeführt, weil in früheren Zeiten kein Interesse dafür da war. 
Aber nochmals und wieder: wo wollen Sie hinaus? Sollen wir 
zurück gehen, damit die Bäume des Fortschrittes nicht in den 
Himmel wachsen? Sollen wir, weil in diesem oder jenem Zeit 
alter ein Punkt noch unklar war, die nunmehr, oft mit Mühe 
und Kampf, erungene Klarheit aufgeben, etwa sagen: wir sind 
lange genug auf zwei Beinen gegangen, versuchen wir s einmal 
wieder auf allen vieren zu gehen, wie unsere Vorfahren es ge- 
than haben sollen? Ein Fortschritt ist die Schwächung der 
Eigenthums Idee in keinem Falle. Sie steht in engster Ver 
bindung mit dem Bestreben (S. Wagner, Lehrbuch S. 351) die 
persönliche Freiheit zu bekritteln, zu bemängeln und zu be 
schränken und es wird dabei ganz vergessen, dass die Freiheit 
doch noch eine ganz andere Bedingung eines »menschenwürdigen« 
Daseins ist, als eine Lohnerhöhung von ein paar Groschen. 
Ehe ich hier zu schreiben fortfuhr, habe ich nochmals das 
betreffende Kapitel des Wagnerschen Lehrbuchs (das fünfte)