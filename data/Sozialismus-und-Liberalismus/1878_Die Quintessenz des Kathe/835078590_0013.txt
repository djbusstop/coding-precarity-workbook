5 
Karl Marx mitgerechnet, erkennen mehr oder minder ausdrücklich 
die Naturgesetze an. Wenn Karl Marx in »Das Kapitale von 
einem Quantum gesellschaftlich nothwendiger Arbeit spricht, so 
erkennt er den Einfluss der Arbeitstheilung und der Konkurrenz 
an. Dass die Arbeitstheilung aber die Produktion vermehrt 
und verbessert, dass die Konkurrenz ein Stachel zum Fortschritt 
ist, das sind Naturgesetze. Wenn zuweilen die Konkurrenz 
übertrieben wird bis zu strafbaren Handlungen, so müssen diese 
Handlungen ihren Lohn erhalten, aber darum bleibt doch die 
Konkurrenz eine wirthschaftliche Kraft. Dass dem so ist, wissen 
die Trades-Unions sehr gut, denn die meisten Bestimmungen 
ihrer Statuten sollen eben auf die Verminderung der Konkur 
renz wirken. 
Wenn ich von der mehr oder minder direkten Anerkennung 
der wirthschaftlichen Naturgesetze seitens der Kathedersozialisten 
spreche, so habe ich eine gewisse Anzahl Schriften vor Augen, 
die mir Belegstellen geben könnten; ich muss mich aber ein 
schränken und deshalb begnügen, mich an zwei der mit am 
stärksten sozialistisch gefärbten Professoren zu halten. H. Held, 
in seinem Grundriss für Vorlesungen^ S. 1, drückt sich also 
aus: »Die beständige Zunahme der Bedürfnisse beruht theilweise 
auf dem Trieb der Menschen, in ihrem äusseren Erscheinen es 
andern gleich zu thun, oder unter denselben hervorzuragen <. 
Worauf noch? das sagt der Verfasser nicht, aber ich frage jeden 
unpartheiischen Leser, ob die Ausdrücke »Trieb der Menschen< 
und Naturgesetz nicht synonym sind? Fahren wir fort. »Diese 
Zunahme tritt stets iin Geleite des Kulturfortschrittes auf«* 
Das klingt doch auch sicher gesetzraässig, der Verfasser setzt 
nur auseinander, dass die Sache in manchen Fällen gut, in 
andern nicht gut ist. Aber in keinem dieser Fälle kann man 
die Gesetzmässigkeit leugnen. 
Nun wollen wir eine andere Stelle (Grundriss, S. 2) be 
trachten: »Wenn es sich bei der wirthschaftlichen Thätigkeit 
des Menschen auch zunächst um die Verfolgung von Interessen 
handelt, so ist doch auch die Wirthschaft ein Ausfluss des ganzen