15 
Der Mensch liebt sich selbst, daher kommt alles Uebel. Denn 
Ja er sich selbst liebt, so strebt er nach seinem eignen Wohl 
sein, und dieses kann er nur finden, wenn es seinen Brüdern 
übel geht. Verhindern wir ihn also, seinen Tendenzen zu ge 
horchen; wir müssen seine Freiheit ersticken; laset uns das 
menschliche Herz ändern; setzen wir ein anderes Mobil (be 
wegendes Prinzip) hinein, als das, welches Gott geschalTen hat; 
lasst uns eine künstliche Gesellschaft erfinden und dirigiren.< 
Nachdem er mit einiger Ausführlichkeit von den 1850 
aufgetretenen Sozialisten und ihren weltrerbessernden Plänen 
gesprochen hat, fährt er also fort: »Gott behüte, dass ich an 
der Aufrichtigkeit irgend Jemandes zweifle; aber ich kann mir 
wirklich nicht erklären, dass jene Publizisten, welche einen 
radikalen Antagonismus im Innern der natürlichen Organisation 
der Gesellschaft erblicken, nur einen Augenblick lang Ruhe 
und Friede gemessen kSniien. Es scheint mir, dass Entmuthigung 
und Verzweiflung ihr trauriges Loos sein müsste. Demi für- 
wahr! wenn sich die Natur irrte, als sie das Selbstinteresse 
zur Tnebfeder der menschlichen Gesellschaften machte (und 
to Irrthum ist handgreiflich, so bald man annimmt, dass der 
Widerstreit der Interessen unvermeidlich ist), so müssen sie 
(die Sozialisten) doch einsehen, dass das Hebel unheilbar ist. 
Da wir selbst Menschen sind, und uns nur an Menschen wenden 
können, wo finden wir einen festen Haltepunkt, um die Tendenzen 
der Menschheit zu ändern? Sollen wir uns an die Polizei, an 
die Richter, an den Staat, an den Gesetzgeber wenden? Das 
liesse ja an Menschen appelRren, d. h. an Wesen, welche mit 
denselben Gebrechen behaftet sind. Sollen wir uns an das 
allgemeine Stimmrecht wenden? Dass Messe doch eben der 
allgemeinen Tendenz zum Triumph verhelfen!< 
Ich behalte mir vor weiterhin noch eine oder zwei Stellen 
aus Bastiafs Hauptwerk aiizuführen, aber schon jetzt sieht man 
dass er keineswegs empfiehlt egoistisch zu sein, sondern blos 
konstatirt, dass nun einmal das Selbstinteresse, die Triebfeder 
der meiischliciicu Gesellschaft ist. Und diese Tliatsache kann