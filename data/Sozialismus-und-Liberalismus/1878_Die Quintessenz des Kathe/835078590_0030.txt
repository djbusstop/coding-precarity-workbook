22 
Naturgesetzen kämpfe ich frei, lerne mich dem Unabänderlichen 
mit Gleichmuth fügen, aber auch die lenksamen Naturkräfte 
zu meinem Besten anwenden ; mit andern Worten, in einer 
freien wirthschaftlichen Gesellschaft kann sich mein Pleiss, mein 
Talent, können sich meine sonstige Gaben und Eigenschaften 
viel besser ihre Geltung verschaffen, als in einer nach Ihren 
subjektivischen Plänen organisirten. 
Wer bis hierher gelesen hat, mag gefunden haben, dass 
ich Streifzüge auf das Gebiet der Moral gemacht habe. — Bastiat 
und andere, vor und nach ihm (ihre Zahl ist gross) thaten es 
auch — und darauf könnte man uns sagen, dass wir thöricht 
handeln wenn wir nicht des Guten mehr thun : wir müssten 
unsere Lehrbücher mit mehr Moral versetzen, der moralische 
Beisatz würde das Werk in die Klasse der y ethischen* erheben. 
Das sollten wir um so mehr thun, als ja die ökonomischen 
Gesetze den Menschen nicht allein regieren: die Religion, be 
sonders aber die ins Gebiet der Moral gehörigen Gefühle und 
Handlungen, so wie die Politik haben ja ebenfalls einen grossen und 
legitimen Einfluss. Allein, wenn wir die politische Oekonomie 
absondern und für sich allein betrachten, wenn wir den Menschen 
— abgesehen von allen seinen anderen Verhältnissen — als 
blosses wirthschaftliches Wesen studiren, so thun wir nur was 
man stets und überall in der wirklichen Wissenschaft gethan 
hat und in progressivem Maassstabe thut: wir isoliren die 
Wissenschaft, um sie besser in ihrer Eigenheit erkennen zu 
können. Bios wenn sie erkannt ist, kann ich jeden gewonnenen 
Satz mit allen möglichen anderen Lehren und Dingen in Be 
rührung bringen, ohne dass Verwirrung entsteht. Sehr oft haben 
ganz entschiedene Oekonomisten die absoluten (d. h. »abstrakten <) 
Sätze der reinen Wissenschaft in der Ausführung, mehr oder 
minder, häufig selbst sehr siarh, gemildert, eben um anderen 
Bedingungen, anderen Momenten Rechnung zu tragen, daher ist 
auch das von unseren Widersachern uns vorgeworfene laissez 
faire — wovon noch weiter die Rede sein wird — eine nicht 
loyal geführte Partheiwaffe. Besagte Widersacher vermischen