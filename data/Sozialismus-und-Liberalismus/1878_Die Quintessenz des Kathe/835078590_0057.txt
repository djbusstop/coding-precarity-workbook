49 
Generation eines Erbpachters immer mit den Sozialisten stimmen 
würde. Vielleicht schon gar die erste! 
VII. 
Wir kommen wieder zu Herrn Held zurück und knüpfen 
an eine andere Schrift des gelehrten, talentvollen Professors an. 
In >Sozialismus, Sozialdemokratie und Sozialpolitik*« S. 28 
sagt er: »Wer soziale und politische Theorieen verstehen will, 
wird immer gut thun, nicht nur nach einzelnen thatsächlichen 
Unrichtigkeiten oder logischen Fehlern, sondern vor allem nach 
der Tmämz der Gründer dieser Theorieen zu fragen.« Das 
ist ein sehr gefährlicher Hath, denn Tendenzen sind etwas 
sehr Vages, Unbestimmtes, Schwebendes (etwa wie ein Nebel), 
die zwar sich sehr zur Propaganda eignen, aber der Wissenschaft 
ganz antipathisch sind. Die Wissenschaft verlangt Klarheit und 
scharfe Abgrenzung, Forderungen die nichts mit dem Radikalismus 
gemein haben, denn man braucht keiner extremen Richtung 
anzuhängen, um genau zu wissen und bestimmt zu wollen. 
Eigentlich ist wohl gewöhnlich die Tendenz da wichtig, wo 
das Gefühl eine starke Wirkung auf den Verstand übt oder zu 
üben trachtet. Der Verstand strebt in logischer Entwickelung 
vorwärts, das Gefühl zieht an oder stösst ab, es sind Impulse, 
die nichts nach der Logik fragen, sie geradezu als »abstrakt« 
schmähen. Der Kathedersozialismus bekennt übrigens seine 
Gefühlspolitik, und Herr Professor Held als einer der Koryphäen 
dieser Richtung kommt häufig auf die Tendenz, die Sympathie 
zurück. Das Gefühl spielt also bei ihm eine hervorragende 
Rolle,** mehr als es bei einem reformirenden Professor sollte. 
Denn er kann so nur einseitig reformiren, was so viel heissen 
mag, als aus dem Regen in die Traufe gehen: er fällt aus 
einen Uebel in das entgegengesetzte, aber bessert nichts. 
Wie falsch man durch eine tendenziöse Brille sieht, das 
* Leipzig, bei Duncker und IIumblot, 1878. 
•* Auf den Seiten 132—133 der neuesten Schrift des Herrn Held steht 
das Wort Tendenz fast in jedem Satz. 
4