23 
die wissenschaftlichen Ordnungen, um Waffen aus der Mischung 
zu schmieden, sie bringen es aber nur dahin, einen Mischmasch 
zu schaffen, der unfruchtbar für den Fortschritt der Wissen 
schaft bleiben wird. 
IV. 
Wir gelangen nun an eine dunkle Rede, deren Sinn wir 
zu erforschen haben. »Endlich verlangen sie (die Kathederso 
zialisten), dass auch der wirthschaftende Mensch als Glied des 
Staatsorganismus betrachtet werden müsse, sie verwarfen die 
Annahme eines allein gültigen Naturrechts und verlangten, das 
jeweilig geltende Rechtssystem müsse im Ganzen und Einzelnen 
als höchst wichtiger Faktor für die Gestaltung der wirthschaft- 
lichen Verhältnisse kritisch beachtet werden — sozial-politische, 
historisch-rechtliche Auffassung.< — Dunkel ist der Rede Sinn 
hauptsächlich desshalb, weil man uns da einen ganz urferwarteten 
und unverdienten Vorwurf zu machen scheint. Vom Staat haben 
wir weiterhin weitläufiger zu verhandeln, mithin sei hier bloss 
gesagt, dass die Oekonomisten den Staat nicht vergessen konnten, 
da sie sich lange genug mit ihm herumgebalgt und hierfür 
sogar sich die feierlichen Lobsprüche der ganzen sozialpoli 
tischen Gesellschaft verdient haben. Was mit dem »allein 
gültigen Naturrecht< gemeint ist, ist mir nicht klar. Vom 
Naturrecht nehme ich nur den Satz an, dass ich auch ohne 
positive Gesetze mein Leben, mein Eigenthum, meine Freiheit 
vertheidigen und schützen darf; ja ich glaube sogar, dass die 
ganze übrige Menschheit darüber so denkt wie ich, wenn auch 
der eine oder andere etwas Gegentheiliges (z. B. gegen den 
Besitz) geschrieben hätte. Aber ich glaube zu merken, dass in 
obigem Citate das Naturrecht der >sozial-politischen< Auffassung 
gegenüber steht. Dieser neue Ausdruck Sozialpolitik wird uns 
gar oft, und à toutes sauces, aufgewaitet, hier steht er nur als 
Gegentheil des Naturrechts, er ist also auch synonym mit 
»positives Recht,< und wirklich scheint das aus den Worten »das 
jeweilig geltende Rechtssystem< hervorzugehen. Dunkle Reden!