43 
garantiren, weiss ich nicht, H. von Laveleye auch nicht, er ge 
steht es; aber Gott weiss es (S. 305,), er hat den brillanten 
H. Professor der Lütticher Universität damit beauftragt, der 
Menschheit das Räthsel aufzugeben: > L’homme doit le découvrir 
et rétablir.< (S. 395.) Das Räthsel lautet also: Wie kann 
man zehn Hektaren unter zwanzig Menschen so vertheilen, dass 
jeder stets ein Hektar besitzt? -- Ach, da fallt mir etwas ein, 
woran H. v. L. gar nicht gedacht hat: wenn wir Alle den 
Acker bauen (allenfalls auch das Vieh hüten) sollen, wer webt 
und näht uns Kleider, wer reparirt unsere Uhren, druckt unsere 
Bücher, wer lehrt vom Katheder herab? — Räthsel! Mystère! 
Was H. V. Laveleye mit dem ihm eignen glänzenden Talent 
vorgetragen, lehren alle (oder fast alle) Kathedersozialisten. 
Dieselben wollen der Jugend den Gedanken recht geläufig machen, 
dass das Eigenthum so eine der gleichgültigen Einrichtungen 
ist, mit der der Staat nach Gutdünken umspringen kann. 
Nehmen wir den Grundriss des H. Professer Held, und jeder 
wird einsehen, dass die Lehre tendenziös abgefasst ist. (Grund 
riss S. 4 u. 5.) 
»Das Vermögensrechtssystem eines Staats kann auf dem 
Prinzip beruhen, dass die Einzelnen nur wechselnde Benutzungs 
rechte an den wichtigsten Sachgütern haben, über welche ju 
ristische Personen von öffentlichem Charakter stets die letzte 
Verfügung sich Vorbehalten — oder es kann auf dem Prinzip 
beruhen, dass alle einzelnen Sachgüter der möglichst ausschliess 
lichen und dauernden Herrschaft einzelner Menschen unterworfen 
sind. — System des Gesanimt- und des Sondereigenthunis.< 
Diese Darstellung ist nicht blos tendenziös, indem man eine 
verhältnissmässig wenig in Europa verbreitete barbarische Ein 
richtung (als solche selbst von H. v. L. anerkannt) auf gleichen 
Fuss stellt mit der in der ganzen zivilisirten Welt herrschenden ; 
sie ist nicht blos tendenziös, sie ist auch auf unwissenschaftliche 
Weise dargestellt. Nämlich : jedermann weiss, dass alle Gesetze 
und alle Gebräuche einen sehr grossen Unterschied machen, 
zwischen beweglichen und unbeweglichen Gütern; jeder Gebildete