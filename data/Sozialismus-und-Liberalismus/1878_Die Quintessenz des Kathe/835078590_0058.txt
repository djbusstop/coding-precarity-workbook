50 
geht schon aus folgendem Satze hervor (S. 35). »Die lächer 
lichste und gedankenloseste Anschauung ist die, welche die 
ganze deutsche Sozialdemokratie für das Produkt der raffinirten 
Redekunst einiger gewissenloser Agitatoren .... hält. Eine 
Partei wird nie durch Heden allein erzeugt.* < Ich könnte 
Ihnen mehrere Parteien nennen, die durch Reden allein ent 
standen sind, ich will aber bei der Stange bleiben. Dass die 
Sozialdemokratie durch die Agitation entstanden ist können Sie, 
unter andern, in Mehrings Schrift geschichtlich bewiesen sehen. 
Ich will Ihnen aber direkt antworten. Drei Zeilen nach obiger 
Stelle (S. 35.) sagen Sie: »Neue Klassen, zum Bewusstsein 
ihrer gedrückten Lage gelangt, werden empfänglich für alle alten 
Revolutionsideen, die andere Stände in anderer Zeit ausgebildet 
haben.**« Diese unklare Phrase will wohl zu verstehen geben, 
dass die Arbeiter (neue Klassen), »zum Bewusstsein ihrer gedrückten 
Lage gelangt« den Aufreizungen ein williges Ohr liehen. Geben 
wir zu, dass die gedrückte Lage auch dazu gehört — aber 
ohne die Aufhetzung wäre das »Bewusstsein« nicht gekommen, 
• Obgleich Sie „Arbeiterpresse" S. 20 sagen: „Wenn Lassalle die 
leidenschaftliche Unzufriedenheit und Uinsturzagitation in den Gang ge 
bracht hat, so . . .“ 
** Leider erlaubt der Raum nicht, so viel zu zitiren, wie es vielleicht 
wünschenswerth wäre, sonst hätte ich den obigen Satz weiter ausgefiihrt. 
Die weitere Stelle lautet also: „Die vergleichsweise besten Kenner unserer 
Sozialdemokratie sind leider diejenigen, welche aus irgend einem Grund mit 
der blinden Leidenschaft der Sozialdemokraten gegen unsere herrschenden 
politischen Grundsätze sympathisiren. Dahin sind zu rechnen die Ultramon 
tanen (von Kettel er, Mon fang, Kolping, .loerg, Schingt), die Sozial konserva 
tiven (d. h. von Lebenden namentlich Rudolph Meyer), der kirchlich eifrige 
Pastor Todt u. s. w. Auch der „Föderalist“ Schäffle gehört hierher.“ 
Diese Naivitäten sind (übrigens überflüssige) Beweise für die Aufrichtigkeit 
des Verfassers. Für ihn versteht nur der die Sozialdemokraten, der mit 
ihnen sympathisirt. Ich, meinerseits, kann nicht glauben, dass der Bischof 
Kettler mit jenem cynischen Atheisten sympathisirt. Tritt Sympathie „in 
die Erscheinung“ so ist sie mir höchst verdächtig. Doch es ist wirklich 
nicht der Mühe werth die Erörterung fortzusetzen. 
Aus dieser Stelle wäre übrigens zu schliessen, dass auch Herr Professor 
Held zu den Unzufriedenen gehört. Der Schluss ist richtig gezogen; warum 
giebt man uns auch falsche Prämissen!