von All> SayflaoïH* \voMn 
I. (>. Hcbröiler» in Berliti. 
56 
und eine Weile zur Modesache machen; es geht damit, wie 
mit vielen philosophischen Systemen, sie entstehen, herrschen 
in einem kleinen Gebiet, und vergehen wieder. Das wird auch 
das Loos des Kathedersozialismus sein, da ihm feste Grundsätze 
fehlen und er sich mit Tendenzen und mit Sympathieen begnügt. 
Darum wird er auch die Wissenschaft nicht bereichern, sein 
einziges Resultat wird sein, dass wir unsere Lehrbücher mit 
mehr Gründlichkeit schreiben, gewisse evidente Wahrheiten mit 
grösserer Ausführlichkeit beweisen und in der Darstellung etwas 
mehr Vorsicht gebrauchen, damit unsere Widersacher nicht 
ungenaue Ausdrücke oder die Weglassung selbstverständlicher 
Ausführungen gegen uns ausbeuten. 
Unsere Hoffnung des Besserwerdens beruht auf der jüngeren 
Generation. Birgt diese ein oder zwei Männer von Talent, 
welche mit frischer Kraft die Darstellung der rationellen National 
ökonomie auf wirklicher, >tendenzfreier« Erfahrung übernimmt, 
so schwindet der kathedersozialistische Nebel und die reine 
Wissenschaft strahlt mit neuer Klarheit, die ganze politische und 
gewerbliche Welt beleuchtend.