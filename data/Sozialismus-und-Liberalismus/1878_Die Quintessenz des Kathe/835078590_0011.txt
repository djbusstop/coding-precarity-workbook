3 
beherrscht und sie bestritten den Satz, der Mensch soUe un 
gebunden nur vom Egoismus beherrscht werden, dadurch werde 
das allgemeine Wohl am sichersten gefördert. Sie behaupten 
dagegen, der Qemeinsinn sei stets mit dem Egoismus wirksam 
und solle das sein — ethische Nationalökonomie. Endlich ver 
langten sie, dass auch der wirtlischaftende Mensch als Glied 
des Staatsorganismus betrachtet werden müsse, sie verwarfen 
die Annahme eines allein gültigen Naturrechts und verlangten, 
das jeweilig geltende Rechtssystem müsse im Ganzen und Ein 
zelnen als höchst wichtiger Faktor für die Gestaltung der 
wirthschaftlichen Verhältnisse kritisch beachtet werden — sozial 
politische, historisch rechtliche Auttässung.« 
Wie sehr die Kathedersozialisten an Unklarheit leiden geht 
schon daraus hervor, dass H. Held die eben angeführte Stelle 
mit den Worten einleitet »die .... Kathedersozialisten . . . . 
erfanden, wie schon gesagt, keine neue Theorie.< Also das 
eben Vorgetragene ist keine Theorie? Was fst’s denn? Man 
verlangt von uns »volles Verlassen des Strebens nach Auf 
stellung allgemein gültiger wirthschaftlicher Naturgesetze«, 
also wir haheii dieses Streben, und Sie nicht, und Sie behaupten 
keine neue Theorie erfunden zu haben ! Sie haben vielleicht 
recht, denn Sie verleugnen jede Theorie. Wenigstens in meinen 
Augen ist die Nationalökonomie nur dann eine Wissenschaft, 
wenn sie Naturgesetze anerkennt. Was bleibt ihr übrig, wenn 
die Naturgesetze verworfen werden? — Ansichten. — H. Held 
meint dies, H. Wagner jenes, H. Rösler ein drittes, und andere 
ein viertes, fünftes, sechtes, u. s. w. — Ansichten sind keine 
Wissenschaft, auch dann nicht, wenn sie von Aristoteles, von 
Plato, von Bacon oder Montesquieu, vom grössten und ruhm 
gekröntesten herrühren. Von Ansichten kann man sagen, dass 
sie »historische Kategorieen« seien, sie entstehen in ihrer Um 
gebung und vergehen in einer andern. Just von einigen der 
neueren Kathedersozialisten, und von den berühmtesten: von 
Roscher, Ad. Wagner, Brentano kann man sagen, die Ansichten 
oder Meinungen haben sich geändert, und wenn dies bei einigen 
1*