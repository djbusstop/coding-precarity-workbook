11 
Dinge, das ist das Naturgesetz. Nicht in jeder menschlichen 
Gesellschaft kommen alle wirthschaftlichen Naturgesetze zur 
Anwendung. Wo keine Brennstoffe sind, da kann kein Feuer 
entstehen, ob aber an einem bestimmten Ort Brennstoffe vor 
handen sind oder nicht, diese Thatsache hat nichts mit den das 
Feuer betreffenden Naturgesetzen zu thun. So lange es keine 
Eisenbahnen gab, konnten sie keinen Einfluss ausübeu, sind sie 
vorhanden, so sind sie von Gesetzen regiert, welche die Wissen 
schaft aufzusuchen und festzustellen hat. Es ist unsere Pflicht 
nach der Auffindung dieser Gesetze zu streben. Dabei wissen 
wir ganz gut, dass die Anwendung eines wirthschaftlichen Ge 
setzes von Zeit und Ort beeinflusst wird; aber Zeit und Ort 
können dem Gesetz an sich nichts anhaben. ln den Sklaven 
ländern — vor 2000 Jahren in Born und vor 20 Jahren in 
Rio Janeiro — waren die Sklaven theuer wenn sie selten und 
billig wenn sie häufig angeboteu wurden. Ja, und dies ist 
vielleicht Wasser auf die Mühle der Sozialisten, es ist ein Natur 
gesetz, dass der Sklave weniger arbeitet als der freie Arbeiter. 
Das Naturgesetz besteht auch, wenn keine Sklaven vorhanden 
sind, d. h. es ist keine vergängliche Wahrheit, sondern eine 
ewige, die nur zeitweise oder ortsweise Anwendung findet. Dies 
geht so weit, dass, wenn man aus Thorheit oder mit demago 
gischen Absichten dem freien Arbeiter einredet, er sei ein Sklave, 
er sei nicht frei (und mehr als ein Kathedersozialist Hess sich 
diese Thorheit zu Schulden kommen), auch der freie Arbeiter 
weniger und schlechter arbeitet. 
Dass ein Gesetz vorhanden ist, auch wenn es augenblicklich 
grade keine Anwendung findet, scheint einleuchtend genug, 
wenn man aber dennoch dieser Wahrheit die Augen verschliesst 
und die >historische Kategorie < zum Postulat erhebt, so ist es 
bloss, um sagen zu können: da die menschliche Gesellschaft 
nicht immer so eingerichtet gewesen ist, wie jetzt, so kann 
man sie auch anders einrichten. Das kann allerdings bis zu 
einem gewissen Maasse geschehen, ich glaube aber nur bis zu 
einem sehr kleinen Maasse, denn die Gesellschaft bewegt sich