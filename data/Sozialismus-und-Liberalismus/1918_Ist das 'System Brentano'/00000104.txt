﻿Linder der Herkunst

Tonne»

Linder »er Herkunft

Tonnen

Maletto-, Mimosa-, Mangrove-
und andere Gerbrinden
(außer Eichen- und Nadelholzrinden).

Brit.-Südafrika
Madagaskar

Einfuhr

Ausfuhr

43 336
30 464
5039
3 595

Quebracho, und anderes Gerbholj
in Blöcken.

Argentinien

Einfuhr

Ausfuhr

112 284
112 284

Gerbstoffe (außer Gerbholjrinben);
Katechu; Kino.

Einfuhr 4Z 463

Türkei........	16	360

Brit.-Jndien usw........	13	738

China......................   4	130

Venezuela .............. 5102

Ausfuhr 1162

Terpentinharze (Fichtenharze).

Einfuhr

Frankreich ........

Der. Staaten von Amerika
Ausfuhr

96 265
16 896
77 010
25 803

Kauri- und andere Kopale.

Einfuhr	5 357
Brit.-Jndien usw		609
Niederländ.-Jndien usw. .	1 962
Neuseeland				I 051
Ausfuhr	863

Andere Hartharze, Weich-,
Gummiharze.

Einfuhr	4737

Niederländ.-Jndien usw. .	1 084

Ausfuhr	1183

Schellack.

Einfuhr	3950

Brit.-Jnbien usw......... 3 743

Ausfuhr	1364

Akazien-, Acajou-, Kirsch-, Kutera-
Bassoragummi.

Ägypten.
Brit.-Jndien usw.

Einfuhr

Ausfuhr

6 185
3727
1471
2414

Kautschuk, roh oder gereinigt.

Einfuhr	20497

Großbritannien...........	497

Brit.-Ostafrika..........	114

Brit.-Westafrika ........	243

Deutsch-Ostafrika......	1 095

Kamerun ................... 1 637

Franz.-Westafrika ............... 313

Madagaskar ...................... 118

Belg.-Kongo ............. i 702

Portug.-Ostafrika ..............  115

Brit.-Jndien usw.	......	4 268

Brit.-Malakka usw.......	689

Ceylon................. . 1125

Niederländ.-Jndien	usw.	.	1037

Bolivien......................... 188

Brasilien ................. 5 556

105