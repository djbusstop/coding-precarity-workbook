﻿Jahrzehnt dieses Jahrhunderts vortreffliche Monographien nament-
lich über die Zustände im Handwerk, in der Hausindustrie, über
Kommunalverwaltung, Sparkassen und Steuerfragen, und über
Auslese und Anpassung der Arbeiter in der Großindustrie und über
Arbeitgeberverbände veröffentlicht. Da kam der große Bergarbeiter-
streik im Frühjahr iyo?. Ganz Deutschland blickte atemlos auf das
Ruhrrevier. Die gesamte deutsche Volkswirtschaft war in Mit-
leidenschaft gezogen; es war augenscheinlich, daß der Zustand,
wonach es in das Belieben einiger weniger industrieller Herren
gestellt war, durch Aussperrung von Hunderttausenden von Arbeitern
die gesamte deutsche Volkswirtschaft lahmzulegen, nicht länger
erträglich war. Da setzte der Verein für Sozialpolitik die Fort-
bildung des Arbeitsvertrages gleichzeitig mit den Kartellorgani-
sationen der Unternehmer auf die Tagesordnung seiner General-
versammlung. Abermals war ich Referent und verlangte eine Neu-
ordnung des Arbeitsverhältnisses auf Grundlage des kollektiven
Arbeitsvertrages sowie, daß die in diesem festgesetzten Arbeits-
bedingungen für alle in einem Gewerbe Tätigen als rechts-
verbindlich anerkannt würden. Dabei benutzte ich die Gelegenheit,
die systematische Untergrabung aller den Arbeitern durch die Ge-
werbeordnung zuerkannten Freiheiten durch die sogenannten
Wohlfahrtseinrichtungen an der Hand von deren Statuten unan-
fechtbar klarzulegen. Gerade die gleichzeitige Behandlung nicht nur
der Duldung, sondern geradezu vielfachen Förderung der Unter-
nehmerkartelle durch Gesetzgebung und Verwaltung und der unter
wohlwollendem Verhalten der Regierung stattfindenden Ver-
kümmerung der durchaus analogen Arbeiterkoalitionen durch eben-
dieselben Arbeitgeber, machte die Darlegung um so eindrucksvoller.
Die Mannheimer Versammlung von 1905, auf der dies verhandelt

29