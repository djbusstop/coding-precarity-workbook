﻿der Schwachen zuzuweisen. Daher auch sein Buch über das deutsche
Kleingewerbe, wenn auch im ganzen gewerbefreiheitlich gesinnt,
mancherlei Maßnahmen, die Gesetzgebung und Verwaltung in der
Zeit des Überganges vom Klein- zum Großbetrieb getroffen hatten,
freundlicher beurteilte als die führenden Geister des Volkswirtschaft-
lichen Kongresses. Sein Buch hat daher zwar Bismarcks Aner-
kennung gefunden; als aber um dieselbe Zeit Gustav Schoenberg,
einer der Legatare Ferdinand Lassalles, in einer akademischen
Antrittsrede im Interesse der Arbeiter weitgehende Forderungen
staatlichen Eingreifens stellte, gab dies H. B. Oppenheim den Anlaß,
in der Berliner Nationalzeitung einen denunziatorischen Artikel
gegen den „Kathedersozialismus" zu schreiben.

Ich war nicht in diesem Artikel genannt; aber als er erschien,
hielt ich mich für in Ehren verpflichtet, darauf zu antworten. Ich
hatte mich kurz zuvor in Berlin habilitiert. Ordinarius der National-
ökonomie war zwar schon damals der kurz vorher nach Berlin be-
rufene Adolph Wagner. Aber noch hatte er sein sozialpolitisches
Herz nicht entdeckt. Abgesehen von Werken über Bankwesen, Finanz-
wissenschaft und Statistik hatte er nur eine Broschüre geschrieben
gegen den Beschluß der Internationale, das private Grundeigentum
abzuschaffen; er galt, wenn nicht als Manchestermann, so doch als
sozialpolitisch sehr zweifelhaft. Ich erschien also als der einzige
Vertreter der angegriffenen Lehre an der Berliner Universität.
Aber welche Erfahrungen mußte ich mit meiner Erwiderung machen!
Es fand sich in ganz Berlin keine Zeitung, die bereit gewesen wäre,
sie abzudrucken. Schließlich hat ihr Julius Eckardt im „Hamburgischen
Correspondenten" Aufnahme gewährt. Von da wurde sie dann von
Engel in die Zeitschrift des preußischen Statistischen Bureaus
übernommen.

15