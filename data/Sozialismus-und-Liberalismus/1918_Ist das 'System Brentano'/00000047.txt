﻿Auf dem Wege der vom Edlen von Braun vorgeschlagenen Ver-
längerung des Getreidemehls durch Kartoffelmehl ist also die aus-
reichende Ernährung des deutschen Volkes nicht zu erreichen, auch
wenn das Kriegsernährungsamt den Kopfanteil an diesem ver-
längerten Mehl nicht auf nur 200 §, vom 16. Juni ab sogar auf
nur 160 g täglich herabgesetzt hätte.

Daß wir das, was das Vieh vor dem Kriege an Brotgetreide
erhalten hat, durch die heimische Produktion nicht zu ersetzen ver-
mögen, wird in der Braunschen Broschüre selbst zugegeben, und
ebenso unmöglich ist es gewesen, das, was wir früher an Fleisch-
und Milchmengen unter Zuhilfenahme ausländischer Futtermittel
erzeugt haben, mit inländischen Futtermitteln allein zu erzeugen.
Wie Kuczynski und Zuns nachgewiesen Habens, betrug schon, als
unsere Ernteerträge noch groß waren, unser Defizit beim Roh-
protein zo%, beim Stärkewert 18%; seit unsere Ernteerträge ge-
sunken find, beträgt es noch viel mehr. Es ist daher ausgeschlossen,
für den Ausfall an Mehl Ersatz in Fleisch zu geben. Desgleichen
fühlt jeder Haushalt, daß die Versicherung des Edlen von Braun,
unser Milchbedarf sei nicht gefährdet, sich leider nicht bewahrheitet
hat, und ebenso ist seine Prophezeiung, daß es uns an Getreide-
produkten nicht fehlen werde, weil wir Graupen, Gries, Grütze usw.
mehr aus- als einführten, kläglich Lügen gestraft worden. Aber
freilich von jemand, der so wenig rechnen kann, daß er schrieb,
100 280 Tonnen zu 20000 Eiern ergäben 20 Milliarden Stet;* 2)
und das Schlachtgewicht der eingeführten Rinder und Schweine

1)	Kuczynski und Zuns, Deutschlands Nahrungs- und Futtermittel.
Allg. Statist. Archiv, Band y. S. 188.

2)	Braun, Kann Deutschland durch Hunger besiegt werden? München 1914.
S. 22.

47