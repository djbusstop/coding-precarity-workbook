﻿gönnen; nach seiner Auffassung war Preußen groß geworden,
indem der König von Preußen allzeit ein roi des gueux gewesen
sei; in diesem Geiste sollten die deutschen Beamten erzogen werden;
er wollte aus dem Verein für Sozialpolitik als Gegenstück zum
deutschen Juristentag einen Tag für Verwaltungsbeamte machen.
Es lag in der Natur der deutschen Verhältnisse, daß Schmoller mit
dieser seiner Auffassung durchdrang; war doch der einzige Resonnanz-
bodrn, den wir, da die Arbeiter als Sozialdemokraten uns feindlich
gegenüberstanden und das Bürgertum selbstverständlich nichts von
uns wissen wollte, für unsere Bestrebungen finden konnten, die
Beamtenschaft. In einer Rede über „die soziale Frage und den
preußischen Staat" hat Schmoller seinen Grundgedanken eindringlich
dargelegt. Nun aber erhob sich aus der Mitte derer, welche die
Einladung zur Eisenacher Versammlung mitunterschrieben hatten,
Heinrich von Treitschke und schrieb seine Aufsätze über den „Sozialis-
mus und seine Gönner", welche nächst dem gegen mich gerichteten
Buche Ludwig Bambergers über die Arbeiterfrage das Arsenal
bilden sollten, in dem unsere sonst recht geistesarmen Gegner froh-
lockend die Waffen fanden. Schmoller antwortete in einem Send-
schreiben, das alle Zeit ein Merkstein in der Geschichte der deutschen
Staatswissenschafien bilden wird; und nur kurze drei Jahre darauf
erlebte er den Triumph, daß der Grundgedanke eben jenes Vor-
trages, um dessentwillen ihm Treitschke den Fehdehandschuh hin-
geworfen hatte, von der deutschen Reichspolitik aufgenommen
wurde. Nach den Attentaten auf Kaiser Wilhelm begann jene
Sozialpolitik, welche dem, was man als das Berechtigte in den
Arbeiterbestrebungen anerkennen wollte, von Staats wegen zu
genügen bereit war.

Nunmehr war der äußere Erfolg des Vereins für Sozialpolitik
a*	19