﻿mit einer Pariastellung zufriedengäbe wie die, in welche es jene
Verträge der Engländer Herabdrücken würden. Wie ich dar,
getan habe, sieht sich ein Volk, das darauf aus sein muß, alles, was
es braucht, selbst zu erzeugen, notwendig zum Kriege getrieben.
Ist es den Engländern und ihren Bundesgenossen, wie sie behaupten,
ehrlich um einen Frieden zu tun, der Dauer verspricht, so ist das
erste Erfordernis, daß unsere Feinde auf alle jene Verträge ver-
zichten, welche die Deutschen auf die Dauer von der unmittelbaren
Verfügung über die ihnen nötigen Rohstoffe auszuschließen be-
stimmt sind. Es würde ein eitler Vorwand sein, daß es dem Staate
unmöglich sei, auf ihre Auflösung hinzuwirken, eben weil es sich
um rechtsverbindliche Abmachungen unter Privaten handle; wie
die Privaten jene Verträge nicht ohne Fühlung mit der Staats-
leitung abgeschlossen haben, vermag diese auch einen Druck auf ihre
Lösung auszuüben. Verstecken sich die Engländer aber hinter diesen
Vorwand, halten sie jene uns ausschließenden Privatverträge auf-
recht, so wird dies den Keim legen zu neuen noch weit furchtbareren
Kriegen. Es wird dies die von unseren Feinden angeblich erstrebte
Dauer des Friedens ausschließen. Und dasselbe würde der Fall
sein, wollte das siegreiche Deutschland die jährliche Lieferung einer
bestimmten Menge solcher Rohstoffe im Frieden sich ausbedingen;
denn bei jeder Einstellung der ausbedungenen Leistungen wäre der
neue Krieg da.

Aber verlangen wir von unseren Feinden den Verzicht auf jene
uns ausschließenden Verträge, so setzt ein solcher Verzicht selbst-
verständlich eine Gegenleistung voraus. Wir müssen, um von unseren
heutigen Gegnern, was wir brauchen, zu erlangen, auch unsererseits
die aggressive Schutzzollpolitik aufgeben, der wir in den letzten
Jahrzehnten gehuldigt haben. In seiner Beantwortung der

80