﻿schiebung des internationalen Gleichgewichts bedrohte die günstige
Handelsbilanz gegenüber einem fremden Staate und eine un-
günstige Handelsbilanz diesem gegenüber drohte ihm auch politisch
das Übergewicht zu geben. So vermischte sich das Streben nach
Wahrung des politischen Gleichgewichts mit dem nach Sicherung
einer günstigen Handelsbilanz. Die eine erschien als die Vorbei
dingung des anderen und umgekehrt.

Der auswärtige Handel hat sich also Tausende von Jahren, nach-
dem er als jüngerer Bruder des Krieges entstanden war, intellektuell
und moralisch auf derselben Stufe wie zur Zeit seiner Entstehung
befunden. Noch immer galt der Gewinn des einen nur möglich
auf Kosten eines verlierenden anderen. Noch immer galt Über-
vorteilung des Mitkontrahenten als das Ziel. Und wo der andere
nicht willig sich fügte, rief der jüngere Bruder nur zu häufig seinen
älteren großen Bruder zu Hilfe, um einen überlegenen Kon-
kurrenten zu schädigen. Groß ist die Fahl der Kriege, die geführt
wurden, sowohl um Kolonien zu erwerben, als auch um vorteilhafte
Handelsverträge zu erlangen oder den Handel eines Konkurrenten
zu schädigen. Ganz besonders gilt dies für England. Es hat seit
dem 16. Jahrhundert fast ausschließlich Handelskriege geführt und
die ausschließliche Seeherrschaft erstrebt zum Zweck der Schädigung
seiner Konkurrenten, und zwar nicht bloß der Kriegführenden,
sondern auch der neutralen. Daher die bitteren Klagen über seine
Gewalttätigkeit seitens der Zeitgenossen. Die Geschichte der Spanier,
der Holländer, der Franzosen, der Hamburger, der Skandinaven,
der Amerikaner weiß davon zu erzählen.

So war die Wirklichkeit. Während die Gewalttätigkeit der
merkantilisiischen Staatsmänner wahre Orgien feierte, war aber
seit der Wiedergeburt der Wissenschaften eine neue Auffassung des

?6