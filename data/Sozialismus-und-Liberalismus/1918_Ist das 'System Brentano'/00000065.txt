﻿der Gewinn des einen nur möglich sei, wenn ein anderer verliert,
und daß es dementsprechend beim Abschluß eines Handelsvertrages
darauf ankomme, daß ein Land vom anderen mehr erlange, als
es ihm gibt.

Die schutzzöllnerisch gerichteten Kolonien versäumten nicht, das
Wiedererwachen von schutzzöllnerischen Bestrebungen im Mutter-
land im Sinne ihrer eigenen auszunutzen. Hofmeyer trat 1887
für die Gründung eines britisch-kolonialen Zollvereins mit Vor-
zugsbehandlung der Kolonien auf dem Markte des Mutterlands
und umgekehrt ein. Der Handelskammerkongreß des ganzen bri-
tischen Reiches von 1892 hat das Projekt zwar verworfen; aber
er hat sich doch für eine wirtschaftliche Annäherung von Kolonien
und Mutterland ausgesprochen, und bald trat dieses Streben in
der Forderung nach hohen Zöllen auf die nichtbritische Einfuhr
hervor. Das hat sich insbesondere gegen Deutschland gerichtet.
Man hatte den englisch-deutschen Handelsvertrag bisher so aus-
gelegt, daß Deutschland auf Grund desselben genau so wie England
von dessen Kolonien behandelt werde. Das wollten die Kolonien
angesichts der deutschen Zollerhöhungen nicht länger dulden. Sie
wollten mit Gegenmaßregeln darauf antworten und begannen zu
erwägen, ob sie vermöge ihrer handelspolitischen Selbständigkeit
nicht auch zum Abschluß selbständiger Handelsverträge mit fremden
Ländern berechtigt seien.

Da bemächtigte sich Joseph Chamberlain dieser Jdeenströmungen,
sowohl der heimischen fair trade als auch der kolonialen, und
machte sie zur Grundlage seiner imperialistischen Agitation. 1896
wurde er Kolonialminister. 1897 hielt er seine große Rede über die
großen Weltreiche. Die Erde sei bestimmt, unter ihre Herrschaft
verteilt zu werden. Wenn England nicht dem Schicksal der kleinen

65

;