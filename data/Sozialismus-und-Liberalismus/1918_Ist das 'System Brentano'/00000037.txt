﻿werden auf die Stellung des Redakteurs jener vom Freiherrn von
Stumm ausgehaltenen Zeitung, von dem er verlangt hat, daß er ein,
schwenke wie ein Feldwebel, oder um einen vornehmeren Vergleich zu
wählen, auf die Rolle des Chors in der antiken Tragödie. Der Leser
erinnere sich an eine der berühmtesten Tragödien. Da ist Kreon, der
soeben die Berechtigung seines Einschreitens gegen Antigone in län-
gerer Rede auseinandergesetzt hat, und alsbald fällt der Chor ein:
„Uns scheint, wofern das Alter nicht den Sinn betrügt.

Daß du mit Einsicht sprachest, was wir angehört";
aber nachdem Hämon dem Kreon geantwortet hat, fährt der Chor fort:
„O Herrscher! Billig mußt du, sagt er Treffendes,

Ihn hören, du den Vater: beide spracht ihr gut."

Oder — wenn Antigone, um lebendig begraben zu werden, weh,
klagend abgeführt wird, ruft der Chor ihr nach:

„An der Herrschermacht
Zu freveln, stehet nimmer frei;

Gestürzt hat dich der eigene Starrsinn."

Nachdem aber die Versündigung eben der Herrschermacht Kreons
an Antigone dazu geführt hat, daß sich Kreons Sohn Hämon neben
Antigones Leiche erhängt, bekennt sich der Chor zu eben der Weis,
heit, um derentwillen Antigone in den Tod ging:

„Am Göttlichen darf
Nie freveln der Mensch!

Großsprecherisch Wort

Der Vornehmen fühlt den gewaltigen Schlag

Der bestrafenden Hand

Und lehret im Alter die Weisheit."

Ich habe schon 1896 die Auffassung, daß sich die Volkswirtschafts,
lehre dieses zittrige Schwanken und mattherzige Nachhinken zum

37