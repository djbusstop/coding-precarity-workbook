﻿Über alten und neuen Merkantilismus

Im Ausschuß der bayerischen Reichsratskammer wurde am n.
und i2. April der Kultushaushalt berate«. Nach den Zeitungen
ist dabei Graf von Soden-Frauenhofen auf die Wiederbesetzung
meines Lehrstuhls zu sprechen gekommen und hat erklärt:

„Ich lege den allergrößten Wert darauf, in welcher Weise die
Neubesetzung erfolgt: Wenn der Krieg eine Lehre gegeben hat, so
ist es die, daß es als eine Notwendigkeit erscheint, das Schutzzoll-
system beizubehalten; diese Professur ist daher mit einem Herrn
zu besetzen, der ein Anhänger dieses Prinzips ist, nicht aber der
Freihandelslehre. Bei aller Anerkennung der Freiheit der Wissen-
schaft ist es mehr als beklagenswert, wenn der Unterricht an den
Universitäten den Staatsbeamten in einer anderen Richtung ge-
geben würde, als die Gesetzgebung ihren Weg geht und voraus-
sichtlich auch in der nächsten Zukunft gehen wird. Der Wunsch ist
daher berechtigt, daß eine Auswahl in der Richtung erfolgt, daß ein
Schutzzöllner diese Professur erhält."

Einst sind auch die deutschen Landwirte Freihändler gewesen —
auch Herr von Soden. Noch 1876, als die deutschen Eisenindustriellen
für eine Hinausschiebung des Fortfalls der Essenzölle über den

1.	Januar 1877 agitierten, erklärte das Statut der agrarischen
Steuer- und Wirtschaftsreformer § 3: „Auf der Grundlage des
Freihandels stehend, sind wir Gegner der Schutzzölle", und die
Eisenzölle kamen zu Fall. Bei der Freiheit der Wissenschaft, wie
Graf Soden sie meint, hätte also bis 1878 der Professor der Volks-
wirtschaftslehre Freihändler sein müssen, um nach der Veröffent-
lichung des Schreibens des Fürsten Bismarck vom 15. Dezember 1878
umzufallen. Danach soll die Volkswirtschaftslehre herabgedrückt

36