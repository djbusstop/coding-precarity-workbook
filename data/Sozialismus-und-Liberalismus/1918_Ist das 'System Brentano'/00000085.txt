﻿im Inland weiterbearbeitet wurden. Unsere Einfuhr diente also
überwiegend der nationalen Produktion, und zwar ganz ebenso der
Landwirtschaft wie der Industrie. Selbst der Wirtschaftszweig, der
heute vor allem nach volkswirtschaftlicher Autarkie schreit, unsere
Landwirtschaft, ist nur eine Teilwirtschaft. Ich habe die amtlichen
Angaben über die Rohstoffe, Maschinen und Arbeitskräfte bei-
gefügt, welche die deutsche Landwirtschaft vor dem Kriege aus dem
Ausland bezogen hat; ich habe ihnen die Angaben über die für den
Industriebetrieb unentbehrlichen fremden Rohstoffe folgen lassen.
Diese Zahlen zeigen, in welchem Maße eine sich selbst genügende
deutsche Volkswirtschaft auch vom Standpunkt der Produktion ein
Ding der Unmöglichkeit ist, wenn Deutschland nicht auf seine wirt-
schaftliche und politische Stellung inmitten der Völker verzichten
will. Oie Waren, die wir aus dem Ausland eingeführt haben,
haben wir aber von diesem nicht umsonst erhalten; wir haben eigene
Produkte dafür geben müssen. Ich habe in der Beilage auch die
amtlichen Angaben wiedergegeben über die Waren, die wir zu ihrer
Bezahlung ausgeführt haben. Dies sind zu 63,3 v. H. Fabrikate,
zu 11,3 v. H. halbfertige Waren, also zu nahezu drei Viertel des
ausgeführten Wertes heimische Jndustrieerzeugnisse gewesen. Eine
weitere Tabelle gibt an, aus welchen Erzeugnissen deutscher Arbeit

kommen". Kein Zweifel, daß ich,der Meinung bin, daß unsere „bewährte Wirt-
schaftspolitik", an der mitgearbeitet zu haben mein verehrter Kollege sich rühmt,
den „Konsumenten" viel zu wenig berücksichtigt hat. Aber stets war mein Haupt-
einwand, daß die Schutzzollpolitik unsere Produktion selbst schädige, und gerade
unter meinen Argumenten in dem gedachten Vortrage steht dieses im Vorder-
grund. (Siehe oben S. 7g ff.) Desgleichen zeigt seine gegen meine Bezeichnung
der deutschen Schutzzollpolitik (s. oben S. 67 ff.) als eine Aggressivpolitik ge-
richtete Kritik, daß er nicht weiß, warum alle Welt, selbstverständlich außer den
deutschen Schutzzöllnern, diese Bezeichnung gebraucht. Wenn er meine Aus-
führungen „Dialektik" nennt, wie soll ich seine Kritik derselben nennen?

85