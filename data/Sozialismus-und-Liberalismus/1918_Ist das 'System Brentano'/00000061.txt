﻿auch wirtschaftlich selbständig werden. Die ihnen fehlenden i«c
Entwicklung nötigen Kapitalien suchten sie durch Zölle, welche
diesen höhere Verzinsung als im Mutterland in Aussicht stellten,
anzulocken. Diese Zölle erhoben sie ebenso von der Einfuhr aus
dem Mutterlande wie aus anderen Ländern. Die Schriften von
James Anthony Froude, Oceuna und The English in the West-
indies geben ein anschauliches Bild von der Denkweise der Eng-
länder in den Kolonien. Sie ist völlig verschieden von der zu Hause.
Hier stand die Entwicklung unter dem Einfluß der liberalen Strö-
mung, welche das gesamte innere Leben Englands seit Beginn des
iy. Jahrhunderts umgestaltet hat. In den Kolonien waren die
Engländer naturgemäß Verehrer der Gewalt, deren Herrschaft sie
daheim aus dem politischen Leben zu verbannen bemüht waren;
denn ihre Herrschaft beruhte in den Kolonien ausschließlich auf der
Gewalt. Froude gibt Zeugnis von dem bitteren Gefühl, das die
englischen Kolonien gegen die Liberalen, besonders gegen Gladstone,
erfüllte, wo immer die Liberalen die Anwendung von Gewalt gegen
einen äußeren Feind verweigerten, und wie sie in Westindien voll
Sehnsucht nach Amerika blickten, das ihren Zucker schützen würde,
während ihn das Mutterland der durch Ausfuhrprämien unter-
stützten Konkurrenz des Rübenzuckers des kontinentalen Europas
preisgab.

Aber noch war die schutzzöllnerische Stimmung in den Kolonien
nicht gefährlich. Sie wurde es erst mit dem Wiedererwachen schutz-
zöllnerischer Tendenzen in Europa. Von durchschlagender Bedeu-
tung war hier der Krach von 1873 und die Überschwemmung der
europäischen Märkte mit amerikanischem, russischem und indischem
Getreide, die nach 1876 eintrat. Seit dieser Zeit lebt der europäische
Kontinent in einer Ära des wiedererwachten Merkantilismus.

61