﻿wir kein Referat übertragen; der Antisemitismus, an den sich in
seiner auf der Versammlung der Inneren Mission gegen H. B.
Oppenheim gehaltenen Verteidigungsrede starke Anklänge fanden,
würde unser ganzes Beginnen bedroht haben. Außerdem ließ seine
Methode, ähnlich wie die der abstrakten Volkswirte des Volkswirt-
schaftlichen Kongresses, aus allgemeinen Prinzipien, wie er sich aus-
zudrücken pflegte, „immer noch weitergehende Lehren" abzuleiten,
frühzeitig einen Gegensatz zwischen seinen und unseren Bestrebungen
hervortreten.

Wir hatten eine schlechte Presse. Das war nicht zu verwundern;
denn diese befand sich ja in der Hand unserer Gegner. Aber wir
ließen uns nicht einschüchtern, und auf die erste Versammlung folgte
eine zweite, auf welcher die Gründung des Vereins für Sozial-
politik beschlossen wurde. Dabei machten allerlei rivalisierende
Tendenzen sich geltend; einerseits versuchte es der Kreuzzeitungs-
Wagener und sein Adjutant Rudolf Meyer, die Neubildung in den
Dienst der Reaktion zu stellen. Sie trafen aber nirgends auf Gegen-
liebe, außer höchstens bei Adolph Wagner, der schon damals den
Übergang von den Nationalliberalen zu den Konservativen suchte.
Andererseits war unser damaliger Vorsitzender Gneist bemüht,
unserer ganzen entstehenden Bewegung die Spitze abzubrechen und
sie ins Lager des Volkswirtschaftlichen Kongresses zurückzueskamo-
tieren. Im übrigen aber war die Frage, ob aus dem Verein ein
Agitationsverein werden sollte oder eine Organisation zur sozial-
politischen Drillung von Verwaltungsbeamten. Das erstere
schwebte mir vor; denn mir waren die Methoden, wie sie in Eng-
land zur Eroberung der öffentlichen Meinung zur Anwendung
kamen, die geläufigen. Schmoller dagegen hatte bereits mit seinen
Studien über die Entwicklung der preußischen Verwaltung be-

18