﻿Weltmarktpreis plus Zoll zu steigern. Das hat den leistungsfähigsten
Betrieben gestattet, mit Vorteil billiger ans Ausland als im Inland
zu verkaufen. Sie wurden also dadurch, daß sie dem heimischen
Verbraucher Preise weit über ihre Produktionskosten abnahmen,
instand gesetzt, ihren Absatz zum Nachteil ihrer auswärtigen Kon-
kurrenten zu erweitern. Ganz ebenso wirkten die Schutzzölle und
die durch sie zusammengehaltenen Trusts in den Vereinigten Staaten
von Amerika.

Das ist das sogenannte Dumping, das in England so viel Er-
bitterung hervorgerufen hat. Sie ist wohl begreiflich. Das einzig
Bemerkenswerte ist, daß, während sich diese früher wesentlich gegen
die Vereinigten Staaten, die das System inauguriert haben, ge-
richtet hatte, sie ihre Klagen wesentlich gegen Deutschland richtete,
seit das Aggressivzollsystem in diesem zur Ausbildung kam. Durch
dieses habe der Freihandel seine Rechtfertigung völlig verloren.
Denn er suche sie darin, daß bei ihm jede Ware da produziert werde,
wo sich die von Natur für ihre Produktion günstigsten Bedingungen
fänden. Infolge des Dumping aber, sagten die englischen fair
trader, werde die englische Wirtschaftsentwicklung nicht länger
durch die natürlichen Bedingungen bestimmt. Nicht mehr Engländer
seien es, welche fortan bestimmten, was in England produziert
werde, sondern fremde Interessenten. Der Engländer sei nicht
länger Herr in seinem Hause; er könne sich nur mehr in den Be-
schäftigungen betätigen, welche seine Nachbarn ihm zuwiesen. Er
müsse die Produktionszweige aufgeben, für die sein Land wohl
geeignet sei, bloß weil Ausländer diese Produktionszweige bei sich
unnatürlich förderten und den britischen Produkten den Absatz auf
ihrem Markte versperrten. Dabei wurde nicht bestritten, daß gewisse
Zweige der Weiterverarbeitung vom Dumping Vorteil zögen; so

68