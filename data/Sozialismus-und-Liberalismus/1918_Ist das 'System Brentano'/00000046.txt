﻿bleibe, 2. der Weizen teilweise durch Roggen, Gerste und Kartoffeln
ersetzt werde und 3. eine möglichst sparsame Verwendung durch
die höchstmögliche Mehlausbeute bei der Vermahlung stattfinde.
Was an Vorschriften zur Erfüllung dieser Forderungen erlassen
werden konnte, ist erlassen worden; wenn trotzdem namentlich die
erste nur sehr unvollkommen erfüllt worden ist, so liegt es bloß an
den allzu viel Rücksichten, welche das Kriegsernährungsamt, dessen
Vorstandsmitglied Edler von Braun ist, auf seine agrarischen
Freunde nimmt. Jedenfalls hat das, was geschehen ist, nicht aus-
gereicht, um den Nahrungsbedarf der deutschen Bevölkerung aus
der Jnlandsernte ausreichend zu decken, und es würde dazu auch
nicht ausgereicht haben, selbst wenn unsere Crnteerträge nicht zurück-
gegangen wären. Nehmen wir z. B. den vom Edlen von Braun
geforderten Ersatz von Weizenmehl durch Roggenmehl, Kartoffeln
und Kartoffelmehl. Wie er selbst zugibt, enthalten Weizen 9%,
Roggen 8,7%, Kartoffeln dagegen nur 0,1% reines Eiweiß und
100 kg Weizen 116,8, 100 kg Roggen 114,8, dagegen 100 kg Kar-
toffeln nur 19,5 Wirkungseinheiten. Um 100 g Eiweiß in der Form
von Kartoffeln in den Magen einzuführen, müßten wir 5 kg Kar-
toffeln verzehren; um aber 100 g Eiweiß zur Resorption gelangen
zu lassen, müßten wir 7 kg Kartoffeln bewältigen. Nun konnte
aber die Versuchsperson Rubners, den Edler von Braun ja als
Autorität gelten läßt, „ein kräftiger Soldat, welcher in der baye-
rischen Oberpfalz zu Hause, an reichliche Kartoffelaufnahme ge-
wöhnt war", nicht mehr als 3—3V2 kg bewältigen H, obgleich ihm
die einförmige Nahrung in der mannigfaltigsten Form zubereitet
verabreicht wurde, so daß der Mann von dem Eiweißvorrate seiner
Gewebe zehrte, d. h. einem langsamen Hungerkode entgegenging,
r) Max Rubner, Zeitschr. f. Biol. Bd. i;. S. 146.

46