﻿schlechter als andere Länder behandeln k-nnen. Gewisse deutsche
Sonderivteressen hegten indessen den Wunsch, dies doch zu tun.
Das Mittel dazu bot sich in einer größeren Spezialisierung einzelner
Positionen des Zolltarifs. Man setzte für Spezialitäten einer Güter-
art, wie sie nur von dem Land, das man begünstigen wollte, pro-
duziert wurden, niedrigere Zölle fest als für die ganze Güterart.
Da Frankreich die niedriger veranlagten Spezialitäten nicht liefern
konnte, war es trotz der Meistbegünstigungsklausel von dem anderen
Ländern gewährten Vorteil ausgeschlossen, und Graf Posadowsky
hatte es im Reichstag sogar rühmend hervorgehoben, daß es
möglich sei, Qsterreich-Ungarn, Italien, Rußland Tarifkonzessionen
zu machen, ohne daß sie Frankreich zugut kämen. Ein Brief Luzzattis
vom 8. März 190z, der diesen Ausspruch kommentierte, hat großes
Aussehen gemacht. Wenn die Zahl der Umgehungen dieses Artikels
auf dem Wege der Spezialisierung auch gering war, so wurde
Posadowskys Ausspruch doch eine Waffe in den Händen der fran-
zösischen Schutzzöllner. Nun schien der böse Wille Deutschlands in
der Durchführung der von ihm abgeschlossenen Verträge erwiesen.

Endlich wurde die internationale Handelseifersucht durch das sehr
berechtigte Streben Deutschlands nach wirtschaftlicher Expansion
in anderen Erdteilen verschärft! Von den ersten Anfängen eines
deutschen Kolonialbesitzes an sind die englischen Kolonien der Ent-
stehung eines deutschen Kolonialreichs unfreundlich gegenüber-
gestanden. Selbst die so bescheidene Niederlassung in Lüderitzland
zu Beginn der achtziger Jahre hat die kapländische Regierung den
Deutschen mißgönnt. Queensland, ja ganz Australien erhoben
bittere Vorwürfe gegen das britische Mutterland, weil es 1885 die
Niederlassung der Deutschen auf Neuguinea zuließ. Als Deutschland
die Karolinen, die Palapinseln, die Marianen und die Samoa-

70