﻿geordnete Held in der Zweiten Kammer des bayerischen Landtags
gerufen. Meine Professur sei mit einem Herrn zu besetzen, der ein
Anhänger des Schutzzollsystems, nicht aber der Freihandelslehre sei,
hat Graf Soden im Reichsrat verlangt.

Demgegenüber behaupte ich: Der Krieg hat den Beweis der
Richtigkeit meiner Anschauungen erbracht. Durch den Katheder,
sozialismus ist das Deutsche Reich vor dem Zusammenbruch be,
wahrt worden, der ihm ohne die von den Kathedersozialisten befür-
wortete Politik vom Abfall der Arbeiterklasse gleich bei Ausbruch
des Krieges gedroht hätte; und wäre das in den sechziger Jahren
zur Herrschaft gelangte Freihandelssystem zur vollen Durchführung
gekommen, statt Ende der siebziger Jahre verlassen zu werden, so
wäre der Krieg, der so namenloses Unglück über die Welt gebracht
hat, nicht gekommen oder hätte wenigstens nicht den die ganze Erde -
umspannenden Umfang angenommen; und ohne Rückkehr zu den
damals verlassenen Prinzipien wird die Welt nie zu einem Frieden
gelangen, der Dauer verspricht.

All das will ich dartun. Zuerst will ich über den Kathedersozialis,
mus sprechen und über die Erfolge, die er erzielt hat. Dann will
ich den Nachweis führen, daß die Rückkehr zum Merkantilismus die
Ursache ist, daß die ganze Welt in den Krieg verflochten worden
ist, und daß keine Aussicht auf einen dauernden Frieden gegeben
ist, außer bei voller Verwirklichung der Prinzipien des Freihandels.
Eine statistische Beilage über die deutsche Einfuhr und Ausfuhr
soll denjenigen, welche noch immer von einer sich selbst genügenden
deutschen Volkswirtschaft träumen, die Unerreichbarkeit ihres Zieles
zeigen, selbst wenn „Mitteleuropa" verwirklicht werden sollte.

München, Anfang Mai 1918.	L. Brentano.

10