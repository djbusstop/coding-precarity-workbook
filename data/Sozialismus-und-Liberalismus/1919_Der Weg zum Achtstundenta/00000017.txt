﻿des Wortes. Sie pflegen in ihrer freien Zeit ihren Gartenanteil oder
gehen aus mit Frau und Kindern oder unternehmen Spazierfahrten
auf dem Fahrrad.»

In Frankreich ergab die Einführung der Achtstunden- an Stelle
der Zwölfstundenschicht in den Eisenhütten der Franche-Comte die-
selben Resultate; jede Achtstundenschicht erzeugt soviel wie früher
jede der beiden Zwölfstundenschichten, ohne Steigerung der Regie-
kosten, und bei gleichen Stücklöhnen. «Das Arbeitsverdienen ist
dasselbe seit zehn Jahren. Aber vier Stunden sind gewonnen! Statt
um Mitternacht abgerackert aus der Fabrik zu gehen, verlässt man
sie um 4 Uhr nachmittags oder um 8 Uhr abends. Unsere Kräfte
sind nicht erschöpft. Man geht seinen Garten bestellen; manche
pflegen ihre Rebgärten. Andere gehen fischen oder auf dem Ge-
birge jagen.»3)

Die deutsche Schwerindustrie leistete der Einführung der Acht-
stun Jenschicht den stärksten Widerstand. Es wurde berechnet, dass
ihre Einführung in den Hüttenbetrieben eine Mehrleistung von
57,000 Arbeitern, eine Mehrausgabe von 85 Millionen Mark und eine
Verteuerung von 3—4% pro Tonne zur Folge hätte.2)

Zu den Industrien, die ihre normale Arbeitszeit am stärksten
durch Ueberstunden zu verlängern pflegen, gehört die Maschinen-
industrie. Diese Uebung der systematischen Ueberzeit ist in der
Maschinenindustrie Englands durch das Experiment der Salforder
'Werke von Malher und Platt bekämpft worden, die schon 1883
1200 Mann beschäftigten und in diesem Jahr vom Neun- zum Acht-
stundentage übergingen. Das Ergebnis war ein doppeltes: Steige-
rung der Produktion und Sinke n der versäumten Arbeitsstunden
von 2,46 auf 0,4%. Im Jahre 1889 ging auf Veranlassung des
Kriegsministers Campbell-Bannermann die Patronenfabrik und ein
Jahr später die Geschützfabrik von Woolwich zum Achtstundentag
über. Kein einziger Arbeiter brauchte deshalb angestellt zu werden;
die Arbeiterzahl dieser Werke betrug 16,000.

In Frankreich, das am x. September 1903 im Arsenal von
Tarbes vom Zehnstundentage zum Achtstundentag überging, wurde

*) „L’Ouvrier metallurgiste“ !<* mai 1906; bei Marcel Lecoq: La
journee de huit heures, 1907, S. 178 und Frangois Delaisi: La journee
de huit heures dans les forges du Jura; in: „Pages libres“, 28. August 1906;
bei Raoul Jay: La protection legale des travailleurs, 1910, S. 356.

2) Franz Wieher: Die Arbeitszeit in ununterbrochenen Betrieben der
Urosseisenindustrie, Duisburg 1912, S. 57.