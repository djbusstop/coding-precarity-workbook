﻿denschicht in Durham, ebenso in Northumberland und in den Erz-
gruben von Cleveland. In Deutschland wurde auf den fiskalischen
Kohlengruben der Achtstundentag im Jahre 1890 eingeführt. An-
fänglich mit Misserfolg; erst die Steigerung der Fördervorrich-
tungen führte zu Produktionssteigerungen. In Oesterreich teilte
einem Sozialstatistiker ein Kohlenwerk, das 1891 die Neunstunden-
schicht eingeführt und grössere Häuerleistungen erzielt hatte, diese
Ergebnisse mit, die zum Schrecken der grossen Kohlenmag-
naten in einem Handelskammerbericht erschienen. Die Techniker
wurden mobil gemacht, um die Tragweite der Ergebnisse in Fach-
zeitschriften zu bekämpfen. Es bedurfte erst eines grossen Aus-
standes mit allen seinen Schrecken, bis Oesterreich im Jahre 1901
zur gesetzlichen Neunstundenschicht in Kohlengruben unter Tage
überging. Damit war das Eis gebrochen; Belgien und Spanien
folgten diesem Beispiel, die Niederlande gingen zur 8j4-Stunden-
schicht über; Frankreich hat seit 29. Juni 1909 in Europa zuerst
die Achtstundenschicht eingeführt, Grossbritannien, Norwegen,
P'inland und Portugal schlossen die Reihe. In Deutschland war die
Gesetzgebung über Bergwerke den Einzelstaaten Vorbehalten. Daher
und aus der Rolle des Staats im Staate, die die Schwerindustrie
spielte, erklärt sich das Zurückbleiben seiner Gesetzgebung. In den
Vereinigten Staaten sind die Arbeitsverbände der Bergarbeiter über
die Hemmungen der Bundesverfassung hinweggeschritten. Durch
Tarifvertrag vom 5. Mai 1916 ist auf allen amerikanischen Kohlen-
gruben die Achtstundenschicht eingeführt worden.

Dreiviertel aller Kohle der Welt wird also heute in Achtstunden-
schichten gefördert.

Kohle ist der Nährstoff, Eisen und Stahl das Rückgrat der In-
dustrie. Wie steht es hier mit der Arbeitszeit? Hochöfen, Eisen-,
Hütten-, Stahl- und Walzwerke haben allerwärts von der Gesetz-
gebung einen Freibrief zu Zwölfstundenschichten erhalten, und ganz
ähnlich verhält es sich mit Glashütten. Mit Ueberzeit können hier
gesetzlich Arbeiter in Deutschland 16, Frankreich 14 Stunden be-
schäftigt werden. Die Ursache dieser Ausnahmestellung ist eine
doppelte: einmal die dominierende, auch für den Kriegsfall wichtige
Stellung der Unternehmungen, die Schonung ihrer Anlagekapita-
lien ; auf Seite der Arbeiter: die Massenverwendung ungelernter,
meist ost- und südeuropäischer unorganisierter Arbeitskräfte.