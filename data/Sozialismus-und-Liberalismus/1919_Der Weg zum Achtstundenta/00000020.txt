﻿'2ET

— i7 —

Die Firma Brunner Mond & Co. hat am 7. Februar 1895 über
die Erfolge berichtet: «Die Arbeitskosten pro Tonne der Produkte
waren jährlich zurückgegangen, und sie waren die gleichen wie zu
Beginn des Experiments im Jahre 1889. Verbesserungen des Pro-
duktionsprozesses waren während dieser Zeit in beträchtlichem
Masse tingeführt worden, aber die Firma hält sie doch nicht für
bedeutend genug, um jenes merkwürdige Resultat zu erklären. Sie
glaubt vielmehr, dasselbe zum grossen Teil darauf zurückführen zu
sollen, dass ihre Leute beständiger und intensiver arbeiten.» Sir John
Brunner sagt: «Das Aussehen der Arbeiter und besonders ihr Gang,
wenn sie die Fabrik nach Beendigung der Arbeit verlassen, hat sich
auffallend gehoben.» — Aber nicht minder hat die Stetigkeit ihrer
Arbeit zugenommen ... In der Fabrik erhalten die ständigen und
regelmässigen Arbeiter jährlich eine Woche Ferien mit vollem Lohn,
die sie preisgeben, wenn sie während des Jahres zehn Tage ohne
Erlaubnis fehlen. Ehe die Achtstundenschicht eingeführt war, be-
trug der Prozentsatz der chemischen Arbeiter, die sich diese Ferien
verdienten, 43 im Jahre 1888, er stieg im Jahre 1891 auf 78 und im
Jahre 1893 auf 92, während der Prozentsatz der andern Arbeiter 59
im Jahre 1888, 67 im Jahre 1891 und 76 im Jahre 1893 betrug.
Ausserdem musste unter dem alten System der Direktor am Morgen
nach dem Zahltage in der Fabrik sein, um Ersatz für die Fehlenden
zu schaffen und Betrunkene zu entfernen. Heute ist das kaum noch
nötig. «Die Arbeiter kommen regelmässig und nüchtern zu ihrer
Schicht.».......Allerdings gilt dieser Achtstundentag in den che-

mischen Fabriken für sieben Tage in der Woche und nur für die
chemischen Arbeiter, die Stücklohn erhalten, während die andern
Arbeiter, die nach Zeit gelöhnt werden, noch immer täglich neun
Stunden tätig sind.»1)

In der chemischen Industrie der Schweiz, die in Basel konzen-
triert ist, betrug die Arbeitszeit bis zum Jahre 1897 neun bis neun-
einhalb Stunden. Ein Lokalverband der Arbeiter wurde zu jener
Zeit bei den Fabriken vorstellig und erwirkte die Einführung einer
achte:nhalbstündigen Arbeitszeit: Der Wochenlohn betrug für er-
wachsene Männer 21 Fr. Im Jahre 1905 kam es zu einem Ausstande 3

3) John Rae: Nene Fortschritte der Achtstunden-Bewegung in
England, Brauns Archiv fiir Soziale Gesetzgebung und Statistik, 1898,
XII, S. 18 und 19.