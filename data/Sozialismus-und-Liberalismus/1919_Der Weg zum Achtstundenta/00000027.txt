﻿2 4

5.	Die Vorteile des Achtstundentages kommen zu gute: den
jugendlichen, deren Fortbildungsunterricht nach zehn- oder
neunstündiger Erwerbsarbeit wertlos wird. Daher schreiben
bereits eine gesetzliche Höchstarbeitszeit von acht Stunden
für die Jugendlichen unter 16 Jahren Bulgarien, 18 Einzel-
staaten der Union und sechs Stunden die Ver. Staaten von
Mexiko vor. Den Arbeiterinnen gewährt der Achtstundentag
die Möglichkeit, ihre Kinder zu betreuen und zur Verringe-
rung der Säuglingssterblichkeit beizutragen, die auf tausend
Neugeborene in Australien 72, in der Schweiz 123, in Italien
137, in Japan 150 betrug. Er führt zur bessern Pflege des
Hausstandes und der öffentlichen Interessen nach Erringung
des Wahlrechtes; den erwachsenen Männern gestattet er die
bessere Körper-, Landwirtschafts- und Geistespflege, die Er-
füllung ihrer Bürgerpflichten und die Vertiefung ihrer poli-
tischen Bildung.1)

*) Ueber die Arbeitszeit grosser Intellektueller gibt es leider unseres
Wissens keine besonderen Untersuchungen. Es ist bekannt, dass ein Alex,
von Humboldt^ 18 Stunden arbeiten konnte; aber solche Selbstüberarbei-
tung und Entbehrung des Schlafes führt auch bei einem Mommsen perio-
disch zu einem Nachlassen der Schaffenskraft. (L. M. Hartmann, Theodor
Mommsen, Gotha, 1908, S. 147.) Goethe zwang den widerstrebenden Körper,
dem Geiste zu folgen, aber er bekennt, in der Poesie Hessen sich ge-
wisse Dinge nicht zwingen und man müsse von guten Stunden erwarten,
was durch geistigen Willen nicht zu erreichen ist. (Eckermanns Ge-
spräche mit Goethe, 21. März 1830.) Balzac hat in 25 Jahren gegen 100
Werke geschrieben und starb als Fünfzigjähriger. Er arbeitete meist
nachts, oft Tag und Nacht, in der Regel 7—8 Stunden; «eine enorme
Ziffer, wenn man bedenkt, was eine wirkliche literarische Arbeits-
stunde bedeutet». (Emile Faguet, Balzac, 1912, p. 27.) Grosse Mathema-
tiker scheinen für die Ueberwindung des Arbeitswiderstandes beson-
deren Stimmungen zu unterliegen. Von Henri Poincare wird berichtet,
dass er nur zwei Stunden vormittags und zwei Stunden vor dem
Abendessen arbeitete und von 10 Uhr abends bis 7 Uhr morgens
schlief. (Dr. Toulouse, Henri Poincare, Paris, 1910, p. 40, 144.) Er nahm
volle intellektuelle Ferien. Bei fehlender Arbeitsstimmung verliess er die
Arbeit im Gegensatz zu Zola, der diesen Widerstand gewaltsam unter-
drückte. «Wenn geistige Arbeit sich nicht in Wärmeeinheiten messen
lässt und länger dauern'kann als Muskelarbeit, so steht nichtsdesto-
weniger fest, dass vom Standpunkt der Ermüdung nur ein Gradunter-
schied zwischen beiden Arten der Arbeit besteht und dass man auch die
geistige Ermüdung, die mehr oder weniger jede gewerbliche Verrichtung
begleitet, in Anschlag bringen muss.» (Jules Amar, Le Moteur humain,
Paris 1914, p. 590.)