﻿22

Im Handelsgewerbe der Schweiz haben seit , kurzer Zeit die
Eureauangesteilten zunächst den freien Samstagnachmittag und
sodann in den grossen Städten den Achtstundentag erworben.

Auf dem Gebiete des Verkehrswesens, vor allem auf dem der
Eisenbahnen herrscht in ganz Europa noch grosse Verschiedenheit
der Präsenz- und Dienstzeiten. Die Schweiz hat bekanntlich auf
diesem Gebiete im Jahr 1890 durch die Einschränkung der täglichen
Arbeitszeit auf 12 Stunden und die Gewährung einer Mindestruhe-
zeit von 8—10 Stunden die Initiative ergriffen. Seither sind die An-
forderungen an den Eisenbahndienst bedeutend gestiegen und das
Streben nach achtstündiger Arbeitszeit für das gesamte Arbeitsper-
sonal nicht zum Stillstände gekommen.

Diese Bestrebungen haben zum ersten Male im Jahre 1916 in
den Vereinigten Staaten von Amerika Erfolg gehabt. Angesichts
eines drohenden Landesstreiks und der Ablehnung eines Schieds-
gerichtes berief Präsident Wilson die Vertreter der Arbeiter und
der Eisenbahngesellschaften nach Washington und legte persönlich
die Ergebnisse der Beratung dem Senate mit dem Vorschlag vor, ein
Achtstundengesetz zu erlassen. Dieses Gesetz, «Adamson Act», ist am
5 September 1916 angenommen worden und steht seit 1. Januar
1917 in Kraft.1)

Die Gesamtziffer der Arbeiter und Unternehmungen, die bereits
im Achtstundenbetriebe stehen, ist uns nur für wenige Länder be-
kannt. Der Industriezensus der Vereinigten Staaten weist 625,652
Achtstundenarbeiter oder 7,9% aller Industriearbeiter auf. Von der
Viertelmillion Buch- und Zeitungsdruckern besitzen den Achtstun-
dentag 53,7%.2 *) Von den Nachbarländern der Schweiz liegen An-
gaben vor für Oesterreich, wo im Jahre 1907 8,8% der Fabrikarbei-
ter neun Stupiden und weniger arbeiteten (darunter 20% in Wien,
15,9% in Prag, 59,2% in Triest8) und für Oberitalien, das im Jahre
1909 im Inspektionskreis von Mailand 7,95, von Brescia 4,3% der
Arbeiter mit 9—9j4stündiger Arbeitszeit verzeichnete.4 *)

*) E. C. R o b b i n s, The Trainmen’s Eight Houfs Day in: Political
Science Quarterly, New York, September 1917, p. 412, und Bulletin des
intern, Arbeitsamtes, 1916, Bd. XV, S. 51.

2) Department of Commerce, Thirteenth Census, vol. VIII, Manufac-
tures General Report, Washington, 1913, p. 316.

s) Die Arbeitszeit der Fabrikarbeiter Oesterreichs. K. K. Arbeitssta-
tistisches Amt Wien, 1907, S. L. V.

4)	Rapporti sulla ispezione del lavoro. Ufficio del lavoro. Roma 1909.

Tabella n° 73.