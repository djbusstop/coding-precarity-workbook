﻿Was nun heute das Achtstundenland charakterisiert, lässt sich
kurz in folgenden Ziffern klarstellen:

1.	Der natürliche Zuwachs der Bevölkerung von 1861 bis 1915
betrug 2,9 Millionen, der Ueberschuss der Einwanderer über
die Auswanderer nur 500,000. Das Land hat also seinen na-
tionalen Charakter gewahrt.

2.	Die Volksvermehrung war nahezu doppelt so stark wie in
der Schweiz; ebenso die Heiratsfrequenz; die allgemeine
Sterblichkeit war um ein Drittel, die Tuberkulosesterblich-
keit um mehr als die Hälfte geringer.

Die Zahl der Fabrikarbeiter stieg von 1901 bis 1915 von
198,000 auf 321,000, fast genau wie in der Schweiz von
1895 bis 1911 (von 200,000 auf 329,000).

Die Zahl der Pferdekräfte der Motoren ist infolge der Aus-
nützung der Wasserkraft in der Schweiz bedeutend höher
(713,000) als in Australien (505,000).

Die Verteilung der Arbeiterschaft in Klein-, Mittel- und
Grossbetriebe (bis 20, 21 bis 100, über 100 Personen be-
schäftigende Anlagen) ergibt, dass von je 100 Arbeitern
sich im Jahre 19x1 befanden: in Kleinbetrieben in der
Schweiz 7, in Australien 28, in Mittelbetrieben in der
Schweiz 22, in Australien 34, in Grossbetrieben in der
Schweiz 71, in Australien 38.

6.	Auf den Kopf der Einwohner berechnet, betrug der Export-
wert Australiens L. st. 14 11 sh. 2 d., der der Schweiz
L. st. 14 10 sh. 11 d. oder fast gleichviel in beiden Ländern.
Die Mitgliederzahl der Arbeiterverbände ist von 1906 bis
1913 gestiegen: in Australien von 147,049 auf 497,925, in
der Schweiz von 68,535 auf 89,398.

Ende 1916 erhielt in Australien der erwachsene Vollarbeiter
10 Shillings per Tag von acht Stunden, die Vollarbeiterin
4)4 Shillings, ln den Grossstädten der Schweiz darf
man für zehnstündige Arbeitszeit einen durchschnittlichen
Tagesverdienst von 6 Fr. für Arbeiter und von 3)4 Fr. für
Arbeiterinnen auf demselben Zeitpunkt annehmen.1)

*) Leider sind, seit der von Fridolin Schüler im Jahre 1895 veranstal-
teten Lohnerhebung, zuverlässige Angaben für die Schweiz nicht vorhanden;
man pflegt zu wenig Sozialstatistik, den Schrittmesser der Wohlstands-
entwicklung. Die Angaben für Australien sind dem ausgezeichneten amtlichen
statistischen Jahrbuch des Statistikers G. H. Knibbs entnommen.



&