﻿9

f* *

!

<?

* •

i .

i »

9t

zeitig in allen Ländern und in allen Städten an einem bestimmten
Tage die Arbeiter an die öffentlichen Gewalten die Forderung rich-
ten, den Arbeitstag auf acht Stunden festzusetzen und die übrigen
Beschlüsse des internationalen Kongresses von Paris zur Ausfüh-
rung zu bringen.

In Anbetracht der Tatsache, dass eine solche Kundgebung be-
reits von dem Amerikanischen Arbeiterbund (Federation of Labor)
auf seinem im Dezember 1888 zu St. Louis abgehaltenen Kongress
für den i. Mai iScjo beschlossen worden ist, wird dieser Zeitpunkt
als Tag der internationalen Kundgebung angenommen.

Die Arbeiter der verschiedenen Nationen haben die Kund-
gebung in der Art und Weise, wie sie ihnen durch die Verhältnisse
ihres Landes vor geschrieben wird, ins Werk zu setzen.1)

Dieser neue Bastillesturm des Proletariats hat nun 28 Jahre ge-
dauert. Er hat schwere Opfer, blutige Ausstände und Massrege-
lungen gekostet. Sehen wir zu, ob und wie sich die Achtstunden-
bewegung ihrem Endziele nähert.

Vorerst eine persönliche Erinnerung. Ein Studienaufenthalt in
London bewirkte, dass ich der ersten englischen Maifeier im Jahre
1890 beiwohnen konnte. Beim Eingangstore des Hyde-Park erwar-
tete ich den Pleerbann der Arbeit. An seiner Spitze ritt ein Herold
in rotem Wams. Ein Polizist suchte ihn zum Verlassen des Rosses
zu bewegen; der Policeman zog sich auf die stolze Ablehnung: «Bin
ich nicht ein freier Brite?» respektvoll zurück. Hinter dem Tore
des Parks stand, um einen Geistlichen im Kreise geschart, eine Ver-
sammlung frommer Beter einer Sekte. Sie sangen ruhig ihre Psal-
men weiter, unbehelligt von den Bannerträgern der neuen Zeit.

Von den Rednern, die auf den Tribünen zu sehen waren, zog
eine auffallende Gestalt, ein Mann mit weissen schlichten Haaren und
blitzenden Augen die Aufmerksamkeit auf sich. Seine Rede wurde
immer heftiger. «Der Tag muss kommen — rief er — wo die Kapi-
talisten Euch auf den Knigen bitten werden, acht Stunden zu ar-
beiten.» Aber die Menge war ungläubig, der Beifall blieb aus,
und vor mir murmelte ein alter englischer Gewerkschafter etwas wie
«blutiger Unsinn» in seine Zähne. Der Redner war Paul Lafargue
und neben ihm sass seine Frau, eine Tochter Karl Marx'.

!) Protokoll des Internationalen Arbeiterkongresses zu Paris; abgehal-
ten vom 14. bis 20. Juli 1889. Deutsche Uebersetzung, mit einem Vorwort
von Wilhelm Liebknecht, Nürnberg 1890, S. 123.