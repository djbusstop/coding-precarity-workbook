96 
provocirten und von Deutschen geschürten) Kulturkampfes unsere 
Unterstützung der Regierung", während eine vierte Partei erklärt: 
„Wir wollen die Beseitigung der jetzigen Staats- und Rechts 
ordnung". 
Das deutsche Volk wird auf die vollständige Erfüllung seiner 
Wünsche bezüglich einer Besserung der wirthschaftlichen Lage und 
eines richtigeren Steuersystems so lange verzichten müssen, als 
gewisse Parteien durch ihr Verhalten in den Parlamenten und in 
der Presse sich dem Borwurfe aussetzen, daß in ihnen der politische 
Parteihaß größer ist, als die Vaterlandsliebe, daß vor: ihnen klein- 
liche Fractions- und sonstige einseitige Interessen eifriger verfolgt 
werden, als die allgemeinen Landesinteressen; daß sie weder Herz 
noch Verständniß für das wirthschaftliche Elend und ebenso wenig 
aufrichtige, thatbcreite Sympathien für sociale Reformen besitzen, 
sondern statt deren der leidenden Bevölkerung parlamentarische und 
publicistische Wortgefechte und Zänkereien bieten, statt mitzuwirken 
bei der Beseitigung der Ursachen der gegemvärtig nod; andauernden 
wirthschaftlichen Noth. 
Das deutsche Volk, in den unseligen Parteihadcr mit hinein 
gezogen , wird sich voraussichtlich erst dann zur Wahl von nur 
seine und des Landes Interessen vertretenden Abgeordneten ent 
schließen, wenn die Mehrheit seiner Glieder den festen Vorsatz faßt 
und auch ausführt, die von den Regierungen vorgeschlagenen Wirth- 
schafts- und Steuerreformen ohne jedes Vorurtheil lediglich auf 
ihren praktischen Werth selbst zu prüfen und sich dabei von ihren 
Widersachern weder durch schönklingcnde aber nebelhaft und ver 
schwommene Phrasen, noch durch seichte Gegengründc beirren zu 
lassen. Aldann wird das Volk zweifellos finden, daß die Vortheile 
dieser Reformen für die Gesammtheit die etwa für Einzelne daraus 
erwachsenden Nachtheile und Unbequemlichkeiten weit überwiegen, 
weil es bei einer unparteiischen und sorgfältigen Beurtheilung der 
selben zu Resultaten nachfolgender Art gelangen wird.