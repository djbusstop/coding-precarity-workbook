36 
J 
wesenes, mithin ein Etwas, das einen Anfang hat, es können 
also auch nicht von Ewigkeit her Gesetze in ihr ruhen. 
Es ist sonach reiner Unsinn, zu behaupten, daß die Materie 
vermöge der Mechanik oder Beweglichkeit ihrer Theile nach ewig in 
ihr wohnenden Gesetzen alles organische Leben im Weltall hervor 
gebracht habe und erhalte. 
Wenn mithin die Materie weder sich selbst, noch die aus ihr 
bestehende Welt geschaffen haben kann, so muß doch eine denkende, 
wollende und könnende Macht oder Kraft vorhanden sein, die nicht 
nur die Materie und aus dieser die Welt m3 Dasein gerufen hat, 
sondern sie auch erhält und zwar, wie wir jederzeit wahrnehmen 
können, nach von dieser Macht gegebenen bestimmten unabänder 
lichen Gesetzen. — Nun, diese Macht nennen die gläubigen Menschen 
Gott! — 
Gleichwohl hat sich unlängst ein allgemein bekannter Natur 
forscher von gliihendem Forschungseifer dazu getrieben oder um 
durch Absonderlichkeit Sensation zu erregen, veranlaßt gesehen, die 
Welt mit einer auf die eben erwähnten beiden Sätze basirten neuen 
Schöpfungslehrc zu überraschen. Nach derselben ist der Ursprung 
der ganzen organisch belebten Natur, sammt den darin vorhandenen 
und wirkenden geistigen und seelischen Kräften von einer Urzelle oder 
dem Urschleim herzuleiten. 
Diese Urzellc sei, so lehrt dieser Naturforscher, durch die in ihr 
seit Ewigkeit ruhenden Gesetze gezwungen, sich zu stufenweise höheren, 
belebten unb beseelten Organismen herauszubilden und zwar so, daß 
immer eine neue Art aus der ihr zunächst vorhergehenden entstehe. 
Ein Professor in Jena hat die sog. Dcsccndenzlchrc zuerst 
auf den Menschen angewandt und ausgeführt, daß dieser selbst aus 
dem ihm zunächst stehenden Affen hervorgegangen sei und zwar 
durch die eigne Kraft und Art des Affen, dessen inneres Bedürfniß 
ihn dazu gebracht habe, sich zum Menschen zu erheben, freilich erst 
im Laufe von vielen Millionen Jahren, deren Zahl nicht zu er 
mitteln wäre. Der Affe besitzt also die gewaltige Fähigkeit und