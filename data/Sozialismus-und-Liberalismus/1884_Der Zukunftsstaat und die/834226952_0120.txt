108 
weniger werden andere Länder und Geldinstitute derartigen Cala- 
mitäten gewachsen sein. Die Stabilität eines mäßigen Discontos 
bleibt fiir die Zukunft ausgeschlossen und gleichwohl kann die vor 
handene und kaum bemerkbar zunehmende Masse des Goldes den 
Anforderungen des Welthandels nicht genügen. 
Soll also dem Volkswohle, dem Handel, der Industrie und 
dem Verkehr gebührende Rechnung getragen und der herrschenden 
Geldnoth Abhülfe geschafft werden, so muß Deutschland für Erhö 
hung des Silberwcrthcs durch Wiederaufnahme der Doppelwährung, 
jedenfalls aber dafür sorgen, daß im Jnnenverkehr das Silbergeld 
nicht nur beibehalten, sondern noch vermehrt werde. Und es ist 
erfreulich, daß man sich im Volke immer mehr von der Richtigkeit 
dieser Ansicht überzeugt. Möge dasselbe sich nicht abermals durch 
gewöhnlich ails unlauterer Quelle fließende parlameiltarische Reden 
irre führen lassen. 
Bezüglich der Schutzzölle darf man sich jedoch nicht etlva dem 
Glauben hingebeil, als könnten dieselben Wunder thun, denn er- 
fahrnngsmäßig vermögen Einfuhrzölle niemals eine wirkliche Ver- 
mehrung, sondern nur eine Umlegung der natürlichen Arbeits- lind 
Kapitalkräfte zu bewirken, deshalb bürsten in Deutschland die Schutz 
zölle von dem Zeitpunkte an, in dem dasselbe bezüglich feiner Pro 
dukte mit denjenigen Ländern, gegen welche die Schlltzzölle errichtet 
sind, ohne Verluste concurriren kann, allmählich wieder beseitigt 
werden. Also so lange Schlltzzoll für das deutsche Reich, bis, wie 
List ganz treffend sagt, zwischen ihm und ben mit ihm concurriren 
den Ländern die Waffen gleich sind, d. h. bezüglich derselben Pro 
dukte mit ihnen concurriren kann, denn sonst würde es jährlich um 
soviel ärmer, als es im Gcldwerthe an Waaren mehr einführt als 
ausführt. Zwar legt das Schutzzollsystem dem Volksvermögen 
Opfer auf, aber sie bestehen nicht in baarem Gelde, sondern nur 
in Produkten, von denen bei gleicher Anstrengung der Productions- 
kräfte weniger hervorgebracht und verbraucht würben, als der freie 
Handel geschaffen haben würde. Die seit den sechziger Jahren im