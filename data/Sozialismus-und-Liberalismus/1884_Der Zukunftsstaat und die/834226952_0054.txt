42 
wesen sein kann, auch nicht eine Zwischenart, denn jedes Indi 
viduum könnte auch wiederum nur ein Individuum, das sich 
inzwischen zu einer Hähern Art entwickelt hätte, erzeugen, es 
sei denn, daß der Affe sich wie der Polyp noch beliebig ver 
vielfältigen lassen könnte. Diese Fähigkeit muß derselbe beim 
Durchlaufen der verschiedenen Stadien seiner Entwickelung, 
vielleicht schon beim Uebergange zu der ihm nächstfolgenden 
Art eingebüßt haben, sonst hätte dieselbe doch bei allen späteren 
Zwischenartcn bis zum Affen und Menschen hinauf bemerkbar 
sein müssen. 
Sind beiden aber diese und noch andere werthvolle Fähig 
keiten ihrer Borartcn, z. B. zu fliegen, zu klettern, im Wasser 
zu leben und andere mehr verloren gegangen, so Hütten sie ja 
diesen gegenüber Rückschritte in ihrer Entwickelung gemacht! 
Die sehr nahe liegende Frage, wie die nur in warmen Ge 
genden lebenden Affen Tausende von Meilen von ihrem Heimath- 
lande entfernt, selbst in eisige und unwirthbare Gefilde gelangt sind, 
um dort als Menschen weiter zu leben, wird von den Darwinisten 
wohlweislich mit Stillschweigen übergangen. 
Wir möchten hier noch einer kurzen Erwägung Raum geben: 
Ist der Mensch wesentlich nur ein Thier, wie der Materialismus 
lehrt, so sind — nach den praktischen Schlußfolgerungen der in der 
Menschheit wuchernden thierischen Triebe — die Lüste auch legitim, 
und demgemäß gestaltet sich die Praxis. Alle Cultur geht von 
einem Culturprincipe aus, das ihre Seele ist und der ganzen Cultur- 
euttvickelung die Direction verleiht. Deshalb wird der theoretische 
Materialismus von dem practischen Bestialismus mit leidenschaft 
licher Hast ergriffen und triumphirend als Freibrief ausgerufen. 
Die große Masse der Menschen hat den Talisman der Bildung 
nicht, und seit der Materialismus sie lehrt, daß sie blos wesenlose 
Automaten sind, lachen sie natürlich darüber, daß man ihnen, da 
sie doch nur als physikalische und chemische Wirkungen existiren, 
Tugend predigt, Das ist die Folge. Der berühmte Sprachforscher