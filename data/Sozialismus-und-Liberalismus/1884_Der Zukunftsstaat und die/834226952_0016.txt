4 
gewagt. Sein System, welches auf Beseitigung des Eigenthums 
und des Erbrechts, sowie des , heutigen ganzen Staatswescns und 
auf die Gründung einer neuen Gesellschaftsordnung auf social 
demokratisch - communistischer Grundlage hinausläuft, wollen wir 
später genauer erörtern. 
Inzwischen war am social-politischen Horizont ein hellstrahlen 
der Stern aufgegangen, vor dessen blendendem Glanze die übrigen 
Gestirne fast gänzlich verblaßten, der aber gleich einem Meteor bald 
wieder verschwand. Das war Ferdinand Lassalle. Schor: 1848 
bctheiligte er sich an der social-demokratischen Bewegung in Düssel 
dorf und war deren eigentliche Seele, trat aber bald ganz von ihr 
zurück und widmete nun seine Geisteskräfte fast ausschließlich der 
Führung des bekannten Processes der Gräfin von Hatzfeld gegen 
den von ihr geschiedenen Gemahl wegen bedeutender Vermögens 
ansprüche, den er mit großem Geschick, Eifer, unermüdlicher Aus 
dauer und seltener Uneigennützigkeit glücklich zu Ende führte. Erst 
1862 übernahm er wieder die inzwischen ungeschickt gehandhabte und 
fast ins Stocken gekommene Leitung der socialen Bewegung. Er 
erstrebte zunächst behufs der Betheiligung des Arbeiterstandes am 
politischen Regimenté die Einführung des directe:: Wahlrechts und 
grünbete zu diesen: Zwecke den Allgemeinen deutschen Arbeiter- 
Verein, als dessen Präsident er bereits im Sommer 1863 über 
10000 Arbeiter verfügte. Um dem Arbeiterstande ein menschen 
würdigeres und unabhängigeres Dasein zu schaffen, hielt er die 
Gründung von Productions-Associativncn mit Staatshülfc für das 
geeignetste Mittel. Er hoffte, daß die Productions-Associationen 
allmählig die gesummte deutsche Arbeiterwelt umfassen und so dessen 
wirkliche Hebung in intellcctueller, moralischer und materieller Be 
ziehung auf friedliche Weise zu erreichen sein werde. Deshalb 
wollte Lassallc auch Vertreter der Besitzenden und des gebildeten 
Bürgerstandes, sowie der Wissenschaft in die Reihen seines Vereins 
hinüberziehen. Dabei war sein Augenmerk auf den preußischen 
Staat gerichtet. Zugleich sprach er sich ganz entschieden gegen eine