28 
Volksstamme doch der Sol):: viele. Jahre, ja mehrere Generationen 
hindurch in verschiedenen aber einander ganz nahe gelegene!: Gegenden 
so verschieden ist? Hier hängt vielmehr der Preis der Arbeit haupt 
sächlich von dei: Sitten und Gewohnheiten des Volkes ab. — Es 
ist ferner Thatsache, dass die Löhne keineswegs durch die Preise de: 
nothwendigsten Lebensmittel regulirt werden, das; die Arbeitslöhne 
vielmehr in den verschiedenen Gegenden Deutschlands weit n:eh: 
unter sich differire::, als die Preise der nothwendigen Lebensmittel 
und daß deshalb auch die materielle Lage der ländlichen Arbeiter 
in den einzelnen Theilen Deutschlands eine sehr verschiedene ist. 
Somit ist denn auch die Ansicht derer, welche für die Verschieden 
heit der Lohnsätze durch die Verschiedenheiten der Lebensmittel eine 
Ausgleichung erwarten, eine durchaus irrige und das eiserne Lohn 
gesetz beruht auf einer bloßen aus grauer Theorie hergeleiteten 
Einbildung. 
Dann sollen nach Carl Marx' Behauptung alle Werthe auf 
Arbeit zurückzuführen, d. h. der Werth eines Dinges oder Products 
gleich sein der allgemeinen gesellschaftlichen Arbeitszeit, welche zu 
dessen Herstellung erforderlich war. Danach müßten denn auch 
z. B. von Hundert Paar Strümpfen, deren eine Hälfte mittelst der 
Maschine hergestellt würde, zehn Stunden, die andere durch Hand 
arbeit zweihundert Stunden Arbeitszeit erfordert. letztere einen 
zwanzig Mal höher:: Werth und Preis haben, als erstere, gleich 
wohl' aber ist ein Preisunterschied zwischen beiden Fabrikaten in 
Wirklichkeit entweder gar nicht vorhanden oder er ist ein ganz 
unbedeutender. Ebenso hat der sehr gelehrte Herr gänzlich über 
sehen oder es absichtlich verschwiegen. daß außer Arbeit und 
Maschinen auch noch andere Productionsmittel, als: Geld, größere 
oder geringere Brauchbarkeit und Zweckmäßigkeit der Handwerkzeuge, 
das Material. Bodenfruchtbarkeit, größere oder mindere Geschicklich 
keit. Regsamkeit und Intelligenz der Arbeiter u. s. w. auf den Preis 
der'Dinge einwirken; er hat ferner nicht berücksichtigt, daß der 
Werth derselben durch die Nothwendigkeit. Nützlichkeit oder An-