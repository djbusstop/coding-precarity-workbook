53 
Bestimmung spurlos in das Nichts versinken? Es kann mcht 
sein, nicht Sterben kann man den Zustand nennen, man muß 
ihn für den Schleier halten, der wohlthätig über das Ent 
wickeln zur Vollendung gezogen. Nichts in der Welt ist wirklich 
vergänglich; alle Kräfte und Stoffe sind ewig, sie wechseln nur 
ins Unendliche ihre Formen, in denen ihre Thätigkeit sich 
äußert. Also ist auch über den Sternen ein Weiterleben, ein 
Wiedersehen. Und selbst der Gottes- und Unstcrblichkeitsleugner 
ruft am offenen Grabe eines theuern Angehörigen oder Freundes 
unwillkürlich das tröstende Wort aus: auf Wiedersehen! 
g. Wenn nun die mit unsern Sinnen wahrnehmbaren Körper 
trotz der unendlichen Veränderungen ihrer Form in ihren 
Grundstoffen unveränderlich, d. h. ewig sind, so müssen es 
auch die unkörperlichen, nur durch ihre Wirkungen wahrnehm 
baren mittels der Sinnesorgane thätigen und herrschenden 
Seclenkräfte sein. 
h. Es liegt ein Etwas in dem Menschen, was ihn antreibt, sich 
mit der Ergründung seines Wesens und seiner Bestimmung zu 
beschäftigen. Und dieses Forschen und Suchen beweist die tiefe 
Sehnsucht uach Gewißheit eines Jenseits und einer persönlichen 
Fortdauer. 
Diese Erwägungen sollten genügen, Jedermann von dem Da 
sein Gottes und von der Fortdauer unserer Seele vollkommen zu 
überzeugen; leider ist dies aber selbst nicht einmal in ben gebilde- 
teren, geschweige denn in den untern Ständen immer der Fall, viel 
mehr werden sogar von Männern der Wissenschaft unter Nicht 
achtung alles Göttlichen und Ueberirdischen so ungeheuerliche Lehren, 
wie die vorhin besprochenen, als vermeintliche Resultate wissenschaft 
licher Forschungen mit frivolem Leichtsinn in die Welt geschleudert, 
unbekümmert um die entsetzlichen Sittenverheerungen, welche dadurch 
unter der Menschheit angerichtet werden und ohne zu bedenken oder 
zu wissen, daß eine Wissenschaft, welche in Allem, was Ahnung 
vom Jenseit, was Glaube an eine höhere Weltordnung, was Gottes-