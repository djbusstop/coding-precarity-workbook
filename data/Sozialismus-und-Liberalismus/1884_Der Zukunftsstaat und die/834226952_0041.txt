29 
nehmlichkeit für den Menschen, non der großem oder geringern 
Einige, in welcher sie vorhanden sind, sowie durch Nachfrage und 
Angebot, welche unter dem Gesetze der Concurrenz stehen, wesentlich 
beeinflußt wird. 
Wir kommen daher zu dem wohlbcrcchtigten Schlüsse, daß alle 
Declamationen von der Vereinbarkeit der individuellen Freiheit mit 
dem communistischcn Wirthschaftsbetriebe nichts sind als absichtliche 
Flunkereien oder sentimentale Anwandlungen, wie denn der Com- 
munismus von jeher für phantastische und gedankenlose Menschen 
seine große Zugkraft geübt hat. Der Communismus erkennt dem 
Einzelnen das Eigenthum ab, woraus dieser als das Ergebniß 
seines Denkens und Schaffens Anspruch erhebt; die bloße Garantie 
der körperlichen Bedürfnisse bietet keinen Ersatz für das Ausgeben 
ber individuellen Freiheit. 
7. Das Verbot, Zinsen zu nehmen. 
Ferner wird von den socialistischen Agitatoren in meist ge 
hässiger Welse das Zinsennehmen als Ausbeutung der Arbeit charak- 
terisilt, während dasselbe in Wahrheit einen wohlberechtigten, weil 
ans freier Vereinbarung beruhenden Anspruch aus eine Gegenleistung 
darstellt. Auch sind die meisten productiven Unternehmungen solchen 
freien Vereinbarungen zu verdanken; denn ohne diese würden jene 
eben nicht möglich gewesen sein, weil zu allen productiven Unter- 
nehmungen eine gewisse Anzahl von Betriebsmitteln, und um diese 
beschaffen zu können, Geld nöthig ist. Man könnte eine solche Ver 
einbai iiug raglici) einen Gesellschaftsvertrag nennen, bei welchem der 
ciiic Theil das erforderliche Geld oder einen Theil davoli hcrleiht 
uiid ihm dafür als Gewinnantheil cm der Höhe des eingeschossenen 
Capitals entsprechenden Zinsbetrag zu gewähren ist, während dem 
andern Theile die übrigen Gewinnantheile gegen Verpfändung des 
gesummten Betriebsmaterials allein zufallen und ihm dadurch die 
Möglichkeit geboten wird, bei geschickter und glücklicher Leitung des 
Unternehmens dieses nicht nur nach und nach zu erweitern und ge-