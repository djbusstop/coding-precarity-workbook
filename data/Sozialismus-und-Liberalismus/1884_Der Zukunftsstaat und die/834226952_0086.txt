74 
trotz der Hetzereien und der Behauptung ihrer Führer, daß Vor- 
schußvercine den Arbeitern nichts nützten, sich immermehr um die 
Mitgliedschaft bewerben. 
Bezüglich einer zweckmäßigen Organisation derartiger Vorschuß- 
und Kreditvcreinc verdient die nach dem Raiffeisen'schen Systeme 
den Vorzug, weil die Darlehnc durch gleiche Solidarhaft aller 
Mitglieder und durch Stellung von Bürgen Seitens der Schuldner 
gegen Verluste sicher gestellt sind und wegen dieser doppelten Ga 
rantie Kapitalien zu 4 und 4y//o leicht zu beschaffen sind. Die 
selben werden an die Mitglieder nur zu 5% wieder ausgeliehen. 
Da aber die Schuldner bei Empfang des Darlehns ein für alle 
Mal eine Provision von pro 3 Monate und von 1% pro 
Jahr der Schuldzeit zu zahlen haben, so wird in 20 bis 25 Jahren 
ein Reservefonds vorhanden sein, welcher es dem Vereine ermöglicht, 
mit eigenen Mitteln auszukommen. 
Die Verwaltung verursacht um deswillen wenig Kosten, weil 
die Vorstandsmitglieder der einen Centralvcrein umfassenden kleinen 
Bezirksvcrcinc außer Ersatz ihrer baaren Auslagen keinen Gehalt 
beziehen, ihr Amt als Ehrenamt umsonst verwalten und nur der 
Rendant eine dem Geschäftsumfange entsprechende Remuneration 
erhält; in den Vereinen ist grundsätzlich die Politik ausgeschlossen, 
so daß sie weder Brutstätten politischer Agitation werden, noch 
einträgliche Sinecuren für politische Freunde schaffen können; jedes 
Mitglied hat in der Generalversammlung nur eine Stimme ohne 
Rücksicht auf die Höhe der etwaigen Einlagebeträge, svdaß Majori- 
sirung der Gesellschaft durch eine Minorität unmöglich ist. 
bb. Wohstoff-Genossenschaften. 
Alsdann ist man in Deutschland zur Gründung von Rohstoff- 
Genossenschaften übergegangen. Der Nutzen derselben ist sehr be 
deutend; denn durch deu gemeinsamen Einkauf der Rohstoffe im 
Großen aus erster Hand und deren Ablaß in kleiner, Partieen zu 
den Engrospreisen mit einem Aufschlage von etwa 5% behufs