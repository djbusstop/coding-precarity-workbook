51 
4* 
unlängst im Wiener Krankenhause verstorbenen 45 Jahr alten 
Marine fand man im Gehirne einen über einen Centimeter 
lairgen gänzlich verrosteten Nagel, der sich wahrscheinlich schon 
seit der Kindheit des Verstorbenen in dessen Gehirn befunden 
hatte, und doch soll der Mann mit einem wirklichen Nagel im 
Kopfe und einem verletzten Gehirn immer recht gescheidt ge 
wesen sein. 
d. Gott, welcher den Menschen geschaffen und mit mancherlei Vor 
zügen ausgerüstet hat, kann nicht wollen, daß ein Geschöpf 
mit solchen Vorzügen untergeht; denn sein Wille ist ja der 
höchst vollkommenste, der sich nicht verändern kann. Hat er 
dav Dasein des Menschen gewollt, so muß er es auch immer 
wollen, und vermöge seiner Allmacht vermag er auch den 
Menschen ewig zu erhalten, und da er allweise ist, kennt er 
auch die Mittel dazu; in seiner Aügüte will er außerdem das 
Glück der Menschen — und zu diesem Zwecke hat er so viele 
geistige Anlagen in ihn gelegt; da diese aber in dem kurzen 
Erdenleben und wegen der in ihrer Wirffamkeit körperlich und 
zeitlich beschränkten Sinneswerkzeuge sich uicht vollständig ent 
falten können und deshalb auch des Menschen Glück in dieser 
Welt unvollkommen bleibt, so müssen wir annehmen, daß unser 
Dasein nicht auf das Erdenleben beschränkt ist. sondern sich 
noch ails ein höheres ausdehnt. — Alle unsere geistigen An 
lagen wären auch zwecklos, wenn sie uns nicht weiter, als wir » 
hier kommen können, führen sollten, aber schon ihre stufenweise 
Entfaltung zeigt uns ihren Keim der Ausbildung ohne Ende. 
e. Der in uns schlummernde sittliche Trieb, erst einmal erwacht, 
hört mit der einzelnen Befriedigung nicht auf, er will vielmehr 
der Sittlichkeit immer mehr nachstreben, der Mensch aber als 
endliches Wesen, kann nie das Ideal der vollkommenen Sitt 
lichkeit erreichen, er kann sich demselben nur in einem endlosen 
Fortschreiten mehr und mehr nähern. d. h. endlos fortdauern 
oder unsterblich sein.