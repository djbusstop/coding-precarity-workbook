55 
faítung kommt, sie bildet ein Hauptunterscheidungs - Merkmal des 
Menschen vom Thiere. Sie ist mit dem menschlichen Wesen so 
ilmig verwachsen und so unverwüstlich, daß sie aus jedem Ver 
nichtungskampfe gleich den: Phönix stets wieder aus der Asche neu 
verjüngt emporsteigt. Deshalb würde es auch den Machthabern 
im Zukunftsstaate niemals gelingen, den Religionstrieb in der 
Menschen Brust zu ertödten. ja. sie würden, wie vordem Robespierre 
in Frankreich, sich sehr bald genöthigt sehen, die gestürzte Gottheit 
und die Religion wieder in ihre äußern Rechte einzusetzen und 
darin mit allen Kräften zu schützen, oder sie würden unter den 
Trümmern des neuen Reichs begraben werden. 
12. Unüberwindliche Schwierigkeiten, welche einer 
praktischen Durchführung des internationalen com- 
munistischen Programms entgegenstehen. 
Wir wollen nun zur Aufsuchung und Betrachtung der haupt 
sächlichsten Schwierigkeiten übergehen, welche sich der Durchführung 
des bereits erwähnten positiven internationalen communistischen 
Programms, insbesondere der Beseitigung der heutigen Staats- 
sormen, sowie der Umwandlung des Privateigenthums in ein col 
lectives und bezüglich der staatlichen und technischen Leitung des 
künftigen Staatswcsens darbieten würden. 
1. Zunächst müßte die Socialdemokratie, wenn sie im Kampfe 
mit der Bourgoisie über diese einen vollständigen Sieg er 
ringen wollte. nicht nur wohl gerüstet und dem Feinde an 
Streitkräften überlegen sein, sondern auch geschickt und umsichtig 
zu opcriren verstehen, hierzu bedürfte sie aber geschulter und 
bewährter Führer; denn wie erst der letzte Krieg gegen Frank 
reich gezeigt hat. genügt weder numerisches Ucbergewicht, noch 
Tapferkeit allein zur Besiegung eines ebenfalls tapfern, aber 
zugleich gut geführten Feindes. Mag nun auch die Social 
demokratie recht klug und theoretisch gebildete Führer besitzen, 
ob sich aber unter diesen tüchtige Schlachtenlenker besinden, ist