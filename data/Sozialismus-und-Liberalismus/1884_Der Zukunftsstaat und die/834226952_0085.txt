73 
In England hat sich das Prinzip der Eigcnkraft mittelst 
Arbeiter-Associationen bis jetzt sehr gut bewährt. Auch in Deutsch 
land hat man bereits die verschiedenartigsten Bedürfnisse und die 
wirthschaftlichen Mißstände der Zeit richtig erkannt, aber, um die 
genossenschaftliche Bewegung vor schweren Erschütterungen zu be 
wahren, nicht, wie in Frankreich, gleich mit der schwierigstell Form 
der Genossenschaft, mit der Produktionsgenossenschaft, sondern mit 
der Beschaffung von ausreichendem Kapital angefarlgcn, d. h. den 
richtigeil Weg natürlicher Entwickelung eingeschlagen, in Folge dessen 
die Genossenschaftsbewegung bereits einen mächtigen unb heilsamen 
Aufschwung gewonnen hat. 
Deshalb wurden: 
aa. Woküsbcrnken, 
zunächst sogenannte Volksbanken oder Vorschuß- und Krcditvereine 
gegründet, welche die Selbsthilfe in Bezug auf das Bcdürfuiß von 
Baarschaft an Gewerbe und Wirthschaft für solche Handwerker und 
Arbeiter bezwecken, bcncn der Bankverkehr entweder gar nicht oder 
nur unter sehr erschwerten Bedingungen offen steht. Es sollen 
durch die Vorschuß- und Kreditvereine dem Harldlverker zur Be- 
schaffllng guter Handwerkszeuge und Materialien, dem Landwirthe 
zur Anschaffung von Vieh, Dünger, guter Aussaat, von zweck 
mäßigen Ackergeräthschaften und Maschinen, dem Tagelöhner zur 
Erwerbung eines Häuschens uild etlvas Land gegen mäßige Zinsen 
nut nicht zu kllrzen Zahlungsfristen die erforderlichen Gelder dar- 
gelicheil werden, lvonlit man verhindern will, daß diese Geldbedürf 
tigen in die Hände herzloser Wucherer fallen, welche es ihnen 
erschweren und meist unmöglich machell, selbst bei Einsicht, Fleiß 
und Sparsamkeit vonvärts zu kommen. 
Die segensreiche Wirksamkeit solcher Volksbanken steht nach 
dell bereits daniit gemachten Erfahrungen, Ausnahmsfällc zugegeben, 
außer Zweifel, weshalb auch die Zahl der Volksbanken inDeutsch- 
laild stetig wächst unb die Betheiligung an denselben eine immer 
größere wird unb selbst von den vernünftigeren Socialdemokraten