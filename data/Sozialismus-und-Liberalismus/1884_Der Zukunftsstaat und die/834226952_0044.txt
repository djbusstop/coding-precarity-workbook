32 
J 
Ehe und Familie sind so werthvolle Einrichtungen der weisen 
Weltregierung, daß die Menschheit auf sie niemals wird verzichten 
wollen; denn sie sind die treuen Hiitcr der Sittlichkeit und deshalb 
für den Fortbestand eines jeden Staatswesens unentbehrlich. Die 
Familie ist der Grund und Eckstein des bürgerlichen Lebens, sie ist 
die ursprünglichste, urälteste, menschlich-sittliche Genossenschaft, aber 
eine Genossenschaft, die erst durch das Christenthum die rechte 
Weihe und Widerstandsfähigkeit gegen oftmals geplante Angriffe 
erhielt. Und das ist geschehen, seit die Stellung der Frau, das 
eheliche und Familienleben durch die christliche Lehre verschönt 
worden ist. Die Jdealisirung des ewig Weiblichen hat die Sitten 
geniiibcttunbbic Gunst mö#9 #tbcrt. 3)tc Giuiü; 
fation und die Bestandeskraft der christlichen Staaten hat ihren 
Grund in der Unification der Frau mit dem Manne, in der Hoch 
schätzung der ehelichen Treue und Häuslichkeit, in der Anerkennung 
der weiblichen Vorzüge, in der Darstellung der Schönheit, Liebe 
und Tugenden der Frauen. Die Art der Emancipation, welche 
ihnen von den modernen politischen Parteien mtb socialen oectireru 
geboten werden will, kann sie nicht begeistern, ihr weibliches Na 
turell und Gefühl bleibt kalt für diejenigen, welche blos die mate 
rielle Seite des Lebens in den Vordergrund stellen. Das deutsche 
Weib (Ausnahmen giebt es leider auch bei uns schon) setzt noch 
sein höchstes Ideal, ein glückliches Familienleben, über eine ver 
goldete Selbstständigkeit der „Emancipation". Die Liebe hält längn 
noch als der Haß, denn die Liebe, sagt das Dichterwort, stirbt nicht. 
Der Klassenhaß aber, mit Hilfe dessen die socialdemokratischen Agi 
tatoren ihre Principien von der materiellen Befreiung unter Dach 
und Fach zu bringen suchen, hat nichts mit der Liebe gemein und 
ist Feind des Familienlebens, damit hat sich die Socialdemokratie 
von vornherein die christliche Frau zur Feindin gemacht und an 
bi# getnb#aft ivitb fie selbst nad) Ucbcmmbirnß anbetn: 
Schwierigkeiten nothwendig zu Grunde gehen. <co ist denn da. 
geben in bet ^amitié baö beste uot alíen foeiaieu