19 
2* 
Mußestunden ein freier Mann ist. Mancher etablirt sich als 
Principal oder Meister, der als Gehülfe ein viel sorgenfreieres und 
bequemeres Leben hatte, aber der Reiz eines eigenen Heerdes und 
die Liebe zur Selbstständigkeit trieb ihn dazu. Im Zukunftsstaate 
würde aber die Arbeit nicht nur für Jedermann eine Nothwendig 
keit sein, weil es ja in demselben keine Capitalisten geben soll, die 
Andere für sich arbeiten lassen könnten, also Jeder sein Brod durch 
eigne Arbeit verdienen müßte, sondern es würde mit dieser Noth 
wendigkeit auch der Zwang verbunden sein; denn da der neue Staat 
zugleich Generalunternehmcr und Generalproducent sein würde, so 
müßte dessen Centralleitung auch nothwendigerweise über die ge 
summten Arbeitskräfte unumschränkt verfügen und dieselben nach 
bestem Ermessen verwerthen können, weil es ihr sonst nicht möglich 
sein würde, die Staatsarbeitsmaschinerie in regelmäßigem Gange zu 
erhalten, und Verwirrung erzeugende Stockungen zu verhüten. Dem 
gemäß würden denn auch von den betreffenden Staatsorganen den 
einzelnen nunmehrigen Staatsarbeitern diejenigen Geschäfte und 
Arbeiten zugewiesen werden müssen, für welche man sie entweder 
für geeignet hielte, oder weil es so das Staatsinteresse erheische. 
Natiirlich könnte dabei auf die Wünsche der Einzelnen nicht jederzeit 
Rücksicht genommen werden. Ebensowenig würde ein Berufswechsel 
statthaft sein, sicherlich wiirden dann aber die mit ihrem Arbeits 
loose Unzufriedenen die Arbeitsämter mit Versetzungsgesuchen be 
stürmen lind wenn erfolglos, wie gefangene Löwen an ihren Ketten 
rasseln, und selbst die Schwächeren und Geduldigeren unter ihnen 
in ihrer trostlosen Gefangcnhausstimmung in den Ruf der Starkem 
nach unbedingter Berufsfreiheit und Freizügigkeit nicht nur begeistert 
einstimmen, sondern auch gemeinsam mit diesen das aufgezwungenc 
Arbeitsjoch gewaltsam abzustreifen suchen. 
Auch diejenigen, welche jetzt unter dem Drucke der Nothwendig 
keit selbst die schmutzigsten, ekelhaftesten, die schwersten, die gesund 
heitsschädlichen oder lebensgefährlichen Arbeiten verrichten, würden 
im Zukunftsstaate murren und sich mit Grund darüber beschweren