25 
emcn Theil ber gewonnenen Güter für zukünftige Bedürfnisse und 
zu weiterer: probuetiven Unternehmungen in der Landwirthschaft, 
Industrie und im Handel zurücklegen. Allein Fleiß und Sparsam 
keit übt der Mensch nur bann, wenn er nicht nur frei über die 
Früchte seiner Arbeit verfügen kann, sondern auch sein Eigenthum 
und das von ihm angesammelte Vermögen vom Staate rechtlich 
geschützt wird. Ohne Capitalien wäre eine Erweiterung der Pro- 
duetion nnb somit des gesellschaftlichen oder Nationalvermögens, 
sowie der menschlichen Cultur unmöglich. Und selbst der entschiedene 
Capitalfeind Lassalle hat sich dieser Einsicht nicht verschließen können; 
beim in seiner Schrift „Bastian Schulze" sagt er: „Völker, die von 
voller, individueller Freiheit ausgehen, wie die Indianischen Jäger 
stämme, können deshalb niemals zu irgend einer Capitalansammlung 
nnd daher niemals zu irgend einem Culturfortschritte gelangen." 
Sehr charakteristisch und mit verständlicher Knappheit drückt 
bieg ber Mannte Matiotmiöfonom 0. Stein anë: „S)er Bkn# 
tit cm Wesen für sich, nicht blos ein Exemplar der Gattung homo 
sapiens. Wo die Individualität in der Gattung aufgeht, da herrscht 
dte Uncultur, die Barbarei, die finstre Knechtschaft der Leiber und 
Geister. In der thunlichst freien Enllvicklung der Individualität 
ltegt ein Hauptantrieb des Fortschrittes; was er hervorbringt, das 
gehört ihm und keinem Andern. Geben wir die persönliche Existenz, 
dte Individualität zu, dann müssen wir auch das persönliche Eigen- 
thnm anerkennen. Wenn wir das Eigenthum aufheben, dann 
erniedrigen wir bett Menschen zu einem bloßen Gattungsbegriff und 
sprechen ihm die Eigenschaften ab, welche sein Sein und Wesen aus 
machen; wir würden ihn zum Thiere degradiren, zum willenlosen 
Selaven eines Herrtt oder einer Wahnidee, oder wir müßten die 
ganze Menschheit nach Art eittes mittelalterlichen eommunistischen 
Bettelordens umformen, in welchem die Persönlichkeit nur durch 
einen höheren Willett gelenkt und bestimmt wird. Watt müßte sich 
burnt den Staat als citte neue moderne Auflage eines solchett Ordens 
vorstellen."