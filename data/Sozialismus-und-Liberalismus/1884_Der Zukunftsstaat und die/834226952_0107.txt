95 
müssen. Zu diesem Behufe sind von der Reichsregierung bereits 
folgende durchaus zweckmäßige Gesetzesvorschläge gemacht oder in 
Aussicht gestellt worden: 
1) betreffend die Einführung des Tabaksmonopols, 
2) über stärkere Heranziehung des beweglichen Vermögens, mittelst 
entsprechender Börsen-, Erbschafts-, Wehr- und anderer in- 
directen Steuern, 
3) über Erhöhung der Wein-, Bier-, Branntwein- und Zuckersteuer; 
4) ferner soll durch Einführung von Unfallversicherungen, Kranken- 
und Altersversorgungskasscn der Noth und Bedürftigkeit der 
Arbeiterklassen Hilfe, sowie der Industrie und den Gewerben 
durch mäßige Eingangszölle Schutz gewährt werden. 
Außerdem beabsichtigt die Neichsrcgierung, eine wesentliche Ver 
minderung der bereits in vielen Gemeinden fast unerschwinglich ge 
wordenen Communalstenern durch staatliche Uebernahme der Kirchen- 
nnd Schullastcn, sowie mittelst Ueberlassung eines Theiles der 
Grund- und Gebäudesteuern herbeizuführen. Als Ersatz für diese 
auf das Reich übergehenden Leistungen und zur Deckung der Kosten 
für weitere Staatszwccke würden das Tabaksmonopol und die eben 
angegebenen Steuern nicht nur vollkommen genügen, sondern auch 
iwch Befreiung der untern Steuerklassen von directe» Steuern und 
Ermäßigung derselben für die höheren Klassen ermöglichen; außer 
dem aber würde durch eine derartige Steuerreform eine gerechtere 
Vertheilung der Steuerlasten erzielt werden. 
Leider ist bei der jetzigen Zusammensetzung der Parteien wenig 
Aussicht aus Annahme aller dieser vortrefflichen Reformvorschläge 
vorhanden, wie ja auch das Tabaksmonvpol und die Wehrsteuer 
in der That abgelehnt wurden. 
Und wie ließe sich auch etwas anderes von antinationalgesinnten 
Parteien erwarten, von denen folgende Parolen ausgegeben und 
befolgt werden: „Keinen Kompromiß mit der Regierung"; „Wir 
kennen zwar die Pläne der Regierung nicht, aber wir verwerfen 
sie im Voraus"; „Erst nach Beendigung des (doch erst von Rom