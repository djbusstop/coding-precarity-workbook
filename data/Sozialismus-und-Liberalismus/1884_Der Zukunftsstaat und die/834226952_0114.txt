102 
[teuer von einem Procent der Kauf- resp. Werthsumme und zwar ohne 
Berücksichtigung der etwa aufhaftenden Hypothekenschulden zu entrich 
ten gewesen. Schon der ausgleichenden Gerechtigkeit nnilcit ist eine Be 
steuerung der Zinsen, Renten und aller Kaufsgeschäfte an der Börse 
geboten und bei sog. Differenz- oder richtiger Wettgeschäften sogar eine 
doppelte Steuer durchaus gerechtfertigt, weil dadurch einer künstlichen 
Steigerung der Preise für Getreide, Petroleum, Häute, Colonial- 
waaren und überhaupt für alle diejenigen Gegenstände, welche in den 
Geschäftskreis der Börse fallen, kräftig entgegengewirkt wird. Damit 
jedoch keine von Kapitalien unmittelbar herrührenden Einnahmen un- 
besteuert bleiben, würden diese sämmtlich, also auch alle Arten von 
Geldanleihen für stempelpflichtig zu erklären sein. 
Die Befürchtung, daß eine Besteuerung der Renten- und 
Börsengeschäfte den Geldverkehr ins Ausland dränge, ist un 
begründet, denn ttotzdem, daß z. B. in Oesterreich von Zins 
coupons eine sehr hohe Steuer erhoben wird, übt dieselbe doch 
nur geringen Einfluß auf den Jnnenvcrkehr mit österreichischen 
Werthpapieren ans und von diesen cursirt sogar ein erheblicher 
Theil im Auslande. Uebrigens kann einem Abflusse an Kapitalien 
ins Ausland behufs Ankauf von dortigen Werthpapieren dadurch 
wirksam entgegengetreten werden, daß von ausländischen Zins- 
eoupons künftig doppelt so hohe Steuern erhoben werden, als von 
inländischen. Anderweite Auslagen von Kapitalien im Auslande 
sind aber wegen der damit verbundenen Schwierigkeiten und Un- 
bequemlichkeiten und da dort zumeist höhere Steuern zu zahlen sind, 
als im Jnlande, weniger zu besorgen. 
Auch der Einwurf, daß den sogen. Differenz- oder Wett- 
geschäften sehr leicht die Form von Zeitgeschäften gegeben werden 
könne, ist unzutreffend, denn ganz abgesehen davon, daß dies ohne 
Gefährdung des Interesses eines der Contrahenten doch sehr schwierig 
sein dürfte, kann eine solche Täuschlmg dadurch unwirffam gemacht 
werden, daß die wirllich erfolgt sein sollende Lieferung glaubhaft 
nachgewiesen werden muß, worauf dann nur der einfache atener-