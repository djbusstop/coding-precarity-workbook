30 
winnreicher zu machen, sondern auch mit der Zeit in dessen allcr- 
uigen und schuldenfreien Besitz zu kommen. Wollte man also das 
Zinsennehmen verbieten, so hieße das nichts anderes, als alle pro 
ductiven Unternehmungen lediglich in die Hände von einigen Tausend 
Capitaliste» legen, also dem Capital dienstbar machen und, was 
man gerade verhindern wollte, die Ausbeutung der Arbeit noch 
mehr fördern. 
8. Die beabsichtigte Abschaffung der Ehe als reli 
giöser, politischer, juridischer und bürgerlicher 
Institution. 
Auch gegen die Familie richtet der Socialismus seine An 
griffe, weil er recht wohl weiß, daß sich mit ihr keine andere 
Verbindung an Innigkeit der Empfindungen und Stärke der Inter 
essengemeinschaft vergleichen läßt, daß sic der Hort und die Trägerin 
des reinen Individualismus ist, der erst in ihr seine vollständige 
Entwickelung findet und nur wenig Raum für Gemeinsinn nach dem 
Wunsche der Socialdemokraten läßt. Um nun die Familie, seinen 
Todfeind zu bekämpfen und womöglich zu vernichten, sucht der 
Socialismus deren Fundament, die Ehe, zu untergraben und zu 
diesem Zwecke scheint ihm die Abschaffung derselben alv politischer, 
religiöser, juridischer und bürgerlicher Institution als wirksamstes 
Mittel. Es wird ganz richtig gefolgert, daß, sobald der Ehe der 
staatliche und religiöse Schutz entzogen wäre, ihrer jederzeitigen 
Wiederauflösung also keinerlei gesetzliche und moralische Hindernisse 
entgegenstünden, künftighin nur noch solche Ehebündnisse von Dauer 
sein würden, welche aus wahrer, auf gegenseitiger Achtung beruhen 
den Liebe geschlossen werden, oder wenn gegenseitige Unentbehr 
lichkeit der Ehegatten oder die Liebe zu den Kindern das Band der 
Ehe gefesügt hätte. Daß aber dergleichen Ehen in einem Commune- 
staate immer seltener werden und nach und nach an deren Stelle 
die auf der Basis der freien Liebe, d. h. des Sinnenrausches ge 
knüpften geschlechtlichen Verbindungen treten, und diese dann wieder