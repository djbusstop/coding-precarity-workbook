8* 
115 
freundlichen Ultramontanen, die Förderer des religiösen Unfriedens 
in Preußen. Da jedoch diese wie die übrigen reichsfeindlichen 
Parteien ihre Existenz durch jene bedroht sehen und zu der Ueber 
zeugung gelangt sind, daß einer solchen Gefahr nur durch die Ein 
führung von nothwendigen Socialreformen vorgebeugt werden kann, 
so läßt sich hoffen, daß sic gemeinsam die Neichsrcgierung, wenn 
auch widcrlvillig, in ihren Rcformbestrebungen, wie schon jetzt er 
sichtlich, unterstützen werden. Die Socialdemokraten könnten zwar, 
wenn die Zerfahrenheit und Uneinigkeit in den Reihen ihrer Gegner 
fortdauert, diese durch Ueberrumpeluug niederwerfen, allein ein 
lebensfähiges commnnistischcs Reich vermögen sie aus den oben 
dargelegten Gründen nimmer zu errichten, vielmehr würden sie 
dasselbe bald wieder zusammenbrechen sehen; ein Beispiel der 
raschen Vergänglichkeit socialdemokratischer Regierung hat in neuerer 
Zeit die Pariser „Commune" (1871) gezeigt. — Was nun die 
andauernde Störung des religiösen Friedens anlangt, so kann 
sic zweifelsohne die sittliche und materielle Wohlfahrt eines 
Staates auf unabsehbare Zeiten ernstlich gefährden. Als in Folge 
der Revolution von 1848 das Prestige der römischen Curie mehr 
und mehr sank und ihr sogar die weltliche Herrschaft über den 
Kirchenstaat verloren ging, sannen die Jesuiten darauf, für das 
Papstthum nicht nur Beides wieder zu gewinnen, sondern auch 
dasselbe zu einer solchen Omnipotcnz empor zu heben, wie sie der 
ehr- und hcrrschsüchtigc Papst Gregor VII. erreicht hatte. Zur 
Vornahme der zu diesem Zwecke für dienlich befundenen Experimente 
schien das wieder erstandene Deutsche Reich unter einem protestan 
tischen Kaiser ìmd insbesondere das Königreich „Preußen" das ge 
eignetste Versuchsfeld zu bieten. Die Jesuitcupartci übernahm die 
Leitung. Sie provozirtc den s. g. Culturkampf gegen Preußen und 
um in demselben eine möglichst scharfe Waffe zu besitzen, glaubte 
sie eine solche in dcm Dogma der Unfehlbarkeit des Papstes ge 
funden zu haben; und deshalb ward es verkündigt, trotz der ener 
gischen Einsprache mehrerer Bischöfe und ihrer Vorhersagung, daß