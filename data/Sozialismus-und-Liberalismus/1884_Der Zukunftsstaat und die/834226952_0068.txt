56 
noch sehr fraglich, und noch ungewisser ist es, über welche 
Heerschaaren diese im gekommenen Augenblicke mit Bestimmtheit 
würden verfügen können, während ans gegnerischer Seite sieg 
gewohnte Streiter mit kriegskundigen und geschickten Anführern 
stehen. Außerdem besteht zwischen dem lediglich auf Umsturz, 
Raub und Eroberung bedachten Angreifern und den für ihre 
theuersten Güter: Religion, Freiheit, Familie und Eigenthum 
kämpfenden Angegriffenen bezüglich der Tapferkeit, der Aus 
dauer und des Muthes ein gewaltiger Unterschied; jene würde 
schon das unklare Bewußtsein des ungerechten Angriffs auf 
ihre Mitbürger, die ihnen nichts zu leide gethan haben, mit 
einer gewissen Zaghaftigkeit erfüllen, und wenn sie gar den 
Gegner nicht gleich beim ersten Ansturm über den Haufen 
rennen oder vielleicht ein paar mal unter starken Verlusteil 
zurückgeworfen wiirden, müßten sie bald den Ninth verlieren 
und sich dann in die Defensive drängen lassen, schließlich die 
Waffen strecken oder sich durch die Flucht zu retten suchen. 
2. Nach der von der Socialdemokratie unbeanstandet gebilligten 
Marx'schen Theorie soll der Werth und somit auch der Preis 
eines Dinges oder Products gleich sein der auf seine Her 
stellung zu verwendenden gesellschaftlich ermittelten Arbeitszeit 
und dieser Werth dem betreffenden Arbeitsindividuum dem ver 
kündeten socialistischen Programm gemäß als Arbeitsertrag 
voll gewährt werden. 
Hierbei drängen sich uns ganz von selbst die Fragen auf: 
a. Wie hoch ist der Geldwerth der Arbeitszeit z. B. für jede 
Stunde zu bemessen? Soll jede Arbeitsleistung ohne Rück 
sicht auf Geschicklichkeit und Fleiß der Arbeiter, ohne Be 
rücksichtigung der Localverhältnisse, Nützlichkeit, Noth 
wendigkeit oder des Gebrauchswerthes der Güter gleich- 
werthig sein? 
d. Woher soll der Staat als Gcneralbesitzcr aller Betriebs 
mittel für die hergegebenen Rohstoffe und die geliehenen