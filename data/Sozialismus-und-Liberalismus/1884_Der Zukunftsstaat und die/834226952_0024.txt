12 
mit kapitalistischen Völkerschaften zur Ausgleichung der Ueber- 
bilanz der Aus- imb Einfuhr durchaus nöthig hatte und 
dasselbe daher schon zu diesem Zwecke von den Privaten besten 
falls gegen Entschädigung mit Arbeitsgeld einzuziehen gezwungen 
wäre. 
5. Da für die Privaten der Spcculationshandcl und die 
industriellen Unternehmungen wegfielen, so könnte cs auch 
keinen Markt und feilte Börse mehr geben. 
6. Es fiele aber nicht nur der Groß- und Kleinhandel sammt 
dem sterilen unprvductiven Zwischenverkehr und der Handelscon- 
currenz, sondem auch das ganze kostspielige itnd die Waare ver- 
theuernde Annoncen- und Schaustellungswesen mit den enormen 
Gewölbe- unb Magazinkosten, sowie die meist wahrheitswidrigen 
Reklamen, d. h. das Judenthum in der Presse fort. 
Dieser Conseguenzen des Kernes des international - commu- 
nistischen Socialismus sind sich, nach ihren Reden und Schriften 
zu urtheilen, wohl nur wenige Führer bis jetzt klar bewußt, die 
jenigen von ihnen aber, welche diese Conseguenzen zu ziehen ver 
mögen, haben sich bisher wohlweislich gehütet, mit einem noth 
wendig darauf zu basirenden Organisationsplane ofien hervorzutreten. 
Sie befürchten mit Grund, daß diejenigen Anhänger der Social 
demokratie (also mindestens dreiviertel von ihnen), welche, wenn 
auch nur ein kleines Besitzthum oder einige Ersparnisse in Metall- 
geld haben oder doch zu ererben hoffen, keines von beiden selbst 
gegen genügende Entschädigung durch Arbeitsgeld sich würden 
nehmen lassen wollen, sondern höchst wahrscheinlich gänzlich zurück 
träten. Daher herrscht darüber tiefes Schweigen; man wartet bis 
nach vollbrachtem Umsturz der heutigen Staatsformen in der Hoff 
nung, alsdann alle Besitzenden zwingen zu können, sich in die 
Wegnahme ihres Eigenthums und Verwandlung desselben in (Ge 
meingut zu fügen. 
Die übrigen Punkte des vorgedachten Programms sind in 
Bezug auf die äußere Umgestaltung des heutigen Staatssystems