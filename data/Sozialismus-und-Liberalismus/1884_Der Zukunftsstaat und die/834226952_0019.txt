7 
schweig einstimmig zum Vercinspräsidenten erwählt und in Folge 
seiner anläßlich der ihm von Liebknecht, Bebel und v. Hochstetten 
in der Generalversammlung in Basel am 28. März 1869 beige 
brachten Niederlage an das souveräne Arbeitervolk gerichteten Appel 
lation ward er in einer Urversammlung nicht allein fast einsttmmig 
abermals zum Präsidentcu gewählt, sondern es wurden auch Lieb 
knecht und Bebel für unwürdig erklärt, jemals wieder in einer 
Arbcitcrversammlnng zu erscheinen, v. Schweitzers letzter Triumph 
erfolgte in der Generalversammlung zu Berlin am 18. Januar 1870, 
indem er sämmtliche Gewerkschaften zu einem allgemeinen Arbeiter- 
unterstützungsverband vereinigte. Er trat Ende März 1871 von 
der Leitung des neuen Vereins mit der Ueberzeugung zurück, daß 
auf treue Anhänglichkeit einer Partei von Arbeitern an ihre Führer 
nicht zu rechnen sei. Bereits am 30. April 1871 ließ er die von 
ihm gegründete periodische Zeitschrift „den Socialdemokraten" und 
„den Agitator" eingehen, v. Schweitzer war ein außerordentlich 
begabter und kenntnißreicher Mann; dabei besonnen und positiv, 
sah er die Dinge, wie sie in Wirklichkeit waren; da er zugleich eine 
unglaubliche Geduld und Zähigkeit, die durch keinen Mißerfolg, 
durch keine Niederlage zu beugen oder gar zu brechen war, besaß, 
so war er gewiß der Mann, den seine Partei im gegebenen Mo- 
mente nöthig hatte. Er war im Grunde keine gemeine und unedle 
Natur; er erstrebte wie Lassalle aufrichtig eine günstigere Lage des 
Arbciterstandes und setzte dabei gleichfalls seine Hoffnung auf den 
Preußischen Staat. 
Liebknecht und Bebel gründeten nach ihrer Ausstoßung aus 
dem Allgemeinen deutschen Arbeiterverein bereits am 7. August 1869 
zu Eisenach mit den von Schweitzer abgefallenen Mitgliedern der 
Genfer Section der Internationale, sowie mit den österreichischen 
und schweizerischen Arbeitervereinen die social-demokratisch-inter- 
nationale Arbeiterpartei mit 262 Delegirten und stellten folgendes 
Programm auf: