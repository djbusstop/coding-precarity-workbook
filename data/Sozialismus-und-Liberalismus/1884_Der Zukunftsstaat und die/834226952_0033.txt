21 
zugemuthet, diese ist eine so seltene Tugend, daß sie meist nur von 
Willensstärken, charakterfesten und ehrenhaften Männern geübt wird. 
Am wenigsten wird sic sich aber bei einer Jugend sinden, welche 
in den jede göttliche und staatliche Autorität untergrabenden Grund 
sätzen der heutigen Socialdemokratie erzogen ist. Gerade diese würde 
in ihrer Zuchtlosigkeit weit entfernt sein, sich dem Berufszwange 
zu fügen. Während sonach die Staatsleitung aus Staatsraison 
am Principe des Berufszwanges festhalten müßte, würde von 
der Staatsbürgerschaft der Durchführung dieses Princips ein so 
gewaltiger Widerstand entgegengesetzt werden, daß trotz einer 
Schreckensherrschaft der neue Staat sehr bald wieder äus den 
Angeln gehoben würde. 
5. Der Drang nach Bedarfsfreiheit. 
Eine andere Art von Freiheit wäre hier noch zu erwähnen, 
an welcher der Mensch gleichfalls mit aller Zähigkeit festhält und 
solche sich nicht nehmen oder verkümmern lassen will, das ist die 
Bedarfsfreiheit. Der österreichische Minister a. D. Dr. A. Schaffte, 
obgleich die Socialdemokraten ihn als ihren Anwalt ansehen, sagt 
in seiner Broschüre „Die Quintessenz des Socialismus" u. A.: 
„Die Freiheit der Bedarfsbcstimmung ist unzweifelhaft die unterste 
Grundlage der Freiheit überhaupt. Würden die Lebens- und Bil- 
dungsmittcl von außen her und einem Jeden nach einem Bedarfs 
schema zugemessen, so könnte Niemand nach seiner Individualität 
uud Neigung leben und sich ausbilden; es wäre der Brotkorb der 
Freiheit beseitigt. Wollte aber der Socialismus die persönliche 
Freiheit der Bedarfsbcstimmung aufheben, so wäre er freiheits- 
feindlich und aller Gesittung entgegen." Der Trieb nach persön 
licher Freiheit im Menschen ist jedoch so mächtig und dieser für 
ihn so werthvoll, daß er ihm selbst für die größten Vortheile, 
welche der künftige Communcstaat bieten könnte, nicht feil wäre 
und letzterer daher mit dem Freiheitsdrange niemals fertig werden 
würde. Mit diesem Drange nach Freiheit und der damit ver-