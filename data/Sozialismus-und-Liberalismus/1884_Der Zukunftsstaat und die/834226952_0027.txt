15 
verlangen und sich dabei gleichfalls auf die versprochene Gerechtig 
keit berufen. 
Also auch im Zukunftsstaate wird die Regierung, mag fie nun 
die abgestufte oder die gleiche Vertheilung des Arbeitsoertrags 
wählen, nicht im Stande sein, alle Arbeiterkreise zufrieden zu stellen, 
es sei denn, daß es ihr gelänge, sämmtliche Zukunftsstaatsbürger 
dahin zu bringen, daß der Gemcinsinn alle ihre Handlungen be 
herrsche, daß Jeder zu Gunsten des Gemeinwohles auf jedes 
Sonderinteresse verzichte, mit anderen Worten: daß er die höchste 
moralische Vollkommenheit erreiche. Hat aber nicht gerade die 
socialdemokratische Presse durch Erregung der schlimmsten Leiden 
schaften: des Neides, des Hasses und der Habgier in den Herzen 
der Arbeiterklassen, der sog. Enterbten, die Wurzeln des Gcmein- 
sinns gänzlich durchschnitten? 
2. Der Ehrtrieb. 
Ein zweiter sehr mächtiger Trieb im Menschen ist der nach 
Ehre, d. h. nach der guten Meinung seiner Mitmenschen. Der 
selbe ist schon bei Kindern als Zeichen des beginnenden Verstandes 
wahrzunehmen und bildet bei den Erwachsenen mehr oder weniger 
die Richtschnur ihres Lebens. Ist das Ehrgefühl in einem Menschen 
wenig entwickelt oder durch schlechte Neigungen erstickt, so charakterisirt 
er sich als ein verächtlicher Lump. Das lebendige Ehrgefühl kann 
sich nach verschiedenen Richtungen hin entfalten; bei beschränkten 
Naturen führt es zur Eitelkeit und zum Hochmuth; bei weltklugen 
Leuten zum Ehrgeiz und zur Herrschaft und nur bei wenigen ist 
dasselbe darauf gerichtet, sich bei ihren Mitmenschen Achtung, Liebe 
und Vertrauen zu erwerben. 
Da die Mehrzahl der Menschen sich in der Allgemeinheit be 
deutungslos fühlt, so sucht der Einzelne mit Andern gemeinsam die 
Anerkennung, welche er für sich allein nicht beanspruchen kann, in 
der Standesehre. Er schließt sich daher den Gleichgestellten an 
und bildet mit ihnen einen Stand. Jeder Stand beansprucht seine