﻿44

der Bukowina imstande wären, nicht etwa bloß den
Bauer zu heben, sondern auch eine Großindustrie
zu schaffen. Das ist allerdings ungeheuer schwierig,
denn eine Großindustrie läßt sich nicht plötzlich aus
dem Boden stampfen. Aber das Land hat natürliche
Schätze in Hülle und Fülle, die es schlecht oder gar
nicht verwertet. Ich erinnere an den ungeheueren
Waldreichtum der Bukowina, der dazu verwendet
wird, fremde Papier- oder Zellulosefabriken zu füt-
tern, anstatt im Lande verwertet zu werden. Ich erin-
nere an den großartigen Obstreichtum der Bukowina.
Könnte er nicht für eine große Konfekt-, Kompott-
und Marmeladeindustrie verwertet werden? In Eng-
land, Frankreich, der Türkei und Südtirol leben ja
ganze Landstriche von der Konfekt-, Kompott- und
Marmeladeindustrie. Und wir? Wir führen unser
Obst in rohem, wirtschaftlich möglichst unergiebigem
Zustande nach dem Auslande aus. Und dabei ver-
stehen unsere Damen aus Weichsein, Pfirsichen,
Kirschen, Marillen, Erdbeeren, Stachelbeeren, Him-
beeren, grünen Zwetschken usw. ein Konfekt, Dult-
schetz genannt, zu machen, das nach meinen Erfah-
rungen zu den besten der Welt gehört, viel besser als
alles, was England, Frankreich, Südtirol und die Tür-
kei erzeugen und womit sie Millionen verdienen. Es
ist zweifellos, daß unser Boden Mineralschätze birgt,
die von ungeheurem Werte wären, haben wir auch
keine Kohlen, so könnten wir doch Industrien begrün-
den, die entweder keine Kohlen brauchten, oder mit
Naphthaabfällen betrieben werden könnten. Was ist
denn mit der Konfektionsindustrie? Überall, wo die