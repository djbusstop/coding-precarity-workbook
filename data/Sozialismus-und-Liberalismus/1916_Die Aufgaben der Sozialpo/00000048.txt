﻿tungen dem russischen und polnischen Juden gleich-
geartet. Und dieser Jude, dem man nach den hiesigen
Erfahrungen höchstens einige Eignung zum kleinen
Handwerker zuerkennen würde, ist nach England und
Amerika ausgewandert und wurde dort in vielen
Industriezweigen der gesuchteste und geschätzteste
Arbeiter, ja, in gewissen Geschäftszweigen beherrscht
er geradezu den Arbeitsmarkt. Sehen Sie sich ein-
mal Whitechapel an, die bekannte Londoner Vorstadt.
Noch vor 20 Jahren war sie die Zufluchtsstätte des
allerärmsten Londoner Arbeiterproletariates, zum
Teile sogar des verworfensten Gauner- und Ver-
brechergesindels; heute ist sie ein blühendes Indu-
strieviertel. Und wer hat sie dazu gemacht ? Die ver-
achteten, verschrienen russischen und polnischen
Juden. Die Engländer, die nach vielen Richtungen
viel billiger denken als man es auf dem Festlande zu
finden gewohnt ist, erkennen es rückhaltlos an, daß
viele Industrien, zumal in Whitechapel, aber auch in
andern Gegenden Englands durch die polnischen und
russischen Juden ermöglicht, zum Teile sogar begrün-
det worden sind, sie erkennen auch rückhaltlos an,
daß das Land ihnen ein Stück seines Wohlstandes ver-
dankt. Kommt man nach Whitechapel, so glaubt man
sich geradezu nach Herzls Judenstaat versetzt. Jüdi-
sche Aufschriften überall, jüdische Anschläge auf
den Anschlagssäulen, jüdische Theater, jüdische
Bibliotheken, Vortragshallen, jüdische Zeitungen,
darunter, wie ich hörte, einige und zwanzig jüdische
Sportzeitungen; alle Gemeindebeamten, alle Police-
mans sprechen Jargon, das sog. „Yiddisch“; andere