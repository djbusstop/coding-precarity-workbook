﻿26

sehen Werke, das in dänischer Sprache geschrieben
und „Dänemarks Kultur im 19. Jahrhundert“ betitelt
ist, ist angegeben, was für Bücher bestellt worden
sind in einem gewöhnlichen Landstädtchen von 14 000
Einwohnern in einer der drei dort bestehenden Buch-
handlungen. In jeder dieser drei Buchhandlungen des
dänischen Landstädtchens sind mehrere Abonne-
mentswerke bestellt im Preise von 100—200 däni-
schen Kronen. (100 dänische Kronen sind 66 Gulden.)
Das ist selbstverständlich sonst nirgends als in Däne-
mark möglich. Der Erfolg ist die blühende dänische
Landwirtschaft. Darüber sind sich die Dänen voll-
kommen im' Klaren: In erster Linie verdanken sie
ihre Landwirtschaft der ungewöhnlichen Intelligenz
des Bauern.

Ich will dabei nur das eine hervorheben: das
dänische landwirtschaftliche Genossenschafts-
wesen. Sie haben in Dänemark keine Gemeinde, die
nicht eine landwirtschaftliche Genossenschaft hätte
und zwar eine landwirtschaftliche Produktivgenossen-
schaft. In Dänemark wird die beste Butter der Welt
erzeugt und zwar nicht etwa von Gutsbesitzern, son-
dern von Bauern und nicht etwa von einzelnen
Bauern, sondern alle Bauern Dänemarks sind in wirt-
schaftliche Produktivgenossenschaften zusammenge-
schlossen, die diese Butter erzeugen und die mit ihrer
Butter ganz England versorgen; in ganz England er-
hält man dänische Butter. Solche Produktivgenossen-
schaften haben sie auch für Schweine. Die Bauern
schließen sich zusammen, gründen Schweinezüch-
tungs- und Schlachtungsgenossenschaften. Die