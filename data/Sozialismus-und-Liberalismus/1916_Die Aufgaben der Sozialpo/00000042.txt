﻿40

semitismus sind mir vollständig gleichgültig. Ich
will Ihnen nur die Wahrheit sagen, damit Sie wissen,
daß die Juden vor einer Katastrophe stehen, die spä-
testens in zehn oder zwanzig Jahren zu erwarten ist.
Von den 100 000 Juden der Bukowina haben viel-
leicht 10 000 Stellungen und Berufe, die ihre wirt-
schaftliche Grundlage und Berechtigung haben.
90 000 haben ihre Stellung der Zurückgebliebenheit
des Landes zu verdanken. Wie sich nun das Land
hebt, wird die wirtschaftliche Mündigkeit dieser ihrer
Stellungen aufhören und sie müssen nach etwas ande-
rem suchen. Im Zusammenhang damit steht die
Flucht der Juden in die gelehrten Berufe. Sie werden
staunen: An unserer Universität sind 50 o/o jüdische
Studierende, obwohl sie in der gesamten Bevölkerung
höchstens 10 o/0 ausmachen. Warum ist das? Des-
wegen, weil die Juden es fühlen, daß es in den Be-
rufen, die sie bisher gehabt haben, einfach nicht
weiter geht. Und das ist ganz richtig.

Ich möchte nicht, daß aus dem, was ich soeben
gesagt habe, irgendwelche Folgerungen im Sinne des
Antisemitismus gezogen werden. Zunächst muß ich
nachdrücklichst hervorheben, daß die Wucherer,
Schänker und Geschäftsvermittler im ganzen euro-
päischen Osten zwar einen bedenklich großen, aber
nirgends einen überwiegenden Teil der jüdischen Be-
völkerung ausmachen. Die große Masse sind Kauf-
leute, Handwerker, Arbeiter, Taglöhner und An-
gestellte, die zwar sehr unter der Ungunst der Ver-
hältnisse leiden, sozusagen von der Hand in den
Mund leben, aber zu Betrachtungen vom antise-