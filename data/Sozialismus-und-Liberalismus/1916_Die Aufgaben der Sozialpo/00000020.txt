﻿18

daß auch diese großartige Maßregel zweifellos zu-
sammenbrechen wird, wenn sie nicht von Reichs-
wegen gehalten werden sollte. Und dasselbe kann
ich wohl auch von allem andern sagen. Unsere Ge-
werkschaften, die auch eine Übertragung von Ein-
richtungen, die sich im Westen bewährt haben, in
die Bukowina sind, mögen vielleicht irgend eine Be-
deutung haben, sie ist aber gewiß nicht groß: mit
der Bedeutung, die ihnen im Westen zukommt, ist
sie gewiß nicht zu vergleichen. Von dort wird das
Heil nicht kommen. Warum? Weil, wie gesagt, die
Bukowina nicht so weit vorgeschritten und der Reich-
tum nicht genügend entwickelt ist, damit davon ein
größerer Anteil auch auf die unteren, gesellschaftlich
zurückgesetzten Klassen zurückfallen würde. Wir
können es einfach nicht. Wir haben allerdings in der
Bukowina eine sozialdemokratische Partei und sogar
auch einen sozialdemokratischen Abgeordneten im
Reichsrat. Aber ich glaube nicht, daß meine Ansicht
widerlegt, ist. Der sozialdemokratische Abgeordnete
ist, soviel ich weiß, in erster Linie von Handwerkern
und Kleinbauern in Rosch gewählt und vertritt in-
folgedessen gar nicht die Interessen der eigentlichen
Arbeiterschaft. Und wenn er die Interessen seiner
Wähler vertreten wollte, so könnte er gar nicht So-
zialdemokrat sein, denn diese sind eben keine Arbei-
ter. Der Sozialismus in der Bukowina, wenn er über-
haupt etwas bedeutet, so bedeutet er weiter nichts
als den politischen Radikalismus und unsere sozial-
demokratische Partei ist, was allerdings auch für
andere Teile Österreichs zutrifft, ebenso wie für