﻿25

vor 100 oder 150 Jahren war der Bauer in Dänemark
gerade so elend wie in der Bukowina. Es war gerade
ein solcher Jammer, wie heute in der Bukowina. Da
habe ich mich gefragt: Wie haben es die Leute dazu
gebracht, daß sie heute das sind, was sie eben sind.
Die Antwort, die mir die Dänen darauf gegeben haben
und die ich aus dänischen Werken geschöpft habe,
war, daß in erster Linie dies das Ergebnis der Bemü-
hungen von Grundtvig sei, eines pietistischen
Schriftstellers und Politikers, der in den 30 er Jahren
des vorigen Jahrhunderts gewirkt hat. Sein Werk
sind die über das ganze Land verstreuten Volkshoch-
schulen, eine Art von Schulen zur Ausbildung für die
Bauern und Bauernsöhne, wo sie insbesondere die
sechs Wintermonate zubringen und alle möglichen
Sachen, die zur allgemeinen Bildung gehören, auch
landwirtschaftliche Gegenstände, lernen. Es wird
aber mehr Wert gelegt auf die allgemeine Bildung, als
auf das, was zur landwirtschaftlichen Bildung gehört.
Diese Volkshochschulen haben zunächst den Erfolg
gehabt, daß sie ungeheuer die Intelligenz des Bauers
gehoben haben. Der dänische Bauer gehört zu den
intelligentesten Bauern der Welt, wie ich mich selbst
zu überzeugen Gelegenheit gehabt habe. Ich habe in
einem dänischen Buche ein Verzeichnis von Büchern
gesehen, die in einer Durchschnittsbibliothek eines
gewöhnlichen Bauern sind. Sie würden, meine sehr
Verehrten, die Hände über den Kopf zusammenschla-
gen, wenn Sie vernehmen, was ein jüttischer Durch-
schnittsbauer in seiner Bibliothek hat. Jeder dänische
Bauer hält zwei bis drei Zeitungen. In einem däni-