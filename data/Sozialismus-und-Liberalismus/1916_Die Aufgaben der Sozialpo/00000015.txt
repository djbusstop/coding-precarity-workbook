﻿Bibliothek des Instituts
für Weltwirtschaft Kiel

13

Ein österreichischer Politiker ist zunächst ein
Mann, der etwas bekämpft. Ob er etwas Posi-
tives will, das ist ihm und seinen Wählern gleich-
gültig. Der eine bekämpft die Polen, der andere die
Czechen, der dritte die Juden, dann wieder die
Ungarn oder die Slovenen; dann kommen welche, die
die Kirche, die Religion oder die Armee bekämpfen.
Unlängst hat mir einer gesagt, man müsse das Nota-
riat bekämpfen. Eigentlich hatte der Mann recht,
denn das Notariat wurde bisher noch sehr wenig be-
kämpft, Nebenbei gesagt, halte ich unser Notariat
wenigstens so, wie es sich im Westen Österreichs
entwickelt hat, für eine der nützlichsten und fast
tadellos funktionierenden Einrichtungen unseres
Rechtslebens.

Nun, ich möchte wiederholen, was ich bereits ge-
sagt habe: Die Sozialpolitik ist positive Arbeit.
Wenn wir wirklich Sozialpolitik betreiben, so müssen
wir zunächst trachten, den Reichtum des Volkes, den
Reichtum des Landes zu heben und das bedeutet in
erster Linie die Verwertung aller Kräfte, die im Lande
vorhanden sind. Alle diese Kräfte müssen den
großen Zielen des Landes dienstbar gemacht werden.
Uas evrige Bekämpfen aber ist eine Vergeudung und
nicht eine Verwertung der Kräfte. Ich habe mich
viel mit der Bekämpfungspolitik, die für unsern Staat
so ungeheuer bezeichnend ist, beschäftigt und glaube,
wenn diese Kräfte, die für das ewige Bekämpfen ver-
wendet werden, für dieses Bekämpfen, das eigentlich
nach meiner besten Überzeugung hauptsächlich in der
schrecklichen Ideenarmut unserer Politiker, denen