﻿16

Wo nehmen Sie nun aber solche her, die Sie doch
haben müssen ? Sie können sich doch nur des Pfarrers
und des Lehrers bedienen, der Einzigen, die Sie selbst
im letzten Gebirgsdorf finden können. Einmal wird
sich der Lehrer, ein anderesmal der Pfarrer besser
eignen; das wird von den Verhältnissen abhängen.
Wenn Sie aber den Klerikalismus bekämpfen, dann
müssen Sie sofort auf den Pfarrer verzichten; denn
der Pfarrer, das ist ja der leibhaftige Klerikalismus.

Ich möchte jetzt etwas näher ins Einzelne ein-
gehen. Zunächst muß ich einige Worte über die
Ihnen übrigens bekannte gesellschaftliche und wirt-
schaftliche Schichtung der Bukowina sagen. Wir
haben hierzulande vor allem den Bauer, dann den
Großgrundbesitzer, ferner den Handwerker, den
Kaufmann, außerdem die liberalen Berufe und zum
Schluß — last not least — die Juden.

Wenn Sie dieses Bild der gesellschaftlichen und
der wirtschaftlichen Schichtung in der Bukowina
überblicken, so finden Sie einige Eigentümlichkeiten.
Zunächst finden Sie das nicht, worüber außerhalb
der Bukowina in erster Linie gesprochen wird, wenn
von der Sozialpolitik überhaupt gesprochen wird,
nämlich den Arbeiter. Warum finden Sie ihn nicht
in der Bukowina? Aus einem ungeheuer einfachen
Grunde. Weil der Arbeiter im westlichen Sinne, der
Arbeiter, um den sich zunächst jede Sozialpolitik
kümmert, der Eabriksarbeiter ist. Und diesen Ar-
beiter, den Fabriksarbeiter, gibt es in der Bukowina
nicht, weil es hier keine Großindustrie gibt, die den
Fabriksarbeiter nähren könnte. Die Folge davon ist,