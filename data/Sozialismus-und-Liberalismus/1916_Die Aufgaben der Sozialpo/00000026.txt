﻿weise hier fast unbekannten Aufsatz des Wiener Pro-
fessors Philipovich in der „Österreichischen Rund-
schau“ über seine Reise nach Kanada. Lesen Sie das
vor allem, meine Herren von der ruthenischen Natio-
nalität, damit Sie sich überzeugen, was aus dem ruthe-
nischen Bauer werden kann. Würden Sie es glauben,
daß der ruthenische Bauer, von dem viele mit solcher
Verachtung sprechen, in Kanada der geschätzteste,
tüchtigste, sparsamste und genügsamste Landwirt ist,
daß die ödesten Strecken dem ruthenischen Bauer zur
Bewirtschaftung übergeben werden und daß er sie
im Verlaufe von zehn bis fünfzehn Jahren in die
fruchtbarsten Gefilde verwandelt? Wissen Sie, daß
sich die kanadischen Staaten um den ruthenischen
Bauer geradezu reißen, um diesen ruthenischen
Bauer, der hier in Hunger und Elend verkommt, wäh-
rend er dort, vor große Aufgaben gestellt, so Groß-
artiges leistet ?

Wie könnte man nun diesem Bauer helfen? Mehr
Grund und Boden könnte man ihnen kaum ver-
schaffen, wenigstens nicht für den Augenblick ; aber
man könnte ihre Arbeit intensiver machen. Ich war
vor zwei Jahren in einem Lande, das sich dessen
rühmen kann, die blühendste Landwirtschaft in Eu-
ropa zu haben, die einzige Landwirtschaft, die über
ihr Schicksal nicht klagt und das ist Dänemark,
das Paradies der Bauern. Dieses Dänemark — ich
habe mich nicht nur mit der Agrarfrage in Dänemark
beschäftigt, sondern auch viele geschichtliche Werke
gelesen — dieses Dänemark ist heute das Land, das
in agrarischer Beziehung am höchsten steht. Aber