﻿die Polen, Südrussen, Ruthenen, Rumänen und teil-
weise auch für die Ungarn handelt es sich um Leben
oder Tod. Gelingt es ihnen, die Judenfrage zu einer
friedlichen und gedeihlichen Lösung zu bringen, dann
steht ihnen, nach meiner besten Überzeugung, eine
Zeit wirtschaftlicher und kultureller Blüte bevor, die
nichts nachgibt der höchsten Blütenentfaltung irgend-
eines andern Volkes der Welt. Die Juden, in ihren
eigenen Volkskörper aufgenommen und angeglichen,
könnten ihn mit einer ganzen Reihe von intellektu-
ellen und wirtschaftlichen Kräften ausstatten, die ihm
bisher noch gefehlt haben. Gelingt ihnen diese Art
der Lösung der Judenfrage nicht, dann zweifle ich
nicht daran, daß sie sich daran verbluten. Die fort-
währenden Kämpfe und Stürme, wilden Ausbrüche
und ohnmächtigen Zuckungen werden für absehbare
Zeit der wirtschaftlichen Entwicklung den Weg ver-
legen, das politische und gesellschaftliche Leben ver-
giften. Glauben Sie nicht, daß wir heute schon wirt-
schaftlich viel weiter wären, wenn wir es dazu
bringen könnten, daß in Laden und Werkstätte jü-
dische und christliche Arbeiter und Angestellte neben-
einander ihr Werk verrichten würden?

Und nun möchte ich allen denen, die soviel von
der Judenfrage reden, die Juden wegen ihres Reich-
tums verunglimpfen und beneiden, sagen, daß es viel-
leicht in ganz Europa kein größeres Elend gibt, als
es unter den Juden des Ostens herrscht. Selbst das
tiefste Lumpenproletariat Süditaliens und Spaniens
dürfte ihnen, zumal, wenn man das günstigere Klima
in Rechnung stellt, in Lebenshaltung und Einkommen