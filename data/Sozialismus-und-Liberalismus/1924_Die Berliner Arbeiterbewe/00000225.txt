﻿212

Johannisthal, Mariendorf und Marienfelde, Neuendorf bei Potsdam und
Nowawes, Niederschöneweide, Schenkendorf bei Königs-Wusterhausen und
Treptow genannt. Einige dieser Orte haben sich aus reinen Landgemeinden
zu Jndustriedörfern entwickelt, andere haben Fabriken entstehen sehen, wo
früher kleinbürgerliche Betriebe vorherrschten, und wieder andere sind aus
Vergnügungsplätzen zu Wohnstätten der in der Nachbarschaft beschäftigten
Arbeiter geworden. Immer stärker macht sich die wirtschaftlich-soziale Am-
wälzung in den örtlichen Verhältniffen geltend, immer mehr Orte zieht sie
aus der Abgeschlossenheit und dem Stilleben in das bewegte Treiben der
großen Verkehrswirtschaft hinein.

Der Kandidat der Sozialdemokratie war 1890 in diesem Kreise Buch-
drucker Wilhelm Werner, seit 1893 ist sozialdemokratischer Kandidat und
Vertreter Fritz Zubeil, gelernter Klavierarbeiter, dann Gastwirt und heute
Expedient.	*	.

Überblicken wir nun das Gesamtbild der Ergebnisse der in unsere Pe-
riode entfallenden Reichstagswahlen Groß-Berlins, so erzählt es uns von
einem Aufschwung der sozialdemokratischen Stimmen, der das phänomenale
Wachstum der Bevölkerung dieses politischen Zentrums noch weit hinter sich
läßt. Eine Gegenüberstellung der Wahlstärke der drei großen politischen
Gruppen Sozialdemokratie, Liberalismus (Freisinnige und National-
liberale) und Konservatismus (Konservative, Mittelständler, Antisemiten
und Zentrum) zu Beginn und Abschluß der Periode ergibt folgendes Bild:

	1890	1907	Absolute	Prozentuale
			Zunahme	Zunahme
Liberalismus. . .	86 945	128240	+ 41295	47,4
Konservatismus. .	68175	130277	+ 62105	91,1
Sozialdemokratie	158 848	412 998	+ 254 150	160,0

Während die Sozialdemokratie ihre Stinnnen fast verdreifachte, konnten
Liberalismus und Konservatismus die ihren nicht einmal verdoppeln. Zu
Anfang der Periode hatte die Sozialdemokratie schon mehr Stimmen, als
die zwei anderen Gruppen zusammen, erhielt aber von den acht Mandaten
Groß-Berlins nur zwei. Am Ausgang aber fielen ihr sieben Mandate zu,
während bei zahlengerechter Verteilung, soweit Groß-Berlin allein in Betracht
kam, ihr nur fünf von den acht Mandaten gebührt hätten. Das ist jedoch
nur eine winzige Entschädigung für die ungerechte Verteilung der Mandate
im Reich. Lerrschte für das ganze Reich zahlengerechte Verteilung, so
würde Groß-Berlins Sozialdemokratie im Jahre 1907 statt sieben über
fünfzig Reichstagsmandate erhalten haben. In diesem Soll erst
veranschaulicht sich die Größe des Werks ihrer von Wahl zu Wahl sorg-
fältiger ausgebauten Organisation und einer mit steigender Wucht betriebenen
Wahlarbeit.

j0