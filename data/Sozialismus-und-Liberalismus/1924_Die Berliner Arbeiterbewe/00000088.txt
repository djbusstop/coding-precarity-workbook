﻿81

was nur irgend darauf aussah, als könne es einen solchen Nachweis unter-
stützen, wie Kassenbücher der Wahlvereine, Abrechnungstabellen von Samm-
lungen für irgendwelche Zwecke, Sammellisten, Notizbücher, Sammelbons
sowie in vielen Fällen auch Privatbriefe, wurde beschlagnahmt. Wo man
nicht gründlich genug gesucht zu haben glaubte, ward einige Tage später
noch Nachhaussuchung gehalten. So am 27. November bei Stefan Fritz,
dem Vertrauensmann für den dritten Berliner Reichstagswahlkreis, und
am 29. November bei Paul Singer.

An diesem letzteren Tage war man auf dem Polizeipräsidium so weit,

auf das Vorspiel die eigentliche Landlung folgen zu lassen. Sie wurde

tags darauf durch den Reichs- und Staatsanzeiger bekanntgegeben und

lautete:	,	.	,

Bekanntmachung.

Es wird hiermit zur öffentlichen Kenntnis gebracht, daß nachstehende
Vereine:

der Berliner Mitglieder der
sozialdemokratischen Partei
Deutschlands,

1—6. Die sechs sozialdemokratischen Wahlvereine fürdiesechs
Berliner Reichstags-Wahlkreise,

7.	die Preßkommission

8.	die Agitationskommission

9.	die Lokalkommission

10.	der Verein „öffentlicher
Vertrauensmänner"

11.	der Parteivorstand der sozialdemokratischen Partei
Deutschlands

auf Grund des 8 8 der Verordnung über die Verhütung eines die gesetzliche
Freiheit und Ordnung gefährdenden Mißbrauchs des Versammlungs- und
Äereinsrechts vom 11. März 1850 vorläufig geschlossen sind.

Jede fernere Beteiligung an diesen Vereinen oder an etwaigen Neu-
bildungen, welche sachlich als Fortsetzungen derselben erscheinen, wird nach
8 16, 1c mit Geldstrafe von 15—150 Mk. oder mit Gefängnisstrafe von
8 Tagen bis zu 3 Monaten bestraft.

Berlin, den 29. November 1895.

Der Polizeipräsident,
von Windheim.

Wie die Leitung der Gesamtpartei diesen Streich parierte, ist an an-
derer Stelle geschildert. In den Reihen der Berliner Sozialdemokratie
herrschte ebenfalls keinen Augenblick Zweifel darüber, daß man sich durch
den Polizeistreich nicht im geringsten einschüchtern lassen dürfe. Lier, wo
man der Polizeiherrschaft unter dem Sozialistengesetz Trotz geboten hatte,
war man am allerwenigsten geneigt, sich unter dem gemeinen Recht den
Launen der Polizei zu unterwerfen.

Schon am 10. Dezember 1895 legte die Arbeiterschaft Berlins in zwölf
großen Volksversammlungen Protest gegen die Polizeimaßregel ein. Als
Referenten sprachen in diesen Versammlungen, die sämtlich überfüllt waren,
Ignaz Auer, Richard Fischer, Albin Gerisch, Georg Ledebour,
Wilhelm Liebknecht, Albert Schmidt, Paul Singer, Arthur Stadt-
hagen, Franz Tutzauer, Ewald Vogtherr und Fritz Zubeil. Ihre
Reden, die das Polizeisystem und die Rechtsungleichheit in Preußen
geißelten, fanden durchweg begeisterte Zustimmung, und mit ebensolchen
Begeisterungsausbrüchen ward in allen Versammlungen einstimmig die na )-
stehende Resolution angenommen:	e

Bernstein, Berliner Geschichte. III.