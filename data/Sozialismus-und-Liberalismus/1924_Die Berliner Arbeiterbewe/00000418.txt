﻿Sechzehntes Kapitel.

Die Entwicklung der sozialdemokratischen Presse

Berlins.

emäß dem Beschluß des Lalleschen Parteitages erschien das Berliner
l\l Volksblatt vom 1. Januar 1891 ab mit dem Obertitel „Vorwärts"
^ und der Bezeichnung „Zentralorgan der sozialdemokratischen Partei".
In Übereinstimmung mit dieser Bezeichnung blieb das Blatt der Verwaltung
durch die Leitung der Gesamtpartei unterstellt. Der Parteivorstand hatte
die Mitglieder der Redaktion und Expedition anzustellen und gegebenenfalls
zu entlassen. Nur hinsichtlich der Chefredaktion des „Vorwärts" hatte der
Kongreß eine Ausnahme gemacht. Zum leitenden Redakteur bestimmte er
selbst Wilhelm Liebknecht, und zwar mit dem Zusah, daß dieser mit den
Mitgliedern des Parteivorstandes gleiches Recht haben solle. Das glaubte
man dem ältesten und bedeutendsten der in Deutschland lebenden geistigen
Vorkämpfer der Partei schuldig zu sein, und solange Liebknecht lebte, ist
es denn auch bei dieser Bestimmung verblieben.

Schwieriger war es, das Verhältnis des „Vorwärts" zu den Berliner
Genossen zu regeln. Er sollte mit der Eigenschaft als Zentralorgan der
Partei auch die verbinden, Organ der Berliner Genossen zu sein. Infolge-
dessen konnte es nicht ausbleiben, daß in den Reihen dieser das Verlangen
laut wurde, auf das Blatt, für das man warb und wirkte, auch unmittelbar
Einfluß zu erlangen. Es erhielt 1891 eine erste Genugtuung durch den
von Bebel formulierten Beschluß des Erfurter Parteitages, wonach dieser
sich damit einverstanden erklärte, daß

„die Berliner Genossen eine Kommission von 9 Mitgliedern wählen,
welche in Gemeinschaft mit dem Parteivorstand die Kontrolle des lokalen
Teils des „Vorwärts" zu übernehmen haben".

Für eine Reihe von Jahren genügte diese Vollmacht den Wünschen
Berlins. In dem Maße aber, als die Mitgliedschaften wuchsen und das
Parteileben sich reicher entfaltete, stiegen auch die Ansprüche der Berliner Mit-
gliedschaften an den „Vorwärts", und es machte sich das Bedürfnis geltend,
auf die Redaktion seines politischen Teils gleichfalls Einfluß zu gewinnen.
Die Besetzung der leitenden Stellen in der Redaktion und der Ver-
waltung und die Bestimmung ihrer Gehälter, alles das war nach Partei-

Bernstein, Berliner Geschichte. III.	26