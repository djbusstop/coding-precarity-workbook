﻿360

Berlins ins Leben gerufen, „Der Kandels-Angestellte", das zweimal
monatlich herauskam und offen das sozialdemokratische Banner entrollte;
seine Redaktion wurde von 1892 bis 1894 durch Julius Türk, dann von
Benno Maaß und August Penn, und im Jahre 1898 von dem zu früh
verstorbenen Wilhelm Swienty geführt. Der „Kandels-Angestellte" nun
brachte das ministerielle Rundschreiben zum Abdruck und setzte in auf-
klärenden Artikeln seinen Lesern die Wichtigkeit der Erhebungen aus-
einander; der Vorstand der Vereinigung aber arbeitete auf Wunsch einer
Mitgliederversammlung, in der das Rundschreiben besprochen worden war,
behufs Veranstaltung eigener Erhebungen einen ganzen Arbeitsplan aus,
den er nach Genehmigung durch die Mitgliedschaft dann zur Ausführung
brachte. Es wurden Referate über die einschlägigen Fragen gehalten und
dann an 293 Mitglieder der Vereinigung Fragebogen versandt, von denen
bis zum 8. Oktober 1893 145 ausgefüllt zurückkamen. Diese Antworten,
die von Kaufleuten verschiedener Berufsklassen und Berufsabteilungen
herrührten, wurden nun systematisch zusammengestellt und auf Grund ihrer
wiederum nach eingehenden Besprechungen, die Antworten der Vereinigung
formuliert und dem Minister übersandt. Äber alles das berichtet genau
die von der Vereinigung herausgegebene Broschüre: „Der Kandlungs-
gehilfe und die Kaiserliche Sozialreform, eine Antwort an den
Minister v. Bötticher, betreffend die Arbeitsverhältnisse im Landels-
gewerbe" (Berlin 1893). Sie war von Alb. Auerbach ausgearbeitet, der
schon 1891 eine wirksame Agitationsschrift „Der Kaufmann und die
Sozialdemokratie" verfaßt hatte, aber von nun an in den Kintergrund
tritt. Für die mündliche Auskunft vor der Kommission für Arbeiter-
statistik, in der die Sozialdemokratie durch Kermann Molkenbuhr ver-
treten war, wurden von der Vereinigung als ihre Vertreter Otto Berger
und Jakob Wiebe bestimmt. Sie fanden dort einen Gesinnungsgenossen
in dem Vertreter der Nürnberger Vereinigung der Kaufleute, Emil Dörn-
berger-Fürth, und die drei traten bei den Vernehmungen, die vom
9. bis 20. November 1894 stattfanden, kraftvoll für die Forderungen der
Vereinigung ein.

Es dauerte bis zum Frühjahr 1896, bis die Reichskommission auf Grund
der Vernehmungen ihren letzten Bericht abfaßte, der bestimmte Vorschläge
enthielt, und erst im Jahre 1898 brachte die Reichsregierung einen Gesetz-
entwurf vor den Reichstag, in dem diese Vorschläge ohne wesentliche Ver-
änderung verarbeitet waren. Auch der Reichstag nahm keine erwähnens-
werte Veränderung an den vorgeschlagenen Bestimmungen vor, die Be-
mühungen der sozialdemokratischen Abgeordneten, sie zu verschärfen und zu
erweitern, blieben vergeblich. In ihrer endgültigen Gestalt ist die Vorlage
als „Gesetz vom 30. Juni 1900" proklamiert und der Gewerbeordnung ein-
verleibt worden. Sie bestimmt für Kandlungsgehilfen in Städten von über
20000 Einwohnern und Geschäften mit mindestens zwei Gehilfen und Lehr-
lingen eine Mindestruhezeit von 11 Stunden hintereinander (in kleineren
Städten und Geschäften mindestens 10 Stunden), eineinhalbstündige Mittags-
pause für Gehilfen und Lehrlinge, die ihre Mahlzeit außerhalb des Ge-
bäudes einnehmen, in dem die Verkaufsstelle liegt, Ladenschluß von 9 Ahr
abends bis 5 Ahr früh, und gestattet eine weitere Kerabsetzung der Zeit
für den offenen Verkauf, „sobald zwei Drittel der Geschäftsinhaber dies