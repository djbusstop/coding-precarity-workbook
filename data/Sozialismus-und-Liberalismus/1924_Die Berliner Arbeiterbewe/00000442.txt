﻿424

besonders, wenn sie sich zur Zeit des Aufrufs „zufällig" am entgegengesetzten
Ende des Saales befanden. So ward es denn 91/« Ahr abends, bis endlich
die Lauptwahl wenigstens zu Ende war. Sie ergab das oben schon mit-
geteilte Resultat, das eine Stichwahl notwendig machte. Sie mußte, gemäß
der Vorschrift des Gesetzes, im unmittelbaren Anschluß an die Lauptwahl
vorgenommen werden. Gültig waren für sie nur Stimmen, die für die
Kandidaten abgegeben wurden, welche an erster oder zweiter Stelle standen,
d. h. die konservativen und die liberalen Kandidaten. Aber nichtsdestoweniger
nahmen auch die sozialdemokratischen Wahlmänner an der Stichwahl teil.
Es machte ihnen Vergnügen, ihre Stimmen noch einmal abzugeben. Nur
ließen sie dabei ihrer Phantasie und Laune etwas Spielraum und nannten
alle möglichen Namen, die ihnen gerade durch den Kopf gingen: Eugen
Richter und dessen Sparagnes, den Knabenprügler Dippold, den Lerrn
Landrat von Stubenrauch und die Frau Landrätin, den „ollen ehrlichen
Seemann" und das Warenhaus Wertheim, und was dergleichen Einfälle
mehr waren. Das war alles ungültig, aber es mußte protokolliert werden.
Das Gesetz schreibt keinem Wähler vor, wem er seine Stimme geben darf
oder nicht darf, es regelt nur die Gültigkeit der Stimmen. So wurde es
immer später in der Nacht, und immer müder wurden die Seelen. Gar mancher
Wahlmann gab durch Naturlaute kund, daß er den Schlaf der Gerechten
schlief, manchen anderen verließen die Geister und er zog seiner Wege.
Der Morgen fing an zu grauen, aber der Wahlakt ging noch immer
maschinenmäßig vor sich. Es schlug sieben Ahr, als er endlich abgeschlossen
war. Mit 917 Stimmen hatten die Konservativen gesiegt, 615 Stimmen
waren in der Stichwahl auf die Liberalen gefallen, 544 Stimmen wurden
für ungültig erklärt. Es waren die gültigsten Stimmen: sie hatten in das
ganze verrottete Wahlsystem Bresche gelegt.

In der Begründung der Wahlrechtsänderung von 1906, die für
bestimmte Fälle die Fristwahl einführt und eine Anzahl Wahlkreise, dar-
unter eben diesen Wahlkreis Teltow-Beeskow-Charlottenburg, in kleinere
zerschlägt, erklärte am 23. März 1906 der Minister von Bethmann Loll-
weg ausdrücklich, die Änderung sei unbedingt nötig, denn der bisherige Zu-
stand ermögliche „in großen Wahlbezirken Obstruktionsgelüsten so viel
Angriffe gegen das ordnungsmäßige Zustandekommen des Wahlaktes selbst, daß,
wie allgemein erinnerlich, es nur einer übergroßen und auf die Dauer
nicht erträglichen Anstrengung der Wahlkommissare und Wahlvorsteher ge-
lungen ist, das Verfahren zu dem ordnungsmäßigen Abschluß zu bringen".

Das war nirgends so sehr der Fall gewesen wie hier. Nun war die
Wahlrechtsänderung von 1906 freilich ein klägliches Flickwerk. Sie sollte
das alte Wahlsystem befestigen. Aber zwischen Absicht und Ergebnis
gibt es oft eine große Kluft. Tatsächlich ist aus dem Flick ein großer Riß
geworden und der Ausspruch hat sich als Prophetenwort herausgestellt, den
am 20. November 1903 im Wahllokal für den dritten Berliner Wahlkreis,
als das Resultat der Abgeordnetenwahl verkündet war, entrüstete Sozial-
demokraten den ob ihres zu Anrecht erlangten Sieges glückseligen Freisinnigen
zuriefen: „Das ist das letztem«! gewesen, daß Ihr diesen Wahlkreis
bekommen habt!"

&