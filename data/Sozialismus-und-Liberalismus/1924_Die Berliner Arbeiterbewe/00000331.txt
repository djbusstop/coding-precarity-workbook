﻿- 317 -

Die Anträge an die Schlichtungskommission hat der Obmann der
Kontrollkommission. Sie werden dem Werkstattvertrauensmann aus-
gehändigt, sobald die Kommission die Differenzen untersucht und das
Anrufen der ßchliclitungskommisslen empfohlen bat. -Ist zwischen dem
Werkstattausschuss und dem Meister keine Einigung möglich gewesen,
so werden beide Hälften des Formulars ausgefüllt nach unserem Bureau
und zwar an die Adresse:

A-n die Ortsverwaltung
des Deutschen Holzarbeiter-Verbandes
Ber lin SO. 16
Engel-Ufer-. 14. parterre

eingesandt.

Hinter den Worten „die Ursachen unserer Differenzen sind" soll
angegeben werden; Herabsetzung der Akkord preiseoder Lohnabzüge,
Verlängerung der Arbeitszeit, Massregelung, Festsetzung des Akkord-
preises bei neuen Arbeiten, beantragte Tariferhöhung. Längere Er-
klärungen brauchen nicht auf das Formular geschrieben weiden. Die
Rückseite bleibt unbeschrieben. Fordern die Kollegen einen erhöhten
Tarif, so ist dieser in awei Exemplaren mit einzusenden. Mündliche
Mitteilungen an irgend, einen Kollegen gelten nicht als Anrufüng der
Schlichtungskommission.

*2)ie Ozfovez-'wa'stu-nty..

Demokratie im Wege
stehen. Soweit von einer
Niederlage zu sprechen
war, hatte sie, wie das
der Sozialpolitiker Philipp
Stein damals in der
„Äilfe" ausführte, in viel
höherem Grade als die
Arbeiter die öffentliche
Meinung erlitten. Sie
hatte sich mit Geräusch auf
die Seite der Arbeiter
gestellt und ihnen doch
keinerlei wahrhaften Bei-
stand geleistet. „Die viel
gefürchtete und oft ge-
rühmte öffentliche Mei-
nung hat sich als ein ganz
unzuverlässiger Kampfge-
nosse erwiesen", konnte
Stein mit Recht schreiben.

And weiter konstatiert er,
daß die Arbeiter diese
öffentliche Meinung der
Bourgeoisie als echte
Sozialdemokraten für das

genommen hatten, was sie wert war: „Sie haben sie nach Verdienst behandelt,
sie haben sie — benutzt." Aber der Nutzen, den sie aus ihr ziehen konnten,
war von keiner entscheidenden Wirkung.

Es wäre unbillig, in gleicher Weise die Tätigkeit des Einigungsamts
einzuschätzen. Es hatte sich redlich Mühe gegeben, den Arbeitern beizustehen.
In 65 Terminen hatte es 22 Konfektionäre, 183 Zwischenmeister, 156 männ-
liche Arbeiter und 325 Arbeiterinnen, zusammen 686 Personen kontra-
diktorisch vernommen, alle Angaben durch Sachverständige prüfen und durch
Vorlegung der Lohnbücher bewahrheiten lassen, jeder beteiligten Gruppe
das Fragerecht zugestanden, und so, trotz der teilweise sehr aktiven Boy-
kottierung durch einen großen Teil Konfektionäre und Zwischenhändler, ein
Material über die Verhältnisse der Konfektionsindustrie ans Licht gezogen,
wie noch keine andere Erhebung in Deutschland. Es hatte sich auch
nicht gescheut, sehr unangenehme Wahrheiten auszusprechen. Sein Schieds-
spruch und der in der außerordentlichen Beilage zur „Sozialen Praxis"
abgedruckte Bericht über seine Erhebungen und Verhandlungen waren wert-
volle Dokumente für die Propaganda zugunsten einschneidender Gesetze
über die Leimarbeit. Im Schiedsspruch heißt es unter anderem, das
Gericht habe auf Grund der stattgehabten Ermittelungen die Überzeugung
gewonnen, daß

„die gezahlten Löhne in vielen Fällen unter ein Niveau gesunken sind,
das ein menschenwürdiges Dasein der Arbeitnehmer trotz ange-
strengtester fleißiger Arbeit nicht ermöglicht".

149 und 150. Antragsformular an die
Schlichtungskommission zur Schlichtung
von Lohnstreitigkeiten