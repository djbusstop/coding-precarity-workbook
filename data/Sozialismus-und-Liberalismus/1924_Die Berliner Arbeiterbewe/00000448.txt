﻿430

Das soll nicht verschwiegen werden. Trotzdem wäre es jedoch sehr
voreilig, aus dieser Differenz auf eine abnehmende Beliebtheit der Maifeier
als Fest der Arbeiterklasse zu schließen. Ein so großer Zustrom von Mit-
gliedern, wie ihn die Gewerkschaften seit der Jahrhundertwende zu ver-
zeichnen hatten, braucht eben Zeit, um die Ideenwelt in sich aufzunehmen,
die den Angeworbenen zum opferbereiten Mitkämpfer im vollen Sinne des
Wortes macht, und grade in einer Reihe der größten Unternehmungen Berlins
war die Arbeitsruhe aus den schon entwickelten Gründen nun mit größeren
Opfern und Gefahren verbunden, als früher. Die absolute Zahl der Teilnehmer
an der Maifeier ist aber fast ohne Unterbrechung gestiegen. Jahre sehr
starken Geschäftsdruckcs haben ihre Zunahme gelegentlich verlangsamt, auch
stellt die Frage der Lokalitäten der vollen Durchführung der Maifeier
große Schwierigkeiten in den Weg. Masscnumzüge werden von den Be-
hörden am l. Mai nicht geduldet, größere Ausflüge ins Freie im voraus
zu veranstalten verbietet in dieser Jahreszeit das Klima. Im ganzen fleht
sich die Arbeiterschaft Berlins daher für ihre Maiveranstaltungcn auf
Saallokalitäten angewiesen, und diese stehen ihr in so großer Zahl, als nötig
wäre, den ganzen Kcerbann der Arbeiterbewegung zu fassen, nicht zur Ver-
fügung. Das zeigt sich ja auch bei Veranstaltungen, wie Wahlrechts-
demonstrationen, Proteste gegen Polizeiakte oder Steuerungerechtigkeiten, die
durch die Unmittelbarkeit ihres Zwecks die Geister besonders lebhaft anregen.
Stets fehlt es an der nötigen Anzahl großer Säle, und die Statistik der
in den gemieteten Sälen sich drängenden Massen liefert immer mehr nur
einen Anzeigcmaßstab für die wirkliche Werbekraft des Demolistrations-
gegenstandes.

hiernach ist auch die Teilnahme an der Maifeier zu bewerten. Es untersteht
nicht dem leisesten Zweifel, daß sie in den Kerzen der sozialistischen Arbeiter-
schaft tiefe Wurzel geschlagen hat. Sehen wir von den ersten Jahren ab,
und nehmen wir das Jahr, wo der Weltfeiertag der Arbeit zuerst in der
noch bestehenden Anordnung zwischen Partei lind Gewerkschaften gefeiert
wurde, als Ausgangspunkt, so hat sich die Zahl der in Berlin am 1. Mai
Arbeitsruhc Äbenden immerhin seit jenem Jahr vervierfacht. 1894 wurden
in Berlin von den Gewerkschaften 12 000 Feiernde festgestellt, 1905 dagegen
waren es zwischen 50- und 60 000. Die Zahl derer aber, die an den Nach-
mittags- und Abendfesten der Wahlvereine teilnahmen, beläuft sich auf
mehrere Kunderttausend. Lind welche Begeisterung bricht auf diesen Festen
neben allen Lustbarkeiten und Llntcrhaltungen immer wieder durch! Mit
welchem Jubel werden auf ihnen die Melodien der Arbeiterlieder begrüßt,
mit welcher Inbrunst die Massengcsänge mitgesungen, mit welch stürmischen
Zustimmungen die Festreden entgegengenommen, die den Sinn der Feier
darlegen! Es ist ein großer Irrtum, sie wegen der Verbindung mit den
Lustbarkeiten als „Kaffeekränzchen" gering einzuschätzen. Das Maifest soll
ein Fest der Freude sein, und um so mehr wird der Weltfeiertag der
Arbeiterklasse seinen Zweck erfüllen, je mehr er für die Heranwachsende
Arbeitergeneration den Wert und die Bedeutung erlangt, den die alten
Kirchenfeste für ihre Eltern und Voreltern gehabt hatten.