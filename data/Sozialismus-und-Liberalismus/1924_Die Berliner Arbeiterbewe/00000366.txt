﻿352

anstaltete Untersuchung ergab, daß die Schuld bei den Beschwerdeführern
oder deren Beauftragten lag, verstummte diese Anklage. Aber es gab auch
Beschwerden, die nicht völlig unbegründet waren. Die Friedensabmachungen
waren in bezug auf die Unterbringung der Ausgesperrten günstiger aus-
gelegt worden, als sich die Sache in der Praxis gestaltete. Man war eben
beim Friedensschluß schon mitten in der Mälzungsperiode, und da die
Arbeitswilligen von ihm nicht ausgeschlossen waren, konnte auch nur ein
geringer Teil der Ausgesperrten alsbald wieder in den Ringbrauereien eingestellt
werden. Roesicke und einige Gleichgesinnte im Brauereiring hatten es bei
ihren Kollegen nicht viel besser, wie Auer, Singer und Genossen bei
einem Teil der Arbeiter. Kurz, die Leiter des Brauereirings erklärten, daß
sie zwar den Ausgesperrten, die sich als einzelne an sie wenden würden,
nach Möglichkeit zur Beschäftigung verhelfen wollten, den Organisationen
gegenüber aber auf dem Buchstaben des Vertrags bestehen müßten. Das
führte zu neuen Diskussionen in der Presse, und immer schärfer hatte der
„Vorwärts" die Versuche der außerhalb des Ringes stehenden Brauereien
und ihrer Verbündeten zurückzuweisen, aus diesen Diskussionen Kapital zu
schlagen und die Gegensätze von neuem zu schüren. Ein Artikel „Nach-
klänge vom Boykott" in seiner Nummer vom 3. Januar 1895 ist für die
Beurteilung der Interessenkämpfe, die mit dem Boykott verbunden waren,
außerordentlich interessant.

Von anderen Artikeln über die Bilanz des Boykotts sind ein
Artikel Lermann Mattutats im damaligen Wochenblatt der Partei, des
„Sozialdemokrat", sowie ein Aufsatz Ignaz Auers „Rückblicke auf
den Bierboykott" im „Sozialpolitischen Zentralblatt" vom 31. Dezember
1894 besonders bemerkenswert. Mattutats Artikel kann als die Formulierung
der Auffassung der großen Mehrheit der Berliner Gewerkschaftskommission
betrachtet werden. Mattutat wie Auer bezeichneten unumwunden den Rixdorfer
Beschluß vom 6. Mar 1894, der den Boykott herbeigeführt hatte, als eine
große Torheit und erklärten es für einen zu bekämpfenden Mißstand, so
folgenschwere Entscheidungen den Zufälligkeiten von Volksversammlungen
zu überlassen, deren Teilnehmer sich in ihrer Mehrheit der Verantwortung,
die sie mit ihren Abstimmungen auf sich nehmen, im Moment meist gar
nicht bewußt seien, sondern bloß ihrer Stimmung folgten, wobei Mattutat
es scharf rügte, daß so viele Arbeiter sich noch durch tönende Worte be-
einflussen ließen. Beide Verfasser betonen ferner scharf die Gefahren eines
zu weit getriebenen Optimismus, der sich in Überschätzung der eigenen und
Llnterschätzung der gegnerischen Kräfte ergeht. Auer bezeichnet es des
weiteren als eine aus dem Kampfe zu ziehende Lehre, daß es „noch nicht
genug ist, einen Kampf mit aller Energie und Bravour zu führen, sondern
-aß auch in diesem Falle sich der Meister in der Bescheidung zeigt". „Der
Zweck des Kampfes", schreibt er, „ist der Friede. Gewiß, nicht der schimpfliche
Friede, oder der Friede um jeden Preis, immerhin aber der ehrliche und an-
ständige Friede." Dieser Friede sei aber hier schon im September zu erreichen
gewesen, als die Brau- und Mälzungsperiode noch bevorstand, die Situation
für die Ausgesperrten also viel besser war, wie drei Monate später. Die
Zusage der Brauherren, die Ausgesperrten in ihre Betriebe nach Bedarf
an erster Stelle wieder aufzunehmen, hätte im September einen praktischen
'Wert gehabt, habe jetzt aber „fast nur mehr eine theoretische Bedeutung".