﻿28

71,8 Millionen Mark, für 1894/95 waren 78,5 Millionen angesetzt.
Caprivi wie auch der damalige Staatssekretär des Marineamts Lollmann
hatten wiederholt der Ansicht Ausdruck gegeben, daß Deutschlands Küsten
hinlänglich geschützt seien und ein Bedürfnis, im Flottenbau mit irgend
einer auswärtigen Großmacht in Konkurrenz zu treten, nicht bestehe. Das
sollte nun indes anders werden. Jetzt agitierten die Kolonialchauvinisten
und Englandfresser — was in Deutschland so ziemlich auf das gleiche
hinausläuft —, mit aller Macht für den Bau von mehr Kriegsschiffen,
und von oben ward dieser Agitation jeder mögliche Vorschub geleistet.
Wilhelm II. hielt den Führern der regierungsfähigen Parteien, zu denen
jetzt das Zentrum gehörte, Vorträge über die Notwendigkeit, die Flotte
auf einen bedeutend höheren Stand zu bringen, ließ in der Wandelhalle
des Reichstags bildliche Darstellungen der Flottenverhältnisse der Äaupt-
länder ausstellen und nahm verschiedene Gelegenheiten wahr, in Ansprachen
bei Festakten und dergleichen seinem Verlangen nach mehr Kriegsschiffen
Ausdruck zu geben. Die Agitation steigerte sich, als 1895 das Glückwunsch-
telegramm des Kaisers an den Präsidenten Krüger wegen der Nieder-
schlagung des Iamesonschen Einbruchs in den Transvaal die Beziehungen
zwischen England und Deutschland erheblich verschlechtert hatte. Der
Reichstag, dessen Mehrheit angesichts der Finanzlage des Reichs keine
große Neigung hatte, auf einen uferlosen Flottenplan sich einzulassen,
wurde eine zeitlang vom Staatssekretär des Äußeren, Freiherrn von Marschall,
und dem Marinesekretär Lollmann abwechselnd mit Beteuerungen über-
schüttet, daß die Regierung durchaus nicht die Flotte übermäßig vermehren
wolle. So ließ man sich zu allerhand kostspieligen Bewilligungen herum-
bekommen, bis im Frühjahr 1897 eine Denkschrift Lollmanns, laut der außer
dem Geld für schon bewilligte Schiffe in den nächsten vier Zähren für Schiffs-
bauten eine viertel Milliarde an Kosten nötig werden würde, dem Zentrums-
führer Dr. Lieber den Schmerzensruf entlockte, er fühle sich als „blamierter
Europäer". Äollmann aber, dem der Reichstag von seinem Schiffsprogramm
für 1897/98 einige Schiffe strich, ging, um dem Kontreadmiral Tirpitz
Platz zu machen, von dem schon im Sommer 1896 das Gerücht umlief,
daß er dem Kaiser einen noch kostspieligeren Flottenplan unterbreitet habe.
Auch Freiherr von Marschall, der letzte hervorragende Mitarbeiter Caprivis,
schied aus seinem Amt und wurde durch den Freiherrn Bernhard von
Bülow, bis dahin Botschafter in Rom, ersetzt, der einen den Flotten-
schwärmern angenehmeren Ton auf der Ministerbank anzuschlagen wußte.
Die Reichstagsmehrheit ihrerseits, der der Kaiser im Frühjahr 1895,
als sie sich weigerte, ein Glückwunschtelegramm an Bismarck zu dessen
80. Geburtstag abzusenden, in einem Telegramm an eben diesen Bismarck
seine „tiefste Entrüstung" kundgegeben hatte, bekam nun in einem Tele-
gramm des Kaisers an den Prinzen Heinrich, das, wie jenes erste, als-
bald veröffentlicht wurde, ein Wort zu hören, von „vaterlandslosen Ge-
sellen, die die Beschaffung der für das Deutsche Reich notwendigen Kriegs-
schiffe zu hintertteiben wissen". Etliche Wochen später gebrauchte der
Kaiser auf einem Festmahl bei einer Denkmalsenthüllung in Köln das Wort
„Der Dreizack (des Meergottes) gehört in unsere Faust". Noch im
gleichen Zahr ward ein Kriegsschiff nach Haiti entsandt, um Sühne für
das verletzte Recht eines Deutschen einzuholen. Dann aber gab die Er-