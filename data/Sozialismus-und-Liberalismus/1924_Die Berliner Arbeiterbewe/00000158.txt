﻿148

Zenkralverbände verlangte, welche in bezug auf die Maifeier sich den
Beschlüssen der internationalen Kongresse zu entziehen suchten oder ihnen
direkt entgegenhandelten. In der Tat zeigte sich, während die internationalen
sozialistischen Kongresse die Feier des 1. Mai durch Arbeitsruhe immer
stärker betonten, in den Reihen der führenden Gewerkschaftsvertreter eine
gewisse LInlust, zur Arbeitsruhc aufzufordern oder zu ermutigen. Man fand,
daß diese den Organisationen Opfer auferlege, mit denen ihr propagan-
distischer Effekt in keinem Verhältnis stehe. An die Arbeitsruhe knüpften
sich stets Kämpfe mit den Unternehmern, die nun, wo man von Organisation
zu Organisation über Tarife verhandelte, vom reinen Gewerkschaftsstand-
Punkt aus als zwecklos oder störend erschienen.

Aus dem am 8. September 190 l tagenden Parteitag für die Provinz
Brandenburg konnte festgestellt werden, daß die 1899 für die Agitation
unter der Landbevölkerung von der Agitationskommission ins Leben gerufene
Monatszeitung „Die Fackel," nachdem sie in den ersten 13 Monaten
ihres Bestehens in 141 000 Exemplaren verbreitet worden war, in den
darauf folgenden 11 Monaten einen Absah von 148 000 Exemplaren
gefunden hatte. Im übrigen besprach diese, von 6p Delegierten aus
26 Wahlkreisen besuchte Konferenz die Vorarbeiten für die Beteiligung
an den kommenden preußischen Landtagswahlen. Trotzdem Berlin bis
zuletzt gegen die Beteiligung sich erklärt hatte, nahm man, nachdem die
Partei endgültig es den Parteigenossen zur Pflicht gemacht hatte, in den
Wahlkampf einzutreten, loyal die Durchführung dieses Beschlusses in die
Land und rüstete rechtzeitig, um es nun auch im Kampf an nichts fehlen
zu lassen.

Die Beschlüsse des Lübecker Parteitags wurden bei der Bericht-
erstattung über diesen in allen Berliner Wahlkreisen mit Befriedigung ent-
gegengenommen. Zu Ehren der österreichischen Parteimitglieder Viktor
Adler und Engelbert Pernerstorfer, die dem Lübecker Parteitag bei-
gewohnt hatten und auf der Rückreise einige Tage in Berlin verweilten,
fanden zwei große Volksversammlungen statt, die eine am 30. Sep-
tember im Saal zum Eiskeller, die andere am 1. Oktober im Saal der
Bockbrauerei. Linker gespannter Aufmerksamkeit schilderten die beiden Ver-
treter der österreichischen Arbeiterschaft, denen selbstverständlich ein begeisterter
Empfang bereitet wurde, den Berliner Arbeitern die Kämpfe und Kampf-
bedingungen der Sozialdemokratie in der Habsburgischen Monarchie. Das
Jahr 1902 fand die Sozialdemokratie Berlins schon stark von dem Ausblick
auf die im Jahre 1903 fällig werdenden Neuwahlen für Reichstag und
Landtag beherrscht, und außerdem fesselte der im Reichstag geführte Kampf
wider die Zolltarifvorlage der Regierung mit den erhöhten Zöllen auf
Lebensmittel wiederholt ihre Aufmerksamkeit. Die Versammlungen, zu
denen er Anlaß gab, trugen indes ausschließlich Demonstrationscharakter
und gehören daher in ein anderes Kapitel. In den Wahlkreiskonferenzen
sind es hauptsächlich Detailfragen der Agitation und Organisation, welche die
Verhandlungen ausfüllen. Streitfragen von grundsätzlicher Bedeutung treten
in diesem Kampfjahr nicht in den Vordergrund, doch verdient es erwähnt zu
werden, daß in den Wahlkreisversammlungen, die sich — am 21. August —
mit der Provinzialkonferenz und dem Parteitag für 1902 befassen, Anträge
gestellt werden, die verlangen, daß die Frage des Kampfes wider den