﻿An

alle Arbeiter-Organisationen Deutschlands.

Werthe Geussserr!

Im Nachstehenden geben wir Euch einen kurzen Bericht über den Stand unserer Sache.

Zur Zeit der Einführung der Kontroll-Marke (im Frühjahr 1690) bildete sich bekanntlich ein Fabrikantcn-
Rkng. dessen Mitgliedern die Verpflichtung auferlegt war, daß, wer die Kontrollmarke annimmt, an den — Ring —
10000 Mark Strafe bezahlen müsse.

Dieser Ring ist durch Eure Unterstützung gesprengt, die Strafe hat allerding-, soweit uns bekannt ist, nicht
ein «einziger bezahlt. Zur Zeit haben 10 Fabrikanten die Forderungen bewilligt, und erhalten infolgedessen die
Marken; sechs anderen mußten dieselben wieder entzogen werden, weil sie ihr theilweise schriftlich gegebenes Ehrenwort
gebrochen hatten. Wenn Ihr uns nun Eure Unterstützung in der gleichen Weise zu Theil werden läßt, wie bisher, so
wird der Erfolg sehr nbald ein och größerer sein.

Einmal in einer Branche durchgeführt, wird das System (nur Waare zu konsumiren. welche in geeigneter
Weise gekennzeichnet ist), sich leicht auch in anderen Branchen einführen lassen, namentlich dort, wo die Fabrikanten
darauf ausgehen, die Koalitionsfreiheit der Arbeiter zu vernichten. Mittelst Streik ist gegen die (allen möglichen behörd-
lichen Schutz genießenden Fabrikanten-Vereinigungen) in nur wenig Fällen Erfolg zu erringen, da die technischen Er-
rungenschaften ihnen die Verwendung jeder exbeliebigen Arbeitskraft gestatten. Greift man sie aber auf ihren Absatz-
gebieten an, so hat man die verwundbarste Stelle getroffen, und das geschieht, indem der aufgeklärte Arbeiter nur
Waare konsumirt. welcht in geeigneter Weise gekennzeichnet ist. wie es durch die Arbeiter-Kontrollmarke geschieht. Das
wissem die Herren auch, Mnb ihre Presse hat schon sehr oft darüber gelcitartikelt. So schreibt die „Volkswirthschaft-
liche Korrespondenz" :

„Wir geben uns der Hoffnung hin, daß, wollen die Arbeitgeber Herr im eigenen Hause bleiben, sie ein-
müthig Front machen müssen gegen dieses Kontrollmarkensystem!" Und der „Arbeitgeber" schreibt am Schlüsse eines
(das halbe Blatt füllenden Leitartikels): „Arbeitgeber, kaust nicht dort, wo es Waare mit Kontrollmarken giebt!"
Jeder aufgeklärte Arbeiter wird nun wohl wissen, was er thun muß.

Um gegen die Kontrollmarke anzukämpfen, preisen viele DetailgeschäftSiuhgber ihre Waare zu den billigsten
Preisen an. In unsrer Branche sogenannte 2,50 und 3 Mark-Bazars. Ein jeder Käufer mag bedenken, daß an dieser
Schundwaare der Schweiß und die Lebenskraft, unglücklicher, maßlos ausgebeuteter Menschen hängt; daß die
eigentlichen. Erzeuger dieser Waare bei harter Arbeit sich nicht einmal satt essen können Ist es nicht eine Schmach,
wenn Arbeiterinnen bei elf- und zwölfstündiger Arbeitszeit 4,50 bis 7 M, Wochenlohn und männliche Arbeiter
6 bis 12 M. Wochenlohn erhalten? Mit dem Erzeugnisse der halbverhungerten Weber im Eulengebirge deckt vielleicht
dieser oder jener großstädtische Arbeiter (angelockt durch den billigen Preis) seine Blöße. Die Fetzen müßten ihm auf L«ib
und Seele brennen, wenn er an das furchtbare Elend denkt, in welchem sich die Verfertiger dieser Waare ausnahmslos
befinden. Er tragt mittelbar mit Schuld daran, weil er solchen Schund konsumirt.

Wie schon oben gesagt, kennen die Unternehmer den hohen Werth der Arbeiter-Kontrollmarke als Kampfmittel,
ihre größte Hoffnung und Stütze ist die Kurzsichtigkeit und Dummheit der Masse. Darum bitten wir jeden Genossen, für das
System der Arbeiter-Kontrollniarke zu agitiren, wo er nur kann; denn geholfen kann der arbeitenden Klasse nur dann,
werden, wenn sie sich selbst hilft, weil freiwillig für die Arbeiter von den Unternehinrrn nichts gegeben wird.

Indem wir noch besonders darauf aufmerksam machen, daß die Marke beim Kausen deS Hutes schon fest ein-
geklebt sein muß, — jedes Einkleben beim Kauf ist Betrug, — bitten wir die Genossen nochmals:

Kauft keinen Kut ohne ArSeiter-Ksutrollmarke!

Berlin im März 1891.

Die Kommission.

3- 21.: T. föfTapt) Berlin NO., Georgenkirchplatz 8

Vcrantworlllch für Redaktion und Verlag C. Keuipe, Lerltn. Druck von Maurer, Ärrner L Co., Berlin, Elisabeth-Üier vb.

154. Flugblatt über die Bedeutung der Kontrollmarke

Bernstein, Berliner Geschichte.^, III.

21