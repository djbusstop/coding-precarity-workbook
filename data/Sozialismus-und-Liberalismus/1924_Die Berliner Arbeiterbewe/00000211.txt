﻿198

Der Wahlkreis, in dem die Sozialdemokratie schon vor dem Fall des
Sozialistengesetzes in die Stichwahl gekommen war, ward 1893 zum ersten-
I mal von ihr erobert. Im Jahre 1898 brachte in der Stichwahl ein win-
!	ziges Mehr an Stimmen den Freisinnigen den Sieg, doch mußte die Wahl

'	für ungültig erklärt werden, lind in der Nachwahl erhielt die Sozialdemo-

i kracke gleich im ersten Wahlgang die absolute Mehrheit. Desgleichen bei
den Wahlen von 1903 und 1907. Das letzte Jahr zeigt auch die höchste
* sozialdemokratische Stimmenzahl, während im Jahr 1903 der höchste Prozent-
satz sozialdemokratischer Stimmen erzielt wurde.

Kandidat der Sozialdemokratie war 1890 Konstantin Ianiszewski,
:	damals Buchbinder, und von 1893 ab Richard Fischer, Geschäftsführer.

Berlin IH.

Jahr	Sozial-  demokraten	Frei-  sinnige	Konser-  vative	Andere Parteien		Zer-  splittert
				Rationalliberale		
1890	12 287	11566	174	229	—	172
Stichwahl	12 945	13 637	—	—	—	—
			Konservat.-  Anttsemiten		Zentrum	
1893	12 732	7 919	4 534	979	318	94
Stichwahl	14 068	9 700	—	—	—	—
1898	11411	8 031	3 809	—	377	86
Stichwahl	12 766	11415	—	—	—	—
1903	15124	5 804	3 673	—	462	—
1907	14259	9 404	1656	—	443	147

Der Wahlkreis wurde 1893 zum erstenmal von der Sozialdemokratie
erobert und ist seitdem in ihrem Besitzstand verblieben. Das Jahr 1903
° zeigt die höchste absolute und relative Stimmenzahl für die Sozialdemo-
kratie. Auch hier verdrängen Umbauten viele Arbeiter aus ihren Wohnun-
gen, während an eine Zunahme der Wohnhäuser nicht zu denken ist, da
von allen Seiten dicht bebaute Quartiere den Wahlkreis umgeben.

Sozialdemokratische Kandidaten waren 1890 Karl Wildberger,
! Tapezierer, 1893 Ew. Vogtherr, Kaufmann, und von 1898 ab Wolf-
gang Leine, Rechtsanwalt.

			Berlin IV.			
Jahr	Sozial-  demokraten	Frei-  sinnige	Konser-  vative	Andere Parteien		Zer-  splittert
				Zentrum		
1890	40 709	14 267	189	696			130
			Konservat.-  Antisemiten			
						
1893	46 356	9 768	7 469	869			499
1898  1903	45 293  68 758	7811	7 273	1256	Polen	334
		9 006	8 651	1988	832	59
1907	82 039	15 708	6 601	2 708	1 313	87

Eiserner Besitzstand der Sozialdemokratie — das ist die Devise dieses
Wahlkreises. Nur 1898, ein Jahr, das für alle Parteien eine schlechte
Wahlbeteiligung aufwies, brachte der Sozialdemokratie einen Stimmen-
rückgang, der indes zu unbedeutend war, um irgendwie ins Gewicht zu
fallen. Sonst ist hier nur beständiger Zuwachs zu verzeichnen, der mit
seinem sprunghaften Charakter die phänomenale Bevölkerungszunahme des
Kreises illustriert, und ohne Unterbrechung ist der relative Stimmenanteil