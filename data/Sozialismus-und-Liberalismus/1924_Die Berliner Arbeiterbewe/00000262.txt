﻿248

schauungen geraten können. Der Arbeiter, welcher durch das Vertrauen seiner
Kameraden zum Gewerberichter berufen werden kann und, sobald er gewählt
ist, seine volle Pflicht tut, ist an und für sich zum Schiedsrichter geeignet."

Dieser Ausspruch eines Mannes, der mit den sozialdemokratischen
Bestrebungen durchaus nicht sympathisiert, wäre noch durch viele Zitate
aus der von Jastrow und Flesch herausgegebenen Zeitschrift „Das
Gewerbegericht" zu ergänzen, die gleiches aussagen. Aber da es sich hier
speziell um Berlin handelt, kann es bei ihm sein Bewenden haben. Die
Arbeiter Berlins, so entschieden sie der Hnternehmerklasse in der sozialen
Tendenz gegenüberstehen, lassen als Gewerberichter es den einzelnen Unter-
nehmer nicht entgelten, daß sie seine Klassengegner sind. Nicht dadurch,
daß es parteiisch zugunsten der Arbeiter entscheidet, erweist sich das Ge-
werbegericht als sozialpolitisches Institut dem Arbeiter günstig, sondern
dadurch, daß es dem sonst unbeschränkten Absolutismus des Unternehmers
als Nechtsinstanz gegenübersteht, die dem Arbeiter wenigstens die Beob-
achtung dessen sichert, was das Gesetz und die mit der Entwicklung der
sozialen Zustände sich selbst entwickelnde allgemeine Rechtsanschauung in
bezug auf den Arbeitsvertrag und das Arbeitsverhältnis für recht und
billig erklären. In diesem Sinne kann man I. Jastrow zustimmen, wenn
er in seinem Buch „Sozialpolitik und Verwaltungswissenschaft" schreibt:
„Das Gewerbegerichtsgesetz ist die iVlagna Charta des deutschen Arbeiters.
Der Ruf: Ich gehe zum Gewerbegericht! hat hier eine ähnliche Bedeutung,
wie die Erzählung von dem Müller von Sanssouci und seinem Ausspruch:
Ja, wenn es kein Kammergericht gäbe!" (a. a. O. S. 405). And M. von
Schulz schreibt im „Archiv für Sozialwissenschast": „Lierzu kommt, daß der
Arbeiter im allgemeinen zum Gewerbegericht ein unbedingtes Vertrauen
gewonnen hat, ein Vertrauen, welches er dem privaten Gericht kaum in
dem Maße entgegenbringen wird." (a. a. O., Jahrgang 1907, S. 614.)

Wie sehr der Schluß dieses Satzes zutrifft, zeigen die in neuerer Zeit
in derArbeiterschaft erhobenen Klagen überden formalistisch-juristischen Geistder
Gewerbegerichtsbeschlüsse, der mit dem Wechsel in der Person des leitenden
Gewerberichters zu überwiegen droht.

Die Arbeiten des Gewerbegerichts Berlin werden durch folgende
Zusammenstellung veranschaulicht, die das Jahrzehnt vom 1. April 1896
bis zum 31. März 1906 umfaßt:

Summe der eingelaufenen Klagen................................ 125 295

Davon wurden erledigt:	Absolute  Zahl	Vom
a) Vor dem Termin		5658	4,74
b) Durch Zurücknahme		26 808	22,40
c) Durch Versäumnisurteil		12 558	10,49
d) Durch Verzicht des Klägers ....	161	0,14
e) Durch Anerkenntnis des Verklagten.	426	0,36
k) Durch Vergleich		60 378	50,47
g) Durch andere Endurteile, zumeist im kontra- diktorischen Verfahren		13 646	11,40
Summa	119 635	100

Es blieben unerledigt und kamen im folgenden

Jahr zur Verhandlung................. 5 560

Zusammen 125 295