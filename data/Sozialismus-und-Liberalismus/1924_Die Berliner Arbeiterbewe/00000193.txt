﻿181

der Anleitung dieser Persönlichkeit wurden die Angeklagten Grüttefien,
Paul und Schütte zu beträchtlichen Geldstrafen, Larnisch, Keßler, Schmidt,
Wißberger und Zachau zu Gefängnis von zwei bis fünf Monaten ver-
urteilt, obwohl ihre Behauptungen durch Aussagen von Polizisten selbst
als zutreffend erwiesen wurden. Übereifer und Ungeschick der Polizei
waren schuld gewesen, daß viele gutwillige Passanten in der brutalsten
Weise gemißhandelt worden waren.

Mehr noch. Der von einigen der erwähnten Blätter geäußerte Ver-
dacht, daß Lockspitzelei bei der Sache im Spiel gewesen sei, hatte schon vorher
Bestätigung erhalten durch das am 23. Januar 1894 in einer Anarchisten-
versammlung abgelegte Bekenntnis des Metalldrehers Emil Brandt,
daß er sich vom Polizeikommissar Röber zu Spitzeldiensten scheinbar habe
anwerben lassen, und daß das Geld, das er von diesem erhalten habe,
unter anderem dazu benutzt worden, den — in sehr scharfen Ausdrücken
gehaltenen — Aufruf zu der Arbeitsloscnversammlung zu drucken. Als
Zeuge vernommen, bekannte Brandt weiterhin, daß er, von Röber nach
Mitteilungen gedrängt, diesem allerhand abenteuerliche Dinge über den
Zweck der Demonstration erzählt und Andeutungen von einem Zug durch die
Straßen und dergleichen fallen gelassen habe. Ging nun daraus auch nicht
hervor, daß Brandt im Aufträge des Röber den „Lockspitzel" gemacht
hatte, so bestätigte es doch jedenfalls, daß Brandt mit Polizeigeld Auf-
rufe in die Welt gesetzt hatte, die auf jüngere Leute wie Lockspitzelarbeit
wirken mußten, während die Polizei auf Grund der Angaben dieses unsicheren
Kantonisten — und wahrscheinlich auch von Berichten berufsmäßiger Spitzel
— Maßnahmen traf, die einen Plan voraussetzten, von dem die große
Masse der Demonstrierenden nichts wußte. Außerdem gaben die Polizei-
zeugen zu, daß Agenten der Polizei in Zivil sich unter die Menge gemischt
hatten, um diese zu verwirren. Damit waren die tatsächlichen Unterlagen
für die Behauptungen der Presse nachgewiesen; ihr Arteil strafen, hieß
unter diesen Amständen Vergewaltigung der Freiheit der Meinungs-
äußerung.

Vier Tage nach der Demonstration vom 18. Januar, am 22. Ja-
nuar 1894, kam im Deutschen Reichstag eine Interpellation der sozialdemo-
kratischen Fraktion in Sachen des Notstandes zur Verhandlung, bei welcher
Gelegenheit vom Regierungsvertreter, Staatssekretär v. Bötticher, anerkannt
wurde, daß ein Notstand vorhanden sei, der die Betroffenen „recht hart"
bedrücke, aber jedes Eingreifen des Reichs abgelehnt wurde, da man über
die Größe des Notstandes und seine Stärke in den einzelnen Industrien keine
genügende Informationen habe. Am Abend des 22. Januar nahmen die
Arbeiter Berlins in fünf großen Volksversammlungen Bericht über die
Reichstagsdebatte entgegen, die sich übrigens noch am 23. Januar fort-
spann, und legten gegen die Antätigkeit der Regierung und der bürgerlichen
Parteien scharfen Protest ein. In einer dieser Versammlungen, im großen
Saal des Eiskeller, ließ der jugendliche Dr. Ladislaus Gumplowih, damals
Anarchist, heute aber ein geschätztes Mitglied der polnischen sozialistischen
Partei, in seiner Entrüstung über das lässige Verhalten der Vertreter von
Staat und Gesellschaft gegenüber der Notlage großer Volksschichten scharfe
Worte über den Staat als Polizist der Ausbeuter fallen, wofür er aus der
Versammlung heraus verhaftet lind dann zu der ungeheuerlichen Strafe von