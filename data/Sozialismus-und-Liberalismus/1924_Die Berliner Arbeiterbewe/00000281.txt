﻿— 266 —

II. Gruppe: Lande!, Verkehr und Bedienung.

Mitglieder in Berlin

1894	1905

1.	Barbiere und Friseure................................. 40	552

2.	Droschkenführer (noch 1895 Verein).................. 1034	2619

3.	Gastwirtsgehilfen (1894 Verein)...................... 700	936

4.	Landels- und Transportarbeiter................. 1632	17	181

5.	Bnreauangestellte...............................  .	143_______287_

3 549	21 575

Nach 1894 beigetreten oder verselbständigt:

6.	Landlungsgehilfen (1896 als freie Vereinigung der

Kaufleute).......................................   367	1524

7.	Kassenbeamte (1894).................................. 137	520

8.	Lagerhalter (1901).................................... 59	56

9.	Lausdiener (noch 1905 Verein)................... —	7	000

10.	Lafenarbeiter (1900)................................. 33	532

11.	Straßenbahner (1902)............................ .	1011	1	137

Insgesamt .	5156	32	344

III.	Gruppe: Baugewerbe und Verwandtes.

1.	Maurer, Zentralverband......................... 850	18	580

2.	Zimmerer, Verband.............................. 200	4	413

3.	Töpfer (1894 Verein)........................... 948	2	440

4.	Bauhilfsarbeiter (1894 Verein)................. 200	7	405

5.	Dachdecker........................................... 130	661

6.	Maler usw....................................... 80	5	073

7.	Glaser............................................... 100	927

8.	Stukkateure (1894 Verein)...................... 125	1	257

9.	Steinsetzer.......................................... 232	613

10.	Steinmetzen....................................  .	300_______487_

3 165	41	856

Nach 1894 beigetreten oder verselbständigt:

11.	Marmorarbeiter (1897)................................ 75	240

12.	Rammer (1900)....................................... 350	423

13.	Asphalteure (1901).............................  ■	250	220

Insgesamt .	3 840	42	739

IV. Gruppe: Metallgewerbe.

1.	Bau-Anschläger (1897 Verein)....................

2.	Chirurgische Instrumente (1897 Verein)..........

3.	Klempner (1897 Verein)..........................

4.	Firmenschildarbeiter (1897 Verein)..............

5.	Zinkgießer (1897 Verein)........................

6.	Gold- und Silberarbeiter........................

7.	Metallarbeiter (Verein).........................

8.	„ (deutscher Verband).....................

9. Graveure und Ziseleure (1897 Verein)

10.	Maschinisten und Leizer (1897 Verein)

11.	Kupferschmiede......................

12.	Schmiede............................

500
250
450
40
45
270
5 000
1223

t* t*

•£ 3 «> „

NZZ ä

t* 'S» g § ^
" ?! £ £
2 « t*

7 778	57	473

160	714

640	1	700

200	575

184	3	330

Insgesamt . 8 922

63 792