﻿An die

Handlungsgehilfen Berlins!

RoUegen!

Di, Handlungsgehilfen sind <«» L°g»

ver FazraldrmoLraire übergrschwenkt!

Endlich kommt der freisinnigen Volkspartei diese Cr-
jkerMtniß. Sie sucht daher in letzter Stunde zu retten, was
iwch zu retten ist, um die wenigen ihr treu gebliebenen üuch
schon schwankenden Kollegen an ihr /verschlissenes Banner
zu fesseln.

Doch wie thut sie das? Unfähig, ein eigenes Flug,
ökatt zu verfassen, greift sie rn den bekannten Lngensäck der
„Freisinnigen Zeitung" und druckt den Extrakt derselben
als Flugblatt.

Kollegen! Die sozialdemokratische Partei soll sich erst
vor den Wahlen ‘ drs 8 Uhr-Ladenschlussev erinnert hohen:
so steht auf der ersten Seite des Machwerks! Ihr, die Ihr
unser Blatt, „Der Handels-Angestellte", gelesen habt. wißt,
daß. so lange diese Frage aus der Tagesordnung ist. wir,
die sozialdemokratischen Handlungsgehilfen, unentwegt für
diese Forderung eingetreten ' sind und daß sich im gleichen
Sinne der sozialdemokratische Parteitag zu Gotha 1896
entschieden hat. — Aber dir Abschreiber des Flugblattes
Sesne« 1-r alleine die Unwahrheit ihrer Behauptung.
Denn wie steht auf der zweiten Seite ihres Machwerks?
„Die Sozialdemokratie macht den Freisinnigen zum Vorwurf,
daß in der Kommission für das Handelsgesetzbuch ihre An-
träge auf Einführung eines MaximalarbeitStatzeS für
Handlungsgehilfen mit allen Stimmen gegen die der Sozial-
demokraten abgelehnt worden sei."

Die foziardemokratifche Partei hat bereits im
Jahre 1830 bei Vorlage eines Entwurfes, zu eiuem voll-
ständigen Arbeiterschutzgesrtz die Festlegung der Arbeitszeit
nLch für das HandekSgrwerbr gefordert und fcitd-m ist
r.nsrre Presse unausgesetzt dafür eingetreten.

Die frerstnurse NoU-.oparter dagegen hat sich in
der von den freisinnigen Kollegen zitirlen Rede Eugen Richters
Argen den gesetzlichen 8 Uhe-Laderrfchl«tz erklärt,
trotzdem der Herr alle Mißstände im Handelsgewerbe, die
nach den entsetzlichen Enthüllungen in der Kommission für
Ar'oeiterstatistik klar vor Allee Augen lagen, anerkennen
mußte. Und wie nennt das freisinnige Muchwer! eine solche
Stellungnahme? „Jeder Unbefangene wird zugeben müssen,
daß diese Stellung die Jnterrss« der Handlungsgehilfen in

jeder Beziehung berücksichtigt.- Wer lacht da? — Wir
wiederholen noch einmal den Vorwurf, daß Mitglieder der
freisinnigen Volkepartei bei Gelegenheit der Berathung des
Antrages Gemp auf Verschlechterung der Sonntagsruhe
im Abgeordnetenhause geschwiegen haben! Zum Eintreten
für die Interessen der Handelsangestellten bedurfte es bei
dieser Gelegenheit nur Mannesmuth. der auch diesmal den
14 Freisinnigen 'wieder fehlte.

Unerhört ist der Vorwurf, die sozialdemokratische Partei
habe ihre Anträge bei Berathung des Handelsgesetzbuches
nicht ernst gemeint! Wie würden die freisinnigen Mannes,
freien zetern, wenn wir einen solchen Borwurf bei irgend
einem Antrage der Freisinnigen in die Welt schleuderten! —
Die Freisinnigen sagen weiter, unsere Angriffe seien nnr aüü
dem Zusammenhange herausgerissen. Theile von Reden.

Gut? Lassen wir die Thaten sprechen.

Dir Irrlstunstzevr, beider Richtungen. HkLurmtett
gegen die gesetzliche Festlegung der Sonntagsruhe;
gegen die Einrichtung von kaufmännischen Schiedsgerichten
analog den Gewerbegerichlen;
gegett die Sitzgelegenheit für Verkäuferinnen;
gegrr» Beschränkung der Arbeitszeit für jugendliche Arbeiter
und Gehilfinnen;

gsgett MinimalründigungSfrist von 4 Wochen;
gegen Freigabe von zwei Stunden täglich für Kollegen in
gekündigter Stellung;

gegen die Festsetzung einer gesetzlichen Arbeitszeit im HandelS»
gewerbe, getreu den Worten Eugen Richters: „Die
freisinnige Vollspartet ist gegen die Einführung eines
allgemeinen Maximalarbeitstages." (Abc-Buch für
1893. S. 24.)

Die Frrlstrmrge». beider Richtungen. fttmwltn
füv die Konkurrenzklausel;

und fKe die Knebelparagrapheu 9 uud 10 des Gesetzes gegen
den unlauteren Wettbewerb.

Das find Thatsache». rmd Thatsache»« vshtu
eltte eindringliche Sprache.

Um diesen Verrath, an unseren vitalsten Interessen zu
verschleiern, glauben die Abschreiber des Flugblattes, auf
die Verhältnisse der Handlungsgehilfen im Zukunftsstaare.
natürlich wie er in ihrem Hirn spukt, hinweisen zu müssen.

Wie steht es aber in der Gegenwart?

Ist wirklich jeder Chef Handlungsgehilfe gewesen?
Kann wirklich jeder Handlungsgehilfe Chef werden?

, ; [ 173 und 174. Flugblatt zur Reichstagswahl 1898