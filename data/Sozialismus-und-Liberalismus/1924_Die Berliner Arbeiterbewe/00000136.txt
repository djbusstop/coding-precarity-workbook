﻿Der Sozialist.

Organ der unabhängigen Sozialisten.

Redaktion und Expedition: Alts Zakodstraße 91, Hof 3 Treppen.

	»Der Sozialist" erschein, wöchentlich einmal.  aeo,»prell: Monatlich »0 ps. vierteljährlich, dtirch die post bezogen I Mk. ro ps. ohne Bestellgeld; direkt per Kreuzband I Mt. c Inserlionrprei» pro ^gespaltene peiilzeile >» Ps.	o Pf. — Linzel-Nummer (Ö Pf.
M I.	Sonntag» 6cu 15. November 1891.	I >. Jahrgang.

Inhalt: AbounemeiilS-EIülodmig. —Unftr Zweck —
Gcgcn den Slslal8jojioliJmu4.. — Vom Kampf btt Partei-
Itlluiifl gegen die Opposition. — Nach dem Parteitag. —
Depesche oder Postkarte. — Zur Bewegung. — Zur Ab-
wehr. — Abrechnung. — Inserate.

Alionncirrenls-Einlnduirg.

.Der Sozialist' tritt mir hcntigcr Nummer znm
erste» Male an die Ocfscntlichkeit. Er wendet sich in erster
Linie an die unabhängig denkenden Sozialisten und an
olle Diejenigen, denen eine Klärung der sozialistischen Prin-
zipien am Herzen liegt.

.Der Sozialist' erscheint am Sonnabend jeder Woche
zum Preise von 40 Pf. pro Monat; siir Monat November
20 Ps. Bei Bezug durch die Post beträgt der vierleljähr-
iiche SbonnemenISprci» I Mk. 20 Ps.; direkt per Kreuz-
band l Mk. 60 Ps. Die Einzel-Nummer kostet 10 Pf.

Bestellungen nehmen alle Postanslalten. Kolporteurc
und ZcitungS-AnSlräger enlgcgcn.

Wir bitten Gleichgesinnte, sich die Verbreitung deö
.Sozialist' angelegen sei» z» lassen und Probenummern
nach Bedarf von \iii-3 zu verlange».

Die Expedition des „Sozialist"

grrtin §.. fttr Jnkobstr. 91, gof «>.

lHuscr Zweck.

f Wer die Vorgänge der leijlc» Zeit ansvicrlsam bcob-
m.j.'.k, den wird uns« Erscheine» nicht überrasche»

Innerhalb der sozlaldeinokratischc» Partei machte sich
bercitb ivährcnd der lebte» Tage de» AuSnahm-geseheS eine
licsgchcnde Gählntig gellend. Die Parieiverhälluisse dräng-,
len schon damals zu einer Klärung, Anzeichen innczcr
Korrnplion riese» in ntanchei» Genossen das Gcsühl de«
Unbehagens »nd der Mißstimmung wach. Die NcichitagS-
sraklion enlsernle sich iuuner mehr vdn> rein proletarischen
Boden; sie begatt» Im Lager de» Kieinbürgerthumr unter-
zntaitchcir. At, die Endziele de» SozialiSnutS kehrte ste sich
inöglichst wenig; vielmehr stichle sie iiti PosstbiliSinuS dar
Heil der Arbellerklasse. Etat! ans dem Boden de» Llasscn-
kamvseS zu verharren, schickte sie sich a». di« SescbeSstickeret
im Große» zn betreiben. Nicht di, prinzipielle Propaganda,
sondern da» Paclaineni imirde ihr Ideal; nicht der rcvo-
Ptiiotiärcn Bcwegnug, sonder» der Sozialreson» widmete
man seine Kräfte. Z» alledem knin noch die Diklalttr der
Parteileitung, die jedes selbständige Denke» der Genosse»
erstickte. Wie ein eiserner Druck lastete die hcrauSgebildetc
Zentralisation ans der freien Bewegung des Proletariat».

Scho» daittolS lirste» sich einzelne Slitnmcn vernehme»,
«elä)e vor dem Weiierschreile» aus dem cittgeschlagenen
Wege warnte». Sic übtet, an den Misiltäiidcti nickt,attSlosc
Kritik und machte» aus da« Vcrhängiiibvolle der neuen
Taktik ausmcrksanr So ersülllen sie ihre Pflicht ,1» Ge-
nossen. Aber der Geist der Diklatur war noch zu »tüchtig;
die eiserne Zentralisation hielt die Masse» »och zu sehr ge-
knedc» — dir den Arbeitern eingedrillte Disziplin erllüric
die Mahnrufe für Hochverrolh an der Partei I Man «er-
kündete: .Disziplin geh! über das Prinzip!' und die Menge
nickte dazu mit dem Kopse. Die Kritiker wurden der Ber-
üchtlichmachung sozlaldcinokralilchcr Slaalscinrichlungcn für
schuldig besundcn — man stempelte sie nach alter Manier
zu Polizeispitzeln und räudige» Schnsen. Die selbst von
der bürgerlichen LescllschasI zugelassen- Vertheidigung wurde
ln diesem Falle dadurch verhinderl, das, mün die Angrklagten
einfach niederschrie. Noch iticnialS hat sich die brutale Ma-
joriiütShcrrschafl so verderblich gräustert, als gerade damal«.
Und itoch nie zelglc sich die Mache geschickter Demagogen
drntllcheri So wurde die erste Regung der Opposition in
Dresden, Magdeburg und Berlin niedergeworfen.

Der Parlcilag zu Halle hieb all' diese Mastregein gut.
Die Opposillon hotte man bi» aus einen Genossen von vorn-
herein von jenem Kongrelle sernzuhallea gewußt. Aus dcm-

jclben wurde die Zciitralisalion woinSgllch »och enger-ge-
schmiedet. die Diklatur noch mveitert. Im Parieivorstand
schuf man eine Behörde, deren Macht und Ei»fl»b in-
zwischen klar geworden Ist. Die Parleiorgauisallon hat sich
als Fessel jsir jede selbständige Bewegung, für jeden freien
Meinungsaustausch erwiesen; sie niacht ihre Mitglieder zu
willenlosen Werkzeugen einer von der Diktaiur approbirlen
DiSzivlltt. Und wer sich nickst unterordnet, der .stiegt ein-
sach hinaus' - wie cS nach berühmtem Muster heiß,.

Auch sonst ist Alle« so gclommcii, wie c« die Opposillon
bei ihren, ersten - Anslrclcn erwartete. Die Partei befindet
sich im schönsten PosstbiliSinuS. Ihre seiiherige RclchSlagS-
IHLIIgkcil hat dies ans daS Bestimmteste gelehrt. Man
denke doch mir m> die Bernlhimgen deS ArbcilerschntzgcsctzeS!
ES war ein Jammer, wie sich dicicnigcn gcbcrdeie», die das
Proletariat zu vcttrclcn vorgaben — bcschänicnd für eine
Arbeilerpuclei! Wenn e« noch etncS Beweises für den klein-
bürgerlichen Charakter der heutigen Sozialdeniokraiie dcdursl
hällc: die pnrlnmcntnrijche Berlrelinig selbst hat ihn i» dieser
Periode hnnderlsnch erbracht!

ES war daher mir natürlich, daß sich die Opposition
imuirr lcbhaster geltend machte. Und ebenso erklärlich war
e«. dab die Zusammenstöße mit der Parteidillaliir gerade
zu jener Zelt am hcsligstcn ersotglcti, alS der Internationale
Ärbellerkongreß zn Brüssel beschickt tverdcii sollte. Allen
ehrlichen Gcnossctt mußte daran liegen, die sozialistischen
Prinzipien aus dem Kongresse »»verwässert und unverfälscht
vertreten zu sehen. Sic vermochten jedoch nicht zu hindern,
daß auch hier die kleinbürgerlichen Beslrebiingen biumphirlcii.
Aber der Stclü war nun unaushallsai» i»'S Rallen gebracht.
Der Kampf wurde immer erregter. Man sachte die Oppo-
sition niederzuschreien; man verleumdete sie, so viel mna
tonnte; man erklär,« sic für Polizeiinnchc — kurz: man
bol alle Mittel auf. um dir Minderheit z» unterdrücken.
Und babei halten die ParteihLuptliNge und ihre Kreaturen
die Slirn, zn ertlären: das Recht der strllif und der
freien Meinungsäußerung sei nicht gcsührdel!

WaS vorher bereits feststand, geschah ans dein Erfurter
Parteitage: man sordcrlc den Ausschluß der Opposition —
natürlich Initiier üit Interesse der freien Meinungsäußerung!
Piait verbreitete nnler bei, Dctcgiricn eineAnklageschrift
gegen die Hochverrülhcr. die nach allcil Regel» staaiSanivalt-
schastlichcr LcrdrchnngSknnst abgesaßl tvar. Die Angeklaglen
zogen ej! jedoch vor, selbst ihren AuSlrllt z» erkläre». DaS
hinderte de» Parlcilag aber keineswegs, sic hinlcrhcr noch
Immer scierlich ouSznjchlirßcn — auf Bclreibrn derselben
Herren, die einst selbst daS Recht der Opposition für sich
in Anspruch genommen! Jehl inöchte man natürlich dir
Bedeulmig de» Vorganges abschwächen. Die Acwalllgc»
de» ParlcivorstandeS bchanvlc». der Ausschluß richte sich mir
gcgcn die Personen Werner und Wildbcrgcr - nickst gegen
die Opposition. ES muß weil gckomme» sein, wein, man
die Genossen durch solche Spicgelfcchtcrci zn täusche» ivagl!
Wer de» Verhandlungen de« Parteitages gefolgt ist. weiß,
daß Ihaijächlich die Opposition als soichc ausgeschlossen
wurde; daS Volum richtet sich ivider die abweichende Mei-
nung — mögen auch »och so viele Parlcibüllcl das Gegen-
theil gläubig nachbeten! Man stempelte Kritik undAnsichten
der Oppostlio» zu Bcrlcnmdnngcn, uni mit staotSanwalt-
Ichaslilcher Logik sagen zck können: die Opposition wurde
wegeik- Verleumdung der Partei ausgeschlossen. Und
daß er wirklich die abweichende Meinung war. welche man
auSsließ, daS Hai Herr Singer in einem milKbiufttcii Augen-
blicke selbst verrathen. Er sagte im vierten' Berliner Wahl-
kreise gelegentlich der Berichterstattung über dr» E-rsurlrr
Parteitag: .Seien wir sroh, daß wir die Nörgler
an der dlsherigen Taktik loSgrivordeii sind.'

Nachdem nun d,e Oppostiioa außerhalb der Panel-
organisalion steht, muß sie die Propaganda sür die Ideen
de« Sozialismus aus eigene Hand betreiben. Sie wird
selbständig und unabhängig operircu. In Berlin hat sich
die Opposillon bereit« zu einem .Verein »»abhängiger So-
zialisten' znsammkttgcschaart, und gleiche Vereinigungen
werden in anderen Jndustrlczenlrcn enistehen. Uebcrall de-
ginnt man einzusehen, daß ' t« unmöglich ist. Im engen
Rahmen der sozialdemokratische» Organisalion noch todter
erfolgreich für die Befreiung deS Proletariats zu wirken.
Mit.diesem Zusammenschließen aller unabhängig denkende»
Sozialisten ist er aber nichk allein gethan. Um die Prin-

zipien deS Sozialismus verfechten, den klcinbürgerlich-posffbi-
listlschcn Bestrebungen der Sozialdemokratie entgegenarbeiten
»nd alle Angriffe abwehren zu kömien — dazu bedarf es
in 'erster Linie deS bcwührlcstcn KampsmillcIS: der Presse.
Die Parteiprcssc hat sich unsShig erwiesen, alle Meinungen
der Genosse» znm Ausdruck zu bringen. Sie folgt in
ihrer Haltung nur der parleibchördlichcn Mnrschrouic nnd
wag! «S nicht, Andersdenkende» Raun, zu gewähren. Und
sie muß derartig schablonenhasl werden, iveil über ihr die -
Zensur deS ParlcivorstandeS schwebt. DaS Recht der sozial
dcinokralischcu NcichSregicrung, die Haltung der Prcffe - zu
koiitroNiren - dieses Recht führt zur Knebelung dcS freie»
Wortes, raubt der Publizistik Lust und Licht, sodast hier
eine Versumpfung „oll,wendig eintreten muß. ES Ist also
unmöglich, nnscrc Anschauungen im Rahmen der Parlei-
presse zur Grllung zu bringen, »nd daher uiachke sich sür
die unabhüngigcn Sozialisten ein eigenes Organ nöthig.

Wir werden unsere Ausgabe zu crsüllcn suchen, indem
wir unS der Hauplsache nach von folgenden Grundsähen
leiten lassen.

Unser Ziel ist die wlrlhschastliche nnd damit die io
ziale Besrcinng der Arbeiterklasse. Wir bckämpsen Hnrschast
»nd KncchlschasI in jeder Form, die »laicricllc. wie die
geistige. Dies wird möglich durch die Aushebung de» bürger-
lichen PrivatelgenihinnS. InSbejonderc durch Lergcscll schal-
lung der ProdnkiionSniiitcl. An Stelle bcB bculigcn So-
zial- nnd WirtbschaslSzustandrS tritt da« gemeinsame Eigen
lhum, die gemeinsame Produliion und die gemeinsame Kmi--
snnilion — wie dict schon nu° der Tendenz der gejchichl-,
lieben Enlwicktnng hervorgeht

Diese« Ziel allein hat für eine strrilg - ioziolistische
Bewegung «IS Gnindlage zu bicnciu Ausgabe der Agi-
tation ist cS, da« Prolclariat dafür vorzubcrrilm, eS zu
orgaiiistrcn für die Bcsrciung. ES ist klar, daß die Be-
wegung durch nnd durch rcvuiullonär sein must, da eine
freiwillige Abdankung der Bourgeoisie mnuögUch crwarlct
tverdcii kann.

Wir verwerfe» alle Koinpromisse mit den beerschendra
Klaffen »nd jrdcS Entgcgenlommc» seitens der Aibeiicr.
Uiitcrhandlnngcn mit der Bourgeoisie cmsm-cckicil einer orole-
larlsch-rcvolulioiiären Bcivegung nicht. Darin» bleiben wir
Gcgncr der gesetzgeberisch - varlnmcninrisckirn Thäligkcil; die
Erlnhrung hat gclchrl, daß dirsclbc «»abwendbar zur »nc-
rnpiion nnd zum PossibiliSnins führt. Man muß scsthallcn.
daß das Parlanient eine Instllntio» Ist. durch welche die
Bourgeoisie ihre Herrschafi Über daS Proletariat ausübt.
Hier elwaS sür die Arbcttcr ertrotzen ober erbetteln zu
tvollcn. ist daher einfach umnöglich. Je weiter sich die
bürgerliche Gesellschaft entwickelt, desto klaffender werden die
Klaffe,igcgcnsätze zwischen Bourgeoisie »nd Proletariat. Der
Boden sür die Unlcrhandliingen mit der Bourgeoisie schwin-
de, Immer »uehr nnd Immer hcsliger muß der Kiaffru-
kamvs enldrcnnen. So wird dar Proleiarial in stcigciideni
Pkäßc gezwungen, der herrschenden Klasse gegenüber eine
rein abwehrcndc Taktik einzuschlagc»; die positive Pkitarbeil
an der Gcjehgcbiing wird mehr den» je zur iinmSglichkft.
Und wenn das Prolclariat einst In die Lage kommt, eine
portamenlarischc Mehrheit bilde» zn können, so wird iB
daraus verzichten, weil <S bnini sein Ziel auf einem weit
kürzeren Wege zu erreichen vermag.

U»S erscheint der geiverfschaflllch - sozialistischc Klaffe»'
kanws alS die ziveckmäßigstc Forni, unter welcher ber Ar-
beiter heule der Bourgeoisie gcgcnübertrett» tan». Er hält
zugleich die Waffen in Muß. fördert die Orgaiiisatlo» »nd
die Propaganda, stärkt daS SolidarilälSgcsbht und beschleunigt
den Konzenlrationkprozeß deS Kapitals, indem er nament-
lich de» Kteinbettieb vernichten Hilst.

Dl/Beseitigung deS Kleinbürger, und Kleinbauer»«,»!»«
hallen wir für eine der Vorbedingungen de«. Sozialismus.
Die wirihschastliche- Entwicklung räumt bereits damit ans,
und wir werden den Untergang dieser Elemente nur zu de
jchkcnnigen suchen. Da« ist eine» der wicht igstc» ttu-er-
schcidungLincrkmalc ztvischen Mi» und der osslziellc» Soztat-
dcniokratic.

Innerhalb der proletarischen Betvegnng selbst werden
Wir jede zwangsweise Zculralisniio» bckSiupfen. -weil
durch diese OrganisalionSfonn die Machlbesngmffc
den Händen Einzelner HLufen nnd somit stet* d>c ©c.aiir
dn Diktatur droht. Der selbständigen Beweg,mg ^niciaer
Glieder n:uß da« zentralistische Prinzip auf ftdeu 0-a»

63. Titelseite der ersten Nummer des „Sozialist" 1891