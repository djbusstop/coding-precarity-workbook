﻿423

solche Antwort mußte bei den letzteren den Beschluß noch befestigen, die
Liberalen ihrem Schicksal zu überlassen. Sie konnten den Drang zur Reform
des Wahlrechts bei Leuten unmöglich ernst nehmen, die sich so engherzig
auf den Buchstaben eben dieses Wahlrechts steiften. Es fehlte in den
Reihen der Liberalen nicht an Leuten, die von der Antwort ihres Wahl-
ausschusses wenig erbaut waren und bis zum letzten Moment sich bemühten,
eine Änderung herbeizuführen — noch am Tage der Abgeordnetenwahl
versuchten die Linksfreisinnigen Dr. Allstem und Fabrikbesitzer Stern eine
liberale Wahlmännerversammlung, die in der nächsten Nachbarschaft des
Wahllokals tagte, in diesem Sinne zu bearbeiten, — aber alle diese Schritte
erwiesen sich als vergeblich, und ebenso vergeblich waren Vermittlungs-
versuche der Professoren Delbrück, Lißt und anderer.

Trotz alledem sollte gerade im Kreise Teltow-Bceskow-Charlottenburg
das Eingreifen der Sozialdemokratie dem elenden Wahlsystem einen kräftigen
Stoß versetzen.

Für die Abgeordnetenwahl hatte die Regierung das Lokal „Neue
Welt" in der Lasenheide im Hinblick auf dessen großen Saal gewählt, der
aber auch nur knapp hinreichte, die 2500 Wahlmänner zu fassen. Der
Landrat des Kreises Teltow, von Stubenrauch, amtierte als Wahlkommissar
und hatte, um seinen Willen zur Anparteilichkeit zu dokumentieren, auch
zwei Sozialdemokraten, die Stadtverordneten Jäger und Wuhky-Rixdorf,
ins Wahlbureau berufen. Ein starkes Aufgebot von Schutzleuten umgab
das Lokal. Doch ließen die sozialdemokratischen Wahlmänner, die hier,
wie in den Berliner Wahlkreisen, die ersten am Platze waren, es nicht zu,
daß Polizisten in Aniform im Wahllokale selbst sich aufpflanzten. Schon
bei der Prüfung der Wahlmännermandate kam es zu erregten Szenen,
die aber noch beschwichtigt werden konnten. Am 1l2l\ Ahr vormittags
begann der eigentliche Wahlakt, und mit großem Jubel ward es von seiten
der Sozialdemokraten begrüßt, als der erste Wahlmann, der aufgerufen
wurde, seine Stimme für die Kandidaten Hirsch und Zubeil abgab. Dann
ging der Wahlakt weiter und es zeigte sich bald, daß er viel Zeit in An-
spruch nehmen sollte. 2600 Wahlmänner nacheinander einzeln aufrufen,
warten, bis sie an den Tisch treten und ihre Stimme protokollieren, das
ließ sich nicht in ein paar Stunden erledigen, zumal die sozialdemokratischen
Wähler gar kein Interesse daran hatten, den Wahlakt zu beschleunigen,
wohl aber ein sehr großes Interesse, das Wahlsystem mit seinen An-
geheuerlichkeiten unmöglich zu machen. Die meisten hatten sich genügend
verproviantiert, um zur Not den ganzen Tag im Wahllokal zubringen zu
können, und übereilten sich nicht gerade, wenn der Ruf an sie kam, mit dem
Vortreten und Wählen. Nach und nach ward Herr von Stubenrauch, der
gewöhnlich sich durch seine kaltblütige Ruhe auszuzeichnen pflegte, nervös
und nervöser, und schließlich ging er unter lautem Protest der Sozial-
demokraten dazu über, zu gleicher Zeit mehrere Wahlmänner aufzurufen
und, wenn Wahlmänner nicht schnell genug vortraten, außer der Reihe zu
rufen. Das widersprach dem Reglement und veranlaßte, als Einsprachen
nichts fruchteten, die sozialdemokratischen Beisitzer, das Wahlbureau zu ver-
lassen. Aber cs half selbst nicht allzuviel; denn nun ward es im Wahl-
lokal immer unruhiger, und die sozialdemokratischen Wahlmänner konnte
man nicht zwingen, im Sturmschritt an den Wahltisch zu laufen, ganz