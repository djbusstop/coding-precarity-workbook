﻿- 140 -

Wahlkreis ward eine Resolution angenommen, welche jenen Beschluß wieder-
holte und außerdem verlangte, daß

„überall, wo durch die Beteiligung der Sozialdemokratie Einfluß auf die
Gestaltung der Wahl vorhanden ist, sozialdemokratische Wahlmänner
aufzustellen sind."

2m zweiten Wahlkreis faßte man überhaupt keinen Beschluß, im dritten
erklärte man sich dafür, daß die Entscheidung über die Frage den preußischem
Delegierten überlassen werde, im vierten, fünften und sechsten Wahlkreis
dagegen wurden Resolutionen beschlossen, die sich mehr oder minder schroff
gegen die Wahlbeteiligung aussprachen. Im ersten, dritten und fünften
Wahlkreis wurden bei dieser Gelegenheit noch Beschlüsse gefaßt, die Er-
weiterung der Rechte der Berliner Preßkommission in bezug auf die Kontrolle
des „Vorwärts" und ein Mitbestimmungsrecht Berlins bei der Anstellung
von Redakteuren forderten.

Als im Sommer 1898 Neuwahlen für den preußischen Landtag be-
vorstanden, fand am 23 August eine von den Vertrauenspersonen Berlins
einberufene allgemeine Pacteiversammluag statt, um die Frage zur Ent-
scheidung zu bringen, ob man gemäß der vom Äamburgcr Parteitag in
dieser Sache gefaßten Resolution sich an der Wahl beteiligen solle oder
nicht. Diese Resolution hatte die Beteiligung als dort für geboten erklärt,
wo die Verhälmisse eine solche den Parteigenossen ermöglichten, die Ent-
scheidung darüber, ob dies der Fall sei, aber den Genossen in den einzelnen
Kreisen überlassen. Ein Zusatz dazu verbot außerdem das Abschließen von
Kompromissen oder Bündnissen mit gegnerischen Parteien. Da nun in
Berlin die Möglichkeit eines Wahlsieges ohne Bündnis nicht bestehe, so
sei auch, führte der Referent der Versammlung, G. Ledebour, aus, die
Wahlbeteiligung in Berlin nicht durch die Verhältnisse gerechtfertigt, sondern
Wahlenthaltung zu empfehlen. Seine Ausführungen wurden in der Ver-
sammlung für durchschlagend befunden, und cs ward eine in diesem Sinne
lautende Resolution beschlossen, die außerdem noch betonte, daß die bürger-
lichen Oppositionsparteien die auf sie in den letzten Jahren gesetzten Hoff-
nungen schwer enttäuscht hätten und die Arbeiter nach wie vor auf ihre
eigenen Kräfte angewiesen seien. Dieser Resolution schloß sich auch am
28. August eine Kreiskonfercnz des Wahlkreises Tcltow-Bceskow-Char-
lottenburg mit 35 gegen 17 Stimmen an.

Am 27. Dezember 1898 fand in Wilkes Restaurant die erste Konferenz,
sozialdemokratischer Gemeindevertreter der Provinz Brandenburg statt. Sie
war von 46 Gemeindevcrtretern, darunter 33 von Groß-Berlin, besucht und
arbeitete ein ganzes kommunalpolitisches Programm aus, dessen Text im
Kapitel über die Kommunalpolitik der Berliner Sozialdemokratie zu finden
ist. Auf Antrag Paul Singers wurde beschlossen, diese Konferenzen alle
zwei Jahre zu wiederholen.

Von den Konferenzen des Jahres 1899 verlangten sowohl die Konferenz
des Kreises Teltow-Beeskow-Charlottenburg (3. September) wie die Kon-
ferenz der Provinz Brandenburg (17. September) wiederum eine stärkere
und systematisch betriebene Agitation unter den Frauen. In Berlin war
am 11. Januar von einer stark besuchten Volksversammlung nach einem
Referat der Genossin Lily Braun die Gründung eines „Vereins für
Frauen und Mädchen der Arbeiterklasse zur Förderung des Wissens und