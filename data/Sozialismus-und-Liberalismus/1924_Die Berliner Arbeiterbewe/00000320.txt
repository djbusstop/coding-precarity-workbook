﻿306

war, machten sie in ihrer Mehrheit eine Wendung und verlangten nun von
den Fabrikanten, was sie selbst erst den Arbeitern bestritten hatten. Die
Doppelnatur ihrer sozialen Position — halb Arbeitgeber, halb Arbeiter —
brachte es mit sich, daß sie nun — wenn auch nicht auf sehr lange — ihr
Arbeiter herz entdecken sollten.

Am gleichen Tage, an dem der Reichstag die Lage der Konfektions-
arbeiter besprach, fanden auf dem Gewerbegericht Konferenzen von dessen Vor-
sitzenden mit Vertretern der beiden LInternehmerkategorien statt. Die Fünfer-
kommission derArbeiter hatte am 10. Februar gleichzeitig mit derProklamierung
des Streiks offiziell sich zu Verhandlungen vor dem Gewerbegericht bereit
erklärt, und nun suchte der Vorsitzende des Gerichts die Unternehmer zu
bewegen, seine Vermittelung anzunehmen. Wieder lehnten jedoch die Ver-
treter der Damenmäntel-Konfektion jedes materielle Entgegenkommen ab.
Lohnzugeständnisse könnten höchstens die Zwischenmeister machen, erklärten
sie. Darauf lud das Gewcrbegericht eine Anzahl Zwischenmeister, die von
den Fabrikanten als kompetente Beurteiler bezeichnet waren, auf den
12. Februar zu einer Besprechung ein, die aber fast dasselbe Gesicht zeigte
wie die Besprechung mit den Fabrikanten. Die Zwischenmeister der Herren-
konfektion sperrten sich weniger gegen Verhandlungen mit dem Arbeiterkomitee,
als die der Damenkonfektion. Beiläufig kein Wunder, da in der Herren-
konfektion die Arbeiter meist männlichen Geschlechts sind, die Damen-
konfektion aber ganz überwiegend Frauenarbeit ist. Nur die Knaben-
konfektion war gleichfalls zumeist Frauenarbeit. Das Verhältnis von Meister
und Arbeiter war in den letzten beiden Zweigen ein wesentlich anderes als
in der Herrenkonfektion.

Da kam es am Abend des 12. Februar in einer großen Versammlung
der Zwischenmeister zu einer charakteristischen Wendung. Berichte über die
Vorgänge im Reichstag, Angaben der Fabrikanten in der Presse, daß sie
durchaus hinlängliche Arbeitspreise an die Meister zahlten, so daß diese für
abnorm niedrige Löhne verantwortlich wären, hatten einen Teil der Meister
äußerst wild gemacht. Ihr Zorn richtete sich nunmehr gegen die Fabrikanten
und machte sich in heftigen Reden gegen diese Lust, bis schließlich einer
auftrat und vorschlug, mit den Arbeitern gemeinsame Sache gegen die
Fabrikanten zu machen. Da die Arbeiter schon streikten, gehörte zu dem
Entschluß nicht viel, er fand sofort stürmischen Beifall, und es verstärkte die
Position der Arbeiter moralisch und bis zu einem gewissen Grade auch
materiell, daß bald gegen 1500 Zwischenmeister mit ihnen als „Streikende"
den Fabrikanten sich gegenüberstellten. Die Flut stieg.

Das bezeugten weiterhin fünf Riesen-Volksversammlungen, die am
Sonntag, den 16. Februar, vormittags, sich mit dem Konfektionsarbeiterstrcik
und seiner Behandlung im Reichstag befaßten. Ein ungeheurer Menschen-
strom, der sich zu ihnen drängte, hatte die Säle Stunden vor der Eröffnung
gefüllt. In vieren von ihnen sprachen Mitglieder des Reichstags: Richard
Fischer, der im Reichstag die Halbheit der bürgerlichen Redner und
Parteien kraftvoll gegeißelt hatte, Robert Schmidt, Bruno Schoenlank
und Emanuel Wurm, in der fünften referierte Marie Greifenberg,
selbst eine Näherin. Der Inhalt ihrer Reden ergab sich von selbst, sie
gipfelten in Anfeuerungen an die Streikenden, muüg auszuharren, und an
die übrige Arbeiterschaft und die mit den Arbeitern fühlende Bevölkerung,