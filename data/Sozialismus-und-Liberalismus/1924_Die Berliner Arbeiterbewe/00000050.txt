﻿bürgerlichen Linken bis zu den Nationalliberalen unterstützt, dem Vorhaben
durch immer neue Abänderungsanträge, Veranstaltung von Auszählungen
und namentlichen Abstimmungen einen hartnäckigen systematischen Wider-
stand entgegen, der sich vom 13. März an eine Woche hinzog und damit
endete, daß die klerikal-konservative Mehrheit vor der Minderheit die Segel
strich und die angefochtenen Bestimmungen fallen ließ. Ein in der literarischen,
künstlerischen und wissenschaftlichen Welt, in der man einen „Goethebund"
zur Verteidigung der bedrohten Rechte der Kunst usw. gegründet hatte,
mit fast einhelligem Jubel aufgenommenes Resultat. Alles, was sich für-
liberal hielt, freute sich über diesen gelungenen Obstruktionsfeldzug der
Sozialdemokratie.

Es sollte indes nicht lange dauern, bis ein zweiter und viel wichtigerer
Obstruftionsfeldzug der Sozialdemokratie einer wesentlich anderen Stimmung
begegnen sollte.

Anfang 1901 wurde der vom Grafen Posadowsky zur Vorberatung
eines neuen Zolltarifs eingesetzte wirtschaftliche Ausschuß mit seinen Arbeiten
fertig, und bald darauf erblickte ein entsprechender Zolltarif-Entwurf der
Regierung das Licht der Welt. Er ging am 23. Juni 1901 dem Bundesrat
und fünf Wochen später dem Reichstag zu. Seine Grundzüge gegenüber
dem alten Tarif waren eine bedeutend größere und, wie anerkannt werden muß,
für seinen Zweck auch systematischere Spezialisierung der Tarifsätze,
Erhöhung fast aller Zölle auf Landwirtschaftsprodukte und eines großen
Teils der Zölle auf Industrieprodukte, sowie die Festsetzung von Mindest-
zöllen auf Getreide, Vieh, Fleisch und andere landwirtschaftliche Erzeug-
nisse, die sehr bedeutend über die Sätze des bestehenden Vertragstarifs
hinailsgingen. Er stellte also eine Verteuerung der notwendigsten Lebens-
mittel des Volkes und außerdem ungünstigere Handelsverträge für die
deutsche Industrie in Aussicht und war damit ein Triumph der Agrarier und
der mit ihnen verbündeten Magnaten der Industrien der Roh- und Halb-
fabrikate. Diese letzteren, die fast alle syndiziert siild, waren durch die
Auslandszölle weniger bedroht, weil die Beherrschung des heimischen
Marktes es ihnen ermöglicht, zu Schleuderpreisen zu exportieren. Das
können aber die deutschen Fertigindustrien nicht oder nur durch sehr viel
stärkeren Druck auf die Löhne, und sie beschäftigen die große Masse der
Arbeiter.

Damit war die Stellung der Sozialdemokratie, soweit sie nicht schon durch
ihre allgemeinen Tcndeirzen vorgezeichnet war, zu dem neuen Zolltarif
gegeben. Die Partei der Arbeiter mußte alle Mittel in Anwendung
bringen, den Tarif nicht Gesetz werden zu lassen oder wenigstens zu erwirken,
daß dieses, so tief in die Erwerbsgelegenheiten der großen Volksmasscn
einschneidende Gesetz nicht eher zur endgültigen Abstimmung kam, bis nicht
den Wählern erst noch Gelegenheit gegeben war, sich über es zu äußern,
sind das geeignete Mittel war die parlamentarische Obstruktion, wie sie in
allen Ländern bei solchen Gelegenheiten zur Anwendung kommt und wie
sie im preußischen Landtag die junkerlichen Agrarier immer noch der Kanal-
vorlage der Regierung gegenüber praktizierten.

Schon in der ersten Tagung des 1898 gewählten Landtages hatten die
Agrarier der Regierung Trotz geboten und, allen Zuredens und Drohens
ungeachtet, ain 19. August 1900 die ganze Kanalvorlage abgelehnt. Darauf