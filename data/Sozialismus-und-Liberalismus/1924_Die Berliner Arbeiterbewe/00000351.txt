﻿337

ring, der fester zusammenhielt, als man erwartet hatte, machte seinen Ein-
fluß nach außerhalb geltend, und gerade die großen Brauereien im Lande
ließen sich schließlich um so eher bewegen, mit den Berliner „Kollegen"
Solidarität zu halten, als viele von ihnen ebenfalls schon die Anannehm-
lichkeiten des Boykotts der Arbeiter zu verspüren bekommen hatten. Es
wurden sogar die Grundlinien eines nationalen „Schutzverbandes gegen
Boykottschäden" vereinbart, und dem Berliner Schuhkomitee Beiträge zur
Anterstützung der durch den Boykott besonders schwer betroffenen Brauereien
zugesichert. Die kleinen auswärtigen Brauereien wiederum hätten zwar zu-
nächst recht gern an den „großen Berlinern" Rache genommen, die ihnen so
fatale Konkurrenz machten, sie waren aber einfach nicht imstande, soviel Bier
auf den Markt zu bringen, als nötig war, um den Boykott für jene
tödlich zu machen. War doch die Mälzereiperiode für untergäriges Bier
mittlerweile zu Ende, und gemalzte Braugerste ließ sich nicht aus der Erde
stampfen. Die Großen hatten nicht nur die größten Mengen, sondern auch
meist die besten Qualitäten Gerste zur Verfügung.

Am nur ein Bild zu geben: Im Geschäftsjahr vom 1. Oktober 1892
bis zum 30. September 1893 hatte die Brauerei Schultheiß eine Produttion
von 436 809 Hektolitern, die Brauerei Münchener Brauhaus, die damals
alles aufbot, die Gunst der Arbeiter zu erwerben, aber eine solche von
67 644 Hektolitern Bier gehabt. Es war einfach eine technische Anmöglichkeit
für die letztere, ihre Produktion nun plötzlich so hoch zu schrauben, um einen
größeren Teil der Kundschaft von Schultheiß zu bedienen. Trotzdem sie von
der Arbeiterpresse sehr viel Förderung erfuhr, stieg denn auch ihre Produttion
im Boykottjahr um im ganzen noch nicht 30,000 Hektoliter — ein Tropfen
gegenüber der Produktion von Schultheiß!

Vielen Provinzbrauereicn wiederum, die sich anschickten, den Boykott
gegen die Ringbrauereien auszunutzen, wurde das Bürgertum am Ort mit
der Parole auf den Hals gehetzt, cs wäre unverantwortlich, in dieser Weise
die Geschäfte der Sozialdemokratie zu besorgen. Nicht bei allen verschlug das
Mittel. Die Boykottkommission konnte im „Vorwärts" eine Anzahl
Brauereien namhaft machen, die bereit waren, den Berliner Arbeitern
boykottfreies Bier zu liefern. Aber bei gar manchen davon konnte es heißen:
Der Geist war willig, aber das Bier nur schwach oder auch spottschlccht,
während in einer Reihe von Fällen der Appell an die bürgerliche Solidarität
doch Erfolg hatte.

Kurz, es war für einen großen Teil der Berliner Gastwirte äußerst
schwer, ohne Bier der Ringbauereien ihr Geschäft zu betreiben. Es kam
sogar vor, daß Inhaber von Arbcitcrwirtschaften Bier von Ringbrauereicn
weiter entnahmen, aber unter dem Titel von boykottfreiem Bier ausschenkten.
Selbstverständlich mußte, als dies ruchbar wurde, die Konttolle entsprechend
verschärft werden, was zu mancherlei bösen Zusammenstößen zwischen
Gastwirten und den Kontrolleuren der Arbeiterschaft führte, denen man von
der Gegenseite den Spitznamen „Bierschnüffler" anhängte.

Äberhaupt dehnte sich der Kampf der Arbeiterschaft mit den
Ringbrauereien bald zu einem solchen mit einem großen Teil der Berliner
Gastwirte aus. Rückhaltlos für die Arbeiter trat nur der „Verein zur
Wahrung der Interessen der Gastwirte Berlins und Amgebung" ein, der

Bernstein, Berliner Geschichte. III.	22