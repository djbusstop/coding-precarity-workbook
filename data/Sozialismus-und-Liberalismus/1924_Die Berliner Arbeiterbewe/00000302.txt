﻿Zehntes Kapitel.

Die bedeutsamsten gewerkschaftlichen Kämpfe

Berlins.

Momente sind es vornehmlich, welche dort, wo gewerkschaftliche

/ Kämpfe im allgemeinen schon alltägliche Erscheinungen sind, ihnen
besondere Bedeutung zu verleihen pflegen: eine ausnahmsweise
weite Ausdehnung ihres Kampfgebiets, eine ungewöhnliche Heftigkeit
oder Dauer des Kampfes und ein Ziel oder Ausgang, die eine
wesentliche Neuerung in den Bestimmungen des Arbeitsverhält-
nisses darstellen. Daß für Berlin Gewerkschaftskämpfe alltägliche Vor-
kommnisse geworden waren, braucht hier kaum noch bemerkt zu werden.
Es sind nur wenige Gewerbe, in denen nicht jahraus jahrein Streitigkeiten
um Lohn, Arbeitszeit oder Arbeiterrccht zu Kämpfen zwischen Unter-
nehmern und Arbeitern führen, und nicht wenige Gewerbe haben deren
fast regelmäßig mehrere im Jahr. Die meisten davon sind längere Zeit
freilich nur Kämpfe von Arbeitern mit einzelnen Unternehmern oder einer
kleinen Gruppe von solchen, womit jedoch nicht gesagt ist, daß sie deshalb
etwa unwichtig oder uninteressant seien. Als soziale Kraftäußerung hat
jeder Kampf an seinem Platz auch seinen bestimmten Wert, der freilich
bei unüberlegtem, zweckwidrigem Vorgehen auch vorwiegend negativer Natur
sein kann, und selten fehlt es an Einzelheiten von besonderem Interesse für
die Beteiligten und die Wissenschaft des Arbeiterkampfes. Ein Eindringen
in die gewerkschaftlichen Kämpfe des doch verhältnismäßig kurzen Zeitraums,
den dieser Band behandelt, enthüllt eine Fülle von Vorgängen, die unter
den verschiedensten Gesichtspunkten der Hervorhebung wert erscheinen. Wir
sehen neue Urteile über die Einzelheiten des Arbeilsverhältnisses sich bahn-
brechen, eine neue Taktik sich herausarbeiten, und überreich sind diese fünf-
zehn Jahre Arbeiterkämpfe an dramatischen Vorgängen, an Beispielen
tapferen, bis zur Selbstverleugnung gehenden Beharrens im Widerstand,
allerdings auch an heftigen Reibungen in den Reihen der Kämpfenden
selber.

Von alledem könnte unsere Geschichte nur grobe Umrisse und Zahlen-
reihen geben, wollte der Versuch gemacht werden, auch nur die wichtigsten
Momente der größeren Streiks zusammenfassend vorzuführen. Es wurde
daher von ihm Abstand genommen und für richtiger befanden, zwei bedeu-