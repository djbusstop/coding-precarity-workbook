﻿IW Auf zur Wahl! "»I

An -ie MSljler der Arveitgever-Keifttzer
zum Gew erb ege richt!

Sic haben sich Zhr Wahlrecht durch Eintragung in
die Listen gesichert und stehen nun vor der Frage, für
welche Kandidatenliste Sie eintreten muffen, wenn das
Gewerbegericht, diese zur Entscheidung von gewerblichen
Streitigkeiten geschaffene Institution, seinen Zweck voll
und ganz erreichen soll.

Die Entscheidung richtet sich ganz nach den Grenzen,
welche für die Tbätigkcit der Beisitzer geschlich gegeben sind.

Die Einrichtung dieses Ecwerbegerichts appcllirt
lediglich an den praktisch > gewerblichen Sin» der
Bevölkerung, der nach drei Seiten hin sich bethätigen soll.
Diese sind:

1.	Die Entscheidung rein gewerblicher Streitigkeiten

zwischen Arbeitgebern und Arbeitnehmern bezw. zwischen
Arbeitern desselben Arbeitgebers untereinander, wenn sie
eine gemeinsame Arbeit übernommen, desgleichen bei Haus-
. gewerbetreibenden.

2.	Die Abgaben von Gntachteu in gewerblichen Fragen
an Staats- und Kommunalbebörden event. Stellung von
Antragen.

3.	Die Herbeiführung einer Einigung zwischen Arbeit-
geber» und Arbeitnehmern bei event. Ausständen,

Lermittelungsvorschlägs zu machen, welche auch der öffent-
lichen Meinung unterbreitet werden.

Wähler! Daß dies- Funktionen der Beisitzer nicht
ohne ein höheres Verständniß für die allgemeinen Zeit-
sragen ausgeübt werden können, daß zu ihnen auch -ine
besondere Erfahrung und Kenntniß der rcchtsgriind-
lcgonden Bestimmungen gehört, daß aber auch ein
imgetiübtcr Blick für die Entstehungsarten der ver-
schiedenen gewerblichen Streitigkeiten unbedingt erforder-
lich ist, wenn die gefällten Urtheile eine rechtliche
Bedeutung für die Bevölkerung haben sollen — diese
Misere Forderung wird von unseren Gegnern
tendenziös dahin ausgelegt, daß wir in den Gewerbe-
Schiedsgerichien ein Mittel zur Verwirklichung sozial-
demolratischer Ziele sehen und womöglich bei jedem
Streitfall — selbst gegen Gewissen und Ueberzeugung —
nach persönlicher Begünstigung entscheide» lassen wolle».

Run, unscrc Kandidaten sind Ehrenmänner
genug, um zu wissen, wie sic ein öffentliches Amt
auszufüllen haben: sie haben aber außerdem auch die
oben aufgestellten besonderen Fähigkeiten, die wir un-

bedingt von qualifizirten Beisitzern unseres Ver-
tranens voraussetzen und verlangen müssen —
und diese sind »st erprobt vor der Ocffentlichkeit,
wir sic auch nur in dem lebendigen Strom deS öffent-
lichen Lebens erworben werden können. Wir wählen
nicht die Arbeitgeber, weil sie Arbeitgeber sind, in das
Gewerbegericht, sondern weil sic Männer sind, die im
praktischen Dienst der Industrie grob geworden, ihr
Gewerbe nunmehr zwar iclbstständig betreiben, aber
trotzdem stets und ständig in Fühlung mit der
öffentlichen Erörterung aller gewerblichen An-
gelegenheiten geblieben sind. Sic blicken als Arbeit-
geber nicht hochmüthig herab aus die Arbeitnehmer,
sondern sehen in ihnen ihre gleichberechtigten Mit-
incnschcn, die zur gemeinschaftlichen Arbeit an jedem
kultursördcindcn Werke ihren Platz neben ihnen ein-
zunehmen haben, wenn es gilt, bas Unrecht zu ver-
urtheilen und bas Recht hochzuhalten. Darum
wählen wir einzig und allein die von uns —
unter inöglichster Berücksichtigung aller gewerblichen
Kategorien (Spruchkammern) — »nsgestellten Kaudi-
baten!

Wähler! Auch habe» mchrcrc hochangcsehene
Männer, Richter, welche praktisch die Erfahrung in
Gewerbegerichten mit soziäldemokratilchen Arbcitgrdern
und Arbeitnehmern gemacht haben — die Aeußerung
unserer Gegner, wir richte» parteiisch, als grundlos und
tendenziös aufgebauscht hingestellt: sie bekundete», daß
jede Parteilichkeit fernblieb.

Wir wünschen aber auch, dag jedes Bornrthcil uud
partriische Gehässigkeit über UNS uutcrlafscn bleib!. Wir
ändern unsere Meinung nicht nach nuferen Interegcu.
sondern das Wohl der gcwcrdetrcibeaden Bevölkerung
insgesammt ist stets und ständig nufere einzige Stichi-
fchnur, in welchem Sinne unsrer Kandidaten ihr Mandat
auszuüben stch zu iedrr Zeit vor der Oeffentlichkeit uud
ihrem Gewiffen vcrpfiichtct fühle» werden, und somit
entscheiden wir UNS für die auf beigefügtem Stimmzettel
verzeichneten Kandidaten —

ihnen geben wir unsere Stimme!

Die Wahl ist getjeittt und findet mittelst Stimmzettel statt.

Eleichlautcude Stimmzettel

wie die einliegenden, «erde« am Tage der Wahl von uns an den Wahllokale» ausgegeben.

Die Wahl findet statt:

Doruxrstag, den 22. September 1898, Mittags von 12 Ahr ab dis Abends 9 Ahr.

Das Komitee

der Arbritgeber-Wähler für die Srwerbegerichts-Wichlen

Jhr Wahllokal befindet fich:

„Wktoria-Aark", Müchrrftraße 3L

und Verlag: Rudolf Millarg, Snnenstr. 16. — Druü: JaniszewSti & Quitt, Dresdcnerftr. 38.

111. Flugblatt an die Wähler der Arbeitgeber-Beisitzer