﻿267

V. Gruppe: Lolzarbeiter.

Mitglieder in Berlin

1894

1.	Bildhauer............................................. 700

2.	Böttcher ............................................. 400

3.	Lolzbearbeitungsmaschinen-Arbeiter (1900)	....	1 100

4.	Stockarbeiter (1894 Verein)............................ 90

5.	Korbmacher............................................. 71

6.	Möbelpolierer (1894 Verein)........................... 400

7.	Kistenmacher (1894 Verein)............................ 230

8.	Einsetzer (1894 Verein)............................... 240

9.	Lolzzurichter......................................... 304

10.	Bilderrahmenmacher (1894 Verein)....................... 65

11.	Knopfarbeiter (1894 Verein)............................ 70

12.	Stellmacher (1894 Verein).............................. 45

13.	Lolz- und Bretterträger (1894	Verein)................. 105

14.	Vergolder............................................. 600

15.	Musikinstrumentenmacher............................... 800

16.	Lolzarbeiterverband (1894: 2800 Tischler, 96 Bürsten-

und Kammacher, 178 Drechsler).................. . 3 074

Insgesamt .	8 294

1905

1275

466

1728

e
s 4

jo a
£.«

: <9

©

22 060
25 529

VI.	Gruppe: Bekleidung, Stoffe- und Lederverarbeitung.

1.	Landschuhmacher....................................

2.	Lutmacher..........................................

3.	Kürschner..........................................

4.	Lederarbeiter (Weißgerber).........................

5.	Lederzurichter (Lohgerber).........................

6.	Portefeuille- und Lederwarenarbeiter...............

7.	Sattler............................................

8.	Schneider..........................................

9.	Schuhfabrikarbeiter (1894 Verein)....................

10.	Filzschuh- und Pantoffelarbeiter (1894 Verein). . .

11.	Schuhmacher.......................................

12.	Tapezierer........................................

13.	Textilarbeiter (Färber, Posamentierer, Weber u. Wirker

14.	Wäsche-Arbeiter.................................._■_

Nach 1894 beigetreten:

15.	Zuschneider (1905 noch Verein)................_.__■_

Insgesamt .

90	178
830	819
88	129
351	790
100	252
250	630
150	2 067
550	5 000
385)	1895 Im Deut-
207	scheu Schuhni.- Verband
403)	1944
280	1521
786	1554
80	4 000

18 884

170

4 550	19 054

VII.	Gruppe: Graphische Gewerbe, Buch- und Papierindustrie.

1.	Buchbinder, Album- usw. Fabrikation................... 1	150	5	471

2.	Buchdrucker........................................... 3	000	8	689

3.	Buchdruckereihilfsarbeiter........................ 404	2 753

4.	Schriftgießer..................................... 299	548

5.	Formstecher....................................... 55	62

6.	Lithographen, Steindrucker usw.................... 850	3 136

7.	Xylographen ....................................... ■	121________

5 879	20	814

Nach 1894 beigetreten:

8.	Photographen (1904)................................ ■	210________290

Insgesamt .	6	089	21	104