﻿397

Verein, von dem dies zutreffe, könne die Aushebung der polizeilichen An-
ordnung für seine Aufführungen beanspruchen. Damit war der Weg an-
gezeigt, auf dem man sich die Polizei möglicherweise doch vom Halse
halten konnte, zumal der Köllerkurs sein Ende erreicht hatte. Änd so ward,
als im Laufe des Zahres 1897 das Verlangen nach Wiederbelebung der
„Volksbühne" sich immer lebhafter geltend machte, ein neues Statut aus-
gearbeitet, das den Charakter des geschlossenen Vereins in aller Schärfe
zum Ausdruck brachte. Der Kerbst 1897 sah die „Freie Volksbühne"
wieder am Leben und Wirken. Nur kurze Zeit, und auch der alte Mit-
gliederstand war wieder erreicht und bald überschritten. Ein fast ununter-
brochenes Steigen kennzeichnet auch die Entwicklung des neuen Vereins.
Ende 1905 war seine Mitgliederzahl 11000.

Prinzipiell war in der Verfassung des Vereins nichts geändert. Ein
Vorstand mit einem Ausschuß zur Seite leiten ihn, die Generalversammlung,
die sie wählt, ist auch heute die entscheidende Instanz. Vorsitzender des
Vereins wurde Conrad Schmidt, während Mehring in den Ausschuß
gewählt wurde, jedoch infolge eines Konflikts persönlicher Natur nach
Jahresfrist aus ihm ausschied. Im Lest 44 des 14. Jahrgangs der
„Neuen Zeit" übte er an der neuen Leitung scharfe Kritik, auf die Conrad
Schmidt im Lest 48 des gleichen Jahrganges geantwortet hat.

Die Kontroverse drehte sich im wesentlichen darum, inwiefern die „Freie
Volksbühne" auch demjenigen Teil ihres alten Programms treu geblieben
sei, gemäß dem sie bedeutenden dramatischen Schöpfungen der Gegenwart,
denen sich um ihres kritischeil Inhalts willen die öffentlich subventionierten
und die der Spekulation dienenden Theater spröde oder absolut ablehnend
gegenüberstellen, eine Stätte darbieten sollte. Nun gibt es aber für die
Trefflichkeit eines Bühnenwerks keinen unbedingt objektiven Maßstab, jedes
Werturteil schließt subjektive Momente ein. Die geübteste Einzelpersönlichkeit
ist so wenig vor Mißgriffen geschützt, wie ein Ausschuß von Personen
mit ungleichmäßiger Ausbildung. Eine Versuchsbühne für dramatische
Anfänger, ein Abladeplatz für mangelhafte dramatische Produkte, bei denen
die Tendenz die Unvollkommenheiten decken soll, sollte die „Freie Volks-
bühne" nicht sein, sie sollte Gutes bieten, und da vieles geschrieben wird,
was der Grenze des Guten nahekommt, ohne sie zu erreichen, aber nur
wenig, was sie erreicht, ist es nicht lediglich vom Willen und Können der
Leitung abhängig, wieviel neues sie zur Aufführung bringt. Indes konnte
Schmidt nachweisen, daß unter der neuen Leitung verhältnismäßig mehr
neue und sozialkritische Dramen zur Aufführung gebracht worden waren,
als in gleicher Zeitspanne unter der früheren Leitung.

Dem Bestreben, dem neuen Guten die Bahn zu brechen, ist die „Freie
Volksbühne" treu geblieben, die Liste besserer Schöpfungen, denen sie zuerst
den Bühnenwcg geöffnet hat, ist eine sehr große. Aber in der Verwirk-
lichung dieses Strebens wurden ihr viele Schwierigkeiten in den Weg
gelegt, sowohl von den Direktionen der bürgerlichen Theater, wie von den
Schriftstellern selbst. Von den ersteren stoßen sich viele daran, Stücke als
Neuheiten zu erwerben, die schon auf der „Freien Volksbühne" gespielt
wurden, und von den letzteren gehen daher diejenigen, die schon einen ge-
wissen Namen haben, mit ihren Neuschöpfungen an der „Freien Volks-
bühne" vorbei. Auf die Neuheitenjagd kann sich diese nicht verlegen.