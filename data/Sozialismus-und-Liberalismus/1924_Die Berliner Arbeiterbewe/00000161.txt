﻿151

Posten des Vizepräsidenten reklamieren und den Gegnern die Verweigerung
nicht dadurch erleichtern, daß sie die Beteiligung des Vizepräsidenten an
dem offiziellen Besuch des Präsidiums beim Kaiser von vornherein strikte
ablehne. Es wurde von den Versammelten der Vorschlag sehr abfällig
kritisiert, aber von einer Beschlußfassung hinsichtlich seiner abgesehen.

Anders in einem Teil der Parteiversammlungen der einzelnen Wahl-
kreise, die am 25. August behufs Stellungnahme zum Parteitag und Wahl
der Delegierten stattfanden. Mit der einzigen Ausnahme von Berlin I
nahmen alle Berliner Wahlkreise in mehr oder weniger scharf formulierten
Beschlüssen zu der Anregung Stellung, und zwar durchgängig ablehnend.
Einige Kreise (Berlin II, III und VI) verlangten zwar, daß die Fraktion
den Vizepräsidenten reklamiere, zugleich aber, daß sie jede Bedingung, „zu
Los zu gehen", ablehne. Berlin IV nahm außerdem einen Antrag an den
Kongreß an, daß dieser die Art, wie Bernstein die Frage in der Öffentlichkeit
erörtert habe, für eine Taktlosigkeit erklären und „ihm und seinen Mit-
schuldigen" scharfe Mißbilligung ausdrücken solle. In der Resolution von
Berlin II und Berlin VI wird die Überzeugung ausgesprochen, daß
„die Anregung der ganzen Frage aus einer maßlosen Überschätzung des
Parlamentarismus entstanden ist, daß die Frage wohl für bürgerliche
Kreise ein so großes Interesse haben könnte, aber nicht für die sozial-
demokratische Partei".

Eine zweite Frage, die in all den bezeichneten Versammlungen zur
Sprache gebracht wurde, war die der Mitarbeiterschaft von Sozialdemokraten
an bürgerlichen Blättern, worüber der Parteivorstand Thesen ausgearbeitet
und zur Diskussion gestellt hatte. Lierzu nahmen indes nur der zweite und
vierte Wahlkreis in Resolutionen Stellung. Im zweiten Wahlkreis ge-
langte mit geringer Mehrheit ein Antrag des wegen Mitarbeit an der
„Zukunft" scharf angegriffenen Georg Bernhard zur Annahme, der die
Frage durch die Sätze des Parteivorstandes nicht für erschöpft erklärte, es
vielmehr den örtlichen Organisationen vorbehalten wissen wollte, je nach
Lage des Falles zu entscheiden. Eine Minderheit von Parteimitgliedern
dieses Wahlkreises — Paul Scholz und 194 Genossen — sprach dagegen
in einer Erklärung ihre volle Übereinstimmung mit den Sätzen des Partei-
vorstandes und die Erwartung aus, daß der Parteitag sie zu den seinen
machen werde. Im vierten Wahlkreise ward in einer Resolution verlangt,

„Stellung zu nehmen gegen diejenigen Genossen, welche als Mit-
arbeiter an gegnerischen politischen und sogenannten unparteiischen
Zeitungen das Ansehen der Partei schädigen und fortgesetzt der
Propaganda unserer Ideen und der Verbreitung unserer Parteizeitungen
schwere Lindernisse in den Weg legen".

In allen Versammlungen, außer in Berlin V, wurden außerdem an
jenem Abend Resolutionen gefaßt, die eine andere Neuregelung der Ver-
tretung der Reichstagsfraktion auf den Parteitagen verlangten. Nach dem
bestehenden Statut hatten die Reichstagsabgeordneten schon auf Grund
ihres Reichstagsmandats Sitz und, außer in Abstimmungen über die
parlamentarische Tätigkeit, auch Stimme auf den Parteitagen, und dies
wünschte man dahin abgeändert, daß die Fraktion nur durch eine Delegation
von begrenzter Zahl vertreten sein solle.