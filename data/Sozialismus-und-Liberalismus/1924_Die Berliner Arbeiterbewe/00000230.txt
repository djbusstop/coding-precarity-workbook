﻿217

R. Koblenzer .	. . 33. Bezirk	Tolksdorf . . .	. 15. Bezirk
W. Pfannkuch .	. . 48.	„	Jul. Wernau	. 2. „
Gottfr. Schulz	. - 12. „	Fritz Wilke . .	. 21. „
Paul Singer. .	. . 13.	„	Em. Wurm . .	. 35.	„
Art. Stadthagen	. - 14.	„	Fritz Zubeil . .	. 10. „

Das Mandat von Th. Glocke wurde wegen eines Formfehlers an-
gefochten, aber erst am 3. Oktober 1901 für ungültig erklärt, so daß die
Ersatzwahlmit den für diesesIahr erforderlichen Ergänzungswahlen zusammen-
fiel. Bei diesen ward der Sitz mit großer Mehrheit — 2302 gegen 1552
Stimmen — aufs neue erobert, und ebenso behauptete die Sozialdemokratie
in allen Bezirken, wo für ihre Vertreter Ergänzungswahlen stattzufinden
hatten — neben dem 44. der 10., 12., 23., 35., 36. und 48. Bezirk —, mit
großen, meist geradezu erdrückenden Mehrheiten ihre Positionen. Außerdem
aber eroberte sie den letzten der neuen und fünf der alten Bezirke, nämlich
den 45., sowie den 9., 11., 22., 32. und 34. Bezirk, so daß die Fraktion
von 22 auf 28 Mitglieder anwuchs.

Die neugewählten sozialdemokratischen Stadtverordneten waren: Emil
Basner (23. Bezirk), Dr. Alfred Bernstein (9. Bezirk), Dr. R. Friede-
berg (22. Bezirk), Karl Liebknecht (32. und 45. Bezirk), Th- Metzner
(11. Bezirk), Lerm. Ramlow (34. Bezirk) und Dr. Weyl (36. Bezirk),
letzterer an Stelle von R. Lerzfeldt, der nicht wieder kandidierte. Wieder-
gewählt waren Glocke, Pfannkuch, Schulz, Wurm und Zubeil.
Karl Liebknecht, der in zwei Bezirken gewählt war, nahm für den 45. Bezirk
an, und bei der im 32. Bezirk am 11. Dezember erfolgenden Nachwahl
wurde an seiner Stelle der Redakteur des „Vorwärts" Karl Leid gewählt.
Eine weitere Nachwahl ward dann im 11. Bezirk notwendig, als am
20. August 1902 Theodor Metzner verschied. Sie fand am 5. November
1902 statt und ergab die Wahl des Sozialdemokraten Emil Voigt mit
1552 gegen 767 Stimmen.

Auch die Ergänzungswahlen des Jahres 1903 brachten eine Vermehrung
der sozialdemokratischen Mandate. Die Neugewählten waren: Otto Antrick
(8. Bezirk), Franz Kotzke (28. Bezirk), Paul Schneider (24. Bezirk) und
Lermann Schubert (31. Bezirk). Ein fünftes Mandat, das damals erobert
war, die Wahl Em. Kerfins, mit 788 gegen 743 Stimmen (17. Bezirk),
ward später auf unbedeutende Formfehler hin für ungültig erklärt, und als
im November 1905 Nachwahl stattfand, ging es, trotz erhöhter sozialdemo-
kratischer Stimmenzahl, wieder an die Gegner verloren. Im 5. Bezirk war der
sozialdemokratische Kandidat Lerm. Werner in Stichwahl gekommen,
unterlag aber in dieser mit 1197 gegen 1587 Stimmen. Die sozialdemokratischen
Stadtverordneten, deren Mandate zu erneuern waren — Augustin,
Borgmann, Bruns, Ewald, Gründel, Singer, Stadthagen —
wurden sämtlich wiedergewählt, die meisten davon nahezu einstimmig. So
erhielten Stimmen:

47. Bezirk Augustin	2100	gegen	2 gegnerische Stimmen.
25.	„ Bruns	3437	„	31	„	„
43.	„ Ewald	3043	„	87	„	„
13.	„ Singer	2901	„	62 „ ,,
14.	„ Stadthagen 3005		tt	91