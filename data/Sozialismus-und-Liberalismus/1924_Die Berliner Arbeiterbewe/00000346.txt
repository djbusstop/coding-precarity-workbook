﻿332

schuldigen Arbeitern als eine unerhörte Provokation der Berliner Arbeiter-
schaft bezeichnet, mit der der Bierring andere Zwecke verfolge, als er vor-
gebe. Der Ring habe die Stirn, sich auf Notwehr zu berufen. Aber
„war es Notwehr, daß gerade die ältesten Arbeiter entlassen wurden, die
zehn und mehr Jahre in einem Brauereibetriebe gegen elenden Lohn ihre
Gesundheit geopfert haben, um die in der deutschen Industrie einzig da-
stehenden Dividenden der Roesicke und Konsorten zu verdienen?" Bloß
aus Übermut, bloß aus Laß gegen die Organisationen seien 400 bis
500 Arbeiter außer Arbeit und Verdienst gesetzt. „Keiner von ihnen hat
am 1. Mai gefeiert, keiner durch irgendeine Forderung Anlaß zur Ent-
lassung gegeben. Nur kapitalistischer Übermut und frivole Brutalität
der Brauereiprotzen verlangte diese Opferung! Gegen 500 Brauereiarbeiter
und 300 Böttcher, 800 fleißige Männer, darunter sehr viele Familienväter,
liegen auf der Straße." Wie haben wir, wird gefragt, diese unerhörte
Provokation zu beantworten? And die Antwort lautet: „Ünzweifelhaft mit
der Waffe, die die Brauercibesitzer am schwersten trifft, mit dem Boykott."

Es könne bloß die Frage sein, wird weiter ausgeführt, ob der Boykott
ein allgemeiner sein solle oder ob es besser sei, ihn auf eine bestimmte
Anzahl Brauereien zu beschränken. And da ein allgemeiner Boykott die
Brauereien wohl schädigen, aber bei dem in den heißen Monaten steigenden
Bierbedarf nicht zum Nachgeben zwingen werde, empfehle es sich, von ihm
abzusehen und die ganze Wucht des Angriffs auf eine Anzahl Brauereien
zu richten. Boykottiertes Bier werde um so leichter entbehrt werden, wenn
anderes zur Verfügung stehe. „Ein allgemeiner Boykott würde den Bier-
ring stärken, ein partieller sprengt ihn."

Es werden nun folgende Brauereien aufgezählt, von deren Bier
fortan kein Tropfen getrunken werden solle: Schultheiß' Brauerei, Brauerei
F. Lappoldt, Böhmisches Brauhaus, Adlerbrauerei (K. Gregory), Vereins-
brauerei Rixdorf, Schönebcrger Schloßbrauerei und die Spandauer Berg-
brauerei. Überall, in Laus und Werkstatt, im geselligen Verkehr und auf
Ausflügen sollten die Arbeiter für die strenge Durchführung des Boykotts
sorgen. Nur so würden sie die Solidarität mit den durch den Anternehmer-
hochmut brotlos gewordenen Arbeitern bekräftigen, ihrer Ehrenpflicht gegen
die Partei nachkommen. Die Welt müsse wissen, „daß die Einigkeit der
Arbeiter stärker ist, als der festeste Ring hochmütiger Kapitalisten. Loch
die Solidarität der Arbeiter! Nieder mit dem Bierring!"

So schließt der Aufruf. Er macht dann bekannt, daß mit der Durch-
führung des Boykotts bis auf weiteres I. Auer, R. Millarg,
L. Mattutat, L. Gumpel und Paul Lilpert beauftragt seien, und
daß am folgenden Abend, Freitag, den 18. Mai, neun öffentliche Ver-
sammlungen zur Besprechung dieser Angelegenheit in den verschiedenen
Stadtteilen Berlins stattsinden würden. 57 der bekanntesteit Vertreter der
Partei und der Gewerkschaftsbewegung Berlins stehen als Anterzeichner
unter ihm. Lier die Namen:

I. Auer. R. Augustin. I. Bamberger. A. Bebel. K. Berger.
N. Beyer. W. Börner. L. Bolze. G. Bosse. L. Dornbusch. Karl
Dost. L. Faber. Nich. Fischer. B. Franke. St. Fritz. I. Gandorfer.
W. Gesche. C. Gruschke. L. Gumpel. R. Lalfter. K. Lergt.
K. Lelbig. P. Lilpert. F. Loch. E. Jost. F. Kitzing. L. Knüpfer.