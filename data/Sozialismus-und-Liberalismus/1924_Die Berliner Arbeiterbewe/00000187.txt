﻿176

schöpfende Beratung des Zolltarifs in seinen Einzelheiten gewaltsam ver-
hindert und das Recht der Minderheit mit Füßen getreten worden sei.
„Die Arbeiterschaft Berlins", heißt es in der Resolution, „Protestiert
entrüstet gegen diesen parlamentarischen Staatsstreich der Mehrheit, gegen
die Vergewaltigungen und Beschimpfungen und spricht den Vertretern der
Sozialdemokratie, als den wahren Volksvertretern, ihren Dank und ihre
Bewunderung aus fiir die unermüdliche Tapferkeit, mit der sie die Inter-
essen des Proletariats gegen die Anschläge der Junker, Pfaffen und Scharf-
macher verteidigt haben." Nachdem der Zolltarif in der Nacht vom 13. zum
14. Dezember durchgejagt war, vertagte sich der Reichstag, so daß eine
unmittelbar darauf veranstaltete Antwort der Berliner Arbeiterschaft in seiner
Abwesenheit erfolgt wäre. Kaum jedoch, daß der Reichstag wieder zusammen-
getreten war, wurde in 32 Volksversammlungen in verschärfter Form gegen
die verfassungswidrige Art der Fertigstellung dieses Gesetzes protestiert und
die Wählerschaft aufgefordert, den Protest dadurch wirksam zu machen, daß
sie bei der bevorstehenden Reichstagswahl die Sozialdemokratie nach Mög-
lichkeit in ihrem Kampf unterstütze.

Zum großen westfälischen Bergarbeiterstreik von 1905 nahmen am
24. Januar 1905 die Berliner Arbeiter in 28 Versammlungen Stellung,
indem sie den Streikenden ihre Sympathie ausdrückten, ihnen materielle
Lilfe in Aussicht stellten, die dann auch in reichlichem Amfange von Berlin
geleistet wurde, ein Reichsberggesetz verlangten, das den Arbeiter in bezug
auf Löhnung, Arbeitszeit, Gesundheitsschutz usw. sicherstellen müsse, und
die Forderung der Verstaatlichung der Bergwerke wiederholten.

Am 8. August 1905 fanden wiederum 26 Volksversammlungen in
Sachen der Fleischvcrteuerung statt, die insbesondere die Aufhebung
der Grenzsperre verlangten. Am 3. November protestierten die Tabak-
arbeiter Berlins gegen die in der famosen Reichsfinanzreform des
Staatssekretärs v. Stengel vorgeschlagene Erhöhung der Tabaksteuer.
Am 21. November demonstrierte eine große Versammlung von Gastwirts-
gehilfen für Ausdehnung des Arbeiterschutzes im Gastwirts-
gewerbe, und am 17. Dezember protestierten die Brauereiarbeiter
gegen die in der Stengelschen Vorlage gleichfalls verlangte Erhöhung
der Brausteuer.

Damit ist die Reihe der wichtigsten Demonstrationen, die entweder
ausschließlich oder doch in erster Linie an die Adresse der Gesetzgeber
gerichtet waren, erschöpft. Es kann selbstverständlich hier nicht ihre mittel-
bare oder unmittelbare Wirkung auf die Gesetzgebung untersucht werden. Wer
sich aber mit der Geschichte der Gesehgebungsmaterien beschäftigt, auf die
sie sich bezogen, der wird sich überzeugen, daß zwischen den Demonstrationen
und dem Gang der Gesetzgebung ein viel engerer Zusammenhang besteht,
als es mancher sich vorstellen mag. Gewiß wird eine einzelne Oppositions-
partei nur ganz ausnahmsweise durch Versammlungen die Geschgebungs-
Mehrheit direkt und sofort zu Maßnahmen in ihrem Sinne bewegen. In
der Regel ist schon viel erreicht, wenn es ihr gelingt, eine öffentliche
Meinung zu schaffen oder zu verstärken, deren Einfluß sich auch die
Mehrheitsparteien und die auf sie gestützten Gesetzgeber nicht entziehen
können. Doch sind immerhin eine ganze Anzahl von Gesetzesentwürfen,
gegen welche die Berliner Versammlungen Protestresolutionen faßten