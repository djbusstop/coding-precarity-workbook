﻿221

Es wäre also hier das Jahr 1905 mit dem Jahr 1899 zu vergleichen. Im
letzteren Jahre fanden aber, wie an betreffender Stelle bemerkt, außer den
Ergänzungswahlen in den älteren Bezirken, deren Mandat erledigt war,
Wahlen in allen neugebildeten Bezirken, zusammen 21 Wahlen statt, während
in späteren Jahren immer nur 16 Bezirke Ergänzungswahl hatten. Es müssen
daher, um das Verhältnis von 1905 zu 1899 richtig zu erkennen, nur die
16 Bezirke verglichen werden, in denen beide Male Wahlen erfolgten. Für
sie ergibt sich nun folgende Gegenüberstellung:

1899	Sozialdemokraten 18 599, Gegner 15 149

1905_____________________ 30	681,	„	10	573

Sozialdemokraten + 12 082, Gegner — 4 576

Einem Rückgang der gegnerischen Stimmen um 4576 steht eine Zu-
nähme der sozialdemokratischen Stimmen um 12 082 gegenüber. Von 55,2
gegen 44,8 ist nur allein in diesen sechs Jahren das Verhältnis der sozial-
demokratischen Mehrheit auf 74,3 gegen 25,7 vom Hundert gestiegen. Ein
glänzender Abschluß. And je weiter wir den Vergleich nach rückwärts fort-
setzten, um so Heller würde sich der Fortschritt der Sozialdemokratie
hervorheben.

Wie viel Arbeit bergen aber auch diese Zahlen! Immer mehr Eifer
wurde darauf verwendet, vor der Wahl keinen Wähler ohne Flugblatt und
während der Wahl keinen säumigen Wähler ohne Mahnung an seine
Wahlpflicht zu lassen. Daher wächst auch von Wahl zu Wahl die Zahl
der Wahlarbeit Verrichtenden, steht ein immer größerer Prozentsatz von
Organisierten und Organisatoren hinter den Wählern der Sozialdemokratie.
Nur so war es zu erreichen, daß ungeachtet der großen Angunst des Wahl-
systems am Ausgang unserer Epoche die sozialdemokratische Fraktion der
Berliner Stadtverordnetenversammlung von 48 Mandaten der dritten Wähler-
klasse nahezu drei Viertel, nämlich 35, innehatte. Das ist ein günstigeres
Verhältnis, als es dem Anteil der Partei an den bei Reichstagswahlen
in Berlin abgegebenen Stimmen entspricht, der im Jahre 1903 sich auf218238
von 326780, also gerade auf zwei Drittel belief, und fast genau so war das
Verhältnis im Jahre 1907 : 251215 von insgesamt 379373 Stimmen. Der
Anterschied rührt natürlich daher, daß bei der Reichstagswahl alle Wähler
ohne Anterschied des Einkommens in einer Klasse, bei der Stadtverordneten-
wahl aber die Wohlhabenden und Reichen in gesonderten Klassen wählen.
Das schafft der Sozialdemokratie in der dritten Klasse notwendigerweise eine
günstigere Position. Wie wenig diese aber als Ausgleich für die Angerechtigkeit
der Klassenwahl betrachtet werden kann, zeigt die Verteilung der Sümmen bei
der Reichstagswahl an. Würde die Stadtvertretung nach dem Reichstagswahl-
recht, das ja auch noch nicht völlig demokratisch ist, und auf Grund der
zahlengerechten Verteilung der Mandate gewählt, so müßte die Sozial-
demokratie statt 35 nicht weniger als 96 Sitze im Rathaus von Berlin
innehaben. Selbst wenn ihr alle Mandate der dritten Abteilung zugefallen
wären — was aber bei der eigentümlichen Gliederung einiger Wahlbezirke
nicht zu erwarten war —, wäre sie immer noch um 100 Prozent der
erlangten Mandate untervertreten, das heißt: erst im Besitz der Hälfte
der Mandate gewesen, die ihr nach demokratischem Recht hätten zufallen
müssen.