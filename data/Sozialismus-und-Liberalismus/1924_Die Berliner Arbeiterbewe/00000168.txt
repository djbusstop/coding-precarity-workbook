﻿Schütz« de» genzen Nachwuchses ottftrea Volke» auftuforöem. Aber wo nüt|cn
die vaeerdrück'm grauen sich Ira öffettllidjen Leben frei rühren « Dar gesetzliche
Verbot der Mitgliedschaft van Frauen in politischen Vereinen besieht
ln Anhalt, Bayern, Draunschweig, Lippe-Detmold, Preußen und Reuß j. L. In
ven nämlichen Staaten, weiter in den beiden Mecklenburg isi den Frauen auch
die Theilnahme an den Vereinssitzungen und -Versaiiimlungc»» untersagt.
Mit anderen Warten: das Kapital darf die Frauen sck/rankeiiloser ausnützen wie
Mals früher, wo die alte Sille und die alle Rechtsordnung die Frauen dagegen
fchützle; aber die Ausgebeuteten dürfen sich zu ihrem Schutze noch genau so wenig
zusammenfinden, wie lemalS früher

Wird ferner die Heranwachsende Generation nicht bereits m viel
lüngeren Jahren gezwungen. sich ielbsisländig durch die Well zu schlagen? Bedarf
sie deshalb der Theilnahme an de» schützenden Organisationen der Arbeiler nicht
viel dringender wie vordem? Erjorder, zudem die IpSIeie Betheiligung der jungen
L-uie an Kassen-, an GeioerbegenchtS.. LandlagS- und ReichSlagSwahlen nicht eine
längere Vorschule, wie sie nur Vereine und Bersniumlungen bieten? Ansialt die
Gesetze den veränderten Verhältnissen besser anzupassen, will man heule die
Rechte der Minderjährigen noch verkürzen>

Die B-chür>-en machen natürlich von ihren Befugnissen sehr verschieden Ge.
brauch. Aber wo» bleibt nach alledem von der nach 1848 durch die Verfassungen
und durch feierlich, Erklärungen verbürgten „Assoziativ,iSsreihcit" noch übrig? Für
die Bevorrechteten freilich meist noch alle«, für dt« Arbeiter sehr wenig
oder wich garnicht».

Doch die Zeit der AnSnahmeges-hgebnng gegen die Arbeit hat
unsere Kapitalisten derart verwöhnt, daß sie i» ihrer Gier nach ungestörter Herr-
Ichaft und nach ungemessenem Reichthum noch wettere Fesseln für die Arbeiter
verlange«. Herr v. Stumm — König StUUlM, wie man ihn mit Recht
nenm — hat hier die Führung in dem UnterdrückungSjeldzug übernommen.
Schoo Anfang 1399 erklärte er im Reichstage:

^)ch erkläre mich vollkommen bereit, im preußifchen Landtag
mitzuwirken, daß das Vereins- und Versammlung-recht einer Revision
unterzogen wird. Wie brauchen beispiel-wrife in Preußen nur eine
ähnlich« Bestimmung einzuführen, wie sie in Bayern besieht, wo es in
Artikel l» heißt: Jede Polizeibehörde ist befugt. Vereine zu schlichen,
wenn dieselben di« religiösen, sittlichen, gesellschaftlichen Grundlagen de»
Staate- zu untergraben suchen.'

Dieser Wunsch des bchioßherrn von Neunkirchrn ist bann tn der Borlage
ves preußischen Ministers des Innern v d. Recke auch tn Erfüllung gegangen.
Wo- der Minister ursprünglich alle» nach geplant Hai, weiß man nicht genau;
sicher WJ nur. daß eO derartig war. daß selbst die sreskonservaliven Schlot, und
Lmdsunker erklärten, sie könnten nicht mitthun. Wo» schließlich Herr o. b. Recke
doch noch verlangte, war hinreichend, alle Arbeiter-Bereis» nrrd »Der»
fnwmlnag-n von« Gntdünkvv des Behörden abhängig zu machen,
vom Gutdünken der politische« Polizei, deren wahrer Charakter nicht erst
durch den Prozrtz Tausch enthüllt za werden braucht» — Vereine sollten
geschloffen werden kömim. wen» sie bla .öffentliche Sicherheit" ober
bea .öffentliche» Frieden" gefährde». Unter denselben Borauksetzungen
w^ren Versautwlnng-n «-.nfznlöseuk Ueber welche» Streben der Arbeiter
haben dir Wortführer lies Kapital» nicht schon gezetert: es „gefährde den
Frieden" unter den einzelnen DefellschafUklasfen! Die Ruhs deck voll«»
Ocldsaikck von Zugrsräuduiffau an die varbeoda Arbeit, boO war für
Wsjö ßfule tauen gleichbedeutend mit dem öffentlichen Frieden. Jeder, d-r die
Keichm an Ihre fozialen Pflicht,» mahnte, war von je ein Wühler und
Feiev-uSstörer. Was für den einzelnen Menschen baS Gcrvifl«, ist. do-
rt»» Snt-ii ttotbt r-nd über dal, Schlecht sich empört, das ist für die Herrschenden
horte bis <Zoziald«mski.orie, und diese Stimme deck Eietoiffesg wollen
di« Reichen «ersticke», weil sie dieselbe fürchtsni

D« prrnHtsche Vorschlag ging iu seiner Am sogar noch über boS !
Gozialistsnglistch hirrauv. DleseS gestattete zwar orich VetetnSauflösungen
ds Lvstrebungm, welch, den öffentlichen Friede«, gefährden, aber nur mit der
ÖtnjufäguEß, daß diese Bestrebungen «ms dm .Umsturz" brr bestehenden LtaatS-
üJib S«stÜfchastöor>i2aug biujklcn müßten.

Die Freikorrfervotivro Im Wgeorduetcnhause haben dann gar noch «Le
ü<.'S«NÄne Redewendung ausgegeben und offen die Feindseligkeit gegen AllrÜ.
■sjh9 ehrlich arbeitet, zum Ausdruck gebracht. Si, wendeten sich tn ihrem
llrckmg aLSschlteßllch gegen .Lozialdcmoiralm. Sqlaliünr und Kommunist^",
dis sie fü: polirtsch vogelfvei und geächtet erklärten

Das Adgcordnktenhaus hat zwar das Anlinnen juruckgewielm. jedoch hatten
die Kanservaltven Ihre Partie noch nicht für verloren. Die Kommission des
Herrenhauses besürworicl die Herstellung des allen Sozialistengesetzes. beschränk
auf Anarchisten und Sozialdemokraten. Der verfolg» Plan Ist nur zu durch,
sichtig. Die im Antrag tm Abgeordnelcnhause mit einbezogmen Sozialisten
und Kommumsien sind im Antrag der Kommission de» Herrenhauses gestriirn.
um dir sozialen Richtungen christlicher und bürgerlicher Observanz zu beruhigen,
dagegen soll die Verbindung der Anarchisten and Sozialdemokraten die Rich'.rgkeU
der vagen Behauptung erweisen, di, Sozialdemokrati- arbekle am Umsturz se,
bestehenden Staat», und Gesellschaftsordnung. Ohne Zweifel felgt das Herren-
haus den Spuren seiner Kommission. Die Abwendung der reaktionären -Leiabi
liegt in den Händen der Nationalliberalen, denselben ManneSmuth zuzutrauen,
wäre thörichte Hoffnung.

Arbeiter und Kleinbürger! Wißt Ihr noch au» btt. zwölf Jahren vor
Bonrgeois-Schrcrrenöhcrrschast unter dem So-.talisiengcjetz, wo? wlüe
Vollmachten an die Behörden bedeuten? Als Ihr camatS crp politisch »»»!>.
todt gemacht wart, wißt Ihr noch, tn welch schamloser SWie tne Kartellparreier.
sich durch Zölle und Liebe-gaben bereicherten, wahrend Euch immer «ehr
LebenSmittelzölle mid Verbrauchssteuern aufgehalst wurden? M« man Esch
geknebelt hatte, erinnert Ihr Euch noch, wie man durch die Verlängerung oer
Wahlperioden Eure kargen politischen Rechte noch weiter hefchsirr?
Fichr nicht die Polizei mit rauhn Faust jedesmal dazwischen, wenn Ihr die
Erhöhung Eurer Ausgaben, wie sie durch Zölle und neue Steuern eingetreten
war. jum Theil wenigsten- wieder «mSgleichen wolltet durch eine Aufbesserung
Eurer Löhne?

Dasselbe wollen die tn» Dtumm jetzt vor» Renew erreichet»,
und da man im Reichstage kein neues Kr.ebelgefetz gegen die Arbeiter z:> Standr
brmgen sann. so wenden sich die „Reichösreuude", die Gegner de- „Parti-
knlariSwnö", an die Dunkelkammern der Siiizelstaaten und wühle» hier nr
lichtscheuer Beschäsiigkett gegen die letzten Bollwerke der Freiheit. Da« preußisch«
HerrcnhauS. jahrzehntelang selbst von Nationalliberalen al» alte Ruine ver>
höhnt und oerspollet, schickt sich an. dle politische Flihr,l»g drS arisgckläneu
Deutschland zu übernehmen! Wie h-rabg«?omMeo sind doch unsere herrschenden
Parteien I

Aber toa» beim Hinblick auf solche Zustände so rtej beschämend wirkt, gieb»
e» auf der anderen Seite für dle Arbeiter nicht Anlaß, aus die tm Reiche mit
tmtltnb Opfern errungene politische Stellung stolz zu sein? Wo das al!ge«
ßtcitrc Wahirechl seinen Einfluß gellend macht, da wagt man heule de»
Lolk-verrretung kein, ähnlichen reaktionären Zumutungen mehr zu machen. Wo
die Vertreter der Arbeiter in größer» Zoh.' sitzen, da gehl die Rraktts»
dem Kampse feig au» dem Weg; nur kl der Smupslust der Klasse«' »>.nd
Lensu-wahlen, da fühlt sie sich ihrer Ernte noch sicher.

Doch gerade darum hasst die Reaktion da« allgemein« Wahlrecht 1»
Reiche. Sie wird die Art an diese» Recht legen, sowie ihr bi, Zeit dazu günstig
scheint. Sie lauert nur ans einen Vorwand; sie hofft auf Mngstwahlr«
wie 1873 oder !L37. und wenn e» damit nicht geht, auf einen Staatsstreich,
auf dl» Umwälzung, auf dir Revolution von oben.

Mit dem Vereins- und VerfaueminngÄrrcht tv Prrerßen fäsgt
man an, mit den, Wahlrecht tm Reiche fehl man die Grdroffelang
aller poiiiikchco Rechte deS Drbrittt« »red deS kloiae» Marrue» frrt.

Darum darf di« Erregung, welch« der peeufiisch« Derrlnsgesetzentmurs tm
ganzen Reiche hervorrief, nicht erlöschen, wenn da« Gesetz erledigi sein wird. Sie
muß zum Sturme angesachl werden und alle Schwachen und Unterdrückien antreibe-,
zu doppelter Thätigkeit für di« Partei der Arbeiter,

für di, So;ia!S,mokratir,
für das nLgeWeine Wahlrecht.

Dst heul« durch dar Volk gehende Erregung muß bei den Nächsten
Wähle»! aufflammen zu einem vernichtenden Strafgericht gegen alle Paririen,
dle setzt sich an der Kuoörlung der Arbeit betheiligeo. Polizeiminlsirr v. b. Recke
sprach ta preußischen Landtage von der „Abrechnn,eg" der Wählt: mit lhrev
Mgeoidneten Nun wohl, wir fordern auch zu dieser Abrcchrmug aus. aber
sie soll ganz ander« au-fallen. al» sie sich die Urheber de: letzten reaktionäre»
Pläne gedacht haken. Sie soll so unzweideutig sei», daß allen derartigen Arbeiter,
feinden die Lust zu femeren .T-nten" vergeht.

Bereitet die nächsten Wahlen vor! Sorgt für eine

WopliiRU kr foplkmolitötif^n ÄinmenM!

Das fei Eure Nnfivllrf anf das Attentat gegen die Bolkssreiheit,
daS dann das letzte in Deutschland gewesen fein wird.

EL Sr»l» tx ean»iirc.®ia»WUtL — Druck: Caatwser vuchdruckrret tnb t3«ü:a8a»BaU «utt » So. in «laaiutj.

75 und 76. Agitationsflugblalt zum Kampf für das bedrohte Vereins- und

Versammlungsrecht