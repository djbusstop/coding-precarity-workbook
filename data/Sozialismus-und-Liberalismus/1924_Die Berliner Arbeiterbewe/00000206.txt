﻿193

Augen der Polizei in so hohem Grade, daß all diese Blätter polizeilich
beschlagnahmt wurden. Noch einmal, im Jahre 1896, ward eine März-
zeitung der Sozialdemokratie Berlins konfisziert, aber der Polizei fiel nur
die zweite Auflage dieser Festnummer in die Lände, die erste war, als die
Beamten sie beschlagnehmen wollten, schon vollständig vergriffen. Eine ge-
wisse geschäftliche Schädigung war alles, was die Polizeimaßregel erzielte.

Wie der Besuch der Gräber, so stieg auch lange Zeit mit der Zahl
der Versammlungen und Feste, die auf den Abend des 18. März angesetzt
wurden, der Besuch dieser Veranstaltungen. Jeder Wahlkreis hatte
mindestens eine, die größeren Wahlkreise aber hatten mehrere Versamm-
lungen oder Feste, die oft sehr eindrucksvoll verliefen. Einen Löhepunkt er-
reichte die Gedenkfeier am 18. März 1898, dem 50. Jahrestag der Re-
volution. Dagegen wurde in den beiden letzten Jahren unserer Epoche von
besonderen Versammlungen zur Würdigung des Revolutionstages ab-
gesehen, da um die gleiche Zeit große Demonstrationen für unmittelbare
Kämpfe die Arbeiterschaft in Anspruch nahmen, so daß es Überspannung
der Kräfte geheißen hätte, zu schnell darauf neue Versammlungen in großer
Zahl an einem Tage folgen zu lassen. Aber hinsichtlich des Besuchs der
Gräber blieb es beim alten, gerade in den letzten beiden Jahren waltete
die Polizeischere besonders eifrig an den Kranzschleifen.

Es bliebe nun noch übrig, die Entwicklung der dritten Gruppe regel-
mäßig wiederkehrender Demonstrationen zu gedenken, nämlich der Begehung
des internationalen Festtages der Arbeiterklasse, der Feier des

1.	Mai. Ihr aber, die nach und nach die andern regelmäßigen Demon-
strationen an Amfang und Bedeutung überflügelt hat, sei ein besonderes
Kapitel gewidmet.

Bernstein, Berliner Geschichte. III.

lZ