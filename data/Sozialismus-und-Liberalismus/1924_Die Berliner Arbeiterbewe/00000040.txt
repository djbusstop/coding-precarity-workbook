﻿Bezirks- und Gemeindesteuern in Anrechnung zu bringen, was natürlich
die Steuersummen der Wohlhabenden sehr viel stärker ins Gewicht fallen
ließ als vordem. Ja, es ward sogar bestimmt, daß dort, wo keine direkten
Gemeindesteuern erhoben werden, wie z. B. in den Gutsbezirken, die bloß
veranlagten Steuern in Anrechnung zu bringen seien. Alle Versuche,
das Wahlrecht wirklich zu reformieren, scheiterten damals, wie auch in den
Jahren 1900, 1901, 1902 und 1903 die Anträge der Freisinnigen auf Neu-
einteilung der Wahlkreise und Einführung der geheimen Wahl im Klassen-
landtag glatt zu Boden fielen. Die beiden konservativen Fraktionen erklärten
kühl, daß ihnen Dreiklassenwahl, offene Stimmabgabe und die Ungleichheit
der Wahlkreise gerade paßten, die Nationalliberalen wollten etwas refor-
mieren, aber wußten nicht wo, und das Zentrum fand die Anträge sachlich
berechtigt, aber — nicht zeitgemäß. Es mußte erst stärker angeklopft
werden, bis die von der Reaktion des Jahres 1849 aufgetürmte Zwing-
burg ins Wanken kam.

Noch vor der Landtagswahl, am 6. September 1898, hatte Wilhelm II.
an einer Galatafel im Bad Oeynhausen, an der die Spitzen der Behörden
und die Auswahl der Bourgeoisie Westfalens teilnahmen, die ein Jahr
vorher am Sparenberg (siehe oben) zum besten gegebene Ankündigung
eines Antistreikgesetzes wiederholt. Er erklärte:

„Das Gesetz naht sich seiner Vollendung und wird den Volksvertretern
noch in diesem Jahre zugehen, worin jeder — er möge sein, wer er will,
und heißen, wie er will —, der einen deutschen Arbeiter, der willig ist,
seine Arbeit zu vollführen, daran zu hindern versucht oder gar zu einem
Streik anreizt, mit Zuchthaus bestraft werden soll. Die Strafe habe
Ich damals versprochen und Ich hoffe, daß das Volk in seinen Ver-
tretern zu Mir stehen wird, um unsere nationale Arbeit in dieser Weise,
soweit es möglich ist, zu schützen."

Der Stuttgarter Parteitag der deutschen Sozialdemokratie beantwortete
diese Kaiserrede mit einer Resolution, deren wichtigste Sätze hier ebenfalls
folgen mögen:

„Der Parteitag erklärt es als eine der ersten Aufgaben der Gesetz-
gebung, allen Arbeitern das Koalitionsrecht zu gewähren und es
gegen die unablässigen Anschläge des Unternehmertums sicherzu-
stellen. Er wendet sich daher entschieden gegen die Drohung in der
Tischrede des Kaisers zu Oeynhausen am 6. September 1898, demnächst
der Volksvertretung einen Gesetzentwurf vorlegen zu lassen, wonach jeder
mit Zuchthaus bestraft werden soll, der „gar zu einem Streik anreizt".

Der Parteitag fordert die deutschen Arbeiter ohne Unterschied der
politischen und religiösen Anschauungen auf, sobald dem Reichstag eine
solche oder ähnliche Vorlage zugeht, Protestversammlungen zu ver-
anstalten und die Abgeordneten zur Stellungnahme gegen eine solche
Vorlage zu drängen.

Der Parteitag erblickt in der Kaiserrede das unzweideutige Zeugnis,
daß die in den kaiserlichen Erlassen vom 4. Februar 1890 in Aussicht
gestellte Sozialreform seitens der Negierung aufgegeben ist. Der
nunmehr erst im richtigen Licht erscheinende Erlaß des Grafen Posa-
dowsky zeigt, daß das Reichsamt des Innern statt des früher in Aus-
sicht gestellten „Schutzes gegen eine willkürliche und schrankenlose Aus-
beutung der Arbeitskraft" eine Politik der Unterdrückung der
Arbeiterbestrebungen auf wirtschaftlichem Gebiete einleitet—,