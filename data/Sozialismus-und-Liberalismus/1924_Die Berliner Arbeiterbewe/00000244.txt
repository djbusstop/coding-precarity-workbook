﻿230

gegen welche sich früher der Klassengeist des kleinen und mittleren
Bürgertums fast noch mehr sträubte, als die neufeudalen Tendenzen der
etwas weiter blickenden großen Bourgeoisie.

Dieser Entwicklungsgang, das meist widerwillige, zögernde und in
Halbheiten sich kundgebende Zurückweichen des bürgerlichen Vorurteils und
Klassenegoismus vor den mit zunehmender Kraft andrängenden politischen
und sozialen Ideen der Arbeiterdemokratie wird in klassischer Weise ver-
anschaulicht durch die Denkschrift, die Paul Hirsch unter dem Titel:
„25 Jahre sozialdemokratischer Arbeit in der Gemeinde" zur 25. Wieder-
kehr des Tages geschrieben hat, wo zum erstenmal sozialdemokratische
Vertreter in das Berliner Rathaus einzogen.*) In überaus reicher
Dokumentierung wird da gezeigt, wie auf allen Gebieten der kommunalen
Betätigung, von der Betonung des Selbstverwaltungsrechts und der
sonstigen politischen Rechtsansprüche der Gemeinde angefangen bis zu
den Einzelnheiten der kommunalen Aufgaben in bezug auf Arbeiter-
politik und allgemeine Sozialpolitik, Wohnungspolitik und Verkehrspolitik,
Schulpflege und Gesundheitspflege, Steucrwcsen und Wirtschaftspolitik,
Armenfürsorge und Kunstpflege, die sozialdemokratischen Stadtverordneten
Pionierarbeit verrichtet und in harten, aber unermüdlich geführten Kämpfen
bahnbrechend gewirkt, die Widerstände, wenn nicht bis zur völligen Ohn-
macht gebrochen — davor schützt sie das Klassenwahlrecht — so doch zur
moralischen und immerhin in vielen Punkten bis zu einem gewissen Grade
auch tatsächlichen Kapittllation besiegt haben.

„Auf dem Gebiete der Verkehrspolitik," schreibt Hirsch im Vor-
wort, „ist die Reichshauptstadt, deren Vertreter Jahrzehnte hindurch ihre
Straßen und Plätze privaten Erwerbsgesellschaften zur Ausbeutung über-
lassen haben, im Begriff, endlich die Bahnen zu wandeln, die die Sozial-
demokraten ihnen längst vorgezeichnet hatten, und eigene Linien zu bauen.
Auf dem Gebiete des Volksschulwesens sind erfteuliche Fortschritte zu
verzeichnen, Fortschritte, die in erster Linie den Sozialdemokraten zu danken
sind. Sind sie es doch gewesen, die als erste im Roten Lause den An-
sturm gegen die den Geist und Körper schädigende gewerbliche Neben-
beschäftigung von Schulkindern unternommen haben! Sind sie es
doch gewesen, auf deren Anregung die Anstellung von Schulärzten zurück-
zuführen ist! Oder werfen wir einen Blick auf das wichtige Gebiet der
städtischen Arbeiterpolitik I Die städtischen Körperschaften, die vor
noch gar nicht langer Zeit Maßnahmen zur Bekämpfung der Arbeits-
losigkeit als nicht zu ihrem Pflichtenkreis gehörend rundweg abgelehnt
haben, beraten heute in gemischter Deputation dieses schwierige Problem.
Die Frage der Gewährung eines Ruhegehalts und einer Hinter-
bliebenenversorgung für städtische Arbeiter, eine Frage, die, als sie die
Sozialdemokraten vor 20 Jahren auswarfen, einfach als indiskutabel be-
zeichnet wurde, ist heute — wenn auch noch nicht in zufriedenstellender
Weise — gelöst. In der Steuerpolitik begegnen wir sozialdemokratischen
Spuren, die Einführung der Grundwertsteuer ist das Werk der Sozial-
demokratie, die Wertzuwachssteuer, deren Notwendigkeit wenigstens vom

*) 25 Jahre sozialdemokratischer Arbeit in der Gemeinde. Die Tätigkeit der Sozial-
demokratie in der Berliner Stadtverordnetenversammlung. Auf Grund amtlicher Quellen ge-
schildert von Paul Kirsch. Berlin I9V8, Verlag Buchhandlung Vorwärts. IX, 552 S. gr. 8«.