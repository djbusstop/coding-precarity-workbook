﻿bürg" wertvolle Auskunft, die den nun folgenden Zusammenstellungen^
gründe gelegt ist.

In den größeren Orten des Kreises hat sich die Stimmenzahl
Sozialdemokratie von Lauptwahl zu Lauptwahl wie folgt entwickelt:

	1890	1893	1898	1903	1907
Rixdorf		3 982	6 508	10 063	17 563	29 339
Charlottenburg ....	3 868	6 537	9 808	16119	22187
Schöneberg 		1 653	3 083	5 627	10 662	14 133
Wilmersdorf		367	621	1 338	2 800	4 730
Köpenick		1 568	2 031	2160	2 941	3 891
Groß-Lichterselde . . .	220	559	776	1 792	2 335
Steglitz		631	995	1071	1982	3123
Britz		556	737	720	1 145	1 149
Adlershof		336	568	924	1 230	1381
Neuendorf		186	294	347	556	894
Nowawes		685	1 093	1 124	1 498	1616
Friedenau		153	323	437	923	1 383
Tempelhof		373	524	528	940	1 181
Treptow		130	182	424	894	1 951
Zehlendorf		195	316	399	703	889

Mit Ausnahme der obenanstehenden Ortschaften Charlottenburg,
Rixdorf, Schöneberg und Wilmersdorf, die heute eigene Stadtkreise
sind, werden die im vorstehenden aufgezählten Orte dem Kreise Teltow zu-
gezählt. Der Kreis Beeskow-Storkow umfaßt keinen Ort von Bedeutung,
und nur in sieben ihm zugehörcndcn Ortschaften wurden bisher mehr als
hundert Stimmen für Kandidaten der Sozialdemokratie erzielt. Folgendes
die Entwicklung dieser Orte unter dem Gesichtspunkt der Stimmenzahlen
der Sozialdemokratie. Es wurden für sozialdemokratische Kandidaten
Stimmen abgegeben in

	1890	1893	1898	1903	1907
Beeskow ....	20	69	68	112	130
Ketschendorf . . .	63	80	107	217	264
Markgrafpieske. .	96	80	112	131	126
Niederlehme. . .	14	49	64	143	163
Rauen		43	76	104	132	149
Spreenhagen. . .	46	63	80	88	109
Storkow ....	58	49	51	119	183
Die günstigste Entwicklung		für die Sozialdemokratie			zeigt der

Ort

Ketschendorf, ein Flecken in unmittelbarer Nachbarschaft der Stadt
Fürstenwalde, die selbst zum Wahlkreis Frankfurt a. O.-Lebus gehört.
Ketschendorf ist stark von Industriearbeitern und Schiffern bewohnt, das
ebenfalls nahe bei Fttrstcnwalde gelegene Rauen beherbergt viele Arbeiter,
die in Braunkohlengruben beschäftigt sind. Markgrafpieske und
Spreenhagen zählen viele Bauarbeiter, Niederlehme, das an einem
in die Spree einmündenden See liegt, hat Ziegelei- und Fabrikarbeiter,
Beeskow und Storkow sind kleine Kreisstädte mit ein wenig Industrie.

Interessant ist ein Vergleich der Entwicklung des Kreises Teltow ohne
die ihm nach mehr oder weniger langer Zeit zugerechneten Städte Rixdorf,
Schöneberg und Wilmersdorf mit der Entwicklung des Kreises Bceskow-
Storkow.