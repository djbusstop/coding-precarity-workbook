﻿362

beantragen". Das sind nur kleine Verbesserungen nach so vieler Vorarbeit,
aber immerhin sind es Verbesserungen, und der rastlosen Agitation der Ver-
einigung und ihrer Nachfolgerin ist es in erster Linie zu verdanken, daß
im Jahre 1908 in Berlin der Achtuhrladenschluß den Neunuhrladenschluß
verdrängt hat.

Als in der Wintersession 1895/96 der mittelstandsretterische Entwurf
des Gesetzes wider den unlauteren Wettbewerb mit seinen, die Bewegungs-
freiheit der Landlungsgehilfen bedrohenden Konkurrenzklauseln vor den
Reichstag kam, berief die Vereinigung auf den 10. Januar 1896 eine
Protestversammlung ein, mit Paul Singer als Referenten. Sie füllte
die geräumigen Kellerschen Festsäle bis auf den letzten Platz und nahm
einstimmig eine geharnischte Resolution gegen den von Singer unter stür-
mischer Zustimmung gebrandmarkten Versuch an, durch die betressenden
Paragraphen des Gesetzes (§§ 9 und 10) die Landlungsgehilfen mittels
unerhört hoher Konventionalstrafen an der Verwertung ihrer Arbeits-
kraft zu behindern. Der Protest hat die Entfernung der anstößigen Para-
graphen aus dem Gesetz nicht erreichen können, aber er hat dazu bei-
getragen, daß sie eine Fassung erhielten, die der willkürlichen Deutung einige
Schranken setzt.

Mittlerweile waren an verschiedenen anderen Orten Deutschlands
Freie Vereinigungen von Kaufleuten ins Leben getreten, die in gleichem
Geist geleitet wurden wie der Berliner Verein und meist auch der von
ihm ausgehenden Propaganda ihre Gründung verdankten. Der Verein
hatte eine Agitationskommission, die sich speziell mit der Verbreitung
seiner Grundsätze beschäftigte. Längere Zeit ward die Verbindung der
örtlichen Vereine untereinander durch ein Vertrauensmännersystem be-
trieben, das dem der lokalistischen Gewerkschaften entsprach. Im Jahre
1896 hielt jedoch die Berliner Vereinigung die Zeit für gekommen, einen
nationalen Kongreß der gleichgesinnten Kollegen zu veranstalten. Die
Agitationskommission ließ die Einladungen ergehen, und in den Ostertagen
1896 tagte in Berlin im Saal der Berliner Ressource „der erste Kongreß
aller auf dem Boden der modernen Arbeiterbewegung stehenden Landlungs-
gehilfen und Gehilfinnen Deutschlands". Er war aus 22 Orten durch
28 Delegierte beschickt, darunter Fräulein Klara Laase-Berlin, M. Ioseph-
sohn-Lamburg, R. Lipinski-Leipzig und Dr. Max Qnarck-Frank-
furt a. Main. Der entschieden sozialistische Charakter des Kongresses
ward schon durch die Büsten von Marx und Lassalle symbolisch ver-
anschaulicht und außerdem durch Annahme einer Resolution bekräftigt,
in der es heißt, daß „nur die sozialdemokratische Partei die Interessen der
Landlungsgehilfen vertritt" und die Delegierten aufgefordert werden, in
allen Städten zu wirken, daß „offen der Anschluß an die allgemeine pro-
letarische Arbeiterbewegung und an die Sozialdemokratie proklamiert wird".
Eine Gegenresolution sprach sich nicht rundweg für den Anschluß an die
Sozialdemokratie aus, betonte aber gleichfalls die Notwendigkeit des Klassen-
kampfes für die Landlungsgehilsen und die Erziehung von Sozialdemokraten
durch den Klassenkampf. Neben Fragen des gesetzlichen Schutzes und der
Versicherung der Landlungsgehilfen beschäftigte den Kongreß auch die Frage
der Gründung eines Zentralverbandes derLandlungsgehilfenDeutschlands, der
ähnlich wie andere gewerkschaftliche Zentralverbände Anterstützungseinrichtungen