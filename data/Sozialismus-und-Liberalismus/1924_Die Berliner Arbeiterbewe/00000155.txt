﻿DemofaslfJe. Sozldlisnnts.
m» kreis Foisdumfl. «<«

Das

neue üloirfagsblatt

Berliner sozialistische JVIontagoecbau.

»: «-I11—7.

CdtjtWJ: Sät I, Ux SW?.

6duard Bernstein.

©xp«KHoa>

Berilo SB., Sdjützea-Simtt R

fir. 11.

I



BsrHn, dsa Jl. 3iril WO*.

I

SnseTQie ** ‘«“JfS*

| SaKrg. I.

DEPESeKEW.

Vom Xrbgsschnphft ln Oslaslea
ticQcn unS zwei. den Japanern gsin-
fllgr MelLungen vor. Die eine. anS
Tokio, oeilätlg! offiziell die
Stfäung KoipiilgS durch di, Zapourr.
Tokio, 10 Juki, (klmtlichc Nit.
-teil»ng.) General Oku meldet: Die zweit«
Armee begann ik.ro auf die Äoietzung Kai-
pingz hinzielenden pperaNone» oni 6. Juli.
Nachdem die Stullen successive au« ihre» Siel-
limgs« vertrieben worden wäre», wurden
»alpin, und die benachbarten
Sähen am 9. Juli von den Java-
norn besetzt.

Sach der zweilen Depesche, allerdings ouB
lAer Queue, haben die

Japaner ein zweite» Fort von Dort Arthur
,	Benommen.

Tschifn, 10. Juli. lRekdiina de» Ren-
terichen Bureaus.) Au» Port Arthur hier
ringetrofsene chinesische Dlchuakensützrer be-
richten. dich am 5. d. M. die Seiche» von
über Svo Nüssen, worunter iiü, dlesenih«»
hon zwei hohen russischen Ossizieren befan-
den. von Chinesen nach Dort Arthur gebrach«
wurden, und daß «in Teil der j a p o n i-
schen Truppen 61» in eine Ent-
fernung von 6 lengllschen <?) Red.)
Meilen nach Port Arthur nach
Eroberung oineS zweiten Forts
auf östlicher Seile vorgerückt
f e i e Ein Teil der Beamten der russisch-
chinesischen Bank tu Port Arthur ist gestern
in Tschisu angekommen und sagt au», dah di«
Berhällniss» in der Stadt unverändert seien.
Die ganze lebte Woche wäre »eben Meilen
von der Stadt entfernt schwer gekämpft wor-
den. Die Mannschasl von geilern «ingelrof-
fenen Dschunken berichtet, sie bäite gestern
morgen Beschützfener in der Höb«
von Port Arthur gehört.

Fortfchritt der inlernatiouole» Schiedö-

Pari», S. Juli. Der Minister de« Aeu-
Here». D e l r a s I e e und der Glclnnb:? von
Schn,«- * n und Norwegen Hoven heute
einen SchiedSgerichtsverlrag unterzeichnet,
welcher entsprechend den mit England, Ita-
lien. Spanten und den Niederlanden abge-
schlossenen abgcsaht ist.

Wollen sie selbst backen?

Nom, 10. Juli. Die Bäckergeiellen be-
schlossen in den AuSstand zu treten. Die Be-
hörden haben die notwendigen Mabregeln ge-
troffen. um die Stadl vor Mangel an Brvl
j» bewahre».

Militärfireik in Montenegro.

Ätfinjf, 10. Juli. (Meldung des Wiener
St. K. Telegr.-Korr.-Bureau«.) Nach hier ein-
getrossenen Nachrichten erhielt eines von den-
beiden in Tanzt bei Vodaorica liegenden Ba-
taillounen Befehl, nach Mlct I» Garnison zu
gehen. Nachdem da» Bataillon in Slutari an-
gekommen war. weigerte eß sich im Einver-
nehmen mit den Offizieren wegen Nichtbezah-
luug de» Solde* weiter z» marschieren. Alke
Osiizierc wurden vcrhaflct. Der Kouverneur
von Skutari. welcher einen Aufruhr der Gar-
nison befürchtet, ist bemüht, das zur Bezah-
lung dev Soldes notwendige Geld cniszu-

(Fortfehung de: Depeschen auf der dritte»

Spitzel an) Sfeatesisisier.

Im Winter 1890 fand In Strcfmi einer
teuer «rohen Sozialtstenorozesse, -hott, an»
welchem man-dazum»! l» Oeiierkrich noch die
imvorticrte sozialistiiche Bewegung zu er-
Iilcken hasste. Der Prozeh ist in- der poinilchen
Sozialdemokralie bekannt al» der Prozeß
WaninSti »nd Genosie». Angcllagk waren
NS Personen, darimter M Runen aus Wol-
bv-rien lUkraine!, zumeist Etnd'ricndc vom
polytechnische» Institut In Petersburg, die in
Äailzlen bei der AgitatlonSärbeit verhaltet
worden waren. Es siel de» Angeklagten
während der verhandln»« aus. daß Ne der
»reiße »ach »nie: irgend einem Vorwand«
durch eine bestimmte Tür de» BerhandlungS-
saalcS hinauSgcsührt wurden, ohne dab Ile
«ich dies sonderbare verjähren erklären konn-
ten. Endlich fand einer von ihnen den wahre»
Grund an». ES war nämlich eine Dovoelisir
ml« rlner tlekeii Nische. In dicker Nische aber
befand sich alö freiwillige: HclserSbclser der
österrclchilchen Polizei und zugleich al» Spion
tlir den eigenen Dienst ein riilüichrr Klink,
lionär, dcr unter den Vorgefi!hri.'n »feine"
Leute agnoscierte. Natürlich kam cd nun zu
schwerer Insultier»»» de» Srlappicn, brr
nur durch die Iiistizkoldaten vor wetteren
Mihhandlungen gemsitzt werden konnte.
Dieser Iunkiionär aber war kein anderer, als
der bcutige Generaiaewailigc von Rußland.
<Bt. Erzellen, der WiTnifitt de» Innern, Herr
von Vietzwe. daniatL noch SiaatSanunlt
tn Warichaa.

Mit dem gelchttdertcn Spihclftück, dessen

5ch die Polen „och beute sehr genau er,
rnarn, iNhrlc «Ich der piovideniiellc Btaal«-.
man» in der außerrullikchen Welt ein. Sr ist
leinen, Charakter getreu geblieben. Sr ist
auch heut«, wo er an der Spitze der Brr-
walln»« dx» größten Staates 'der Welt steht,
nicht» anderes al« der größte Polizeispitzel
der Gelt. Seine Polilik trägt oSe die Merk-
male dcr polizeiliche» Abstammung an sich,
die Polizei im inacchiaveNisilsckcn Sinne be-
trachlcl, all das Verbrechertum lm
Dienste der Ordnung.- Ich kiabe in
ganz Rußland nicht einen einzigen Menschen
gesprochen, der zur Bezeichnung de« Plehwe.
scheu CharallerS andere Anddrlickc gewählt
lliilie al» lene, die zur Bezeichnung der
untersten Linie der moralischen Ezistcu»
dienen. Nach den Aeußerungen, die mir in
geradezu unerhörter Einmütigkeit über ihn
gemacht worden sind, will Ich versuchen. Oie*
da» Bild de» Herrn Plehwe zu zeichnen.

Mau dar! Nirmaudem Unrecht tun. ES
loll daher betont werden, dah !>n Lande der
allgemeinen ftäullichicit Herrn Plehwe doch
dcr eine Vorzug nedigcril&nit wirb. Saß er
absolut unbestechlich sei. kl» Pscüwe km) sich
noch ulchi einmal dir Bcrdächllguna bcrangc-
wagt, die Ions, auch nicht der Großkiiriicn
schont. Aber die Mulle» wissen ihm illr lene
Ctgenlchai» wenig Dank. Denn Plehwe gilt
-IS weit ärgere», denn al» ein Bcrlchwender
oder Wlistling. Er gilt ulk Bösewich» ohne
Skrnpcl, alö polltlicher Sadist. alS Bluthund
und ralstnierlcr Betrüger. Dabei «IS Cyniker
ohne lebe Gesinnung, als Dabanauc. und
gakichspleler, für- den da» politische Metier
»nd da» Spiel mit Mcnichculcbcn nicht» ist
al» ein angenehmer Nervenreiz: kurz. alS
Tiger in Menschengestalt. Dabei Ist er von
den bezauberndsten Manieren, ein Charmeur
„nd Causeur mit dem Irenherzigsiev Ge-
sichiionSdruck.

Seine unglaubliche Iallchhel» Ist do«
ibei alle diejenigen klagen, die

eich). Ist ein

- .......... .Jede» Wort, da» ..

eine Lüge." ist die Bemerkung, die

____ _... meiste» über ihn härt. Ta» Ber-

brcchcr»chc seiner Taktik besteht nicht nur
darin, baß er dem Zaren einrede», die Rcvo-
luiion stehe vor der Türe, und ihn durch Droh,
bricse, Proklamationen u. a., die er in die
innersten Gemächer, ia, In die Slocktaschen
schmuggeln läßt, i» sorlwäbrcndcr, nerven,
zcrstürendcr Angst erhält, sonder» »och mehr
darin, baß ei saklllch Unruhen provoziert, um

(« alS Argumente benützen und seine Position
Hrtoi zu kiinnen: daß er fortwährende Non-
spirationcn entdeckt »nd die angeblichen Teil-
uehmer in der iurchtbarsten Weise maßregelt,
um keine UnentbcbilichscU zu erweisen.

TaS ganze Arsenal der Polizeikuiiststücke,
di« ie in Despotien verübt worden sind, „m
Autokraten i» ivillsährige Werkzeuge ihrer
Prälorianer zu verwandeln, ist von Plchwc
geplündert worden, um lein System bis zur
Lollk-mmcnhcii ouSzubanen. Insbesondere

^esährNV..,.._______________ _____________

enibezrltchkeit de» Herrn"ylchwe —~ii "er.
härten. Daß die tiilchcncwer Mordtolen in
seinem Auftrage verübt wnrden, brzwelscli iu
Rußland kein Diensch: der EynISmu», mit
dem er Eruschewan. den Hauplhetzer au»
Bcsiarabicn, guSzelchueic, ml« dem ei de» von
einen! Lehreilmigrcß insultierte» ' Agitator
SiDnin In Schutz nahm, sind eben so viele
schamlose Zugeständnisse seiner Tat, die er
auch nur vor dem AuSlande. nicht aber vor
leine» Getiene» verkenguet.

Jede Kleinigkeit greis, er auf. um eiue
Aksäre daraus z» machen. In Warschau find
Mitgliedern eine» aomItecS, Sa» für «lncii
pdliilsche» SanllätSziig ngch dem ' «riegv-
ichaurbitz Sammlungen eingeleitet hatte, von
Studenten die Fenster elugcworscn worden.
Sosort langte der tescgraviiilchc Besetzl ei»,
die Sache auf brelkeltcr Basi« z» nnlcrsuchen,
und wenn nicht die tiicschädigleu dcr Polizei
jede Plithllle verweigert hätlen. wären wieder
ein paar Dutzend kecke Demonsiianicn - na»
Sibirien geschickt worden, dam.l dag Bor-
handenfei» einer voluischcn vieri-lntion dar-

B' I, werbt» könne.' Ein rnssstcher Rcbak»

. besten Blatt wegen Abdrucks eine» revo-
Inlionärcn Bedichi» »erboten worden war,
wurde bei dcr obersten Zcnfuibehärbe im
Ministerium de» Inner» vorstellig, um die
tkilanbuiS des WledcierscheuicuS zu ec-
IgNgen. Der Herr SeltlouSibes crklärle dem
Ncdgiteur. einem ruilikchen Edelmann, wört-
lich, er mvste-bep, Minister intiltzile.-,. dotz.ihm.
da» revalnlionSr« vedlchl von den Inden in«
Blatt gelchmuggett wordr» sei, dann werde
er svsorl ,nieder die Erlandnit! erhalten.

Bon einer Seite, von der Ich eine solche
Angabe niemals erwartet hätte» von einem
bochkonservaliven Nristokrateitziwtavene einer
Exzellenz Im Staatsdienst: wuzde mir allen
Ernstes crllnrl, daß nur- Ptthwe im Bunde
mit Alexesew den Srteg -durch abstchtltchc»
Hinbalten der Japaner .hcransbelchwoien
stabe, wett er durch tbn nur um so nuent.
bchrlicher geworben sei. Ja. ielbst die An.
bentnng wurde mir gemacht, daß die Rihi-
lisic», die «lerander ». gerade im Moment
loteten, da er die VrrsasinngSnrkunde zur
Uiilerlchrsfl Irrtig auf drin Tische liegen hatte,
nicht ganz ohne Hille der Polizei den Weg
„nn Woge» de» Kaisers gefunden Ritten. Der
Gehille Lori» MclikowS aber, der Manu, der
den Entwurf ausgearbeitet hatte, und d«r am
beste» wußte, wie nabe bis Untcizcichiuing
dcr Urtnndc bevorstand, die e» zn verhüten
galt, ici lein auderrr gewesen al» — Plehwe.
Sein Fnsttnlt treibe ihn aus die Seile der
Reaktionäre. Denn iu einem konstitniionellen
Elnaie lei illr Leute leine» Kaliber» weniger

Kere Verwendung. In Antisemitismus.

i er nqtstrlich jedem jstdttchcn Besucher
gegenüber in Abrede stellt, mache er nur, weil
lehr hochgestellte und elnslußieiSe Perionen.
wie die «aiicrlii-Witwc. v'..o',:!:!ri:	a'.n:

und andere anS der Generation Alexan-
ders III. iaualische Antilrmiken llstd. Auch
sein AnlilemilivmuS !r> nicht echt: nicht» sei
echt bei tbm, al» der Spielehrgetz, sich Io lange
alS möglich zu behanptrn »nd bi» Neesen-
kitzel eine» Seiltänzei» zu baben, der Uber
oufgcnflauzlcn Basonetten ans Sem gespann-
ten Seile jongliert.

Da» ist da« Bild d«S Minister» de«

änuern, wie Ihn die dlsenlliche Meinung ln
uxlano zeichnet. Ich muß gestehen, imch
metnrui Geschmack Ist diese ZclLnnnn !o
wenig, wie der Man». Ader während d>«
nroßcn ruMIchc» Romandichtcr vor allem
Meister in der Verwendung bee Abtönungen
lind. arbeitet die politische dlsenlliche Meinung
gern mit den kiärlsic» garbe», mit den blutig-
sie» Superlativen. Al» Richtrufsc wird man
wohl daran »i», Herrn Plehwe auö dem
Uebcr-nenschcnsormat etwaS in» alltäglich
McnichNche »» übertragen, und da kommt im.
merbln ei» etwa» aniereS Bild heran».

Ich denke e» mit, wie folgt! Pichn»
kommt von der SlaatSanwalt. und Polizel-
karrirre. Etwa» von dieiem Ursprung hasiet
jedem an. der von dort seine» AuSgang ge.
nonnncn hat. GcrichlSprälidciiten, die SiaatS-
aiiwälte gewesen, sind die Schrecken dcr Ad-
vokale» durch ihre lugnlsllorlschc Manier
und durch tbrc Neigung, in jedem Beschuldig,
ten schon einen Uebeiwicscncn zu erblicken.
Die Brschäittgung ferner mil den Polizei.
Agenic» ist am wenigsten geeignet, zur Skru-
pulosiläi zu erziehen. Mair'»r!»»ere Nch nur
de» Pn!!kammer,chcn WarleS:' - .GcnUcm-n
gehen sich Illr diesen Dienst nicht her." Dt»

beständige Furcht vor Attentaten, die bei
einem Ehes der russischen Polizei wohl br-

Cdet ist - Plehwe läßt sich seine periSu-
Ueberwachuug jährlich 800IX» Rubel
kosten — trägt auch nicht dazu bei. iemaode»
menschenfreundlicher zn machen.

Bet Plehwe und anderen kommt»» allen
üblen Einslüssen noch der hinzu, daß iielich
alS NtchNnsien - Plehwe ist poinisch-lclttsch-
jüdischen Ursprung» — bnrch besonderen rul».
schen ShanpiniSmuS hervortun müncn, um
sich uiwcodüüiig zu erhalten. Groß ab" ist
an Plcbwe gar nicht«, i-inc ganze Ministei.
täligkei« weist nicht eine etnziye bemerken

bchaurien will und Ilai -----------...... .. . .

da» Jntrlgiienspiel seiner Rivalen mit alle»
laudeüttbltchen Mitteln zu burchtrruzc».
„Voils laut!" Wahrheitsliebe ist im allge.
meinen keine Eigenkämit dcS iogcnannic»
öljcnklichcu Lebens In Rußland. E» wäre
daher unbillig, Herr» Plehwe Verlogenhei:
als ein besonderes Laster anzurechnen. eBU
ist bei ihm nur in» belondt

Man wird «»geben müssen,, daß auch l°

Maßlose eut-

b°S Bild noch.weil davon kntscrnt ist, ein
-rfreullchr» i» sein. Füg, man zu dielc-.
Zügen noch die eewielene Tatsache hinzu, daß
Herr Plehwe seine eigenen polniso-en Pflege-
eitern, die ihn iozukage« von der Straßc ans-
gelesen und erzogen batten, dem Grneialgou-
veruchir »rastn Pkuiowiew briinnzirrlc. I-
daß sie.-»um Drück-für fbr«. Wobilak.guuh. S>-
lttrstn geschickt wurden, daß also Plcbwe fein-.
Karriere mit einer Handlung deS inlomgen
Undanks »nd Verrat« begonnen hat — siehe

Strnwe'S OSwobolchdicnic' — ko wirb man

KV	«Ä,rÄ*!i

verzlchlen zu können.

Das Bezeichnendste aber, was ich über »ad
System Pkebwe hülle, war doch die Antwort,
die ich erhtiH. als ich einen recht hochange-
stcllten Mann fragte, ob denn eine Beilerung
zu erwarten sti. wenn Plehwe einmal von
feinem unausbleiblichen Schicksal ereilt wor-

.Neiu." lauleic dies« Antwort. .So wohl-
verdient auch jedes Schicksal siir thu stln
wird, gebossen wird unk damit nicht. Ein
anderer Man», da« ist alles. Pledw« ist nur
in idealer AuSvrUgunn, waö da« Loiicin ver-
langt. Der Polizeiiiaat brauchl Poliz-!-
jeelr», und cr findcl'sie immer. Plehwe >»
rnit allen Lastern außer dem der Vestrchlich-
keit behaltet, aber cr ist durchaus kein Uni-
kum in dcr ruMlchen Bcamtenweik. Und e»
ist durchaus »ich! wahrscheinlich, daß etwa»
BcflercS nachkammen würde. Wenn. «an»
Rußland holst (wörllich!!. daß ihm bald der
GarauS gemacht werde, io ist e» nicht, wen
man davon sich eine Besserung dcr Zuk.ande
verspräche, sondern weil man dom irgend eine
Genugtuung erleben will, wrun bas Mas
einer dieser Bestien voll Ist. Ein Phttantroo
nnd Recht«!!,und wird aber Io wenig I« Mi-
nister beb Innern unter dem AbloluttSmuS
werden, wie er da« «edürsntS hatte, gferl-
rlchter zu werben. Rur ein anderes ««Neu.
kann mt» andere Männer bringen. Da« Sal-
grnsystcm verträgt ^joSaBj

Der Bersasser de« vorstehenden ia« «Ine
längere poiitilche Studlcureist in Rußland
hinter sich, die ihn in«, «ngehdriae» alle.
Klaffen und Parteien in veibindimg brachte
Er «l ei» Morset Beobachter, dessen Urteile
eine um so stärkere Sprache sprechen, al» d >
Mann weder Sozialist »och Revolutionär ist.
Selbst in seiner abgetönten Darstellung Ist
baö Bild de» Herrn v. Plehwe
unabhängig gesinnten Deutschen die Röte Do
Scham In da» Gesicht ,U treiben bei dem Ge-
danken. baß deutsche Gerichte und bcutsch-
B-amte in Anioruch genommen «erden, em
System zu unterstützen, beffr» anserwSvi.er
Rrpräsrnlanl dieser g-sinm„,g-l°se. kavo-
artige StreSer mit ber>> Spitzel- und Drnnn
zianIen-Jnstiukl Ist-	In. fe'»««

Lande weist die üsseutlichc Meinung mit ol»-

gern auf ihn. und hi Deutschland stellt man
Deutsche vor Gericht °l» Hochoerräter ^
- Plebwe. Welch eine Ehre kür da, *>■<»

Me W f	at-

71. Titelseite einer Seite des „Neuen Montagsblattes"

Bernstein, Berliner Geschichte. III.

10