﻿..<Sr

— 40 -

Zu mehr konnten sich die Lerren vom Zentrum und Freisinn jedoch
nicht verstehen. Anträge der Sozialdemokratie, das Koalitionsrecht dev
Arbeiter, von dem erwiesen war, daß es nur erst ganz unzulänglich bestand,
nun auch sicherzustellen, wurden am 1. Dezember 1899 unter Mitwirkung
derselben Parteien abgelehnt, die mit der Sozialdemokratie die Regierungs-
vorlage verworfen hatten. Dagegen nahm der Reichstag fünf Tage
darauf in letzter Lesung das schon oben erwähnte „Not-Vereinsgeseh" an,
das "für ganz Deutschland alle Bestimmungen aufhob, die politischen
Vereinen verboten, untereinander in Verbindung zu treten. Damit war
wenigstens den Gewerkschaften eine größere Bewegungsfreiheit ermöglicht,
als ihnen bis dahin zustand.

Was die Agitation der Arbeiterschaft gegen die Regierungsvorlage
ungemein stärkte und der sozialdemokratischen Propaganda überhaupt
großen Nachdruck verlieh, war das im Februar 1899 ergangene Lirteil im
Prozeß gegen neun Maurer in Löbtau, die, von einem Richtschmaus
komniend, auf einem Ban im Streit mit dem Bauherrn, der ihnen den
Zutritt verwehren wollte, Gewalttätigkeiten verübt hatten. Sieben von
ihnen waren zu durchschnittlich acht Jahren Zuchthaus, zwei zu je
vier Jahren Gefängnis verurteilt worden. Wenn je, so war in diesem
Erkenntnis des Dresdener Schwurgerichts, vor dem die Sache verhandelt
war, der Ausdruck Klassenjustiz am Platze. Denn wenn die Arbeiter
sich auch straffällig gemacht hatten, so rechtfertigte doch nichts eine Ver-
urteilung, die über das Maß der Bestrafung gewöhnlicher Raufereien
hinausging. Die Maurer hatten den Bauherrn, der auf sie aus einem
allerdings blind geladenen Revolver geschossen hatte, so geschlagen, daß er
mehrere Tage das Bett hüten mußte, aber weder ihm noch sonst jemand
war ein ernsthaftes Übel zugefügt worden. Wenn trotzdem jene furcht-
baren Strafen verfügt wurden, so stand das in so schreienden, Wider-
spruch zu dem Verhalten der Gerichte, wo vornehme Raufbolde in Betracht
kommen, daß der Spruch überall, wo Arbeiter zu denken angefangen hatten,
die tiefste Entrüstung Hervorrufen und die Gemüter mehr als je für den
politischen und gewerkschaftlichen Kainpf anstacheln mußte.

Das hatte aber um so mehr zu bedeuten, als unter dem Einfluß der
Eaprivischen Handelsverträge die Entwickelung Deutschlands zum Industrie-
staat gerade in dieser Zeit Riesenfortschritte machte. Zwischen 1882 und 1895
hatte die landwirtschaftliche Bevölkerung Deutschlands um 700 000 Köpfe
abgenommen, die industrielle um 4 200 000 Köpfe sich vermehrt, in der
Industrie aber entfiel der Zuwachs auf die Arbeiterklasse, die mit ihren
Angehörigen von 8,7 Millionen auf 12,9 Millionen Köpfe gestiegen war.
Lind dieser Entwickelungsgang, der sich ähnlich, wenn auch nicht in gleicher
Schroffheit, im Lande! und Verkehr vollzogen hatte, — von einer Gesamt-
zunahme um 1 400 000 entfiel eine Million auf die Arbeiter dieser Berufs-
gruppen — nahm unausgesetzt in der gleichen Richtung seinen Weg. Die
Industrieorte wuchsen mit einer Schnelligkeit, die vielfach an amerikanische
Vorgänge erinnert, und die Arbeiterschaft nahm einen immer breiteren
Raum in der Öffentlichkeit ein. Die Arbeitseinstellungen nahmen an Zahl
und Ausdehnung zu, und öfter als früher fanden die Arbeiter bei ihren
Kämpfen bürgerliche Elemente auf ihrer Seite, wofür ein besonders bekannt
gewordenes Beispiel die Unterstützung der Hafenarbeiter Hamburgs bei