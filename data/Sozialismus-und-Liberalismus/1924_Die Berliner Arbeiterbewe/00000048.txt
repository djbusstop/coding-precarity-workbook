﻿44

Von der Flottenvorlage der Regierung strich der Reichstag nur die
geforderte Vermehrung der Auslandsflotte, ein belangloser Abstrich, da die
Ausführung des bewilligten Programms anderthalb Jahrzehnte erforderte,
die Zwischenzeit aber hinlänglich Gelegenheit bot, diesen Teil wiederherzu-
stellen. Im übrigen wurde ein Schiffsprogramm bewilligt, das die Schlacht-
flotte auf vier Geschwader von je 8 Linienschiffen, 8 großen und 24 kleinen
Kreuzern, nebst zwei Flottenflaggschiffen, sowie eine Erhöhung der Material-
reserve um 2 große Linienschiffe, der Besatzung um 30 000 Mann bringen
und bis zur Fertigstellung im Jahre 1917 nicht weniger als 4352 Millionen
Mark kosten sollte. Bei der Endabstimmung — am 12. Juni 1900 —
ward das Flottengesetz mit 201 gegen 103 Stimmen angenommen. Wie
wenig es den marinistischen Tendenzen der Regierenden eine Grenze setzte,
zeigte das im Danktelegramm des Kaisers wiederkehrende „Run aber weiter-
arbeiten!" Im Etat für 1906 erschien denn auch der Posten für die Aus-
landsflotte und wurde selbstverständlich bewilligt.

Kaum, daß das Flottengeseh von 1900 angenommen war, trafen die Nach-
richten aus China ein, die den Anlaß zu der schon besprochenen China-Expedition
gaben. Da in China nur wenig Ruhm zu holen war, gingen die mit-
interessierten Mächte gern darauf ein, daß ein deutscher Leerführer den
Oberbefehl über die vereinten Straftruppen erhielt, und Graf Waldersee
ward als „Weltfeldmarschall" unter einem Aufwand von tönenden Reden
und Schaustellungen nach Ostasien entsandt, die die Welt weit mehr in Er-
staunen versetzten als seine militärischen Verrichtungen. Daß die deutschen
Soldaten, wie übrigens die Soldaten der anderen Mächte, bei den Schar-
mützeln mit Chinesen ihren Mann standen, soll anerkannt werden. Daß aber
ein Teil von ihnen unter dem verrohenden Einfluß des Krieges mit einem
Volk, dessen Sprache und Kultur sic nicht verstanden, Landlungen
begingen, die durch die Notwendigkeiten des Krieges nicht zu rechtfertigen
waren, ist durch viele Soldatenbriefe vom Kriegsschauplatz in glaubwürdiger
Weise bekundet worden. Wenn die China-Expedition, bei der über 500 Deutsche
teils auf den» Schlachtfeld und teils im Lazarett ihr Leben ließen und
Tausende für ihre Lebenszeit zu Krüppeln oder Patienten wurden, Deutsch-
land keine materiellen Vorteile eintrug, so war das noch hinzunehmen. Viel
schlimmer ist, daß der Name des Deutschen als Kriegsmann unter kulturellen
Gesichtspunkten schweren Schaden gelitten hat.

Bei der China-Expedition hatte, wie bei der Agitation für die Flotte,
Wilhelm II., unterstützt durch den Minister des Auswärtigen, Bülow, den
Dirigentenstab geschwungen, der altersschwache Lohenlohe hatte nicht mehr
dreinzureden. Seine Rolle als Aushilfskanzler war ausgespielt, er mußte
gehen.

5.	Fünf Jahre der Ära Bülow.

Bald nach Einleitung der China-Expedition legte Fürst Chlodwig Lohcn-
lohe fein Reichskanzleramt nieder und an seine Stelle trat am 17. Ok-
tober 1900 der ein Jahr vorher in den Grafenstand erhobene Bernhard
von Bülow, bis dahin Minister des Auswärtigen. Als solcher hatte er
sehr viel stärker als seine Vorgänger nationalistische Töne angeschlagen und
sich als ein gewandter Parlamentsredner offenbart, der seine Reden
wirksam zuzuspitzen verstand. Aus der Rechten sah man ihn nicht ungern.