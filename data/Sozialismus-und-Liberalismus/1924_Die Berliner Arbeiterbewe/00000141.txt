﻿131

0rutullinieti

für «ln gtuiklulam« Zutaifirccsiarbeiit» der Partei uitd du
öetwrklAafi« tu den ttädtlten Kelätstaatunble» tu dir
Prent«? Braadesburg.

die Beseitigung des Kapitalismus
selbst, sich zur Aufgabe zu stellen
hätten. Das lautete hinsichtlich
der Gewerkschastssache nicht sehr
tröstlich, entsprach aber so sehr der
in der sozialistischen Arbeiter-
schaft Berlins vorherrschenden
Stimmung, daß dieVersammlung
diesen Darlegungen begeisterten
Beifall zollte und eine in ihrem
Sinne lautende Resolution ein-
stimmig annahm. Nicht anders
wie Bebel sprach sich IgnazAuer
am 26. Januar 1892 in einer Ver-
sammlung, die von der Lokal-
kommission Berlins zur Erörte-
rung der Boykottfrage veranstaltet
worden war, über die Möglich-
keiten der Gewerkschaftsbewegung
aus. Latte er doch schon im
Juli 1890 in dem bemerkenswerten
Artikel „Gewehr bei Fuß!" drin-
gend dazu aufgefordert, die Streik-
lust einzudämmen, und dieselbe
skeptische Note klang in seinem
Referat wieder, das er im Lerbst
1892 auf dem Berliner Partei-
tag der deutschen Sozialdemo-
kratie hielt. Des weiteren trug
der gerade in Berlin stärker als
anderwärts spielende Streit
zwischen lokalistisch und zen-
ttalistisch organisierten Gewerk-
schaften viel dazu bei, den Glauben
an eine große Zukunft der Gewerk-
schaftsbewegung zu erschüttern,
und diese ungünstige Stimmung kommt in vielen Referaten und Debatten
zum Ausdruck, die sich in jener Zeit in Berlin mit der Gewerkschaftsfrage
befaßten oder auf sie Bezug nahmen.

Am so eifriger widmete man sich der politischen Agitation und Orga-
nisation, und immer weiter in die Provinz hinaus streckt sich Berlins
Sozialdemokratie ihr Arbeitsgebiet. Am 15. Mai 1892 tagte in den
Arminhallen die erste größere sozialdemokratische Parteikonferenz
für Berlin und die Provinz Brandenburg. Sie war von 54 Dele-
gierten aus 20 Wahlkreisen besucht und wählte Fritz Zubeil und Ferdinand
Ewald zu ihren Vorsitzenden. Ihre Debatten drehten sich vornehmlich
um Fragen der Agitation in den ländlichen Bezirken und unter den Frauen.
Es ward beschlossen, die drei bereits ins Leben gerufenen Zeitungen:
„Brandenburger Zeitung", „Märkische Volksstimme" und

9*

Die Agitations- und Organisationsleitungen übernehmen die
moralische Verpflichtung, soweit das ohne Gefährdung der örtlichen
Organisationen möglich ist, durch Ihre Funktionäre unter den Mit-
gliedern und Anhängern für die Beteiligung an den. Arbeiten und
der Agitation zu den nächsten Reichstagwahlen zu wirken.

Um der Bewegung den Charakter der Einheitlichkeit zu sichern,
informiert die örtliche Parteileitung erstere von Zeit zu Zeit über
die wichtigsten Fragen, insbesondere die der Taktik.

HB.

In der Praxis hätten die Arbeiten in folgendem zu geschehen:
a) Beteiligung an der Wahlarbeit,
d) Agitation für den sozialdemokratischen Kandidaten,
c) Agitation für Presse und Literatur,
et) Aufbringung von Geldmitteln.
t) Hilfeleistung und Besorgung von Lokalen zu Versamm-
lungen und Besprechungen.

IE«.

Um diese Tätigkeit ln. bie Wege zu letten, empsiehtt es sich:

1.	Dah die Gewerkschüstsagitatoren angewiesen werden, in
der nächsten Zelt, ihre Anhänger ständig In geeigneter
Weise auf di« Wichtigkeit der Wahlen aufmerksam zu
machen und in allen Zusammenkünften ebenfalls in ge-
-igneter Welse nach dieser Richtung hin zu wirken;

2.	die Agitationsleitungen mögen in geeigneter Form die
Vorstände der örtlichen Organisationen für obige Tätig,
keil zu interessieren versuchen;

3.	soweit es möglich durch die Fachpresse für vorstehendes
zu wirken.

IV.

Um den örtlichen Parteileitungen alle Kräfte zur Dersüb-ng
stillen zu können, ist zu empfehlen, daß die Gewerkschafisvorständc
jedes einzelnen Ortes alle vertrauenswürdigen Mitglieder be-
zeichnen — insbesondere in Dörfer» — um diese als Stützpunkte
bei der Hausagklation und Flugschriftcnvcrbrciliing verwenden zu
können. Falls mehrere an einem Orte wohne«, sind sic l» persön-
liche Verbindung zu bringen.

V.

Diese Regeln wollen die Organisationen nach Möglichkeit
versuchen, inne zu halten. Die Durchführung derselben, unter
Anpassung der Eigenart des Berufes ist "Aufgabe jeder Orgaut-
sation selbst.

v«urcr»D>tum>».«

64. Zirkular an die Vertrauensmänner
in der Provinz Brandenburg