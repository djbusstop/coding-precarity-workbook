﻿Dem Jenaer Parteitag der deut-
schen Sozialdemokratie war der erste
Parteitag der sozialdemokra-
tischen Partei Preußens voraus-
gegangen, der vom 28. bis 31. De-
zember 1904 in Berlin tagte und von
148 Delegierten besucht war. Dieser
Parteitag faßte nach Referaten von
Lugo Leimann, Art. Stadt-
hagen, Leo Arons und Georg
Ledebour Beschlüsse über einen
Wohnungsgesetz-Entwurs der
Regierung und einen Gesetzentwurf
dieser gegen kontraktbrüchige Ar-
beiter, über ein Schulgesetz-
Programm, sowie über die Frage
des Kampfes für das gleiche
Wahlrecht in Preußen. Die
Parteivertretung von Groß-Berlin
wurde beauftragt, spätestens 1906
einen zweiten preußischen Parteitag
einzuberufen und diesem Vorschlage
für eine Landesorganisation dcrSozial-
demokratie Preußens zu unterbreiten.

3.	Die letzten Jahre der Ära Caprivi.

Wir kehren nunmehr zur Schilderung des allgemeinen Ganges der
Politik im Reich und in Preußen zurück.

Am 6. Mai 1893 lehnte der Deutsche Reichstag mit 210 gegen 162
Stimmen eine Militärvorlage der Regierung ab, die für die Einführung
der zweijährigen Dienstzeit eine Erhöhung der Friedenspräsenzstärke um
nahezu 100 000 Mann verlangte, sowie einen Vermittelungsantrag des
Zentrumsabgeordneten von Lucne, der die Erhöhung auf 70 000 Mann
gebracht hätte, worauf die Regierung den Reichstag auflöste und auf
den 15. Juni 1893 Neuwahlen ausschrieb. In dem nun sich abspielenden
Wahlkamps wurden von Seiten der Gegner der Sozialdemokratie die
Broschüren Eugen Richters, worin dieser den Sozialismus als unmöglich
hinzustellen suchte, in vielen Lunderttausenden verteilt, während von sozial-
demokratischer Seite, neben anderen Agitationsschriften, das Stenogramm über
Reichstagsverhandlungen vom Januar und Februar 1893 über den Zu-
kunftsstaat der Sozialdemokratie, bei denen diese und die gegnerischen
Parteien ihre Lauptredner ins Feld geschickt hatten, in hunderttausend
Exemplaren verbreitet wurde. Das Resultat war, daß die Sozialdemokratie
ihre Stimmen von 1 427 000 auf 1 787 000 vermehrte und 44 Vertreter
in den Reichstag entsandte, während sic 1890 nur 35 Abgeordnete durch-
gebracht hatte. In Berlin eroberte die Sozialdemokratie zum vierten und
sechsten noch den zweiten, dritten und fünften Wahlkreis und vermehrte
ihre Stimmen von 126 000 auf 151 000. Sonst aber ergaben die
Wahlen eine für die Leeresvorlagen der Regierung günstigere Zusammen-