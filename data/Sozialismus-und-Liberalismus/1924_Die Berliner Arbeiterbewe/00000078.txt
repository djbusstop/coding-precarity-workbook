﻿72

rekrutiert, Abbruch tun. Es scheint denn auch, wenn man von dem Treiben irr
der Friedrichstadt absieht, die Straßenprostitution in Berlin abgenommen
zu haben. Was sich aber auf der Friedrichstraße und ihren Nebenstraßen
allabendlich an Prostitution cinfindet, das ist zu einem guten Prozentsatz
aus allen Teilen Deutschlands und selbst des Auslands zugereist. Berlin
ist auch in diesem Punkt jetzt — Weltstadt.

Hauptstadt der Vergnügungen und Hauptstadt der Arbeit, das ist das
Berlin, nrit dem wir es nun zu tun haben. Könnte man eine Statistik
der Menschen aufmachen, die in Berlin von den Vergnügungen Und was
man dafür hält, leben, cs würden ganze Armeen znsaminenkommen. Mit
der Zahl der Menschen, die nur dem Genuß leben, mußte natürlich auch
die Zahl der Menschen zunehmen, die von der Arbeit für das Vergnügen
und von der Ausbeutung der Vergnügung Suchenden leben. Aber es
ist das nur Ein Faktor der Steigerung dieser Armee. Das ganze über-
hastete Leben unserer Zeit, die intensivere Art der Arbeit, die veränderte
Art des Wohnens in den die Menschen immer weiter von der Natur
entfernenden Mietskasernen — alles das schafft die nervöse, nach Ab-
wechselung und starken Reizen drängende Stimmung, die den zunehmenden
Anlockungen gegenüber widerstandslos ist. Hierher gehört auch die Aus-
breitung, die das Wetten in Berlin gefunden hat. Ehedem wetteten, außer
den Sportsleuten selbst, fast nur noch die Angehörigen der Fleischerzunft.
Der Anfang des zwanzigsten Iahrbunderts findet in Berlin das Wetten
so verbreitet, daß selbst in den Volksvierteln Barbiere, Gastwirte und
Zigarrenhändler in wachsender Zahl sich veranlaßt sehen, die Nennlisten
zu abonnieren und dies durch Aushänge bekanntzugeben. Die mit dem
Wetten verbundenen sozialen Gefahren werden wohl manchmal übertrieben,
daß aber solche Gefahren bestehen, kann niemand leugnen. Seine starke Aus-
breitung muß daher als ein übles Symptom der Entwickelung bezeichnet
werden. Sie würde zu sehr Pessimistischen Schlüssen nötigen, wenn Berlin
nicht zugleich damit, daß es eine .Hauptstadt der Arbeit ist, die größte
Hauptstadt der Arbeiterbewegung wäre.