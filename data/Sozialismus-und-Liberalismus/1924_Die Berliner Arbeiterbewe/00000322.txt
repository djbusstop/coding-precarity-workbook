﻿308

stituierten Gewerbegericht geschehen. Während sonst solche Einigungs-
verhandlungen womöglich in engster Abgeschlossenheit gepflogen werden, ward
für diese mit Rücksicht auf das große Interesse, welches die Öffentlichkeit
an dem Streik nahm, der Bürgersaal des Rathauses gewählt, den dann
auch am festgesetzten Tage, dem 19. Februar, eine Zuhörerschaft von gegen
500 Personen aus den verschiedensten Bevölkerungsklassen füllte. Gelehrte
und Handwerker, Studenten und Arbeiter, Fabrikanten und Kaufleute,
Männer und Frauen folgten, nicht nach ihrer Klassenzugehörigkeit, sondern
fast streng nach ihrer Parteinahme für die Kampfparteien getrennt, gespannt
den Verhandlungen, die nahezu zehn Stunden — von 10 Ahr vormittags,
mit einer Stunde Anterbrechung, bis 81/2 Ahr abends — dauerten. Das
Einigungsamt bestand aus dem Magistratsassessor von Schulz als Vor-
sitzenden, den Fabrikanten Weigert und Gerschel als Beisitzern sowie
dem Rentier Brock, einem früheren Konfektionär, als Vertrauensmann der
Arbeitgeber, den Arbeitern Ad. Schulz und £. Stubbe als Beisitzern und
dem Schneider Witte als Vertrauensmann der Arbeiter. Vor Gericht ver-
treten waren die Fabrikanten durch 9, die Zwischenmeister durch 17 und
die Arbeiter durch 7 Personen: die um zwei Berufskollegen verstärkte Fünfer-
kommission. Das Ergebnis der langen und eingehenden Verhandlungen war
ein Kompromiß, über das für die Herrenkonfektion vom Einigungsamt ein
Protokoll aufgenommen wurde. Es erhielt auf diese Weise einen amtlichen
Charakter und ward von allen Beteiligten unterzeichnet. Die Fabrikanten
der Damenmäntel-Konfektion zogen es vor, die Abmachung als freie Ver-
einbarung abzuschließen, über die indes auch ein schriftlicher Vertrag aufgesetzt
und von den Vertretern der drei Parteien unterzeichnet wurde. Die Be-
dingungen waren in beidenFällen: 1. die Zusage einer Erhöhung der Lohnsätze;

2.	Gegenseitigkeitsverpflichtungen in bezug auf allseitige Durchführung der
Tarife (die Fabrikanten sollten nur tariftreuen Meistern Arbeiten geben,
Meister und Arbeiter nur für tariftreue Firmen arbeiten); 3. Bestimmungen
über Schlichtung von Tarifstreitigkeiten durch paritätische Schiedskommissionen
und 4. bessere Regelung der Abfertigung der Heimarbeiter. Die Lohn-
erhöhung sollte in der Herrenkonfektion in 12T/3 Prozent Aufschlag auf alle
vor dem Streik bezahlten Lohnsätze bestehen, die unterhalb eines provisorisch
aufgestellten Mindesttarifs stünden; in der Damenmäntel-Konfektion ver-
pflichteten sich die Vertreter der Fabrikanten, bei ihren Mandatgebern Lohn-
erhöhungen für Meister und Arbeiter zu befürworten, die sich bei den ganz
niedrigen Löhnen auf 30 Prozent, bei den besten Löhnen auf 10 Prozent
Zuschlag berechneten. Die endgültige Festsetzung der Mindcsttarife in der
Herren- und Knabenkonfeftion sollte erfolgen, nachdem das Einigungsamt
auf Grund von Erhebungen über die Arbeitsvcrhältnisse in der Berliner
Konfektionsschneiderei ein Gutachten abgegeben hätte, und diese Erhebungen
sollten unter Hinzuziehung von Vertretern der Beteiligten unverzüglich ihren
Anfang nehmen.

Da an Aufbringung so großer Mittel, um 30000 Streikende längere
Zeit zu unterstützen, nicht zu denken war, und cs sich bei der großen Mehrheit
dieser um sehr arme und widerstandsschwache Elemente handelte, konnte
man diese Abmachungen als einen schönen Erfolg der Bewegung betrachten.
Bei loyaler Durchführung bedeuteten sie, wenn sie auch nicht alles brachten,
was die Arbeiter verlangt hatten, einen wesentlichen Schritt vorwärts