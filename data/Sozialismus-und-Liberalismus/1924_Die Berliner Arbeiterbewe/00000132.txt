﻿Reichstag sei zur Losung der sozialen Frage berufen. — Und. wir sollen aus praktischen-Gründen parlcp.
meuteln? Nur der Werralh oder die KurzstchligKeil kann . es unS zumuthm.'

Und an einer andern Stelle schreibt Liebknecht:

.Der Sozialismus ist keine Frage der Theorie mehr, sondern einfach eine Machtfrage.- die Ist, keinem Parla»
larncnt, die nur auf der Straße, auf dem Schlachtfelde, zu lösen ist, gleich jeher, andern Machksrüge.

Damit vergleiche man nun die jämmerlichen Gesetzentwürfe und die kläglichen Reichstagsteden, welche die FrÄktkött
Und Liebknecht mit. inzwischen verbrochen hak.

Auf was ist nur» diese, für Jeden sichtbare Schwenkung zurückzuführen?

Die Führer sage», daß sie bei ihrem Vorgehen Rücksicht auf das Kleinbürgerthuür rc. .zu' nehmen gezwungen seien,
um die Partei zu $ti,er volksthüintichen zu »nachen und die breite Mass? hinket sich zu hahett. Wir übet sind der Ansicht,
baß Ulan mit dieser breiten Masse nur sich selbst und Andere tauscht. Die Masse wttd im gegebenen Augenblick eben so
rasch abfallen, wie sie gekommen ist. »veil ihr nicht »»»ehr wie Alles fehlt, wäs eine SdZialdemckkratische HWWhaW beseelen
muß. Die Masse kann schließlich Jeder habe»», der es versteht, sich überall dön Verhältnissen anzupassen. wie die üntifemitische
Agitation dies auf das Deutlichste beweist.

Deshalb ist auch dir neue tzaktik in dieser Hinsicht nichts weiter, als ein Kompromik mlt der Masse, auf Kosten
Lrs Prinzip'-.

Wir alle glaubten, daß nach Allshören des Sozialistengesetzes und dem Einzug von 33 Sozialdemokraten ist den
Reichstag die Agitation prinzipieller betrieben und die Gesetzentwürfe und Anträge unsererseits weiter als bisher geheü
m ritzte». und das gerade Gegentheil trat ein,

Rach den» Entwurf'unserer Fraktion soll der 8stündige Arbeitstag erst mit dem Jahre 1868 Gesetzeskraft erlangen,
krotzdern auf dein internationalen Kongreß in Paris 1880 beschlossen würde, daß der 8stündige Arbeitstag schon jetzt überall
von der Gesetzgebung zu fordern sei. Wie verträgt es sich aber außerdem mit der Demokratie, daß der Reichstag im JaHr'v
1890 etwnS beschließen und zum Gesetz erheben soll, »vas erst sin Jahre -1996 Gesetzeskraft erlangt? In unserem Hrogramck
fordert» wir alljährlich Neuwahlen und halten es für unstatthaft, daß die jeweiligen Vertreter des Volkes irgend etwas fest-
legen, »vas erst Lu einer Zeit zu Recht bestehen soll, »vo unter Umständen Bessere oder auch Schlechtere, jedenfalls aber Andere
die Vertretung ausüben. Wie verträgt es sich ferner mit der Dernokratie, wenn im Reichstage Seitens unserer Fraktion bei'
Berathung der Gewerbenovelle der Antrag gestellt »vird, daß das Eiflbezichen verschiedener Gewerbe unter dieses- Gesetz durch
Kabinetsordre, also den Kaiser persönlich, geschehen soll, und nicht durch den Reichstag oder der Zustimmung desselben?

Auch das ist ein Zugsständniß au die Krone, welche- -sich von unserem Standpunkte durch ' nichts' rechtfertigen läßt,
auch dann nichts wenn man Grund zur Annahme hätte, daß der Träger der Krone volksfreundlicher als die Volksvertret,«kg
selber wäre.

Feind des Mitilarisinus in jeder Form, verstieg sich dennoch Bebel im vorigen Jahre so weit, - daß er der
Regierung feine Unterstützung versprach, wenn sie in Anbetracht des rauchlosen Pulvers, anstatt der bisherigen blanken Uniform
ganz schwarze Uniformen anschaffen wollte. waS er später dainit rechtfertigte, daß ja auch diese vermiitderte Treffsicherheit deö
Feindes den bei der Armee sich befindenden Genossen zu Gute käme. Wer so argumentirt, kann- schließlich alles rechtfertigen.
Auch die Junker behaupten, daß die hohen Getreidepreise dem landwirthschastlichen Arbeiter.an seinem Lohne und somit 'dem
Volke zu Gllte kämen. Wenn die Regierung etliche hundert Millione»» zuin Kasernen- »ind Festungsbau verlangt, dann be-
hauptet auch sie, daß dadurch Arbeitsgelegenheit geschaffen mürbe und der weitaus größte Theil dieser verlangten Summen doch
wieder in die Laschen der Arbeiter z»»rücksiieße. Dasselbe behauptet' 'der Zünftler, wenn es sich um öffc»tt!iche Arbeiten
handelt, gleichviel welchem Z»veck sie dienen.

Das höchste in dieser Beziehung leistete in den letzten Tagen der ehemals als radikal bekannte Avg. v. Wollmar.
Wenn die Regierung nun immer noch kein Einsehen hat und dem, mit unstreitig ftaatsmännischem Talent miSgestattekeir
Genossen den Marschalöstab nicht verleiht, dann hat'er sich, sicher das erste Anrecht auf das zunächst frei werdende Minister-
Portefeull durch seiner Münchener Rede vom Montag, den 1. Juni, erworben. Richt bloS die Natidnalliberalcn und Frei-
sinnigen, auch wir haben jetzt unsern StaatSma:»»». Und da thut man noch empört, »venu »vir vo>» Korruption sprechen? Da-
Wort ist viel zu gut. denn die Handlungen einzelner grenzen nahezu an Verrath.

So hat sich such die Agttotisn gegen die Getreidezölle durch nichts von dem. ent dir Freisinnigen die Aufhebung
bebegründen, unterschiedn!.

59—62. Anklageflugblatt der Berliner Opposition gegen die sozialdemokratische

Reichstagssraktion