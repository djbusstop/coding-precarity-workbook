﻿160

schaffen, so geschah dies nur, um die Sache nicht übers Knie zu brechen.
Es ward beschlossen, den Parteitag schon nach zwei Jahren zu wieder-
holen und dann die Organisationsfrage zu beraten. Mit der Veranstaltung
dieser zweiten Zusammenkunft wurden die Berliner Parteigenossen betraut,
denen der von Paul Singer und Oskar Schütz-Breslau präsidierte
Parteitag seinen besonderen Dank für die Unterstützung seiner Arbeiten
und alle ihm sonst erwiesene Gastfreundschaft aussprach.

Bei der Berichterstattung über den Preußentag, die am 17. Januar 1905
in den Wahlkreisen Berlins erfolgte, ward in allen Versammlungen dem
Gefühl der Zufriedenheit mit den Arbeiten des Kongresses freudiger Aus-
druck gegeben. Lier und da erörterte man auch die Fragen der Straßen-
demonstrationcn und des politischen Streiks, jedoch noch wesentlich als
abstrakte Fragen bloßer Möglichkeiten. Da brach Rußlands Revolution
aus mit ihren gewaltigen Massenstreiks, und die überraschenden ersten
Erfolge dieser letzteren wirkten auch auf Deutschland zurück. In 21 großen
Volksversammlungen, die sämtlich überaus stark besucht waren, gaben schon
am 9. Februar 1905 die Arbeiter Berlins ihrem solidarischen Empfinden
für den russischen Freiheitskampf Ausdruck, immer mehr richteten sich
die Augen nach Rußland, und es ist nur begreiflich, wenn in der ersten
Begeisterung übertriebene Erwartungen an den dortigen Kampf geknüpft
und aus den ersten Erfolgen der russischen Streiks, bei denen ja die Arbeiter
die große Masse des Bürgertums auf ihrer Seite hatten, übertriebene
Folgerungen abgeleitet wurden. Sie fanden leidenschaftliche Vertreter in
einigen schriftstellerisch und rednerisch begabten Genossen, und die so ge-
schaffene Stimmung machte sich auch in Gewerkschaftskreisen bemerkbar.
Aber aus Gewerkschaftskreisen kam auch die erste Gegenströmung.

Maßgebende Leiter der deutschen Gewerkschaftsbewegung erblickten in
der Propagierung der Maffenstreikidee eine Schädigung der organischen Ent-
wicklung der Gewerkschaften, und auf dem Kongreß der freien (sozialistischen)
deutschen Gewerkschaften, der zu Pfingsten 1905 in Köln tagte, fand mit
allen gegen eine Stimme eine von Th. Bömelburg beantragte Resolution
Annahme, die „alle Versuche, durch die Propagierung des politischen Massen-
streiks eine bestimmte Taktik festlegen zu wollen, entschieden zurückweist".
Weniger noch der Wortlaut der Resolution, als die zu ihrer Motivierung
gehaltenen Reden, erregten in denjenigen Parteikreisen, in denen die oben-
geschilderte Stimmung obwaltete, lebhaften Anwillen, und das zeigte sich
unter anderem in einem Teil der Versammlungen, in denen am 22. August 1905
die Sozialdemokraten Berlins in den verschiedenen Wahlkreisen zum bevor-
stehenden Parteitag der Gesamtpartei Stellung nahmen. Von einer anderen
Seite her behandelte am 23. August 1905 in einer von 3—4000 Personen
besuchten Versammlung im Feenpalast die Frage ». R. Friedeberg. Er
entwickelte in einem Vortrag über „Weltanschauung und Taktik des deutschen
Proletariats" einen antiparlamentarischcn Sozialismus, den er als „Anarcho-
Sozialismus" bezeichnete und dessen Aktionsprogramm in der Idee des
großen, umwälzenden Generalstreiks gipfelte. Eine in diesem Sinne
formulierte Resolution fand in der stark von lokalistischen Gewerkschaftlern
besuchten Versammlung Annahme, rief aber in sozialdemokratischen Kreisen
nun eine Ablenkung der Kampflinie in der Richtung gegen links hervor.
Noch vor dem Zusammentritt des Parteitags, der vom 17. bis 23. Sep-