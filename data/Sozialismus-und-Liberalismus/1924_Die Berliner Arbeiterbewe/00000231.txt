﻿218

Im 24. Bezirk war das Verhältnis ebenso beschaffen: der hier zum
erstenmal zur Wahl stehende Sozialdemokrat P. Schneider erhielt 2901
gegen 36 Stimmen, und mit gleichem Stimmenverhältnis waren schon 1901
-er 12., 34., 35., 36. und 48. Bezirk teils erobert und teils behauptet
worden. In den Stadtvierteln, wo die Arbeiterbevölkerung überwiegt, waren
Gegenkandidaturen gegen die Kandidaten der Sozialdemokratie aussichtslos
geworden.

Im Jahre 1904 wurden einige Veränderungen in den Personen not-
wendig. Am 14. April legte Dr. Kurt Freudenberg, durch hoch-
gradiges Leiden, dem er nur zu früh erlag, genötigt, und am 8. September
Dr. R. Friedeberg sein Mandat nieder. Für den ersteren ward am

31.	Mai 1904 im 40. Bezirk Dr. Leo Arons, für Friedeberg (22. Bezirk)
am 23. November Rob. Wenzels gewählt. Am 30. Mai erneuerten
außerdem die Wähler des 32. Bezirks mit glänzender Mehrheit das Mandat
von Karl Leid, dem in einem Prozeß wegen Majestätsbeleidigung zugleich
mit Verurteilung zu Gefängnis seine aus öffentlichen Wahlen hervor-
gegangenen Rechte aberkannt worden waren — vier Jahre später, als
das vom Daily-Telegraph veröffentlichte Pseudo-Interview Wilhelms II.
die nationalistischen Gefühle des deutschen Philisters verletzte, würde
die gleiche und gleichheitliche Anwendung dieses Strafmittels für
dasselbe Delikt sämtliche deutsche Vertretungskörper in Einsiedeleien ver-
wandelt haben.

„Acht Bezirke behauptet, in drei Bezirken das Mandat erobert und
in vier Bezirken der Sozialdemokrat in Stichwahl," das war das Ergebnis
der letzten Stadtverordnetenwahl Berlins, die noch in unsere Periode
entfällt: die Ergänzungswahl vom 8. November 1905. Die eroberten
Bezirke waren: 3. Bezirk Ioh. Sassenbach, 16. Bezirk Dr. I. Zadek,
20. Bezirk Waldeck Manasse. Behauptet wurden ohne Personenwechsel
der 15., 21., 26., 27., 33., 40. und 46. Bezirk. Im 38. Bezirk ward an
Stelle von Franz Gleinert, der sein Mandat niedergelegt hatte, Karl Mars
gewählt. Stichwahlen waren im 1., 6., 7. und 30. Bezirk nötig. Sie
endeten zwar überall mit dem Sieg der Gegner, brachten aber im 7. und
30. Bezirk die sozialdemokratischen Stimmen denen der Gegner sehr nahe,
im ersteren erhielt I. Sassenbach 1080 gegen 1351, im 7. Bezirk Rechts-
anwalt Jos. Lerzfeld 1312 gegen 1583 Stimmen der Gegner. Im 17. Bezirk,
wo die Nachwahl stattfand, die durch die Annullierung des Kerfinschen
Mandats nötig geworden war, wurden 997 gegen 1208 gegnerische Stimmen
abgegeben.

Insgesamt war das Stimmenverhältnis bei den vier Ergänzungswahlen
nach der Neueinteilung von 1899 das Folgende gewesen:

1899	Sozialdemokraten	26 877,	Gegner 19 588
1901		33 599,	„	10 249
1903		26 894,	„	8 854
1905	„	30 681,	„	10 573

Da m den gleichen Bezirken immer nur alle sechs Jahre gewählt wird
kann auch nur d.e Vergleichung der Wahlen vom sechsten Jahr nach jedem
der früheren Wahljahre e.nen genaueren Maßstab für das Anwachsen der
sozialdemokratischen Stimmen im Verhältnis der gegnerischen Stimmen liefern.