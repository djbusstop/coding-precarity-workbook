﻿286

c) Die Unterstützung auswärtiger Kampfgenossen.

Wie die sozialdemokratische Arbeiterschaft Deutschlands im allgemeinen
es stets als eine Ehrenpflicht betrachtet hat, ihren auswärtigen Brüdern
bei wichtigen Kämpfen mit Unterstützungen hilfreich zur Seite zu stehen,
so haben es ganz besonders auch die Berliner sozialistisch gesinnten Arbeiter
gehalten, und oft ist man in Berlin mit Sammlungen für solche Zwecke
vorangegangen. An ihnen beteiligten sich die politischen Organisationen
jeweilen nicht weniger rührig als die Gewerkschaften, aber aus Zweckmäßig-
keitsgründen wurden die Sammlungen für Kämpfe gewerkschaftlichen
Charakters von der Gewerkschaftskommission organisiert oder in ihren Länden
zentralisiert. Zn ihren Kassenberichten finden wir daher auch Zusammen-
fassungen über die Erträge dieser Sammlungen.

Nehmen wir die bedeutendsten davon heraus, so wurden für gewerk-
schaftliche Kämpfe an anderen Orten Deutschlands in Berlin gesammelt:

1.	Für den großen Lafenarbeiterstreik in Lamburg vom Winter
1896/97 zusammen 155 843 Mk. Auf dem Berliner Gewerkschastsbureau
wurden für diesen Streik 7000 Doppelquittungen ausgestellt, 962 Post-
sendungen in Empfang genommen und 411 Postanweisungen nach Lamburg
entsandt.

2.	Für streikende Weber in Krefeld, 1899, 25 901 Mk.

3.	Für streikende Weber in Meerane, 1902, 11 399 Mk.

4.	Für die streikenden Textilarbeiter in Crimmitschau und Umgegend,
1903, 179 000 Mk.

5.	Für die streikenden Bergarbeiter in Westfalen, 1905, 150 000 Mk.

6.	Für streikende Zigarettenarbeiter in Dresden, 1905, 10 200 Mk.

Von Sammlungen für gewerbliche Kämpfe im Ausland sind zwei be-
sonders zu erwähnen:

1.	Für den mehr als sechs Monate spielenden Kampf der englischen
Maschinenbauer von 1897/98 51 570 Mk.

2.	Für die von der großen Aussperrung des Jahres 1899 betroffenen
dänischen Arbeiter 18978 Mk.

Die Zahl der kleineren Sammlungen ist Legion. Auch darf man nicht
vergessen, daß neben diesen Sammlungen häufig allgemeine Sammlungen
für größere Kämpfe in bestimmten Gewerben Berlins liefen. So ward
z. B. im Jahre 1899 außer für die dänischen Arbeiter auch für die in einen
großen Streik verwickelten Steinarbeiter Berlins gesammelt, die mit
16 883 Mk. unterstützt wurden, und ferner wurden den Töpfern Berlins
15 000 Mk. und den Steinsetzern 4000 Mk. Anterstützung aus Mitteln der
Allgemeinheit gewährt. Im Zahre 1905 aber finden wir neben den
Sammlungen für die westfälischen Bergarbeiter und die Dresdener Zigaretten-
arbeiter u. a. noch die Sammlung für die streikenden Elektrizitätsarbeiter
Berlins mit einem Erttage von 138 683 Mk. Alles das im Verein mit
den nie pausierenden Sammlungen für politische Zwecke legt erhebendes
Zeugnis ab für das starke Solidaritätsgefühl und die stets offene Land
der Arbeiter Berlins.