﻿280

fln die arbeitende Bevölkerung
Berlins und der Vororte!

Der.Zwrck dieser ArdeitSkoseuzöhIung ist. eine mox
lichst gcliauc Fcststcllunq der Zahl der Arbeitslosen herbe -
zuführen, und zu beweisen, in welchem Mähe die Erwerbs
tätigkcit zurückgegangen ist. Daß ein.Notstand besteh:
wird van keinem einsichtsvollen Menschen bestritten Werder
können, jedoch sind nach den Meinungen der stödtisehen
und örtlichen Verwaltungen nicht genügend Beweise für
einen größeren Umfang der Arbeitslosigkeit erbracht
worden. Es ist notwendig, daß alle vom Arbeitsmangel
Betroffenen wal>rl)eitsgetreue Angaben über ihre ArbcitS-'
losigkcit oder Erwerbsbeschränkung machen, um gegebenen-
'alle bei dein Staat oder den Gemeinden begründen zu
können, daß Notstandsarbeiten in Angriff genommen
werben müssen, und daß eine Beschleunigung der prosek.
tierten und notwendigen Arbeiten erfolgt. Durch die Ar-
beitslosenzählung nn November vor,gen Jahres ist dir
Zahl der Arbeitslosen auch nicht annähernd festgestellt.
Das lag an dem verkehrten System, Das einzig richtige
System der Zählung, nämlich jeden Einwohner persönlich
zu befragen, lehnen die Behörden auS nichtigen Gründen
ab. Deshalb hat e8 die organisierte Arbeiterschaft selbst
übernommen. die Arbeit zu verrichten., um die arbeitende
Bevölkerung zu ihrem Recht zu verhelfen.

Wir wenden unL sinn an alle Volksschichte'.
Berlins und der Vororte mit der Bitte, uns bei dieser Ar-
beit, welche keinem zum Schaden ist. zu helfen, damit, ge-
stützt auf das eingegangene Material, an geeigneter Stelle
darauf hingewirkt wird, den Notstand zu mildern.

Die beste Unterstützung besteht darin, daß, jeder In-:
Haber girier Karte, gleichviel, ob er arbeitslos ist oder
nicht, dieselbe für sich, und lvcnn inchrere erwerbsfähige
Personen, in seinem Haushalt sind, für diese die Zähl»
karten gewissenhaft ausfüllt, und seine Nachbarn veran-
laßt. dasselbe zu tun.

Wir machen besonder» darauf aufmerksam, daß die
-Kamen und Wohnungen der Gezählte» zu keinerlei öffent-
liche» Zwecken oder zu sonstigen Unlauteren Mitteln ver-
wendet werden; diese sind nnr ans den Karten, um zu ver-
hindern, daß mehrere Zählkarten auf ein und dieselbe
Person ausgefüllt werden kann.

Es wird deshalb höflichst gebeten, diese Zählkarte
auszufüllen. Auck bitten wir. dafür zu sorgen, önß ary
<10 Februar Jemand zu Hause ist, der dem Zählenden
Auskunft geben kann resp. die Karte zurückgibt.

Wenn ln einem Haushalt mehrere erwerbsfähige
Personen wohnen, so ist'für -jede weitere Person -eine
-folgende Rubrik auszufüllen, reichen die- Rubriken nicht
aus. so wird gebeten. «>„.00 Februar vom Zähler eine
zweite Zählkarte zu fördern.

Unter erwerbsjähign.Pcrionen sind alle .in, Haus-
(}sllt Wohnende zu verstehen, welche für Geld arbeiten
gleichviel ob sie zur. Fymilie gehören oder nicht.

Unfall- und Jnvalidenrentncr. welche noch arbeits-
fähig sind, haben sich, wenn sie arbeitslos sind oder ver-
kürzt arbeiten, ebenfalls einzuzeichnen.

Arbeiter, welche durch die Naturverhältuissc zurzeit
arbeitslos siud. als wie Bauarbeiter. Schisser, Hafen- und
PollwerkSarb^itcr usw., haben ebenfalls ihre Arbeits-
losigkeit' anzugeben.

Die Zähler . sind angewiesen, alle zur Aufklärung
»dienende Fragen zu beantworten, auch-wird während der
Zeit voii,	Februar'in uusercmTBurcau.

von nwrgcnS 9— 1 und nachmittags von 6—8 Uhr. jeb*
webe Auskunft gegeben.

Die Alliier CrrotrMfljnffsbfrwiatsfam

Engel-Ufer 15.

128 und 129. Anweisung zur Aus-
füllung einer Zählkarte

nicht gemacht werden, da einige Listen
nicht rechtzeitig eingingen, ihre Zahl be-
lief sich auf zusammen zwischen 3500
bis 4000 Personen.

Das Zählungswerk selbst, das
mehrere hundert Personen beschäftigte
und bei dem durchwiederholteKontrolle
die größte Zuverlässigkeit zu erzielen ge-
sucht wurde, ergab für Berlin und die
Vororte nicht weniger als 77051 Ar-
beitslose, wovon 4256 am Zählungs-
tage in Asylen und verbergen Untcr-
kunst hatten. Außerdem ward festgestellt,
daß 53098 Personen nur zu ver-
kürzter Arbeitszeit gegen entsprechend
geringere Bezahlung Arbeit hatten.
Der ihnen dadurch erwachsene Aus-
fall berechnete sich auf zusammen
694 576 Arbeitsstunden pro Woche,
während für 72795 gänzlich Arbeits-
lose ein Ausfall an Beschäftigung von
789 019 Wochen festgestellt wurde.
8603 Personen waren im vierten, 3763
im fünften, 2360 im sechsten Monat
und 2416 Personen zwischen sechs
bis neun Monate beschäftigungslos.
Von den 57222 männlichen Arbeitern,
die in Berlin und, mit Ausnahme
Charlottenburgs, dessen Aufstellung
hierüber keine Summierung gibt, in
den Vororten gezählt wurden, waren
8032, das heißt noch nicht der siebente
Teil unter 2l Jahren, während diese
Altersgruppen in der Industrie den
vierten Teil der Arbeiterschaft aus-
machen. Noch geringer war ihr Anteil
an der Zahl der auf verkürzte Ar-
beitszeit Gesetzten, nämlich 2613 von
42245 oder gerade ein Sechzehntel.
Dagegen hatten mehr als die Lälftc
der Arbeitslosen das 31. Lebensjahr
überschritten und 34458 von ihnen
waren Laushaltungsvorstände mit zu-
sammen über 44000 Kindern. Es sind
also gerade die vorgerückteren Alters-
klassen, die im Verhältnis am stärksten
von der Krise erfaßt werden. Ein
Llmstand, dessen schwerwiegende Be-
deutung ohne weiteres in die Augen