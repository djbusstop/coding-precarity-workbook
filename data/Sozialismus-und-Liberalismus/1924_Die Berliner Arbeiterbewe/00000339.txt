﻿325

frage, die Brauereihilfsarbeiter aber, die mit der allgemeinen Arbeiterbewegung
weit stärkere Fühlung hatten, als die noch stark zünstlerisch denkenden
Gesellen, den Brauereien schon dadurch allerhand Zugeständnisse abringen
können, daß diese sich nicht gern mit der Arbeiterschaft überwerfen mochten,
deren Bedeutung für den Konsum des in Berlin gebrauten Bieres ihnen
ganz besonders beim großen Saalboykott des Jahres 1889 sehr eingehend
zum Bewußtsein gebracht worden war. Aus diese Tatsache gestützt, hatten die
Brauereiarbeiter seit jenen Jahren wiederholt bei Streitigkeiten mit Brauereien
den Boykott ins Spiel gebracht und meistens auch mit ihm oder vielmehr
schon mit der bloßen Drohung, ihn ins Spiel zu bringen, Erfolge erzielt.
Die Brauereien, denen das immer lästiger wurde, riefen zur Abwehr neben
dem schon genannten Verein noch einen „Schutzverband gegen Boykott-
schäden" ins Leben, was aber auf die Arbeiter vorderhand noch keinen
tieferen Eindruck machte. Ein felsenfester Glaube an die Allmacht des
Boykotts im Braugewerbe hatte sich der Köpfe bemächtigt und wurde noch
durch die Kenntnis des Umstandes gestärkt, daß die scharfe Konkurrenz, die
sich die Brauereien untereinander machten, zu Gegenströmungen gegen die
großen unter ihnen geführt hatte, so daß eine ganze Anzahl von mittleren und
kleineren Brauereien dem Verein oder „Ring", wie man ihn gern nannte,
ferngeblieben waren. An der Spitze dieser Gegenbewegung stand die
Brauerei „Münchener Brauhaus", deren Direktor Arendt in dem zu
beschreibenden Kampf eine größere Rolle spielen sollte.

Den Anstoß zu diesem Kampf gab ein Konflikt wegen der Maifeier
des Jahres 1894. Etwa 300 Böttcher, die am 1. Mai feierten, wurden
von den Ringbrauereien auf einige Tage ausgesperrt, worauf am 3. Mai,
vormittags, in einer großen Böttcherversammlung beschlossen wurde, in den
Generalstreik einzutreten und die Wiederaufnahme der Arbeit von der Ver-
kürzung der Arbeitszeit auf 9 Stunden, derFreigabe des l.Mai alsFeiertag, der
Verpflichtung zur ausschließlichen Entnahme von Arbeitern aus dem Arbeits-
nachweis der Gewerkschaft und der Bewilligung von Lohnerhöhungen
abhängig zu machen, die flch zwischen 10 und 15 Proz. bewegten. Eine
große, aus Sonntag, den 6. Mai veranstaltete Versammlung der Brauerei-
hilfsarbeiter erklärte sich einstimmig mit dem Vorgehen der Böttcher ein-
verstanden, machte es allen Hilfsarbeitern zur Pflicht, jede Böttcherarbeit
zurückzuweisen, und verpflichtete sich ferner, die Böttcher in ihrem Kampf
pekuniär zu unterstützen. Auch ward beschlossen, an die Brauereidirektionen
behufs Bescheid auf im April gestellte und unbeantwortet gebliebene Lohn-
forderungen heranzutreten.

Wie in dieser Versammlung der Referent Winter bekanntgab, hatte
schon am gleichen Tage vormittags eine in Rixdorf vom dortigen
Gewerkschaftskartell einberufene Volksversammlung beschlossen, wegen der
Maßregelung der Böttcher über die dem Brauerring zugehörende Rix-
dorfer Vereinsbrauerei den Boykott zu verhängen.

•Unter den erfahrenen Vertretern der Arbeiterbewegung Berlins sagte
man sich sofort, als der Beschluß bekannt wurde, daß er mindestens eine
große Voreiligkeit war und für die Allgemeinheit Folgen nachziehen
könne, über welche diejenigen, die ihn gefaßt, schwerlich sich Rechenschaft
abgelegt hatten. Die Redaktion des „Vorwärts" nahm aus diesem Grunde
Anstand, den Bericht über die Versammlung in derjenigen Form zu