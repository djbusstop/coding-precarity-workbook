﻿stimme geben, wenn Ihr wollt, bas; Gurr
Interessen wirksam und machtvoll vertreten Werden.
Die Sozialdemokratie hat es sich überall zur
Aufgabe gemacht, den schiveren Kamps ans alle»
Erbieten der Nolboniohlsahrt zu sichren. In
unseren vorhergehenden Flugschriften haben wir
bereits klar gelegt, was sich die Sozialdemokratie
als Ziel gesetzt hat und was die Sozialdemokratie
in unserem Stadlpartamcnt geleistet hat! Jeder
prüfe selbst, ob daraus der Vorwurf zutrifft, den
uns die, „Liberalen" machen: „Wir treiben einen
iiserloseir Radibaliomus und lassen uns nur
vom Neid der besitzlosen Klasse» leiten,"

Wir treten für das Wohl der Allgemeinheit
ein! Um das zu erreichen, ist eS aber notwendig,
daß von unten heraus angefangen wird, und
den Äetitzlose» daS gegeben ivird, was ihnen
heute von der bürgerlichen Gesellschaft vorent-

halten wird. Kein bürgerlicher Kandidat ist frei
und unabhängig genug, um imstande zu sein;<.nn
das Wohl der grotzen Mehrheit der Schöneberger
Einwohnerschaft das ztt tun, Niao die Sozial-
demobratie ans ihre sahne geschrieben hat.

Sein Arbeiter, Handwerber und Klein-
gewerbetreibender darf am Sonnabend von der
Wahl zurückbleiben: Ein jeder erscheine an der
Wahlurne und gebe seine Stimme nur für die
soziaidemobratischen Kandidaten ab. Lasse sich
niemand von der Ausübung seines Wahlrechts
zurückhalten. Es kommt aus jede Stimme an.
Eine einzelne Stimme kan» unter Umständen den
Ausschlag geben, denbe beiner, es werde auch
ohne ihn geizen. Bedenke ein jeder, dah von
gegnerischer Seite alles mögliche ansgeboten
ivird, um der Sozialdemokratie den Sieg zu vcr<
eilein.

rlüttelt jeden Säumigen aus und gebt Eure Stimme de» Kandidaten der Sozialdemokratie:

Schriftsteller Eduard Bernstein,

Mechaniker Richard Gabriel.

Die Wahl findet statt am

Sonnabend, den 2.November, von vormittags 11 bis abends 8 Uhr.

Ihr Wahllokal befindet sich:

bderssir. S, Turnhalle der Gemeindeschule.

Ufer nicht bis 8 ühr im Aahllokal anwesend Mt, geht feines Wahlrechts verlustig.

Als srgitiraation bringe ein jeder die mm Magistrat zugesandte Whlrrkarle mit, wes nicht in den fiept; einer
laschn, gelangt ist, der uersrhe pch mit einer grniigendrn anderen fegitiinntion |Strnrr;rttrl, lüüitärpnpirrr usw.j.

Wahlberechtigt ist jeder, der i» der im August v. I. a»sgestellte» Wählerliste dieses
Bezirks eingetragen ist, auch wen» er während dieser Zeit auS dem Bezirk verzogen ist.

Das sozialdemokratische Wahlkomitee.

Freitag, den 1. November, abends 8 Uhr:

8 öfjentl. Komtnunaiwähier-Versamttilungen

in Obit’s Westfalen, ffleiningerftr. 8;

Cindenparh, FjauptUr. 13; Whelmshof. Ebersltr. 80.

Tagesordnung:

Die Aufgaben der Sozialdemokratie in der Gemeinde.

Referenten:

Stadtverordn. DttjtSnl-Berlin; Stadlverordn. Schubert-Berlin; Reichstagsabgeordn 2tlde»»BerIin.
Zum Besuche einer dieser. Versammlungen sind Sie hiermit freundlichst eingeladen., "Wf8

V.randvottlich: Ae kl WoIIer mann. SchSnedere. Martin Lulhrrsir. HO. — Druck: Ar-rw.i.tü «uchdu/tsnei uild BerlohSr,nilalt Baut k.in^er fi ^04'JBi'tün SWlW.

100 und 101. Allgemeines sozialdemokratisches Flugblatt an die Wähler der

dritten Abteilung