﻿136—138. Protokoll des als Einigungsamt angerufenen Gewerbegerichts

17. Februar 1896

anderer Berufe das Gewerbe des Zwischenmeisters aus. So wenig man
als Fabrikant nötig hatte, selbst die Nadel führen zu können, so wenig
war die Aufgabe des Zwischenmeisters daran gebunden. Spekulations-
talent und je nachdem ein gewisses organisatorisches Geschick waren in
beiden Fällen die hauptsächlich erforderten persönlichen Eigenschaften, und
so fand man gewesene Kommis, Gastwirte, Gärtner — ja, selbst ehemalige
Dienstmänner und Droschkenkutscher unter den Personen, die zwischen
Fabrikanten und Arbeitern der Konfektionsgewerbe die Mittelsperson
spielten. And gerade diese aus anderen Berufszweigen stammenden Zwischen-
unternehmer waren es oft, die den hier ohnehin starken Lohndmck noch
empfindlich steigerten.