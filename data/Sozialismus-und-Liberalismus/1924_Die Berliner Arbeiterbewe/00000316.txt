﻿302

eine Resolution vorgelegt, durch die sie erklären sollten, nun in den Streik
zu gehen, für den die Unternehmer die volle Verantwortung trügen, und
die Arbeit so lange ruhen zu lassen, bis ihre sämtlichen Forderungen
bewilligt seien. Von den Forderungen aber ward die der Errichtung von
Betriebswerkstätten und der Bewilligung wesentlich erhöhter Löhne in den
Vordergrund gestellt. Der Fünferkommission ward die weitere Führung
des Kampfes und der eventuellen Verhandlungen übertragen, und der
Agitationskommission wurde aufgegeben, die Agitation und das Interesse
für den Kampf bei den Fachgenossen und außerhalb des Kreises dieser
wachzuhalten. Die Versammelten aber versprachen ihrerseits alles daran-
zusetzen, den aufgezwungenen Kampf zum siegreichen Ende zu führen. Nach
einem Appell an diejenigen, welche bisher den Arbeitern ihre Sympachie
in warmen Worten ausgedrückt hatten, es nun auch an der Tat in Gestalt
von Unterstützungen nicht fehlen zu lassen, erklärte die Resolution noch:

„Die Anwesenden verlassen sich aber nicht auf Anterstützungen, sie
erklären, in diesem großen Kampfe freudig alle Entbehrungen auf sich
nehmen zu wollen, um endlich ihre Lage zu bessern.

Die Schneider und Näherinnen müssen wegen der ungeregelten Pro-
duktionsweise oft monatelang darben und hungern, sie werden es in
diesem Kampfe ohne Murren auf sich nehmen, um zum Ziele zu gelangen.

Die Anwesenden verpflichten sich mit aller Kraft und Lingebung zum
Siege in diesem Kampf beizutragen."

Der Andrang zu den Versammlungen, für die inan möglichst große
Säle genommen hatte, war ein ungeheurer. Fast ausnahmslos mußten sie
lange vor Beginn wegen Überfüllung abgesperrt werden. Eine Welle von
Poffnungsfrcudigkeit und Kampfesmut hatte nunmehr die so gedrückten
Heimarbeiter und Heimarbeiterinnen erfaßt, wie man es vorher für un-
glaublich hätte halten mögen. Mit stürmischen Ausrufen der Kampfes-
stimmung wurden die Referate entgegengenommen, mit jubelnder Begeisterung
überall einstimmig die vorgelegte Resolution beschlossen.

Es waren keine leeren Beschlüsse, dem Wort folgte alsbald die Tat.
Schon am nächsten Tage stellten 6000 Arbeiter und Arbeiterinnen die
Arbeit ein, am nächsten Tage war die Zahl nahezu verdoppelt und stieg
von Tag zu Tag bis auf zwischen 20 und 30000. Unterstützt von der
Agitationskommission und der Berliner Gewerkschaftskommission hatte die
Fünferkommission den Streik in wahrhaft großartiger Weise organisiert.
Dreißig Kontrollbureaus waren mit einem Schlage in den verschiedenen
Stadtvierteln Berlins eingerichtet und hielten die Zenttalstelle über alle
Vorgänge von Wichtigkeit für die Beurteilung der Kampfsituation auf dem
laufenden. Jeden zweiten Tag ward ein Flugblatt über den Fortgang
des Kampfes in 130000 Exemplaren mit jener Promptheit verbreitet, die
das Geheimnis der organisierten Arbeiterschaft ist, Sammellisten wurden in
großer Zahl angefertigt und in Umlauf gesetzt, und allerlei Versammlungen
sorgten für einen regen Gedankenaustausch der Kämpfenden unter sich und
mit der organisierten Arbeiterschaft anderer Berufe.

Diese letztere hatte selbstverständlich die Bewegung mit lebhaftem
Interesse verfolgt. Die Gewerkschaftskommission war von den Leitern der
Bewegung über alle wichtigen Maßnahmen rechtzeitig unterrichtet worden,
und am Vorabend des Kampfes, den 31. Januar, hatte die Delegierten-