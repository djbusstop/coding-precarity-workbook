﻿32

nicht weniger als das Recht, Vereine oder Versammlungen, deren „Zweck
oder Tätigkeit den Strafgesetzen zuwiderläuft oder die öffentliche Sicher-
heit, insbesondere die Sicherheit des Staates oder den öffentlichen
Frieden gefährdet", aufzulösen, bezw. zu schließen. Mit diesen Kautschuk-
bestimmungen hätte es ganz von dem guten Willen der Regierung ab-
gehangen, jeder Oppositionspartei, ob bürgerlich oder sozialistisch, das
Wirken in geregelten Vereinen unmöglich zu machen, zumal Beschwerden
nicht vor den ordentlichen Gerichten, sondern im Verwaltungsstreit-
verfahren ihre Erledigung finden sollten. Daß dieses ungeheuerliche Gesetz
noch nicht ein Jahr später im preußischen Landtag hatte eingebracht werden
können, nachdem im deutschen Reichstag der Reichskanzler die Freigabe
der Verbindung politischer Vereine für das ganze Reich in Aussicht gestellt
hatte, und daß dieses Gesetz, welches die Freigabe der Verbindung politischer
Vereine zum blutigsten Lohn geinacht hätte, sehr nahe daran war, im
Landtag angenommen zu werden —seine Ablehnung kam schließlich mit
nur vier Stimmen Mehrheit zustande! — dieser Vorgang war es, der in
der Stellung der Sozialdemokratie zur Frage der preußischen Landtags-
wahlen eine vollständige Amwälzung zustande brachte. Run erkannte ein
großer Teil ihrer führenden Vorkämpfer, Auer und Bebel voran, daß die
bis dahin gepflegte Enthaltungspolitik nicht mehr fortgesetzt werden dürfe,
sondern daß man, statt in Presse und Versammlungen zu protestieren, dem
Landtag direkt zu Leibe gehen müsse, und es kamen nach lebhaften Debatten
in Presse, Versammlungen und Komitees die bezüglichen weiter oben ver-
zeichneten Beschlüsse der Kongresse von Lamburg und Stuttgart zustande,
welche die Beteiligung der Sozialdemokratie an den Landtagswahlen in
Preußen zur Folge hatten.

Im Spätherbst 1897 legte Lerr Tirpitz seinen Flottenplan dem Reichs-
tag vor. Danach sollten nicht weniger als 7 neue Linien- (Schlacht-)
Schiffe, 2 große und 7 kleine Kreuzer zu den schon bewilligten Schiffsbauten
hinzukommen, neben allerhand Ersatzbauten und Verstärkungen des Torpedo-
bootbestandes, dazu sollte das Friedenspersonal der Flotte um nahezu die
Lälste, nämlich von 18 138 auf 26 637 Mann, erhöht werden. Für die
Fertigstellung der Bauten waren 7 Jahre vorgesehen. Das alles wurde vom
Reichstag bewilligt; mit Ausnahme der Bayern und eines halben Dutzend
anderer Widerspenstigen stinnnte das Zentrum für die Vermehrung, die die
einmaligen Ausgaben für die Flotte um nicht weniger als 408 Millionen
Mark erhöhte und für die fortdauernden Ausgaben des Marineetats eine
Steigerung von jährlich 49 Millionen Mark vorsah. Ja, man ging so
weit, die Periode für die Bauten auf ein Jahr weniger anzusetzen, als die
Regierung verlangt hatte, nämlich auf sechs statt auf sieben Jahre, was
die Ausgabe pro Jahr entsprechend erhöhte. And dies, obwohl es die
Spatzen von den Dächern pfiffen, daß die Vorlage nur erst den Anfang
des Programms der Regierung brachte. So sehr hatte die Stimmung
umgeschlagen. Das Zentrum war Regierungspartei geworden.

Am 28. März 1898 war die Schlußabstimmung über das Flotten-
gesetz erfolgt und am 6. Mai ward der Reichstag, dessen Mandat abge-
laufen war, geschlossen und der Termin für die Neuwahlen auf den 16. Juni
1898 angesetzt. Der nun entbrennende Wahlkampf fand die Sozialdemokratie
in jeder Linsicht gerüstet. Sie verfehlte nicht, der Arbeiterschaft klar-