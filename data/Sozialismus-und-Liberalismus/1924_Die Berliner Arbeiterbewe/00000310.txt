﻿Eine Kommission von fünf Personen sollte das für die Aktion er-
forderte Agitationsmaterial beschaffen und zusammenstellen, sowie mit den
Kommissionen an den einzelnen Orten behufs einheitlichen Vorgehens in
Agitation und Aktion Fühlung halten. Sie ward am 21. Januar 1895
in Berlin in öffentlicher Versammlung gewählt und aus drei männlichen
Berufskollegen — O. Brand, an dessen Stelle später K. Riesing trat,
Ioh. Timm und A. Zander — und zwei Arbeiterinnen — Frau A. Noack
und Frau E. Reimann — zusammengesetzt. Sie beschloß zunächst die
Forderung von Betriebswerkstätten zu propagieren. Am 6. Mai 1895
fanden in Berlin und den anderen Sitzen des Gewerbes große Versamm-
lungen statt, in denen die Wichtigkeit der Errichtung von Betriebswerkstätten
auseinandergesetzt wurde. Äberall wurden Resolutionen zu ihren Gunsten
angenommen und mit entsprechenden Begleitschreiben von den Bureaus
der Versammlungen an die Unternehmer, Fabrikanten wie Ländler versandt.
Gemeinsam mit dem Vorstand des Schneiderverbandes beauftragte die
Fünferkommission ihr Mitglied Johannes Timm, behufs Gewinnung der
öffentlichen Meinung eine Broschüre über die Schäden des Zwischenmeister-
systems und der Leimarbeit im Gewerbe abzufassen. Die wirksam ge-
schriebene Arbeit ward unter dem Titel „Das Swcating-System in der
Konfektionsindustrie" in 6000 Exemplaren ausgelegt und unter anderem den
meisten namhaften Sozialpolitikern und den Organen und Körperschaften
zugeschickt, die von Amtswegen mit der Sozialpolitik zu tun haben. Sie
erfüllte auch in hohem Grade ihren Zweck. Die Presse befaßte sich aufs
neue lebhaft mit der aufgeworfenen Frage und sprach sich im ganzen für
die in der Broschüre entwickelten Ideen aus. Noch waren die Blüten-
träume der deutschen Sozialpolitik nicht aus den Gemütern der deutschen
Ideologen verflogen, die jüngeren Elemente der evangelischen Geistlichkeit
und die antikirchlichen Ethiker wetteiferten im Eintreten für soziale Reformen,
im Schoße der liberalen Parteien gab es Gruppen, die sich mit der Idee
des staatlichen Arbeiterschuhes befreundeten, und da die großen Firmen
der Kleiderkonfektion überwiegend jüdisch waren, nahmen auch Antisemiten
und Konservative für deren Arbeiter Partei. Kurz, die Agitation der
Konfektionsschneider fand selbst in der bürgerlichen Welt viele Freunde.
Der tatkräftigen Sympathie der organisierten Arbeiter dursten sie sich selbst-
verständlich versichert halten. Die Leitung der deutschen Sozialdemokratie
sehte die Frage der Schwitzarbeit mit Johannes Timm als Referenten auf
die Tagesordnung des Parteitags, der im Oktober 1895 in Breslau zu-
sammentrat, und nahm einstimmig eine Resolution an, die es den Genossen
allerorts zur Pflicht machte, den Kampf für die Beseitigung des Zwischen-
meistersystems, für Betriebswerkstätten und feste Lohntarife zu unterstützen.
Am 24. und 25. November 1895 tagte in Erfurt eine zweite allgemeine
Konferenz der Arbeiter der deutschen Konfektionsindustrie. Sie war er-
heblich stärker besucht als die erste, erneuerte das Mandat der Fünfer-
kommission mit dem Recht der Ergänzung und beschloß einen genau
fixierten Agitationsplan, sowie die sofortige Organisierung von Sammlungen
behufs Bildung eines Kampffonds.

Es handelte sich also nicht um einen willkürhaft und unüberlegt an
einem einzelnen Ort ins Werk gesetzten Kampf, sondern um eine reiflich
überdachte und wohl vorbereitete Bewegung von nationalem Amfang, deren