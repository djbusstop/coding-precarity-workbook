﻿Dies«- Blatt erscheint wöchentlich einmal, wird den
organisirten Schneidern und Schneiderinnen gratis
zugestellt, lieg« in über 500 Restaurants init
Schneiderverkehr aus und wird in allen Versammlungen
verbreitet.

Anzeigen-Watt

der

Inserate: Die btzrspälten» Petilzeile oder^ deren
Raum 25 Pf. Bei mehrmaliger Aufgabe entsprechender
Rabatt. Aufträge werden entgegengenommen: Vorm,
non 8—1 Uhr un CeittralarbeitsnachweiS- und AuS»
kunstS-Bureau der Schneider u. Schneiderinnen Berk..
Alte Jakobstr. 83, Rest. Pasch, bei Albert Aon des.

Maeitfet ums Sdmetdeciaaea

Herlins und Umgegend.

Ubonnem-ntSpreis pro Quartal frei ins 75 Pf., pro Monat 30 Pf.

Abonnemmts nimmt rntgcgrn: A. Zondrr, Alte Jalob-Strasr 83, Restaurant Pasch, Borm. van 8—1 Uhr.

II. Jahrgang.

Berlin, Sonnabtnb, de» 25. Januar 1896.

Nr. 4.

Kollegen! Kollkginnen! Verbreitet dieses f latt überall in Euren Kreisen und ngitirl s. d. Besuch d. Nersannnlungen!

12 große öffentl. Versammlungen

der

Mantelnäherinncii, Bügler, Zuschneider, Stepper. Stepperinnen, Kinderkonfektions-
Nätzcnnnen, Herrenkonfektions-Schneider und -Näherinnen, sowie Schneider und
Näherinnen aller Branchen der Konfektion« - Industrie
W am Montag, den 3. Februar, Abends 8 Uhr. "VE
Für den Osten:	Nicft's Festsälc, Wcbcrstr. 17.

Für C-ntmm:	Englischer Garten^ Al^anderstr. 27 e.

Für den Nord-Osten: Paster's Gescllschaftshaus, Nene Königstr. 7.

Für den Süd-Osten: Sanssouci, Kottbuscrstr. 4s und

Märkischer Hos, Admiralstraße l8o.

Für den Westen: Kvmgshvs, Bülvwstr. ?>7.

Für den Norden: Brrtmer PrM«.	7.

Slmticmunöcr @csetlfd)aft0ljau0. SrnmcmimOcrftc. 39 nn»
Criindel's Fcstsäle, Bmnnenstr. 188,

Für Wedding-Gesund- Köslincr Hof, Küslincrstr. 8.
brunnen:

Für Moabit:	Arendt'sche Brauerei, Stromstr. 11—16.

Für Rixdors:	Diktoria-Säle, Hennannstr. 48—50.

Täges-Ordnung in allen Versammlungen:

1.	Die Ergebnisse der Verhandlungen init den iionfektions.
Unternehmern, Händlern und Meistern und unsere Stellung-
nahme dazu.

2.	Diskussion und Beschlußfassung darüber.

Die Referenten werden in den Versammlungen bekannt gemacht.

Kollegen, Kolleginnen' Erscheint in den Versammlungen vollzählig. Es gilt,
darüber -zu entscheiden, ob Ihr gewillt seid, in eine Lohnbewegung einzutreten.

Die Agitations-Kommission der Schneider und Schneiderinnen Berlins.

Im Auftrage: Z. tzimm, Vertrauensmann.

Montag, dc» Z. Februar

ist der Tag, an welchem die Entscheidung getroffen
werden soll, ob in eine Lohnbewegung eingetreten
wird. Die Agitationskommission der Schneider und
Schneiderinnen Hot den Auftrag der letzten großen
Versammlungen ausgeführt und den Unternehmern.
Händlern und Meistern die angenommenen Forder-
unge« zugestellt: in den bekannt, gegebenen Ver-
sammlungen wird sie-über doS Ergebniß der ge-
pflogenen Verhandlungen, zu denen sie beauftragt
wurde. Bericht erstatten.

Wir glauben nicht nöthig zu haben, die Kollegen
und Kolleginnen noch besonders daran zu erinnern,
daß e8 ihre Pflicht ist. in diesen Versammlungen
zu erscheinen, um mit zu berathen und mit zu
bestimmen, was geschehen soll. Es ist weiter ihre
Pflicht, alle Säumigen, alle Gleichgültigen wach-
znrütteln. damit sie die zu ihrem eigenen Wohle
dienenden Bestrebungen unterstützen. Laßt Euch
nicht abhalten von Jenen, die ans wohlberechneiem
Jntereffe die Forderungen als undurchführbar hin-
stellen. die mit der Hungerpeitsche drohen, wenn
Ihr Euch nicht fügt. Tretet. ein in den Kampf
mit dem Gedanken, daß Ihr Euer gutes Recht aus
menschenwürdige Existenz verfechtet. Müßt Ihr
in der schlechten Zeit wochenlang, monate-
lang hungern, vergebens um Arbeit betteln,
so könnt Ihr jetzt, wo es sich darum handelt,
bessere Zustände zu erringen, einige Wochen
Ovfermulh bezeugen, um die Forderungen
durchzusetzen. Geschloffenheit führt zum Sieg.

Rur die in diesem Blatt bekannt gegebenen
Versammlungen sind von der beauftragten Agitations-
kommission einberufen. Es wird in dem bevor-
stehenden Kampfe nicht an Leuten fehlen, welche
die einheitliche Bewegung zu zerstören suchen und
bewußt oder unbewußt wie immer im Interesse der
Unternehmer handeln. Die beste Antwort darauf
ist. daß- man solchen Veranstaltungen fern bleibt.

Die Anfnahmclisten de« Verbandes deutscher
Schneider und Schneiderinnen sind weiß, ebenso
die' Sammellisten, der Agilationskommission. Wir
bitten dieses zu beachten, weil ander« Sammlungen
als gestempelte Bons u- s. w. nicht von uns ausgehen.

Durch Kampf zum Sieg!

Die Agitations-Kommission
der Schneider und Schneiderinnen Berlins.

Unsere Forderungen und der „Sun-,
fckttonär."

Der „Konfektionär" ist bekanntlich das Inter-
essen-Organ der Unternehmer und Händler der
Konfektions-Industrie. Dieses Blatt hat also den
Profit der großen und größten Kapitalisten -wahr-
zunehmen. Das hindert den Konfektionär aber
nicht, ab und zu auch einmal die soziale Lage der
Schneider und Näherinnen zu beleuchten, allerdings
nur dann, wenn es höchst ungefährlich ist. Die
Unternehmer schlagen dann gleich zwei Fliegen mit
einer- Klappe: sie wahren ihr Kapitalinfereffe und
geben sich einen sentimentalen arbeUerfrcundlichen
Anstrich.

So schreibt noch der „Konfektionär" im vorigen
Jahre folgendes: „Die Mäntelnäherin verdiente
früher viel: augenblicklich aber giebt es für ein
Jcnzget nur noch 90 Pf., oft noch weniger, für
einen Regenmantel l.25 M.'; und an anderer
Stklle: „Die Frauenarbeit ist von jeher schlecht
be^hlt worden, selten waren aber die Löhne so
gesunken wie jetzt."

Im Anschluß hieran heißt es dann: „Eine
allgemeine Lohnerhöhung könnte nur durcbgeführt
weiden, wenn unsere Geschäftsinhaber wüßten, daß
Niemand billiger arbeitet, daß ein einheitlicher
Minimal-Arbeltstarif bestehe. Dann würden
sic gewiß gern bereit sein, höhere Löhne zu bewilligen,
denn alsdann müßte auch der Preis für das fertige
Fabrikat steigen, den durchzusetzen, unter den
obwaltenden Umständen nicht gar zu schwer wäre."

Jetzt rückt der Termin heran, wo die Ver-
wirklichung dieser Forderungen erstrebt werden soll.
Die logische Konsequenz der obigen Ausführungen
wäre, daß auch die Unternehmer mit aller Macht

i VW '^ -vcVernnrun unterstützen MÜsit>2'.
f ans feuert 3fce(fcn sa)un &(latmen laut, baß (Id)

die Anfertigung der Stapclfachen kaum noch ver-
lohne, weil die Preise so heruntergedrückt sind.
So konsequent sind die Unternehmer aber nicht,
denn „zwei Seelen wohnen ach in einer Brust".
Wenn auch die elenden Zustände dkr Konfektions-
schneider und Näherinnen eine deutliche Sprache
reden, so wird sie übertönt von dem keifenden
Schrei: Profit! Profit!

Wuthentbrannt 'über den imposanten Ausfall
unserer letzten Versammlungen speit der „Konfektionä "
Gilt und Galle. Daß er die im Vordergründe
der Bewegung stehenden Kollegen mit allerhand
Ehrentitel belegt, ist aus der jetzigen unbequemen
Verlegenheitsstimmung einigermaßen begreiflich und
genirt weiter nicht. Die Zahl der Versammelten
wird vorsichtiger Weise auf nur 5000 angegeben,
während die Lokale Sanssouci und Keller, die bis
ans den letzten Platz besetzt, allein schon 5000
Personen fasten. Die seltene Einigkeit in der
Beschlußfassung soll dadurch beeinträchtigt werden,
daß einige Zwischenmeistcr und Meisterinnen dagegen
opponirten. Diese werden natürlich als „Arbeiter"
ausgespielt. Es wird dann noch über große „Un-
ruhe" und heftige Opposition gegen die aufgestellten
Forderungen berichtet. Der Bericht läßt aber
deutlich die unangenehme Situation der Unter-
nehmer erkennen. Nun, sie können beruhigt sein,
der einmal beschrittene Weg wird weiter verfolg
bis die zu einer menschenwürdigen Existenz erfor-
derlichen minimalen Forderungen bewilligt sind.

An's werk.

Die organisirten Schneider und Näherinnen
Deutschlands haben die zum Besten der Konfektions-
arhciterschast bereits im vorigen Jahre eingeleitete
Bewegung einen weiteren, entscheidenden Schritt
vorwärts geführt. Gemäß der Anregungen und
Beschlüsse der Berliner Konferenz vom. 13. Januar
und der Erfurter Konferenz vom 24. und 25. No-
vember 1895 fanden am letzten Montag in fast
allen Mittelpunkten der Konfektionsindustrie große
öffentliche Versammlungen statt, in welchen die
Arbeiter und Arbeiterinnen der Branche Äe Er-
richtung von Betriebswerksiätien und die Einführung
fester Lohntarife forderten. Die Forderungen
kommen dem Unternehmerthum nicht überraschend,
die. gewerkschaftliche Aktion bedeutet ihm keine
Ueberrumpelung. Bereits im Januar vorigen
Jahres erklärten die Vertreter der Konfektions-
arbesterschaft die Errichtung von Betriebswerkstätten
für eine dringend nöthige Reform, und die Massen-
versammlungen vom 6. Mai pflichteten durchaus
dieser Ansicht bei, ließen den Konfektionären ihre
diesbezügliche Forderung zur - Kenntniß bringen
und setzten den 1. Februar 1696 als Termin für
die Einführung, der Neuerung fest. Was aber

das Verlangen nach höheren Lohnen und festen
Lohntarifen anbetrifft, so ist es seit Jahren nie
verstummt, hier und da. nun und jetzt wieder und
wieder formulirt worden.

In brennender Dringlichkeit wachsen, die er»
hobenen beiden Forderungen ans den Arbeits-
bedingungen der Konfektionsarbeiterschaft hervor;
aus Arbeitsbedingungen, welche mit eisernein. Druck
die Lebenshaltung vieler Zchntausende auf einem
denkbar niedrigen, kiilturunwürdigen Niveau fest-
klammern. Und- bieSi während einer Handvoll
Konfektionären millionenrcicher Gewinn zufällt, eine
mehr als auskömmliche Existenz anderen ihres*
gleichen, milfammt einer Anzahl besonders geriebener
Zwischenmeistcr. Welches sind denn die vor»
stechendsten Züge dieser Arbeitsbedingungen?

Löhne, die an der Hungergrenze hin- und her»
pendeln und sehr oft unter dieselbe sinken. Eia
nicht bloß kärglicher, sondern im höchsten Grade
unsicherer und unregelmäßiger Verdienst, der aus-
bleibt und sDwankt, je nach der Saison, den
Mlri-tito-.r.dtniffeu und dem willkllrULeu Bel'Herr

1 bet Stonfcttionäie unb ßmifrijfant elfter. &n an’

haltendes Sinken der Löhne, ganz besonders iu
Folge der unheilvollen Rolle, welche der Schwitzer
als Werkzeug der Ausbeutung und Ausbeuter
spielt. Ungemeffen lange und ungeregelte Arbeits-
dauer. ohne feste Abgrenzung der Zeit für Essen,
Schlafen, Erholung und Erfüllung der Aufgaben
gegenüber Familie, Klaffe, Gesellschaft. . Eine
Beschlagnahme der Nachtstunden, der Sonn- und
Feiertage während-der Saison, ein fast oder ganz
vollständiges Feiern während der Flaue. Als Zu-
gabe zu diesen Herrlichkeiten kapitalistischer Ordnung
aber die Frohn in Räumlichkeiten, die in der Regel
gleichzeitig Werkstatt, . Wohnung, Schlafzimmer,
Küche, Waschhaus sind, eventuell auch Kranken-,
Geburts-und Sterbezimmer: die Frohn unter Ver-
hältnissen. welche allen Anforderungen der Hygiene
Hohn sprechen, direkte Ursache oder fruchtbarer
Nährboden körperlicher Leiden sind, welche Gesund-
heit und Lebenskraft vorzeiffg.brechen. Es mangelt
der dürftige gesetzliche Schutz gegen übermäßige
Ausbeutung, es mangelt die geringfügige staatliche
Hilfe im Falle von Krankheit, Alter, Unglücksfall.
Schutz und Hilfe, auf die das Fabrikproletariat
rechtlichen Anspruch hat. Statt deren für die
Konsektionsarbeiter dafern sie, Heimarbeiter
sind der Zwang, einen Theil der Betriebskosten
ans dem eigenen schmalen Beutel z» decken.

Auf ErwerbSverhältniffen jämmerlichster Art
baut sich für die Arbeiter und Arbeiterinnen der
Konfektion eine Existenz jämmerlichster Art auf.
Tagtäglich schreibt die äußerste, Leib und Leben
schädigende Dürftigkeit den Küchenzettel, stets hockt
die graue Sorge gespenstig am häuslichen Herde,
sehr oft tritt der Hunger über die Schwelle des
armseligen Heims. Die Erwerbsarbeit saugt jede
Minute Zeit, jedes Tröpfchen Kraft auf, verwandelt
Menschen, fühlende, denkende, sich sehnende Men-»
scheu in ArbeitSmaschinen. regelt warmes, mensch-
liches Leben nach der Nadel Stich. Wochenlang
kein ordentliches Ausspannen ,und gründliches Aus-
ruhen, keine Möglichkeit, frischen Geistes und fröh-
lichen Herzens den Seinigen zu leben, den Freun-
den, den- sozialen Pflichten, sich selbst; leine
Möglichkeit zu bescheidener Erholung, zur Befriedi-
gung des vielleicht heiß emporquellenden BildungS-
dranges. Und das vereinzelte Schaffen, ohne stete
Berührung und innige Fühlung mit größeren
Gruppen von Arbeits- und LeidenSgenoffen läßt
das Elend als individuell lastendes Verhängniß
auffassen, hindert die Erkenntniß der wirthschaft-
ljchen Wurzeln der' Noth, die Erkenntniß der
Klaffenlage und -des Klasscnintereffes. hemmt die
Entwickelung des 'sittlich erneuernden und stä'rkeriden
SoUdaritätLgefühls, das Bewußtsein von der un-
abwendbaren Nothwendigkeit des Klaffenkampfes.,
üumme, dumpfe, verzweifelnde Resignation herrscht

139. Titelseite einer Nummer des Anzeigen-Blattes der Schneiderund Schneiderinnen'^Berlins mit den programmatischen Forderungen

der Schneider und Schneiderinnen