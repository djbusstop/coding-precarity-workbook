﻿314

sollten unter Zugrundelegung dieses „Mindesttarifs" der freien Ver-
einbarung überlassen bleiben. Aber selbst unter dieser Voraussetzung, und
obwohl die Sätze des Tarifs hinter den angeblich von vielen von ihnen
gezahlten Löhnen noch zurückblieben, wollten die Fabrikanten von dem Tarif
nichts wissen. Damit fiel aber auch für die Arbeiter jeder Grund hinweg,
noch länger Zugeständnisse zu machen. In einer auf den 31. August ein-
berufenen Versammlung beschlossen sie nach einem Referat Timms ein-
stimmig eine von diesem beantragte Resolution, die in die Sätze ausläuft:

„Wenn die Anternehmer in ihrer Gesamtheit dafür eintreten würden,
den Mindesttarif überall durchzusetzen, so würde er als Mittel zur An-
bahnung geregelter Verhältnisse in der Konfektion unsere Zustimmung
erhalten; wenn er aber jetzt dazu benutzt wird, die Löhne noch weiter
herabzudrücken, so sind wir außerstande, diesem Tarif unsere Zu-
stimmung zu geben. Würde es geschehen, so berufen sich die Konfek-
tionäre in ihrer Charakterlosigkeit noch auf das Votum der Arbeiter,
um ihrem brutalen Vorgehen noch einen Schein von Recht zu geben.
Deshalb lehnen wir den Minimaltaris des Einigungsamts ab. Die
Versammlung bedauert, daß der Versuch des Einigungsamts, geordnete
Zustände in der Konfektion zu schaffen, gescheitert ist. Es bleibt den
Arbeitern kein anderer Weg, als durch eine geschlossene Organisation
sich bessere Zustände zu erringen."

In jeder Form waren die Bemühungen des Einigungsamts fehlgeschlagen,
was dieses unterm 23. September 1896 in der Tagespresse feststellte.
Zugleich mit seinem Mandat war jetzt auch der provisorische Vertrag vom
19. Februar abgelaufen, und die darin ausbedungenen Lohnverbesserungen
hingen nun wieder völlig vom guten Willen der einzelnen Fabrikanten und
der Widerstandskraft der Arbeiter als Individuen oder Gruppen ab. Mit
dieser war es aber am Ende des Kampfes nur unwesentlich besser gestellt
als wie am Anfang. Was in den ersten Wochen des Kampfes der
Organisation zugeströmt war, hatte sich mit sehr geringen Ausnahmen
wieder verlaufen, so daß man zeitweise bitter von einer „Organisationsflucht"
sprechen konnte. Die Statistik der Gewerkschaftskommission verzeichnet für
1897 in der Maß- und Konfektionsschneiderei nur 1030 Organisierte.

Dies widerlegt im Grunde allein schon die Kritiken, die während des
Kanrpfes und hinterher der Fünfcrkommission wegen angeblich zu weit
getriebener Nachgiebigkeit gemacht wurden. Die Anhänger der Lokal-
organisation, die während des Kampfes keine Versammlung hatten vorüber-
gehen lassen, ohne Vorwürfe dieser Art zu erheben, steigerten sie am Schluß
noch zu allerhand häßlichen persönlichen Verdächtigungen. Aber es liegt
auf der Äand, daß, wo man mit einem so wenig ausdauernden Element zu
tun hatte, das Formulieren auch der radikalsten Forderungen, das Bestehen
auf den letzten Buchstaben des Verlangten am Ausgang der Sache nichts
hätte bessern können. Die in jenem Sinn agitierende Frau Gubcla hatte
sich am 14. August ebenfalls im Sitzungssaal des Einigungsamts eingestellt
und im Gegensatz zur Fünfcrkommission erklärt, daß sie als Arbeiterin den
vom Einigungsamt vorgeschlagenen Tarif ablehne. Wenn diese Erklärung
auf den Ausgang der Verhandlungen überhaupt einen Einfluß hätte aus-
üben können, so höchstens nur den, den Fabrikanten für deren Nein den
angenehmsten Vorwand zu liefern.