﻿the scale towards

o

DD

-vj

>

•vl

O

oo

DD

00

3“i

«:■

*

£~-

r.

ff

o

*



s:

Handlungsgehilfen! Handlvngsgehilfinnen!

Der Reichstag hat den Gesetzentwurf der Regierung, der Eure tägliche Arbeitszeit
auf 14 Stunden festsetzt,- einer Kommission überwiesen. Diese hat Gegenvorschläge gemacht,
die thatsächlich eine Verbesserung des' Regierungsantrages bedeuten. Kaum waren dieselben
bekannt, da erhob sich unter der Prinzipalität ein Sturm der Entrüstung. War doch ihre
Freiheit, Euch auszubeuten» in etwas beschränkt worden. Wußtet Ihr doch jetzt, wann,
unabhängig von der Laune der Chefs, Eure Ruhezeit beginnt: wann Ihr Euch Eurer
Familie, Eurer Erholung, Eurer Fortbildung widmen konntet.

Dem Ansturm der Schreier droht die Regierung nachzugeben. Alles wollen
jene der freien Vereinbarung überlassen! Keine gesetzliche Festlegung! Hohn und
Spott über die „freie Vereinbarung"! Was ist sie unserer Prinzipalität, ihr, die sich mit
Ruhe über Gesetz und Recht wegsetzt?!

Eine gesetzliche Sonntagsruhe besteht seit Jahren. So kümmerlich sie ist, wir
begrüßten sie als ersten Schritt zur Besserung unserer Lage. Doch wo ist sie hin.? Heute,
nach kaum zehn Jahren, kümmern sich Eure Prinzivale nicht mehr uni jene Vorschriften, ver-
achten das Gesetz, schlagen den Ueberwachnngsorganen ein Schnippchen.

Und warum?	»

Weil jene Maßregeln nur halbe waren, Eure Sonntagsruhe nur eine
theilweise.

Kollegen und Kolleginnen! „Noth lehrt beten", sagt das Sprüchwart, „aber Noth
lehrt auch kämpfen!"

Nur ein« einheitliche Sonntagsruhe kaun uns frommen! Nur die Ver-
kürzung unserer täglichen Arbeitszeit kann uns ein menschenwürdiges Dasein gewähren!

Darum, Kollegen und Kolleginnen, benutzt jede Gelegenheit, für Eure Rechte zu
demonstriren. Zeigt den gesetzgebenden Körperschaften, welches Eure Wünsche sind.

Erscheint alle in der am

Mittwoch, de» 20. September, Abends 8'|2 |U)t

stattfindende»

Großen öffentlichen

Ikrfammlung (kr kanüelsangWlten

in der Berliner Ressource, Kommandantenstr. 57.

Hages Krdnung.

l. Vortrag des Reichstags-Abgeordneten Pfannkuch über: „Der gesetzliche
Ladenschluß".

S. Dortrag des Kollegen August Hii.lze - „Die Sonntagsruhe vor den
Stadtverordneten".

3. Diskussion

Kköert Koyn.

Vertrauensmann der Berliner Handlungsgehilfen und Gehilfinnen.

_____________" gcl)lieiiiamiftrgjjc 11, II.

StSäiiiüii >m& «Klag: Alber, S»oön.	11.- Druck, Maurer u. DImmIck, BnU«.

O

CD

Q.

O

n

c

0D

CD

171. Flugblatt zugunsten der Sonntagsruhe im Handelsgewerbe