﻿146

Eingehend beschäftigte sich die vorerwähnte Kreiskonferenz mit der Frage
der Organisation der Landarbeiter und beschloß die Ausgabe von Frage-
bogen über deren Löhnungs- und Wohnungsverhältnisse, ihre vertraglichen
Verpflichtungen und sonstige ihre Lage beleuchtende Punkte. Das gleiche tat,
nach einem Referat Stadthagcns über den Gegenstand, am 28. August
1900 die Parteikonferenz für Berlin und die Provinz Brandenburg.

Aus der großen Zahl von sozialdemokratischen Partei- und Volks-
versammlungen, die Berlin im Jahre 1900 sah, beschäftigten sich zwei
speziell mit dem nach Paris einberufenen Internationalen Sozialisten-
kongreß für jenes Jahr. Die erste davon fand am 4. September statt
und galt der Wahl von Delegierten für den Kongreß. Es wurde be-
schlossen, als Vertreter der Sozialdemokratie Berlins drei Delegierte nach
Paris zu senden, und mit großen Mehrheiten wurden Stephan Fritz,
Franz Kohke und August Taeterow dazu gewählt. Außerdem sandten
auch die Metallarbeiter Berlins noch eigene Delegierte nach Paris.
Es war die Zeit, wo der Eintritt Al. Millerands in das Ministerium
Combes - Waldeck-Rousseau die Gegensätze in der französischen Sozial-
demokratie sehr verschärft hatte, und man erwartete vom Kongreß, daß er zu
dieser Frage Stellung nehmen und womöglich den Konflikt schlichten werde.
Es ist nur begreiflich, daß dieser Fall und die mit ihm verbundene grund-
sätzliche Frage auch die Sozialisten Berlins lebhaft beschäftigten. Ein Teil
von ihnen hielt zwar die Art, wie Millerand den Eintritt ins Ministerium
vollzogen hatte, für falsch, den Eintritt selbst aber, wenn er mit Zustimmung
und unter der Kontrolle der Partei erfolgt wäre, unter den in Frankreich ge-
gebenen Amständen für zulässig oder mindestens entschuldbar. Andere aber ver-
traten schroff die Meinung, daß ein Sozialdemokrat unter keinen Amständen in
einem bürgerlichen Ministerium Platz nehmen dürfe, und verwarfen ebenso
entschieden jedes Gegenseitigkeitsbündnis der Sozialdemokratie mit bürgerlichen
Parteien mtd damit auch den Pakt, der damals beim republikanischen Block
Frankreichs zwischen demIaurässchenFlügel der französischenSozialdemokratie
und der Linken der republikanischen Parteien bestand. Der Pariser Kon-
greß nahm indes, wie bekannt, eine vermittelnde Resolution an, die ohne
die Beteiligung von Sozialisten an einem bürgerlichen oder aus bürgerlichen
und sozialdemokratischen Elementen gemischt zusammengesetzten Ministerium
absolut zu verfemen, sie doch nur für außergewöhnliche Fälle, d. h. als Aus-
nahme, zulässig erklärte. Für diese Resolution hatte auf dem Kongreß als
Redner der deutschen Delegation Ignaz Auer gesprochen, der mit Paul
Singer von der Partei nach Paris entsandt war, und die deutsche Delegation
hatte demgemäß abgestimmt. Dies vor allen Dingen bildete das Dis-
klissionsthema in der Versammlung, in der die Berliner Delegierten am
l 6. Oktober 1900 über die Ausübung ihres Mandats Bericht erstatteten.

Nachdem zu Anfang 1900 die Sozialdemokratie Berlins in einem
Teil der 19 großen Volksversammlungen, in denen sie gegen die damalige
Flottenvorlage der Regierung protestierte, die Versuche der nationalsozialen
Gelehrten, sie für eine nationalistische Politik zu gewinnen, schroff zurück-
gewiesen hatte, fanden gegen Ende 1900 in Berlin stark besuchte sozial-
demokratische Versammlungen statt, in denen der bis dahin nationalsoziale
und freiwillig von seinem Pfarramt zurückgetretene ehemalige Pastor Paul
Göhre Vorträge über „Christentum und Sozialdemokratie" hielt und seinen