﻿419

Rur im ersten Landtagswahlkreis, d. h. im Westen und Südwcsten Berlins,
hatten die bürgerlichen Parteien zusammen eine Mehrheit, die aber auch
noch ziemlich schwach ist. In den anderen drei Kreisen aber ist die sozial-
demokratische Mehrheit gegenüber den beiden Parteien zusammen jedesmal
erdrückend. Rach den Wählerklassen abgeteilt, verteilten sich die Arwähler-
stimmen von Berlin, dieses als Einheit genommen, folgendermaßen:

	sozialdemokratisch		freisinnig	konservativ
Erste	Wählerklassc	222	3 508	412
Zweite	„	5 010	9 085	1 846
Dritte	„	116918	8140	4 856
	Insgesamt	122 150	20 733	7 114

Selbst in der zweiten Wählerklasse hatte die Sozialdemokratie somit
noch nahezu ein Drittel der Stimmen. Alle Klaffen und Wahlkreise zu-
sammengerechnet aber hatte sie, selbst wenn man die Arwählerstimmen der-
jenigen Wahlmänner mit berücksichtigt, die sich nicht an der Abgeordneten-
wahl beteiligten, 68,3 Prozent aller abgegebenen Stimmen, ein höherer
Prozentsatz als selbst bei der großartigen Reichstagswahl vom 16. Juni
des gleichen Jahres. Lind das bei offener, protokollierter Stimmabgabe,
überzeugender konnte die Befürchtung nicht widerlegt werden, daß es nicht
gelingen werde, für die Landtagswahl die Massen an den Wahltisch zu
bringen. Pier war eine Massenaktion, wie Berlin sie noch bei keiner
Abgeordnetenwahl erlebt hatte. And daß die 68 Prozent der Wähler auch
nicht einen Abgeordneten ihrer Gesinnung erhielten, daß die neun von Ber-
lin zu vergebenden Mandate sämtlich den Freisinnigen zufielen, die nur
11,59 Prozent der Arwählcr hinter sich hatten — konnte der Vergewaltigungs-
charakter des Dreiklassenwahlsystems eindrucksvoller festgestellt werden, als
durch die Demonstrierung dieser Tatsache?

Auch in den beiden zu Groß-Berlin gehörenden Vorortswahlkreiscn
erhielt bei der Arwahl die Sozialdemokratie die Mehrheit der Stimmen.

Im Kreise Niederbarnim wurden Arwählerstimmen abgegeben:

I. Klaffe II. Klaffe III. Klaff- Insgesamt
für sozialdemokratische Wahlmänner 63	1 059	12 599	13 721

„ freisinnige	„	372	634	283	1 289

„ konservative	„	666	1 458	3 503	5 627

Anders im Kreise Oberbarninr, mit denr zusammen Niederbarnim einen
Wahlkreis bildet. Dort erhielten die Konservativen auch die große Mehr-
heit der Arwählerstimmen und die erdrückende Mehrzahl der Wahlmänner.
Dank ihrer erlangten die Konservativen eine absolute Mehrheit über Liberale
und Sozialdemokraten zusammengenommen. Es wurden bei der Abgeordneten-
wähl für Nieder- und Oberbarnim 719 Konservative, 327 sozialdemokratische
und 316 liberale Wahlmännerstimmen abgegeben, und so erhielten die
Konservativen alle drei Mandate des Kreises — was man Volksvertretung
nannte.

Im Kreise Teltow-Beeskow-Charlottenburg sollte der Wider-
sinn des Wahlsystems noch krasser in die Erscheinung treten. In jedem der
zu ihm gehörenden drei Stadtkreise sowie im Kreise Teltow hatte die Sozial-
demokratie die Mehrzahl der Arwählerstimmen, nur in dem am schwächsten
von allen bevölkerten Kreise Beeskow-Storkow entfiel die Mehrheit der
Stimmen auf die Konservativen. Die Vertretung des Kreises aber ward,

27*