﻿haben und dadurch denjenigen Kollegen, die aus den alten kaufmännischen Ver-
einen ausschieden, einen Ersatz bieten solle. Die betreffenden Anträge fanden
keine Mehrheit, da namentlich die Berliner Delegierten sie als zum min-
desten verfrüht bekämpften, und als zu Pfingsten 1897 auf einer zu Leipzig
abgehaltenen Konferenz von Delegierten aus acht Orten Deutschlands doch
ein solcher Zentralverband beschlossen wurde, der dann am 1. Juli 1897
mit Lamburg als Zentralsitz ins Leben trat, hielten sich Berlin und einige
andere Städte mehr als ein Jahr noch abseits. Am 2. Oktober 1898 jedoch
beschlossen auch die Freien Vereinigungen dieser Orte, sich aufzulösen und
Mitgliedschaften des Zentralverbandes zu bilden. Das geschah, und der
erste Bevollmächtigte der Berliner Mitgliedschaft wurde Willy Fried-
länder, der dieses Amt bis zum Jahre 1906 in selbstlosester Weise ver-
sehen hat, Kassierer Arban, und neben diesen und den vorher schon ge-
nannten Streitern traten nun ganz besonders Ida Baar, Julius Kaliski,
Regina Kraus, Emil Rosenow und Georg Acko als Kämpfer für die
Bewegung in den Vordergrund.

Da derZentralverband sich im „Landlungsgehilfenblatt" ein eignesOrgan
geschaffen hatte, war mit Beitritt derVereinigung zum Verband „Der Pandels-
Angestellte" überflüssig geworden und stellte am 15. Oktober 1898 sein Er-
scheinen ein. Das „Landlungsgehilsenblatt" wurde damals in Berlin von
Leopold Liepmann redigiert.

Einen großen Erfolg erkämpften im Jahre 1897 die im Geist der
Arbeiterbewegung wirkenden Landlungsgehilfen bei der Delegiertenwahl für
die Ortskrankenkasse der Apotheker, Kaufleute ustv. Berlins. Die Ortskranken-
kaffc für Landlungsgehilfen, die bis dahin neben jener Kasse bestanden hatte,
war aufgelöst worden, und es galt nun, Einfluß auf die Verwaltung der erst-
genannten Kasse zu gewinnen. Zu diesem Zweck gingen die Mitglieder
der Vereinigung gemeinsam mit den Mitgliedern der Berliner Ortsstelle
des Verbandes der Landels- und Transportarbeiter vor und erreichten es,
daß die mit ihnen vereinbarte Delegiertenliste die große Mehrheit der
Stimmen erhielt. Die Ortskrankenkasse für Kaufleute, Landelsleute und
Apotheker ist die größte Ortskrankenkasse Deutschlands, sie war im Jahre
1905 auf über 90 000 Mitglieder angewachsen und hat unter der Leitung
des bald darauf zum Rendanten der Kasse gewählten Vertrauensmanns
der Landlungsgehilfen Albert Kohn und anderer den Interessen der An-
gestellten ergebenen Personen ganz Bedeutendes auf dem Gebiet der Sozial-
politik, speziell auch der Lygiene für ihre Mitglieder geleistet. Anter anderem
haben ihre Veröffentlichungen über die Wohnverhältnisse ihrer Mitglieder
die Anerkennung aller Sozialpolitiker gefunden. Allerdings sind der Ver-
waltung auch Angriffe von Unternehmern und Behörden nicht erspart
geblieben. Sie haben sie aber in ihrem Wirken nicht von der bcschrittenen
Bahn ablenken können. Bei den Erneuerungswahlen von 1902 erhielt die
Liste der beiden gewerkschaftlichen Verbände 2322, die der vereinten Gegner
aber nur 1057 Stimmen.

Ein weiterer Erfolg der rastlosen Agitation der nun im Zentralverband
organisierten Landlungsgehilfen Berlins war es, daß im Jahre 1903 endlich
die städtischen Behörden sich dazu entschlossen, den Krankenkassenzwang für
alle Angestellten einzuführen, die bis zu 2000 Mk. Iahresgehalt beziehen.
Lier waren es die sozialdemokratischen Stadtverordneten, die in der Gemeinde-