﻿Zwei Achtundvierziger.

Leonei Fe!ringer.

Zwei Marxianer sieht man bie>

Die einstmals zu des Meislers Füssen
Ihr Leben suchten zu versüssen
Mit radikaler Theorie.

Es trennte sich bald ihr Lebenspfad —
Dieweil der Eine sich .entwickelt*.
Den Marx ins Praktische venniquelt
Als keck entschlossner Mann der That

Blieb treu der Andre seinem Licht.

Heut sitzt er hinter Kerkermauem —

Und dennoch — wollt ihr ihn bedauern?
Ich glaub', er tauscht mit jenem nick»

202. Karikatur aus dem „Narrenschiff"

Bernstein. Berliner Geschichte.

III.

28