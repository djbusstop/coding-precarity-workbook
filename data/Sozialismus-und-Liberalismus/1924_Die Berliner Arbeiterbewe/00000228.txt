﻿215

Dro MKrneVl-FnlrtzLifZ.

28ä3 Baurath Schwechten einen „Scherz" ts

»t rP:£ ee!-^)nfchrlfi hctt nunmehr endlich eine Auslassung von «zuständiger" Seite im Gesolge gehabt, eine
Auslassung, die allerdmgS aucb darnach ist. Die bürgerlichen Preßoraane veröffentlichen eine ihnen vom Erbauer
der Kira)e, dem B au rath Schwechten, zugesandte Erklärung des folgenden Inhalts:

«Mit der von dem „Vorwärts" mit so viel unwahren und tendenziös aufgebauschten Zusätzen versehenen
Nachricht über die^ ans die Stadtverordneten sich beziehende Inschrift an einer Thür der Kaiser Wilhelm-
Gedächtniß-Kirche verhalt es sich folgendermaßen: Entgegengesetzt den Mittheilungen des «Vorwärts" ist die
betreffende Thür die von den, Altar am weitesten entlegene. Das betreffende Relief ist als Entwurf nach
den Bildern aus der Bibel von Schnorr v. CarolSfeld nur begonnen, an einer durchaus dunklen Stelle, so
daß eS den Angen des Kirchenbesuchers ganz entzogen war und noch heute ist. Daß das Relief Porträts enthalte,
ist unwahr; die Inschrift ist eine Nachahmung eines bei altromanischen und gothischen Bauten fast
'überall sich findenden Architekten-Scherzes. Die Fertigstellung des Reliefs wurde durch die Ein-
weihung der Kirche unlerbrochen und nimmt jetzt noch eine längere Zeit in Anspruch. Der nur skizzirte
Entwurf ist von der V a u k o m m i s s i o n noch nicht einmal besichtigt worden, und es war derselben der
daraus befindliche Scherz vollständig unbekannt. Daß der Scherz sich, wie der „Vorwärts" erzählt, auch auf
den Oberhofmeister Freiherrn v. Mirbach bezöge, ist selbstverständlich unwahr."

Mas Herr Schwechten dem «Vorwärts" gegenüber Unwahrheiten nennt, muß der Leser errathen, wir haben es
nicht herausgefunden. übrigen verlohnt es sich nicht, ernsthaft darauf einzugehen. Selbst die «Vosfische Zeitung"
sieht sich gemüssigt, die merkwürdige Ehrenrettung auS der Jeder des Bauleiters mir folgenden bitteren Worten
zu begleiten:

«An der Beurtheilung der Sache wird durch diese Erklärung nichts geändert. Es kommt nicht auf
-das Relief, sondern auf die Inschrift an und diese enthält nicht einen Scherz, svudern, ein-
grobeBejchimpfung derB erline r Stadtverordneten- Versammlung, und eS wäre
nicht ohne Interesse, den Vorüber dieses Scherzes und feine etwaigen Theilnehmer
kennen zu lernen."	a

99. Die Kamel-Inschrift in der Kaiser-Wilhelm-Gedächtniskirche

Aus dem .Vorwärts"

durch A.Iacobey wiedererobert, dafür ging aber der 15. Bezirk (Th.Mehner)
verloren. Sonst fand nur ein Personenwechsel statt, indem für den 24. Bezirk
an Stelle G. Tempels, der nicht wieder kandidierte, Paul Dupont gewählt
wurde. Wiedergewählt wurden die ausscheidenden Mitglieder Fr. Zubeil
(11. Bezirk), G. Schulz (13. Bezirk) und Rob. Lerzfeldt (35. Bezirk).
Das Stimmenverhältnis war in den fünf Bezirken, die sozialdemokratisch
wählten, 8888 sozialdemokratische gegen 5141, in allen 14 Bezirken 12 691
sozialdemokratische gegen 14 173 gegnerische Stimmen.

Einen Mandatsverlust, dem kein Gewinn gegenüberstand, brachte das
Jahr 1897. Im 10. Bezirk, den bis dahin Dr. I. Zadek vertreten hatten
aber aus Gesundheitsrücksichten nicht weiter vertreten konnte, ging das ,
erst in der Stichwahl eroberte Mandat wieder verloren. 2n 4 Bezir en