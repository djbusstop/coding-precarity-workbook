﻿Neuntes Kapitel.

Die Gewerkschaftsbewegung Berlins.

a) Die Entwicklung der Gewerkschaften.

^U^ur langsam und unregelmäßig hatten sich unter dem Sozialistengesetz

1 die Gewerkschaften Berlins entwickeln können. Einem organisch sich
vollziehenden Aufbau und Ausbau standen die wechselnde Praxis
in der Handhabung des Ausnahmegesetzes durch die Behörden und die
Fußangeln des preußischen Vereinsgesetzes im Wege. Statt ihre Organi-
sationsform und den Ausbau ihres Wirkungsgebiets lediglich von den Be-
dürfnissen der Gewerkschaftsarbeit bestimmen zu lassen, mußten die Gewerk-
schaften sie in hohem Grade von Rücksichten abhängig machen, die mit
ersteren nichts direkt zu tun hatten, und blieben dadurch auch in bezug auf
Wachstum und Leistungsfähigkeit in größerer Abhängigkeit vom Wechsel
der Geschäftskonjunkturen, als es bei ungehemmter Entwicklung der Fall
gewesen wäre.

Immerhin hatteir sich noch unter dem Sozialistengesetz die Gewerkschaften
Berlins, wie im zweiten Band dieser Geschichte berichtet wurde, in der Streik-
Kontrollkommission eine Instanz für die Regelung gemeinsamer örtlicher
Angelegenheiten gewerkschaftlichen Charakters geschaffen (vergleiche Bd. ll
dieser Geschichte, Seite 327/328). Sie bestand unter diesem Namen bis
Ende 1892, wo sie, entsprechend den sich erweiternden Arbeiten der Kom-
mission, den Namen Berliner Gewerkschaftskommission erhielt,
den sie heute noch trägt. In den ersten Jahren arbeitete sie mit überaus
bescheidenen Mitteln und konnte daher nur summarische Abrechnungen ver-
öffentlichen. Die letzte dieser Abrechnungen bezieht sich auf die Zeit vom
14. Juli bis zum 27. Dezember 1892 und verzeichnet eine Einnahme von
3012 Mk. (wovon 980 Mk. Extrabeiträge für die Vertretung am Gewerbe-
gericht waren) und eine Ausgabe von 1718 Mk. Die beiden folgenden
Berichte rühren schon von der Gewerkschaftskommission her, der erste davon
für die Zeit vom 29. Dezember 1892 bis zum 7. August 1893. Nach ihm
betrugen die Einnahmen, abzüglich eines Vortrags von 1007 Mk., 4081,
die Ausgaben 3727 Mk. Allmählich hatten sich die Arbeiten der Kommission
so vermehrt, war namentlich das Bedürfnis der Arbeiter nach Auskunft
über die neuen Versicherungs- und Gewerbegesehe so gestiegen, daß sich nun
die Notwendigkeit der Einrichtung eines ständigen Bureaus der Kommission
immer dringender geltend machte. Sie ward nach eingehenden Debatten
Bernstein, Berliner Gechtchte. III.	17