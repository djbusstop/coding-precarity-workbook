﻿432

des neuen Jahrhunderts bringt mit dem China-Feldzug und dem Feldzug
in Südwestafrika stärkere Verurteilungen wegen Beleidigung von Kriegsherr,
Truppenführer oder Truppen. Von anderen gerichtlichen Verfolgungen
politischer Natur dagegen wird es verhältnismäßig still. Lediglich die Pro-
zesse wegen Überschreitungen der erlaubten Formen des Gewerkschaftskampfes
dauern in größerem ümfange an; wie denn überhaupt die Verurteilungen
von Arbeitern auf Grund solcher Anklagen in der Liste der Opfer des
Klassenkampfes einen großen Raum einnehmen.

Eine nach Möglichkeit erschöpfende Zusammenstellung aller Freiheits-
strafen, die im Bereich von Groß-Berlin von Anfang des Jahres 1891
bis zum Jahresschluß 1905 wegen politischer und gewerkschaftlicher Vergehen
verfügt wurden, ergibt in runden Zahlen die Verurteilung von 130 Personen
zu zusammen 45 Jahren Gefängnis. Es fehlen jedoch dabei die Verurteilungen
zu Äaftstrafen von unter einer Woche, sowie die nicht geringe Zahl der
Fälle, wo Geldstrafen durch Gefängnishaft abgebüßt wurden. Mit ihnen
würde die Summe der Freiheitsentziehungen sich auf gegen 60 Jahre
erhöhen. Was die verfügten Geldstrafen anbetrifft, so ist von ihrer Auf-
rechnung abgesehen worden, da Vollständigkeit hier noch schwerer zu erzielen
ist, als bei den Freiheitsstrafen, und die Geldstrafen noch wenig über die
Geldkosten besagen, die der Arbeiterschaft durch die polizeilichen und
gerichtlichen Verfolgungen auferlegt wurden, und die sich, da auch die
Prozesse hierhergehören, die mit Freisprechung endeten, zu ganz anderen
Beträgen summieren. Wir glauben nicht zu übertreiben, wenn wir die
Gesamtsumme aus über 100 000 Mark schätzen.

Mehr als ein Drittel der vorerwähnten 45 Jahre Gefängnis, nämlich
rund 18 Jahre, wurden Arbeitern wegen Vergehen im Koalitionskampf
auferlegt, gegen 10 Jahre waren Strafen für Preßvergehen oder was
Richter dafür erklärten, der Rest entfällt auf Verurteilungen für Reden in
Versammlungen, Ausrufe bei Zusammenstößen mit der Polizei und die
vereinzelten Fälle von Widerstand gegen Schutzleute bei solchen Zusammen-
stößen. Eine Aufzählung aller dieser Verurteilungen im einzelnen verbot
sich von selbst. Aber auch eine Aufzählung der gravierenderen Fälle von
Klassenjustiz stellte sich als nicht durchführbar heraus, weil es ihrer zu viele
sind, um sie alle vorzuführen, jede willkürliche Begrenzung aber falsche
Vorstellungen erwecken würde.

Es soll natürlich nicht geleugnet werden, daß ein Teil der ürteile nach
heutigem Recht gefällt werden mußten, daß Arbeiter durch Erbitterung über
unkollegialisches Landein, Redner durch Leidenschaftlichkeit sich zu Aus-
drücken und Schriftsteller durch Leichtgläubigkeit sich zu Beschuldigungen
verleiten ließen, die auch vor einem Tribunal von Sozialdemokraten nicht
bestehen würden. Aber selbst bei diesen Vorkommnissen kommt im Straf-
maß oft die Klassendenkweise der Richter zum Ausdruck, wie sich das unter
anderem zeigt, wenn genau dieselbe Landlung — sagen wir: handgreifliches
Vorgehen gegen Arbeitswillige bei gleicher Lage des Falles — das eine Mal
von dem einen Gerichtshof mit ein paar Tagen, das andere Mal von
einem anderen Gerichtshof mit ebensovielen oder selbst noch mehr Wochen Ge-
fängnis geahndetwird. Diese fast alltäglichenünterschiedein der Strafbemessung
lassen erkennen, wie sehr beim Richterspruch die persönlichen Vorurteile
und Stimmungen mitreden. Es sind keineswegs nur die größeren Freiheits-