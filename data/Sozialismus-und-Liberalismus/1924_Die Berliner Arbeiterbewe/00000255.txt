﻿Achtes Kapitel

Die Sozialdemokratie in den Gewerbegerichten
und den Kaufmannsgerichten Groß-Berlins.

A. Die Wahlen zu den Gewerbegerichten.

m 1. April 1891 trat das Gesetz betreffend die Gewerbegerichte vom

29.	Juli 1890 in Kraft, aber es dauerte noch zwei Jahre, bis
Berlin ein Gewerbegericht erhielt. Das Gesetz stellt die Errichtung
in das freie Ermessen der Gemeinden, es war aber selbstverständlich, daß
Berlin ein solches Gericht brauchte. Schon 1887 hatte die Stadtverordneten-
versammlung, dem Drängen der Sozialdemokraten folgend, ein ziemlich gutes
Statut beschlossen, dem aber die Oberbehörde die Genehmigung versagte. Eine
1890 angenommene, abgeschwächte Fassung blieb im Hinblick auf das werdende
Reichsgesetz unausgeführt. Nun war das Gesetz da, es dauerte jedoch bis
Februar 1892, bevor der Magistrat eine entsprechende Vorlage vor die
Stadtverordnetenversammlung brachte. Das Statut, das auf Grund ihrer
zustande kam, unterscheidet sich in verschiedenen Punkten unvorteilhaft vom
Statut, wie es am 17. April 1890 für den gleichen Zweck beschlossen war.
Das Reichsgesetz schloß die weiblichen Personen gänzlich von der Wahl
aus, und während die Sozialdemokraten vorgeschlagen hatten, die Geltungs-
dauer der Mandate auf zwei Jahre zu beschränken, wird sie hier auf sechs
Jahre ausgedehnt. Statt der von den Sozialdemokraten befürworteten
Verhältniswahl ward die Wahl nach Bezirken beschlossen, und entgegen
dem ursprünglichen Vorschlag des Magistrats, Gebührenfreiheit einzuführen,
ward unter Mitwirkung der Regierung von der Mehrheit der bürgerlichen
Vertreter im Statut durchweg Kostenerhcbung vorgeschrieben.

Bis zum Jahre 1900 wurde jedesmal in allen Bezirken gewählt,
obwohl nach der ersten Wahl immer nur Ergänzungswahlen vorzunehmen
waren. Es hatten aber aus allen Bezirken Vertreter auszuscheiden, und
so wurden auch alle Wähler aufgefordert, an den Wahltisch zu treten.
Nach der Wahl von 1898 aber war behufs Verringerung der Wahlarbeit
die Anordnung getroffen, daß jeweilig in einem Drittel der Bezirke alle
Vertreter auszuscheiden und für sie Ergänzungswahlen stattzufinden haben.
In diese Zeit entfällt aber eine weitere Änderung. Es trat das der
Rettung des Handwerks bestimmte Gesetz vom 21. Juli 1897 über Zwangs-
innungen usw. ins Leben, für eine Reihe von Gewerben wurden
Innungs-Schiedsgerichte ins Leben gerufen, und die Arbeiter dieser
Bernstein, Berliner Geschichte. III.	16