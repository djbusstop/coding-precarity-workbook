﻿222

b)	Die sozialdemokratische Vertretung in den Vororten.

Wenn aber in Berlin nicht einmal die Eroberung aller Mandate der
dritten Wählerklaffe zu erreichen war, so ist dies Werk dagegen in einer
Anzahl von Vororten Berlins möglich gewesen. Dort liegen eben die
Dinge einfach so, wie sie in Berlin in den Stadtvierteln des Nordens,
Nordostens, Ostens, Südostens und zum Teil noch des Südens und des
Nordwestens beschaffen sind: die Arbeiterschaft bildet die erdrückende
Mehrheit der Bevölkerung. Sobald einmal das Eis gebrochen war und
die Sozialdemokratie in den Vororten dem Beispiel Berlins folgte und den
Kampf um die Vertretung in der Gemeinde aufnahm, wurden — mit
wenigen Ausnahmen, die sich durch Veränderungen in der Zusammen-
setzung der Bevöllerung erklären — daher auch hier von Wahl zu Wahl
bessere Erfolge erzielt.

Man darf jedoch nicht meinen, daß das Verhältnis der Arbeiter-
wähler zu den bürgerlichen Wählern allein schon die Aussichten der
Wahl bestimmt. In den kleinen Ortschaften war ursprünglich überall
— und ist verschiedentlich selbst heute noch — die große Mehrheit der Arbeiter-
schaft so verschüchtert und der politischen Selbständigkeit so ungewohnt,
daß sie sich nur sehr schwer dazu bewegen ließ, ihre Stimmen für sozial-
demokratische Kandidaten oder auch nur für einfache Arbeiterkandidaten ab-
zugeben. In kleinen Gemeinden kann sich die ökonomische Übermacht eben
ganz anders geltend machen als in der Großstadt, und so hat denn
di« Vorschrift der öffentlichen Stimmabgabe hier auch zumeist eine weit
größere Beschränkung der Wahlfteiheit zur Folge. Ferner ist in den
kleinen Ortschaften, die unterhalb einer gewissen Bevölkerungszahl bleiben,
von kommunalem Leben so wenig zu spüren, daß der Antrieb der Arbeiter,
an ihm teilzunehmen, meist auch nur ein sehr geringer ist. Das Lerkommen
beherrscht die Geister, nur ausnahmsweise und nur in kleinen unauffälligen
Dosen wird an ihm geändert. Das lähmt die Gemüter, und viel besser
stand es auch lange Zeit selbst in der Masse der oberhalb der Linie be-
findlichen Gemeinden nicht. Kurz, es wäre durchaus irrig, anzunehmen,
daß in den Vorortsgemeinden mit überwiegender Arbeiterbevölkerung die
Arbeit für die Vertretung in der Gemeinde von vornherein eine leichtere
gewesen sei. Sie war im Gegenteil vielfach eine sehr viel schwerere, als in Berlin.
Viel Intelligenz und Verwaltungsarbeit wirkt unauffällig an kleinen Orten, die
abseits vom Verkehr liegen, oder hat den Grund zu Vertretungen gelegt,
die dann, nachdem die betreffenden Gemeinden stärkere Bevölkerung er-
halten hatten, auch eine bedeutende Wirkung ausüben konnten. Man be-
greift aber ohne weiteres, daß es nicht angeht, diese Entwicklung nun für
jeden in Betracht kommenden Ort im einzelnen hier vorzuführen. Es muß
dem Leser anheimgestellt werden, was mit den Unterschieden, wie sie durch die
Größe der Orte und die soziale Gliederung ihrer Bewohner gegeben sind,
für alle Orte ohne Ausnahme gilt, auf jeden einzelnen anzuwenden.
Überall hat man klein angefangen, anfänglich Niederlagen und gelegentlich
auch einmal Rückschläge erlitten, überall mußte ein mühsames Erziehunzs-
werk geleistet werden, und wo immer nur Mitgliedschaften der Sozial-
demokratie von einiger Stärke bestanden, ist es geleistet worden. So
konnten denn, am Abschluß unserer Epoche, d. h., da die Landgemeinden