﻿I

Kr. 1.

Erscheint tSstuch nutzer MsntagL
Abonnements-Preis sür Berlin:
Vierteljährlich 3,30 Mark, monat».
lich i,io Mark, wöchentlich 28 Pfg.
fretin'SHauS. Einzelne Nummer
K Pfg. Sonntags -Nunnner mit
teilt „Sonntags - Blatt" io Psg^
Post-Abonnementt 3.8^Mark pro
Quartal. Nnter Kreuzband: Für
Deutschland u.Oestecretch-Ungar».
2 Mark, für da§ übrige Ausland
8 Mark pro Monat. Eingetragen
In der Post-ZeituNgS-Preisliste
für 1891 unter Nr. S4«s.

cK2D>>25cx>s

S.Inhrg.

JnfettionS-Gebühr betrügt lür die
fünfgespaltene Pelitzeile oder deren
Raum 40'Pfg., für Vereins- und
VerfammlungS - Anzeigen so Pfg.
Zusernte für die nächste Nummer
müssen bis 4 Nbr Nachmittags in
>er Erpeditio» abgegeben werden.
Die Expedition ist an Wochentagen
bis t Uhr Mittags und von s bis
1 Uhr Nachmittags, au Sonn- und
Festlagen bi» s Uhr Vormittags
geöffnet,

Lwnkprschevr A«rS,Nv. SL0S.

Berliner VelksUatt

Zsntralorgan der sozialdemokratischen Partei Deutschlands.

Redaktion: Muth-Straße 2.	^ Donnerstag» den 1. Januar 1891. n ßrpeditio«: ZLeuch - Straße 3,

Freih eit

5reiheit! Freiheit!

Du Wunderxort, du WiMderwort!

Du Inbegriff der herrlichsten der Lieder,

Wie klingst du in des Menschen Seele wieder l
Ein Wunderwort, ein Wunderhort,

Der alles Schönste in sich birgt.

Der alles Schönste aus sich wirkt!

Freiheit! sie wird nicht ohne Mühe dein ;

Will wie ein schönes Weib errangen sein.

Zum Urue« Jahre 1891.

Nur kämpfend dringst du vor
Zu ihrem köstliche» Eeiniß,

Nur wenn du sagst: Ich. peiß, ich muß
Und kann nicht anders!

Du bist nicht frei, wenn du das Schlechte willst.

Du bist nicht frei, wenn du erwählst.

Was dir bequeme Freude» schafft.

Ein Sklave bist-du deiner Leidenschaft.

Doch führt der Weg-zur Schönheit auch durch. Noth,
Droht er im Katupfe 'ftlbst den Tod, —

Doch im erkennst und weißt, du mußt,

Und vorwärts gehst du mit jauchzender Lust,
Bleibst deinem Ziel vollendet treu,

Dana bist du frei!

Die Schönheit ist des Werdens Ende,

Die Schönheit ist des Werdens Ziel,

Vollendetes Gezwnngensein,

Den Weg zu wandeln vollbewußt
Rach diesem Ziel ist Freiheit!

Freiheit!	Leopold Jacob?.

3um Menen Fstze!

Die Jahreswende bietet, nach allgemeiner .und sehr
natürlicher Sitte, den Anlaß zu einem Rückblick in die
Vergangenheit und zu einem Vorblick in die Zukunft —
wir lassen, die Ereignisse Und Vorgänge des dem Abschluß
zueilenden Jahres noch einmal vou-uns vorübergehe«, und
schaue« dann dem kommende« -Jahr froh ins Angesicht.

Für uns Sozialdemokraten ist die politische Inventur-
Aufnahme diesmal eine besonders erfreuliche Beschäftigung:
wir haben auf eine Reihe großer Erfolge zurückzuschallen.

Als einer unserer Abgeordneten an, Schluß des
vorigen Jahres im Reichstag erklärte: .Wir haben das
Sozialistengesetz besiegt — die nächste Wahl wird «ine
Million — anderthalb Millionen Stimm en
für uns ergeben", da glaubten selbst viele Parteigenossen,
die Prophezeiung sei zu kühn.

Und der 20. Februar 1890 hat sie im vollsten
Umfange wahr gemacht.

Der 20. Februar 1890 ist ein weltgeschichtliches
Dalnni — er bedeutet den endgiltigen Sieg der sozial-
demolratischen Idee und Weltanschauung über die mecha-
nische Gewalt — feie Bankrotterklärung des Sozialisten-
gesetzes, seiner Urheber und des Systems, dem es ent-
flossen.

Um die Tragweite und Bedeutung des Tags voll
zu begreifen, müssen wir uns ins Gedächtniß zurückrufen,
daß heule vor 12 Monaten Fürst Bismarck noch „fest
im Sattel saß", daß 16 Tage vor dem Wahltag die
„Kaiserlichen Erlaffe" veröffentlicht wurden, die aus ben

Feuilleton.

Nachdruck vrrborm.j	- - .	£

Der Mama.

Roman von Arne Garborg.

I.

Frau Holmsens Antlitz zog sich in die Länge. „Ver-
stehen Sie das, Fra» Mühlberg?" sagte sie. Auch Frau
Mühlberg schien etwas verdutzt

„Wahrhaftig, ich kenne mich uiiht ans in all dieser
Weitschweifigkeit," erividerte sie; „ich denke, wir uersliche»
es noch einmal,"

Sie nahuien das schwierige Dokument wieder vor und
rersnihten es noch einmal. Frau Holniseu hatte Kopf-
schmerze», daher mußte Frau Mühlberg die Vorleserin
mache»; wenn mail es recht bedachte, war diese vielleicht
auch die Runenknudigere von beide». Tenn rund und dick
und solide, wie fie da saß, schien sie eine Frau, die ein Stück
in der Welt herumgekommen.

Zum Glück hielt Fanny sich ruhig; sie saß auf ihrem
Liiblmgsplatz am großen Nähtische neben Mamas Stuhl und
arbeitete an einem Kleide für ihre Rosalie.

Frau Mühlberg begann:

„Die geschiedene Gattin des Exam. juria Andr. Holmsen
hgt sich an den Unterzeichneten mit dem Ersuchen ge-
wendet, bei dem Amtmann daraus hin anzutragen,
daß ihrem vorgenannten Manne durch obrigkeitliche Reso-
lution auferlegt werde. Beider gemeinsaniem Kinde Fanny,
sur deren Alter ein Zeugniß des Pastors beiliegt, «inen
jährlichen Sustentationsbeitrag auszusetzen. Dieies Kind
sollte nämlich der Vater insolge des der Auflösung der Ehe

ersten Blich einen „Neuen 'Kurs" der Sozialreforin zu ver-
heißen schienen, und ,die.ganz geeignet waren, eine nicht
durchaus zielbewußte.„Partei in-.Verwikrnug zu bringen.
Wir hatten alle Parteien gegen uns und mußten Front
machen gegen alle und — unser Kurs blieb immer der
alte — vorwärts, vorwärts zum Sieg!

-- Dev 2.0,iFebruar sprengte die Koalition der Mok-
Uoiisparteien, genannt Kartell, zerbrach das Sozialisten-
gesetz und warf das 'Postament um, auf welchem der
größte der Demagogen des Kapitalismus — Fürst Bis-
marck sein Ich aufgepflanzt und eine Fainilieiidynaslie,
mit erblichem Hausmeierthum zu begründen versucht halte.

Am 18. März — für Gewalthaber ein„ominöses
Düimu — wurde das Urtheil des. 20. Februar an dem
Urheber des Sozialistengesetzes und der Millionärzüchtungs-
Polstik vollstreckt: er kvikrde lebendig begraben.
Die Nemesis hatte ihn ereilt, und was sie. noch übrig ließ
an' Sympathie für den Gefallenen,' das zerstörte dieser
selbst in wahnsinniger Verblendung. Nie hat falsche, mit
allen Listen, Kniffen und Künsten zu ungeheuerlichen
Dimensionen aufgeblähte Größe ein jäheres und schmähe
sicheres Ende genommen.

Und dem Schöpfer mußte sein Werk folgen. Nach-
dem das Fiasko der internationalen Ar
b eit e rsch utz-K onferenz aller Welt klar geworden
war, und nachdem die deutsche Sozialdemokratie noch, gemäß
dem Beschluß desJNternationalenArbeiterkongresses zuParis,
trotz aller Hindernisse von oben, trotz des verzweifetten
Widerstandes der Unternehmersippe den ersten Mai
auch für Deutschland zum Festtag der Arbeit er-

zii Grunde liegenden Vertrages, dessen Abschrift mitsolgt,
ebenso gut wie die übrigen Kinder versorgen; allein da er
keinen Schritt gethan hat, dieser Pflicht nachzukommen, weiß
sich die Mutter, welche hier in dürjtigen Umständen lebt
keinen anderen Ausweg, als um eine amtliche Resolution in
Betreff des jährlichen Beitrages anzusuchen.

Wenngleich er es als zweifelhaft betrachtet, daß die
Sache durchführbar sei, hat der Gefertigte es dennoch Frau
Holmsen nicht abschlagen wollen, dem Herrn Anitmami
dieses Ansuche» zuzustellen, wobei noch bemerkt wird, daß
besagter Herr Holmsen sich z. Z. in Christiania") ans-
halten soll.

Stadtvogtei zu Kristiansborg, den 14. Dez. 1664.

D. Broch.

An den

Herrn Amtmann im Oberlands-Amte,"

„Wird dom Herrn Stiftsamtmann in Christiania zur
Verfügung gestellt.

Oberlands-Amt, 19. Dezbr. 1864.

I» Abwesenheit des Amtmanns:
Hj. Aye.

„Wird ’ samt den Beilagen an de» Stadivogt von
Chriftiailia zu vorbereitender Behandlung geschickt
Stift von Christiania, 80. Januar 1866.

Im Auftrag: Joh. Knap."

„Wird dem Stadtdiener Engh zu der gewöhnlichen Be-
handlung übergeben.

Unterv. v. Chr., 9. Febr. 1865.

Für den Unteroogt: Edv. Olafse»."

I") Die ungleichmäßige Schreibung von Christiania, Kristiania.
Kristiansborg ist zu charatteristisch sur das Schwanken der
Bureaukratie zwischen Altem und Neuem, um nicht getreu bei-
behalten zu werden.	Anm. d. Ueb.

hoben hatte,, an dem die Arbeiter aller Länder den
internationalen Proletarierbinid feiern —
wurde am 1. Oktober das Sozialistengesetz sang- und
klanglos — nicht begrabe», das wäre zu viel Ehre ge-
wesen, nein verscharrst bei Seite geschafft, wie irgend ein
Ding, das. Ekel einflößt. —

M« Sozialdemokratie hielt am 1. Oktober Rückschau'
und Heerschau — und am Jahrestag der Entdeckung von
Amerika — 12. Oktober — träfen die Erwählten der
deutschen Arbeiterklasse zum Ko-ngreß in Halle zu-
sammen, der am 18. Oktober, dem Jahrestag der Völker-
schlacht von Leipzig, seine Aufgabe: die Reorganisasion der
Partei vollendete.

Die wunderbare Einkracht, die aus dem Halleschen
Kongreß waltete, zerstörte die thörichten Hoffnungen unserer
Feinde auf selbstmörderischen Bruderzwist der deutschen
Sozialdemokratie — dennoch wurde das alberne Lügen-
märchen von „Spaltungen" von 'der gegnerischen Presse
gedankenlos weiterverbreitet. Unsere Femde nennen das
„geistigen Kamps".	»

Cs ist zwar eine alte und gute Ersahrungsregel, daß
man den Feind nicht verachten soll; es ist aber wirklich
schwer, unsere Feinde nicht zu verachten. Haben sie
jenials den Versuch gemacht, unsere Ziele vorurtheilslos
ins Auge zu fassen, mit unserem Programm sich bekannt
zu machen? Haben sie jemals die allgemeinen Verhält-
nisse und die Fragen, um welche der politische Partei-
kainpf sich drehst zu erforschen getrachtet? Haben sie
jenials Ernst, Würde, Muth in dem Kampfe mit uns ge-

„Wird dem Herrn Uniervogt mit dem Bemerken zu-
gestellt, daß der Betreffende d. Z. sich in Kristiansborg
aushäli.

' Christiania, 6 März 1865.

Ehr. Engh."

„Wird dem Stiftsamt. von Christiania mit ehrerbietiger
Hinweisung aus Vorstehendes zurückgeschickt.

Uiitervogteiamt von Christiania, den 8. März 1865.

P. Rawm,.
provisorischer Leiter."

„Wird dem löblichen Oberlands-Amte zurückgestellt.

Stfftsauit von Christiania, den II, März 1865.

Im Auftrag: Joh. Knap."

„Wird an den Herrn Stadtvogt in Kristiansborg mit
dem Ansuchen geschickt, über den Vater des Kindes die üb-
lichen Aukkläruuge» zu geben.

Oberlands-Amt, de» 15. März 1865.

Krohn."

„Andreas Holmsen ist 89 Jahre alt, nicht beim Militär,
arbeitsfähig, doch- augenblicklich ohne festen Erwerb.

Ich kann nicht umhin, beizufügen, daß mir berichtet
wurde, er wünsche ailSzuivandern. Die Beilage» folgen.
Etablvogtei zu Kristiansborg, den 21. März 1865.

D. Broch."

An den

Herrn Amtmann des Oberlaiides-Dmtes."

„Wird dem Herrn Stadwogt in Kristiansborg mit
dem Anstichen zurückgestellt, Frau Holmsen bekannt zu geben,
daß das Amt nach erfolgter Einsichtnahme in den beigelegten
Kontrakt nicht findest daß es Mittel giebt, ihren geschiedenen
Gatten z» irgend einem Beitrag zur Erhaltung ihres Kindes
zu verpflichte»,

Oberlands-Amt, den 26. März 1885.

Krohn."

4. Titelseite der ersten Nummer des in den „Vorwärts" umgewandelten „Berliner Vottsblatt". 1. Januar 1891