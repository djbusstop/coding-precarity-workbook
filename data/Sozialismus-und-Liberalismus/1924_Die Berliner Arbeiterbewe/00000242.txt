﻿228

„Hausbesitzer". Doch fehlt es auch nicht an Fällen, wo die Aussonderung
bestimmter Bezirke für die Vertretung durch einen Hausbesitzer die Auf-
stellung sozialdemokratischer Kandidaten unmöglich oder die Wahl von
Sozialdemokraten ungültig machte. Ähnlich, wie es in dieser Hinsicht in
Charlottenburg und Schöneberg zuging, ging es auch in Mariendorf,
Wilmersdorf und noch anderwärts zu.

* *

*

Niederbarnim, der Reichstagswahlkreis der Vororte im Norden und
Nordosten Berlins, umfaßt keine Großstädte im Stil von Charlottcnburg,
Rixdorf und Schöneberg. 1905 zählten nur 7 Ortschaften dieses Kreises
über 10 000 Einwohner. An ihrer Spitze steht Lichtenberg-Friedrichsberg,
das Ende 1905 über 55 000 Einwohner zählte. 3hm folgen: Weißensee,
Boxhagen-Rnmmelsbnrg, Pankow, Reinickendorf, Friedrichsfelde-Karlshorst,
und Oranienburg. Wir unterlassen es, die Entwicklung der Vertreterschaft
dieser Orte in ähnlicher Weise zu verfolgen, wie bei den Hauptorten des
Wahlkreises Teltow - Beeskow - Storkow - Charlottenburg. Nicht, daß sie
weniger interessant wäre, als jene, aber sie bietet prinzipiell nur das gleiche
Bild. So hat Lichtenberg-Friedrichsberg, wenn auch nicht in
gleichem Grade, mit ähnlichen Schwierigkeiten zu kämpfen, wie Schöneberg,
und konnte daher bis 1905 nur 8 von 15 Mandaten der dritten Klasse
erobern. In Weißensee dagegen waren am Abschluß unserer Epochen
7 von 8 Mandaten der dritten Klasse, in Boxhagen-Rummelsburg
nnd Pankow je die dort zu vergebenden 8 und in Reinickendorf die
6 Mandate der dritten Wählerklasse im Besitz der Sozialdemokratie. In
Friedrichsfelde-Karlshorst, welche Orte einen starken Prozentsatz bemittelter
Einwohner und auch viele am dortigen Rennplatz interessierte Geschäfts-
leute beherbergen, waren nur erst 2 Mandate erkämpft, und ganz unvertreten
ist die Sozialdemokratie noch im spießbürgerlichen Oranienburg. Von
kleineren Orten sind Erkner, Friedrichshagen, Borsigwalde-Wittenau und
Waidmannslust-Lübars mit je 3 Mandaten zu nennen. Je 2 Gemeinde-
vertreter hatten neben Friedrichsfelde-Karlshorst noch Herzfelde, Mahls-
dorf, Oberschöneweide und Schönbeck-Fichtenau und je 1 Schönfließ,
Stralau und Tegel.

c)	Das kommunale Wirken der Sozialdemokratie.

Was nun die Tätigkeit der Sozialdemokratie in den Gemeinden an-
betrifft, so haben es die sozialdemokratischen Vertreter selbstverständlich
nirgends am guten Willen fehlen lassen, im Sinne der sozialdemokratischen
Grundsätze auf die Gemeindeverwaltungen einzuwirken. Aber durchgängig
einer Mehrheit gegenübergestellt, die durch den Wall der preußischen
Klassengesehgebung davor geschützt war, einer sozialdemokratischen Mehr-
heit weichen zu müssen, und sich zum Äberdruß in Notfällen noch durch
die Äberwachungsvollmachten der Regierung gedeckt wußte, stießen sie dabei
auf einen Widerstand, der oft absolut unüberwindlich schien, und in vielen
Fällen hat sich, namentlich in der ersten Zeit, das Klassenwahlsystem in der
Tat als ein Eisenpanzer für Klassenvorurteil und Klassenegoismus er-
wiesen. Aber die Tätigkeit der Sozialdemokratie in den Kommunen ist
trotz dieses großen Hemmnisses nicht wirkungslos geblieben. Schon die