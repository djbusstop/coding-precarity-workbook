﻿234

Knechtsgesinnung nennt, vor allem am Herzen lag. Aber nur ausnahms-
weise konnte solche Einheitlichkeit erzielt werden. Denn ein Recht, das man
reklamierte, nun auch durchzukämpfen, dazu fehlte den Liberalen meist teils
der Mut und teils der nötige gute Wille. Das zeigte sich schon Mitte
der achtziger Jahre, als im März 1884 der damalige Oberpräsident Achen-
bach dem Stadtverordnetenvorsteher kurzweg verbot, einen Antrag Singer
und Genossen, gemäß dem die Stadtverordnetenversammlung bei Reichstag
und Landtag um zahlengemäße Vertretung Berlins in diesen Körperschaften
petitionieren sollte, auch nur auf die Tagesordnung zu setzen. Erst ent-
rüstete man sich über diesen Eingriff, beschloß, alle Schritte zu unternehmen,
rim das Recht der Versammlung zu wahren, und schließlich ging man mit
der lahmen Erklärung, daß das inzwischen in Kraft getretene Zuständigkeits-
geseh der Gemeindevertretung das Recht sichere, gegen Beanstandung oder
Verhinderung von Beschlüssen das Oberverwaltungsgericht anzurufen, über
den Antrag, die Petition aufs neue auf die Tagesordnung zu setzen, „zur
Tagesordnung" über. Das heißt, man beließ es faktisch bei dem, was der
Oberpräsident gewollt hatte und stand freiwillig von dem ab, was man für
sein Recht erklärt hatte. So ging es auch, als 1898 die Regierung der
Stadtverordnetenversammlung die Wahl von Paul Singer in die städtische
Schuldeputation verbot und der Magistrat sich dabei zu der ihm auferlegten
Rolle als Exekutor des „Staatswillens" hergab, statt ihm als ausführendes
Organ des Gemeindewillens entgegenzutreten. Erst entrüstete man sich, protestierte
einmütig und beschloß, da man der Sozialdemokratie das Recht auf eine ihrer
Stärke entsprechende Vertretung in der Schuldeputation nicht bestreiten mochte,
keinen Ersatz für Singer zu wählen. Dann aber lehnte man den vom sozial-
fortschrittlichen Abgeordneten Preuß gestellten Antrag ab, dem Nichtwählen
durch eine ausdrückliche Begründung den Charakter eines Protestes zu geben,
und als es späterhin aufs neue zur Wahl der Schuldeputation kam, zog man
es vor, überhaupt gar nicht erst einen Sozialdemokraten zu wählen, „um
keinen Schlag ins Wasser zu tun". Ebenso kam es zu wenig mehr als
einem ersten Anlauf von Bürgermut, als 1901 der König der Wahl des Stadt-
rats Kauffmann zum zweiten Bürgermeister kurzweg die Bestätigung ver-
sagte. Erst Wiederwahl Kauffmanns, Beschwerdeversuch, Verzicht auf
Wahl eines Ersatzes, und dann war's aus. Als am 27. Februar 1902 der
sozialdemokratische Stadtverordnete Borgmann anregte, die von der Re-
gierung verlangte kommissarische Verwaltung des Bürgermeisterpostens Herrn
Kauffmann zu übertragen, gaben Zeichen des Unwillens aus der Mehrheit
ihm zu verstehen, daß ein energischer Kampf ums Recht nicht in ihrer Ab-
sicht lag. Der plötzliche Verzicht Kauffmanns und sein wenige Wochen
darauf erfolgter Tod verhinderten, daß es zu einem völligen Rückzug der
Stadtverordnetenmehrheit kam. Aber daß, wenn Kauffmann ihr nicht den
Gefallen getan hätte, selbst abzutreten, sie auch hier umgefallen wäre, steht
außer jedem Zweifel.

Dieselbe Halbheit zeigt die freisinnige Mehrheit in der kommunalen
Steuerpolitik. Ängstlich, als handle es sich um Tod und Leben, vermeidet
sie es, die kommunale Zuschlagsrate zur staatlichen Einkommensteuer über
100 °/o steigen zu lassen, weil für die Überschreitung dieses Satzes die
Genehmigung der staatlichen Aufsichtsbehörde eingeholt werden müsse und
dies die Preisgabe des Grundsatzes der Selbstverwaltung der Gemeinde