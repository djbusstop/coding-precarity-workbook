﻿322

An die Delkgirtkn des DUteitagkS.

Werthe Genossen!

Punkt 6 der provisorischen Tagesordnung des. Parteitages
äat ein Referat des Genossen I. Arier vorgesehen „Ueber
Genossensä)aften, Boykott und Kontrollmarken.

Die Unterzeichneten haben nun zum Theil das System
der Kontrollmarke als Kampfmittel im gewerkschaftlichen Kampfe
in Anwendung gebracht, zum Theil beabsichtigen sie. es in
nächster Zeit zu thun.

Wir halten uns daher für verpflichtet, den Delegirlen zum
Parteitag einiges Material zur besseren und leichteren Jnfor-
mation zu unterbreiten.

Im Lause dieses Jahres erschienen in dem wissenschaft-
lichen Organ unserer Partei „Die Neue Zeit" eine Reihe von
Artikeln wider und für die Kontrollmarke. Wir bringen diese
Artikel des Gegners sowohl als auch der Anhänger, damit sich
die Genossen an der Hand des Vergleiches ein umfassendes
-Urtheil bilden können.

Die Gewerkschaft der Hutmacher war die erste, welche die
Kontrollmarke anwendeten, wir bringen daher eines ihrer Flug-
blätter in Vordruck.

Wir lassen den drei Artikeln auö der „Neuen Zeit" einen,
kurzen Auszug über die „Bedingungen beim Boykotten", er-
schienen in der New-Aorker Zeitung. Typographical-Umon
Nr. 6.*) außerdem die gegen eine Stimme angenommene .Reso-
lution des Halberstädter Gewerkscvafts-Kongreises folgen.

*) Siche S v. Wultershaulen. Die Nordamerika,,. Gewerkschaften.
S. 255.

die innere Gliederung nach
Werkstätten, das System
der Werkstättenkontrolle und
der Vertrauensmänner in
den Werkstätten wird syste-
matischer ausgearbeitet, und
das kommt auch in der
systematischen Führung der
Streiks in die Erscheinung,
bewährt hier seine großen
Vorteile. Im September
1897 stellen Former und
Berufsgenossen in den
großen Maschinenfabriken
Berlins die Arbeit ein und
nötigen den Fabrikanten-
bund, der bis dahin keine
Arbeiterorganisation aner-
kennen wollte, zum ersten
Male vor dem Berliner
Einigungsamt in der Ge-
stalt seiner Führer den
Leitern der Gewerkschaft
der Arbeiter Auge in Auge
gegenüberzutreten. Denn
Andauern des Stteiks der
Former hätte Stillsetzung der Fabriken mit Brotlosmachung von 27000
Arbeitern geheißen, und diese Möglichkeit hatte die Presse veranlaßt, sich
mit dem Konflikt eingehender zu beschäftigen.

Bedeutungsvolle Erfolge wurden in den Jahren 1898 und 1899
namentlich in den Baugewerben Berlins erzielt, sowohl was die Arbeits-
zeit als was die Lohnsätze anbetrifft. Damals war bei den Maurern der
Lohnsatz meist 55 Pfennig die Stunde und die Arbeitszeit 9^2 Stunden ini
Tag. Ende des Jahrhunderts beträgt er 65 Pfennig die Stunde und die
Arbeitszeit ist 9 Stunden im Tag. In den Berichten über den Verlauf der
Kämpfe findet man eine fortlaufende Statistik der Verhältnisse auf den einzelnen
Bauten, die erkennen läßt, wie sehr die Organisation in jedem Moment über
die Sachlage auf dem laufenden war und die wirksamsten Maßnahmen
treffen konnte. Das gleiche Bild zeigen die Berichte über die Kämpfe
anderer Gewerbe, und ein wahrhaft klassisches Bild der Werkstättenkontrolle
findet man in dem Bericht der Berliner Verwaltung des Verbandes der
Lolzarbeiter von 1904 über ihre Kämpfe in der Möbelindustrie, Kämpfe,
die im Verein mit solchen der Klavicrarbeiter und noch einiger anderer
Branchen der Holzindustrie, die Ausgabe des Verbandes für Streiks und
Aussperrungen in Berlin in jenem Jahr auf über eine Million Mark
bringen.

Das Jahr 1905 endlich sieht in Berlin, nachdem 1903 und 1904 schon
große Kämpfe in der Messingindustrie gespielt hatten, den großen Kampf in
der Elektrizitätsindustrie, der zur Aussperrung von 40000 Arbeitern —

155. Bericht an den Parteitag von 1892
über den Wert der Kontrollmarke