﻿

- 163 -

erhielt der Antrag, daß der „Vorwärts" ausschließlich Organ der Berliner
Organisationen sein sollte, keine Mehrheit. Aber in den Reihen der Funk-
tionäre der Berliner Organisationen war die Mehrheit doch für diese An-
träge. Anders im übrigen Deutschland. In Jena blieben die bezeichneten
Wünsche der Berliner samt und sonders unerfüllt. Durchgängig stimmte
die Mehrheit des Kongresses den betreffenden Vorschlägen des Entwurfs
zu. In einem Teil der Berliner Versammlungen, die sich mit dem Partei-
tag befaßten, ward auf eine Polemik Bezug genommen, die gerade um jene
Zeit zwischen der Redaktion des „Vorwärts" auf der einen Seite und den
Redaktionen der „Neuen Zeit" und der „Leipziger Volkszeitung" auf der
anderen Seite spielte. Gegensätze zwischen den bezeichneten Redaktionen
bestanden schon lange und sie wurden vielfach so aufgefaßt, als ob die
Redaktion des „Vorwärts" bezw. deren Mehrheit, „revisionistisch" gesinnt
sei, während die anderen beiden Redaktionen als marxistisch bekannt waren,
und diese Gegensätze galten in vieler Augen als gleichbedeutend mit ge-
mäßigt und radikal. Der Verfasser hält im konkreten Fall diese Gegen-
überstellung für irrig, aber da sie nun einmal in Berlin vorherrschend ge-
worden war und die Mehrheit der leitenden Parteimitglieder Berlins auf
der als marxistisch-radikal bezeichneten Seite standen, ward es begreiflicher-
weise von ihnen sehr unangenehm empfunden, daß im Berliner Organ das
umgekehrte Verhältnis obwalten sollte. Nicht zum wenigsten um dieses
Amstandes willen war man neuerdings wieder aus den Gedanken zurück-
gekommen, vom Parteitag für die Berliner die volle Verfügung über den
„Vorwärts" zu fordern. Das wurde nun abgelehnt, aber in der Besetzung
der Redaktion trat nach Jena doch eine Änderung ein.

Die damit verbundenen Vorgänge spielten während der letzten Monate
des Jahres 1905 und wurden damals in sensationeller Form vor die
Öffentlichkeit gebracht. Schon in Jena hatte August Bebel am ersten
Vcrhandlungstag namens des Parteivorstandcs erklärt (ProtokollS. 187/188),
daß dieser bereit sei, mit den Berliner Parteigenossen zu beraten, wie dem
auch von ihm für unhaltbar erkannten Zustand hinsichtlich der Redaktion
des „Vorwärts" am besten abzuhelfen sei. In dieser stünden Mehrheit
und Minderheit sich sehr gereizt gegenüber, und da die Berliner Genossen
zur Redaktionsminderheit das größere Vertrauen hätten, müßte gesucht
Werden, das Mehrheitsverhältnis zu ändern.

In der Tat standen sechs Mitglieder der Redaktion, nämlich
P. Büttner, Kurt Eisner, Georg Gradnauer, Jul. Kaliski,
Wilhelm Schröder und Leinrich Wetzker auf ziemlich gespanntem
Fuß zu den anderen vier Redakteuren: Leinrich Cunow, Paul John,
Karl Leid und Leinrich Ströbel. Vor allem hatten sich die Be-
ziehungen der vier politischen Redakteure Eisner und Gradnauer einer-
seits und Cunow und Ströbel andererseits so zugespitzt, daß ein gut
kollegialisches Zusammenarbeiten kaum noch möglich erschien. Irgendwie
mußte hier Abhilfe getroffen werden.

Als der Parteitag vorüber war, fanden zu diesem Behufe zunächst
Beratungen zwischen dem Partcivorstand und den leitenden Komitees der
Berliner Parteiorganisation statt. Am zweiten Oktober 1905 fand eine
gemeinsame Sitzung statt, zu der die letzteren — die Preßkommission, die
Vertrauensleute, die Vorsitzenden der acht Wahlvereine, die Agitattons-

n-