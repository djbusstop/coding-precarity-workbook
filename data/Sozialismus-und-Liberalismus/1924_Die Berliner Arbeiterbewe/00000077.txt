﻿71

Sie belief sich im Jahre 1890
auf 3713, im Jahre 1905 auf
3287 Personen. Der Rückgang
entfällt jedoch erst auf die zweite
Lälfte der fiinfzehn Jahre. In
der ersten Zeit findet im Gegen-
teil noch Zunahme statt, und
zwar so erheblich, daß 1892
4364, 1893 sogar 4663 Per-
sonen unter Kontrolle standen.
Seit Ende des Jahrhunderts
aber geht die Zahl der „Kon-
trollmädchen" ununterbrochen zu-
rück. Von 1901 ab ist die
Entwickelung die folgende:

Unter fittenpolizeiliche Kon-
trolle waren gestellt

flrbcitcr=Gc$undl)eit$=ßibliotl)ck

JT hirftwagegcten unter Leitung van 's

Dr. mcd. Zadch	'V

in der Arbeiterfamilie

Von

Dr. 3uUan pVarfeuse

1901 . .	4147 weibl. Pers.
1902 . .	3976	„	„
1903 . .	3815	„	„
1904 . .	3709	„	„
1905 . .	3287	„	„

preis 20 Pfennig

Berlin 1908

Verlag: Buchhandlung Vorwärts
lLans Weder.Berlin)

Berlin SW. 68



Für denjenigen, der den
Gesundheitsschutz, den die sitten-
polizeiliche Kontrolle erwirkt für zz. Titelblatt einer im Verlag der Buch-
sehr problemattsch, dre durch sie .	m ..	...	....

bewirkte moralische Entwürdi- ^»"lung Vorwärts erschienenen Broschüre

gung aber für ebenso zweifellos

wie ruchlos hält, ist dieser Rückgang höchst erfreulich. Ob ihm ein ebensolcher
Rückgang der weiblichen Prostitution überhaupt entspricht, muß dagegen als
zweifelhaft bezeichnet werden. Die sogenannte Sittenpolizei hat immer nur
einen Teil der vom täglichen oder häufigen Verkauf ihres Körpers lebenden
Frauen unter ihre Kontrolle genommen. Wie viele sie hat laufen lassen, weil ihr
teils die Lust, teils aber auch geradezu die Möglichkeit fehlte, auf dem ihr
unterstehenden Gebiet radikal vorzugehen, darüber gehen die Schätzungen aus-
einander, doch herrscht Einstimmigkeit darüber, daß die wirkliche Zahl der
Prostituierten die der eingeschriebenen Prostituierten um ein vielfaches —
manche nehmen an, bis auf das Achtfache — übersteigt. Die Polizei-
statistik läßt uns also bezüglich des wirklichen Standes der Dinge auf
diesem Gebiete im dunkeln. Es sprechen aber viele Amstände dafür, daß
die weibliche Prostitution in Berlin, wenn nicht der absoluten Zahl nach
zurückgegangen, so doch im Verhältnis zur Bevölkemngszunahme zurück-
geblieben ist. Ein erheblicher Teil der engen Straßen, die mit ihren ver-
fallenen Läufern wahre Brutnester der Prostitution waren, sind von der
Bildfläche verschwunden, dagegen haben sich die Erwerbsmöglichkeiten für
Frauen sehr bedeutend vermehrt und im Durchschnitt auch etwas ver-
bessert, und damit hat sich das Selbstgefühl der Frauen, insbesondere sowei
sie von der Arbeiterbewegung erfaßt sind, ganz wesentlich gehoben,
muß auf die Dauer der Prostitution, soweit sie sich aus diesen o