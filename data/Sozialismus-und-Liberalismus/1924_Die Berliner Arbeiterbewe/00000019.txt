﻿14

6 und 7. Verstorbene Redakteure des „Vorwärts"

doch findet ein Teil der Begründungsrede Calwers, worin dieser sich für
Kündigung des Meistbegünstigungsvertrags mit den Vereinigten Staaten
ausspricht, der dem Abschluß günstiger Landclsverträge im Wege stehe,
starken Widerspruch und hat eine Amcndierung der Resolution zur Folge.
Weiter beschließt der Kongreß, da das Verbot der Verbindung von poli-
tischen Vereinen durch Reichsgeseh vom Dezember 1899 in Wegfall ge-
kommen war, ein von I. Auer begründetes neues Organisations-
statut der Partei und beschließt auf Antrag Bebel, daß in denjenigen
deutschen Staaten, wo das Dreiklassenwahlsystem bestehe, die Genossen
bei den nächsten Wahlen mit eigenen Wahlmännern in die Wahl-
agitation einzutreten hätten. Dem Parteitag war eine erste Konferenz
sozialdemokratischer Frauen Deutschlands vorausgegangen, die ein
Organisationsstatut für die sozialdemokratischen Frauen Deutsch-
lands schuf.

Lübeck (22. bis 28. September 1901). Es wird beschlossen: eine von
Südekum beantragte und begründete Resolution zur Wohnungsfrage,
eine Resoluüon Bebels gegen den bekannt gewordenen Zolltarif-Entwurf
der Reichsregierung, eine Resolution Bebel, welche den Vertretern der
Partei in den gesetzgebenden Körpern der Einzelstaaten die Ablehnung
der Gesamtbudgets überall dort zur Pflicht macht, wo nicht besondere
Ausnahmeverhältnisse zur Zustimmung zwingen, eine Resoluüon Fischer
und eine Resolution Bernstein über das Recht der Parteimitgliedschaften,
gewerkschaftlichen Disziplinbruch und Sonderbündelei durch Aus-
schluß zu beantworten (Angelegenheit der Akkordmaurer in Lam bürg).
Ein Vortrag, den Ed. Bernstein im Sozialwissenschaftlichen Studcnten-
verein zu Berlin über die Frage gehalten hatte, ob wissenschaftlicher So-
zialismus möglich sei, gab zu einer längeren Debatte Anlaß und endete mit
Annahme einer Resolution Bebel, die erklärt, daß Bernstein durch ein-
seitige Kritik der Partei sich in eine zweideutige Position gebracht und die
Mißstimmung eines großen Teils der Genossen erregt habe, der Parteitag