﻿An die werktlMige Bevölkerung Berlins!

Arbeiter! PchrLekgenoffen! Der Dierboykott

bauert fort. Die Verhandlungen wegen Beendigung des-
selben mußten abgebrochen werden, weil die Ningbrauercien
eine Bedingung stellten, deren Annahme mit der Ehre der
Arbeiterschaft unvereinbar ist. Trotzdem die Vertreter
des Brauerringes zugeben mußten, datz sie die Wirkung
Deß Boykotts bitter empfinden, hatten sie die Stirn, zu
fordern, daß der Friedensschluß ihrerseits davon abhängig
gemacht werde, dasi 33 Arbeiter auf alle Zeit auS den
Betrieben der Ningbrauer ausgeschlossen bleiben.

Arbeiter! Parteigenossen! Ihr wißt, daß am
16. Mai d. I., ohne jeden Anlaß seitens der betreffenden
Arbeiter. Hunderte aufs Pflaster geworfen wurden, durch
die Willkür der Brauereiprotzen mit ihren Familien an
den Rand des Elends gebracht werden sollten. Die
Solidarität der Arbeiter allein hat dafür gesorgt, daß
die Absicht der Ringbrüder zu Schanden wurde. Jetzt,
nach monatelangcr Aussperrung, nach monatelangen
Entbehrungen sollen breiunddrtitzig Arbeiter dauernd
dem Elend, für immer der Erisienzlofigkcit. dem
langsamen Zugrundegehen überliefert werden. —
dreittnddreitzig Mann. von denen nicht ein Einziger
Schuld nn dem Boykott trägt. — dreiunddreißig
Arbeiter von denen zwciundzwanzig vcrheirathct
sind, die zweiundvierzig Kinder zu ernähren haben.

An der. barbarischen Aussperrung so vieler braver,
unschuldiger Arbeiter am 16. Mai hatte der Braucrring
nicht genug. Seine ursprüngliche Absicht, mit jenem Schlage
vom 16. Mai die Arbeiterorganisationen zu vernichten,
tritt auch, bei seinem neuesten -Versuch, klar zu Tage.
WcS konnte den 33 Arbeitern nachgesagt werden —
welches Verbrechen hatten sie begangen? — Nichts!
— Sie waren sämmtlich organisirt und größtentheilS
jahrelang in den Brauereien thätig: eln großer Theil
von ihnen bekleidete in der Organisation ein Ver-
trauenSamt. und sie haben nichts gethan, als durch
die Organisation für die Interessen ihrer Arbeitö-
genossen einzutreten

Arbeiter! Parteigenossen! Wir wissen. daß wir
in Eurem Sinne gehandelt haben, als wir dem ungeheuer.

lichen Ansinnen des Brauerringes ein empörtes' kurzes
Nein entgegensetzten und die Verhandlungen abbrachen.
Die Arbeiter Berlins konnten und wollten einen ehrlichen
Frieden schließen; niemals aber werden wir rrnsereHarid
dazu bieten, niemals werden die klassenbewußten, in den
Gewerkschaften und der Sozialdemokratie organisirten
Arbeiter dulden, daß ein ehrloser Friede geschlossen
wird. Und ein ehrloser Friede wäre eS gewesen, wenn
wir auf die Bedingungen unserer Gegner eingegangen
wären.

Nun ist die Entscheidung getroffen. Von Neuem
treten wir in den Kampf ein. Der Boykott muß mit
verschärfter Energie fortgeführt werden. Die Parole:

Kein Tropfen Ringbier!

mich mit unwiderstehlicher Mach! zur AnLsührung ge-
langen.

Arbeit«»! Parteigenossen! Ihr habt scheu
oft in Zeiten dcr Noth und des Kampfes zusammen-
gestanden und gezeigt, waS Eure Solidarität vermag.

Die Millionen des Kapitalistenringes bleiben möcht,
los. wenn Ihr geschlossen zusammenhaltet.

An Euch ist cs. wieder einmal zu zeigen, war die
orgamsirte. sozialdemokratische Arbeiterschaft vermag. Kie
Arbcllcrschaft der ganzen Welt sicht aus Euch!
Beweist. Sah Ihr dcr Achtling Ser Klnsfcngenossen
würdig seid!

Jeder Einzelne von Euch muß seine ganz« Krast
answende» um den Boykott zur vollen Wirkung zu
bringen. Wir fordern Euch auf. mit verdoppelter Kraft
Haud ans Werk zu legen und die zur Organisation,
Ücbcrwachung und Tnrchführnng de» Boykott» er-
sorderlichei, Maßregeln energisch zu unterstützen.

Der Kamps ist uns ausgezwunge» worden. Die
Berliner Arbeilcrschast hat den Handschuh aufgeuomme«.
und sie wird den Kamps durchführen bis zum Ende!

Arbeiter! Euer Klassen«Interesse nicht blos.
Eure Klasse»,Ehre ist im Spiel. Do giebt c» leinen
andere» Gedanken als: Sieg!

vorwärts zum Sieg! Kein Hropfen Hiingöier! Koch der Boykott!

Die Boykott-Rommission.

R-dakUon und Vcrl-gc H. Mallulcu. 80. Wiongelslr. ILt. - Drug: Maurer L Dimmlck. Sv.. Llifabech-Uler 55.'

161.	Flugblatt der Boykott-Kommission. SituaLionsbericht

22*