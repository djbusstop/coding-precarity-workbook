﻿97

Zähk-Karte für den Wahl-Bezirk M

fiftenfütir«: ,.<2Zau.~7

Einge-  schriebene  Wühler.	Ab-  gegebene  Stimmen.	Sozial-  demo-  kraten.	Deutsch-  frei-  sinnige.	Konser-  vative.	Christlich  soziale.	Centrum.	Zer-  splittert.	Ungültig.
K/6		$0	66	//	1	*6	6	£

DIrft Karle gilt »nr für das Endresultat.

- !

45. Ausgefüllte Zählkarte von einer Reichstagswahl

Nach alledem ist der Geist der Kreisorganisation als fast durchgängig
zentralistisch zu bezeichnen, und dies spiegeln auch die Statuten der ört-
lichen Wahlvcreine dadurch wider, daß sie sich in den Hauptpunkten kurz-
weg auf das Statut des Zentralwahlvereins berufen. So lauten die §8 5,
7 und 8 des Statuts des diesem Kreise angehörenden Wahlvereins
Schöneberg:

„Die Erwerbung und der Verlust der Mitgliedschaft, die Löhe
des Eintrittsgeldes und die monatlichen Beiträge werden nach den
88 6,7 und 8 des Statuts des Zentralwahlvereins bestimmt." — „Pflicht
eines jeden Mitgliedes ist es, sich an allen Parteiarbeiten des Vereins
zu beteiligen (88 9 und 17 des Statuts des Zentralwahlvereins)." —
„Die Verwaltung der vom Vereine aufgebrachten Gelder wird nach
8 15 des Statuts des Zentralwahlvereins geregelt."

Die Abhängigkeit der örtlichen Wahlvereine vom Zentralwahlverein
wurde auch noch dadurch zum Ausdruck gebracht, daß die ersteren sich mit
Bezug auf ihren politischen Zweck gleichfalls kurzerhand auf das Statut
des letzteren beriefen. Der Paragraph 1 der örtlichen Wahlvereine im
Kreise Teltow-Beeskow-Storkow-Charlottenburg lautete: „Der Verein,
dessen Sitz ..... ist, dient der Förderurig der Sozialdemokratie gemäß
§ 2 des vorstehend abgedruckten Statuts des Zentralwahlvereins." Die
Bestiinmungen, die das eigene Leben des örtlichen Wahlvereins betrafen,
waren auf das Notwendigste beschränkt. Sie betrafen die Zusammen-
setzung und Wahl des Vorstandes sowie der Revisoren, die Festsetzung
des Tages der monatlich abzuhaltenden regelmäßigen Vereinsversammlungen
und der vierteljährlich abzuhaltenden Generalversammlungen, die Behandlung
eingebrachter Anträge und den Leimfall des Vereinsvermögens an den
Zentralwahlverein im Falle der Auflösung des örtlichen Vereins.

Soweit war alles streng zentralistisch geordnet. Indes schon die
Bestimmung, daß jeder örtliche Wahlverein unbekümmert um die Zahl
seiner Mitglieder drei Delegierte in die Generalversammlung des Kreises
entsenden durfte, trug in das Statut ein Stück Föderalismus hinein, und
dem Prinzip des Föderalismus entsprach es auch, daß der örtliche Wahl-
verein in bezug auf die Wahl seiner Leiter und Beamten vollständig
unabhängig war; sie waren seine Angestellten und nicht die des Zentra -
Bernstein, Berliner Geschichte. III.	7