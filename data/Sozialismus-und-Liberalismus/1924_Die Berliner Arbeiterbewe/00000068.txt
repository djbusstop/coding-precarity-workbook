﻿Haus-Ordnung

für das

em

Wicfenstratze 55-59.

I.	Da-3 Asyl darf von einer und derselben Person innerhalb vier Wochen nicht öfter als
viermal hintereinander benutzt werden.

Ä. Die Benutzung findet statt:

&) im Witter (twia i. Oktober bis i. Avril) von Abcllds 5 bis Morgens ? llhr,
b) im Sommer (i. Avril bis i Ok-b-r) von Abends 6 bis Morgens 6 Uhr.

Wer das Asyl des Morgens früher verlassen will. hat dies Abends vorher bei der
Ausnahme zu melden.

3.	Di- S-MM-lh-Le wird !M Winter nm 3. im Sommer um 5 Uhr Nachmittags geöffnet.

4.	Betrunkene oder mit Ekel erregenden sichtbaren Krankheiten behaftete Personen werden
nicht in das Asyl aufgenommen.

5.	Die Angabe des Namens und der sonstigen persönlichen Verhältnisse wird von Personen,
welche das Asyl benutzen wollen, nicht gefordert. Dieselben sind berechtigt, während ihres
Aufenthaltes im Asyl jede Auskunft in dieser Beziehung, mag sie gefordert werden von wem sie
wolle, zu verweigern. Eine Ausnahme hiervon findet nur statt bei Vornahme von Zählungen,
welche zu statistischen Zwecken gesetzlich vorgeschrieben sind.

6.	Wer das Asyl benutzt, har sich während seines Aufenthalts daselbst den Anordnungen
der Hausbeamten zn fügen.

7.	Nach den» Eintritt und vor dem Verlassen mntz jeder Besucher sich Hände und Gesicht
waschen. Die Benutzung der Badeeinrichtnng wird dringend empfohlen. In den Fällen, wo der
Hausinspektor es anordnet, must der betreffende Asylist ein Bad nehmen. Während des Badens
werden die Kleider desinsicirt.

8.	Jedem Besucher wird seine Lagerstätte für die Nacht angewiesen, welche er sPälksttW

in Witter ca 8 Uhr. in Sommer am 10 Uhr -!»zm>-bm-i> ha». In den Dche-mi-« ist

jedes rvhestörende Geräusch zu vermeiden.

9.	Oberkleider und Schnhzeug sind beim Einnehmen der Lagerstätte abzulegen und die Kleider
am Bettpfosten aufzuhängen.

10.	In der Ehhalle. welche auch zur Ansbesserung von Kleidungsstücken und Schuhzeug
benutzt werden kann, ist gesittete nicht zu laute Unterhaltung gestattet. Ausdrücklich V§rÜ0tekl

ist bei Strafe sofortiger Ausweisung aus dem Asyl alles Lärmen, Kartenspiel,
Labakrauchen und Branntweiutriuken.

II.	Das Waschen. Baden, sowie die Reinigung der Kleider findet nach Anweisung der Haus-
beamten statt.

12.	Abends wird eine Suppe mit Brot gereicht. Morgend Kaffee nebst einer Schrippe.

13.	Die Benutzung des Asyls ist unentgeltlich.

14.	Zuwiderhandlungen gegen die Hausordnung können vom HauSinspektor mit Ausweisnng
aus dem Hans bestraft werden.

Berlin, 1. Dezember 1898.

Der Vorstand

Ser Berliner üfyl-üerelns für Obdatölofe.

33. Plakat. Hausordnung für das Männer-Asyl des Berliner Asyl-Vereins

für Obdachlose