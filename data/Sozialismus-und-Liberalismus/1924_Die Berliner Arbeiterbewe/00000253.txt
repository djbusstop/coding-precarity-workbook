﻿239

P

<3

^/Sozialdemokratische Gemeindepolitik

£ Kommunalpolttirchc Hbhancllungen
( herausgegeben unter Leitung von Paul hlrseh

«

Kommunale

Arbeiterpolitik

Von Dugo l^ingemsnn

Preis 40 Pfennig

Berlin 1905

Verlag: Buchhandlung Vorwärts, Berlin SW. 68, Lindcnftr. öS

«rost Precran». Berlin-RatznSVorf



III



Arteil stets besonders hoch ge-
schätzt wurde. Aber im ganzen
war das geleistete Werk doch
Kollektivarbeit, in der viel
mehr steckt, als ein einzelner
oder einige wenige hätten leisten
können, und wenn sie Aniversal-
genies gewesen wären. Der
demokratische Charakter der
Sozialdemokratie bewährte sich
auch hier. Es gab keinen,
dem nicht Gelegenheit gegeben
wurde, auf dem Verwaltungs-
gebiet sich zu betätigen, für
das er besonderes Interesse,
besondere Sachkenntnis mit-
brachte. Ein Bild dafür gibt
die Liste, die Lirsch auf Seite
548 und 549 seines Werkes
über die Vertretung der Sozial-
demokratie in den verschiedenen
städtischen Deputationen, Kom-
missionen und Kuratorien gibt.

Man wird nicht einen der
sozialdemokratischen Stadtver-
ordneten dort vermissen, die in
der Zeit, für welche die Liste
gilt, die Fraktion der Sozial-
demokratie im Berliner Rat-
haus bildeten.

And was von dieser Fraktion gesagt worden ist, trifft den Verhält-
nissen entsprechend auch für die sozialdemokratischen Vertretungen in den
Gemeinden im Amkreise Berlins zu. Es ist im Wesen überall derselbe
Kampf, der von ihnen geführt wird, und er wird auch grundsätzlich im
gleichen Geiste geführt. Ferner liegt es auf der Land, daß die Zahl der
Vertreter nicht schlechthin als Maßstab für die Menge der geleisteten
Arbeit und ihren Wert genommen werden darf. Je nachdem entfällt, wo
die Vertretung aus nur wenigen Personen besteht, auf den einzelnen eben
mehr Arbeit und Verantwortung, als dort, wo viele die Aufgaben unter
sich einteilen können. And auch die Erfolge in der Verwaltung stehen in
verschiedenen Vororten nicht hinter dem zurück, was in Berlin erzielt wurde.
Es ist nicht die Wertfrage, die von einer Schilderung der kommunalen
Tätigkeit der Sozialdemokraten in den Vororten absehen läßt, sondern die
Anmöglichkeit, auf dem zur Verfügung stehenden Raum diese Tätigkeit im
einzelnen zu beleuchten. Oft war der Widerstand, dem die Sozialdemo-
kraten mit ihren Forderungen begegneten, noch erheblich kleinlicher als in
Berlin, hier und dort wäre aber auch von mehr Verständnis und Ent-
gegenkommen zu berichten. Gemeinden, die über reiche Mittel verfügen, wie
z. B. Charlottenburg und Schöneberg, lassen sich unter Amständen leichter

108. Titelblatt einer Broschüre der Buch-
handlung Vorwärts über Kommunale
Arbeiterpolitik