﻿27

ausgeübt, dessen Entwurf im
Januar 1896 zum zweiten
Mal an den Reichstag ge-
langt war und nun endlich
Gesetz werden sollte. Die
sozialdemokratische Fraktion
tat ihr bestes, in das Bürger-
liche Gesetzbuch und das mit
ihm verbundene Einfüh-
rungsgesetz soviel Schutz
der ärmeren Volksklassen
und speziellderArbeiterklasse
hineinzubringen, als nur
möglich. Da für die An-
nahme des ganzen Gesetzes
unter Umständen es auf jede
Stimme ankam, fanden sie
soweit Entgegenkommen, daß
verschiedene arbeiterfeindliche
Bestimmungen aus dem Ent-
wurf entfernt wurden und in
bezug auf andere Fragen —
wie eben in bezug auf dasVer-
einsrecht — Abhilfe durch
besondere Gesetze zugesichert
wurde. Die Bemühungen,
durch das Bürgerliche Gesetzbuch eine Beseitigung der halbfeudalen Gesinde-
ordnung Preußens und anderer Staaten zu erlangen, scheiterte am Versagen der
Freisinnigen. Wohl nahm auf Antrag der Sozialdemokratie der Reichstag
am 11. Dezember 1896 fast einstimmig eine Resolution zugunsten baldtun-
lichster einheitlicher Gestaltung des Arbeitsvertrags im Deutschen Reich
an, aber, wie so manches andere „bald", hat sich auch dieses noch nicht
„tun" lassen.

Das Bürgerliche Gesetzbuch ward am 1. Juli 1896 vom Reichstag
mit 222 gegen 48 Stimmen angenommen. Die Sozialdemokraten stimmten
mit Nein. Sie verkannten nicht, daß in der Vereinheitlichung des bürger-
lichen — man sollte eigentlich sagen, des sozialen — Rechts ein großer
Fortschritt lag, der auch für die Arbeiterklasse von Bedeutung war. Aber
das Gesetzbuch läßt soviel Anrecht gewordenes Recht absterbender, über-
flüssig oder schädlich gewordener Klassen stehen und verstößt gegen so viele An-
sprüche der arbeitenden Volksklassen, der Frauen und insbesondere der besitz-
losen Frauen, daß die Vertreter der Partei der Arbeiter nicht die Verant-
wortung für diese und andere Angerechtigkeiten auf sich nehmen mochten,
die in der Zustimmung zu dem Gesetz lag.	,	, .

Die Ära Lohenlohe läutete für das Deutsche Reich den Eintritt m
die sogenannte Weltpolitik und in Verbindung damit die gestrigere
Pflege des Marinismus oder Flottenchauvinismus ein. Anter Eaprivl
hatte dieser verhältnismäßig stille Tage gehabt. 1890/91 bette, sich
Flottenetat — fortdauernde und einmalige Ausgaben zusammen	T

Äugen auf!

Die Volksfeinde gehen um. Mit Lügen, Verleumdungen. Der»
dächtigungen aller Art geht eine Garde Hausiren. die für die „Zucht-
hansoarlage" eintritt.

Von wem ist diese Lügen-Garde bezahlt?

Von den Junkern und Junkergrnossen, von der Unternehmecklasse,
von den Kapitalisten jeder Art. Weshalb werden die gelben Hefte und
die numerirten Flugblätter mit schwerem Geld von der Unternehmer-
klasse hergestellt und bezahlt? Weil die Unternehmrrklasse Stimmung
für die Entrechtung und Verknechtung der arbeitsthätigen Bevölkerung»
im Interesse der Großgrundbesitzer, Schlotbarone. Börsenleute. kurz der
Kapitalisten jeder Art machen will.

Die kapitalistischen Unternehmer sind die wahren {Imßntrgler.
Sie wollen allen Arbeitenden das Uerfammlnugsrecht. daS ■
Neveinsrecht, das Wahlrecht rauben. Sie streben einen gemeine
gefährlichen Zukunftsstaat an.

In diesem junkerlichen Zukunft-staat soll der tu ehrlicher Acbekt
sich Abrackernde, ohne jegliches Recht sein und Sklave, Arbeitsvieh
des „gnädigen Herrn" Krautjunker oder Schlotbarons werden. Der
Arbeiter — ob er geistig oder körperlich arbeitet, ist gleich — soll im
Zukunftsstaat der konservativen, freikonservativen und nationalliberalen
Kapitalisten schlimmer daran sein wie ein Packesel. Keinerlei Recht,
nur die Pflicht, sich zu Gunsten des Kapitalisten abzurackern, soll
er nach dem Wunsch der Unternehmerklasse haben.

19. Erste Seite eines Flugblattes gegen die
Zuchthausvorlage