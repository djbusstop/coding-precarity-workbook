﻿Händler, Haufirev!

nt Meitsg, öm 24. ö. U. findet von
18—6 Khr üie Kichwshl

in unserem 5. Berliner Reichstagswahlkreise statt.

Ihr habt zu entscheiden, ob uns der Kandidat der freifkUNige« Wolksparksl» fyv* Stcdt«-
schul-Insvektor

Dr. Zwick

oder sein sozialistischer Gegenkandidat, Herr Schmidt, in den nächsten 5 fahren im Reichstage
vertreten soll. —

Was hat Herr Schmidt für uns geleistet? Als er im Zähre \893 um unsere Stimmen
warb, da versprach er freilich, für UNS eintreten zu wollen! Als aber im Reichstags die uns so
schwer drückende Novelle zur GewerdeordltUNg vorlag, da wußte Herr Schmidt mancherlei zu Gunsten
der Theaterunternehmungen, aber nichts für uns zu sagen!

Kollegen! Keine Partei ist mit. ber Entschiedenheit und mit dem Geschick für uns im
Parlament eingetreten, wie zu allen Zeiten die

Freisinnige Volkspartei!

Sie trat der polizeiliche« Ueberwachung unseres Standes entgegen, die uns wegen mannig»
facher Umtriebe der Sozialdemokratie seitens der Regierung auferlegt wurde!

Die freifinuige BolksparLei kämpfte energisch gegen die ungerecht hohe Tteuerdelastung
unseres Gewerbes.

Aber die Sozialdemokratie will gar nicht unser Wohlergehen; im Gegenteil erklärte ihr größter
Führer, daß der Ruin des kleinen Mannes durch das Großkapital ein gutes Werk wäre!

Nicht unsere Wohlfahrt, nur unsere Stimmen will die Sozialdemokratie.

Und der Zweck heiligt ihr die Wittel! Hat üe es doch nicht verabscheut, in einem Berliner
Nachbarwahlkreise sogar Antisemitismus zu treiben!

Kameraden, wer die Minderung der unerträglichen Steuern anstrebt, wer seinen Beruf in
Ehren hält und nicht geneigt ist, unwahrhaftigen Versprechungen zu trauen, der wähle am 2d. M. Herrn

vr. Zwick.

Adolf Färber.	Herma«;! Lachmann,

Handelsmann,	Praylaserfir. 9.

HehrbtlmerAr. 13«.

Albert Koste,

Grenaöierstr. 5.	__________________

Verantwortlich für Redaktion und vertag: H. Lachmann, prenzlauerstr. 9. Druck von Alb. Lehmann, Berlin C., lHünjftr. 20.

90. Freisinniges Flugblatt zur Stichwahl im V. Berliner Neichstagswahlkreis 1898