﻿125

die damalige Führerschaft der Sozialdemokratie Deutschlands, ein Kriegsruf
der äußersten Linken der Partei für Wiederaufnahme der radikalsten Tra-
ditionen der Sozialdemokratie. Obwohl die revolutionäre Linke, hinter der
in Berlin zeitweilig fast die ganze „innere Bewegung" stand, durchaus
nicht nur aus jugendlichen Personen bestand, sondern Leute aller Alters-
klassen in ihren Reihen zählte, kam nun doch, und zwar zuerst in der bür-
gerlichen Presse die Bezeichnung „die Jungen" für sie auf; denn gegen-
über den Auer, Bebel, Liebknecht, Singer waren die führenden Vertreter
der Opposition allerdings „junge Leute".

Wie ohne weiteres begreiflich, schenkte ein Teil der bürgerlichen Presse
diesen „Jungen" alsbald starke Beachtung und ein gewisses Wohlwollen. Man
vermied es zwar, rund heraus für sie Partei zu nehmen, erklärte aber, das,
was sie verlangten, doch nur die folgerichtige Anwendung dessen sei, was
die alten Führer stets gepredigt hätten; die letzteren hätten nur nicht die
Ehrlichkeit oder den Mut, die Konsequenzen ihrer Lehren zu ziehen. And
als Wilhelm Liebknecht das Wort fallen ließ, nachdem der Parteitag ge-
sprochen haben werde, würden diejenigen, die sich seinen Beschlüssen nicht
fügten, „fliegen", gebärdeten sich namentlich die reaktionären Blätter höchst
entrüstet über solche „Parteidiktatur". Nun wäre die Drohung sicherlich
besser unterblieben, aber das Pharisäertum in der sittlichen Entrüstung
jener Presse lag zu offen zutage, als daß sie auf die sozialistische Arbeiter-
schaft Berlins größeren Eindruck hätte machen können. Sie schadete viel-
mehr den „Jungen" eher noch, als daß sie ihnen von irgendwelchem realen
Vorteil gewesen wäre.

Es fanden nun im August und September 1891 noch verschiedene
Versammlungen in Berlin statt, die zu Zusammenstößen zwischen Vertretern
der offiziellen Parteitaktik und Vertretern der Opposition führten. So am
25. August eine große Versammlung im Wahlverein des sechsten Berliner
Reichstagswahlkreises, in der das Vorstandsmitglied Albin Gerisch über
Kritik und Disziplin referierte. Die Darlegung Gerischs, daß das
Prinzip der Disziplin und die Forderung freier Kritik durchaus vereinbar
seien, solange die Kritik sachlich bleibe, daß aber die von der Opposition
geübte Kritik dieses Maß überschreite, rief eine lebhafte Debatte hervor.
Der Schriftsetzer Eugen Ernst bekannte sich mit Wärme zur Opposition,
indem er betonte, daß sie durchaus von ehrlichem Wollen beseelt sei und
nur aus innerster Überzeugung die Laltung der Parteileitung als schädlich
bekämpfe. Ihm entgegnete in längerer Rede Ignaz Auer, der die
Maßnahmen der Parteileitung verteidigte und insbesondere die Bedeutung
des Parlamentarismus hervorhob, während für die Opposition noch der
Arbeiter E. Börner Stellung nahm. Die Versammlung wurde schließlich
vertagt und am 1. September im Saal zum Eiskeller fortgesetzt. An diesem
Abend erklärte zunächst Eugen Ernst, in Antwort auf einen in der ersten
Versammlung geäußerten Vorhalt, daß er nicht der Verfasser des Flug-
blattes der Opposition sei, kritisierte dann eingehend das Auftreten der ein-
zelnen Parteiführer als zu weit getriebenen Parlamentarismus, sowie die Ab-
wiegclei in der Maifeierfrage, und faßte den leitenden Gedanken der Kritik
in den Satz des Flugblattes der Opposition zusammen: „Nicht Anehrlichkeit
werfen wir den Führern vor, sondern zu weit getriebene Rücksichtnahme
nuf allerhand Machtfaktoren, hervorgegangen aus ihrer veränderten Lebens-