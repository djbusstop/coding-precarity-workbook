﻿

den beteiligten Organi-
sationen mit dem Ersuchen,
ihn zu prüfen und inner-
halb von 10 Tagen dem
Amt ihre Ausstellungen,
falls sie solche zu machen
hätten, mitzuteilen. Dann
ward, nach Ablauf dieser
Frist, die Schlußsitzung
des Einigungsamts aus
den 14. August angesetzt,
und an die interessierten
Parteien erging die Ein-
ladung, zu dieser Sitzung
ihre Vertreter zu entsen-
den. Die Arbeiter kamen
dem sinngemäß nach, nach-
dem sie in öffentlicher Ver-
sammlung zu dem Tarif-
entwurf Stellung genom-
men hatten. Die Fabri-
kanten fanden sich zwar
in ziemlich großer Zahl
ein, als aber die Ver-
tretungen festgestellt wer-
den sollten, erhob sich der
zweiteVorsihende des Fa-
brikantenvereins — ein
Mann (Gollop), der als

einer der ersten den Vertrag vom 19. Februar gebrochen hatte — und erklärte,
eine Vertretung der Konfektionäre sei nicht erschienen, ein jeder von ihnen
sei nur in seinem eigenen Namen da. Das ließ für jeden Kundigen, der etwa
noch Zweifel hatte, das negative Ende der Verhandlungen im voraus erkennen.
And doch war das Einigungsamt, um den Fabrikanten die Zustimmung so
unbedenklich wie nur möglich zu machen, in keinem Punkt über die im
Februar von den Fabrikanten selbst vor ihm gemachten Vorschläge hinaus-
gegangen. Amgekehrt konnte Timm namens der Fünferkommission dem Amt
offiziell mitteilen, daß die Arbeiter in der erwähnten Versammlung beschlossen
hätten, den Tarif, obwohl er ihren Forderungen keineswegs entspreche, als
Abschlagszahlung anzunehmen. Aber allen Bemühungen des Amts, die
Fabrikanten zu bewegen, auf Grund dieser Erklärung einen Einigungs-
vertrag mit den Arbeitern abzuschließen, setzten diese die Berufung darauf
gegenüber, daß sie nur als Privatpersonen gekommen seien und keiner von
ihnen ein Recht habe, irgend etwas im Namen seiner Kollegen abzumachen.

So blieb dem Einigungsamt nur übrig, einen Schiedsspruch zu fällen
und es den Beteiligten zu überlassen, sich ihm zu unterwerfen oder nicht.
In diesem Schiedsspruch ließ cs die für bessere Arbeiten normierten höheren
Sätze sämtlich fallen und beschränkte sich auf Bestimmung der Lohnsätze für
Arbeiten allereinfachster Qualität. Die Zuschläge für bessere Arbeiten

Gknige Zeit darauf.

146 und 147. Karikaturen aus dem Kladderadatsch
auf die Großkonfektionäre

A

m