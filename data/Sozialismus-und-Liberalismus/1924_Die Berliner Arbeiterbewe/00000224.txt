﻿211

Arbeiter! yartetgcnossew!

Heute ist

Heichstagswahl!

Wir rufen Euch in letzter Stunde zu:

WtT Tut Sure fflieht! "WE

und gebt Eure Stimme dem Kandidaten der
Sozialdemokratie: Arthur Stadthagen l

Da» foxial-rm-kratische Wahtkomttee.

Vorwärts Buchdruckerci und VerlagSrmftalt Paul Singers Ci., Per'üo SW. 68, Linden st?. 69-

98. Letzte Mahnung an die Reichstagswähler Niederbarnim

naturgemäße Vermehrung und Zuwanderung kommt die politische Mobil-
machung von Reserven aus ihren alten Beständen.

So sind die bei der Wahl vom 25. Januar 1907 erfolgten Ver-
schiebungen der Parteiziffern zu erklären. Im ganzen stimmen bei den
Reichstagswahlen in Berlin und Umgebung die Wähler heute gemäß der
großen Klassenscheidung: die Arbeiter für die Sozialdemokratie, die Wähler
aus den anderen Klassen für die eine oder die andere der bürgerlichen Par-
teien. Aber es gibt stets Äberläufer, und große Schichten von Wählern
stehen ihrer Klassenlage nach zwischen den großen Gruppen, bereit, heute
sich dieser und morgen , jener zuzugesellen. Ohne Leranziehung der poli-
tischen Momente, die dem Wahlkampf seinen besonderen Stempel auf-
drücken, würde jeder Rückschluß aus der Stimmenverteilung auf die Stellung
der Klassen irrig ausfallen.

Es ist daher nicht nur wünschenswert, sondern auch sehr wohl möglich,
daß die nächste Reichstagswahl in allen vier Orten wieder größere Prozent-
sätze für das sozialdemokratische Wählerkontingent ergibt. Aber sie werden
den prinzipiellen Charakter des Bildes nicht ändern. Charlottenburg, Schöne-
berg, Wilmersdorf bleiben bis auf weiteres Stätten, wo das wohlhabende
Bürgertum sich in wachsender Masse ansiedelt, wie Rixdorf Lochburg der
Sozialdemokratie bleibt, auch wenn durch sein Wachstum ein größerer Ein-
schlag bürgerlicher Elemente herangezogen werden sollte. Die mächtig empor-
blühende Arbeiterstadt im Südosten Berlins darf sich rühmen, einen höheren
Prozentsatz sozialistischer Stimmen aufgebracht zu haben, als irgend ein
anderer Ort im Wahlkreis. Keiner der 49 Wahlbezirke Rixdorfs, der nicht
der Sozialdemokraüe mehr Stimmen gebracht hätte, als den anderen Par-
teien zusammen. Von kleineren Orten, die annähernd gleiches von sich
sagen können, seien Adlershof, Alt-Glienicke, Köpenick, Grünau, Loherlehme,

14*