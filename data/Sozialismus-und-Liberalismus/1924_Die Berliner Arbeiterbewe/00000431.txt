﻿414

Machtverhältnisse seiner Parteien und den Geist seiner Gesetzgebung und
Verwaltung schildert, unter Vorführung der Leistungen, Sünden und Unter-
lassungen dieser Gesetzgebung auf den verschiedenen ihr unterstehenden Arbeits-
gebieten.

So ausgerüstet traten kurz nach dem Dresdener Parteitag die Genossen
Groß-Berlins in den Wahlkampf ein, den das Zentralwahlkomitee mit
einem packenden Wahlaufruf eröffnete, der im „Vorwärts" vom 18. Oktober
1903 an der Spitze des Blattes abgedruckt ist. Außerdem wurden ver-
schiedene speziell auf Berlin bezügliche Flugblätter verbreitet, und selbst-
verständlich begleitete der „Vorwärts" den Wahlkampf mit anfeuernden
Artikeln und Notizen über die Bedeutung der Wahl. Ebenso ward wieder-
holt in großen Volksversammlungen, die in bezug auf Besuch und Be-
geisterung die hochgespanntesten Erwartungen rechtfertigten, für kräftige
Beteiligung an der Wahl agitiert. Am 6. November erfolgte in den vier
Landtagswahlkreisen Berlins die Aufstellung der Kandidaten. Es waren dies:
für	Berlin I	Bohn, W. Pfannkuch und Sb. Schubert;

„	„	II	Leo Arons und L. Silberschmidt;

„	„	III	August Bebel und G. Ledebour;

„	„	IV	Lugo Leimann und Paul Singer.

Für Niederbarnim und Oberbarnim kandidierten G. Freiwaldt, A. Stadt-
hagen und Bernhard Bruns, für Teltow-Beeskow-Charlottenburg Paul
Lirsch und Fritz Zubeil.

Mehr noch als bei der Reichstagswahl fiel bei der Landtagswahl die
Kleinarbeit ins Gewicht. Waren doch in Berlin allein in zwischen 1200
und 1500 Arwahlbezirken Wahlmannskandidaten aufzustellen und die Mann-
schaften für den Dienst bei der auf den 12. November angesetzten Arwahl
zu organisieren und zu instruieren. Lierbei bewährte sich nun die schon vor-
handene Gliederung nach Bezirken, die mit den Stadtbezirken zusammen-
fielen, auf das vortrefflichste. Auch die bei den Stadtverordnetenwahlen
gesammelten Erfahrungen kamen der Partei am Wahltage sehr zunutze.
And das Wahlergebnis war aufs neue ein Freund und Feind gleicher-
maßen überraschender Erfolg der Sozialdemokratie. Von insgesamt 6823
Wahlmännern Berlins waren nahezu ein Drittel, nämlich 2175, sozial-
demokratisch. Davon entfiel die Lälste auf den dritten Landtagswahlkreis,
der den ganzen sechsten und Teile des fünften Reichstagswahlkreises um-
faßt, und es konnte einen Augenblick zweifelhaft erscheinen, ob die Frei-
sinnigen in diesem Wahlkreis bei der Abgeordnetenwahl so viele eigene
Wahlmänner zum Wahltisch würden bringen können, um über Sozial-
demokraten und Konservative eine absolute Mehrheit zu erhalten. Zn der
Tat erhielten ihre Kandidaten bei dieser Wahl, die am 20. November 1903
stattfand, nur sieben Stimmen über die Lälste der Wahlmänner, wobei
noch angenommen werden muß, daß ein Teil konservativer Wahlmänner
gleich von vornherein für die Freisinnskandidaten gestimmt hatten, da an
einen Sieg der Konservativen hier nicht zu denken war, sondern nur die
Kandidaten der Sozialdemokratie in Frage kamen. Überhaupt hat das
Wahlsystem mit den drei Wählerklassen und der indirekten Wahl die Eigen-
tümlichkeit, daß bei Wahlmännern der ersten und selbst noch bei einem Teil
der bürgerlichen Wahlmänner der zweiten und dritten Klasse von einer
bestimmten Parteisarbe kaum die Rede ist. Sie werden von ihren Klassen-