﻿119

oder Demonstrationsver-
sammlungen waren. And
doch waren diese Beschlüsse
mehr als bloße Versamm-
lungsentscheidungen. So-
weit sie in Betracht kamen,
war die öffentliche Ver-
sammlung eben nur die
Schlußsitzung einer Konfe-
renz, deren erste Sitzungen
ursprünglich Korporen und
später Besprechungen der
Bezirks- und Abteikungs-
sührer gewesen waren. Da
es nun nicht möglich ist,
diese letzteren zu verzeich-
nen, müssen wenigstens
die bezüglichen öffentlichen
Versammlungen erwähnt
werden, und so läßt es
sich gar nicht umgehen,
im Kapitel, das den Kon-
ferenzen gilt, auch von Zu-
sammenkünften zu berich-
ten, die entweder schlecht-
hin als öffentliche Ver-
sammlungen einberufen
wurden oder sogar zu drei
Vierteln Demonstrations-
Versammlungen waren.

ZwischcnKonferenz, öffent-
licher Parteiversammlung
und Volksversammlung besteht insofern kein ausschließender Gegensatz.
Ihre Aufgaben laufen ineinander über, und die Einreihung in das
Kapitel der einen oder anderen Gruppe darf nicht so aufgefaßt werden,
als liege ihr eine streng schematische Anterscheidung zugrunde. Anter
welchem Gesichtspunkt Versammlungen oder Zusammenkünfte dem Verfasser
wichtiger erschienen, danach wurden sie der einen oder anderen Gruppe
zuerteilt.

Schon die Parteiversammlungen, die nach dem Parteitag von Lalle
in Berlin veranstaltet wurden, um die Beschlüsse jenes Parteitags zu
erörtern und die Wahl der Vertrauenspersonen zu vollziehen, die das in
Lalle beschlossene Parteistatut vorschrieb, illustrieren das oben Gesagte.
Wir haben sie als den Abschluß von Konferenzen zu betrachten, auf denen
Beratung über die zu wählenden Vertrauenspersonen gepflogen worden
waren. Das gleiche gilt von zwei, am 3. und 9. Juli 1891 im
Saale des Feenpalastes — jetzt Palast-Theater — abgehaltenen großen
Versammlungen, die über die Frage der Beschickung des auf Mit e
August 1891 nach Brüssel einberufenen Internationalen Sozra.

Die MWbipifltn

der

Berliner Opposition.

Für den Parteitag

I»s»wmkngkl>illl »ach di» Kklichlen di« „Karwärk"

Fartei-Iorstand.

Kerl!« .1891.

Verlag der LxpeLUlon bt» „Vorwärts", Berliner volksbla».
(Th. ©lotse.)

58. Titelblatt der Antwort des Parieivorstandes
auf die Anschuldigungen der Berliner Opposition