Der Ansturm 
gegen den Achtstundentag 
Eine Reihe in der Zeitschrift 
„Die Soziale Praxis" erschienener Aufsätze, 
gesammelt und mit Genehmigung 
des Verfassers, Lerrn 
Prof. Lujo Brentano 
herausgegeben 
Berlin 1923 
Verlagsgesellschast 
des Allgemeinen Deutschen Gewerkschaftsbundes