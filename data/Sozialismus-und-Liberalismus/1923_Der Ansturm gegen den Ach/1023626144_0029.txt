25 
rot. Die Arbeitervertreter forderten 250 Mk. mehr pro 
Schicht. Ein Unternehmervertreter sagte ihnen daraufhin ge 
sprächsweise: „Warum habt Ihr nicht mehr gefordert?" 
Jahr 
und 
Monat 
Durchschnittliche Schichtlöhne 
der Hauer und Schlepper im 
Ruhrqebiete 
Mk. 
Durchschnittliche Stein- 
kohleupreise (Fettiörderk. 
Rhein.-Wests.) 
Mk. 
1913 
ledig 
6,75 
verheiratet 
12 
Januar 1922 
107 
122 
405 
Februar 
März 
April 
Mai 
121 
137 
468 
141 
157 
605 
159 
176 
784 
187 
205 
908 
Juni 
203 
221 
* 908 
Juli 
259 
277 
1 208 
August 
351 
374 
1 513 
September 
689 
719 
4 105 
Oktober 
843 
903 
5 055 
November 
1 599 
1767 
1*063 
Dezember 
2 753 
3 053 
22 763 
Januar 1923 
4 246 
4 696 
32 622 
Februar 
12 963 
14 279 
107 657 
Darauf ein ArbeitorDertreter: „Ja, wir haben selber schon 
gestern abend davon gesprochen, ob wir nicht 300 Mk. for 
dern sollten " Am nächsten Tage forderten die Arbeiteroer 
treter 300 Mk. und stimmten der von den Unternehmern ge 
forderten Tonnenpreiserhöhung von 2600 Mk. (einschließlich 
Kohlensteuer) zu, während eine Lohnerhöhung um 300 Mk. 
an sich äußerstenfalls eine Preiserhöhung um 800 Mk. (ein 
schließlich Kohlensteuer) rechtfertigte. 
Aus den auf solche Weise erzielten Gewinnen hätten 
wohl die Kosten der Anlegung neuer Schächte bestritten wer 
den können, hätte nicht die Ausschaltung jeder Konkurrenz 
es ermöglicht, die Gewinnste auf mühelosere Weise zu steigern. 
Auch in der Eisenindustrie, deren Konkurrenzfähigkeit 
durch die Forderungen der Arbeiter bedroht sein soll, ist es 
nicht anders. In allen Zweigen der Schwerindustrie ist der 
auf die Lohnausgaben fallende Anteil am Produktionswerte 
mehr oder weniger zurückgegangen^). Dieser Anteil betrug im 
Reiche (Umfang von 1920, ohne Saargebiet) in: 
!) Text und Tabelle sind entnommen aus „Wirtschaft und 
Statistik". III. Jahrg., Nr. 5, S. 130.