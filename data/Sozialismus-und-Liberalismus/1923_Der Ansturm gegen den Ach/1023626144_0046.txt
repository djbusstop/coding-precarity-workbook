42 
wenn ihr Name aus solchem Anlatz in die Zeitungen käme. 
Ja noch mehr! Entsprechend dem Vorbild des antiken Chors 
scheint, seit eine sozialpolitisch reaktionäre Strömung bei unseren 
oberen Klassen sich geltend macht, wie Geheimrat Herkner in 
seinem aufsehenerregenden Aufsatz „Sozialpolitische Wandlun 
gen in der wissenschaftlichen Nationalökonomie" ausgeführt 
hat, der heutige Verein für Sozialpolitik auch diese Strömung 
mitmachen zu wollen. Daher es nur in der Ordnung ist, wenn 
im letzten Verzeichnisse der Mitglieder seines Ausschusses auch 
die Namen früher ausgesprochener Gegner sich finden. Sie 
haben bei der Annahme der Wahl in denselben nichts zurück 
zunehmen gehabt: sie konnten mit Genugtuung in den Aus 
schuß des Vereins eintreten, den sie früher bekämpft hatten. 
Der genannte Aufsatz des heutigen Vereinspräsidenten ist 
selbst eigentlich nichts als eine Aufforderung zu einer Umkehr. 
Ich habe in meinen vorausgegangenen Aufsätzen schon des 
öfteren Anlaß genommen, ihm entgegenzutreten. Nicht als ob 
er nicht auch Wahres enthielte. So stimme ich insbesondere 
dem zu, was darin über die im Wirtschaftsleben zunehmende 
Bureaukratisierung gesagt ist und über die Ueberschüttung des 
selben mit einem Gestrüpp von Verordnungen und Gesetzen, 
die an die Zeit des Merkantilsystems -erinnern. Ich erinnere 
nur an die dem Bedürfnis eines Landes, das auf die Ausfuhr 
angewiesen ist, um Lebensmittel, Rohstoffe und Devifen zur 
Erfüllung seiner Reparationspflichten zu erlangen, geradezu 
widersprechenden Erschwerungen seiner Ausfuhr. Auch muß 
ich bekennen, daß ich es bin, der in einem Briefe an Geheim 
rat Herkner jenen von ihm wiedergegebenen Satz geschrieben 
hat, in dem die von Karl Marx an den englischen Gewerk 
schaften geübte Kritik unter Gebrauch seiner Worte auf die 
deutschen Gewerkschaften angewendet ist, daß sie mancherorts 
„ideallose, profitgierige kleine Bourgeois geworden sind", und 
daß ich weiter hinzugefügt habe, daß sie sich hüten, in kürzerer 
Arbeitszeit und bei höherem Lohn intensiver zu arbeiten, wenn 
sie es in der Macht haben, durch bloße Mehrforderung mehr zu 
erlangen. Das schrieb ich in meinem Unmut, als ich von der 
in meinem dritten Aufsatz angeführten Verschwörung von 
Arbeitgebern und Arbeitern im Reichskohlenrat auf Kosten der 
Gesamtheit hörte. Meines Erinnerns bin ich in jenem Briefe 
noch weiter gegangen; ich habe sogar darin gesagt, daß, wenn 
dies das Ende fei, zu dem mein lebenslanges Eintreten für den 
kollektiven Arbeitsvertrag führe, ich mein ganzes Leben für 
verfehlt erachte. Denn ich habe stets nur für Gerechtigkeit ge 
kämpft und bin allzeit jedweder Ausbeutung der Gesamtheit 
durch Sonderinteressen scharf entgegengetreten. Wie ich dies