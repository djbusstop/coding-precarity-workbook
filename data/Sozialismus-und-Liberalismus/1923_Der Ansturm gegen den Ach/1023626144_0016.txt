12 
rend er z. B. in England in der Eisen- und Stahlindustrie 
seit 1906 größtenteils besteht, während durch ein Ge 
setz vom 15. August 1919 der Achtstundentag im Koblen- 
bergbau unter Tag sogar durch einen Siebenstundentag ersetzt 
worden und die Washingtoner Uebereinkunft über das Verbot 
der Nachtarbeit der Frauen und jugendlichen Personen durch 
ein Gesetz im Jahre 1921 durchgeführt worden ist. Der Höhe 
punkt solch erstaunlicher Behauptungen findet sich in den Aus 
führungen des Dr. Habersbrunner, der am 22. März 1922 
Herrn von Siemens im Sozialpolitischen Ausschüsse des Vor 
läufigen Reichswirtschaftsrats vertreten hat und im übrigen 
Geschäftsführer des Verbandes der Glasindustriellen ist. Er 
hat da von einer Verlängerung der achtstündigen auf eine 
zwölfstündige Arbeitszeit in der Tschechoslowakei wiederholt 
gesprochen.'! „Wird sich die deutsche Arbeiterschaft," rief er 
aus, „beschämen lassen von ausländischen Arbeitern, wenn ich 
Ihnen beweise, daß ausländische Arbeiter der Suspendierung 
des Achtstundentages zugestimmt haben, und ich habe da auf 
die Gesetzgebung verwiesen, die z. B. in der Tschechoslowakei 
den Achtstundentag aufgehoben und in den Zwölfstundentag 
verwandelt hat". Als ich dies las, wollte ich meinen Augen 
nicht trauen. Ist doch in der Tschechoslowakei die achtstündige 
Arbeitszeit von der tschechoslowakischen Nationalversammlung 
zu Ehren und zur Begrüßung des Präsidenten der Republik, 
der fast ein Vierteljahrhundert vom Katheder aus für den 
Achtstundentag eingetreten war, am Tage vor dessen Ankunft 
beschlossen worden, und nachdem die Produktion sich dem 
Gesetze gut angepaßt hat, wird das Gesetz über den Acht 
stundentag und seine Befolgung als wesentlicher Bestandteil 
des gesamten republikanischen Systems bezeichnet. Ich habe 
daher nach Prag geschrieben, ob sich vielleicht in der aller- 
neuesten Zeit etwas ereignet habe, was Dr. Habersbrunner 
rechtfertigen könnte, und ein erstauntes „Nein" zur Antwort 
erhalten. Da es den Sachverständigen, die im Sozialpolitischen 
Ausschüsse des Reichswirtschaftsrats vernommen wurden, be 
kannt war, daß die Tschechoslowakei das Washingtoner Ab 
kommen, über das ihr Achtstundengesetz noch hinausgeht, ohne 
Vorbehalt ratifiziert hat, hätten Dr. Habersbrunners damit 
unvereinbare Behauptungen doch auch den Vertreter der Wis 
senschaft im Sozialpolitischen Ausschuß veranlassen können, sich 
in Prag zu erkundigen, wie es mit ihrer Richtigkeit steht. 
) Siehe Reichsarbeitsblatt vom 15. Nov. 1922, S. 641*, 670*.