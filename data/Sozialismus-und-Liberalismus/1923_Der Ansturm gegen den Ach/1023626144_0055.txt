CD 
Image Engineering Scan I 
37 
o 
to h i 
- Kl I 
en der Arbeiter kennt, wird zugeben, daß damit 
zspunkt für die erbittertsten Arbeitskämpfe ge- 
Die §§ 18—20 bieten dagegen keine Sicherheit, 
t vorzubeugen, habe ich 1905 auf der Mann- 
mmlung des Vereins für Sozialpolitik den Vor- 
ht, die verschiedenen Gewerkschaften eines jeden 
llten eine Tarifkommission wählen, in die eine 
:l)em Proportionalwahlfystem Delegierte entsenden 
sollte mit den Arbeitgeberverbänden die Arbeits 
vereinbaren, die dünn für alle Gewerkschaften 
pt alle Arbeiter der betreffenden Beschäftigung 
3 Tarifgebiets maßgebend fein sollten. Auch soll- 
hiedenen Gewerkschaften aus ihren Mitteln ein 
tf gleicher Grundlage zufammenf hießen, aus dem 
für die Jnnehaltung der Tarifverträge seitens 
.haften bestritten werden solle. 
"orschlag hat 1918 die Billigung der freien, der 
rschen und der polnischen Gewerkschaften ge- 
i am Widerstande Stegerwalds, des Vertreters 
1 % n Gewerkschaften, gescheitert. Er meinte, die Ge- 
welche die Mehrheit hätten, könnten sich mit 
schlag zufrieden geben; sie aber seien die Min 
zesehen davon, daß dies keineswegs überall der 
0 richtig, daß nach der Ordnung, wie sie der Ent- 
Arbeitstarifgesetzes vorsieht, die Möglichkeit ge- 
I die Minderheit die Mehrheit vergewaltigt. Man 
i die unglaublichsten Verdrehungen des Charak- 
Zorfchlags vorgebracht. Man hat gesagt, er wolle 
historisch gewordener Organisationen eine bureau- 
n, während mein Vorschlag umgekehrt auf den 
. der historisch gewordenen Organisationen aufge- 
ie nur zu gemeinsamem Handeln heranziehen will, 
> vor bureaukratischer Vergewaltigung zu schützen, 
rer Ablehnung meiner Vorschläge haben sich be- 
Die Gewerkschaften hatten 1918 sich bereit er- 
ülrechtliche Haftung für die Durchführung der 
■, die ich, seit ich über Arbeiterkoalitionen ge- 
t, für unerläßlich erklärt habe, auf Grund meiner 
", übernehmen. Man hat mir geschrieben, daß 
on nichts mehr wissen wollen. Ich weiß nicht, 
ist. Aber das weiß ich, daß bei solcher Ordnung 
rtragsrechtes das Prinzip, die Arbeitsbedingun- 
nfamer Beratung der Vertreter der Arbeitgeber 
verbände feststellen zu lassen, bei Arbeitgebern 
gleichmäßig diskreditiert märe; wir würden nie