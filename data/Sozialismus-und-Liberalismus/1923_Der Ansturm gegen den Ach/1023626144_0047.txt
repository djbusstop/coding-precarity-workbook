43 
gegenüber Agrariern und industriellen Unternehmern getan 
habe, so werde ich es auch gegenüber Arbeitern tun. Aber ich 
habe stets gefunden, daß die Arbeiter, wenn man nur im rich 
tigen Tone zu ihnen spricht, für die Interessen der Gesamtheit 
nicht weniger zugänglich sind, als die Angehörigen anderer Ge 
sellschaftsklassen. Man muß nur so zu ihnen sprechen, daß sie 
die Empfindung haben, daß ein Freund zu ihnen spricht, der 
es gut mit ihnen meint. Wenn man sie dagegen in Organen 
ihrer Gegner herunterkanzelt, weil sie am Koalitionsrecht als 
an einem Noli me tangere festhalten, und damit den Anschein 
erweckt, daß man dieses Palladium der persönlichen Freiheit 
des Arbeiters, das man während Jahrhunderten zu unter 
drücken vergeblich bemüht war und für das sie Gefängnis, Ver 
bannung und selbst den Tod erlitten haben, preiszugeben be 
reit ist, darf man sich nicht wundern, wenn man sich alle Rich 
tungen unter den Arbeitern zum Feinde macht. Es war be 
sonders bedauerlich, daß die Redaktion des „Arbeitgeber" sich 
von der Veröffentlichung des cherknerschen Aufsatzes selbst 
durch die Erwägung nicht hat abhalten lassen, daß das Vater 
land der freudigen Hingebung der Arbeiter zurzeit ganz be 
sonders bedarf. Hängt doch seine ganze Zukunft von der Fort 
dauer ihres heroischen Widerstandes im Ruhrgebiet ab. Man 
mag daraus, daß selbst die Rücksicht hierauf die Redaktion nicht 
abgehalten hat, die Verleugnung alles dessen, was die „Ka 
thedersozialisten" seit fünfzig Jahren vertreten haben, durch 
ihren derzeitigen Führer, den Präsidenten des Vereins für 
Sozialpolitik, zu veröffentlichen, ermessen, welche Wichtigkeit 
sie seiner Kundgebung beigelegt hat. Auch haben alle Organe 
der Arbeitgeber und die große Presse, soweit sie diesen nahe 
steht, sie mit großer Befriedigung begrüßt. Die „Schweizerische 
Arbeitgeberzeitung" hat dies sogar mit einem gewissen Hohne 
getan. Ihr war die Kundgebung Wasser auf die Mühle. Hat 
doch, wie erzählt, in der Schweiz die rückläufige Bewegung 
gegen den Achtstundentag ihren Anfang genommen. 
Ich glaube, in den vorausgegangenen Aufsätzen den Nach 
weis erbracht zu haben, daß die meisten gegen die deutschen 
Gewerkschaften erhobenen Vorwürfe vor einer kritischen Prü 
fung nicht Stich halten. Es hat sich gezeigt, daß die deutschen 
Gewerkschaften sich nicht der Einsicht von der Notwendigkeit 
einer Steigerung unserer Produktion verschließen: desgleichen, 
daß sie es nicht verweigern/ wenn nötig, zu diesem Zweck 
Opfer zu bringen. Auch hat sich gezeigt, daß es eine unhalt 
bare Behauptung ist, daß, mit Ausnahme der Schweiz, die 
übrigen Völker daran gar nicht denken, den Achtstundentag 
durchzuführen: insbesondere sind sie in den Hauptkonkurrenz-