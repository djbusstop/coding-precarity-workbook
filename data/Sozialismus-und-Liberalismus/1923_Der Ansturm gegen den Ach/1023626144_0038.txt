34 
gungen und zur Ueberproduktion. Würden sie sich vereinigen, 
um das Angebot der Nachfrage anzupassen, so würde dies den 
verhängnisvollen Absatzschwankungen vorbeugen, welche zum 
Ruin von Unternehmungen führten und Tausende von Ar 
beitern brotlos machten. 
In Deutschland hat es seit 30 Jahren an Unternehmer 
syndikaten, welche das Angebot der Nachfrage anpaßten, nicht 
gefehlt, nur daß sie vor dem 11. November 1918 statt mit den 
Arbeiterorganisationen über die Arbeitszeit zu verhandeln, sie 
zu unterdrücken bemüht waren. Aber nunmehr ist durch das 
Versailler Diktat die gesetzliche Festsetzung eines Normal 
arbeitstages, und zwar im Prinzip auf 8 Stunden festgelegt, 
und ist Deutschland auch nicht an das Washingtoner Ab 
kommen, an dem es nicht teilgenommen hat, gebunden, so 
hat doch die deutsche Reichsregierung in einer Note von Mitte 
November 1922 an die Reparationskommission ausdrücklich 
erklärt^) 
„Deutschland wird alle erforderlichen und geeigneten Maß 
nahmen lergreifen, um insbesondere durch Erhöhung des Wirkungs 
grades der Arbeit zu einer Steigerung der Produktion und damit 
zu einem Ausgleich der Handelsbilanz zu gelangen. Zu diesem 
Zweck wird insbesondere eine Neuregelung des Arbeitsrechts 
unter Fe st Haltung des Achtstundentags als Nor 
ma la r b e i t s ta g und unter Zulassung gesetzlich 
begrenzter Ausnahmen auf tariflichem oder 
behördlichem Wege zur Behebung der Notlage der deutschen 
Wirtschaft in die Wege geleitet." 
Damit scheint mir, daß auch für uns die gesetzliche Fest 
stellung des Achtstundentages als Prinzip weiterer Diskussion 
entrückt ist, und es kann sich nur mehr darum handeln, ob die 
Ausnahmen auf tariflichem oder behördlichem Wege bestimmt 
werden sollen. Auch hatten sich, wie ich schon wiederholt her 
vorgehoben habe, die beiden Parteien einander so genähert, 
daß ganz abgesehen von der angeführten Regierungserklärung 
die weitere Diskussion sich nur mehr um diese Frage handelte. 
Ich bedauere dies sehr. Denn hätte man von Anfang an die 
Feststellung der Dauer der Arbeitszeit dem Tarifvertrag zu 
gewiesen, so wäre die durch die ganze Vernehmung der Sach 
verständigen sich hinziehende Frage, ob der Achtstundentag 
schematisch oder schablonenhaft stattfinden solle, unterblieben. 
Die Vertreter der Gewerkschaften haben ohnedies wiederholt 
erklärt, daß sie von einer solchen schematischen Durchführung 
nichts wissen wollen. Man hätte dann von vornherein für 
1 ) Siehe Wilhelm Beckmann in der 47. Sitzung des RWR. 
vom 13. Dezember 1922.