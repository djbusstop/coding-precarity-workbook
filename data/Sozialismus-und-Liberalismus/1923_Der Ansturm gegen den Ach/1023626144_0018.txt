14 
haben, dem Achtstundentag zuzustimmen; die Verhandlungen 
im Sozialpolitischen Ausschuß des Reichswirtschaftsrates und 
in diesem selbst zeigen, daß sie heute zum Neunstundentag 
zurückkehren möchten. Obwohl der Vater Siemens in seinem 
eigenen Betrieb den Achtstundentag schon in den siebziger 
Jahren eingeführt und sein Haus sich seitdem dabei wohl 
gefühlt hat, hält Herr von Siemens den Achtstundentag aus 
wirtschaftlichen Gründen für Deutschland für abträglich. Er 
hat sich dabei auf den von Herrn von Borsig und Herrn Legien 
unterzeichneten Brief an den Vollzugsausschuß der Arbeiter 
und Arbeitgeberverbände berufen, der da schließt: „Die Ver 
bände sind sich darüber einig, daß diese Vereinbarung nur 
dann dauernd durchgeführt werden kann, wenn der Acht 
stundentag für alle Kulturländer durch internationale Verein 
barung festgesetzt wird." M. a. W. er hält die deutsche Kon 
kurrenzfähigkeit bei Fortdauer des Achtstundentages für be 
droht. Gleichwohl sagt er: „Wenn aber aus anderen als rein 
wirtschaftlichen Motiven ein solches Gesetz (wie der vom 
Arbeitsministerium ausgearbeitete Entwurf eines Arbeitszeit 
gesetzes) vom Reichstag verabschiedet werden soll, so 
werden wir selbstverständlich unsere Mitarbeit zur Verfügung 
stellen" usw. 
Ich sehe in dieser Aeußerung einen Beleg für die Richtig 
keit des in meinem vorigen Artikel zitierten Ausspruchs des 
Herrn Marcus, daß beide Parteien vom besten Willen beseelt 
gewesen seien, sowie für die Richtigkeit meiner Auffassung, daß 
es nur an einer objektiven Untersuchung gefehlt hat, ob und 
inwieweit die deutsche Konkurrenzfähigkeit in Wahrheit be 
droht ist, um die Parteien zusammenzubringen. 
Von vornherein erscheint dies nämlich nicht wahrscheinlich. 
Warum würde sich sonst das Ausland durch Antidumpinggesetze 
gegen die durch die Markentwertung erleichterte deutsche Kon 
kurrenz zu schützen suchen? Auch ist ja bekannt, daß die Preise 
in Deutschland heute so hoch sind, daß selbst die unter den un 
günstigsten Bedingungen arbeitenden Betriebe sich rentieren 
und solche, die im Absterben waren, zu neuem Leben erwacht 
sind. Ganz besonders aber erscheint es fraglich, daß eine Be 
drohung der deutschen Konkurrenzfähigkeit, die ja aus ver 
schiedenen Ursachen stattfinden könnte, wie infolge Mangels an 
Rohstoffen, an Eisenbahnwagen für Kohlen und dergleichen, 
gerade durch die achtstündige Arbeitszeit verursacht sei. Wenn, 
wie Minister Gothein gesagt hat, England, Belgien und Frank 
reich in der Eisenindustrie alles an sich reißen, so ist darauf 
aufmerksam zu machen, daß in der englischen Eisen- und Stahl 
industrie der Achtstundentag schon seit 1906 in den meisten