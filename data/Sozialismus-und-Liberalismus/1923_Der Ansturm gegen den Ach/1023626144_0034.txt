30 
arbeitet, Konkurrenz mache, indem er in Ostündigem Arbeits 
tage seine Arbeitskraft erschöpft, damit er dem kapitallosen 
Arbeitgeber das Kapital schaffe, das dieser zur rationellen 
Einrichtung seines Betriebes braucht. Ist Geheimrat Herkner 
damit einverstanden? 
Und wenn nun der deutsche Arbeiter weiter liest, was Ge 
heimrat Bücher in seiner Vernehmung im Sozialpolitischen 
Ausschuß des RWR. von der Landwirtschaft gesagt hat:') 
„Wenn alle Landwirte in Deutschland heute die Erfahrungen, 
die die Wissenschaft für die landwirtschaftliche Praxis gemacht 
hat, verwerten würden, dann würde Deutschland in der Lage 
sein, noch Getreide auszuführen, das heißt, wenn wir das 
praktische, nicht das theoretische Optimum in der Landwirt 
schaft erzielen. Aber wir haben es mit Menschen zu tun, und 
ein sehr hervorragender Landwirt hat mir einmal gesagt: es 
gibt in Deutschland unter den 800 000 Betrieben nur zirka 
10 000 Landwirte, die den Namen wirklich verdienen, die das 
Optimum wirklich erzielen." Der Arbeiter wird sagen, also 
weil 790 000 landwirtschaftliche Betriebe nicht auf der Höhe 
ihrer Aufgabe stehen und eine große Anzahl unserer gewerb 
lichen Betriebe wegen Kapitalmangels rückständig ist, soll ich 
das preisgeben, was mir erst ein menschenwürdiges Dasein 
ermöglicht! Und, wie Herr Umbreit, der in der Sachverstän 
digenvernehmung im Sozialpolitischen Ausschuß des RWR. 
ben_ Vorsitz führte, die Argumente zusammenfaßt/) wird er 
„auf das Beispiel des Herrn Stinnes hinweisen, der seine 
Kapitalien ins Ausland trägt, überall in der ganzen Welt 
Werke aufkauft und in Rußland, Oesterreich, der Tschecho 
slowakei usw. Millionen, ja Milliarden investiert, anstatt 
diese Kapitalien dem deutschen Bergbau zuzuführen. Seit dem 
letzten Friedensjahre 1914 ist nahezu nicht ein einziger Schacht 
im Bergbau niedergebracht worden, obwohl von der Verbrei 
terung der deutschen Kohlenbasis das Wohl und Wehe unserer 
ganzen Wirtschaft, unserer Landwirtschaft, unserer Industrie, 
unseres Volkslebens abhängig ist. Milliardenweise werden 
Kapitalien über die Grenze gebracht, ins Ausland verschleppt, 
in fremden Industrien angelegt, nicht weil es daheim an nutz 
bringender Verwendung dafür fehlte, sondern weil man 
glaubt, diese Kapitalien auf diese Weise sicherer dem deutschen 
Steuerdruck entziehen zu können, und weil man draußen 
bessere Anlagemöglichkeiten zu finden meint als in der deut 
schen Industrie." 
st Reichsarbeitsblatt v. 15. Nov. 1922, S. 647*. 
st Ebenda, S. 676*.