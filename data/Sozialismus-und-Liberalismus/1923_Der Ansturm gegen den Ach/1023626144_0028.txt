24 
ben wie etwa der AEG-, aber den kleinen. Darunter ist auf 
die Aussage') des Geheimen Legationsrats Bücher, die, neben 
der des Dr. Hilferding unter denen der vom Sozialpolitischen 
Ausschuß des RWR. Vernommenen durch Klarheit und Sach 
kenntnis sich auszeichnet, besonderes Gewicht zu legen. Er 
glaubt nicht, daß die Banken den Ansprüchen zur Inten 
sivierung der Betriebe würden genügen können. Und in 
der Tat steht der Zinsfuß heute hoch. Aber wenn auch nicht 
aus den Depositen ihrer Kunden, sehen wir heute, wie die 
Banken fortwährend durch Emissionen junger Aktien den Be 
trieben neue Gelder verschaffen. Und wenn auch dies viel 
leicht nur eben zureicht, um den Betrieben die Mittel zu 
schaffen, die sie infolge der gesteigerten Preise der Rohstoffe 
und der erhöhten Löhne benötigen, so muß doch auch berück 
sichtigt werden, daß die Gewinnste, welche heute erzielt wer 
den, außerordentlich groß sind. Vielleicht liegt es gerade in 
der 5)öhe dieser Gewinnste, die ohne Betriebsverbesserungen 
erzielt werden, daß man nicht zur Betriebsverbesserung schrei 
tet, um die Gewinnste hoch zu halten und noch zu erhöhen. 
Es ist im Sozialpolitischen Ausschuß des RWR. mit Recht 
immer und immer wieder auf die Notwendigkeit verwiesen 
worden, die Produktion im Kohlenbergbau zu steigern, da 
durch sie die Produktion aller übrigen Erwerbszweige bedingt 
werde. Es scheint daher vom größten Interesse, zu ver 
gleichen, in welchem Maße die Kohlenpreise im Vergleich zu 
den Löhnen der Grubenarbeiter im Ruhrgebiete gestiegen 
sind"). (Tabelle siebe auf Seite iS) 
Die Löhne der ledigen Grubenarbeiter sind also von Ja 
nuar 1922 bis Februar 192,3 im Verhältnis von 100 : 12 115, 
die der verheirateten mit 2 Kindern unter 14 Jahren im 
Verhältnis von 100 :11 704, die Kohlenpreise aber in gleicher 
Zeit im Verhältnis von 100 :26 582 gestiegen. 
Das entspricht ganz einer mir von zuverlässiger Seite 
zugegangenen Darstellung von Vorgängen im Reichskohlen- 
st Reichsarbeitsblatt v. 15. Nov. 1922, <5. 647*. 
-) Die Zahlen der Spalten I und II für Januar 1922 bis Januar 
1923 sind entnommen aus „Wirtschaft und Statistik", 111. Jahrg., 
Nr. 2, S. 53, die Zahlen für Februar 1923 aus „Wirtschaft und 
Statistik", III. Jahrg., Nr. 5, S. 155. Die Zahlen der Spalte III 
für Januar und Februar 1922 sind entnommen aus „Wirtschaft und 
Statistik", II. Jahrg., Nr. 20, S. 657, die für April 1922 bis Januar 
1923 aus „Wirtschaft und Statistik", III. Jahrg., Nr. 25, S. 33, die 
Zahl für Februar 1923 aus „Wirtschaft und Statistik", III. Jahrg., 
Nr. 5, S. 129. Die Zahl für März 1922 ist durch Dr. Kuczynski 
berechnet worden.