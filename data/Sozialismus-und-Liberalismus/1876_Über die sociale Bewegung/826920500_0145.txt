133 
lergrund gehabt liaben. Der Verfasser untersucht nicht, wie das sechs 
zehnte Jahrhundert, welches der Grund und der Anfang der Bewegung 
der neueren Zeit ist, durch die Befreiung der Persönlichkeit auch für 
die sociale Frage eine neue Zeit in Grund und Folge geworden. 
Er geht sofort zu dem Socialismus und Gommunismus der fran 
zösischen Revolution über, ohne ihren tieferen geschichtlichen Grund 
und Zusammenhang nachzuweisen. Wahrscheinlich hat ihn seine con- 
fessionelle Stellung (V) daran gehindert, obwohl er nach der vorher 
gehenden geschichtlich und psychologisch feinen Behandlung der voran 
gegangenen Zeit für eine solche Untersuchung ganz besonders befähigt 
zu sein schien.*) 
Diese verschiedenen Bemerkungen haben durchaus nicht die Absicht, 
den Werth des Buches, wie es da ist, zu verringern, sondern wir freuen 
uns der günstigen Aufnahme, die so bald eine neue Auflage herbeige 
führt hat, in welcher die einzelnen Abschnitte theils vermehrt, theils 
neu gestaltet worden sind und der auch neue Beilagen, die Arbeiter 
frage im Jahre 1871, Ein internationaler Triumph, Alte und neue So- 
cialisten, beigegehen sind. Ein besonderer Vorzug des Buches, der in 
der neuen Auflage noch mehr hervortritt, ist sein Verständniss des Christen 
thums und die Anerkennung der socialen Bedeutung desselben. F. M. 
*) Bei der Bearbeitung der folgenden Auflage soll dem angeführten 
Bedürfniss durch genauere Behandlung der betreffenden Abschnitte Rück 
sicht getragen werden.