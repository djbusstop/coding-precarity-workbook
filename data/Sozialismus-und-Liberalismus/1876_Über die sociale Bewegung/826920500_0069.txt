57 
.Realistisch ist aber auch unsere Wissenschaft nach ihrem 
Gehalte. Sic ist nicht ein Gebäude von Idealen der Vol 
kerbeglückung, nicht ein System speeulativer Ethik ; au 
Grund der thatsächlichen Beobachtungen und Erfahrungen des 
täglichen Lebens nimmt sie vielmehr den Menschen wie er 
ist und sich gibt. 
Mit diesem Realismus trägt die Nationalökonomie nur 
die Signatur unseres Zeitalters an sich , welches nicht durch 
Wortformeln oder reine Gedankenrevolutionen, son ern urc 
thatsächliche Erfolge der Forschung die Befriedigung desden 
höchsten Fragen zugewendeten Erkenntnisstriebes erwartet. 
Unsere realistische Zeit ist mit Recht misstrauisch ge 
worden gegen die Versuche, die Gesellschaft a priori zu con- 
struireii.*) Tliatsachen zu sammeln und zu ordnen, vom Ein 
zelnen zum Allgemeinen aufzusteigen, von der Erfahrung 
auszugehen und auf der Staffel derselben allmälig in das Ge 
biet zu gelangen, wo die Speculation beginnen darf weil sie 
sich dort von selbst aufdrängt: das ist die Forderung, 
welche heute mit Recht an jede Art der Forschung nach 
Wahrlieit gestellt wird.**) 
, *) Die Ausdrücke A priori u. A posteriori sind zuversichtlich durch 
die Latinisirung des Aristotelischen und fó']« üOT5f>OV 
entstanden. Behufs eiuer systematischen Welierklärung verstand nam ic 
der Stagirite unter dem, was der Natur nach das Erste sei, die apyai, 
die Ursachen, die Principien. Ihnen entsprechen als Posterius die Wir 
kungen , die Erscheinungen, die aber für den Beobachter das Erste 
und Nächste bleiben. Diese Denk- und Sprachwcise blieb massgebend bis 
zum Ende des vorigen Jahrhunderts. 
**) „Philosophirt und speculirt wird instinctiv (bemerkt sehr richtig 
Martin Katzenberger a. a. O. S. 45-47) in jeder gut geleiteten 
Werkstätte, wie in jeder einzelnen Wissenschaft. Der Erfinder des Ther 
mometers z. B. soll ein holländischer Bauer, Cornelius Drebbel, 
am Ende des 1«. Jahrhunderts gewesen sein. Und dennoch ist die Werk- 
Btätte kein philosophischer Hörsaal, und nicht jede Wissenschaft schon 
als solche Philosophie. Was die Philosophie Apartes hat, ist ihr Inhalt, 
nicht die Methode oder sonst etwas. Insbesondere schliesst auch sie die 
inductive Methode und das exacte Verfahren nicht aus; sondern sie be-