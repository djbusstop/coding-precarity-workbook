123 
dustrielle Blaustrümpfe dürfen desshalb kaum liebenswürdiger 
sein, als literarische, die Em. G ei bei so treffend charakteri- 
sirt ; und immer bleiben doch dergleichen geistige Zwitter, 
Virtuosinnen in Männersachen, nur überflüssige Ausnahmen. 
Aber selbst ein Heranziehen derselben in sekundäre Stellungen 
der Industrie entzieht sie ihrer naturgemässen Mission inner 
halb der Gesellschaft. 
Nur tiefere Kulturstufen und niedere Lebens 
aufgaben lassen in der unentwickelten Natürlichkeit selbst und 
in den einfachen Beschäftigungen den Unterschied des männ 
lichen und weiblichen Geschlechts zurücktreten, wie nament 
lich beim Landbau. Freilich ist indess auch hier die kultur 
historische Bedeutung dieses Verhältnisses, namentlich des 
Masses, in welchem die stehenden Heere dem Landbau junge 
männliche Kräfte entziehen, und die AVeiber übermässig in 
die Arbeit ausser dem Hause treiben, kaum noch genug 
erwogen. 
Die geschlechtliche Arbeitstheiluug ist die erste, welche 
eine höhere Kultur voraussetzt. Ueberspannte industrielle 
Zustände können in der Hast der Erwerbssucht, welche alle 
Kräfte rücksichtslos in ihren Strom zieht, jene Unterschiede 
zu verwischen trachten , werden es aber nur theilweise ver 
mögen, und nie ohne Opfer höherer menschlicher Interessen. 
Abgesehen von dem sittlich verderblichen Einflüsse, welchen 
die Beschäftigung in Fabriken auf die Arbeiterinnen übt, ist 
die Vernachlässigung der eigenen, sowie der körperlichen und 
sittlichen Pflege der Kinder derselben bekannt. Wo die Weiber 
das Haus erhalten , was oft bei Fabriken der Fall ist, wo 
Männerarbeit entbehrt werden kann, da ist das ganze Familien 
leben auf den Kopf gestellt. 
Erwägt man hiezu, dass das Weib im Allgemeinen kör 
perlich nicht viel weniger bedürftig, als der Mann, in seiner 
körperlichen Schwäche vielfach nicht bloss Schonung, sondern 
Pflege braucht, Krankheiten mehr ausgesetzt ist, dass seine 
Arbeits- oder industrielle Thätigkeit häufig durch jene Funk 
tionen unterbrochen wird, welche ihm die Natur speciell zu-