auf einer blossen, aus grauer Theorie hergeleiteten Fiction 
beruht, oder doch nur in einer sehr beschränkten Ausdehnung 
Gültigkeit hat, dass gerade die von der Socialdemokratie daraus 
gezogenen Konsequenzen hinfällig werden. 
„Die Lehre vom Arbeitslohn ist“, wie schon der hoch 
verdiente K. H. Kau bemerkte, „darum besonders anregend 
und von hoher Bedeutung, weil sie die Bedingungen der Wohl 
fahrt für die zahlreichste Volksklasse entwickelt, und weil 
Irrthümer hierüber viele Nachtheile hervorrufen , z. B. die 
Arbeiter mit Groll gegen andere Volksklassen erfüllen, An 
sprüche, die sich nicht befriedigen lassen, veranlassen und zu 
einer fehlerhaften Handlungsweise veranlassen können.“ 
Die Resultate exacter Untersuchungen thatsächlicher Zu 
stände nach dieser Richtung sind daher höchst erfreulich. 
Daraus erklärt sich auch die Thatsache , dass heute bereits 
eine grosse Gruppe von Reformern und auch eine grosse Zahl der 
Arbeiter selbst nicht mehr um die Erhöhung des Lohnes im 
Allgemeinen kämpfen , sondern mehr um die Regelung des 
Arbeitsverhältnisses und insofern ist es unleugbar, dass die 
heutige sociale Bewegung ihrem Ziele immer näher rückt.*) 
So haben wir auf diesem Gebiete eine fortschreitende 
Entwicklung in den vergangenen Jahren wahrgenommon, mehr 
Klarheit und Wahrheit gegen die frühere Verworrenheit, mehr 
gesunden Realismus, mehr Wirklichkeit gegen die früheren 
Träumereien. So sehen wir, wie die Arbeiterfrage , die mit dem 
Communismus begonnen und dann nach und nach zum Socia. 
lismus, zu den Phalanstères eines Fourier**), zur Lassalle’- 
*) Das CharaklcristiscliP der neuen Gewerksvereinsheweyung liegt 
darin, dass es sich nicht um Beseitigung, sondern um Begelung des 
Arheilerverhältnisses handelt. Je zahlreicher unsere Beobachtungen 
werden, je ausgedehnter unsere Erfahrung wird, um so mehr macht sich 
die Ansicht geltend, dass die Arbeiterklasse einer Organisation ihres 
Klasseninteresses bedarf. 
*) Vgl. Fourier’s Zeitschrift ; Le Phalanstère oder La réforme indu 
strielle. 18:P2. Die zahlreichen Schriften Fourier's und seiner Schule sind