118 
nur nicht müde werden, die Stimme zu erheben, indem das 
wahre Wort endlich ebenso Eingang ins Leben findet, wie 
der Wassertropfen mit der Zeit den Stein auszuhöhlen vermag. 
Möchten darum einsichtsvolle Patrioten unermüdlich jener ein 
seitigen Geldspeculationswirthschaft entgegenarbeiten, wie dies 
besonders von Prof. Dr. B a u r in höchst gelungener Weise 
geschehen ist. In seiner gediegenen Schrift : „lieber die Be 
rechnung der zu leistenden Entschädigungen für die Abtretung 
von Wald zu öffentlichen Zwecken , mit Rücksicht auf die 
neuere Theorie des Waldbaug der höchsten Bodenrente“ (Wien 
1869) sagt der geschätzte Verfasser unter Anderem : „Waldungen 
niederhauen ist leichter als solche anbauen, Geld ausgeben 
leichter als Kapitalien für seine Nachkommen Zusammenhalten 
und aufsparen und von schlechten llf^ushaltern in kleinern und 
grössern Privatforsten, welche , trotzdem dass sie ihre älteren 
Bolzvorräthe niederschlugen und möglichst gut versilberten, 
dennoch nicht reicher, sondern ärmer geworden sind, weiss die 
Geschichte wie die Gegenwart leider nur zu viel Absclireckendes 
zu berichten.“*) 
Die neue Schule des Waldbaus des höchsten Reinertrags 
fasst den Wald in erster Linie von seiner finanziellen Seite 
auf ; nach ihr wird die vortheilhafteste Umtriebszeit in den 
Zeitraum verlegt, für welchen sich ^ie höchste Bodenrente er- 
giebt ; sie ist für niedrigere Umtriebe, welche allerdings oft 
im Interesse des Privatwaldbesitzers liegen, aber vom volks- 
wirthschaftlichen Standpunkte aus nicht zu billigen sind. „Wenn 
in gegebenen Fällen, bemerkt K. II. Rau (,,Grund8ätze 
*) Vgl. hierzu die bereits cilirten Beiträge Baur’s: „Zur Ehrenret 
tung des deutschen Waldes und seiner Bewirthschafter“ a. a. 0. (Monats 
schrift für Forst- und Jagdwesen 1872), sowie Helferich in der Zeit 
schrift für die gesaminte Staatswissenschaft. Jahrgang 1867, Bd. 2:1.' 
S. 1 u. ff.; 1871, Bd. 27 S. 5-Í9 ff. 1872 Bd. 28, S. 387 ff. (Auf diese 
letzte gegen Oberforstrath Judeich gerichtete Abhandlung hat dieser 
geantwortet und diese Antwort in der Zeitschrift für die ges. Staats^. 
Jahrgang 1873. Heft I S. 145 ff. abgedruckt.)