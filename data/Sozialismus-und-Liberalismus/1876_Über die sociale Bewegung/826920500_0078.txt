66 
So weist die Gesellschaft einen erschreckenden Mangel 
an idealer Anffassung des Lebens, ein bis zur Manie steigen 
des Haschen nach Genuss, überhaupt Bestrebungen und An 
schauungen auf, die der materialistischen und atheistischen 
Bewegung des 18. Jahrhunderts in Frankreich ähnlich sind 
und unsere Zukunft ernstlich bedrohen. Auch unser Volk 
hat schon tiefe Giftzüge aus dem Becher gethan, der das 
Lebensmark des Nachbarvolkes so sehr geschädigt hat; schlimme 
Geister, die sich seit langer Zeit aus verdorbenen Elementen 
und verkehrten Richtungen der Gesellschaft entwickelt haben, 
sind plötzlich rege geworden, sie sind alle von dem Grund 
triebe beherrscht, die Materie über den Geist, das Niedere 
über das Höhere, das Sinnliche über das Nichtsinnliche zu 
setzen. 
„In der That, wenn die sinnliche Befriedigung des Men 
schen das höchste und letzte Ziel ist, das er erstreben kann 
und soll, dann ist es ganz konsequent, wenn der Bau von 
Staat und Gesellschaft lediglich auf dieses Ziel berechnet wird, 
und es erscheint nun, wenn damit das Gleichheitsprinzip in 
Verbindung gebracht wird, als höchste Aufgabe, möglichst 
viele Genussmittel durch die freilich unvermeidliche Arbeit 
zu erzeugen, und diese so zu vertheilen, dass Jedem der 
gleiche Genuss zukommt. Dieser Aufgabe muss jede andere 
Rücksicht weichen. Die Freiheit des Naturrechtes kann nach 
dieser Ansicht als Mittel dazu dienen, die erforderliche sociale 
Umwälzung möglich zu machen; aber sie ist an sich unter 
geordnet; sie gibt an sich die wirkliche Befriedigung noch 
nicht, und wenn ihre Beschränkung, ja ihre Aufhebung von 
dem Grundprinzipe des gleichen Genusses wieder verlangt würde, 
BO ist nicht einzusehen, wesshalb man nicht selbst zu dieser 
Folgerung sich verstehen sollte. Das ist der einfache Ge 
danke, der, wenn auch nicht überall mit gleicher Einseitigkeit 
und Konsequenz festgehalten, doch allen socialistischen Theorien 
zu Grunde liegt und der ihr Verhältniss zu den andern poli 
tischen Lehren erklärt. Sie abstrahiren wie das Natur recht 
von allem geschichtlich Gewordenen, von aller Rücksicht