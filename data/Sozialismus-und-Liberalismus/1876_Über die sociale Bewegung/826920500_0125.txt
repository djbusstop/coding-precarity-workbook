113 
Im Gefolge der erwähnten Umstände tritt eine Preis- 
erhöhung der nothwendigen Lebensmittel als eine öffentliche 
Oalamität von Tag zu Tag hervor. 
Das Umsichgreifen der hieraus hervorgehenden gesell- 
schafthchen Missstände war nach dem Zeugniss der Geschichte 
stets „der erste Spross für die Leiter, auf welcher Staaten und 
Volker abwärts zum Verfall steigen.“ Gleichwohl haben die 
zur Leitung des Staates berufenen Faktoren in Deutschland 
und Oesterreich den Ursachen der eingerissenen Theuerung 
nicht nachgeforscht und daher auch keine Mittel in Anwen 
dung bringen können, um das Übel zu bekämpfen.*) 
Nicht ohne Interesse sind nachstehende Bemerkungen 
aus einer Zuschrift eines uns befreundeten Fachgenossen : 
Endlich thut man staatliche Schritte, um dem sinnlosen Streike- 
ists kein AVunder, wenn Prediger und Schullehrer die Lust zu ihrer 
Sisiphusarbeit verlieren. Der hinkende Bote kann aber nicht ausbleiben. 
I er Moment muss kommen und ist in vielen Familien schon da. wo die 
Miethe soviel vom Einkommen verschlingt, dass Nahrung, Kleidung 
und Feuerung darunter leiden müssen, dass Hunger und Kummer die 
lesundheit untergrübt, was leicht zum Ausbruche von Seuchen führen 
kann Dann sterben viele bestehende Wohnungen aus, neue werden 
nicht gebaut und die Bauhandwerker werden sich nach den Fleisch- 
tüpfen sogar vormärzlicher Lohnsätze sehnen; da diese selbst jedoch 
allem ein. Das wird das Resultat des Allruinirens werden, d. h. eine 
nungen hatten wenigstens das Gute, dass sie den Arbeiterstand am 
Luxus an der Toilette sehr hinderten; Schattensseite dagegen war. dass 
«e dem Hochmuth der nicht von ihnen Berührten Nahrung gaben 
Die grösste Schattenseite der Kapital-Herrschaft unserer Tage scheint 
mir die ObermOthige Luxusentfaltung im Bau- und jeglichem anderen 
Prunkwesen zu sein, weil es dem Arbeiterstande den Anlass zum Ver 
gleiche so nahe legt und namentlich auf indirectem Wege dazu bei- 
trägt, das Dasein eines wohlhabenden und gebildeten Mittelstandes zu 
untergraben. 
*) Vgl. H. Maurus: Ober die Ursachen der herrschenden allee- 
meinen Theurung. Heidelberg 1874. ® 
Contian, tooUle Bewegung; g