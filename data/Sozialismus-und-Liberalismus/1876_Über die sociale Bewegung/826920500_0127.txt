llõ 
versilbernden Gründerthums, des Güterschachers, des boden 
losen Schwindels, an dessen tiefsten Abgründen gerade die aller, 
jüngste Gegenwart schaudernd und verzweifelnd angelangt ist *) 
Wer die Natur und die Eigenthiimlichkeiten der Wald- 
wirthschaft nur einigermassen kennt, wird wohl kaum einen 
Zweifel hegen können, dass der Wald als kein blosses Specu- 
lationsobject betrachtet werden darf. Die Waldwirthschaft 
geht in zahllosen Fällen über den engen Horizont des gewöhn 
lichen Gewerbetriebes hinaus. Ihr ist, wie Rau treffend be 
merkt, die momentane Gewinnsucht der heutigen kurz lebenden 
Generation fremd und feindlich. Sie gehört ebenso gut der 
Gesammtheit als dem Einzelnen, sie ist nicht blos auf Erzeu 
gung von Werthen gerichtet, welche der Gegenwart allein zu 
Gute kommen, sie arbeitet vielmehr für eine entfernte Zukunft 
und unterscheidet sich dadurch wesentlich von allen anderen 
Formen menschlichen Wirthschaftsbetriebes. 
Wenn die Landwirthschaft den grösstmöglichen Ertrag 
aus dem Boden zieht, so fördert sie gleichzeitig das Interesse 
des jeweiligen Besitzers und dasjenige der Gesammtheit, und 
wenngleich dieses auch durch s. g. Raubwirthschaft in gewisser 
Beziehung auf Kosten der Zukunft geschehen kann, so schä 
digt doch einerseits wegen der rasch sich bemerkbar machenden 
Folgen der Besitzer sich selbst, andererseits ist ein Ersatz der 
geraubten Bodenkraft in nicht zu ferner Zeit durch rationelle 
Bewirthschaftung möglich. Ganz anders gestaltet sich die 
Sache beim forstwirthschaftlichen Betriebe. Eiugetretene Zer- 
Störungen der Wälder lassen sich nicht in 3, 4—6 Jahren 
hersteilen, wie beim Acker- und Wiesenbau ; beim Walde reichen 
oft Dezennien nicht aus , sondern es ist oft ein Jahrhundert 
erforderlich, um das Fehlende zu ersetzen, und in manchen 
Fällen ist dies gar nicht mehr möglich. Der Wald ist dazu 
bestimmt, das schützende, schirmende Kleid der Erde zu bilden, 
*) Gleim: Forstliche Briefe, ausser im „Siehenbiirger Deutschen 
Wochenblatte“ Nro. 30, 31. 1873 mehrfach gedruckt, so in den „Land- 
wirthschafllichen Blättern für Siebenbürgen“ (Drollef, Hermannsladt) 
Nro 9, September 1873, sowie in den „Verhandlungen der Forstwirthe 
von Mähren und Schlesien.“ Brünn, Rud. Bohrer. IV. Heft 1874. 
8*