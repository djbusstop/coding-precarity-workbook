8 
Zuständen und Einrichtungen ab : sie sind die wesentlichsten 
Lebensbedingungen und wichtigsten Elemente und Grundlagen 
der bürgerlichen Gemeinwesen. Wo daher die Finanzen eines 
Staates blühen sollen, muss zuvor der Wohlstand eines Volkes 
blühen, denn die Blüthe der Finanzen ruht lediglich im Volks 
wohlstände , in der steigenden Güterproduktion der einzelnen 
Volksglieder. „Ein Hausvater“, sagt daher bezeichnend von 
Schröder, Fürstliche Schatz- und Rentenkammer (1686), 
„muss seinen Acker düngen und pflügen, will er davon etwas 
erndten. Das Vieh muss er mästen, will er es schlachten und 
die Kühe muss er wohl füttern, wenn er will, dass sie sollen 
viel Milch geben. Also muss ein Fürst seinen Unterthanen 
erst zu einer guten Nahrung helfen, wenn er etwas von ihnen 
nehmen will.“ Durch blühende Volkswirthschaft wohlhabend 
werdende Völker bereichern in steter Progression die Staats 
kassen. ) Dies ist ein mit unverkennbaren Lettern aus der 
Geschichte der Volkswirthschaftslehre hervorleuchtendes Axiom, 
dies ist das Fundament, die festeste Grundlage einer prospe- 
rirenden Finanzverwaltung.*) **) Nur wenn wir auf der Basis 
gesunder materieller Interessen stehen, vermögen wirauch eine 
gesunde Politik durchzuführen.***) Nur der wirthschaftlich ent- 
*) „Ñeque enim rex inops esse potest, cujus imperio ditissimi homines 
subjiciunter“ : Maxime des Grafen Diomedcs von Caraffa. 
**) Vgl. A. E. Ritter von Korners: Abriss der Nationalökonomio 
2. Auflage. Prag 1868, sowie die in französischer Sprache erschienene 
Schrift: „Les finances de l’Autriche, Etude historique et statistique sur 
les finances do l’Autricho-Cisleithnnienne par le comte de Mülinen, envoyé 
extraordinaire et ministre plénipotentiaire. Paris, Guillaumin — Vienne, 
Braumüller.“ 1875. S. 1 : Le Baron Louis célèbre financier de la Re 
stauration disait : „Faites-moi de la 1 onne politique, et je vous donnerai 
de bonnes finances.“ Rien ne nous parait plus juste que de retournert 
en, 1 étendant, çe mot si profond, et d’ajouter, comme complement de la 
pensée qui l’a inspiré : Donnez-moi de bonnes finances, et je vous fera 
de la bonne politique.“ 
***) Ueber die ethische und sociale Bedeutung des Wohlstandes und 
Eigenthums vgl. noch Vorländer in der Zeitschrift f. d. ges. Staats 
wissenschaft 1855. S. 579 ff. V. Ilasner System der politischen Ökonomie. 
Bd. !.. Prag 1861, S. 59 ff. In Frankreich haben insbesondere schon dio