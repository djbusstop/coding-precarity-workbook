75 
und Schwächen kennen. Derjenige wird das Feld behaupten, 
welcher von seinem Gegner das Beste zu lernen weiss*), um 
dessen wirklich begründete Ansprüche in einer Form zu be 
friedigen, welche '— frei vom Charakter gehässiger Feindselig 
keit, frei von der Leidenschaft des einseitigen Parteiinteresses 
— eine wirksame Vereinigung der Gewerbsmittel zu sittlichen 
Lebenszwecken durchzuführen im Stande sein wird.**) 
Die Wissenschaft der Volkswirthschaft hat hier eine grosse 
Aufgabe zu losen, sie muss, je grosser die Gefahren und Ver 
irrungen auf socialem Gebiete sind, doppelt Mahnung und Be 
ruf in sich fühlen, immer mehr und mehr zu prüfen und mit 
tiefstem Ernste der Wahrheit nachringen. Denn gerade auf 
dem empfänglichen Boden der socialen Fragen hat die Propa 
ganda des Irrthums ganz besondere Chancen und schon mehr 
mals in überraschender Weise Wurzel getrieben. Sind doch 
die socialen Missstände, die eben den Inhalt der socialen Frage 
bilden, bei dem stets sichtbaren Auftreten im Strom des bür 
gerlichen Lebens auch dem Auge des weniger Einsichtigen 
erkennbar, regen sie doch bei den Eingriffen in die Sphäre der 
materiellen Interessen oft auch das Gemüth des Gemässigten 
zur Unzufriedenheit, ja hie und da zur Leidenschaft desZ nies auf. 
de mehr der Gegensatz von Kapital und Arbeit sich zu 
spitzt und je wider williger sich die Arbeiter in ihre Lage 
fügen, desto reichlicher ist der Samen verbreitet, welcher zur 
blutigen Saat aufgehen kann. 
Wie erklärlich ist es, dass der Arbeiter in seiner Noth, 
wenn er ausser Stande ist, die volkswirthschaftliche Berech 
tigung des Iteichthums anderer zu erkennen, auf socialistische 
und communistische Gedanken kommt und da und dort auch 
zum Verbrechen seine Zuflucht nimmt. 
*) Den Feind zu unterschützen, hat sich zu allen Zeiten gerächt. 
•*) Friedr. Schuler von Libloy: Der Socialismus und die 
Internationale nach ihren hervorragendsten Erscheinungen in Literatur und 
Leben. Leipzig. 1875.