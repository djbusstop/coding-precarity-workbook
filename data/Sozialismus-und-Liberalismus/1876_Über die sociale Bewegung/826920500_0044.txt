/, 
n. 
Die gegenwärtige Aufgabe der National 
ökonomie. 
So Grosses und Bedeutendes auch auf volkswirtlischaft- 
licliem Gebiete, besonders seit den letzten Decennien, in 
allen vorgeschrittenen Ländern geleistet worden ist, so dürfen 
wir doch die Yolkswirthschaftslehre nicht als eine abgeschlossene 
und vollendete ^Wissenschaft ansehen, weil sie berufen ist, die 
wirthschaftlichen Erscheinungen jedes Zeitalters zu begreifen 
und zu erklären, weil sich ihr im Fortgang der gesellschaft 
lichen Entwicklung stets neue Aufgaben zur Lösung aufdrängen. 
Weir uns, wollten wir auf geträumten Lorbeeren uns zur 
Hube legen ! Nur der beschränkte Kopf kann glauben, dass, 
weil er still steht, auch die Y issenschaft nicht fortschreitet*). 
Erzeugt nicht jede veränderte Richtung, welche die Pro 
duktion und der Verkehr in ihren Strömungen und Bewegun 
gen erhalten, überhaupt das wirthschaftUche Leben in seinen 
tausendgestaltigen Erscheinungen täglich neue Probleme ? Nur 
*) Newton antwortete Jedem, der seine Wissenschaft liehen Lei 
stunden bewunderte : ,.Ich weiss nicht, was die Welt zu meinen Arbeiten 
sajjen wird. Mir seihst kam ich nur wie ein Kind vor, spielend am Ufer 
des Meeres, bald ein buntes Steinchen , bald eine glänzende Muschel 
schale findend, i n d ess sich d er Ozean der Wahrheit, uner 
forscht und unerfors c hli ch, in unendlicher Weite vor 
meinen Augen ausdehnte“. Sehr schön sagt ferner Lessing: 
„Wenn Gott in seiner Rechten alle Wahrheit und in seiner Linken den 
einzig immer regen Trieb nach Wahrheit, obschon mit dem Zusatze, mich 
immer und ewig zu irren, verborgen hielt, und spräche zu mir: wähle! 
— ich fiele ihm mit Demuth in seine Linke und sagte: Vater, gieb! 
Die reine Wahrheit ist ja für Dich allein !“ Je mehr wir diese be 
scheidene Sprache führen, desto näher liegt die Verständigung in dem 
oft leidenschaftlich erregten Streite, der zwischen den Parteien über 
volkswirthschaftliehe Gegenstände geführt wird.