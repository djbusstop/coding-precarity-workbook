121 
Endlich ist es heilige Pflicht aller wahren Freunde des 
Volkes, bei der socialen Frage auch die Arbeit der Frauen 
welt mit in Betracht zu ziehen.*) 
Früher genügte die Stellung des Weibes in der Ehe und 
in der Familie, um ihre Existenz zu sichern und ihren Lebens 
bedürfnissen Befriedigung zu verschaffen. Jetzt ist das grössten- 
theils anders geworden, jetzt muss auch die Frau, gleich dem 
Manne, arbeiten, um sich und die Ihrigen zu ernähren. Wohl 
verstanden , nicht arbeiten im Haus und in der Familie — 
denn dies hat das Weib zu allen Zeiten gethan ; — sondern 
arbeiten im Dienste des Kapitals und nach dessen Gesetzen, 
das Weib muss nicht mehr häusliche, sondern wirthschaftliche 
industrielle, gewerbliche Arbeit leisten, um existiren zu können. 
Das ist der grosse Unterschied, der das Weib aus dem Hause 
und aus der Familie in die Fabrik treibt**), der sie eben damit 
sein Klageruf: „Man will die Bäume des heiligen Hains der Kunst fällen“! 
in vielen Malerherzen, und wer von edler Gesinnung in Rom weilt, haupt 
sächlich die im deutschen \ aterlande lebenden Krinnerungsfrohen, ant 
worten! „Das darf nimmermehr geschehn“! 
Da flössen dio Scherflein von nah und fern, bis des Goldes genug 
war, den alten Hain für die deutsche Kunst zu erwerben. Keines Frev 
lers freche Axt darf sich mehr den geweiheteu Stämmmen nahen ; auf 
denen ungestört der deutsche Adler nisten darf; denn man bot diese 
kleine Landschaftsinsel dem deutschen Reiche zum Geschenk an. Um 
fasst diese auch nur 28000 Quadratmeter mit 98 alten und jungen Eichen 
bäumen, so ist der h leck des Landes gross genug, um rechte Malernester 
darauf zu bauen, wenn nur das deutsche Reich die rechten Vögel dazu 
hersendet. Das Material ist da, also auf! und regt die Schwingen! 
*) Vgl. Hasner: Politische Ökonomie I. Prag 1856. 8. 193—195. 
H. Rösler: Über die geschichtliche Entwickelung der volkswirthschaft- 
lichen Ideen der neueren Zeit. Rostock. 1873. H. Contzen: Die National 
ökonomie ein politisches Bedürfniss unserer Zeit Berlin 1872. Bd. II. 
8. 226 235. 1. von Holtzendorf; Ueber die Verbesserungen in der 
gesellschaftlichen und wirthschaftlichen Stellung der Frauen. 1867. 
**) 1^8 gibt manche Industriezweige, welche ganz oder doch zum gröss 
ten Theil durch weibliche Hände betrieben werden, z. B. die Nadel- 
und Cigarrenfabrikation. Es klingt unglaublich und doch ist es wahr, dass 
Mädchen und Frauen in die Fabrik gehen, während der Vater die Haus 
haltung besorgt.