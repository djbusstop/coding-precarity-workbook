62 
Wie weit die religiöse Verkommenheit bereits vor der 
Pariser Commune in der Arbeiterbevölkerung Platz gegriffen 
hatte, zeigt uns das ruchlose Programm in dem französischen 
Blatte „La Cigale“, in dem folgende Stellen Vorkommen: 
„Gott und Christus sind zu jederzeit die Schutzmauern 
des Kapitals und die erbittertsten Feinde der arbeitenden Klassen 
gewesen. Gott und Christus sind schuld daran, dass das Volk 
bis Jetzt noch in der Leibeigenschaft schmachtet; indem man 
demselben lügenhafte Hoffnungen und phantastische Paradiese 
vorgaukelte, hat man das Volk bewogen, alle Leiden der Erde 
nicht nur ohne Widerspruch, sondern sogar mit Freude auf 
sich zu nehmen. Nur erst, wenn alle Religionen weggefegt, alle 
sowohl christlichen als sonst religiösen Begriffe bis auf die 
letzte Spur werden ausgetilgt sein, können wir das socialistische 
und politische Ideal erreichen, das wir anstreben. Mag Jesus 
sein Reich im Himmel behalten, diesen Köder des Proletariers, 
wir glauben nur an die Menschheit, an dieses tausendjährige 
Opfer der Religion. Unsere Principien sind Krieg gegen Gott, 
gegen Christus, Krieg den Hespoten des Himmels und der 
Erde. Dies ist der öchlaclitruf des neuen grossen Kreuzzuges.“ 
' Dieser Geist hat sich auch der Majorität der deutschen 
Social-Demokratie mitgetheilt*), wie A. Held durch viele 
*) Von der dciitsclicn Socialdemokratio wurde bekanntlich die Com 
mune aufs Energischste in Schutz* genommen, selbst der Umsturz der Ven- 
dome-Säule als herrliche That gepriesen. Es möge hier das Decret der 
Pariser Commune bezüglich der Vernichtung der Vendonie«Sâule mitge 
theilt werden zur warnenden Erinnerung an jene traurigen Tage, die über 
die Hauptstadt Frankreichs gekommen waren. Das hetreireiide Decret lau 
tete : „Die Commune von Paris, in Erwägung dass die kaiserliche Säule 
auf dem Vendôme - Platz ein ^lonumcnt der Barbarei, ein Symbol 
brutaler Gewalt und falschen Ruhmes, eine Bekräftigung des Mi 
litarismus, eine Verneinung des internationalen Rechtes eine permanente 
Insulte für die Besiegten seitens der Sieger, ein beständiger Angriff gegen 
eines der grossen Prinzipien der französichen Republik, der Brüderlichkeit 
ist — decretirt: Einziger Artikel: Die Säule des Vendome-Platzes wird 
zerstört.“ Vergeblich suchte Victor Hugo die Säule in einer Ode zu rotten. 
Ein paar Wochen später sollte vielmelir in Erfüllung gehen, was einst 
©in deutscher Dichter, Heinrich Heine, in einem Briefe prophezeit hatte. Dieser