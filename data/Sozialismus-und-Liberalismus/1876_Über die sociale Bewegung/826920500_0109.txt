97 
Endlich gibt es kaum einen anderen Punkt, wo die 
herrschenden misslichen Zustände sich so scharf ausprägen, 
und wo zugleich die fortwährende Rück- und Weiterbildung 
eine so energische ist, wie die Wohnungsverhältnisse des Ar 
beiters. Man muss in die Arbeiterwohnungen, wie sie unter ge 
wöhnlichen Miethsverhältnissen, zumal bei unverhältnissmässig 
anwachsender Bevölkerung angeboten werden, mit eigenen 
Augen hineingeschaut haben, um all’ die physischen und mora 
lischen Uebel, die ganze sociale Misere derselben zu verstehen. 
Kein Wunder also, wenn die sociale Frage in unseren 
Tagen in ihrer einschneidendsten, drohendsten, ins Fleisch der 
modernen Gesellschaft am meisten sich einbohfenden Spitze 
als Arbeiterfrage so bedenklich hervortritt, wobei es sich nicht 
allein, wie schon aus dem Vorhergehenden resultirt, um die 
Fabrikarbeiter , sondern auch um die ländlichen , überhaupt 
um die Lohnarbeiter*) handelt. Ihre bei der numerischen Ab 
nahme des bisherigen Mittelstandes täglich wachsende Schaar 
bildet heute einen neuen Stand, den vierten, dessen Interessen 
der heute auf der Tagesordnung stehenden Arbeiterfrage zu 
Grunde liegen, die wiederum aus einer Fülle von Einzelfragen 
Pensions- und Invalidenkassenwesen, Fabrikgesetzgebung, 
Schiedsgerichte und Einigungsämter — besteht, deren Ergrün 
dung grosse Mühe und Anstrengung erfordert und deren jede, 
ohne die Allgemeinheit ihres Zusammenhangs aus dem Auge 
zu verlieren, insbesondere ihrer Lösung soweit als möglich 
entgegengeführt werden muss. 
•) Der Sprachgebrauch verbindet mit dem Worte „Arbeiter“ in der 
Regel den engeren Sinn des wesentlich von körperlicher Arbeit lebenden 
Lohnarbeiters. Streng genommen sind unter dem Ausdruck „Arbeiter“ 
alle diejenigen zu verstehen, welche nicht nur durch ihre körperliche, 
sondern auch durch ihre geistige Thätigkeit Nutzen gewähren, denn die 
geistige Wirksamkeit spielt in der Produktion die Hauptrolle. Es sind 
daher die Gelehrten, die Unternehmer und die Handarbeiter sämmtlich 
Arbeiter und es sollte (wie Böhmert betont) im allen Fabriken und Werk 
stätten und am Eingänge in alle Häuser und Gemeinden der Wahlspruch 
„Viribus unitis“ (mit vereinten Kräften) die Leute zum Frieden und zum 
Yerständniss ihrer eigenen Interessen mahnen. 
C O B t B • B, BMial« B*W»fBBg. >7