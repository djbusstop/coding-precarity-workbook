130 
ein dunkler Fleck das Sklaventhum und die Verachtung der Arbeit. 
Die Führung des Staates Hess man durch eine beschrankte Anzahl be 
rechtigter Bürger geschehen. Die politische Freiheit war eine sehr be 
schränkte. Einer kleinen Gemeinde von Bürgern standen die Sklaven 
rechtlos gegenüber. Selbst die Philosophen wie Platon erkennen diesen 
Zustand als einen nothwendigen an. Eine innere Fäulniss, wie sie 
schon Thucydides beschreibt, untergrub das Heidenthum, bis das Chri 
stenthum mit seiner Idee der ewigen Persönlichkeit die Welt verjüngte. 
Eine merkwürdige Stellung nehmen die Einrichtungen der Juden ein, 
die wirklich darauf berechnet sind, dass jeder, der im Schweisse seines 
Angesichtes arbeitet, sein Brod zu essen habe. So lange das jüdische 
Volk treu an den mosaischen Satzungen festhielt, gab es diesen Satzungen 
gemäss bei ihm keine Proletarier, keine Arme in der Weise, wie sie 
uns bei den späteren Kulturvölkern, den Griechen, Römern und so weiter, 
und in unsern Tagen begegnen. 
Der Kriegsgefangene als Sklave, der Dürftige und der Fremdling 
erfreuten sich der Achtung als Mensch. Im mosaischen Staate besass 
jeder Mann und jede Familie einen unveräusserlichen Ackerantheil. 
Wie anders sah es mit der Freiheit des Einzelnen in Israel als in Ägypten 
mit seinem Kastenwesen aus. Die socialen Anordnungen in Israel ent 
halten für uns praktische Winke von tiefer Bedeutung, und zwar nicht 
für den Einzelnen, sondern auch für die Gesellschaft, für die Staaten. 
Die sociale Frage konnte in Israel nicht gestellt werden. Was als 
höchste Errungenschaft gilt in der neueren Zeit, in dem Besitze dieser 
Freiheit war das Volk Gottes schon vor 3000 Jahren. Ein Nationalöko 
nom, der von unsern Verhältnissen aus den Blick in jene Zustände wirft, 
sollte meinen in das Land der Träume, der sehnsüchtigsten Hoffnungen 
zu schauen. Die Griechen nehmen in Bezug auf ihre Weltanschauungen 
zwischen den Orientalen und den christlichen Völkern des Abendlandes 
eine mittlere Stellung ein. Ihnen galt das menschliche Leben nicht 
wie jenen als ein in feste und unwandelbare Schranken eingeschlos 
senes, sondern nur als entwickelungsfähig zu einem bestimmten Ziele. 
Zur Erreichung dieses Zieles hat die Wirthschaft die äusseren 
Hilfsmittel zu beschaffen und Vermögen ist daher der Inbegriff von 
Werkzeugen zur Erreichung der Zwecke des Lebens. Socrates hält das 
Handwerk für nöthig, aber eines Freien unwürdig. Aristoteles weiset 
den Handwerkern und Künstlern ihre Stelle neben den Sklaven an. 
Auch die Arbeiter im Reiche des Gedankens gehören zu den Banausen, 
wenn ihre Arbeit auf Erwerb gerichtet war. Nur Polygnotos macht 
eine Ausnahme von der öffentlichen Verachtung, weil er uuentgeldlich 
malte; als aber der berühmte Zeuxis das von ihm gemalte Bild Helena’s 
für Geld zeigte, nannte man es das Bild einer Buhlerin. Zu den Ba 
nausen gehörten die Lehrer, die Sophisten, die Redner, die Dichter, die