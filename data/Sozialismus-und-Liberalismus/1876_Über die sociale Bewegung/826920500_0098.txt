IV. 
Die sociale Frage der Gegenwart. BegriflP, Wesen 
und die einzelnen Zweige derselben. 
Die sociale Frage steht auf der Tagesordnung unserer 
Zeit und wir haben sie, wo möglich, zu lösen und zu erledigen. 
Diese Aufgabe und Pflicht erfüllt sich nicht durch die 
trostloseste Blindheit und Taubheit gegenüber den bestehenden 
Missständen und Disharmonien der Gesellschaft. Der Grund 
satz taugt nichts , Alles beim Alten und die Dinge gehen zu 
lassen, wie sie können (Thornton). Es ist nichts gethan mit 
der scheinbar sittlichen Indignation gegen den Socialismus. 
Ebensowenig fruchtet die Bemängelung und Verlästerung be 
stehender Verhältnisse durch allgemeine Phrasen und Schlag 
wörter. Nur der leidenschaftlichen Hitze schmeichelt das 
Schroffe und Scharfe; aber das zu düstere Ausmalen der 
Schattenseiten bringt nicht Licht, Klarheit und Ordnung in 
die Verwirrung ; Unbehagen und Unzufriedenheit werden nur 
unerträglicher gemacht und geschärft; bittere Gährung und 
Gift auch sonst gesunden und werkthätigen Naturen beige 
bracht , wenn sie von den „Bettcrn der Gesellschaft syste 
matisch angelernt und gewöhnt werden, „ihre V ünsche mit 
ihren Rechten und ihre Bedürfnisse mit ihren billigen An 
sprüchen zu verwechseln.“ Es gilt vor Allem allseitig rich 
tiges Verständniss.*) 
*) „Jedenfalls muss das Erkennen in Bezug auf 
liehe Wirthschafl dem Vor schreiben vorausgehen. Rieses 
mmmsi 
suchungen in der Zeilschi-ifl für Schweiz. Statistik 18,i.