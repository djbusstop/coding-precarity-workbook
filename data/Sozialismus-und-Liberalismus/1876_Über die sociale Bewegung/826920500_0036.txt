24 
Aufgaben beabsichtigt die neue Schule zu erörtern. Sie folgt 
dem Beispiel einiger Katheder-Socialisten nicht, welche die 
Befugniss des Staates über Gebühr vergrössern — die Freiheit 
wird als leitendes Princip angenommen und praktisch geübt. 
Nur im seltensten Falle, wenn die Nothwendigkeit am Tage 
liegt, gibt man versuchsweise und als Ausnahme irgend eine 
Beschränkung zu. Um dieses Ziel erreichen zu können, suchen 
die Anhänger dieser Schule mit grosser Vorsicht vorzugehen, 
und die englische Methode der „Enquêtes“ anzuwenden *) 
Aber die Frage der Staatsstellung ist weder die einzige noch 
die wichtigste der ökonomischen Forschung und die Schule hat 
noch andere zu prüfen. So will sie sich denn auch der Ge 
schichte und Statistik bedienen und mit steter Anwendung 
der Beobachtungsmethode diejenigen volkswirthschaftlichen 
Lehrsätze, welche als unerschütterlich gelten, einer thatsäch- 
lichen Prüfung zu unterwerfen.“ 
So begegnen wir in Italien einer ausserordentlichen Rührig 
keit auf volkswirthschaftlichem Gebiete, welche zugleich den 
Beweis liefert, dass die italienische Nationalökonomie heute 
ebenso wie in früheren Zeiten ebenbürtig der Literatur anderer 
Völker sich zur Seite zu stellen vermag, wobei als ein be 
sonderes Moment die entschiedene Hinneigung der neuen 
Schule zur Beachtung der ethisch-politischen Beziehungen des 
ökonomischen Völkerlebens, die specielle Betonung der natio 
nalen Gütervertheilung im Gegensatz zur Produktion , sowie 
auch ein bewusstes Stroben an praktische Fragen anzuknüpfen 
hervorgehoben zu werden verdient. 
Die Wissenschaft der Nationalökonomie hat in Italien 
überhaupt von den ersten Anfängen ihres Auftretens im 
Mittelalter**) bis in die Gegenwart immer einen fruchtbaren 
*) Ein Beispiel von der Fruchtbarkeit dieser Methode liefert die in 
Italien von Luzzatti geleitete und vor Kurzem beendete industrielle Enquête, 
welche sich allgemeiner Anerkennung erfreut. Wie für S c h u 1 z e’s Ge 
nossenschaftswesen , so int^ressirt sich Luzzatti lebhaft für die Enquête 
Böhinert’s bezüglich der Thoilnahme der Arbeiter am Reingewinn. 
**) Rossi, Economie politique tom. I. p. 220. C o n t z e n, 
Geschichte der volkswirthschaftlichen Literatur im Mittelalter. 2.