122 
ganz natürlich zur Emancipation hindrängt und ihr den Zu 
gang zur naturgemässen Verbindung mit dem Manne, zur Ehe, 
mehr und mehr verschliesst. Wahrlich die Frauen unserer 
Tage haben in diesem Betracht einzustimmen in die Worte 
der Iphigenie, welche ihr Göthe in den Mund legt : „Der 
Frauen Zustand ist beklagenswerth.“ *) 
Wir sehen daher auch das Weib durch die modernen 
Zustände in ihren heiligsten und then ersten Interessen verletzt 
und es ist wahrlich kein blinder Zufall, dass wir fast überall 
kühne und leidenschaftliche Frauen an der Spitze der socialen 
Kämpfe sehen, wie noch jüngst bei der Pariser Commune, 
wo die Rolle der Petroleusen mit Schrecken und Abscheu 
uns erfüllte und an die Worte Schiller’s erinnerte : 
Die Strassen füllen sich, die Hallen 
Und Würgerbanden zieh’n umher. 
Da werden Weiher zu Hyänen 
Und treiben mit Entsetzen Scherz! — 
Die ökonomische Sphäre des Weibes ist ihrem natürlichen 
Grunde gemäss unmittelbar das Haus, die Hauswirthschaft ; 
und nur mittelbar durch Mann und Kinder die weite Welt, 
die öffentliche Wirthschaft. Wo die Frau in den eigentlichen 
industriellen Beruf hineingezogen wird, streift sie nothwendig 
die Eigenschaften ab, die sie für ihren eigensten Beruf nicht 
entbehren sollte; sie verkömmt als Weib, und wird doch kein 
Mann. / 
Ein selbständiger Gewerbsbetrieb setzt das Durchmachen 
einer Schule voraus , welche für die Entwicklung des eigent 
lich Weiblichen wenig übrig lässt. Der weibliche Charakter 
bildet sich nicht „im Strom der Welt“, sondern „in der Stille“, 
welche seine Schwächen schützt und seine Stärke pflegt. In- 
*) „Wüthender noch als die Männer (sagt ein Zeitgenosse der hus- 
sitischen Revolution) sind die zahlreichen Weiber, die brennen und sengen, 
ihren unglücklichen Schlachtopfern die Eingeweide aus dem Leihe reissen“ ; 
an einer andern Stelle heisst es: „Auch die Weiber nahmen Theil an 
den Kriegszügen und zeichneten sich bei der Plünderung eroberter Orte 
nicht allein durch ihre Raubgier, sondern noch mehr durch ihre Grausam 
keit aus.“