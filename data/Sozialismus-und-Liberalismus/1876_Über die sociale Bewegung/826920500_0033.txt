21 
neue sittliche und ökonomische Kräfte, deren Ausdruck seine 
Volksbanken, Konsum- und Bildungsvereine u. s. w. waren. 
Er lud alle Armen, die Handwerker und Bauern ein , sich 
durch ein energisches Sparsystem und Einigung aller zu ge 
meinsamem Handeln ein Kapital aus dem Kredit zu schaffen. 
Schulze träumte die Harmonie nicht, er bildete sie sich nicht 
ein, er setzte sie in das Werk, stiftete sie ! Eine andere deutsche 
Schule wendet die Beobachtungsmethode auf die Volkswirth- 
schaft an, und läugnet die allgemeinen natürlichen nothwen- 
digen Gesetze, im Recht, wie in der Nationalökonomie, Die 
historische Schule ist ein Zweig derselben. Sie hat ohne 
Zweifel das Gesetz der historischen Continuität und Fa 
talität im Recht, wie in der Nationalökonomie übertrieben, 
und zugleich den Irrthum begangen, das Ideal allge 
meiner Gesetze zu läugnen ; aber Knies, Roscher, Hilde 
brand, Anderer zu geschweigen, haben doch eine unendliche 
Menge von Problemen im Lichte ihrer Methode aufgeklärt. 
Sie zeigten, dass das ökonomische Element weder das einzige 
noch das überwiegende sei, sondern sich allen anderen gesell 
schaftlichen, sittlichen und politischen Elementen, die eine Epoche 
bilden, anreihe , und dass der wahre Mensch nicht der ideale 
sei, den die Nationalökonomen in ihren Büchern träumten, 
sondern der historische. Mit Recht betrachtet diese Schule 
die Geschichte der Volkswirthschaft und ihrer Einrichtungen 
nicht wie einen äusserlichen Schmuck der ökonomischen Ge 
setze, sondern als einen innerlichen und wesentlichen Bestand- 
theil der Wissenschaft. Ohne die ökonomische Geschichte des 
Eigenthums würde kein deutscher Rechtsgelehrter es unternehmen 
wollen, die Frage der ökonomischen Fesseln des Eigenthums 
zu beantworten. Dasselbe gilt für ähnliche Aufgaben. Ein 
anderer Zweig dieser experimentalen Schule bedient sich der 
Statistik zum Studium der Staatswirthschaft. Er erinnert an 
den Satz von Aristoteles : Das W issen muss die Phänomene 
bestätigen, und diese ihrerseits das Wissen. Von all’ den be 
rühmten Ökonomisten Deutschlands nennen wir nur Wappäus, 
der in seinem vortrefflichen Buche über die Bevölkerung, an-