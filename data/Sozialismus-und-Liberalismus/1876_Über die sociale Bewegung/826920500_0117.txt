105 
Zu bemerken ist noch, dass die sociale Bewegung selbst 
in die entferntesten Lagen der Wälder ein gedrungen ist, dass 
die socialistischen Ideen anfangen, auch unter den W aldar 
be item Anklang zu finden.*) Wenn auch unsere Wald- 
wirthschaften noch nicht, wie dermalen Industrie und Ge 
werbe von der Socialdemokratie tyrannisirt werden , so fehlt 
es doch selbst auch da nicht an Versuchen, den Heerd ihrer 
Thätigkeit dahin zu verlegen, was insbesondere in höchst fühl 
barer Weise in jenen Waldlagen zur Geltung gelangte, welche 
durch die Orkane der Jahre 1868 und 1870 in Österreich 
und Deutschland tief geschädigt wurden und in der Gegen 
wart durch die nachfolgenden Insectenverheerungen mit der 
Vernichtung bedroht werden.**) Ein rechtzeitiges Einschreiten 
gegen diesen bedrohlichen Zustand der beginnenden Arbeiter 
bewegung beim Waldbau dürfte vollständig gerechtfertigt sein. 
Will der Forstwirth sich gute Arbeiter sichern, so muss 
er vor Allem so viel als möglich für die Verbesserung ihrer 
ökonomischen Lage sorgen, den Corporationsgeist unter ihnen 
Tom Hofe gelegenen Ackerstückes zum Anbau der Kartoffeln und wohl 
auch des für die kleine Wirtbschaft erforderlichen Flachses; 
4. Die Verstattung der Haltung einer Kuh oder die Gewährung von 
Milch als Theil des Natural-Deputats ; 
5. Die Berechnung des durch Kauf zu deckenden Bedarfs an Naturalien 
aus der Gutswirthschaft nach mässigen Durchschnittspreisen; 
6. allmählig steigende Erhöhung des Einkommens bis zu Maximal« 
betrügen ; 
7. Aussicht auf Inraliden-Pensionen in höherem Alter und bei Dienst 
unfähigkeit; 
8. Gewährung von Prämien bei hervorragend braver Dienstleistung 
und von Belohnungen für besondere Auszeichnungen im Dienste; 
9. Gründung von Arbeiter-Sparkassen und Versicherungs-Anstalten 
zur Hilfe in Unglüksfällen; 
10. Errichtung von Kleinkinder-Bewahranstalten ; 
11. Bildung eines Ehrengerichts unter den Arbeitern. 
•) Siehe Forstliche Berichte. 1867. 2. und 3. Heft: Die Arbeiterbe 
wegung und die Waldarbeiter, sowie über die Gründung von Arbeiter 
kassen. 
**) A. R. Ton Korners: Jahrbuch für österreichische Landwirthe. 1873.