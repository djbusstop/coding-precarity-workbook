89 
grossen, in einander verschlungenen Ganzen findet jede einzelne For 
derung die rechte Stelle und Stütze, nur innerhalb desselben wird es 
möglich, auch die gedachten speziellen Verhältnisse in harmonischer 
Gesammtentwicklung der Vervollkommnung entgegenzufahren. Das Zer 
spalten in Theilfragen dagegen, das Herausreissen einzelner Beziehungen 
aus dem Ganzen, mag nur gar zu leicht einen Conflict der Sonderinteressen 
herbeiführen, einen Klassenkampf entzünden, der, neben seinen ge 
meinschädlichen Folgen, am härtesten auf diejenigen zurückfallen würde, 
die ihn herausgefordert hätten. Denn niemals, das bestätigt die Ge 
schichte, hat eine einzelne Gesellschaftsklasse, die, isolirt von den übrigen 
auf Kosten der Gesammtentwicklung vermeintliche Sonderinteressen 
verfolgte, auf die,Dauer etwas anderes erzielt als, neben dem Verfall 
des Gemeinwesens, den eigenen Ruin.*) 
^ Jeder Versuch zur Lösung der socialen Frage, welcher 
nicht von diesem universellen Standpunkte aus seine Aufgabe 
erfasst, ist als ein hoffnungsloses Beginnen zu betrachten.**) 
) Schulze-Delitzsch bekämpft vor Allem mit Recht die Auflassung, 
als ob die sociale Frage eben nur die Arbeiter, überhaupt solche be 
rühre, die mit ihrem Loose unzufrieden zu sein mehr oder weniger Ur 
sache haben möchten, während die sogenannten „gemachten Leute“ mit 
der ganzen Angelegenheit höchstens nur soweit zu thun hätten, als es 
gelte, etwaigen extravaganten, auf den Umsturz des Bestehenden ab 
zielenden Forderungen von jener Seite entgegenzutreten. Die sociale 
Frage ist vielmehr im tiefsten Grunde eine Existenzfrage der modernen 
Kultur. Es handelt sich demnach bei ihr nicht darum, einen speciellen 
Interessenkampf zwischen Arbeit und Kapital durchzuführen, viel mehr 
um einen einheitlichen Kulturfortschritt der ganzen Gesellschaft, um 
eine gesunde Organisation des ganzen gesellschaftlichen Lebens, ver 
möge welcher es jedem Gliede möglich gemacht werden soll, seine na 
türliche Anlage vollständig zu entfalten, ohne dass dadurch die Grundlage 
der Existenz anderer verletzt und in ihrer Entwickelung gehemmt werde. 
Die sociale Frage muss, wie v. Bilinski a. a. 0. betont und wie wir im 
Verlaufe unserer Betrachtungen sehen werden, naturgemäss in so viele 
einzelne Fragen zerfallen, als in der Gegenwart eiternde Wunden am 
Wirthschafts- und Gesellschaftskörper der Völker gibt. Die Arbeiter-, 
die Wohnungs- und die Frauenfrage, sowie die Lehrerfrage spielen 
dabei die hauptsächlichsten Rollen in der Gegenwart. Auch die Armen- 
und Waisenpflege bilden einen integrirenden Bestandtheil der socialen 
Frage; endlich liegt innerhalb ihres Gebiets ein bedeutender Theil der 
Bevölkerungs- und Gesundheitspflege. A. Sch^root: Ueber Industrialis 
mus. Hamburg 187.3. 
♦*) „Wenn die Wissenschaft sich so einseitig ausbildet und auf theil-