100 
Die massenhaften Arbeitseinstellungen, welche in jüngster 
Zeit in den grösseren Städten in Scene gesetzt sind, die 
tumultuasen Vorgänge in Königshütte, auf einem Gebiete, wo 
volles Material für die Lösung der heute so brennenden socialen Frage 
zusammengetragen und es fehlt weder an Fleiss, noch an Talent und 
Geschick, jenes Material täglich in gesteigertem Masse zu vermehren. 
Allein die bezüglichen Untersuchungen waren in der Regel in erster 
Linie auf die industriellen Klassen beschränkt, während die ländlichen 
Arbeiter-Verhältnissse, wenn wir die Schrift von v. Lengerke: „Die länd 
liche Arbeiterfrage“ und Schinoller's Abhandlung in der Tübinger Zeit 
schrift für 1866: ,Die ländlichen Arbeiterverhältnisse mit besonderer Rück 
sicht auf die norddeutschen Verhältnisse“ abrechnen, in der Regel nur 
beiläufig in Betracht kamen. Herrn von der Goltz gebührt das Ver 
dienst. zuerst in systematischer Weise die ländliche Arbeiterfrage in 
ihrem ganzen Umfange behandelt zu haben. Seinem Vorgänge folgten 
F. Knauer, (Die soziale Frage auf dem platten Lande. Berlin 1873), 
Lobe, Sucker u. A. An dieser Stelle sei auch der vom Congress 
deutscher Landwirthe angeregten Enquête über die Lage der ländlichen 
Arbeiter im deutschen Reiche gedacht, deren Ergebnisse vor Kurzem von 
dem Redaktionsausschuss, bestehend aus den Herren von der Goltz, von 
Langsdorf und Richter, in einem Quartalbande von 503 Seiten der 
Öffentlichkeit übergeben worden sind. (Die Lage der ländlichen Arbeiter 
im deutschen Reiche. Berlin Wiegandt, Hempel und Parey.) 
Unter den Ursachen, welche den von uns in einem früheren Abschnitte 
berührten Unterschied des Lohnes nach verschiedenen Gegenden bedingen, 
führt der Bericht in erster Linie die Verschiedenheiten des Angebots 
und der Nachfrage auf. Er bemerkt z. B. dass die hohen Löhne, welche 
man in Oldenburg, sowie in den Landbezirken von Stade und Stralsund 
findet, wesentlich auf grossen Mangel an Arbeitskräften zurückzuführen 
seien. Dieser letztere komme aber von dem Ueberwiegen des Grossgrund 
besitzes und dem Zurücktreten des bäuerlichen Besitzes her. Im Reg.-Bez. 
Stralsund bedinge die geringe Zahl von Grundeigenthümern wesentlich den 
grossen Mangel an Arbeitern und die hohen Löhne derselben. Das gleiche gelte 
von Oldenburg und von denjenigen Bezirken Bayerns, wo der Grossgrund- 
besitz vorwaltet. Der Bericht glaubt daher den Satz aufstellen zu dürfen: 
dass da, wo der grosse und geschlossene Grundbesitz besonders stark 
vertreten ist, die Löhne verhältnissmässig immer höher sind als dort wo 
auch der bäuerliche und kleine Besitz zahlreicher vorhanden. 
Als weitere Ursache für die Höhe der Löhne zieht der Bericht die 
natürliche Productivität des Bodens, bezw. die Rentabilität des landwirth- 
schaftUchen Gewerbes, in Betracht. Bei günstigem Boden und Klima 
welche einen manniohfaltigeren und ausgiebigeren Betrieb des landwirth-