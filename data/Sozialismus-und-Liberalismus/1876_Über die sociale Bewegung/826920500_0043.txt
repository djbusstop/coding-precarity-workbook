dem, welche berufen ist, den Wohlstand der Nation zu heben, 
den socialen Frieden zu ermöglichen*) 
*) Als eine hemerkenswerthe Thal sache ist hier noch die Würdigung 
S c h u 1 z e ’ s in F rankreich durch die Übersetzung seines „ Arheiterkatechis- 
mus“ von Benjamin Rampai zu erwähnen. V^l. Cours d'economie 
politique a l’usage des ouvriers et des artisans par Schulze-De 
li tzscli. Traduit et piécédé d'une esquisse hiogra])liique et d'un aperçu 
sur les nouvelles doctrines par Benjamin Rampai. Paris 1874 (2 Bde.) 
Eine Reihe von französischen Zeitungen und Zeitschriften (z. B. Journal 
des Economistes, L’Economist français, Le Mémorial diplomatique, La 
France, Le Siècle, L’Opinone nationale, Gazette du Midi (Marseille), Jour 
nal de Rouen, L’Auhe, L’Arrondissement d’Arc, L'Indépendant) haben 
dem Rampal’schen Werke ihre Aufmerksamkeit gewidmet. Erst der 
zweite Band enthält die sorgfältige Übersetzung des ,.Arbeiterkathechis- 
mus“, während der erste eine auslührliche Biographit^ Schulze’s, ein 
Wahrheitsgetreues Bild seiner Persönlichkeit, seines Lebens und Wirkens, 
sowie eine kritische wissenschaftliche Beleuchtung des Genossenschafts 
wesens und seiner Entwicklung bietet. In der Vorrede sagt Rampai von 
dem „Arbeiterkatechismus“ : „Nous ne connaissons ni dans notre littéra 
ture ni dans celle d’aucune autre nation un ouviage répoi.dant mieux, 
sous une forme élémentaire, aux questions (jui préoccupent à un si haut 
point les esprits en France et dans les autres États de l'Europe occiden 
tale, questions qui sont communes à tous les pays où dans le cours de 
ce siècle, s’est constitué et développée la grande industrie, ou bien 
s'est maintenue la grande propriété et (ju'on désigne sous le titre géné 
ral de question sociale. En présence des dissentiments profonds 
qui divisent la société française touchant le problème de la condition 
humaine, nous avons considéré comme un devoir ¡¡atriotique de faire 
entendre la voix d'un homme dont la vie a été consacrée à l’étude de 
ces questions ardues et dont l'autorité a reçu la double sanction de 
la science et de la pratique“. Indern die R a m iial’sche Übersetzung die 
gebildete französische Welt nach einer bestimmten Seite hin zu einer 
ruhigen Beschäftigung mit deutschen Leistungen auffordert, bahnt sie 
zugleich den Grundsätzen der Genossenschaftsbewegung neue Wege, 
auf denen sich ihre erlösende und versöhnende Kraft bewähren kann. 
Schulze-Delitzsch , der gegenwärtig auf eine segensreiche i2õjäh 
rige Thätigkeit zurückblicken kann, ist so zu einem Wohlthäter nicht 
bloss der deutschen Nation, sondern der ganzen Menschheit geworden. 
Näheres über die neuere Literatur zur Genossenschaftsbewegung sieh^ 
Im Arbeiterfreund von Bö liniert 1875.“ I. Heft. S. 07—73.