16 
die so geläufig gewordenen Laissez-faire-Grundsätze zur allseiti 
gen Verständigung im Lager des volkswirthschaftlichen Fort 
schritts nicht mehr aus. Die bisher im Hintergründe schlum 
mernden tiefem Gegensätze in der Auffassung der Volkswirth- 
schaft und ihrer Stellung zum Staate, sowie hinsichtlich der 
genaueren Bestimmung und Ableitung der die Wissenschaft 
beherrschenden wirthschaftlichen Grundbegriffe werden nunmehr 
in den Kampf gerufen.*) 
wegrüumiing nller Beschränkungen und Hemmnisse des Verkehrs; sei diese 
JHinwegräumung einmal vollständig durchgeführt, so würden die einzelnen 
Kräfte schon ihren Wog finden, und es würde das entstehen, was er eben 
harmonies économiques, d. h. etwa Ausgleich der volkswirthschaftlichen 
Bestrebungen, nennt. 
*) Vgl. viele sehr beherzigungswerthe Bemerkungen in der so eben 
erschienenen Schrifc von Heinrich von Treitschke: Der Socialis- 
nms und seine Gönner. Nebst einem Sendschreiben an Gustav 
Schmoll er. Berlin 1875. Daselbst heisst es u. A. S. 85 und 141: 
„Der reinen Freihändler sind wenige, die Meisten erkennen im Allgemeinen 
die Lehren der englischen Schule an und fügen bei jedem Satze ihre 
Vorbehalte hinzu.“ — „Heute wird gerade in ihrem Kreise sehr eifrig jene 
■Wissenschaft geprtegt, die für alle sociale Reform den Weg bahnen muss: 
die sociale Statistik. Ich bekenne gerne, dass ich aus den thntsächlichen 
Mittheilungen in Böhmert’s „Arbeiterfreund“ mehr gelernt habe, als aus 
mancher anspruchsvollen Theorie der Sociallehro.“ Eine unbefangene Prü 
fung und Vergleichung der Berichte über die Danziger , Crefelder und 
Eisenacher Verhandlungen ergibt, dass sich auf den Versammlungen beider 
Vereine von Volkswirthen die verschiedensten Standpunkte und Parteien 
begegnen, und dass beide sich redlich bemühen, durch Berichterstattungen 
und Debatten Beiträge zur Beleuchtung brennender wirthschaftlicher Ta 
gesfragen zu liefern. Die Frage der Wohnuiigsnoth, welche auf der Tages 
ordnung der ersten Eisenacher Versammlung stand und von Engel beleuchtet 
wurde, ist auch schon auf früheren volkswirthschaftlichen Kongressen 
von verschiedenen Standpunkten aus erörtert worden. Auch mit der in 
Eisenach (1872) verhandelten Hauptfrage, der Fabrikgesetzgebung, haben 
sich die volkswirthschaftlichen Kongresse mehrfach beschäftigt und Aus 
nahmen von der allgemeinen Erwerbs - und Vertragsfreiheit bei Kindern 
und Unmündigen, die sich nicht selbst schützen können, als selbstver 
ständlich erachtet. Endlich lautet eine Resolution des Lübecker volks 
wirthschaftlichen Kongresses von 1871 : 
Zur Verhütung von Arbeitseinstellungen empfiehlt der volkswirthschaft- 
liche Kongress den betheiligten Kreisen die Einrichtung von Vergleichungs- 
ausschüsseu.