68 
wirthschaftlichen Schwerpunkt der volkswirthschaftlichen Organi 
sation, der Kapitalismus war noch nicht herrschende 
Grundform der volkswirthschaftlichen Verbindung geworden.*) 
Arme konnten wohl gegen die Reichen aufgerufen werden, 
aber die grossen Gegensätze zwischen Lohn und Kapitalprofit 
konnten nicht rationell bewusst werden. Ein national-ökono 
mischer Schlachtenruf gegen das Kapital, wie ihn Ferdinand 
Lassalle und der Mitbegründer der Internationale**), Carl 
Marx zu London, erhoben haben, wäre im Alterthum, wo über 
haupt die Arbeiterfrage in der heutigen Gestalt noch nicht 
vorhanden war , da es damals noch keine freien Arbeiter im 
heutigen Sinne gab, geradezu unverstanden geblieben. 
Sodann war in allen früheren Zeiten ein grosser Theil 
der individuellen Erwerbskreise einer Massenbewegung ent 
zogen***). 
Die mittelalterliche Staats- und Gesellschaftsordnung na 
mentlich war auf eine ganz andere Basis gebaut, als die 
neuzeitige. 
Das Recht des vollen Lebensgenusses , Besitz und Er- 
Familien-, Gewerbe-, Verfassung^- und Finanzrechtes gemacht, um Eu 
ropa vor der potenzirten Wiederholung des Elends und der Schande 
antiker Plutokratie und Ochlokratie zu bewahren. Trefflich sind auch 
die Bemerkungen des Verfassers über die mit der steigenden Macht der 
Börsenbarope zusammenhängende (Korruption der Presse. 
*) Hieran reiht sich in natfirlichem Zusammenhänge eine Kette 
von wirthschaftlichen Einrichtungen, Hebeln und .Mitteln, die der alten 
Welt insgesammt abgingen, oder in höchst unvollständiger Form, oder 
nur in einzelnen localen Kreisen vorhanden waren. So vor Allem der 
Mangel an Communications- und Transportmitteln, an Geld-, Bank- 
und Creditinstituten, an Assecuranz- und Versicherungsanstalten. 
♦*) Die ersten Anfänge der Internationale reichen vermuthlich 
bis in die Tage der Februarrevolution zurück. Pher die International® 
siehe u. A. Tüll io Martello Storia dell’ Internazionale. 
***) Doch lassen sich bereits im 1.3. Jahrhundert Versuche, Arbeits 
einstellungen in Scene zu setzen , nachweisen, 1475 der erste grössere 
Sieg der Gesellen über die Meister, in Folge dessen das Gewerbe der 
Blechschmiede in kurzer Zeit aus Nürnberg verschwand ; schon damals 
handelte es sich um die zwei Ziele: möglichst hohen Lohn und mög 
lichst kurze Arbeitszeit.