81 
die Tendenz, den Ursachen der socialen Bewegungen durci 
vorurtheilsfreie und gründliche Forschung gerecht zu werden.*] 
Eine Geschichte des Socialismus (bemerkt in diesem Sinne 
auch Carl Bücher in seiner Schrift: Die Aufstände dei 
unfreien Arbeiter 143-129 v. Chr. Frankfurt a/M. 1874) 
welche die Theorien nur insoweit berücksichtigte, als sie an 
thatsachhche Verhältnisse anknüpfen oder irgendwo praktisch 
geworden sind , scheint überhaupt ein dringendes Bedarf, 
aiss zu sein. Sie würde uns auf die empirischen Gesetze der 
epidemischen Atrophie ganzer Bevölkerungsklassen führen und 
uns ebensowohl von manchen theoretischen Schnurrpfeifereien 
betreien, als von dem unverantwortlichen Leichtsinn, der sich 
amit begnügt die ganze moderne Bewegung als eine „künst. 
hch gemachte“, ihre Principien, für einen Wust von Halb- 
Wisserei und Phrasenthum zu erklären. 
wssonschaftlichon Behandlung 
ier Volkswirthaehaftslehre .et, wie wir schon an einer anderen 
Stelle betonten, durch die eocialietiechen Systeme wenig ge- 
Wonnen. Die Erfolglosigkeit der praktischen Versuche«) ist