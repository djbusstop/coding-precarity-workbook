﻿die beiden Klassen — welcher Art immer die gegenseitigen Verun-
glimpfungen und Gewalttaten sein mögen — einander immer mehr
achten, je mehr sie einander, längs einer immer ausgedehnteren Front
und durch immer umfangreichere Organisationen, Widerstand leisten.
Heute ist es wahrhaftig auch den verbohrtesten Unternehmern nicht
mehr möglich, die Arbeiterklasse zu verachten. Sie befestigt sich in der
Wirtschaftsordnung und in der Politik, als eine Kraft, die sich sogar bei
jenen eine stille Achtung erzwingt, die sie am meisten hassen und die
vorgeben, sie zu verachten. Die letzte Ausflucht ist der Einwand, daß
sich die Arbeiterschaft von einer Minorität führen läßt. Aber jene, die
diesen Einwand erheben, wissen sehr wohl, daß es schon allein eine
bewunderungswürdige Sache ist, aus der allzuoft trägen und über-
lasteten Masse eine Auslese herangebildet zu haben, die fähig ist, die
Masse zu gewissen Stunden in Bewegung zu versetzen und zu verei-
nigen. Im übrigen wächst die Proportion jener Lohnarbeiter, die in
die Exekutive der Organisation eintreten; und man kann den Tag vor-
hersehen, da den Arbeitern ihr Meisterwerk gelungen sein wird: die
organische und wirkende Einheit der Arbeiterklasse. Das Untemehmer-
tum sieht sich mehr und mehr einer proletarischen Auslese gegenüber,
gegen die es kämpft, mit der es rechnet. Diese proletarische Elite ge-
langte durch Erfahrung zu großen Fähigkeiten, gewöhnte sich durch die
wachsende Ausdehnung der Organisationen an eine immer kompliziertere
Verwaltung, an ausgedehntere Verantwortlichkeiten und eine gewand-
tere Taktik. Andererseits ist es ausgeschlossen, daß die Arbeiter nicht
erkennen lernen, welche Werte durch die bürgerliche Klasse repräsentiert
werden. In den großen und harten Kämpfen der modernen Welt sind
sich die führenden Arbeiter bewußt, welche Schwierigkeit die Rolle ei-
nes Führers der Menschheit bedeutet, die das Bürgertum so lange ge-
spielt hat und noch spielt. Und diese Arbeiter können nicht umhin, die
Energie, die Kraft und kühne Voraussicht jener Führer der Industrie
festzustellen, die ihre sozialen Kampfmethoden erneuerten, die ihrer-
seits auch Disziplin zu halten gelernt haben, die durch Ausgleiche aller
Art die Folgen der Konkurrenz milderten und beschränkten, die ihre
Kräfte sowohl für den Kampf wie für die Produktion anordneten und
die sich entschlossen, die Stoßkraft der Offensive der wuchtenden Macht

5»

67