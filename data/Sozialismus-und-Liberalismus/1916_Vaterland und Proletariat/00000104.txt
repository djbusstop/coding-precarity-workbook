﻿literarischen Schmerzen jenes Charles d'Orleans nahegingen, dessen
Gefangenschaft das Herz des guten Lothringen rührte, in dieser Ge-
sellschaft, die alles eher als ländlich war, erschien Jeanne d'Arc. Sie
war ein schlichtes Landmädchen, das die Schmerzen und Nöte der Bau-
ern, die sie umgaben, gesehen hatte, dem aber all diese Bedrängnisse
nur ein nahegerücktes Beispiel des erhabenen und größeren Leides be-
deuteten, welches das geplünderte Königtum und die überfallene Na-
tion erduldeten. In ihrer Seele und in ihrem Denken spielt kein Ort,
kein Grundbesitz eine Rolle; sie blickt über die lothringischen Felder hin-
weg. Ihr Bauernherz ist größer als alles Bauerntum. Es schlägt für
die fernen, guten Städte, die der Fremdling umzingelt. Auf den Fel-
dern leben, bedeutet nicht, notwendigerweise in den Fragen des Acker-
bodens aufgehen. Im Lärm und Getriebe der Städte wäre Jeannes
Traum sicherlich weniger frei, weniger kühn und umfassend gewesen.
Die Einsamkeit beschützte die Kühnheit ihres Denkens, und sie erlebte
die große vaterländische Gemeinschaft viel stärker, da ihre Phantasie
ohne Verwirrung den stillen Horizont mit einem Schmerz und einer
Hoffnung erfüllen konnte, die darüber hinausgingen. Nicht der Geist
bäuerlicher Auflehnung erfüllte sie; sie wollte ein ganzes großes Frank-
reich befreien, um es späterhin dem Gottesdienst, der Christenheit und
Gerechtigkeit zu weihen. Ihr Ziel erscheint ihr so hoch und gottgefällig,
daß sie, um es zu erreichen, später den Mut findet, sich sogar der Kirche
zu widersetzen und sich auf eine Offenbarung zu berufen, die hoch über
jeder anderen Offenbarung stehe. Den Theologen, die sie drängen, aus
den heiligen Büchern ihre Wunder und ihre Mission zu rechtfertigen,
antwortet sie: „Im Buche Gottes steht mehr geschrieben, als in all
eueren Büchern." Ein wunderbares Wort, das in gewisser Beziehung
im Gegensatz zur Bauernseele steht, deren Glaube vor allem im Her-
kommen wurzelt. Wie fern ist das alles von dem dumpfen engherzig-
beschränkten Pattiotismus des Grundbesitzes! Jeanne aber vernimmt
die göttlichen Stimmen ihres Herzens, indem sie zu den strahlenden
und sanften Himmelshöhen aufblickt.

Ebenso und in noch viel augenscheinlicherer Weise geht der leiden-
schaftlich-revolutionäre Patriotismus weit über die Interessen des
Grundbesitzes hinaus, oder vielmehr, er ist von anderer Ordnung. Die

7«

99