﻿Kräfte hemmen; möge man die Vorurteile besiegen, die Unwissenheit
austreiben; möge doch die gewaltige heimliche Gärung der sozialen
Kräfte in voller Offenheit weiter verlaufen. Wer kann die Säfte der
Natur, wer die Kräfte der Menschen fesseln? „Die Gottheit ist im Be-
griffe, sich zu schaffen", und sie wird nicht dulden, daß man ihr enge
Fesseln von Dogmen und herkömmlichen Anschauungen anlege. „Dehnt
die Gottheit aus" und dehnt in gleichem Maße die menschliche Hoffnung
aus! Sollte dieses große schäumende und wogende Meer der Philo-
sophie Diderots das Bürgertum emportragen oder ertränken? Rous-
seau ruft diese gewaltige, zerstreute Gottheit wieder zum Herd des Be-
wußtseins zurück; er konzentriert sie zu einem moralischen Wesen, damit
sie den Schrei des unterdrückten Gerechten vernehmen könne. Aber wer
ist der große Beraubte, der große Ausgebeutete? Das Volk ist es, oder
vielmehr, es ist der Mensch, vertrieben aus seinem natürlichen Recht
und aus seiner ursprünglichen Freiheit, durch die Gewalt der Gesell-
schaft, vertrieben aus dem Recht des Gemeinbesitzes durch die Anma-
ßung des Einzelbesitzes, vertrieben selbst aus seinem Willen durch die
Anmaßung der Regierung, vertrieben endlich aus leichten einfachen
Freuden durch die Freuden der Eitelkeit, worin sich in der sozialen Welt
der Welteifer der Eigenliebe erschöpft. Oh, daß man doch diesem trau-
rigen Chaos, dieser ungleichen künstlichen Gesellschaft, diesem Schwarm
von Gesichtern, die blaß von Elend, geschminkt von Dünkel, verzerrt
von Neid sind, entrinnen könnte! Oh, daß man ein Ende machen könnte
mit allen diesen falschen Freuden über einem Abgrunde vonSchmerzen,
mit dieser ganzen höllischen Maskerade einer Charonsfähre, die wie
zum Fest bewimpelt ist, die mit dem furchtbaren Widerschein ihrer vene-
zianischen Laternen die dunkeln Wasser des Styr beleuchtet und zur
Strafe der Verdammten noch die Lüge des Glücks dort oben hinzufügt!

Dieser elenden Gesellschaft gilt es also zu entrinnen, aber wie? Soll
man zur Vergangenheit zurückkehren? Die erste Vergangenheit ist voll
Elend: Der Mensch in seiner Urzeit war bloß ein irrendes, zitterndes
Tier. Nicht so weit soll zurückgegangen werden: Die Zeit des Glücks
und des Gleichgewichts war der Beginn jener Epoche, als der Mensch
der Wildheit entronnen, aber noch nicht zur sogenannten Zivilisation
gelangt war; als er die Naturkräfte sich dienstbar zu machen wußte.

29