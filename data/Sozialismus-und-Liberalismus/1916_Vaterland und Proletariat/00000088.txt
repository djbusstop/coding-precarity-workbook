﻿mögen sie an welche Form der Beteiligung immer denken, sei es nun
eine Beteiligung am Erträgnis oder am Kapital der Unternehmung:
es ist nicht durchzuführen, ohne einen Teil der Unternehmerautorität
preiszugeben; es ist ohne Einstellung auf eine neue Idee nicht durch-
zuführen. Ich weiß wohl, daß die Mehrzahl der Lohnarbeiter in den
angekündigten Konzessionen nichts anderes als einen Versuch der Bour-
geoisie sehen, das Proletariat zu „verbürgerlichen", seine Triebkräfte
zu schwächen, die Arbeiterschaft, indem man sie von Fabrik zu Fabrik
am Erfolg der Unternehmer interessiert, uneinig und innerlich unschlüs-
sig zu machen. Und es ist wohl möglich, daß diese bewußte oder unbewußte
Rechnung vielen Plänen dieser Art zugrunde liegt. Aber ohne mich
durch einen haltlosen Optimismus bestimmen zu lassen, behaupte ich,
daß es vom Proletariat abhängen wird, ob es jenen neuen Einrich-
tungen sein Zeichen, das Zeichen seiner Einheit, aufprägen will. Von
ihm wird es abhängen, ob die Arbeiterklasse auf diese Art in das Innere
der Produktion selbst eindringen und sich die neuerliche Erschütterung
der kapitalistischen Herrschaft zunutze machen wird, nachdem sie vorerst
durch ihre Organisationskraft zunichte gemacht haben wird, was an den
Beteiligungsvorschlägen bloß ein bourgeoises Ränkespiel bedeutet.

Der Klassenkampf führt trotz seiner Ausdehnung und trotz seiner zu-
nehmenden Heftigkeit nicht zu einer sozialen Zersplitterung und verletzt
weder das Entwicklungsgesetz der Demokratie noch die Grundeinheit
des Vaterlandes; dies erklärt sich aus dem Umstande, daß alle Aus-
gleiche, Auskunftsmittel und Reformen, die in den verschiedenen Pha-
sen des Kampfes in Erscheinung treten, an der Größe des Kampfes
Anteil haben und Lösungen vorbereiten, die dem Umfange des Pro-
blems angemessen sind. Darauf nehmen alle jene zu wenig Bedacht,
die, indem sie von der zunehmenden Schärfe der sozialen Gegensätze
sprechen, auf die drohende Notwendigkeit von Katastrophen zu schließen
scheinen und die so den teilweisen und allmählichen Eroberungen des
Proletariates fast allen Wert absprechen. Sie bedenken nicht, daß gerade
die Größe des Kampfes sich auf die allmähliche Vereinigung der Rech-
nung überträgt, die die Fortdauer des sozialen Lebens ermöglicht. Ge-
wiß, die Allgegenwart des sozialistischen Gedankens gibt allen Kämpfen
der Bourgeoisie und des Proletariates eine dramatischere Bedeutung.

83