﻿■HMn

ihres eigenen Vorteiles willen ist es ihr Bedürfnis, ihre Ideen in voller
Wirklichkeit zu erproben, die Entdeckungen des Laboratoriums in so-
ziale Kräfte umzusetzen und ihre Formeln der ungeheueren und heil-
samen Probe der Massenproduktion zu unterziehen. So dient der Ka-
pitalismus nicht allein der Gegenwart, er dient auch der Zukunft. Er
entwickelt und stärkt den Geist und macht ihn geschmeidiger, indem er
ihn mit der ungeheueren und wuchtenden Materie den Kampf aufneh-
men läßt; er erteilt den kühnen aber gebrechlichen Flügeln des reinen
Denkens, welches sich steil in die Lüfte erhebt und von vornherein weit
fähiger ist, die Nation zu beherrschen, als sie zu unterstützen,widerstands-
fähigere Festigkeit und vielfältigere Geschicklichkeit. Wie sollte man sich
wundern, daß das Bürgertum zuzeiten, indem es sich selbst mit jenen
unvergänglichen und segensreichen Gewalten verwechselt, dessen ver-
gänglicher und oft unwürdiger Vollstrecker es ist, sich selbst gleich ihnen
ewig und notwendig dünkt? Daher auch jener ungeheure Schwung, der
der menschlichen Tätigkeit vor allem in einem fieberhaft-rastlosen Zeit-
alter unerläßlich ist. Daher auch dieses Selbstvertrauen, dieses Selbst-
bewußtsein, die es bewirken, daß die Bourgeoisie sich nicht als eine
Kraft der rücksichtslosen und unfruchtbaren Eroberung betrachtet, son-
dern als eine Macht von einer Fülle, die es ihr gestatte, durch ihr eigenes
Gedeihen auch für das Gedeihen der anderen zu sorgen und selbst die-
jenigen, die sie ausbeutet, oder von denen man behauptet, daß sie sie
ausbeute, über jenes Niveau zu erheben, auf dem sie ohne diese Aus-
beutung stünden. Daher stammt ferner auch ohne Zweifel, wenn es
gestattet ist, über die Gewissensfragen einer Gesamtheit, die bisher nur
allzu summarischen Untersuchungen unterworfen wurden, Vermutun-
gen aufzustellen, — daher stammt eine zweifache, widerspruchsvolle
Wirkung des sozialen Verhaltens der Bourgeoisie. Gerade weil sie durch
vielerlei Gründe zu dem Glauben an die Gesetzmäßigkeit und Kraft
ihres Privilegs geführt wird, ist sie desto unversöhnlicher, und zwar mit
guter Zuversicht, wenn sie sich gegen einen allzu heftigen Angriff ver-
teidigen zu müssen wähnt. Aber aus dem gleichen Grunde kann das
Bürgertum, von seiner dauernden und wesentlichen Kraft überzeugt,
der aufsteigenden Demokratie, dem erstarkenden Proletariat zu gewissen
Zeiten Zugeständnisse machen, die ihm nicht an sein ungeheuer tätiges

47