﻿Revolution." Darunter verstand er die untrennbar verbundenen na-
tionalen und demokratischen Forderungen. Er wollte ein bourbonisches
Frankreich; ein unterjochtes und zersplittertes Italien; ein kraftlos-
uneiniges, rechtloses Deutschland; ein von der Nutzlosigkeit jedes Wie-
derbelebungsversuches für immer überzeugtes, eingeschüchtertes Polen;
ein abhängiges und überwachtes Ungarn. So war es denn möglich, die
europäische Reaktion zu bekämpfen, ohne, trotz ihr und gegen sie, gleich-
zeitig Nationen und Demokratien erstehen zu lassen; und hätte sich das
Proletariat im Vaterlande wie ein Fremdling betragen, hätte es die
Sarkasmen des Manifestes ernst genommen, dann wäre es nicht mehr
als eine wunderliche Sekte von ohnmächtigen und schädlichen Phantasten
geworden. In keiner Weise wäre es zu einer lebendigen, revolutionären
Kraft geworden, und es hätte, als die Stunde der Tat gekommen war,
all das weiterbetrieben, was am „utopistischen Sozialismus" der ersten
Tage am kindischsten ist. Marx sah auch sicherlich voraus, daß weder die
deutsche Revolution noch die europäische Revolution — die nach seiner
Meinung die Folge der deutschen sein sollte — sich ohne Widerstand
vollziehen würde, über ein revolutionäres und demokratisches Deutsch-
land wären die Kosakenhorden zweifellos ebenso hergefallen, wie über
die ungarische und italienische Revolution. Wie hätte den deutschen Pro-
letariern das deutsche Vaterland gleichgültig sein können, da es doch
von da ab mit der Revolution gleichbedeutend gewesen wäre? Mit un-
auslöschlichem Hasse verfolgte Marx den russischen Zarismus, der in der
europäischen Krise des Jahres 1848 die wirksamste Kraft der Gegen-
revolution bedeutete. Nach den Ereignissen in der Krim beschwor er das
englische Volk und die englische Regierung gegen Rußland unzweideu-
tig und kühn Stellung zu nehmen und hoffte, daß der Fall des mosko-
witischen Kolosses das Zeichen zu einer demokratisch-revolutionären
Wiedergeburt bedeuten werde. Hätte eine Gleichgültigkeit des Prole-
tariates gegen das Vaterland nicht Europa den Kosaken ausgeliefert?
Aber wenn das Proletariat des Okzidents berufen war, mit der Un-
abhängigkeit der demokratischen Völker gleichzeitig seine eigene Aktions-
freiheit und seine Aussichten für die Zukunft zu verteidigen, so bedeu-
dete dies wohl, daß es mit Interessen an das Vaterland geknüpft war,
so bedeutete es, daß es absurd und gefährlich war zu sagen: Die Pro-

93