﻿

Senates fordert; wenn es in England die Abschaffung des Oberhauses,
dieser Festung des Landadels und der konservativen Politik verlangt;
wenn es in Deutschland für die Demokratisierung der Landtage und
der Stadtverwaltungen, für die Errichtung eines parlamentarischen
Regimes und die Volksherrschaft kämpft; wenn es in Ungarn das all-
gemeine Wahlrecht fordert, und wenn es in Österreich den kaum er-
rungenen Parlamentarismus gegen die offensiven Bestrebungen der
Reaktion verteidigt; wenn es sich in den Vereinigten Staaten empört
gegen einen korrumpierenden Föderalismus auflehnt, der den Volks-
willen hindert, sich auf weiten Gebieten zu betätigen und der aller
Ecken und Enden dem anrüchigsten Kapitalismus Unterschlupf gewährt;
kurz, wenn es überall die Demokratie vollständig durchsetzen will, voll-
bringt es gleichzeitig ein revolutionäres und ein erhaltendes Werk. Ein
revolutionäres Werk ist es, weil das Proletariat dadurch künstliche Hin-
dernisse aus dem Wege räumen wird, die sich der Entwicklung und dem
Fortschritt der sozialistischen Macht, die das soziale System revolutio-
nieren will, entgegensetzen. Ein erhaltendes oder zumindest ein be-
schwichtendes Werk ist es, da die Kraft der natürlichen Hindernisse, die
sich aus dem Widerstreit und der Mannigfaltigkeit der Interessen er-
geben, dem Proletariat um so klarer zum Bewußtsein kommen werden,
wenn die künstlichen Hindernisse verschwinden. Diese Dinge versinnbild-
lichen der Arbeiterklasse sogar die Schwierigkeiten, die sie zu überwin-
den hat, und seine Wünsche; nicht durch die Willkürlichkeit politischer
Institutionen, sondern durch die ungeheuere Größe des zu lösenden
Problems selbst wird das Proletariat besser verstehen lernen, zu welchen
Lebensbedingungen es gelangen kann und welchem Gesetze der Ent-
wicklung und der allmählichen Eroberungen es unterworfen ist; eine
unmittelbarere und sichere Betrachtungsweise wird seinen Vormarsch
lenken, der gleichzeitig an Schnelligkeit gewinnen und an Abenteuer-
lichkeit verlieren wird.

Dem obersten Schiedsspruch der Demokratie kann sich das Prole-
tariat nicht entziehen; denn die Demokratie ist der Mittelpunkt, um
den sich die Klassen drehen, und wenn das Proletariat sich ihr entziehen
wollte, dann würde es im Leeren kämpfen oder sich ins Abstrakte ver-
lieren. Aber nichts läßt dies befürchten. Es kann diesen Schiedsspruch

73