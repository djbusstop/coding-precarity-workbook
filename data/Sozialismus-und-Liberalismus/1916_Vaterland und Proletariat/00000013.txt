﻿Es sinkt wohl die Hoffnung, einmal etwas Sicheres über den Mord,
seine Ursache und seine Anstifter zu erfahren. Aber das ist zweifellos,
daß der Mord Jaurös der französischen und russischen Regierung sehr
gelegen kam.

Daß der Krieg, den Jaures ja wohl hätte doch nicht verhindern können,
ihn in die tiefste Not gestürzt hätte, wird jeder, der sein Wirken und
seine Persönlichkeit gekannt hat, bestätigen müssen. Er war ein höchster
Typus des modernen Kulturmenschen, dem die Verbrüderung der Na-
tionen nicht ein Paradewort, sondern innerstes Herzensbedürfnis war.
Aber wer wollte zweifeln daran, daß er nach Ausbruch des Krieges sich
mit Eifer und Leidenschaft zu seinem Volke gestellt hätte. Er hätte ge-
handelt nach dem Worte unseres Schillers: „Was auch draus werde,
steh zu deinem Volke." In einer Schicksalsstunde der Nation gibt es kein
Klügeln und keine Rechthaberei. Er hätte seine Pflicht getan mit dem
Tod im Herzen!

Das Buch „Die neue Armee" konnte sowohl wegen seines Umfanges
als auch wegen seines militärisch-fachmännischen Charakters keine
große Verbreitung finden. Vier Fünftel des Buches beschäftigen sich
mit militärischen Fachfragen. Aber das 10. Kapitel mit der Überschrift
„Soziale und moralische Triebkräfte. Armee, Vaterland und Prole-

tariat" führt Gedankenreihen aus, die der Verfasser wohl auch mit der
Heeresverfassung in Zusammenhang bringt, die aber sehr wohl auch
selbständig bestehen. Sie betreffen den Kapitalismus, seine Entwicklung
und weltgeschichtliche Rolle, die Erörterung des Vaterlandsgedankens
und der Nationalidee. Die sozialistische Partei Deutschlands hat sich
mit diesem Buche und insbesondere mit dessen 10. Kapitel zu wenig
befaßt. Soweit es besprochen wurde, wurden seine militärisch-demokra-
tischen Anschauungen erörtert, aber auf die tagfälligen Fragen des
Vaterlandes und der Nation wurde nicht eingegangen. Und doch sind
gerade diese Fragen zumal heute von der größten Wichtigkeit. Ich hatte
bald nach Erscheinen des Buches dem Herrn Verleger den Gedanken
nahegelegt, das 10. Kapitel als Sonderabdruck herauszugeben. Die
gegenwärtige Lage der deutschen Sozialdemokratie erheischt es gerade-
zu als eine Tat, daß Jaures Ausführungen weiteren Kreisen zugänglich

8