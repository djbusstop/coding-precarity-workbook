﻿Es kann wohl kaum einem Zweifel unterliegen, daß Jean Jaures neben
Karl Marx, Friedrich Engels und Ferdinand Lasalle die blendendste
Erscheinung des modernen Sozialismus ist. Wie sie war er ausgerüstet
mit der ganzen Wissenschaft seiner Zeit, voll der eindringlichsten Ge-
lehrsamkeit auf den Gebieten der Philosophie, Geschichte, National-
ökonomie, Soziologie und Politik und dabei ein leidenschaftlicher Freund
der Künste, insbesondere der Poesie. Mit diesen reichen Fähigkeiten
verband er eine seltene Gabe der schriftlichen und mündlichen Darstel-
lung. Ich habe in meinem langen Leben viele und ausgezeichnete
Redner gehört. Jaurös war unter ihnen der größte. Die Gabe der Be-
redsamkeit ist den romanischen Völkern angeboren. Aber oft artet sie
bei ihnen in ein virtuoses Spielen mit der Form aus, hinter der wenig
oder gar kein Inhalt steht. Bei Jaures war die Vereinigung von Form
und Inhalt. Die schöne Phrase um der Schönheit der Phrase willen
war ihm fremd. Ihn beherrschte der Gedanke, der fteilich bei ihm aus
einem sprühenden Feuerwerk von Antithesen und Bildern heraus-
sprang. Dabei war seine Beredsamkeit viel weniger Kunst als Natur.
Er enthusiasmierte mit seiner Rede, weil ihre Kunst selber aus einer
enthusiastischen Intuition sproß. Er riß mit, weil er selber mitgerissen
war. Der Jubel der Zustimmung kam aus dem Jubel seiner Gedanken
und Worte. Sein rednerisches Wesen hatte etwas Dichterisches und
Prophetisches. In ihm war nichts Fanatisches. Dazu war seine Seele
zu groß und zu weit. Er dachte mit einem starken Gehirn und aus
einem vollen Herzen. Er gehörte nicht zu den Grüblern und Analy-
tikern. Der Gedanke sprang bei ihm gewappnet aus dem Kopfe, wie
Pallas Athene aus Zeus' Stirne.

Was Jaurös ms Franzosen besonders auszeichnete, und was bei die-
sen und bei fast allen nichtdeutschen Nationen so selten gefunden wird,
das ist sein echter Internationalismus, seine Achtung und Hoch-
schätzung aller, insbesondere auch deutscher Kultur. Er machte sich die

1 Jauris, Vaterland und Proletariat

1