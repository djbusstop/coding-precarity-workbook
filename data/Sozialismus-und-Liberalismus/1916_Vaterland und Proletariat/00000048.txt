﻿zialen Formen hätten sich auf dieselbe Art rechtfertigen können. Auf
seine Legionen von Sklaven und seine endlosen Ländereien, in denen
der Flug der Geier müde wurde, hinweisend, hätte auch der durch
Plünderungen von Besiegten reich gewordene, vornehme Römer mit
Stolz sagen können: Als Lohn für viele Mühen, mit der Spitze meines
Schwertes, habe ich all dies erobert — und wären nicht heute noch die
Sorge und die Verpflichtungen meinesEhrgeizes eine zu schwereBürde
für Schultern, die weniger stark als die meinen sind? Auch der Feu-
' dalherr hatte lange Zeit seine Gefahren und seine Arbeiten. Aber mit
welcher Arbeit sind heute die Könige und Kaiser überhäuft! Das Recht
der Demokratien wird dadurch nicht vermindert. Ihr Sieg kann dadurch
etwas verlangsamt werden, und ein größerer Aufwand an Tüchtigkeit
und Klugheit wird ihr notwendig sein, um gegen eine Macht Recht zu
behalten, die nicht aus Nachlässigkeit ihre eigenen Gefahren verschärft
und die sich nicht durch unedlen Müßiggang oder aufreizende Laster
eine Blöße gibt. Nicht Arbeitsscheu werfen die Demokraten der Mon-
archie vor: sie werfen ihr ganz im Gegenteil ein Übermaß an Arbeit
vor, daß sie allzuviele Lasten und allzuviel Macht auf sich nimmt, daß
sie sowohl ihre eigene Tätigkeit wie ihre eigene Verantwortlichkeit der-
art überspannt, daß die politische Initiative und bürgerliche Betätigung
der Gesamtheit der Nation dadurch eingeschränkt und geradezu gelähmt
wird. Es wäre wohl leicht, mich auf den überanstrengten, neurasthe-
nischen und von fieberhafter Arbeit erschöpften amerikanischen Milli-
ardär zu verweisen, der von seiner andauernden Galeerensträflings-
arbeit kaum so viel Zeit und Kraft erübrigt, um an den Freuden des
Lebens zu nippen. Was hat das zu sagen? Auch dies ist regelwidrig und
beweist die tiefgreifende Verwirrung eines sozialen Systems, welches,
für Arbeit und Genuß, einzelne an die Stelle der Genossenschaften tre-
ten läßt; späterhin wird hier die Kraft der sozialen Gemeinschaft ein-
greifen. Napoleon war ohne Zweifel einer der außerordentlichsten
Arbeiter seiner Zeit. Er fiel dennoch, trotz seiner Arbeit und seinem
Genie. Alle Cäsaren des Kapitals werden wie er dahingehen, aller-
dings weniger schnell als er, denn ihre Macht rpht auf dauerhafteren
Gesetzen, auf dem ganzen widerstandsfähigen Unterbau eines sozialen
Systems, und ihr Geschick ist, stärker als dasjenige Napoleons, mit dem

43