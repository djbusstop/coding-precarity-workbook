﻿mmmm

Empörung, durch seine Forderungen nach Gleichheit, deren Form zwar
oft noch verwirrt war, deren Pathos sich aber weit über alle bürger-
lichen Einrichtungen erhob, durch alle Beschwerden über die Lebens-
haltung, durch alle Theorien, welche die Elendskrisen heraufbrachten,
durch die ersten Phantastereien der sozialen Sekten, die sich gleich dem
Schaum, der sich auf den Wogen bildet, der Bewegung zugesellten,
durch die Lehren vom allgemeinen Wohl, die sich in den Geistern reg-
ten, ehe sie in einem System feste Form gewannen, durch eine bedroh-
liche Agitation, die ungestüm wie die Menge und doch durch die Idee
der absoluten Demokratie mit unwiderstehlicher Logik geordnet war.
Das waren die Lehrjahre der Ideen und die Prüfungen der Taktik,
welche das revolutionäre Bürgertum durchzumachen hatte, gleichsam,
als müßte es sich vom ersten Tage an auf die kampfteiche Folgezeit, auf
die stürmische Größe seines Schicksals vorbereiten. Unter dem Eindrücke
der schreckensvollen und großen Lehre der Tatsachen entwickelte sich jene
denkwürdige Debatte über die Konstitution, in der das Bürgertum der
verschiedenen Schattierungen gewissermaßen seinen ideellen und prak-
tischen Feldzugsplan entwarf; die Bergpartei mit Robespierre, die
Girondisten mit Vergniaud suchten nach einem System der Versöhnung
von Eigentum und Proletariat durch eine fortschrittliche und gerechte
Demokratie.

Allerdings, die beiden Redner schienen einander zu widerstreben:
Robespierre war nicht darauf bedacht, die Armen durch eine Beschrän-
kung der Rechte des Eigentums zu schützen; dieses sollte den sozialen
Interessen unterstellt und zu Opfern herangezogen werden, die es zwar
nicht in der Wurzel treffen, aber dennoch zwingen sollten, sich auf viele
zu verteilen und dadurch gegen jene, von Jean Jacques so nachdrücklich
angekündigteTendenzderGüterkumulierung anzukämpfen:Vergniauds
Sorge hingegen war es, den Vermögen die volle Bewegungsfteiheit
zu wahren, die großen Unternehmungen aller Art gleichsam die Kraft
ihrer Flügel im Winde voll entfalten zu lassen; aber er war überzeugt,
daß Überfluß mit Verallgemeinerung des Besitzes gleichbedeutend sei,
daß in einer tatkräftigen Demokratie, die von gesetzlichen Privilegien
und juristischen Monopolen befreit wäre, Reichtum nicht entstehen kann,
ohne auf die Gesamtheit überzuspringen, daß er nicht anwachsen könne.

54