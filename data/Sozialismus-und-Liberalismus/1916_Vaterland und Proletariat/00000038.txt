﻿ist er von keinerlei habsüchtigen Absicht beherrscht. Er sucht kein Privileg
zu verteidigen, denn auch er hat die Sehnsucht, sich seines Vermögens
zu entledigen, wie man sich einer Bürde entledigt. Er trachtet auch nicht
danach, die Geldmacht, die er erworben hat, in seinem Geschlechte zu
verewigen und eine Erbaristokratie der Milliarde zu begründen; denn
er ist dem Erbrecht wenig günstig gesinnt. Geld hat nur, wenn es er-
worben ist Wert, weil es dann eine Triebfeder der persönlichen Tätig-
keit ist; übertragenes Geld bedeutet eine Schwächung, weil es persön-
liche Anstrengung erspart. Der Mann, der diese Ideen hegt und in ihnen
stirbt, steht nicht im Verdacht, aus Interesse oder Eigennutz kapital-
freundlich zu sein. Dennoch zweifelt er nicht einen Augenblick an der
Gesetzlichkeit dieser furchtbaren Vermögensanhäufung, deren Gewicht
er selbst nicht mehr zu tragen vermag, deren tote Macht er den Seinigen
nicht übertragen will. Ohne Bedenken, ohne Schwanken verherrlicht er
die amerikanische Demokratie. Es genügt ihm, daß sie gesetzlich, juristisch
kein Kasten- oder Klassenprivileg anerkennt, und daß von Rechts wegen
jeder in die Höhe kommen kann, wie er selbst in die Höhe gekommen ist.
Das Leben ist für ihn nichts als Handlung, Anstrengung, Kampf. Und
handeln heißt in der modernen Demokratie Kapital erwerben. Alle ha-
ben Zutritt zur Karriere, und diese muß schrankenlos sein. Das Wachs-
tum der Vermögen hindern, hieße die Unendlichkeit der Perspektiven
der sozialen Welt begrenzen; und wenn man jene, die sich am höchsten
erheben, erniedrigen wollte, würde man die Schwungkraft aller, die
emporsteigen,schwächen. Der Milliardär ist nichtlosgelöstvon derMasse;
daß er höher hinaufgelangt ist, als die andern, kommt von der kraft-
volleren Anstrengung jenes Mutes, der sie alle hinauftreibt. Der steilste
Gipfel ragt bis zur Sonne; aber indem er emporsteigt, zeigt er bloß die
innere Arbeit der Erde, den allgemeinen Auftrieb der sozialen Kräfte
an. Das einzige Korrektiv gegen die großen Vermögen besteht darin,
daß sie gewissermaßen durch ihr eigenes Gewicht sinken müssen; daß
sie infolge ihrer Riesengröße, die zur Fähigkeit des einzelnen, sie zu ver-
walten, zu genießen und sich ihrer zu erfteuen, außer allem Verhältnis
steht, den einzelnen geneigt machen, sobald er seinen Sieg hinter sich
hat, die Millionen zur Gesamtheit zurückkehren zu lassen und zu ver-
künden, daß Geld nur als Lohn der persönlichen Leistung Wert habe.

8 JaurLs, Vaterland und Proletariat

33