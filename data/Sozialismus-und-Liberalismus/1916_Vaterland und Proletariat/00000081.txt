﻿das die Maßlosigkeit ihres Reichtums zwischen ihnen und den übrigen
Menschen schafft. Es ist unmöglich, daß der Widersinn jener maßlosen
Vermögen nicht manchmal ihre Vernunft in Erstaunen setzt; denn allein
schon das Nachdenken über die Verwendung eines solchen Vermögens
muß einen Geist ermüden oder das Gewissen verwirren. In solchen
Stunden sind jene Kapitalisten weniger fest gegenüber manchen For-
derungen der leidenden Massen, die nach elementarer Menschlichkeit
verlangen, die zumindest einige Garantien fordern gegen die Not ihrer
alten Tage, gegen die Geißel der Arbeitslosigkeit und gegen die bar-
barischsten Ausbeutungsmethoden, die sogar Frau und Kind treffen.
Und Schritt für Schritt setzt das Proletariat, über diese halbe Willens-
losigkeit hinweg, einige Gesetze durch, die es beschützen. Aber vor allem
erschrickt der Kapitalismus zweifellos manchmal selbst, wenn er inne
wird, daß er durch seine Entwicklung alle Prophezeiungen, alle Be-
hauptungen des revolutionären Sozialismus wahr macht. Proudhon
zeigte in seinen wirtschaftlichen Widersprüchen, wie die Konkurrenz not-
wendigerweise schließlich zu ihrem Gegenteil gelangt, nämlich zum Mo-
nopol. Considörant, Pecqueur, Vidal, Marx haben die unwiderstehliche
Tendenz des Kapitals nach Konzentration erkannt. Und heute ist das
Kapital tatsächlich fast überall auf dem Wege zum Monopol. Heute
vollzieht sich unermüdlich die Konzentration der Unternehmungen in
den wichtigsten Zweigen der modemen Produktion, trotz allen Bemü-
hungen der neuen, kleinen Industriellen, denen das Großkapital den
Weg verlegt. Und wenn es dem Kapitalismus gelingt, wenigstens für
kurze Zeit die Erfüllung der sozialistischen Vorhersagungen zu durch-
kreuzen, so doch nur, indem er sich selbst Lügen straft und eines seiner
eigenen Gesetze verleugnet. Marx hatte gesagt, daß der Kapitalismus
von der unendlichen Flut der Produktion überschwemmt werden und
die Kontrolle über sie verlieren würde, daß er sie nicht genügend kal-
kulieren und regeln können würde, um den Krisen zu entgehen, und
daß die periodischen Krisen den Kapitalismus erschöpfen und ihn zum
Eingeständnis seiner Ohnmacht und zur Kapitulation zwingen würden.
Aber auf welche Art versucht heute das Kapital die immer wieder-
kehrenden Krisen der Überproduktion und der Marktstockungen zu ver-
hindern? Wie versucht er die Produktion zu regeln und zu organisieren?

76