﻿Lebenszentrum zu greifen scheinen; und sogar wenn ihm diese Zuge-
ständnisse von außen aufgezwungen wurden, gefällt es sich darin, sich
einzubilden, daß es sie sozusagen nur aus seinen eigenen Reserven ge-
macht und daß gerade diese Zugeständnisse, fern davon die Aussichten
der Zukunft zu verringern, ihre unerschöpfliche Lebenskraft und ihre
fast schrankenlose Anpassungsfähigkeit erweisen.

Diese Anpassungsfähigkeit, diese Elastizität erlauben und werden auch
fernerhin der bürgerlichen Welt erlauben, ohne unheilbaren Riß, ohne
tödliche Verschiebungen eine ausgedehnte Entwicklung durchzumachen.
Gleichzeitig mit der wachsenden Schulung des Proletariats, das im
Grunde, je revolutionärer es wird, immer verständnisvoller wird, wirkt
jene Anpassungsfähigkeit des Bürgertums aus den Klassenkampf, ohne
die wesentliche und organische Einheit der menschlichen Gesellschaft zu
sprengen, wie ein wunderbarer Ansporn für die Willenskräfte und wie
ein entscheidender Faktor der Umwälzung. Diese Anpassungsfähigkeit,
diese Kraft der Selbsterhaltung kommt in der ganzen Geschichte des
Bürgertums immer wieder zur Geltung. Seit sieben oder acht Jahr-
hunderten hat es sich in allen Ländern und sogar in jedem einzelnen
Lande unter den verschiedenartigsten politischen und wirtschaftlichen
Bedingungen entwickelt. Welch ein Umschwung seit den Tagen, da die
Handwerker sich von der feudalen Vormundschaft und der Abhängigkeit
von den Ritterburgen zu befreien begannen, bis zu dem heutigen Tage,
da die großen kapitalistischen Trusts umfangreiche Demokratien, deren
Parlament, Presse, Verwaltungen und Gemeindevertretungen beherr-
schen ! Welch eine ununterbrochene Kraftanspannung, aber welch eine
verschwenderische Fülle von taktischen Formen verschiedenster Art!
Welch ein Umschwung der Ideen! Die Bourgeoisie besaß jenes Fol-
gerungsvermögen, das eine Stärke ist. Sie besaß keinen Geist der Tra-
dition, der meist eine Schwäche ist. Sie mühte sich stets um ihr Wachs-
tum, brachte sich schrittweise vorwärts, vereinigte zuerst, wie Auguste
Comte in seinen wunderbaren ersten Schriften bewies, die industrielle
und intellektuelle Macht, bevor sie bald unter einem gemischten Re-
gierungssystem, bald unter einer reinen Demokratie ihren Eroberungs-
zug unternahm. Aber jede Generation befreite sich, wenn es not tat,
ohne weiteres von der Theorie und Praxis früheren Generationen, ob-

48