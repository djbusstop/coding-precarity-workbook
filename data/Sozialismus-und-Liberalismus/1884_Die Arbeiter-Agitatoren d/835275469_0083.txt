Im Wirlhshause. 
77 
Kunst sich rasch entfalten würde, wenn die Stellen an den Gallerten 
nur erst einmal alle durch Kunstdeklamatoren besetzt sein würden, zu 
welchen wir dann staunend hinaufsehen würden!*) 
Ul—Die übermäßige Erziehung „verkehrter Gelehrter" wird mehr und mehr 
fühlbar, dem Staate höchst schädlich und verderblich werden. 
£— Bon wo hört man keine Klagen! Aber die über Nahrungssorgen sind 
doch die berechtigtsten und vor allen zu beherzigen. 
B— Nach Tausenden zählt man landwirthschaftliche Arbeiter-Familien, die 
Monate hindurch kein Fleisch zu essen haben. Ist doch der ganze 
Bauernstand übel genug daran und verhältnißmäßig zwei- und drei 
fach höher besteuert als gewisse andere Stände. 
Sremder: Bon der gesannnten Bevölkerung Schottlands sagte Bright,**) 
daß 30 o/« der Familien in nur einem Zimmer wohnen — Noth und Elend 
sei überall. — Es möge sein, und er zweifle nicht, daß es Mißstände 
gebe, welche des Menschen Geist und Kraft nicht verhindern können, 
aber es gäbe auch solche, die von der Unwissenheit der Bölker oder 
von der Thorheit und Schuld ihrer Lenker herrühren. 
M— Bei diesen letzten Worten denkt man unwillkürlich an die vielen 
deutschen Jünglinge, welche ftudirt haben, Jahre lang auf Anstellung 
warten müssen. Auch sie sind zu bedauern und in den meisten Fällen 
die Eltern derselben nicht minder. 
£— Die Folge ist, daß manche keine Mittel scheuen, um eine Anstellung 
zu erlangen. 
R— Schlimm genug, daß es so weit gekommen ist. 
E— Welch' mißliche Erfahrungen haben in dieser Beziehung wir Sonnc- 
berger gemacht! Schmach den Anstiftern! 
&— Hier waren Sozialdemokraten die Führer, Berather und Informatoren 
der jugendlichen Arbeiter-Agitatoren der Sozialaristokratie. Diese ist 
schlimmer als jene. 
Ģ ~ ijinjd machen sä gohr die ThüringerwaldMereinswag unsicher! 
*) Siehe: Carl Hoff, „Künstler und Kunstschreiber." München: Theodor Ströser. 
**) In seiner Antrittsrede als Rektor MagnifituS der Universität in Glasgow.