64 
Das Verhör. 
(Der Polizeisekretär liest ihn vor:) 
„Deutsches Volk — . . Erst nach Entfernung und Ausstoßung aller 
Phrasenhelden aus dem Kreise, der auf Grund ernstester Studien die Fragen 
von Deinem wirthschaftlichen Wohl und Wehe diskutiren und lösen sott, 
wird die Pflege einer echten, praktischen Vvlkswirthschaft möglich sein! 
Deutsches Volk: erst wenn Du jene, Dein Elend als „natürlich" und „noth 
wendig" preisenden „Volksanwälte", die sich noch zwischen Dich und Deine 
Regierung drängen und sie am liebsten selbst sein möchten, mit einer gewal 
tigen, aber lohnenden Kraftanstrengung zur Seite geschoben hast, wirst Du 
Deinem so lange und mit Recht ersehntem Kaiser nahe genug stehen, um 
ihm selbst sagen zu können, was Dir fehlt! Erst dann kann Dich vielleicht 
ein Höh enz oller, wie er Dich aus dem Wirrsaal politischer Zerfahrenheit 
zur Einigung und Macht geführt hat, berathen von wahren Volksfreun 
den und Volkswirthen, auch aus dem wirthschaftlichen Elend mit kräf 
tiger Hand sicher hinüberleiten zu wirtschaftlich gerechter Eini 
gung und Versöhnung Deiner sich jetzt selbst zerfleischenden Gesell 
schaftsklassen, und Dir und Sich Selbst die Austragung des furcht 
baren Klassenzwistes in einer sonst unvermeidlichen Katastrophe ersparen, 
die Dir und Ihm neben dem erreichten Guten unsägliches Leid und Elend 
bringen müßte!" — 
Sie sind angeklagt, Herr Pseudonymus, also unter falschem Namen, 
ohne' Legitimation, verkappt, 
die Stadt Sonneberg verunglimpft, verlästert, 
deren Indù strie stand herabgewürdigt und 
dessen Han dels st and geschmäht, beschimpft, verleumdet und 
des Falschen beschuldigt zu haben, 
zugleich damit die sozialistisch-staatsgefährlichen Mittel angewendet zu haben, 
welche geeignet sind, das Volk zu verwirren und den Haß der 
niederen Arbeiterklassen gegen den Handelsstand zu erzeugen 
und zu schüren. 
Sie si nd deshalb verurtheilt: drei Monate Gefängniß bei 
Wasser und Brod in unserer Zelle für Wissenschafts-Proletariat zu verbringen. 
Für die maßlose Steigerung der persönlichen Ausfälle und Angriffe in 
ihrem wissenschaftlichen Werk, mit dessen Tendenz der burschikose Ton zusammen 
hängt, der als obligate Begleitung dasselbe durchzieht, diktire ich Ihnen ferner 
zu: Schopenhauer's Kapitel: „Ueber Universitäts-Philo 
sophie" auswendig zu lernen und dasselbe in der Aula der Universität und 
des staatswissenschaftlichen Seminars, wo Sie studirt haben, Ihren Herren 
Professoren der „Philosophie in der National-Oekonomie" mit dem gebühren 
den Pathos, ohne Anstoß und ohne Falsch vorzutragen, namentlich die Stellen 
auf Seite 35-40 dieses Buches und folgende dazu: