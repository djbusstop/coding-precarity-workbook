/ 
66 
Das Verhör. 
korrupter Ansichten und lassen, wo man Gedanken erwartet, hohl- Phrasen, 
nichtssagendes Wischiwaschi, ekelhaften Hegeljargon vernehmen? Ist ihnen 
nicht die ganze Lebensansicht verrückt und die platteste, philisterhafteste. ja 
niedrigste Gesinnung an die Stelle hoher und edler Gedanken getreten? 
.... Erst wenn der Intellekt frei geworden ist, in welchem Zustande 
er gar kein anderes Interesse auch nur kennt und versteht, als das der 
Wahrheit, ist die Folge, daß man alsdann gegen allen Lug und Trug, 
welches Kleid er auch trage, einen unversöhnlichen Haß faßt. 
Damit wird man freilich es in der Welt nicht weit 
bringen, wohl aber in der Philosophie!"