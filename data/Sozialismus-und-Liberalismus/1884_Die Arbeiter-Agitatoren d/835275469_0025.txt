19 
uns von ber Wiege bis zum Grabe; ob sie uns auch in's Jenseits be 
gleiten wird, wissen wir noch nicht genau. . . . Fürwahr, die moderne 
Zahlenstatistik wurzelt in einer bewunderungswürdigen Methode: „aus der 
Berechnung der quantitativen Berhältnisse folgert sie die Qualität! In 
diesem Schlüsse aus der äußern Zahl auf das innere Wesen der Dinge, ruht 
ihre Kunst, ihr Zauber, ihr Meistergeheimniß." *) 
Erst nach dem Fall des berüchtigten Sonneberger Handels-Privilegs 
und seit der freien Bewegung der Individuen in Handel und Industrie 
durch den Einzug der Gewerbefreiheit, stieg die Zahl der Ex 
porthandelshauser innerhalb 20 Jahren derart, daß sie jetzt vierzig 
beträgt, mit denen im nahen N e u st a d t und in der Unigegend aber s i e b e n z i g. 
Und belief sich gegen Mitte dieses Jahrhunderts der jährliche Umsatz jener 
19 Handelshäuser nur auf 2 Millionen Mark, so ist er seitdem auf 12 
Millionen gestiegen.**) 
So die Verhältnisse in Sonneberg zu Zeiten der Bevormundung des 
Handels und zu Zeiten der Handelsfreiheit, von denen die Geschichtswerke 
der Sozialisten Ur. Emanuel Sax und Konsorten selbstverständlich und 
wohlweislich schweigell, weil sie in ihren Kram nicht passen. 
So räumlich beschränkt die Sonneberger Holz- und Papiermasse- 
Spielwaaren-Fabrikation an und für sich ist, so wenig bekannt, erkannt und 
gewürdigt ist sie im Deutschen Reich. Im Ausland aber, in allen Ländern 
der Erde, wo ihre Erzeugnisse vom Handel gesucht und verbreitet sind, 
schätzt man sie als einzig ihrer Art und preist ganz besonders den volks- 
wirthschaftlichen Werth dieser von Tausenden von Arbeitern selbstständig 
betriebenen plastischen Hausindustrie, — denn k e i n e z w e i t e I n d u st r i e 
der Welt hat ein so unabsehbares, ertragfähiges, ausschließlich manuell 
*) Siehe: W. H. Riehl: Die statistische Krankheit. Allgemeine Zeitung. (Nr. 322 
bis 325.) München, November 1882. 
**) Tie Sonneberger Industrie, mit der im Nachbarstädtchen Neustadt und Um 
gegend kaum 4 Quadrat-Meilen bergiges, bewaldetes Land umfassend, liefert jährlich für 
ca. 12 Millionen Mark Spielwaaren und Luxusartikel, (vornehmlich aus Papiermasse 
und Holz: Puppen, Figuren, Thiere, A ttr a p en u. s. w.), ausschließlich der von 
Porzellan und Glas. Mit dem lokalen Großhandel und Berkehr ernährt sie gegen 20 Tau- 
feiib Menschen und beschäftigt außerdem eine große Anzahl auswärtiger Fabriken zur 
Beschaffung der nöthigen Rohmaterialien und Halbfabrikate, welche gegen 4 Millionen Mark 
betragen. (Siehe: A. Fleischmann : Die Sonneberger Sp iel w a a r e n-H a usi n- 
d«strie und ihr Handel rc. Berlin, L. Simion, Seite 41.) 
2*