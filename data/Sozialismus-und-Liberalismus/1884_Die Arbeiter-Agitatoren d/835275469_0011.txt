5 
Obgleich die Sonneberger Haus- und Fabrikindustrie und der Handel da 
selbst ausführlich und getreulich bereits geschildert ist,*) so geben wir in gedrängter 
Kürze (in Kapitel II) doch noch ein Bild davon, nicht um den Versuch zu machen, 
kathedersozialistische Schriftgelehrte zu belehren, denen ihre Wissenschaft nicht 
einträglich und ergiebig genug zu sein scheint, und die unter dem schäbigen 
Mantel derselben ihre Mißgunst und ihren Neid und Haß gegen das Ver 
mögen und den Besitz kaum mehr zu verbergen suchen, sondern um jedem 
wahren Deutschen, so es angeht, — Einsicht zu vergönnen in die Proze 
dur und Taktik der als Volksbeglücker sich auswerfenden Sozialisten und 
ihrer Agitatoren und zu zeigen, wozu letztere sich gebrauchen lassen, wem 
sie dienen und welchen Werthes ihre wissenschaftlichen Berichte und 
Schilderungen sind. 
Wie es zu den höchsten Idealen der abstrakten Wissenschaft gehört, 
daß die neuen Gedanken, zu denen man auf Grund theoretischer Kombina 
tionen und Schlußfolgerungen gelangt ist, die scharfe Kontrole der Praxis 
bestehen, indem diese sich ihrer bemächtigt und durch die Verwerthung für 
ihre Zwecke die wissenschaftlichen Wahrheiten constatirt, so ist auch umge 
kehrt das Ideal der Praxis, daß die Staats- und Volkswirthschafts-Wisscn- 
schaft ans das freie Erwerbsfeld des selbstständigen Geschäfts- und Ver- 
kehrslebens sich begiebt, um Erfahrungen zu sammeln nnd die darauf 
historisch gewordenen Verhältnisse zu berücksichtigen, als Grundbedingung 
der Prosperität der auf freie Initiative der Selbsthülfe angewiesenen 
Klassen. 
Aber die deutsche Staats- und Volkswirthschafts-Wissenschaft war von 
jeher zu stolz und zu bequem , von der Praxis Lehre anzunehmen, 
wie dies in all' den Staaten geschieht, welche wirthschaftlich uns voraus 
sind. In Deutschland herrscht eben das Prinzip der Trennung 
*) A. Fleischmann: Die Sonneberger Spielwaaren-H ausindustrie 
und ihr Handel. Zur Abwehr gegen die fahrenden Schüler des Katheder-Sozialismus 
in der National-Oekonomie. Berlin, L. Simion 1883. 
*) A. Fleischmann: Gewerbe, Industrie und Handel des Meininger Ober 
landes, in ihrer historischen Entwickelung. Hildburghausen, Kesselring'sche Hofbuchhand 
lung. 1878.