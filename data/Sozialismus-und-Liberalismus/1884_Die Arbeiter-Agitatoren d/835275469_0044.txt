38 
Gevatter stehn, gewiß nicht fehlen wird. Danach werden dann die 
Herren, weil sie'ja von der Philosophie leben, so dreist, daß sie sich 
Philosophen nennen, und dem,rach auch vermeinen, ihnen gebühre das 
große Wort und die Entscheidung in Sachen der Philosophie, ja daß 
sie am Ende gar noch Philosophenversammlungen (eine contradicho 
in adjecto, da'Philosophen selten im Dual und fast nie im Plural 
zugleich aus der Welt sind) ansagen und dann schaarenweise zusammen 
laufen, das Wohl der Philosophie zu berathen!" 
In der That, man staunt, wie trefflich das Spiegelbild, welches der 
Philosoph Schopenhauer vor langer als einem Menschenalter seiner 
Generation zum Beschauen vorgehalten hat, aus die Jetztzeit paßt! - Nur 
daß sein Spiegel das Schreckensbild von heute bei weitem nicht zu fassen 
vermöchte, so beträchtlich hat das Heer der Philosophaster und Sophisten 
aus dem Gebiet der National-Oekonomie sich vermehrt, die, nach Grund 
sätzen staatswissenschaftlicher Katheder-Weisheit dressirt, als Dozenten 
und Schriftgelehrte dem Staatssozialismus und der Sozialpolitik der 
Regierungen dienen. 
Mit allen Stempeln der Maturität auf der fahlen Stirn, ja mit 
dem Doktorhut, deutscher Fa^on, Abtheilung der Volkswirthschaft, auf dem 
Kopf, ziehen die gefügigen Universitäts-Zöglinge aus, auf vorge 
schriebenen Streifzügen Dissertationen auf Bestellung zu liefern und je 
nach dem Werth derselben einer festen Anstellung früher oder später 
gewärtig zu sein. 
Sie richten ihre Schritte vorzugsweise nach hausindustrielleu Gegenden, 
wo Industrie uud Handel gegenseitig unabhängig sind, wo jeder Arbeiter, 
Fabrikant oder Kaufmann frei und selbstständig ist, — wo aber - ob solcher 
Freiheit und Unabhängigkeit, auch der Liberalismus und die Freisinnig- 
keit in der Politik vorherrschen. Hier fungiren jene „Streber und Geschäfts 
männer von der lukrativen Philosophie" als Henkersknechte des politischen, 
reaktionären Vehmgerichtes. Sie schreien den liberalen Handelsstand als 
verantwortlich aus für das in der selbstständigen Hausindustiie e,wachsende 
Proletariat, ergreifen und schleppen ihn in die Arena, um daselbst — 
früher oder später — von den Hungernden zerfleischt und aufgezehrt zu 
werden, selbstverständlich - „gelegentlich," wenn die Zeit des Sozialismus 
hereingebrochen und der Liberalismus im Volke völlig ausgerottet sein wird. 
Das sind die Leute, die Wahrheit aus dem Brunnen zu ziehen, dem 
Volke vorzuleuchten, aller Verfinsterung Hohn zu sprechen, das sind die Leute, 
die den Stein der Weisen zu finden glauben. „Wenn sie ihn hätten, der Weise