Das Verhör. 
57 
„Dilettanten, Dilettanten! — so werden die, welche eine Wissenschaft 
oder Kunst, aus Liebe zu ihr und Freude an ihr, per il loro diletto, 
treiben, mit Geringschätzung genannt von denen, die sich des Gewinnes 
halber darauf gelegt haben, weil sie nur das Geld delektirt, das damit 
zu verdienen ist. Diese Geringschätzung beruht auf ihrer niederträch 
tigen Ueberzeugung, daß Keiner eine Sache ernstlich angreifen werde, 
wenn ihn nicht Noth, Hunger oder sonst welche Gier dazu anspornt. 
Das Publikum ist desselben Geistes und daher derselben Meinung: 
Hieraus entspringt sein durchgängiger Respekt vor den „Leuten vom 
Fach" und sein Mißtrauen gegen Dilettanten. In Wahrheit hingegen 
ist dem Dilettanten die Sache Zweck, dem Manne vom Fach, als 
solchem, bloß Mittel: nur der aber wird eine Sache mit ganzem 
Ernste betreiben, dem unmittelbar an ihr gelegen ist und der sich aus 
Liebe zu ihr damit beschäftigt, sie eon amore treibt. Von Solchen, 
und uidļt von Lohndienern, ist stets das Größte aus 
gegangen! .... 
.... Zu einem Selbstdenker verhält sich der gewöhnliche Bücher 
philosoph. wie zu einem Augenzeugen ein Geschichtsforscher: Jener 
redet aus eigener, unmittelbarer Auslassung der Sache. Daher stimmen 
alle Selbstdenker im Grunde doch überein und ihre Verschiedenheit ent 
springt nur aus der des Standpunktes; wo aber dieser nichts ändert, 
sagen sic alle dasselbe, bloß das, was sie objektiv aufgefaßt haben. Sie 
allein sind es, von denen die Welt Belehrung empfängt. 
Die Menschen von richtigem Urtheil und'die Leute, denen es Ernst 
mit der Sache ist, sind alle nur Ausnahmen; die Regel ist überall in 
der Welt das Geschmeiß: und dieses ist stets bei der Hand und emsig 
bemüht, das von jenen nach reiflicher Ueberlegung Gesagte auf seine 
Weise zu verschlimm - bessern." *) 
Schtußverhandkung. 
II. Scene. 
Der Syndikus. 
Gestatten wir uns, im Hanpttheil des pseudonymen Werkes all den 
kathedersozialistischen und sophistischen Wortschwall, der für ein nasegeführ 
tes Publikum geschrieben ist, zu übergehen. Denn „das Denken mit Leuten, 
die es eigentlich auf bloßen Schein, mithin auf Täuschung des Lesers ab 
gesehen haben, ist Kopf- und Zeitverderb." — Auch kennen wir schon das 
*) Siehe Schopenhauer: Parerga und Paralipomena: Kapitel „über 
Gelehrsamkeit und Gelehrte.-