Das Verhör. 
51 
sie der letztjährige Kongress des Thüringer-Wald-Vereins (Rudolstadt 1882) 
nicht ehrenvoller anerkennen konnte, als er aut Vorschlag Prof. Kirchhofs s 
in Halle es als eine seiner Aufgaben bezeichnete, zu erforschen, ob nicht des 
Wäldlers Gutmütigkeit in jenen Gegenden, in denen sich eine grosse Industrie 
angesiedelt, zu seinem Schaden benutzt worden sei! - (Welch' philosophischer 
Gedanke! Zwar nicht durch Gutmütigkeit, jedoch unbeschadet derselben, 
kann hier jeder arme Arbeiter durch eigene geistige Kraft sich auf 
schwingen. Der Kreis Sonneberg zählt solche Fälle nach Tausenden!) 
Der pseudonyme Herr Verfasser überbietet ja seinen Vorgänger im Ueber- 
treiben und Verschweigen und spricht da von einem „ganzen, elenden Stadt 
viertel", das gar nicht existirt in Sonneberg, dessen Industrie vielmehr 
fremdes Proletariat massenhaft aufsaugt und beglückt — bis aus einen ver- 
hältnißmäßig geringen, übrigens geringeren Theil, als die 
große Mehrzahl der Industriestädte überall aufzuweisen hat. 
Aber angenommen, es sei dem wirklich so, daß Jammer und Elend 
hier herrschten, was vermag die wahre Philosophie dagegen zu 
rathen? Sie schweigt — wohlweislich! — Warum aber treiben die 
staatswissenschaftlichen Hochschulen und Regierungen ihre, der Katheder- 
Philosophie-Fütterung zu entwöhnenden, als Dozenten anzustellenden Jünger 
in Gegenden auf die Weide, wo Industrie und Handel allem gesunden Augen 
schein nach in bester Blüthe stehen, trotzdem im Verhältniß zu ihrer Be 
deutung so außerordentlich wenig zu ihrer Förderung geschieht, wo durch 
die Unabhängigkeit der selbstständigen Arbeiter und Fabrikanten von den 
Arbeitgebern des Großhandelsstandes die Verhältnisse gerade für den kleinen 
Mann so günstig liegen, wie nirgends zwischen Industrie und Handel, - 
Verhältnisse selbstständigen, freien Erwerbslebens, die der verschulte Verstand 
auf Staatsfütterung angewiesener, zukünftiger Volkswirthschafts-Dozenten 
nicht erkennt und nicht zu fassen vermag! 
Warum lassen die Regierungen in ihren staatswissenschaftlichen Hoch 
schulen die Wirthschaftsschüler nicht lehren, vorerst vor ihren eigenen Schul- 
thüren zu kehren und aus den Winkeln ihrer Universitäts-Städte Armuth und 
Elend wegzuschaffen, behufs Prüfung ihrer Maturität zur Lösung des Problems, 
auf das seit Jahrtausenden die äußersten Anstrengungen seitens der seltensten, 
mit den außerordentlichsten Fähigkeiten begabten Köpfe gerichtet gewesen sind? 
Wie kommt es, daß man in Deutschland von Schülern der lautersten, edelsten 
aller Wissenschaften, der Philosophie, diese als Reisemantel benützen läßt