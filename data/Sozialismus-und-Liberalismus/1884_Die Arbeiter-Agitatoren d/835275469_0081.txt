Im Wirthshause. 
75 
Mei Mehning iß: Wär do Uern niet sälber aus d'r School',, raus 
ka gefimi, dar soll sich schäbm, daß'r niet morrà verstand aus d'r Schulst 
mit ähehm gebracht hott — beßt dö's — wenn'r niet falber ä Sozialer 
iß worn nun zum ksirbergs-Ahcmg vam sshilosopheu-^andwerk gebort! 
U%— Wer sich selbst nicht helfen kann, der werfe sich getrost den Lazareth- 
briidern vom Sozialismus in die Arme, die werden ihn pflegen! 
D— Mir soll,,' sci gestuhl'n war. I)hg soll oppor für Necksküuner àrbet um, 
die mit zu d'eruchrn helf? )hg will sä joog! 
^— Wir losten uns van der philosophischen Arahket niet ah>teck, mir leni, 
van ļfaus aus gesund. 
%— wer bei uns im Oberland' schä bei seiner Geburt niet tacktfest iß, der 
störbt schä in sein' erschien odder zweeten sMjr äwäck, sogen die 
Statisten. 
ftolporteur: Eine Bitte, geehrte Herren. Ich reise um Gaben zu sammeln 
zur Errichtung eines nationalen Denkmals für den Philosophen 
Schopenhauer. Der Aufruf geht von Koryphäen deutscher Wissen 
schaft aus. 
Äffe: Wir Alle geben mit Vergnügen dazu! — Wir Fremden auch! — 
Haben Sie die Biographie dieses arg verleumdeten Ehren-Philosophen 
zu verkaufen? 
ftokporteur: Hier. — Auch mit Schriften von anderen, wahren Gelehrten 
kann ich dienen. 
ft— Es ist ein harter Kampf, dem ein Häuflein furchtloser Gelehrter aller 
Fakultäten sich unterzogen hat, die, das Anathema der Zunft nicht fürch 
tend, ihrer Ueberzeugung Ausdruck geben, daß die ausgefahrcnen Ge 
leise der jetzigen Volksbildung nicht zur Volkswohlfahrt, vielmehr weit 
abseits von ihr führen. 
£— Warum rührt man sich überhaupt nicht und duldet, daß der ohnehin 
vernachlässigte Nährstand, zunächst der Fabrikanten- und Handelsstand 
von einem Haufen überflüssiger Volkswirthschafts-Dozenten, die ihre 
Sonderinteressen, ihr Heil in, Sozialismus suchen, fortwährend be 
helligt und verleumdet werde? 
— Hat man denn aus dem kläglichen Ausgang staatlicher Fabriks- und 
Handelsunternehmungen in Deutschland noch immer keine Lehre gezogen?