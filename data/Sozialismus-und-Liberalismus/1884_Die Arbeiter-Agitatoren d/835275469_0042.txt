36 
Geschrei zu Wege bringen, bei dem Keiner mehr sein eigenes Wort 
'hìch ansehen, als Einen, der den Geist ihres Spieles 
nicht gefaßt hat und dadurch es ihnen allen zu verderben droht; 
mithin als ihren gemeinsamen Feind und Widersacher. Wäre, was 
du bringst, das größte Meisterstück des menschlichen Geistes; vor 
ihren Augen könnte es doch nimmermehr Gnade finden. Denn es 
wäre ja nicht ad normam conventionis abgefaßt, um nun 
auch davon zu leben. Einem Philosophieprofessor fällt es gar nicht 
ein, ein auftretendes neues System darauf zu prüfen, ob es wahr sei, 
sondern er prüft es sogleich nur darauf, ob es mit den Absichten 
der Regierung und den herrschenden Ansichten der Zeit in Einklang 
zu bringen sei. Danach entscheidet er über dessen Schicksal. . . . Wenn 
es aber dennoch durchdränge, wenn es, als belehrend und Auf 
schlüsse enthaltend, die Aufmerksamkeit des Publikums erregte und 
von diesem des Studiums werth befunden würde, so müßte es ja in 
demselben Maaße die kathederfähige Philosophie um eben jene Auf 
merksamkeit. ja um ihren Kredit und, was noch schlimmer ist, um ihren 
Absatz bringen. Ui meliora! Daher darf dergleichen nicht auf 
kommen und müssen hiegegen alle für einen Mann stehen 
Auch wenn du mit reinem Herzen und ganzem Ernste der 
Forschung nach Wahrheit obgelegen hättest und deren Früchte jetzt 
anbötest, welchen Empfang hast du zu erwarten von jenen zu Staats- 
zwecken gedungenen Geschäftsmännern der Katheder, die mit Weib und 
Kind von der Philosophie zu leben haben, und deren Losnna daher 
ist- erst leben und dann Philosophiren", die demgemäß den Markt in 
Besitz genommen und schon dafür gesorgt haben, daß hier nichts 
gelte, als was sie gelten lassen, mithin Verdienste nur existiren, sofern 
es ihnen und ihrer Mittelmäßigkeit beliebt sie anzuerkennen 
Wenn, bei alledem, der Nachtheil, welchen die Unberufenen 
und Unbefähigten den Wissenschaften bringen, bloß dieser wäre, daß 
sie darin nichts leisten; wie es in den schönen Künsten - (wie in 
den Gewerben und in der Industrie auch —) hierbei sein Bewenden 
hat, so könnte man sich darüber trösten und hinwegsetzen. Allein hier 
bringen sie positiven Schaden, zunächst dadurch, daß sie, um 
das'Schlechte in Ansehen zu erhalten, Alle im natürlichen Bunde 
gegen das Gute stehen und aus allen Kräften bemüht sind, es nicht 
auskommen zu lassen. Denn darüber täusche man sich nicht, daß zu 
allen Zeiten, auf dem ganzen Erdenrnnde und in allen Verhältmsien, 
eine von der Natur selbst angezettelte Verschwörung aller mittel 
mäßigen, schlechten und dummen Köpfe gegen Geist und Verstand 
existirt. Gegen diese sind sie sämmtlich getreue und zahlreiche Bundes 
genossen. Oder ist man etwa so treuherzig, zu glauben, daß sie viel 
mehr nur ans die Ueberlegenheit warten, um solche anzuerkennen, 
zu verehren und zu verkündigen, um danach sich selbst so recht zu 
nichts herabgesetzt zu sehen? — Gehorsamer Diener! — Sondern: 
„Stümper und nichts als Stümper soll es geben auf der Welt, damit 
wir auch etwas seien!" Dies ist ihre eigentliche Losung und — die