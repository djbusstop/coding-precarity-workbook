Das Verhör. 
59 
die Menschenbeglücker selber spielen, so zwar, daß mit ihrem „Recht auf 
Rente," nach sozialaristokratischer Weltanschauung, sie selbst den Löwen 
antheil ziehen. 
Von jeder Stadt und Gegend, in welche die Agitatoren der Katheder 
sozialisten als Exekutoren einfallen, — also auch von Sonneberg — berichten 
sie über „erschreckliche Sterblichkeit" unter den niederen Ar 
bei ter klaffen. Es ist dies eines ihrer gewichtigsten Schlagworte. 
Nach den statistischen Ermittelungen betrug sie durchschnittlich der Jahre 
1838/77 im Kreise Sonneberg jährlich: 28,%,, im Herzogthum Altenburg 30,4, 
Wasungen 30,g pro Tausend der Bewohner. Bezüglich der Kindersterblich 
keit bis incl. des 5. Lebensjahres wird Sonneberg, mit 43 °/ 0 der Ge 
storbenen, übertreffen von Erfurt (44,z4), Dresden (46,zi). Hannover (48,5), 
Nürnberg (48,?), München (5O,i), Altenburg (51,4), Berlin (55,z), Stutt 
gart (56,4), Chemnitz (63,gg), Apolda (64, 94 ). 
Diese Apostel des universitätsphilosophischen Sozialismus erdreisten 
sich weiter, dem blühenden Städtchen Sonneberg „ein ganzes elendes 
Stadtviertel" anzudichten, das kein Einwohner kennt und, außer 
den beiden Agitatoren, kein Fremder findet und gewahrt. — Sollte 
das in Sonneberg als ärmster Stadttheil angesehene „Grünthal" gemeint 
sein, so dürfte den von den Sozialisten hierüber Belehrten interessant sein 
zu erfahren, daß die Besitzer — meist Arbeiter und Fabrikanten - der in 
jenem Stadttheil gelegenen 66 kleinen, zumeist einstöckigen Wohnhäuser,*) 
die Summe vou Mk. 900 — als freiwillige Beiträge zu einem Trottoir 
vor Kurzem dem Magistrat zur Verfügung gestellt haben. 
Von den 700 Wohnhäusern in Sonneberg gehören 30 den Kaufleuten 
des Exporthandels (60 an der Zahl). Alle übrigen sind Eigenthum von 
Fabrikanten, Gewerbetreibenden, Kaufleuten des Lokalhandels, Pri 
vaten, u. s. w. 
Der städtische Etat für Armenpflege in Sonneberg ist jährlich 
Mk. 45O0. — Die Statistiker mögen sich die Mühe geben, zu ermitteln, auf 
welcher Stufe der Armuth das lOTausend Einwohner zählende Städt 
chen Sonneberg unter seines gleichen steht? In der Privat-Mildthätig 
keit wird Sonneberg anderen Städtchen nicht nachstehen. 
Bekanntlich forschen solche Geschichtsschreiber, die keine eigene Beobach 
tungsgabe und kein Verständniß für Handel und Verkehr in der Gegen- 
*) Die übrigen, zu jenem Stadtviertel zählenden 35 Wohnhäuser liegen an gepflasterter 
Straße und brauchen kein Trottoir.