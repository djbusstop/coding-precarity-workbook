— 2() — 
Nr. 6 der politischen Wochenschrift, Berlin 1883, als Schutzpatron des Dr. 
Sax, seinen Kathedersozialismus kennzeichnend, bemerkt: 
„Alles, was (in Sonneberg) als ganz natürlich gilt, erscheint nns, 
den Anhängern der Sozialreform als nicht natürlich, sondern als höchst unnatür 
liche Zustände, die auch, wie dieErfah runggezeigt hat, (!) sehr wohl 
auf künstliche Weise. (!) d. h. durch staatliche Aufsicht und Regelung 
der betreffenden Verhältnisse, wenigstens theilweise, gebessert werden 
könnten." 
Das überaus rasche Aufblühen der Sonneberger Industrie und ihres 
Handels erforderte mehr Hände, als die Heimath beschaffte; deshalb zogen 
junge Kräfte aus verschiedenen Gegenden Deutschlands zu, die, dem Handels 
stand angehörend, fast alle der Fabrikation sich zuwandten. Ans weiter 
Umgegend aber strömten zugleich auch Arme in Schaaren herbei, meistens jugend 
liche Arbeiter beiderlei Geschlechts, um von der Sonneberger Industrie oder ihrem 
Handel besser ernährt zu werden, als von Gewerben und Landwirthschaft 
in ihrer Heimath, welchem natürlichen Drang und Bedürfniß in allen 
Fällen, wo Trieb, Fleiß und Sparsamkeit sich zusammenfanden, durch 
guten, ja zunl Theil durch außerordentlich günstigen Erfolg entsprochen 
ward.*) 
Das Zuströmen von armen Menschen zur Industrie, die für 
plastische, geistige Arbeit weder Verständniß noch Sinn mitbrachten, ging 
allezeit mit der Hochfluth des Handels flott von statten. Natürlich — 
daß dann bei zeitweiligem Rücktritt dieser, zur Ebbe, eine Menge Schwache r 
und Untauglicher enttäuscht sich auf den Strand geworfen sahen, Menschen, 
deren übermäßig stark belastete Nahrungszweige durch den Druck der Konkurrenz 
ertraglos wurden, — Menschen, die jeglicher geistigen und moralischen Kraft 
entbehren zur Befruchtung ihres Nahrnngszweiges, die nichts gelernt haben, 
w eil ihn e n ni ch ts ge le h r t w o rde n ist, was auf ihr selbstständiges Fort 
kommen gedeihlich hätte einwirken können. Gegen sie, wie überhaupt gegen alle 
Produzenten in sinkenden! Begehr stehender Fabrikate ist der Welthandel ein 
erbarmungsloser Patron. Aber er ist darum kein Haarbreit weniger an 
ständig, weniger hochherzig als das Publikum, dessen Versorger er ist. Denn 
so lange jede Privatperson nicht mehr kauft, als was sie 
braucht, nur daskauft,was ihr beliebt, wird es ewigArbeits- 
lose oder Ueberproduzenten geben und unter ihnen die Konkur- 
*) Siche: A. Fleischmann: Tie Sonneberger Spielwaaren-Hausindustrie und ihr 
Handel. Berlin: L. Simion, Seite 32, 39—42.