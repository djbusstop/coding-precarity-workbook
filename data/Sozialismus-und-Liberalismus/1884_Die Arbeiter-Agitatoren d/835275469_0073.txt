O ; u 
\ A r __ Q £ ¿j. g 
Znl Winhõhause. 
67 
Dritter Akt. 
I m WiŗtHsHcrus. 
Arbeiter. Fabrikanten. Handwerker. Kaufleute, einheimische und fremde. Ein Aus- 
wanderungs-Agent. Ein Kolporteur. Ein Künstler. Ein Wanderer. 
Auswanderungs-Agent: Erlauben die Herren, daß ich ihnen meine Aus 
wanderungs-Prospekte zur geneigten Durchsicht vorlege? Es sind die 
günstigsten, mit billigsten Ueberfahrtspreisen, welche je geboten wurden. 
% Wir machen keinen Gebrauch von Ihrem Anerbieten. Wir bleiben 
daheim. 
E Was uns drückt, dem kann abgeholfen werden. Vielleicht haben Sie 
ein Land für Svzialaristokraten? 
Auswanderungs-Agent: Bedaure! Dann lieber Sozialdemokraten. Nur 
Arbeiter sind gesucht, Leute, die etwas schaffen, dem Staate nützen, 
auch wenn sie mittellos sind. — Für hiesige Spielwaaren-Fabrikanten 
zum Beispiel, hätte ich allergünstigste Propositionen, wenn sich die 
nöthige Anzahl zur Auswanderung zusammensindct. 
$ Neckskünner ka'r gekriech, su vill wie'r will! «Lhner, der wo's 
g'scheidt's geleist ka, gett niet van hier fort, braucht's niet, find' sei Aus 
kumm do ähmsugut wie öllerwärts. — 
(Seiltet, der Stier’s seiner wor drühm in Amerika unn iß ober rar 
widder rüber, weil kä Verdienst für nä drühm wor. In ^umbàrg hott'r 
k5 Iohr fleißig geärbet mit seiner Früh, unn iß a reicher Akah worn. 
Õ Dühm in Amerika fliegen Lnn die gebrothnà Taum ah niet net’s ZÌTcml ! 
Auswanderungs-Agent: Sie werden doch zugeben, meine Herren, daß über alles 
freie Erwerbsleben die Sonne im Westen aufgeht — in Amerika! — In