68 
Im Wirthshause. 
Deutschland geht sie, vom wirthschaftlichen Standpunkt aus gesehen, 
allerdings noch immer weit im Osten — hinter Rußland auf! 
— ZDos woll'n Sie damit soog? IDos bedätten föttd Hieben? 
Änswanderungs - Agent. Ich meine, meine Herren, daß Landwirthschaft, 
Gewerbe, Industrie und Handel in Amerika weit rascher blühen und 
viel besser gedeihen, als in Deutschland. Dafür haben Sie freilich in 
Deutschland Etwas, was Amerika nicht hat: den Staatssozialis 
mus, vertreten von einem ungeheueren Heer von Schriftgelehrten und 
Dozenten, die, in Deutschland geflissentlich gezüchtet, in Amerika — gelinde 
gesagt — verhungern müßten! 
%— Was geht das Sie an? Wer sind Sie, der so spricht? 
Auswandernngs-Agent: Ereifern Sie sich nicht, meine Herren. Ich bin 
ein Deutscher, oder besser, ich war ein Deutscher, hatte studirt, fand 
keine Anstellung und wanderte aus. Bleiben Sie, ich gehe. — 
£— Richten Sie Ihre Wege in andere Gauen. Unser Oberland ist ein 
von Natur gesegnetes Stückchen Erde, wie selbst Amerika keines hat. 
In Deutschland versteht man nur nicht die hiesige Hausindustrie so 
zu würdigen wie im Ausland. 
ft— Ich hörte, dieser Mensch suche hier 30 Arbeiter und Fabrikanten 
sammt Familien anzuwerben, um in Amerika eine Spielwaaren-Haus- 
industrie zu begründen. Er soll den Lenten glänzende Verheißungen 
machen. 
E— Ich halte ihn für einen internationalen Sozialisten. 
ft— Nicht um eine Million ist eine Hausindustrie künstlich zu begründen! 
Die wenigen, weltberühmten, die Deutschland hat, sind vor Jahr 
hunderten von selber angeflogen, unter Verhältnissen, die Gott sei Dank 
in keinem Kulturstaate mehr vorkommen. Es war zu Zeiten der Hungers 
nöthe in Deutschland — als — wegen Mangel an Verkehrs - Wegen 
und-Mitteln, — der Güteraustausch nicht stabil , häufig ganz unmög 
lich war, daß mehrere Hausindustriezweige entsprossen. Wie im Mei 
ninger Oberlande, so auch im Sächsischen Erzgebirge mußten jene Be 
gründer der Spielwaaren-Jndustrie auf Lager arbeiten und darben, 
bis die Nürnberger Kaufleute auf ihren Meßreisen ihre Orte passirten 
und ihre Waaren je nach dem Bedarf, bald zu höheren, bald zu niederen 
Preisen ihnen abkauften. Aber die wenigsten der Arbeiter vermochten 
Monate hindurch Waaren zu verfertigen ohne alsbald Zahlung