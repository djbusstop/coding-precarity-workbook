unseres Volkes frißt, zeigt das iin Jahre 1910 erschienene, 
von der Medizinal-Abteilung des Kultus-Ministeriums her 
ausgegebene Werk: „Über das Gesundheitswesen des preu 
ßischen Staats im Jahre 1908". In diesem Jahre kamen 
auf je 1000 Einwohner 32,99 lebend Geborene. Im länd 
lichen Regierungsbezirk M ü n st e r betrug diese Zahl 44,28, 
im Stadtkreis Berlin nur noch 23,39 ! 
Dabei betrug sie noch 1876, ehe die heutige Mietskaserne 
ihren Siegeszug vollendet hatte, auch in Berlin noch 47,19, 
im Jahre 1911 nur noch 21,64. Von je 1000 Ehefrauen 
wurden 1876 noch 240,3 Kinder geboren, 1910 nur noch 90,5. 
In München betrug in den siebziger Jahren die durch 
schnittliche Eeburtsziffer 43, 1910 nur noch 24,3. 
Immer noch steht als Todesursache die Tuberku 
lose in erster Reihe. Auf je 1000 Gestorbene erlagen ihr 
im Durchschnitt des Staates 16,46. In ländlichen Distrikten 
wie A l l e n st e i n sinkt die Zahl auf 9,75, dagegen steigt 
sie in einer Großstadt wie Breslau auf 20,86, in Berlin 
auf 21,83! 
Auffallend erscheint es, daß trotz der vielgerühmten Vor 
züge der Großstadtkultur auch für die Zahl der Totgeborenen 
und der im Wochenbett Gestorbenen Berlin weitaus die un 
günstigsten Ziffern aufweist. Während z. B. auf je 1000 Ge 
borene im Regierungsbezirk Königsberg i. Pr. 28,44 Totge 
borene kommen, zeigt Berlin deren 36,26. In der Provinz 
Posen, die in hygienischer Beziehung gewiß nicht besonders 
günstig dasteht, kamen auf 10000 Entbundene 34,60 Todes 
fälle im Kindbett; der Stadtkreis Berlin zeigte deren 60,06. 
Ein besonders düsteres Kapitel ist das der übertragbaren 
Geschlechtskrankheiten. Es können natürlich nur