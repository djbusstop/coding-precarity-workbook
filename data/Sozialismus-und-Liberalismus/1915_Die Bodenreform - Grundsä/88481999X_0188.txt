geringer entlohnten ungelernten Arbeiter, sind an zahlreichen Orten 
noch recht schlechte. Die Berichte der die Wohnungsaufsicht aus 
übenden Beamten geben mitunter Kenntnis von unhaltbaren Zu 
ständen, die wegen der Mittellosigkeit der Familien nur sehr schwer 
gebessert werden können." 
Die Wohnungsfrage ist also keine Lohnfrage, wie von 
sozialdemokratischer Seite oft betont wird. Die Lohnerhö 
hung wird im wesentlichen durch die Erhöhung der Grund 
rente aufgesogen, die zu einer Verteuerung nicht nur der 
Wohnungen, sondern auch der Werkstätten, Geschäftsräume, 
Läden und damit zu einer Verteuerung aller Lebensbedürf 
nisse führt. Hier entsteht ein Problem, dessen Bedeutung weit 
über das rein wirtschaftliche Gebiet hinausragt. Wer einmal 
die Bedeutung der wachsenden Industriebevölkerung für 
unser Volksleben erkannt hat, muß auch aus nationalen 
Gründen wollen, daß diese zahlreichste Schicht unseres Volkes 
in schrittweiser Emporentwicklung immer mehr Anteil an 
unserem Kulturleben erringt, und so immer mehr auch die 
innere Einigung unseres Gesamtvolkes herbeigeführt wird. 
Mit Befriedigung begleiten deshalb weite kreise der 
Bildung die steigende Bedeutung der Organisationen unserer 
Industriearbeiter, die in Tarifverträgen usw. geregelte Ver 
hältnisse auf dem Arbeitsmarkt herbeizuführen berufen sind. 
Nun aber sehen wir, wie alle Eenossenschafts- und Gewerk 
schaftsarbeit zuletzt doch zum großen Teil um ihren Lohn be 
trogen wird. In unzähligen Arbeitern, die den Zusammen 
hang mit der Grundrente nicht erkennen, wächst ein Gefühl 
des Zweifelns und des Verzweifelns, steigt der Gedanke empor, 
daß erst diese ganze „kapitalistische Gesellschaftsordnung" ver 
nichtet werden muß, ehe die Arbeit zu ihrem Rechte kommen 
kann. Soll diesem Gedanken der Nährboden entzogen werden,