hat er keine andere Wirkung als die, den Kaufpreis zu er 
höhen. Eine Baustelle, die bei einem Zinsfuß von 6 °/o 
10 000 M kostet, wird bei einem Zinsfuß von 3 % 20 000 M 
kosten. Billiger städtischer Kredit kann deshalb nur zu leicht 
ein Geschenk an die zufälligen Bodeneigentümer bedeuten, 
ohne eine soziale Wirkung auszuüben. 
Bei rechter Ausgestaltung aber können städtische Hypo 
thekenämter die folgenreichsten Reformen vorbereiten. Sie 
sollten nur Darlehen geben, wenn es sich tatsächlich um 
bauliche Aufwendungen handelt, deren kosten natürlich 
unschwer zu schätzen sind. Sie dürften auch nur gegeben 
werden, wenn die Beleihung an erster Stelle in der Form 
der Tilgungs-Hypothek vorgenommen ist, so daß 
die städtische zweite Hypothek allmählich vorrücken muß. 
Am vollkommensten wird die städtische Hypothek ihre 
sozialen Aufgaben erfüllen, wenn die spekulative Verwertung 
der beliehenen Grundstücke grundsätzlich ausgeschlossen ist: 
etwa durch das Wiederkaufsrecht, wie es in Ulm und 
Opladen durchgeführt ist, oder da, wo der Boden im 
Eigentum der Gemeinde steht und nur das Gebäude in Form 
des Erbbaurechts den Privaten gehört. 
Das Ziel der Bodenreform: die gesamte Grund 
rente für die Gesamtheit zu gewinnen, 
kann nicht erreicht werden ohne eine durchgreifende Reform 
des Hypothekarrechts. Schon heute muß der städtische Haus 
besitzer in der Regel die gesamte Grundrente abgeben. Er 
tut es in Form von Hypothekenzinsen, und die ganze ge 
waltige Summe fließt in die Taschen des Privatkapitals, 
zumal der Hypothekenbanken, anstatt in die lassen der Ge 
samtheit. Eine organische Umwandlung dieser Zinsen in