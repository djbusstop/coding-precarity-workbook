Die Lodenreform in 2frael. 
1. Die Lehren der Geschichte. 
3 st wirtlich der entscheidende Teil des sozialen Problems in 
der Bodenfrage gegeben, so muß diese Wahrheit auch in 
der Weltgeschichte offenbar werden. Wenn wir aus dem 
Lärm der Tageskämpfe zurücktreten, wenn wir die Kulturen 
der vergangenen Zeiten vor unserem geistigen Auge prüfend 
erstehen und vergehen sehen, so muß sich die Bodenrechts 
gestaltung als das Grundlegende erweisen. 
Im einzelnen wird das Bodenreformideal verschieden sein 
können, ja sein müssen, je nach den Zeitverhältnissen. Aber 
das Wesentliche wird sich gleichbleiben, heute wie einst: Die 
große Menge des arbeitenden Volkes muß freien Zutritt zu 
der Urquelle aller Produktion, der Natur, sie muß gesicherte 
Heimstätten in ihrem Vaterlande haben, wenn gesunde so 
ziale Verhältnisse herrschen sollen. Die Trennung des Volkes 
von seinem Vaterland, das Aufhäufen von Grundeigentum 
in wenigen Händen unmittelbar oder in Form der Boden 
verschuldung, muß sich überall als verhängnisvoll erweisen, 
wenn die Bodenreformlehre die Wahrheit enthält. 
Die Geschichte ist die einzige Lehrerin, deren Urteil un 
bestechlich ist, und deren Wahrspruch auch kein Augenblicks- 
sieger dauernd fälschen kann. Es gibt deshalb für den, der 
seiner Zeit und seinem Volke dienen will, nicht höhere Weis 
heit, nicht ernstere Lehre als die, welche ihm die Geschichte 
der vergangenen Zeiten zu bieten vermag.