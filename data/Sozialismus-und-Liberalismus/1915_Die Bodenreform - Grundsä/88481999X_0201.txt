181 
1894: 
1895: 
1896: 
1897: 
1898: 
1899: 
1900: 
1901: 
1902: 
1903: 
237 289 
255 608 
277 498 
321 058 
357 547 
387 895 
395 694 
401 392 
393 751 
444 834 
000 Jí 
000 „ 
000 .. 
000 „ 
000 . 
000 „ 
000 „ 
000 „ 
000 , 
ooo .. 
1904: 
1905: 
1906: 
1907: 
1908: 
1909: 
1910: 
1911: 
1912: 
1913: 
407 286 
469 306 
515 194 
556 315 
584 154 
640 280 
733 800 
739 880 
747 920 
787 140 
000 M 
000 „ 
000 , 
000 , 
000 „ 
000 „ 
000 „ 
000 , 
000 ^ 
ooo „ 
Insgesamt betrug die Zunahme der Verschuldung 
von 1886 bis 1913 rund 11000 Millionen M. Eine Durch 
schnittsverzinsung von 4% ergibt, daß die preußische Land 
wirtschaft heute rund 440 Millionen M mehr Jahres 
zinsen zu zahlen hat als noch im Jahre 1886 ! Das bedeutet 
für jeden Tag — Sommer und Winter, Werktag und Sonntag 
— eine Zinsenlast von über 1 200 000 M mehr als noch vor 
27 Jahren. 
Natürlich ist nicht diese ganze Summe als eine Mehr 
belastung des nackten Bodens aufzufassen. Der Wert der neuen, 
die Verbesserung der alten Gebäude und die Meliorationen 
des Bodens sind zum Teil dagegen zu rechnen, allerdings 
nur zum Teil — weil zweifellos viele Baulichkeiten bereits 
wieder verfallen sind, während die Schulden, die zu ihrer 
Errichtung aufgenommen wurden, weiter bestehen. 
Aber auch darüber unterrichtet uns die amtliche Statistik 
nur überaus dürftig. Was sie aber sagt, zeigt einen wesent 
lichen Teil der Hypothekenlast als eine Verschuldung des 
reinen Bodenwertes. Selbst in dem vorwiegend indu 
striellen und städtischen Sachsen nahm von 1884—1890 
der Versicherungswert der Privatgebäude nur um 673, 
die Hypothekenlast aber um 813,64 Millionen M zu!