140 
ber Stadt machtvoll fördern müssen. Auch das Privatkapital, 
das sich in den Baulichkeiten betätigt hat, wirft zureichenden 
Gewinn ab. Die Lagerhaus-Gesellschaft verteilt seit Jahren 
5 % Dividende. 
7. Industrielles Neuland. 
D erhältnismäßig am leichtesten können die Bodenreform 
gedanken auf Neuland durchgeführt werden. Neuland 
für unser wachsendes Volk aber must geschaffen werden. 
Denn bei gleichbleibender Entwicklung werden schon in der 
Mitte des Jahrhunderts im heutigen Reichsgebiet 100 Mil 
lionen Menschen Wohn- und Werkstätten finden müssen. 
Will man sich darauf beschränken, die bisherigen Sammel 
punkte, wie Berlin, Hamburg, Breslau, Leipzig, Dortmund, 
Gelsenkirchen usw. in der alten Weise mit ihrem Miets 
kasernensystem wachsen zu lassen? Oder sollen nicht vielmehr 
neue, wenn möglich bessere Siedlungsmöglichkeiten geschaffen 
werden? 
Aus diesem Gesichtspunkt heraus sind die Kanalbauten 
der letzten Zeit aufzufassen. Aber es zeigte sich bald, dast 
sie unter unserem heutigen Bodenrecht nicht imstande waren, 
billiges Neuland zu erschliesten. Überall fanden sich Terrain 
spekulanten, die die Ufer der neuen Verkehrswege „zur 
rechten Zeit" in ihre Hand brachten, um sich dann ihre Vor 
teile in erhöhten Bodenpreisen von denen bezahlen zu lassen, 
die das neue Siedlungsgebiet als Wohn- und Werkstätten 
benutzen wollen. 
Als die preußische Regierung den sogenannten „Mittel 
landkanal" vorschlug, der quer durch Preußen neue Siedlungs 
möglichkeiten erschließen sollte, und dieser Plan zu schweren