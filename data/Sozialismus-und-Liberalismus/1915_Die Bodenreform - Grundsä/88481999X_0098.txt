78 
5. Teil des Einkommens betrage, und deren Schlafräume 
für jede über 10 Jahre alte Person mindestens 10, für jede 
jüngere mindestens 5 cbm Luftraum enthalte. Die Unter 
suchung ergab, wie der amtliche Bericht zugeben mußte, Zu 
stände, die niemand für möglich gehalten hätte: 
„In einer Wohnung schliefen z. B. drei über 14 Jahre alte 
Mädchen mit den Eltern in einer Stube von 34 cbm Rauminhalt 
und in einer andern sogar drei Söhne von 10—24 Jahren und 
zwei Töchter von 13—19 Jahren mit der Mutter in einem Zimmer 
von nur 25 cbm Rauminhalt." 
In einem einzigen Polizeirevier fanden sich 148 Woh 
nungen, die nicht einmal den oben bezeichneten Ansprüchen 
genügten. Und dabei wird kein Gefängnis und Zuchthaus 
neu gebaut, das nicht etwa das Dreifache, nämlich 28 cbm 
Luftraum, jedem Gefangenen sichert! 
Als der Volkswirt Or S t i l l i ch die Spielwarenindu 
strie im Meininger Oberlande studierte, gab ihm ein Pfarrer 
die Versicherung, daß in seinem Orte „die Leute gut und 
angenehm wohnen, und eine Wohnungsfrage nicht existiere". 
Schon das erste Haus, das Stillich betrat, zeigte demgegen 
über geradezu entsetzliche Zustände. 
Um diese allgemeine Unkenntnis zu beseitigen, fordern 
die Bodenreformer die Schaffung von Wohnungs 
ämtern. Es muß eine Stelle in der Gemeinde geben, 
die sich berufsmäßig mit den Ubelständen im Wohnungswesen 
zu befassen hat, eine Stelle, die unparteiisch Jahr für Jahr 
über die Verhältnisse Bericht zu erstatten hat. 
In Hamburg haben die Lehren der letzten furcht 
baren Choleraepidemie zur Einsetzung einer besonderen 
„Wohnungspflegschaft" geführt. Der Wert solcher Ein-