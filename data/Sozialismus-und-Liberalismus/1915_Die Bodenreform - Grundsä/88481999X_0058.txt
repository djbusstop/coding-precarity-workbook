38 
ihm als Grundlage seines Systems gewählte „Gesetz der 
kapitalistischen Akkumulation" ebenfalls seine Wurzel in 
der pseudo-liberalen Lehre. Ihr Hauptvertreter neben Mal 
thus: David Ricardo, hatte bereits in seinen „Grund- 
sätzen der Volkswirtschaft und Besteuerung" (Kap. 31) er 
klärt: ,,Ich bin davon überzeugt, daß der Ersatz der mensch 
lichen Arbeit durch Maschinen den Interessen der Arbeiter 
klasse häufig verderblich sei". 
^şuch für Marr ist es unumstößliche Wahrheit, daß in 
unserer wirtschaftlichen Ordnung technischer Fortschritt 
und steigender Reichtum die Lage der arbeitenden Bevölke 
rung immer mehr verschlimmern muß. Das ist der Inhalt 
seines „Gesetzes der kapitalistischen Akkumulation", das er 
in seinem „Kapital" (4. Aufl. I, S. 609) so formuliert: 
„Je größer der gesellschaftliche Reichtum, das funktionierende 
Kapital, Umfang und Energie seines Wachstums, also auch die ab 
solute Größe des Proletariats und die Produktivkraft seiner Arbeit, 
desto größer die industrielle Reservearmee. Die disponible Arbeits 
kraft wird durch dieselben Ursachen entwickelt, wie die Expansivkraft 
des Kapitals. Die verhältnismäßige Größe der industriellen Reserve 
armee wächst also mit den Potenzen des Reichtums. Je größer aber 
diese Reservearmee im Verhältnis zur aktiven Arbeiterarmee, desto 
massenhafter die konsolidierte Übervölkerung, deren Elend im umge 
kehrten Verhältnis zu ihrer Arbeitsqual steht. Je größer endlich die 
Lazarusschicht der Arbeiterklasse und die industrielle Reservearmee, 
desto größer der offizielle Pauperismus. Dies ist das absolute, 
allgemeine Gesetz der kapitalistischen Akkumulation." 
„Dieses Gesetz schmiedet den Arbeiter fester an das Kapital, als 
den Prometheus die Keile des Hephästos an den Felsen. Es bedingt 
eine der Akkumulation von Kapital entsprechende Akkumulation von 
Elend. Die Akkumulation von Reichtum auf dem einen Pol ist zu 
gleich Akkumulation von Elend, Arbeitsqual, Sklaverei, Unwissenheit,