411 
Eine wesentliche Unterstützung fand, wie schon aus 
dem Titel meiner Streitschrift hervorging, der Kampf um 
das deutsche Neuland durch die Gestaltung unseres ost 
asiatischen Pachtgebietes Kiautschou, das nicht dem 
Kolonialamt, sondern dem Reichsmarineamt unter 
stellt wurde. 
Im Augenblicke der deutschen Besitzergreifung begann 
die Landspekulation einzusetzen. Die Chinesen schlossen 
einen Ring und verlangten von den deutschen Beamten 
für ihren Boden fast zehnmal so hohe Preise, als die, die 
vor der deutschen Besitzergreifung üblich waren. Die deutsche 
Verwaltung aber, die in diesem wichtigen Augenblicke glück 
licherweise in den Händen eines Mannes lag, wie Admiral 
von Diederichs, eines treuen Mitgliedes des Bundes 
Deutscher Bodenreformer, beugte sich diesem Ringe nicht. Sie 
gab jedem chinesischen Grundbesitzer eine Barsumme, die 
ungefähr das Doppelte von dem Iahresbetrage der bis 
herigen Grundsteuer ausmachte. Dafür aber mutzte sich 
ein jeder verpflichten, seinen Boden nur noch an das deut 
sche Gouvernement zu verkaufen, und zwar zu dem Preise, 
der vor der deutschen Besitzergreifung landesüblich war, 
und der ntit Hilfe der chinesischen Grundsteuerlisten ohne 
besondere Schwierigkeiten festgestellt werden konnte. 
In den großen Handelsplätzen Ostasiens fanden sich bald 
Leute, denen der Boden des neuen deutschen Pachtgebietes 
ein vielverheitzendes Spekulationsobjekt zu sein schien. Man 
schloß Kartelle, zum Teil nach denselben Grundsätzen, wie 
sie die Trödler unserer Städte zu befolgen pflegen, d. h. man 
kam überein, sich gegenseitig die Preise nicht zu verderben, 
und verteilte vorher die einzelnen Blöcke auf den Bauplänen.