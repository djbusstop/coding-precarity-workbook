375 
acker in der Alerand erstratz e kaufte, zahlte sie 52 Taler. Jetzt 
gilt derselbe nackte Boden über 3 000 000 M ! 
Friedrich Wilhelm I., der größte „innere" König Preu 
ßens, war auch der rücksichtsloseste Bodenreformer seiner 
Zeit. Er erkannte scharf die Bedeutung des Ediktes des 
Großen Kurfürsten vom Jahre 1667 und erneuerte es aus 
drücklich in den Jahren 1721 und 1722. 
Die Besitzer aller noch vorhandenen Baustellen in 
der Friedrichstraße mußten erklären, ob sie ihre Plätze be 
bauen wollten oder nicht. Denen, die ihre Baustellen länger 
wüst liegen ließen, wurde das Eigentumsrecht abgesprochen. 
Gegen eine Entschädigung für die etwa vorhandenen Scheu 
nen, Ställe oder die Aussaat sollte jeder Baulustige sie in 
Besitz nehmen können. In den meisten Fällen wird es natür 
lich zu einer Wegnahme nicht gekommen sein, sondern die 
Besitzer werden sich beeilt haben, selbst Gebäude zu errichten, 
womit das Ziel: der Bau neuer Werk- und Wohnstätten, 
la auch erreicht war. Den Bauwilligen wurde eine Steuer 
freiheit bewilligt, die bis auf 10 Jahre ausgedehnt wurde. 
Steine, Holz und Kalk wurden vom Könige umsonst in großem 
Umfange gegeben, und 10—15 % der an sich nicht hohen 
Baukosten wurden oft bar zugeschossen. 
In dem einen Jahre 1736 hat Friedrich Wilhelm I. 
insgesamt den preußischen Städten einen Barzuschuß zu 
Hausbauten in Höhe von 350 000 Talern geleistet. Das war 
wehr als der 20. Teil der gesamten Staatseinnahmen! Die 
Bodenpolitik, die in Berlin galt, wurde natürlich auch in 
ben anderen preußischen Städten befolgt, wenn auch dort 
der Einfluß des Königs persönlich nicht so stark einsetzen 
konnte wie gerade in der Residenz. In der Instruktion für