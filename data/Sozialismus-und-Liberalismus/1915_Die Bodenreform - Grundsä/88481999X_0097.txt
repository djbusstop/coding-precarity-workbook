77 
„In dem Maße, als die Wohnungsverhältnisse ungenügend sind, 
wird die allgemeine körperliche Leistungs- und Widerstandsfähigkeit 
geschwächt, der Ausbruch oder die Verbreitung gewisser schwerer 
Krankheiten, insbesondere von Typhus, Schwindsucht und Syphilis 
gefördert, Sittlichkeit und Zufriedenheit untergraben, die geistige Aus 
bildung unterbunden, damit aber auch das wirtschaftliche Fortkommen 
des einzelnen und die allgemeine Bolkswohlfahrt gefährdet!" 
ìì^oglich sind die heutigen Zustände allerdings nur, weil. 
^1 i sie in den weitesten kreisen überhaupt nicht bekannt 
sind. Es gibt sehr gebildete und sehr wohlwollende Men 
schen, die mehr von den Lebensbedingungen der alten oder 
der heutigen Ägypter, Griechen und Römer oder der Hindus 
und Neger wissen, als von den Lebensbedingungen ihrer 
eigenen Volksgenossen, die entweder im Hintergebäude des 
selben Hauses oder wenige Gassen entfernt wohnen. 
Auch auf diesem Gebiete kann man das bittere Wort 
anwenden, das S i m r o ck einst in anderem Zusammen 
hang prägte: 
„In Rom, Athen und bei den Lappen, 
Da späh'n wir jeden Winkel aus, 
Und darum müssen wir auch tappen 
Fremd in dem eigenen Vaterhaus." 
Welche Unkenntnis über diese Dinge auch an verant 
wortlichen Stellen in deutschen Gemeinden herrscht, zeigte 
ein Vorstoß bodenreformerischer Kreise in Halle. Es wurde 
von maßgebender Seite einfach das Vorhandensein besonderen 
Wohnungselends in der schönen Saalestadt bestritten, und 
man ging gern auf den Vorschlag ein, einmal den Tat 
bestand durch Untersuchungen festzustellen. Jede Wohnung 
sollte als genügend gelten, deren Miete nicht mehr als den