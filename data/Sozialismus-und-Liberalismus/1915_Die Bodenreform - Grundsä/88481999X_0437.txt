Wie hat sich denn nun diese Bodenreform in der Praris 
bewährt? Die zweite „Denkschrift, betreffend die Entwick 
lung des Kiautschougebietes in der Zeit vom 1. Oktober 1898 
bis 1. Oktober 1899" gibt die Antwort: 
„Es war vorauszusehen, daß diese in Kiautschou zum erstenmale 
praktisch durchgeführten Grundsätze neben vielfacher Zustimmung zu 
nächst auch einigen Widerspruch aus Interessentenkreisen Hervorrufen 
würden; es kann jedoch bereits jetzt festgestellt werden, daß letzterer 
innerhalb und außerhalb des Schutzgebietes mehr und mehr verstummt 
ist und einem lebhaften Einverständnisse Platz gemacht hat." 
Wo immer man Kenntnis hat von der Bedeutung der 
Grund- und Bodenfrage für die gesamte wirtschaftliche Ent 
wicklung, gilt die „Landordnung von Kiautschou" als eine 
Tat ersten Ranges. Wenn vieles überwunden und vergessen 
sein wird, was jetzt lärmend im Vordergrund öffentlichen 
Kampfes steht, wird diese „Landordnung" noch als ein 
Ruhmesblatt deutscher Sozialpolitik gefeiert werden. 
Auf dem 7. Internationalen Geographen-Kongreß zu 
Berlin legte der Vertreter der Vereinigten Staaten, P»ult- 
ney Bigelow, am 3. Oktober 1899 die Bedeutung 
dieses Vorgehens, wie folgt, dar: 
„Kiautschou verdient in ganz besonders hohem Maße die Auf 
merksamkeit der weitesten Kreise. Hier sind zum erstenmale die Grund 
sätze Henry Georges, d. h. also der Bodenreform, in die Praxis 
übersetzt. Und zwar sind diese viel bekämpften Lehren unter dem 
Schutze, unter der Autorität des Deutschen Reiches ins Leben ein 
geführt. Das hat eine Bedeutung, deren Tragweite noch gar nicht 
zu übersehen ist. In der ganzen Welt, in Amerika, in Australien, 
in England und wo immer man den Lehren Henry Georges Ver 
ständnis entgegenbringt, sieht man mit größter Spannung auf die 
Entwicklung dieser Kolonie." 
Damaschle, Bodenreform, 31.—35. Tausend. 27