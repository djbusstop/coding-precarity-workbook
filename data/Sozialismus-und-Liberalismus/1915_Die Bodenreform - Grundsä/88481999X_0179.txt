159 
werkern eine gewisse Sicherheit. Er soll aber nur für solche 
Gemeinden gelten, die durch landesherrliche Verordnung 
ausgewählt werden. Bisher ist aber keine einzige Gemeinde 
dieses Schutzes teilhaftig geworden. 
Das Preußische Statistische Landesamt hat über die 
Jahre 1909—1911 eine Umfrage in 48 Gemeinden von 
Eroß-Berlin vorgenommen. Danach gaben 2384 Handwerker 
und Lieferanten 9289 Verlustfälle an. Von den 1278 Land 
häusern, die in den drei Jahren aufgeführt wurden, brachten 
145, von den 432 Geschäftshäusern 55, von den 5252 Miets 
kasernen aber 2618 den Baugläubigern Verluste! Insgesamt 
wurden 20 601 580 M verloren! Dabei ist zu beachten, daß 
diese Zahl eine Mindestzahl ist, denn nach alter Erfahrung 
geben viele Bauhandwerker ihre Verluste nicht an, weil sie 
eine Schädigung ihres Kredits fürchten. 
Die amtliche Untersuchung zeigt, wie wenig das heutige 
Recht den Bauschwindel bestraft. Ein Bauunternehmer, der 
Baugeld beiseite gebracht hatte, so daß die Bauhandwerker 
13 000 JK> verloren, erhielt eine Geldstrafe von 60 M. 
Als Strohmänner zeigt die amtliche Untersuchung z. B. 
einen vermögenslosen Mechaniker, der 90 Pfennig die Stunde 
verdient, von einer Terraingesellschaft aber ein Grundstück 
erhält, und von anderer Seite Baugeld in Höhe von 
173 000 M ! Sie zeigt einen Reisenden, dem eine Grund 
stücksgesellschaft eine Baustelle verkauft, obwohl er von vorn 
herein erklärt, weder Gew noch Fachkenntnisse zu besitzen 
und bei der Vernehmung weder den Preis des Grundstücks 
noch die Höhe des Baugeldes angeben konnte! 
Wiederholt wurde festgestellt, daß die Strohmänner in 
den Kaufverträgen sich die Bedingung von den Direktoren