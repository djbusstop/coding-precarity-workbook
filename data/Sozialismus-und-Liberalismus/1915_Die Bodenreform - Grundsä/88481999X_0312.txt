292 
watte Tiberius Gracchus war zweimal Zensor gewesen. Er 
hatte in Spanien über 100 Städte unterworfen und, was 
mehr war, sie durch Gerechtigkeit und Verständnis so ge 
fesselt, daß sein Name noch lange dort in hohen Ehren blieb. 
Cornelia hatte ihm zwölf Kinder geboren, von denen aber 
neun früh starben: so blieben noch übrig eine Tochter, Sem- 
pronia, und die beiden Söhne, Tiberius und Eajus. Nach 
dem Tode des Gatten widmete sich Cornelia ganz der Er 
ziehung ihrer Kinder. Als einst vornehme Frauen sich ihres 
Schmuckes rühmten und sie aufforderten, doch auch einmal 
ihre Kostbarkeiten zu zeigen, da führte sie ihre Kinder in 
das Zimmer und sagte: „Das ist mein Stolz!" Als der 
König Ptolemäus von Ägypten sie zum Weibe begehrte, 
lehnte sie das verlockende Angebot ab, weil sie fürchtete, 
daß die Königskrone sie in ihrer Mutterpflicht beengen 
würde. Die Tochter Sempronia heiratete den jüngeren 
Scipio Africanus, den Zerstörer Karthagos und Numantias. 
Tiberius nahm ein Weib aus dem Geschlechte der Claudier. 
Sein Bruder Eajus vermählte sich mit der Tochter des 
Oberpriesters Mucianus. 
Tiberius durfte also auf manche Hilfe rechnen, wenn er 
nach seiner Wahl zum Tribunen an die Durchführung der 
Bodenreform ging. Die verpachteten Landstücke, für die 
eine Abgabe an den Staat gegeben wurde, berührte sein Vor 
schlag überhaupt nicht. Lediglich die Staatsländereien, 
die ohne Entgelt benutzt wurden, sollten eingezogen werden. 
Um aber den Optimalen möglichst weit entgegenzukommen, 
sollte jeder bis 500 Morgen vom Staatsland und für jeden 
Sohn noch 250 Morgen, insgesamt bis zu 1000 Morgen 
Staatsland, als freies Eigentum behalten können! Für alle