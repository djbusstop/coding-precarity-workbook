361 
den Weberinechten in Speyer 1362, daß Meister und Gesellen 
sich in bestimmtem Verhältnis, wie 1 zu 1 oder wie 3 zu 2, 
den Ertrag der Arbeit teilen. 
Professor Lnapp hat Recht: 
„Was auch Böses dem Mittelalter nachgesagt werden mag, einen 
Vorwurf darf man ihm nicht machen: das Mittelalter kennt weder 
im städtischen Gewerbe noch in der Landwirtschaft die wirtschaftliche 
Ausbeutung des Nebenmenschen". 
Und selbst in der Zeit, in der das Freiland im Osten 
der Elbe aufhörte, als die Grundrente im Deutschen Reiche 
wieder zu steigen begann, wirkte jene außerordentliche wirt 
schaftliche Blüte noch nach. In der zweiten Hälfte des 
15. Jahrhunderts hatte sich die Lage der arbeitenden blassen 
bedenklich verschlimmert. Trotzdem finden wir noch in dieser 
Zeit Verordnungen gegen übertriebenen Lurus in Lleidung 
und Nahrung! 
Die Landesordnung, welche die Herzoge Ernst und 
Albert von Sachsen im Jahre 1482 erließen, ist in bezug 
auf die Lebenshaltung der damaligen Handwerksgesellen 
außerordentlich lehrreich. Es werden als Höchstlohn festgesetzt 
sür einen Handarbeiter mit Lost wöchentlich 9 neue Groschen, 
ohne Lost 16 Groschen. „Denen Werkleuten sollen zu ihrem 
Ņttag- und Abendmahle nur 4 Essen, an einem Fleischtag 
eine Suppe, zwei Fleisch und ein Gemüse; auf einen Freitag 
und einen andern Tag, da man nicht Fleisch isset, eine Suppe, 
ein Essen grüne oder dörre Fische, zwei Zugemüse; so man 
saften müsse, fünf Essen, eine Suppe, zweierlei Fisch und 
zwei Zugemüse und hierüber 18 Groschen, den gemeinen 
Aîerkleuten aber 14 Groschen wöchentlicher Lohn gegeben 
werden; so aber dieselben Werkleute bei eigener Lost arbei-