18 
schenken, überall wird um Antwort, wenn auch nur um 
ein Stückchen Antwort, gerungen. Der höchsten Aufgabe 
dient, wer an seinem Teile hier ehrlich mitarbeitet. 
In unserer Kulturwelt gibt es im wesentlichen drei 
Antworten auf die soziale Frage: die m a m m o n i st i s ch e , 
die k o m m u n i st i s ch e und die bodenreforme- 
rische Antwort. 
2. Der M a m m o n i s m u s. 
^^ie mammonistische Auffassung ist die heute herrschende. 
Nach ihr ist im großen und ganzen alles so gut einge 
richtet, wie es auf dieser unvollkommenen Erde nur sein kann. 
Unwissenschaftlich, „utopisch" ist nach ihr jeder Versuch, eine 
dauernde durchgreifende Besserung in der Lebenshaltung 
aller Volksschichten herbeizuführen. 
Mit Unrecht legt sich diese Auffassung oft den Namen 
liberal bei. Die großen Vorkämpfer der liberalen Wirt 
schafts - Auffassung, die P h y s i o k r a t e n und Adam 
Smith, erstrebten die Beseitigung aller Sonderrechte und 
Monopole, weil für sie die wirtschaftliche Freiheit die 
Mutter aller wirtschaftlichen Harmonie war: in einer wahr 
haft freien Wirtschaftsordnung muß der gesellschaftliche 
Wohlstand und seine gerechtere Verteilung wachsen. Aber 
diese von glühender Hoffnungsfreudigkeit getragene Lehre 
ist bisher nie ehrlich durchgeführt worden. Was die Phy- 
siokraten, und in gemilderter Form auch Adam Smith, 
von der Bedeutung der Grundrentensteuer sagten, ließ der 
dritte Stand unbeachtet, als er zur Herrschaft kam. Ja, 
durch ihn wurde im Namen der wirtschaftlichen Freiheit das