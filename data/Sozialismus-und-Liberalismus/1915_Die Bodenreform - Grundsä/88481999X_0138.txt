118 
^%te[e Erkenntnis war aber bereits die Folge eines Kampfes, 
den die Bodenreformer feit der Gründung ihres ersten 
Bundes im Jahre 1888 für den Gedanken der Zuwachssteuer 
geführt hatten. Es war in den ersten langen Jahren ein 
schwerer Kampf. Immer wieder mußten sie sich sagen lassen, 
daß der Gedanke in der Theorie zwar sehr schön, aber in der 
Praris völlig undurchführbar sei! 
Da, nach zehn Jahren, 1898, tat die deutsche Marinever 
waltung den ersten Schritt, indem sie in der Landordnung 
von Kiautschou zum erstenmal eine Zuwachssteuer von 
331/3 °/o einführte (siehe den VIII. Abschnitt dieses Buches: 
„Hohenzollern und Bodenreform"). 
Natürlich benutzten die Bodenreformer das mutige und 
erfolgreiche Vorgehen der Marineverwaltung, um auch in 
deutschen Gemeinden für die Zuwachssteuer Stimmung zu 
machen. Aber in Hunderten von Versammlungen der näch 
sten Jahre habe ich dann stets das gleiche Schauspiel erlebt: 
In der freien Aussprache meldete sich ein Herr, der für 
Grundstücksgeschäfte besonderes Interesse hatte, und erklärte 
feierlich, daß er mit warmem Herzen für die Zuwachssteuer 
sei — in Kiautschou, am Großen Ozean, daß er sie aber für 
völlig ungerecht und unpraktisch, für ganz undurchführbar 
halte in Deutschland selbst und namentlich in der Gemeinde, 
in der er selbst Gelände besäße. 
Unter dem Druck steigender Finanznot hat dann am 
1. April 1904 Frankfurt a. M. eine milde Zuwachssteuer 
eingeführt; am 1. April 1905 folgte Cöln, und nun ging es 
schnell vorwärts. Bis zum 1. April 1910 hatten 470 Gemein 
den und 13 Landkreise die Zuwachssteuer angenommen. 
Überall zeigte ja auch die Praris, daß die von interessierter