62 
ist Bodenreformlehre: diese Grundrente ist soziales 
Eigentum. Diese Grundrente soll durch irgendwelche 
Reformarbeit möglichst für die Gesamtheit, die die Grund 
rente allein erzeugt, zurückerrungen werden. Jedem das 
Seine! Dem Einzelnen den möglichst vollen Ertrag seiner 
Arbeit und seines Kapitals! Aber auch der Gemeinschaft, 
was sie allein hervorbringt! Was alle zusammen erarbeiten, 
das soll kein Einzelner ohne genügende Gegenleistung mit 
Beschlag belegen dürfen. 
Das ist der Friede zwischen Sozialismus und Indivi 
dualismus : die Grundrente soziales Eigentum, Kapital 
und Arbeit aber der individuellen oder freien ge 
nossenschaftlichen Betätigung gesichert! — 
Das soziale Eigentum, das in der Grundrente natürlich 
gegeben ist, würde die Gesamtheit reich genug machen, um 
aller unverschuldeten Not ein Ende zu bereiten und jedem 
Menschenkinde, das in diese Gesellschaft hineingeboren wird, 
die Möglichkeit zu geben, seine körperlichen, sittlichen und 
geistigen Fähigkeiten voll zu entwickeln. Wie in einer reichen 
Familie jedem Kinde ein gern gewährtes Recht auf die beste 
Erziehung, auf die sorgsamste Pflege zusteht, so würde in 
dieser reichen Gemeinschaft, die das Produkt ihrer gemein 
schaftlichen Arbeit, ihre Grundrente, besitzt, auf dem Gebiet 
der Schule, der Gesundheitspflege, der Kunst usw. jede Ver 
besserung im voraus bewilligt sein! 
Ist die Grundrente soziales Eigentum, so fällt jede Ur 
sache, ja auch jede Möglichkeit, den Boden und seine Schätze 
zu monopolisieren. Der freie Zugang jeder Arbeit zu der 
Urquelle aller Produktion ist gesichert. Natürlich wird damit 
auch das Verhältnis zwischen Arbeit und Kapital wesentlich