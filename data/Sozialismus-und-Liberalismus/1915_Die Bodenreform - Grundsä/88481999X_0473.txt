453 
Die Hauptstadt Tsingtau, die im Jahre 1898 ein un 
ansehnliches Fischerdorf war, hatte 1913 rund 60 000 Be 
wohner, darunter 2000 Europäer. In ihrem Hafen liefen 
1913 bereits 902 Schiffe ein, darunter 317 unter deutscher 
Flagge. Der Gesamtwert des Handels war auf etwa 200 Mil 
lionen Mark gestiegen. 
Diese erfolgreiche Arbeit, bei der stets von deutscher 
Seite auf ein freundliches Hand- in Handgehen mit China 
der größte Wert gelegt wurde, lieh natürlich den deutschen 
Einfluh weit über die Grenzen Kiautschous und Schan- 
tungs an Bedeutung gewinnen. Wie die Volksstimmung 
ist, läht ein Brief erkennen, den De Gerhard Menz am 
3. Juni 1915 aus Shanghai sandte, den die „Akademischen 
Blätter" am 1. August veröffentlichten: 
„Bei den Chinesen sind wir unbedingt ganz Nummer eins. Es 
ist kaum glaublich, wie wir im ganzen Volk bewundert werden und 
gut angeschrieben sind. Es ist geradezu eine Empfehlung, ein Deut 
scher zu sein." 
Am Kaiserhof und später, als der frühere Gouverneur 
von Schantung I u a n s ch i k a i Präsident der Republik wurde, 
wuchs der Einfluh des Deutschen Reiches, dessen ehrliche 
Freundschaft und Leistungsfähigkeit in Kiautschou erprobt 
war. Die Wut der englischen Jndustriekreise wuchs in glei 
chem Mähe — nein, ins maßlose. Sie hatten sich eine be 
sondere Organisation zur Bekämpfung der deutschen Waren 
in Ostasien in der „British Engineer Association" geschaffen, 
deren Mitglieder ein Firmenkapital von 1400 Millionen M 
vertraten. Bezeichnend für ihre Kampfesweise ist ein Auf 
satz, den ihr Organ, die „Eastern Engineering", im März 
1913 brachte. Er behauptet, wir hätten die Türkei, die sich 
auf uns verlassen hätte, in schmählicher Weise betrogen, in-