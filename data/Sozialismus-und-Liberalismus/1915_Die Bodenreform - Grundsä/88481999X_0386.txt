360 
etwa 78 Jl, ein Morgen also nicht ganz 20 M. Nur das 
Weinland von Berlin machte eine Ausnahme. Berliner 
Wein wurde geschätzt, und ein Morgen Weinland deshalb mit 
etwa 400 M gewertet. Man zählte zu jener Zeit 70 Wein 
berge und 26 Weingärten im Weichbilde der beiden Städte. 
Die Hauptsteuer war der sogenannte Schoß, eine Art 
Vermögenssteuer, die der heute von den Bodenreformern 
geforderten Grundsteuer nach dem gemeinen Wert etwa ent 
spricht. Nach den Bedürfnissen der Stadt wurde der Steuersatz 
bestimmt und als Pfundschoß erhoben. Im Jahre 1671 be 
trug er für ein Schock Groschen Vermögen 9 Pfemüg, also 
etwa 114%, während die E rundwertsteuer heute nur in 
wenigen Fällen bis zu %% steigt. 
Die Einwohnerzahl Berlins und Köllns wird um das 
Jahr 1600 auf 12 000 Seelen geschätzt. 
So sah es in Berlin aus, als die furchtbarste Katastrophe 
über Deutschland hereinbrach, die jemals über unser Volk 
gekommen ist: der Dreißigjährige Krieg. Man kann sich 
die Verwüstungen an unserem Volkstum und unserer Kultur 
nicht furchtbar genug vorstellen. In der Kurmark z. B. 
war die Zahl der Feuerstellen in den Jmmediatstädten von 
10 000 auf 3000 Stellen gefallen! In Neustadt-Eberswalde 
waren von 216 Häusern 1648 nur noch 34 bewohnt. In Schwedt 
war die Zahl der Feuerstellen von 1625—1643 von 216 auf 
26, in Frankfurt a. O. von 1029 auf 272 gesunken, die Zahl 
der Einwohner in dieser Stadt von 11 000 auf weniger 
als 2000. Berlin hatte eigentlich „nur" 16 Kriegsjahre durch 
zumachen, und doch war am Ende des Krieges auch hier 
ein Viertel aller Wohnhäuser zerfallen und unbewohnt. Die 
Einwohnerzahl war auf kaum 8000 gesunken!