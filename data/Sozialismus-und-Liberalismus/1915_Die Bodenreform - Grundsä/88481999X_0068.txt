48 
Staatskartell führe, so daß nur noch die Gesamtheit Pro 
duktionssubjekt sei. Solche Darlegungen sind für weite kreise 
von überzeugender Kraft. Sorgfältiger Prüfung aber ent 
hüllen gerade sie den grundlegenden Irrtum des kommunisti 
schen Gedankengangs. 
Soweit Vereinigungen von Produzenten lediglich die 
Ausgaben für unfruchtbare Reklame und entbehrlichen 
Zwischenhandel ausschalten, umschließen sie natürlich 
keinerlei Gefahr für Arbeiter und Konsumenten, sind auch 
kein Hindernis für einen gesunden und notwendigen Wett 
bewerb. 
Von den Kartellen aber, die mit Recht als eine Gefahr 
in unserem Wirtschaftsleben empfunden werden, haben bis 
her allein diejenigen dauernd bestehen können, die irgend 
welchen Monopol besitz erlangen konnten, so daß ein 
Ausgleich ihnen gegenüber durch eine wirklich freie Kon 
kurrenz ausgeschlossen ist. 
Solche Vereinigungen finden sich z. B. in Amerika und 
Frankreich als Herren der großen Verkehrsmittel, der Eisen 
bahnen. Wichtiger noch sind diejenigen Kartelle, die sich in 
den Besitz der Naturschätze gesetzt haben. Wer sich die 
Kohlengruben eines Landes aneignet, der kann allerdings 
der gesamten übrigen Bevölkerung dieses Landes — soweit 
es die Weltkonkurrenz nicht hindert — seine Bedingungen vor 
schreiben, der kann bestimmen, mit welchen Unkosten die In 
dustrie eines Landes, ja, jeder einzelne Haushalt, rechnen 
muß. Die Petroleumquellen in den Händen der Rockefellers 
und Rothschilds bilden gleichfalls ein derartiges Monopol. 
Keine Arbeit kann den Besitz solcher Kartelle in genügen 
dem Maße ersetzen, und deshalb muß hier alles Verweisen