83 
„Acht Eigentümer haben ihren Besitz bereits mit Nutzen 
wieder verkauft; zwei Häuser haben schon den dritten, eins sogar den 
fünften Eigentümer!" 
Emen großartigen Versuch, der Wohnungsnot unserer 
wachsenden Jndustrieplätze durch Schaffung kleiner, „freier" 
Hauseigentümer zu begegnen, bietet die vielgenannte „Ar 
beiterstadt" in Mülhausen i. E. Trotz reicher Unter 
stützung durch Industrielle, durch die Gemeinde, durch die 
Regierung, hat sie mit einem Mißerfolge geendet. Die 
Räumlichkeiten wurden oft in gefährlicher Weise überfüllt, 
um möglichst viel Mietseinnahme zu erzielen. Bald wurde 
mit den Häusern Handel getrieben, und nach kurzer Zeit war 
nur noch ein kleiner Teil in den Händen der Leute, für die 
sie eigentlich bestimmt waren. 
Die zweite Art der Baugenossenschaften begnügt sich 
nicht damit, neue Hauseigentümer nach dem alten Typus 
zu schaffen. Sie will das Geschaffene dauernd vor Mißbrauch 
bewahren. Hier kommt allerdings alles auf die Sache, nichts 
auf die Form an. Legen die Mitglieder Wert darauf, 
Hausbesitzer zu werden, so mag man ihnen ruhig den Besitz 
von Ein- oder Zweifamilienhäusern einräumen, wenn nur 
jede mißbräuchliche Benutzung (durch Eintragung eines 
Wiederkaufsrechts usw.) ausgeschlossen ist. 
Wo ungünstige Verhältnisse ausnahmsweise zur Errich 
tung größerer Wohnhäuser zwingen, bleiben diese zweck 
mäßig Eigentum der Genossenschaft. Die Einzelnen erhalten 
dann ihre Wohn- und Werkstätten gleichsam als Heimstätten 
in den Genossenschaftshäusern, deren Miteigentümer sie sind. 
Die Schwierigkeiten der baugenossenschaftlichen Praris 
sind groß. Es sei nur an die gefährliche Klippe der Miete- 
6*