174 
eigenen Stand liebst". Nur wenn es allen erwerbenden 
Ständen gut geht, kann dauernde Besserung auch in dein 
eigenen erzielt und aufrecht erhalten werden. Nur wenn es 
unserer ländlichen Bevölkerung so gut geht, daß eine über 
mäßige Abwanderung in die Industrieorte nicht erfolgt, ist 
auch eine hohe Lebenshaltung der städtischen Bevölkerung 
und damit ein hochstehendes Wirtschaftsleben des ganzen 
Volkes möglich. 
Dazu kommt die ganz besondere nationale Bedeutung 
einer gesunden Landbevölkerung. Sie ist der Jungbrunnen 
des Volkes. Noch heute leben in Deutschland rund 26 Mil 
lionen Menschen auf dem Lande, und daß hier Kraft und 
Zucht in höherem Maße vertreten sind, als in dem lauten, 
aufreibenden Leben unserer Jndustrieorte, ist trotz vereinzelter 
Versuche niemals ernstlich bestritten worden. 
Während in weiten Gebieten der Industrie die Schichten 
der wirtschaftlich Selbständigen abnehmen und sich immer 
mehr Riesenbetriebe in wenigen Händen vereinen, zeigt sich 
in der Landwirtschaft der Mittel- und Kleinbetrieb dem 
Großbetrieb nicht nur ebenbürtig, sondern in mancher Be 
ziehung sogar überlegen. Die Entwicklungstendenzen in 
der Industrie finden also in denen der Landwirtschaft ein 
Gegengewicht, das vom nationalen und sozialen Stand 
punkt gleich bedeutsam erscheint, da es unserem Volke eine 
starke wirtschaftlich selbständige Mittelschicht sichert. 
Und noch ein Gesichtspunkt sei hervorgehoben. Jedes 
Volk hat nur einen Bauernstand. Man kann aus Land 
arbeitern Fabrikarbeiter machen. Aber man kann nur sehr 
schwer aus Fabrikarbeitern eine neue Landbevölkerung 
schaffen. Es ist ein verhängnisvoller Irrtum, der aus dem