120 
In geringerem Maße gilt dies von jeder Stadt Preußens und 
den Einzelstaaten. Nehmen Sie z. B. dieses Stuttgart. Stuttgart 
ist nicht das Prodult der Bevölkerung dieser einen Stadt allein, 
sondern des ganzen südlichen und mittelwestlichen Deutschlands; auch 
das flache Land hat dazu beigetragen. Tritt da nicht der Gedanke her 
vor: Der Wertzuwachs ist das Produkt der Arbeit der ganzen Be 
völkerung, und dementsprechend sollte auch die Gesamtheit Anteil haben 
an dem Wertzuwachs! 
Das führt dazu, daß wir anerkennen: Eine Zuwachssteuer ist 
ihrem Wesen nach eigentlich eine Reichssteuer!" 
Der Vorschlag rief zunächst manchen Widerspruch her 
vor. Aber der Gedanke ging seinen Weg. Und der stampf 
um die deutsche Reichsfinanzreform brachte ihn schnell in 
den Vordergrund des öffentlichen Interesses. Als am 
1. Mai 1909 in der Budgetkommission des Reichstags der 
Antrag gestellt wurde, die Verbündeten Regierungen zu er 
suchen, „ohne Verzug eine Gesetzesvorlage auszuarbeiten, 
die eine Besteuerung des Wertzuwachses am Boden vorsieht", 
da sah der Deutsche Reichstag das seltene Schauspiel, daß die 
Führer aller Parteien einstimmig diesen Antrag annahmen. 
Es war aber noch ein weiter Weg von der grundsätz 
lichen Zustimmung bis zur praktischen Verwirklichung. Als 
am 11. April 1910 der Reichsschatzsekretär Wermuth den 
Entwurf einer Reichs-Zuwachssteuer vorlegte und mit weitem 
Blick und festem Mut vertrat, entstand ein stampf, wie er in 
gleicher Heftigkeit wohl in Deutschland um eine Steuer noch 
nicht geführt worden ist. 
Der Bund Deutscher Bodenreformer hatte in ihm die 
Führung. Von ihm aus wurden etwa 800—900 Versamm 
lungen für die Steuer veranstaltet, 600 000 Flugblätter, 
12 000 „Kampfbroschüren", eine große Anzahl von „Denk-