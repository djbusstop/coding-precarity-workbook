277 
wachsenen Sohne ctleomertes die Hand reiche. Zuerst^wei- 
gerte sich die edle Frau, dem Knaben die Rechte des Ehe 
herrn einzuräumen. Dann aber tat sie es. In ihrer Brust 
erwachte die Hoffnung, dem geliebten Toten vielleicht einen 
Rächer erwecken zu können. Und es gelang ihr. Sie wußte 
in dem Herzen des Königssohnes die Ideale der neuen großen 
Zukunft des Volkes lebendig zu machen,' und auch des 
Kleomenes Mutter, das Weib des Leonidas, wurde von 
dieser Begeisterung erfaßt. So konnten die Bodenreformer 
Spartas noch einmal den Kampf um die Erneuerung ihres 
Vaterlandes wagen. 
Kleomenes war eine andere Natur als Agis. Wohl war 
auch er gerecht. Aber jene Weichheit, die Agis so verhäng 
nisvoll geworden war, fehlte ihm. Er berechnete kühl jede 
Möglichkeit und führte dann seine Pläne rücksichtslos durch. 
Schon im Jahre 236 v. Chr. wurde er König. Aber er sah ein, 
baß er zunächst ein Heer haben müsse, das ihm bedingungslos 
gehorche, da Waffengewalt allein doch das letzte Wort bei 
diesen Reformen spreche. Er führte glückliche Kriege. Unter 
der Hand schürte er das Feuer der Sozialreform. So konnte 
er es wagen, im Jahre 226 mit einem Teile seines Heeres 
plötzlich nach Sparta aufzubrechen. Die Führer seiner Feinde 
iieß er niederstoßen. Achtzig Oligarchen mußten in die Ver 
bannung gehen. Dann ließ er von neuem alle Schuld-Ur 
kunden verbrennen, und jetzt wurde die Bodenverteilung 
durchgeführt: 4000 neue Teile geschaffen, dabei auch An 
teile für die 80 Verbannten, und so ein neues Sparta auf 
gerichtet. Die Verfassung wurde geändert. Die Ephoren 
wurden beseitigt, das Königtum in seiner Macht gestärkt. 
Jubelnd folgte das Volk dem jungen Löwen, wie man