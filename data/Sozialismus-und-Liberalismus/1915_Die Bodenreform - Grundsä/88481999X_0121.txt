101 
gezwungen sind, sich bei Abgabe ihrer Grundstücke mit ge 
ringerem Gewinne zu begnügen als vorher. 
Einen Vorteil von dieser Reform haben: 
Die Besitzer von Häusern mit kleineren Wohnungen, 
denen die Gebäudesteuer erleichtert werden kann; 
Alle Einwohner, die Einkommensteuer zahlen; denn 
ein jeder spart (229—185=) 44 °/ 0 Steuerzuschlag; 
Die Maurer, Maler, Zimmerer, Dachdecker, Schlosser, 
Tischler, kurz, alle Handwerker, die an reger Bautätigkeit 
ihren Verdienst haben; 
Alle Mieter, für die auf dem billiger gewordenen Boden 
nun preiswerte Wohnungen möglich werden. 
^^eben der schärferen und gerechteren Heranziehung der 
^ C Bodenwerte zu den Lasten der Gesamtheit spricht für 
diese Reform eine wesentliche Erleichterung in der Be 
rechnung der Steuer. Den gemeinen Wert seines Eigentums 
in runden Zahlen anzugeben, vermag jeder ohne weiteres. 
In C ö l n fanden 1893 etwa 21 000 Veranlagungen 
nach dem Rutzungswert statt. Dagegen wurden 2703 Be 
rufungen erhoben. 1899 fanden etwa 30 000 Veranlagungen 
nach dem Maßstabe des gemeinen Wertes statt, und jetzt 
wurden nur 171 Einsprüche geltend gemacht. 
Diese Reform führte auch eine Verschiebung der Steuer 
lasten innerhalb der Hausbesitzerkreise herbei. Der Nutzungs 
wert mußte nach dem gesamten Mietsertrag bestimmt werden. 
Er ist bei den Häusern mit kleineren Wohnungen im Ver 
hältnis viel höher, als bei den Häusern mit großen Woh 
nungen oder bei den Villen zum Alleinbewohnen. Bei den