263 
Herrschaft in Athen aufzurichten. Da gelang es Solon, 
einem vornehmen Manne, der sich von dem Unrecht seiner 
Standesgenossen freigehalten hatte, durch ein hinreißendes 
Lied die Bürger zu einer letzten Kraftanstrengung zu bewegen. 
Unter seiner Führung wurde Salamis zurückerobert. 
Auf ihn richteten sich bald alle Hoffnungen des Volkes. 
Solon zögerte aber, in die inneren Wirren einzugreifen. Er 
fürchtete, nach seinem eigenen Worte, „den Eigennutz der 
einen und die übertriebenen Forderungen der anderen." 
Aber unterstützt von hochgesinnten Mitgliedern der alten 
Adels-Geschlechter, wagte er den schweren Schritt. 
In einer zweiten Elegie machte er sich zum Wortführer 
des attischen Volkes: 
„Nach dem ewigen Ratschluß des Zeus und dem Willen der 
seligen Götter wird unsere Stadt niemals untergehen, und die Tochter 
des gewaltigen Vaters, die athenische Pallas, hält hoch die Hand über 
ihr. Aber die Bürger selbst trachten in Torheit, die Stadt zu ver 
derben, von Habsucht verleitet. Die Führer des Volkes sind unge 
recht. Bald werden sie ihrer schweren Frevel harte Strafen büßen 
müssen! Sie wissen ihren Durst nach Geld und Gut nicht zu zügeln. 
Es genügt ihnen nicht, sich in Ruhe ihres reichen Besitzes zu freuen. 
Ohne Scheu selbst vor dem Besitz der Tempel und des Staates stehlen 
und rauben sie. Sie achten nicht die heiligen Satzungen der Dike, 
welche schweigend gewahrt, was geschehen ist und was geschieht. 
Aber es kommt die Stunde, in der sie naht, Vergeltung zu üben. 
Unheilbare Wunden sind der Stadt schon geschlagen. Mit 
raschen Schritten geht sie schnöder Sklaverei entgegen. Die Empörung 
droht auszubrechen und der schlafende Bürgerkrieg, der die fröhliche 
Jugend vieler dahinrafft. Im Zwiste der Bürger, den Gewalttätigen 
erwünscht, ist die vielgeliebte Stadt bald aufgerieben. Von den 
Armen sind viele verkauft, mit schmählichen Fesseln gebunden in 
ftemdes Land geschafft und müssen, der Gewalt gehorchend, die 
schweren Leiden der Sklaverei tragen. Das Unglück des Gemein-