125 
Daraus ergibt sich ohne weiteres die Forderung: Kein 
Schritt breit Staats- und Gerneinde-Boden darf bedingungs 
los der Privatspekulation ausgeliefert werden! 
Um das Widersinnige des bisherigen Vorgehens zu 
verstehen, braucht man nur verkauftes Eemeindegrund- 
eigentum in seiner Entwicklung zu verfolgen. Es ist oft 
genug vorgekommen, daß eine Stadt heute Boden verkauft, ' 
und nach verhältnismäßig kurzer Zeit für ein kleines Stück 
dieses Geländes, das sie für den Bau einer Schule usw. 
braucht, einen höheren Preis geben muß, als sie einst für 
das Ganze erhalten hat. 
Eine zweite Forderung tritt natürlich ergänzend hinzu: 
Staat und Gemeinde sollen jeden gangbaren Weg benutzen, 
um ihr Grundeigentum zu vergrößern. 
Ja, die Bodenreformer stehen nicht an, zu diesem Zweck 
ein Vorkaufsrecht für Staat oder Gemeinde, ja selbst 
eine Ausdehnung des Enteignungsrechtes zu ver 
langen. Wenn heute eine Eisenbahn oder ein Kanal ge 
baut werden soll, und ein Einzelner weigert sich, seinen 
Boden zu diesem Zweck herzugeben, so kann er dazu gegen 
Entschädigung gezwungen werden. Die Verkehrsbedürfnisse 
der Gesamtheit werden eben mit Recht höher bewertet als 
das wirkliche oder eingebildete Interesse eines Einzelnen. Sind 
aber die Wohnungsverhältnisse eines großen Teils unseres 
Volkes für das „öffentliche Wohl" nicht zum mindesten von 
derselben Bedeutung wie irgendwelche Verkehrswege? 
Die Grundzüge eines sozialen Enteignungsrechts hat der 
bekannte Erlanger Hochschullehrer Professor Paul O e r t - 
mann auf dem Dresdner Bodenreformtag entwickelt und 
im „Jahrbuch der Bodenreform" 1911 dargestellt.