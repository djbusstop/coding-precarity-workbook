291 
das Kreuz schlagen, um die übrigen durch solchen Schrecken 
im Gehorsam zu erhalten. 
Es war natürlich, daß sich in Rom trotz aller Verderbnis 
Ņîänner fanden, die den Ernst der Zeit erkannten und auf 
Abhilfe sannen. Zu ihnen gehörte in erster Reihe der an 
gesehenste Bürger Roms, der Zerstörer Karthagos, S cip io 
Africanus. Um ihn scharte sich ein Kreis ehrlicher, ein 
flußreicher Männer. Hier erkannte man zuerst, daß eine 
gründliche Bodenreform die Voraussetzung aller Besserung 
fein müßte, und Eajus Lälius, Scipios bester Freund, 
^>ar es, der als Konsul zuerst im Jahre 140 v. Chr. den 
Vorschlag machte, durch eine neue Vergebung des Ge 
meindelandes dem schwer darniederliegenden römischen 
Dauernstand Hilfe zu bringen. Aber als er sah, welchen Sturm 
ev mit diesem Antrage bei den Optimaten erregte, zog er 
seinen Vorschlag zurück. Lälius wurde ob dieses Zurück- 
zeichens von den herrschenden Schichten mit dem Ehren- 
öeinamen „der Verständige" ausgezeichnet. Aber es gab 
^och manchen ernsten Mann, der es bedauerte, daß man 
so leichten Herzens auf die Bodenreform verzichtet hatte, 
und im Vertrauen auch auf die Hilfe solcher Männer wagte 
ein Jüngling das, wovor der Mann zurückgeschreckt war. 
3. Tiberius Gracchus, 
y? " iberius Gracchus war es, der sich im Jahre 133 
vor Christo in der festen Absicht um das Tribunal 
bewarb, die Bodenfrage einer Lösung entgegenzuführen. 
^ gehörte einem der ersten Geschlechter Roms an. Seine 
Cutter Cornelia war die Tochter des berühmten Scipio 
Africanus, der einst Hannibal niedergezwungen hatte. Ihr 
19*