333 
hätte er dieselbe Sicherheit, das Grundstück zu behalten, das 
er jetzt hat, während er, wenn er mehr Grundstücke brauchte, 
sogar hinsichtlich der Neuanlagen gewinnen würde. Ob 
gleich er mehr Steuern für seinen Boden zahlen würde, 
ginge er für sein Haus, seine Verbesserungen, sein ganzes 
Persönliches Eigentum, für alles, was er ißt, trinkt und 
braucht, frei aus, während sein Verdienst sich bedeutend ver 
mehren würde. Auf dem flachen Lande würde die Bevölkerung 
dichter werden, während sie in den Großstädten abnähme. 
Die Erträgnisse der Steuer ließen sich auf tausenderlei Weise 
dem Gemeinwohl dienstbar machen. 
4. Kapitel. In der Staatsverwaltung würde die 
größte Reinheit, im gesellschaftlichen Leben die gesunde Ent 
wicklung aller edlen Keime möglich werden. 
X. Buch. Das Gesetz des menschlichen Fortschritts. 
1. Kapitel. Wenn unsere Lehre richtig ist, so wird 
sie unter ein größeres Gesetz fallen. 
Welches ist das Gesetz des menschlichen Fortschritts? 
Nach der herrschenden Ansicht kommt der Unterschied 
Zwischen dem zivilisierten Menschen und dem Wilden von 
einer langen Rassenerziehung her, die in der geistigen 
Organisation dauernd zum Ausdruck kommt,- diese Ver 
vollkommnung führt in steigendem Verhältnis zu einer 
immer höheren Gesittung. Diese Lehre erklärt aber die 
-Zivilisationen nicht, die weit vorgeschritten waren und dann 
Zum Stillstände gekommen sind, noch die, welche zurück 
gegangen sind. Jede bisherige Zivilisation hatte ihre Zeit 
bes kräftigen Wachstums, des Stillstands, des Sinkens und 
Fallens. Die Erde ist das Grab der toten Reiche nicht weniger