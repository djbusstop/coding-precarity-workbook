281 
Schwelger. Als Kleomenes über das unmännliche Treiben 
am Hofe seinem Unmute Ausdruck gab, wurde er von den 
Günstlingen des entarteten Königs verdächtigt und mit seinen 
Gefährten in einer Burg gefangen gehalten. Um nicht in 
diesem Verier langsam zu verderben, beschloß der Spartaner- 
könig einen verzweifelten Schritt. Er befreite sich und seine 
Genossen, sprengte auf die Straße und rief das Volk zur 
Freiheit auf. Doch wer sollte in den Straßen von Aleran 
dria diese Sprache verstehen? Niemand folgte ihnen. Da 
gaben sie sich selbst den Tod. An der Seite des Königs fiel 
sein Liebling, der junge Panteus. Des Königs Mutter und 
seine Kinder, sowie die Frauen der Spartaner wurden darauf 
durch Henkershand getötet. 
So starben die letzten Bodenreformer Griechenlands. 
Aber auch ihre Feinde sollten nicht lange triumphieren. 
Das Land wurde wieder von Bürgerkriegen zerrissen. 
In Sparta gelang es Abenteurern, von Zeit zu Zeit 
die Herrschaft an sich zu reißen und ein Schreckensregiment 
aufzurichten. Zwei Menschenalter später zertrat der Schritt 
der römischen Legionen die letzten Reste hellenischer Freiheit. 
Korinth, dessen Oligarchen am meisten zum Landes 
verrat beigetragen hatten, indem sie ihre Burg den Make 
doniern öffneten, wurde dem Erdboden gleich gemacht und 
alle Einwohner in die Sklaverei verkauft. Die Söhne jener 
Ņornehmen konnten nun unter der Peitsche italischer Sklaven 
vögte und die Töchter unter den raffinierten Grausamkeiten 
verderbter römischer Frauen über das alte Wort nachdenken, 
daß Gerechtigkeit, auch soziale Gerechtigkeit, das Fundament 
bildet, auf dem allein ein freies Staatswesen fest gegründet 
werden kann.