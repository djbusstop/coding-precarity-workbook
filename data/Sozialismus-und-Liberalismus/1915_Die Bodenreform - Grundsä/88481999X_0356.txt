336 
tigen Leben ist natürlich und tief. Aber der großen Mehrheit 
der Menschen scheint es eine eitle Hoffnung. Die Gedanken, 
welche die Hoffnung auf ein künftiges Leben vernichten, 
haben ihre Quelle in jenen Lehren der politischen und so 
zialen Wissenschaft, daß mehr menschliche Wesen ins Leben 
treten, als versorgt werden können, daß Laster und Elend 
in der Natur und ihren Gesetzen begründet seien. Wir haben 
diese Lehren bekämpft und ihre Irrtümer enthüllt. Dadurch 
verscheuchen wir den Alb, der aus der modernen Welt den 
Glauben an ein künftiges Leben verbannt. Alle Schwierig 
keiten sind nicht beseitigt. Denn wohin wir uns auch wenden 
mögen, wir stoßen auf Dinge, die wir nicht verstehen können. 
Aber Schwierigkeiten find gehoben, die beweiskräftig und 
unüberwindlich schienen. Und so bricht die Hoffnung an — 
die Hoffnung, die das Herz aller Religionen ist. Die Dichter 
haben sie besungen, die Seher haben sie verkündet, und in 
seinen tiefsten Pulsen pocht das Herz des Menschen ihrer 
Wahrheit entgegen! 
şş>ie Henry George zu der Erkenntnis gekommen ist, der 
er in „Fortschritt und Armut" so glänzenden Ausdruck 
verlieh, hat er selbst einmal erzählt: „Als ich Ende der sech 
ziger Jahre von Kalifornien wieder nach unseren Oststaaten, 
nach New Pork, gekommen war, hat mein tiefstes Innere die 
Erscheinung erregt, daß ich überall dort eine Zunahme der 
Armut fand, wo die Industrie am meisten fortgeschritten 
war. Diese Frage hat mich von da an dauernd beschäftigt. 
Als ich eine kleine Zeitung in dem Städtchen Oakland heraus 
gab, kam eine Erklärung dieses Problems, einer Offen 
barung gleich, über mich. Ich ritt einmal aus. In Gedanken