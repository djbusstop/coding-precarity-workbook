465 
städtischen Grundstücke nicht verleiten lassen wollen, die Wege zu ver 
lassen, welche uns nach allgemeinen wirtschaftlichen Grundsätzen an 
gewiesen sind, so glauben wir doch, unter den zulässigen Wegen ge 
rade den wählen und empfehlen zu müssen, welcher den Druck der 
augenblicklichen Spannung für die Obdachsuchenden am billigsten 
zu verteilen und die harten Konsequenzen der jetzigen Uebergangszeit 
am meisten zu mildern verspricht. Wir glauben, daß dies der Weg 
der Verpachtung auf längere Zeit zum Zwecke und unter der Be 
dingung sofortiger Bebauung ist, für welchen auch der Umstand spricht, 
daß er das Bauen erleichtert, insofern die Kapitalanlage für den 
Grund und Boden erspart wird." 
Diese Ausführungen gewähren wohl das beste Bild der 
herrschenden Anschauungen jener Zeit. Aber alles Rücksicht 
nehmen auf die Privatspekulation half nicht. Die Berliner 
Stadtverordnetenversammlung stimmte dem Vorschlag ihres 
Oberbürgermeisters nicht zu. Ihre Mehrheit stand wohl auf 
dem Standpunkt des „Jahresberichts für Hypotheken und 
Grundbesitz pro 1871", den E. Salomon am 20. Januar 
1872 erscheinen liest, und der mit Freude feststellte: 
„Das verflossene Jahr kann wohl als eins der ergiebigsten und 
günstigsten für den sogenannten Realkredit und Jmmobilienverkehr 
der letzten zehn Jahre bezeichnet werden. . .. 
Gleich nach Friedensschluß trat eine bedeutende Nachfrage 
nach Grundbesitz ein. Der bedeutende Zuzug von Kapitalisten nach 
hier, der Bedarf großer Räumlichkeiten für die in nicht unbedeutender 
Zahl gegründeten neuen Institute, die wenigen Neubauten in den 
letzten vier Jahren, haben einen Mangel an Wohn- und Geschäfts 
räumen hervorgerufen, dessen Folge eine ganz enorme Steigerung 
der Mieten war. Die natürliche Folge mußte eine Steigerung 
des Grundwertes sein, und rief die Spekulationslnst wach . .. 
Eine ganz natürliche Folgerung der Steigerung in Grundstücken 
war die Steigerung des Grund und Bodens und haben die darin 
stattgefnndenen Umsätze zu steigenden Preisen einen ganz enormen 
Umfang angenommen. Es zeichneten sich wiederum hierbei die 
Damaschke, Bodenreform, 31.—35. Tausend. 30