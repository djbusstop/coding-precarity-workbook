298 
weiß niemand. Am nächsten Morgen war Scipio Africanus 
tot. Ob er in jener Nacht von einem Parteigänger derGracchen 
erwürgt worden ist, ob seine Gemahlin selbst, die Schwester der 
Gracchen, an dieser Tat einen Anteil hat, oder ob ein Schlag 
anfall in jenen Tagen höchster seelischer Aufregung seinem 
Leben ein Ende gemacht hat — man wußte es nicht, und 
es gelüstete niemand, nicht Optimalen, nicht Volkspar 
teiler, den Schleier zu heben. Das entstellte Gesicht blieb 
verhüllt, und die Flammen, die seinen Leib verzehrten, ver 
schlangen das Geheimnis seines Todes. 
4. Eajus Gracchus. 
beste Mann der Volkspartei, der jüngere Bruder des 
Tiberius, Gajus Gracchus, weilte fern von Rom. Der 
Senat hatte ihn als Quästor nach Sardinien geschickt und, 
indem er die Amtsdauer des vorgesetzten Konsuls wiederholt 
verlängerte, auch den gefürchteten Quästor dort festgehalten. 
Aber Eajus Gracchus kehrte auch ohne Ablösung nach 
Rom zurück. Das Volk empfing ihn jubelnd, sprach ihn von 
der Anklage, seinen Posten widerrechtlich verlassen zu haben, 
frei und wählte ihn im Jahre 123 v. Chr. zum Volkstribunen. 
Umsonst war die Warnung „verständiger" Freunde, um 
sonst auch, was wohl am schwersten wog, die Bitte der 
verehrten Mutter, die der furchtbare Tod ihres ältesten 
Sohnes doch tief erschüttert hatte: 
„Ich könnte es auf mich nehmen, einen feierlichen Eid zu schwören, 
daß außer den Mördern des Tiberius niemand mir soviel, Kummer 
und Leid gemacht hat, wie Du wegen dieser Dinge. Und doch hättest 
Du die Pflichten all der Kinder, die ich schon gehabt habe, auf Dich 
nehmen und dafür sorgen müssen, daß ich möglichst wenig Kummer 
im Alter habe, daß Du alle Deine Pläne besonders meinen Wünschen