478 
diese den Weg der Landgewinnung für Kriegerheimstätten 
in außerordentlichem Maße! 
So gibt es mancherlei gute Antwort auf die Frage nach 
dem nötigen Land für die Heimstätten, auch wenn man 
zunächst von allem Neuland absieht, das dieser Arieg 
uns an den Grenzen oder in Kolonien erschließen 
mag, und dessen Besiedlung durch Kriegerheimstätten natur 
gemäß eine besoirdere nationale Bedeutung gewinnen muß! 
Auch in der Geldfrage wollen die Andeutungen 
der „Grundsätze" natürlich in keiner Weise erschöpfend sein. 
Der Reichswohnungsfürsorgefonds mit sei 
nem Jahresetat von 4 Millionen M konnte bis jetzt in der 
Tat nur 8 Millionen M zum Ankauf von Boden verwenden, 
der dann in Erbbaurecht ausgegeben wurde. Das ist eine 
kleine Summe, ebenso wie die 25 Millionen M, die der 
Reichsbürgschaftsfonds bisher aufweist — aber 
was hindert es, daß beide Summen vervielfacht werden? 
Der Krieg hat uns gelehrt, mit sehr großen Zahlen zu rechnen, 
und wenn Hunderte von Millionen aufgewandt werden, um 
unsere Waffen scharf zu erhalten, dann werden mit mindestens 
dem gleichen Recht die gleichen Summen aufgewandt werden 
müssen, um die deutscher! Herzen freudig und um die deut 
schen Hände kräftig werden zu lassen, um jene Waffen im 
Notfälle zu gebrauchen. Gewiß sind die beiden Einrichtungen 
bisher nur für Arbeiter und Angestellte „im Dienst des Rei 
ches" bestimmt; aber hat dieser Krieg uns nicht gelehrt, daß 
jedes Glied unseres Volkes zuletzt ein Reichsangestellter ist? 
Die Reichspfandbriefanstalt, die die „Grund 
sätze" außerdem fordern, ist gedacht als die Vermittlern, die 
alle Sammelstellen der Sparkraft unseres Volkes für den