247 
abgesehen von irgendwelchen radikalen politischen und reli 
giösen Tendenzen, rein als wirtschaftliche Richtung betrachtet) 
mit ihrer Verstaatlichung aller Produktionsmittel und der 
damit notwendig gegebenen Aufhebung der freien Ent 
wicklung der Persönlichkeit abgestoßen werden muß." 
Friedrich Naumann ist wegen seiner weitver 
breiteten und in vielen kreisen hochgeschätzten Andachten: 
„Gotteshilfe" von der Universität Heidelberg zum Ehren 
doktor der Theologie ernannt worden. 
Ist es nicht eine wahre „Bodenreform-Andacht", wenn 
es dort heißt: 
„Wehe denen, die ein Haus an das andere ziehen und 
einen Acker zum andern bringen, bis daß kein Raum mehr 
da sei, daß sie allein das Land besitzen. (Jesaja 5, 8.) 
Das Land gehört dem Leben des Volkes. Es ist dazu 
da, daß Menschen auf ihm atmen, arbeiten, wohnen, spielen 
und sich tummeln sollen. Land, das nicht in irgendeiner 
Weise Menschen zum Leben dient, daß sie Luft und Licht 
und Raum um sich herum haben, hat seinen Wert verloren. 
Damit ist nicht gesagt, daß es nicht in Privatbesitz sein könnte; 
aber es ist gefordert, daß der Privatbesitz an Land kein Hinder 
nis der Entwicklung aller Volksgenossen werden darf. Das 
ist die Ansicht eines so alten ehrwürdigen Propheten wie 
Jesaja. 
Natürlich sind wir über Jesaja weit hinaus. Wir er 
tragen es, daß große Landstriche in einzelnen Händen und 
viele Bauplätze in der Macht weniger Gesellschaften sind. 
Es empört uns nicht mehr, wenn wir in den Außenteilen 
der großen Städte die unbebauten Grundstücke liegen sehen, 
auf denen Hunderte und Tausende von Familien wohnen