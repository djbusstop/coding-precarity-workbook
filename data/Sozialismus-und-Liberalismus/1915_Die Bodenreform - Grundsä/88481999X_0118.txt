C eider ist in der deutschen Steuerpraris eine scharfe Tren 
nung zwischen Boden und Gebäude nur schwer durch 
führbar. Das Preußische Kommunalabgabengesetz vom 
14. Juli 1893, das für die Gemeindegesetzgebung auch in 
anderen deutschen Staaten vielfach vorbildlich geworden ist, 
faßt noch die Begriffe Grund- und Gebäudesteuer als Ein 
heit auf. Trotzdem ermöglicht dieses Gesetz einen wesent 
lichen Fortschritt, indem es in § 25 die Umlegung der Steuern 
vom Grundbesitz nicht nur, wie bisher, nach dem Nutzungs 
wert, sondern auch nach dem gemeinen Werte gestattet. 
Die große Mehrzahl der Gemeinden hat von diesem 
Recht allerdings noch keinen Gebrauch gemacht. Sie erhebt 
die Steuer noch immer nach dem Nutzungswert. Diese 
Umlegung wirkt geradezu wie eine Prämie auf den Boden 
wucher. Welchen Nutzungswert soll eine städtische Baustelle 
als unbebautes Stück Land besitzen? 
Im Januar 1899 sprach ich über die Wohnungsfrage 
in Dortmund. In der Aussprache wünschte ein Stadtver 
ordneter Wege zur praktischen Bekämpfung der Spekulanten 
zu erfahren, die sich der wertvollsten Baugelände bemächtigt 
hätten, und diese nun von der Bebauung zurückhielten. Ein 
Herr aus Breslau erklärte z. B., daß er eine Baustelle, die er 
billig erstanden, nicht eher verkaufe, als bis man ihm 500 000 
M dafür gäbe. Ich fragte: „Was leistet dieser auswärtige 
Spekulant an Steuern für die Gemeinde, von deren Wachsen 
er viele Tausende an Gewinn erwartet?" Die etwas klein 
laute Antwort war: „Wir haben ihn so hoch wie möglich 
eingeschätzt; aber als Kartoffelland konnte dies Baugelände 
eben nicht höher herangezogen werden als mit 3 M Steuern." 
Ich wies auf den § 25 des Kommunalabgabengesetzes und