100 — 
konnten in der Steuer entlastet werden. Im Jahre 1901 
hatten die Gemeinde-Zuschläge zur staatlichen Einkommen 
steuer 200 % betragen. Das Jahr schloß aber mit einem 
Fehlbetrag ab. Es war ein größerer Steuerertrag erforder 
lich. Nach dem bisherigen Steuermodus hätten die Zu 
schläge zur Einkommensteuer im Jahre 1902 etwa 229 % 
betrogen müssen. Durch die Einführung der Grundwert 
steuer aber wurde es möglich, trotz des erhöhten Geldbedarfs 
die Zuschläge zur Einkommensteuer für 1902/03 auf 185 % 
herabzusetzen. 
Die weiteren Wirkungen schildert die „Berliner Morgen 
post": „Die Grundbesitzer hatten gefürchtet, daß die neue 
Steuer auswärtige Spekulanten von Spandau künftighin 
fernhalten würde, so daß kein Landverkauf mehr zustande 
käme. Nun ist aber gerade das Gegenteil eingetroffen. In 
der kurzen Zeit seit Einführung der Erundwertsteuer, seit 
dem 1. April, sind in Spandau so viele Terrainverkäufe abge 
schlossen worden, wie seit Jahren nicht, und die Nachftage 
ist noch immer sehr rege. Erleichtert werden die Geschäfte 
allerdings durch den Umstand, daß verschiedene Spandauer 
Eigentümer ihre unbebauten Ländereien jetzt lieber ver 
kaufen als früher, nur um die Grundwertsteuer nicht zahlen 
zu brauchen. Sie sind auch mit ihren Preisforderungen etwas 
heruntergegangen, während sie vordem durch allzu hohe Preise 
von vornherein meist jedes Geschäft unmöglich machten. Die 
Bautätigkeit ist deshalb hier auch eine recht rege 
geworden." 
Einen Nachteil von der bodenreformerischen Regelung 
der Grund- und Gebäudesteuer haben die Grundstücksspeku 
lanten, die nun mehr Steuern zahlen müssen und dadurch