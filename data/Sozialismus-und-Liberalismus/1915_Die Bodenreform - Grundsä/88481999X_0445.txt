425 
Bei der Vergebung des „Mir" forderten sie eine Besei 
tigung des Zerstückelungswesens. Damit jede Familie von 
jeder Bodenart Gleiches erhalte, war im Laufe der Zeit 
vielfach eine Zerstückelung einzelner Anteile eingetreten, die 
jede wirtschaftliche Ausnutzung äußerst erschwerte. So kamen 
im Kreise Uglitsch im Gouvernement Jaroslaw im Durch 
schnitt auf ein Familienoberhaupt 36 Streifen Landes, in 
einigen Gemeinden wurde die Zahl 120 erreicht. Im Gou 
vernement Kursk gab es einen Besitz, der in 171 Teile zer 
stückelt war. Die Zusammenlegung konnte natürlich er 
folgen, ohne das Wesen des „Mir" aufzugeben, ja sie war 
sogar hier einfacher, als wenn der Boden Privatbesitz ge 
wesen wäre. Es kam nur darauf an, die Bauern davon zu 
überzeugen, daß nicht das Flächenmaß als Grundlage einer 
gerechten Verteilung gelten könne, sondern der Wert des 
Anteils, bei dem neben der Fruchtbarkeit auch die Lage des 
Bodens von maßgebender Bedeutung ist. Endlich empfahlen 
sie eine Ausgabe des Landes auf Lebenszeit, unter Hinweis 
darauf, daß in dieser Weise die Vergebung von Allmend 
teilen in Deutschland häufig mit größtem landwirtschaft 
lichen Erfolge vor sich gehe. 
Eine Zeitlang wurde diese Richtung auch von der Re 
gierung unterstützt. Meine „Aufgaben der Eemeindepolitik", 
das praktische Handbuch der deutschen Bodenreformer, wur 
den ins Russische und ins Finnische übersetzt. Die russische 
Ausgabe — die in einer besonderen Volksausgabe weit ver 
breitet wurde — erhielt eine erhöhte Bedeutung durch ein 
Vorwort O s e r o f f s , das die russische Intelligenz auf 
forderte, sich mit diesem Gedanken vertraut zu machen, und 
durch ihre Anwendung Rußland auf den Weg organischer