21 
Selbstzucht, sondern das Wohlwollen ist, nach den unentrinnbaren 
Gesetzen der Natur und nicht nach einem Fehler der menschlichen 
Einrichtungen in sehr kurzer Zeit zu einer Gesellschaft entarten würde, 
ähnlich derjenigen, die heute in allen bekannten Staaten obwaltet, 
zu einer Gesellschaft, die in eine Klasse von Eigentümern und in eine 
Klasse von Arbeitern zerfällt, und deren Haupttriebfeder die Selbst 
sucht ist." 
Malthus hat auch den Versuch gemacht, die verschieden 
artige Tendenz der Zunahme der Bevölkerung und der Unter 
haltsmittel zahlenmäßig darzustellen (Buch I, Kap. 1): 
„Angenommen daß die gegenwärtige Bevölkerung 1000 Mil 
lionen betrage, so würde die Vermehrung des Menschengeschlechts in 
folgender Weise vor sich gehen: 1, 2, 4, 8, 16, 32, 64, 128, 256, und 
die der Lebensmittel wie: 1, 2, 8, 4, 5, 6, 7, 8, 9. In zwei Jahr 
hunderten würde die Bevölkerung zu den Lebensmitteln im Verhältnis 
von 256 zu 9 stehen; in drei Jahrhunderten von 4096 zu 13, und 
es ist beinahe unmöglich, den Unterschied für 2000 Jahre überhaupt 
zu berechnen." 
Einen Beweis für die Richtigkeit dieser Aufstellung 
findet Malthus namentlich auch in der Entwicklung der nord 
amerikanischen Kolonien. Wie töricht ist es aber, derartige 
Zahlenreihen damit begründen zu wollen, daß vielleicht die 
ersten zwei oder drei Zahlenverhältnisse übereinstimmen! 
Einem Hunde wächst der Schwanz im Anfang verhältnis 
mäßig schneller als später. Wenn man sich den Schwanz 
nach den Verhältnissen, die in den ersten Tagen des Hunde 
lebens gelten, in gleicher Weise fortwachsend denkt, so kommt 
man dahin, „wissenschaftlich" festzustellen, daß ein Hund bei 
einem Körpergewicht von 50 Pfund einen Schwanz haben 
müßte, der eine englische Meile lang wäre. Die Wissenschaft 
müßte demgemäß auf die Notwendigkeit hinweisen, irgend 
wie eine derartige „Überschwanzlänge" zu verhindern!