484 
Glänzen, den Blick nicht lassen von dieser Frage, die, un 
beirrt durch alle Strömungen und Meinungen des Tages, in 
dieser einen Grundbedingung deines Lebens still aber fest 
ihre Schuldigkeit tun, damit deutsches Wesen seine großen 
Kulturausgaben in der Welt erfüllen kann, und es durch 
alle Zeiten hindurch Wahrheit bleibe, was in diesen Tagen 
so oft von den Lippen unserer Jugend in Not und Sturm 
erklang: 
O Deutschland hoch in Ehren!