134 
Eigentum zu sitzen, d. h. solange, wie das Erbbaurecht in 
der Regel währt. 
Daß eine getrennte Behandlung des Bodens und der 
Gebäude, d. h. dessen, was die Natur bietet, und dessen, was 
die Menschen schaffen, durchaus möglich ist, zeigt ein Blick 
auf London, die größte Stadt der Welt, wo diese Trennung 
die Regel bildet, und wo das „loase-liold"-System dahin ge 
führt hat, das Einfamilienhaus als den herrscheirden Typus 
zu bewahren. Es besteht dabei allerdings vom bodenreforme- 
rischen Standpunkt der schwere Fehler, daß der Boden nicht 
Eigentum der Gemeinde oder des Staates ist, sondern daß 
die ganze ungeheure Pacht den wenigen „landlords" von 
London zufließt, deren Verwalter natürlich auch bei den ein 
zelnen Pachtverträgen und ihrer Erneuerung nicht das Ge 
meinwohl, sondern allein den privat-kapitalistischen Vorteil 
ihrer Auftraggeber wahrzunehmen berufen sind. 
Die hypothekarische Beleihung der Erbbauhäuser bietet 
zurzeit noch Schwierigkeiten; aber es ist zu hoffen, daß eine 
Weiterbildung des Rechts hier sichere Grundlagen auch für 
mündelsichere Anlagen bieten werde. 
Da das Erbbaurecht nach einer gewissen Anzahl von 
Jahren erlöschen muß, so ist für diese Verhältnisse die Amor 
tisations-Hypothek das Gegebene, d. h. das Darlehen, das 
nicht mit einem Male, sondern in jährlichen Zuschlägen zu 
den Zinsen getilgt wird. Wer das schwierige mrd überaus 
wichtige Gebiet der deutschen Realverschuldung kennt, der 
weiß, wie wichtig eine Neubelebung dieser Hypothekenart 
wäre, die zu regelmäßiger Schuldenabtragung zwingt. 
Im Juli 1906 hat die Landesversicherungsanstalt der 
Rheinprovinz beschlossen, bis zu 75 % der Baukosten auf