In Bayern, rechts vom Rhein, betrug der Zuwachs 
Preußen hat im Jahre 1902 die Eesamtverschuldung 
seiner Landwirtschaft auf 7 842 761588 M angegeben. Bei 
ihrem schnellen Wachstum muß heute mit einer Schulden 
summe von rund 15 000 000 000 M gerechnet werden, die 
die preußische Landwirtschaft zu tragen hat, — eine unge 
heure Last, deren steigendes Gewicht auf die Dauer alles 
gesunde Leben erdrücken muß. 
Mit Recht erklärte Professor S e r i n g deshalb am 
6. Februar 1896 im deutschen Landwirtschaftsrat: 
„Die Schuldenerleichterung und Schuldenentlastung ist nicht nur 
als eines der Abhilfemittel für den gegenwärtigen Notstand anzu 
sehen, sondern sie bildet, im Verein mit der mit ihr notwendig ver 
knüpften Reform des Agrarrechts, den Kern aller agrarischen Sozial 
politik!" 
Überall erwacht das Verständnis für die grundlegende 
Bedeutung dieses Problems. Aus der schweizerischen Boden 
reformbewegung, deren Gründer die auch um das Genossen 
schaftswesen hochverdienten Männer Landrat Stephan 
G s ch w i n d in Oberwil und Professor Schär in Zürich (jetzt 
in Berlin) waren, ist der „Bauern- und Arbeiterbund Basel- 
Land" hervorgegangen, der sich aus allen politischen und 
religiösen Parteien zusammensetzt und als Hauptziel an 
die Spitze seiner Forderungen stellt: 
1895 
1896 
1897 
des Ver 
sicherungswertes 
93,3 
114,6 
149,0 
der Hypothekar 
verschuldung 
129,64 
165,46 
227,39 Millionen J(.