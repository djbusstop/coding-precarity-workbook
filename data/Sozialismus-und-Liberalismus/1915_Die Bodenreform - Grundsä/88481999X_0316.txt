296 
festesten und treuesten Anhänger des Tribunen, waren auf 
dem Felde beschäftigt. Aber auch diesmal stimmten die ersten 
Bezirke für Gracchus. Wieder erfolgte der Versuch, die Wahl 
handlung zu sprengen. Der Senat hatte sich im Tempel der 
Treue versammelt. Die erbittertsten Optimaten führten das 
Wort. Aber der Konsul Scaevola, ein gemäßigter und der 
Reform wohlgeneigter Mann, ließ sich zu keinem Einschreiten 
bewegen. Da rief Scipio Nasica, ein erbitterter Gegner 
des Gracchus, seine Freunde auf, sich zu bewaffnen, wie 
sie könnten, und ihm zu folgen. Mit knüppeln und Stuhl 
beinen versehen, stürzten die vornehmen Senatoren in die 
Menge hinein, die scheu beiseite wich. Tiberius strauchelte. Ein 
Schlag mit einem Knüppel traf ihn auf die Schläfe. Mit ihm 
wurden 300 seiner Anhänger erschlagen. Am Abend wurden 
die Toten in den Tiber gestürzt. Vergebens flehte Cornelia 
um die Leiche des Sohnes. Der Senat erklärte, daß eine 
Verschwörung zur Aufrichtung der königlichen Gewalt von 
Gracchus versucht worden, und daß deshalb sein Tod das 
verdiente Ende eines Staatsverbrechers gewesen sei. 
Einen solchen Tag hatte Rom noch nie gesehen. Selbst 
der Schwager des Ermordeten, Scipio Africanus, fand nicht 
den Mut, die Tat offen zu mißbilligen, als er jetzt als sieg 
reicher Feldherr aus Spanien zurückkehrte. Er begnügte 
sich mit der zweideutigen Antwort: „Wenn Tiberius wirklich 
nach der königlichen Würde gestrebt hat, ist er mit Recht be 
kämpft worden." 
Aber wenn man auch die Person getötet hatte, das 
Werk wagte man zunächst nicht anzutasten. Die Einziehung 
der Staatsländereien und ihre Aufteilung in kleinere Lose 
begann wirklich. Bereits der Konsul des nächsten Jahres