36 
Boden ihres Vaterlandes überhaupt wohnen und arbeiten 
zu dürfen! 
Und ist es nicht auch auf dem Gebiete der Industrie ähn 
lich? Man nenne einen einzigen nützlichen Gegenstand, der 
wirklich in der ganzen Welt überproduziert wird, d. h. von 
dem so viel hergestellt wird, daß keine Familie mehr dafür 
eine zweckmäßige Verwendung fände! 
Was in unserer Zeit Überproduktion genannt wird, ist 
nichts anderes als Unterkonsumfähigkeit. Wenn 
nur die Menschen das erwerben könnten, wofür sie nützliche 
Verwendung haben, wie schnell würde alle „Überproduktion" 
verschwinden! Deutschland zählt etwa 13 Millionen Fami 
lien. Im Durchschnitt wird jede Familie leicht 3 M täg 
lich mehr als bisher nützlich oder angenehm ausgeben können, 
ohne der Verschwendung geziehen zu werden. Denken wir 
nur an die Millionen von Familien, die unter 900 JÍ 
Jahreseinkommen haben, so ließen sich diese 3 M 
Mehrausgabe wohl unschwer erhöhen. Aber nehmen wir 
nur 3 M täglich Mehrausgabe, so würde das an den 
365 Tagen des Jahres die Summe von 14235 Millionen^ 
ergeben, für die in Deutschland mehr Ware gekauft und 
verbraucht werden könnte als jetzt, d. h. mehr, als die ganze 
große, glänzende und viel beneidete Ausfuhr des Deutschen 
Reiches betrug! 
So bedeutet denn das Wort Aberproduktion, in seinem 
Wesen erfaßt, nichts anderes als die Frage: wo liegt der 
Fehler in unserem Wirtschaftsleben, daß trotz der leichten 
Herstellungsmöglichkeit aller Waren, trotz der gefüllten Lager 
räume, doch der größte Teil der Bevölkerung von den Er 
zeugnissen menschlicher Tätigkeit nicht genügend erhalten