114 
willig dem Golde beugt, war es „natürlich", daß die beste 
Gesellschaft mit diesem Manne verkehrte, daß z. B. ein Prinz 
von Thurn und Taris Arm in Arm mit ihm spazieren ging. 
Vor Gericht ergab sich, daß dieser Kaufmann bereits achtmal 
im Gefängnis oder Zuchthaus gesessen hatte. Der Präsident 
glaubte daraus schließen zu dürfen, daß dieser Mann keine 
Zeit gehabt habe, auf ehrliche Weise die Mittel zu seinem 
glänzenden Haushalt zu beschaffen, und daß er deshalb 
das Spiel in jenem Klub als Erwerbsquelle benutzt habe. 
Aber mit sittlicher Entrüstung konnte der Angeklagte diesen 
Verdacht zurückweisen: „Ich kann aus meinen Büchern 
nachweisen, daß ich jährlich 60 000 M ehrlich verdiene. 
Ich bin Grundstückshändler. Wenn ich einen Bauplatz 
irgendwo gekauft habe, muß ich natürlich warten, bis sich 
die Gegend gehoben hat. Wo ich das abwarte, ist gleich 
gültig." 
Kam der Mann aus dem Gefängnis, dann hatte die 
Stadt vielleicht eine Schule in jener Gegend gebaut, oder 
eine neue Straßenbahnlinie war dorthin gelegt, oder irgend 
etwas anderes war auf Kosten aller Steuerzahler dort 
an Verbesserung geleistet worden, was diesem Mann eine 
„ehrliche" Einnahme von 60 000 M ermöglichte. Ist das 
„von Rechts wegen" im deutschen Volke möglich, dann muß 
die ehrliche Arbeit verachtet werden, und mit mitleidiger 
Geringschätzung werden die Wissenden dann auf diejenigen 
sehen, die vielleicht für 6000 M im Dienst des Staates oder 
der Kirche oder der Gemeinde oder in Handel und Industrie 
ihre Kräfte aufreiben. 
Und dabei vergesse man nicht, daß trotz aller gesteigerten 
Wohnungs- und Lebensmittel-Preise die Hälfte aller deut-