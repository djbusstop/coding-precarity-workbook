488 
Löhr, Bezirksamtmann. 409, 410. 
Ludwig XIV. 387. 
Ludwig XV. 388. 
Ludwig XVI. 389, 390. 
Lykurg. 270, 271, 310, 359. 
Maaß, Amanda. 13, 14. 
Maaß, Max. 13, 14. 
Malthus, Robert. 19-25,29-31, 
34, 37. 
Manlius. 284, 285, 310. 
Marie Antoinette. 389. 
Marius. 304. 
Marx, Karl. 6, 37, 38, 41, 42, 57, 
463. 
Mauvillon, Jakob. 391, 392. 
Meltzl, v., H. 203. 
Menz, Dr., Gerhard. 452. 
Micha 240. 
Mill, Stuart. 22, 57. 
Miltiades. 269. 
Miquel, Minister. 175. 
Moltke, Feldmarschall. 422. 
Mirabeau d. Ä. 391. 
Mommsen. 289. 
Moses. 231—239, 241, 242, 255. 
309, 359. 
Morus, Thomas. 436. 
Mucianus, Oberpriester. 292. 
Münch, Stadtsekr. 410. 
Mueller, Bankdir. 137. 
Müller, Max, Prof. 339. 
Waboth. 239. 
Napoleon I. 144, 437. 
Naumann, Friedrich. 247—249. 
Necho. 241. 
Nehemia. 242, 243. 
Nehring. 374. 
Nero. 305. 
Newton, Dr., Herbert. 349. 
Nicolai. 382. 
Nulty, Dr., Thomas, Bischof. 
252—254, 353. 
Hbolenski, Fürst. 430. 
Octavianus, Tribun. 293, 294. 
Oertmann, Paul, Prof. 125. 
Oertel, Dr., Abg. 415. 
Oganowsky, Dr. 431. 
Onnasch, Pastor. 127. 
Oriola, Graf, Abg. 415. 
Oseroff, Prof. 425, 426. 
Paulus. 281. 
Pawlik, Anton. 13, 14. 
Pechmann, Frhr. v., Bankdir. 137. 
Persiander. 269. 
Perikles. 268, 269. 
Pesch, Prof. 111. 
Peters, Dr. 408. 
Philipps. 350. 
Pisistratus. 266, 267. 
Plinius. 287, 305. 
Plutarch. 293, 294. 
Pohlman, A. 137, 196, 396. 
Pompejus. 305. 
Popillius, Publius. 297. 
Posadowsky, Graf. 136. 
Ptolemäus. 279, 280, 292. 
Pufendorf, Samuel. 373.