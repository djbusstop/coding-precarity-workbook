306 
Bodenreformer Roms, ist tausendfältig heimgesucht worden 
an denen, die sich stolz für ihre Überwinder hielten. 
6. Die Lehren. 
3 st es nötig, die Lehren zu betonen, die aus jenen ge 
waltigen Ereignissen vor mehr als 2000 Jahren für uns 
lebendig werden? 
Es sei nur einiges kurz hervorgehoben: 
Wie vorsichtig und mißtrauisch soll man doch das Lob 
der „Maßgebenden" betrachten! Wie gefährlich ist doch eine 
sogenannte „maßvolle" Politik, die nur den einen Grundsatz 
kennt, das Ruhende nicht zu bewegen, eine Politik, die nie 
mals „ja" und niemals „nein" sagen will. 
Wenn Lälius und Scipio Africanus und jener ganze 
Kreis von einflußreichen und ehrlichen Männern wirklich 
entschlossen die Bodenreform in die Hand genommen hätten 
— die Optimalen jener Zeit hätten Lälius dann wohl 
nicht den Beinamen der „Verständige" gegeben, aber viel 
leicht hätte das Urteil der Geschichte ihm diesen Ehrennamen 
verliehen, während sie so doch nur seine bequeme, ja feige 
Schwäche beklagen kann. Wer etwas leisten will, muß den 
Mut zum Willen haben. Und auf das Lob seiner Zeit muß 
kühlen Herzens verzichten können, wer weitschauend an der 
Zukunft bauen will. 
Und die zweite Lehre? Sie zeigt das radikale Schlag 
wort in seiner Verderblichkeit. Mit allem festen Willen und 
allen hohen Zielen muß doch stets die Einsicht in das Mög 
liche verbunden bleiben. Die Volksmenge, die dem Drusus 
zujubelte, als er statt einiger ausländischer Kolonien 36 000 
Bauernstellen in Italien selbst verhieß, als er statt der Erb-