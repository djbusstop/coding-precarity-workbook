463 
tüchtige und ehrliche Menschen init sogenannter „praktischer" 
Reforrnarbeit zu beschäftigen, und sie von dem Kernpunkt 
der Frage abzulenken. 
Die Berliner Sozialdemokratie war gespalten. In der 
am 8. Juli 1871 von der Marrschen Richtung einberufenen 
Versammlung sollte eine Eingabe an den Reichstag ein Ge 
setz fordern, das jede Gemeinde verpflichte, ihre Angehörigen 
ausreichend mit Gemeinde-Wohnungen zu versorgen. Die 
Mitglieder des Allgemeinen Deutschen Arbeiter-Vereins 
(Lassallescher Richtung) erlangten jedoch die Mehrheit und 
nahmen folgende Entschließung an: 
„Die Versammlung verwirft entschieden all dies reaktionäre Ge 
bühren, was nur dazu führen würde, den Arbeitern neue Ochsenkopf 
lokale zu öffnen. Dagegen fordert die Versammlung alle Arbeiter 
Berlins auf, dem Allgemeinen deutschen Arbeiterverein beizutreten, 
damit durch diesen auf dem Wege der Freiheit die Arbeiterfrage und 
mit ihr selbstverständlich zugleich die Wohnungsfrage gelöst werde." 
Der „Reue Sozialdemokrat" erklärte ausdrücklich jeden 
Versuch, die Lage der Arbeiter in der bürgerlichen Gesellschaft 
durch billigere Wohnungen zu verbessern, unter Hinweis auf 
das „eherne Lohngesetz" für aussichtslos. — 
Die Deutschen Gewerkvereine, deren Anwalt Mar 
Hirsch in den liberalen Parteien eine angesehene Stellung 
einnahm, forderten die Errichtung von Baugenossenschaften 
zur Erwerbung von Eigen-Häuschen. Zur Unterstützung 
wurde von Gemeinde und Staat u. a. gefordert: 
1. eine gänzliche Reform des Hypothekenwesens, nach Muster der 
Bremischen Handvesten. 
2. Begünstigung von Baugenossenschaften und Unternehmungen 
mittlerer und kleinerer Wohnungen speziell durch Erb-Ver- 
pachtnngen öffentlicher Ländereien,