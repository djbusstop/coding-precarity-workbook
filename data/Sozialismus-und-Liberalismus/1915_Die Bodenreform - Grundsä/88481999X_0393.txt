373 
ìheenstadt zu, die bald mit 170 Häusern besetzt war. Wie 
einfach es in ihrer Hauptstraße „Unter den Linden" noch 
iange aussah, zeigt ein Befehl, den Friedrich III. um 1700 
erlief, 
„daß die Misthaufen vor den Häusern der Linden auf beiden 
Seiten fortgeschafft werden und daß man darauf achten solle, daß 
die Schweine den Mittelgang der Linden nicht gar zu sehr aufwühlen." 
Als der Große Kurfürst starb, betrug die Zahl der 
Bewohner Berlins, die 1648 auf 8000 geschätzt wurde, 
18 000. 
Daß der Große Kurfürst auch der ländlichen Boden- 
frage großes Interesse zuwandte und die Erhaltung und 
Vermehrung des staatlichen Errmdeigentums grundsätzlich 
erstrebte, darf man wohl mit Sicherheit aus den Schriften 
şeines Vertrauten Samuel Pufendorf schließen, 
bcs bedeutendsten deutschen volkswirtschaftlichen Schrift- 
itellers jener Zeit. Pufendorf war durchaus, den Gedanken- 
gängen seiner Zeit entsprechend, ein Anhänger der absoluten 
àst engewalt. Nur in einem Punkte habe diese ihre Grenze, 
^ìein Fürst dürfe Domänen veräußern. Der Boden der 
Domänen gehöre dem Staate und dem Geschlechte des 
Fürsten. Dem einzelnen Herrscher stehe nur das Recht zu, 
Uber den Ertrag des Bodens zu verfügen, nicht aber über 
diesen selbst! 
D i e großen „inneren" Könige Preußens, 
e ersten Könige Preußens schritten auf der Bahn 
weiter, die der Große Kurfürst mit so gutem Erfolge be 
teten hatte. Friedrich I., dessen Freude an Pracht und Glanz 
Berlin vielfach verschönte, setzte einen besonderen Ausschuß