257 
Fast wie ein Hauch aus modernem Leben berühren 
die Sittenschilderungen dieser Zeit. 
Besonders wertvoll sind in dieser Beziehung die Werke 
Zweier Dichter. Der eine, Hesiod, war selbst ein kleiner 
Landmann, der durch einen Prozeß mit seinem Bruder einen 
Teil seines Gutes eingebüßt hatte. Er gab in seiner Dichtung 
„Werke und Tage" der Stimmung der Kleinen, der Schwachen 
Ausdruck, derer, die beim Übergang zur Kapitalwirtschaft 
verloren. 
Er klagt, daß das goldene Zeitalter, das im Anfang ge 
wesen, verloren gegangen sei; auch das zweite, dritte und 
vierte Geschlecht sei dahin: 
„Möchte ich doch nicht gehören zum fünften Geschlecht! Wäre ich 
lieber vorher gestorben oder später erst geboren! Denn jetzt ist das 
eiserne Zeitalter, wo Mühe und Sorgen den Menschen nicht loslassen, 
Feindschaft aller gegen alle herrscht, Gewalt das Recht beugt, schaden 
froher, übelredender, häßlich blickender Wettbewerb alle antreibt. Nun 
entschweben Scham und die Göttin der Vergeltung, Nemesis, zu den 
Göttern. Alle Übel verbleiben dem Menschen, und es giebt keine 
Abwehr des Unheils." 
Der Dichter klagt, wohl aus eigener bitterer Erfahrung 
heraus, über die „Spenden fressenden" Richter. An seinen 
Ņruder, der ihn im Prozeß besiegt hatte, wendet er sich mit 
folgender Mahnung: 
„Siehe, das Böse — man kann es sich haufenweise gewinnen 
Lhne Bemühen; glatt ist sein Pfad, nah seine Behausung. 
Doch vor die Tugend setzten den Schweiß die unsterblichen Götter. 
Lang und jäh zu dieser erhebt sich der schmale Gebirgspfad 
Und auch rauh anfänglich; doch bist du zur Höhe gelanget, 
Wird sie gewiß dann leicht, wie sehr sie beschwerlicher Art war." 
Und wie Anklage und Aufschrei klingt es durch die erste 
europäische Fabel, die wir demselben Dichter verdanken, und 
Damaschke, Bodenreform, 31.—35. Tausend. 17