259 
Nur sie erziehen. Doch zu frein des Niedrigen niedrige Tochter 
Kümmert den Edelen nicht, bringt sie nur Güter genug. 
Auch verschmäht nicht das Weib die Ehe mit niedrigem Manne, 
Ist er nur reich; es erfreut mehr als die Tugend sie Gold. 
Reichtum ehrt man allein: aus des Schurken Haus freit der Edle, 
Aus des Edlen der Schurk; Mammon verbindet die Art." 
„Erst suche dir einen Lebensunterhalt," heißt es bei 
einem anderen Dichter, „nach Tugend trachte erst dann, wenn 
du zu leben hast." 
Und A l k ä o s prägt das bittere Wort: 
„Geld allein macht den Mann; kein Armer hat Ehre!" 
Es hätte nicht das heiße Blut des Südens in den Adern 
^er Hellenen rollen müssen, wenn nicht die große Masse des 
Ņolkes, die ihren Besitz mehr und mehr schwinden sah, Ber 
sche hätte unternehmen sollen, diesen Zustand zu ändern. 
Ņei der maßgebenden Bedeutung des Grundeigentums, die 
îņ einfacheren wirtschaftlichen Verhältnissen noch unver- 
hüllter in die Erscheinung tritt, als in unserer modernen, so 
vielgestaltigen Wirtschaftsweise, ist es klar, daß jeder Ruf 
nach einer ernsten Reform ein Ruf nach Bodenreform sein 
uiußte. 
So sind es denn auch zwei Forderungen, die immer und 
unmer wieder laut werden: Tilgung der Pfandschulden und 
Neuverteilung des Bodens! 
Die Pfandschulden jener Zeit gleichen unseren Hypo 
theken. Dies Wort selbst ist ja griechischen Ursprungs; es 
heißt: Unterpfand. Wie wir Hypotheken in die Grundbücher 
eintragen, so wurden die Hypothekenschulden vor 2500 Jahren 
1111 alten Hellas auf sogenannten Pfandsteinen angezeigt, die 
auf dem verschuldeten Acker aufgerichtet wurden und auf 
i^bNen der Name des Gläubigers und die Schuldsumme ein- 
17*