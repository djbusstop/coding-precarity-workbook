39 
Brutalisierung und moralischer Degradation auf dem Gegenpol, d. h. 
auf seiten der Klasse, die ihr eigenes Produkt als Kapital produziert." 
Bringt man dieses Gesetz auf die einfachste Formel, so 
sagt es: Die technischen Fortschritte ersparen in steigendem 
Matze menschliche Arbeit; sie werfen immer mehr Arbeiter 
aus ihren Stellungen; sie vergrößern ständig das Heer der 
Arbeitslosen, die „industrielle Reservearmee". Diese mutz 
natürlich um jeden Preis ihre Arbeitskraft den Besitzern der 
Produktionsmittel anbieten und drückt dadurch die Lebens 
haltung aller Arbeiter dauernd auf den möglichst tiefen 
Stand hinab. Die Maschinen aber werden immer riesen 
hafter, und ihr Besitz vereinigt sich in immer weniger Händen, 
deren wirtschaftliche Macht dadurch ins Ungeheure wächst. 
Ist das Gesetz der kapitalistischen Akkumulation richtig, 
so würde in der Tat das notwendige Ende unserer wirt 
schaftlichen Entwicklung der Kommunismus sein müssen. Der 
technische Fortschritt, der natürlich nicht aufgehalten werden 
darf und kann, schüfe dann die ökonomische Voraus 
setzung des Kommunismus, indem er die Produktion zuletzt 
in wenigen Riesenbetrieben vereinte, und zugleich seine poli 
tische Vorbedingung, indem er die ungeheure Mehrzahl der 
Menschen in allen Kulturländern zu hoffnungslosem Prole 
tariat verdammte. 
Müssen wir also in den Kommunismus hinein? 
Zweifellos zeigt das Gesetz der kapitalistischen Akkumulation 
eine richtige Tendenz. Eine Maschine wird aufgestellt, um 
Menschenarbeit zu ersparen. Wer aus einem Betrieb ent 
lassen wird, weil seine Arbeit nun durch die Maschine ge 
leistet wird, sieht darin einen untrüglichen Beweis für die 
Richtigkeit der marxistischen Lehre. Und diese Auffassung