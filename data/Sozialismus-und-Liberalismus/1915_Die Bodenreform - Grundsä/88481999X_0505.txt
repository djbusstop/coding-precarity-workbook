Personen-Vryeichnis. 
(Die Ziffern bedeuten die Seiten.) 
Ablott, Nyman. 349. 
Adler, Dr. Felix. 351. 
Agathokles. 261. 
Agesilaos. 274, 275. 
Agesistrata. 276. 
Agiatis. 273, 276, 279. 
Agis. 241, 272, 277, 310. 
Agrippa. 283. 
Albert, Herzog v. Sachsen. 361. 
Alexander der Große. 272. 
Alkäos. 259. 
Amos. 240. 
Antäus. 229. 
App ins Claudius. 295.. 
Aratos. 279. 
Archimadia. 276. 
Arenberg, Herzog v. 395. 
Argyll, Herzog v. 340. 
Arnim, v., Minister. 400. 
Arendt, Henriette. 7. 
Arnd, Karl. 356. 
Aristoteles. 262. 
Asabos. 274. 
Auer. 50. 
Augustus, Kaiser. 30, 305. 
Auhagen, H. 206, 223. 
IZaltzer, Eduard. 356. 
Barth, Stadtver. 106. 
Baumeister, Prof. 124. 
Bebel, August. 416. 
Berlepsch, ü., Minister. 395. 
Bethmann-Hollweg, v. 107, 401. 
Bigelow, Poultney. 417. 
Bismarck, v. 390, 396. 
Blankenfelde, Johannes, v. 365. 
Blunck, Abgeord. 122. 
Boeters, Konter-Adm. 408. 
Bodelschwingh, v. F. 126. 
Bosse Dr. 154. 
Brandis, Dr. 102, 106. 
Buchka, v. 403, 406, 409. 
Bücher, Karl, Prof. 124, 204, 
Büring, Kfm. 386. 
Burdekin, Bürgermstr. 342. 
Gaesar. 305. 
Calmer, Abg. 147. 
Cassius, Spurius. 283, 284. 
Catharina v. Braganza. 447. 
Cato. 290. 
Chalmers. 23. 
Cocceji. 379. 
Colbert. 387, 388. 
Condorcet. 19. 
Cornelia. 291, 292, 296, 298, 
299, 802. 
Crassus. 305, 308.