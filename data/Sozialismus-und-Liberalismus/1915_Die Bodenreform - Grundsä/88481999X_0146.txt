126 
£ tn ausgedehntes ©emembegnmbeigerttum wirb auch 
von höchster Bedeutung sein für die Bekämpfung der 
schwersten sozialen Not, der unverschuldeten Arbeitslosigkeit, 
und bei der Rettung derer, die in unserer Kulturentwicklung 
und wesentlich auch durch sie im doppelten Sinne des Wortes 
jeden Boden unter den Füßen verlieren. 
Als der große Menschenfreund Pastor von Bodel- 
schwingst durch das Berliner Asyl für Obdachlose ging, 
das abends und nachts den Elendesten eine vorübergehende 
Zufluchtsstätte bietet, da ergriff ihn der Gedanke: Wie kann 
diesen Menschen, die jetzt fast rettungslos der Bettelei und 
Schlimmerem verfallen müssen, wirklich geholfen werden? 
Bodelschwingh hatte nicht nur ein heißes Herz, sondern 
auch einen bodenreformerisch geschulten Kopf. Aus beiden 
entsprang im Jahre 1905 die Schöpfung der Kolonie Hoff 
nungstal (später Lobetal und Gnadental). Mit 17 Obdach 
losen aus dem Asyl, die lieber Arbeit als Almosen haben 
wollten, begann er. Heute bietet Hoffnungstal Raum für 
420 Kolonisten. Fast 6000 Menschen sind inzwischen aus 
dem Asyl durch diese Kolonie wieder mit dem Boden in Be 
rührung gekommen, und mehr als ein Drittel davon ist 
wieder in ein Arbeitsverhältnis oder in die Familie zurück 
gekehrt. Daneben haben diese Menschen, die sonst durch 
Bettelei und Diebstahl der Gemeinschaft schwere Kosten ver 
ursachen würden, 300 Morgen Ödland in prächtig gedeihende 
Obstanlagen verwandelt. 
Wie leicht Gemeinden und Private auch beim besten 
Willen durch allerlei Wohltätigkeitseinrichtungen im Kampfe 
gegen Obdachlosigkeit und Arbeitslosigkeit tiefgehende Schäden 
hervorrufen, wie das Recht auf Arbeit zuletzt nur ein Recht