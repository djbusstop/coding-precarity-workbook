385 
wert einem heutigen Geldlohn von etwa 4—6 M>. Wie 
konnte bei solchem Preise für eine Lohnarbeit, die durch 
keinerlei maschinelle Hilfe unterstützt wurde, eine „rentable" 
Landwirtschaft begründet werden? Nur durch die Billig 
haltung des Bodens unter Ausschluß alles spekulativen 
Mißbrauchs: 
„damit auch während den Frey-Jahren aller wucherliche Handel 
mit denen Loosen vermieden wird, so soll während den Frey-Jahren 
dem Amte freystehen, wenn ein Wirth verkaufen will, das Loos gegen 
die Taxe desjenigen, so der Besitzer darauf verwandt hat, zurück 
zu nehmen." 
Natürlich bedingte die Annahme von billigem Land 
auch einen Bauzwang. Die Kolonisten mußten 
„den Aufbau ihrer Wohnungen binnen einem (bzw. zwei) Jahre 
bewürcken, oder in Entstehung dessen sich der Exmission von ihren 
Loosen unterwerfen und gefallen lassen . . ., daß solche an andere 
tüchtige Wirthe vergeben werden." 
Als Ideal schwebte dem Könige vor, was er in der 
Kabinettsorder vom 23. März 1780 ausspricht, 
„daß der ernste und gnädige Wille Unserer höchsten Person da 
hin gehe, daß die Kolonisten ihre Loose schlechterdings nicht verkaufen 
sollen, sondern solche vielmehr auf ihre Kiuder und Kindeskinder 
bringen müssen". 
Der Grundzins (Kanon) war im wesentlichen die einzige 
Abgabe an die Staatskasse; er schwankte bei guten, gegen 
Überflutung geschützten Grundstücken zwischen 8 und 12 
Groschen für den Morgen. 
Schon bei der Gründung neuer Ansiedlungen in Berlin 
hatte der König die Bedeutung der Verschuldungsfrage und 
der dann liegenden Gefahren ins Auge gefaßt. Diese Er 
kenntnis führte ihn zur Einführung der heute noch von den 
Damaschke, Bodenreform, 31.—35. Taufend. 25