Einen noch schwereren Kampf wagte George gegen 
einen Gesetzentwurf, der den freien Branntweinausschank 
begünstigte. George hatte es bei seinem engen Verkehr mit 
dem arbeitenden Volke in seinen Jugend- und Wander 
jahren oft erfahren, welch ein Verderben der Mißbrauch 
geistiger Getränke für Unzählige ist, wieviel Kraft durch ihn 
vergeudet, wieviel Sittlichkeit untergraben, wieviel Ehre ver 
loren, wieviel Familienglück vernichtet wird. Er zögerte 
deshalb nicht einen Augenblick, den stampf gegen den ver 
hängnisvollen Gesetzentwurf aufzunehmen. Es gab in jener 
-Zeit etwa 4000 Branntweinschenken in San Franzisko. Sie 
alle erklärten dem gefährlichen Manne den Krieg, und die 
großen Branntweinbrenner standen mit ihren Geldmitteln 
und mit ihrem Einfluß hinter ihnen. In keinem öffentlichen 
Ļokal, bei keinem Krämer, der Branntwein verkaufen wollte, 
sollte die „Abendpost" von jetzt an ausgelegt werden. Auch 
die Anzeigen, eine überaus wichtige Einnahmequelle für 
jede Zeitung, sollten ihm soviel wie möglich entzogen werden. 
Aber die Freunde des freien Wortes und der Mäßigkeit 
hielten fest zu George, so daß die Zeitung siegreich diesen 
schweren Kampf bestand. An seinem Ende zählte sie mehr 
Ļeser als zu seinem Beginn. 
Da suchte man sich auf eine andere Weise des gefürchteten 
Ņiannes zu entledigen. Ein erster Versuch, Henry George 
'.auszukaufen", d. h. die Anteilscheine der Zeitung in anderen 
besitz zu bringen, gelang nur vorübergehend. George konnte 
bald wieder die Zeitung in seine Hände bringen. Da be 
ding er einen Fehler, der für sein Unternehmen verhängnis 
voll werden sollte. Es war ein Lieblingsgedanke von ihm, 
alle Angestellten zu Teilnehmern des Geschäftes zu machen