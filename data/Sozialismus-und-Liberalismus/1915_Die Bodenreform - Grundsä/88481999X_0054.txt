34 
sich ben Weg durch bas Dickicht, den die fortschreitende Mensch 
heit hernach zu einer Landstraße erweitern kann. In immer 
höhere, großartigere Sphären steigt und ruft das Verlangen, 
und ein Stern, der im Osten aufgeht, leitet ihn weiter." 
Die Lehre des Malthus, die durch ein Naturgesetz jede 
Aufwärtsentwicklung des Menschengeschlechts wissenschaftlich 
zu hemmen unternimmt, muß klar und scharf abgelehnt 
werden. Mit dem Bevölkerungsgesetz des Malthus ist dem 
Mammonismus jedes wissenschaftliche und sittliche Recht ge 
nommen, und wir haben freie Bahn, das große soziale Pro 
blem von neuem aufzurollen und unbehindert und unbeirrt 
um eine volle Antwort zu ringen! 
S cheinbar im Gegensatz zu der Anschauung des Malthus 
steht eine andere von den Verteidigern der mammo- 
nistischen Auffassung oft gegebene Antwort auf das moderne 
Wirtschaftsproblem: Die Überproduktion. 
Wie oft kann man sie als Antwort auf die Frage nach 
der Ursache der sozialen Not auch in gebildeten Kreisen, 
selbst im Deutschen Reichstage, nennen hören: „Es wird viel 
zu viel produziert. Sehen Sie sich doch die Magazine und 
Warenlager an. Alles ist überfüllt. Überall hört man Klagen 
über unverkaufte Vorräte, und dazu werden an jedem Tage 
neue Waren auf den Markt geworfen". 
Die Gedankenlosigkeit auf diesem entscheidungsvollen 
Gebiete unserer Zeit geht so weit, daß es manche Leute 
fertig bringen, in einem Atem Übervölkerung und Über 
produktion als Ursachen der Not zu bezeichnen. Und da 
bei heißt doch Übervölkerung: zu viel Menschen, zu wenig 
Ware, und Überproduktion: zu viel Ware, zu wenig Men-