73 
Ortskrankenkasse feststellte, daß von 329 Tuberkulose-Kranken 
101 kein Bett für sich allein hatten. 
Bei einer Untersuchung durch die Ortskrankenkasse der 
Kaufleute in Berlin 1910 fand es sich, daß von 1380 Lungen 
kranken 1137 mit anderen Personen einen Schlafraum teilen 
mutzten. Nicht weniger als 360 Kranke hausten in nicht heiz 
baren Räumen. Die Wohnungen von 611 anderen Kranken 
waren nur durch eine Kochmaschine zu erwärmen, d. h. es 
waren Küchen, in denen gekocht und auch gewaschen wurde. 
In Wien starben von 1887—90 irrt ersten Bezirk, 
in dem nur 3,58 % ber Bevölkerung in überfüllten Woh 
nungen lebten, an Tuberkulose 21,5 von 10 000 ; irrt 10. Be 
zirk dagegen, in dem 42,8 % „überfüllt" wohnten, starben 
59,8 von 10 000, also fast dreimal soviel. In Paris er 
gaben seit 1894 geführte Hausakten: ein Block mit Klein 
wohnungen, der nur zu 56 % überbaut war, und zwar mit 
drei geschossigen Häusern, zeigte eine Tuberkulosesterblich 
keit von 3,47 °/ 00 der Bewohner, während bei einem zweiten 
Block mit 80 % überbauter Fläche und sechs Geschossen 
diese 9,66 °/ 00 , also fast das Dreifache betrug! — 
Der Kampf gegen die Säuglingssterblich 
keit führt zu bewundernswerten Anstrengungen privater 
uttd öffentlicher Kreise. Aber ein Kenner wie Professor 
F. S i e g e r t, der Direktor der städtischen Kinderklinik in 
Köln, mutzte in seinem Vortrag „Säuglingsfürsorge und 
Wohnungsfrage" auf dem Dresdener Bundestag der Deut 
schen Bodenreformer doch dartun, datz alle anderen Ur 
sachen gegenüber dem Wohnungselend zurücktreten. Aus 
seinem Vortrag (Jahrbuch der Bodenreform 1911) seien nur 
wenige Angaben wiedergegeben.