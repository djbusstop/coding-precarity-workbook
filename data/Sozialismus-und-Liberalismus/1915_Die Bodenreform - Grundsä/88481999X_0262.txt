242 
der ägyptischen Übermacht nahm er im Tale von Megiddo 
die Schlacht an. Es ist die letzte große Feldschlacht, die Israel 
schlug. Die ägyptische Übermacht war zu groß. Als der 
König durch einen Pfeilschuß tödlich verletzt wurde, ging die 
Schlacht verloren. Er sollte, wie die Prophetin Hulda ihm 
gesagt hatte, das Verderben nicht sehen, das nun über sein 
Volk hereinbrach. 
' Inwieweit Josia auch Bodenreformgedanken durchge 
führt hat nach den Gesetzen Moses', wird nicht ausführlich 
mitgeteilt. Aber daß Josia auch auf sozialem Gebiete reichen 
Segen gestiftet haben muß, geht aus den heftigen Anklagen 
hervor, die Jeremias gegen den unglücklichen Sohn des 
Königs richtet (Kap. 22): 
„So spricht der Herr: Wehe dem, der sein Haus mit Sünden 
bauet und seine Gemächer mit Unrecht; der seinen Nächsten umsonst 
arbeiten lässet und gibt ihm seinen Lohn nicht. 
Meinest du, du wolltest König sein, weil du mit Zedern prangest? 
Hat dein Vater nicht auch gegessen und getrunken? und hielt dennoch 
über dem Recht und Gerechtigkeit, und ging ihm wohl! 
Er half dem Elenden und Armen zu Recht, und ging ihm wohl. 
Ist es nicht also, daß solches heißt, mich recht erkennen? spricht der 
Herr." 
Schnell brach der Untergang über das Volk herein, das 
seine sozialen Grundlagen verloren hatte. Wir kennen den 
Tag des Zornes, an dem babylonische Krieger die Brand 
fackel in den Tempel Salomos warfen. 
Aus der Geschichte lernt man, daß die Menschen nichts 
aus der Geschichte lernen. Bald nach der Rückkehr aus der 
babylonischen Gefangenschaft begann wieder eine Nicht 
achtung der mosaischen Bodengesetze, die Land und Menschen 
in Knechtschaft zu bringen drohte. Da war es N e h e m i a,