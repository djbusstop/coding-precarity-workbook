270 
Wo die Bodenreform nicht vermag, in gesunder Heimat 
politik die Wurzeln des Volkstums im eigenen Lande zu 
sichern, da Hilst auch die glänzendste Entfaltung nach außen 
nur auf eng begrenzte Zeit! 
3. Die letzten Bodenreformer Spartas, 
^^er andere führende Staat der Hellenen, Sparta, er- 
^) wuchs auf den Grundlagen der lykurgischen Gesetz 
gebung. Die Historiker streiten darüber, ob ein Lykurg 
überhaupt gelebt habe, ob die Gesetzgebung, die seinen 
Namen trägt, überhaupt von einem Einzelnen herrühre, oder 
ob sie nicht vielmehr die Frucht langjähriger Entwicklung sei. 
Als feststehend und wesentlich bleibt in jedem Fall auch hier 
die Tatsache, daß unter den dorischen Einwanderern, die 
als Herrenvolk im Eurotastale saßen, schwere innere Kämpfe 
ausgebrochen waren, die in der Verschiedenheit des Grund 
eigentums ihre Ursache hatten; daß der Staat vor dem 
Untergange stand, und daß eine weitgehende Bodenreform 
allein Frieden und damit Rettung schaffen konnte. Die 
Zahlen der einzelnen Landlose werden verschieden ange 
geben. Gewöhnlich sagt man, daß im eigentlichen Eurotastale 
4500 gleiche Landlose und in dem später unterworfenen 
Messenien gleichfalls 4500 Landlose für die Spartaner er 
richtet worden wären. Die friedlichen Ureinwohner, die 
Periöken, sollen 30 000 kleine Landlose erhalten haben. Die 
im Kriege gefangenen oder mit Gewalt unterworfenen Heloten 
endlich waren Unfreie. Jedem spartanischen Erbgut und 
jedem Staatsbesitz war eine bestimmte Zahl von ihnen zu 
geteilt. Aber sie blieben Eigentum der Gesamtheit. Der 
einzelne Gutsherr konnte sie weder freilassen, noch verkaufen,