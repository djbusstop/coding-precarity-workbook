486 
Croasdale, William. 341. 
Crosby, John. 349. 
Croy, Herzog v. 396. 
Damaschke, A. 170,171, 358, 405, 
406, 407. 
Dankelmann. 374. 
Danneel, Exz. 169. 
Darwin. 24, 353. 
Dawydof, Exz. 432. 
Delbrück, Prof. 27, 28. 
Demades. 262. 
Dernburg, Prof. 157. 
Diederichs, v. Admiral. 411. 
Dionysius. 261. 
Dorothea. 372. 
Drakon. 262. 
Drusus, Tribun. 300, 301, 306. 
Drusus, Livius. 304. 
Düwell, Kriegsberichterst. 433. 
Hngel, Dr. 462, 466. 
Ehrler, Dr. 80. 
Elster, Prof. 22. 
Epitadeus. 271. 
Erman, Prof. 137. 
Ernst, Herzog v. Sachsen. 361. 
Eschwege, Ludwig. 148. 
Eukleides. 280. 
Aichte, Joh. G. 393. 
Fleischer, Geh. Rt. Dr. 227, 
Flürscheim, Michael. 357. 
Fox, Annie. 316. 
Francois, C. von, Landeshaupt 
mann. 407. 
Franke Dr. 69. 
Freese, Heinrich. 15. 
Freund, Dr., Ministerialdir. 456. 
Friedrich IL, der Eisenzahn. 363. 
Friedrich der Große. 378, 386, 
391, 394, 399, 401. 
Friedrich III. (1) 873, 374, 383. 
Friedrich III., Kaiser. 129. 
Friedrich Wilhelm d. Gr. Kurfürst. 
368, 370—375, 377, 383, 399, 
401. 
Friedrich Wilhelm I. 369, 375— 
378, 383. 
Friedrich Wilhelm II. 392. 
Friedrich Wilhelm III. 197, 198. 
Hamp, Frhr. v. 396. 
Gayl, Frhr. v. 223, 224. 
Georg III., v. England. 437. 
George, Annie. 316, 317, 348. 
George, Henry. 31—33, 255, 311 
—320, 337—353, 357, 358, 424. 
George, Henry, d. I. 346. 
George, Lloyd. 438—440. 
George, Richard. 346. 
Gerlosf, Bürgermstr. 471. 
Gierke, v., Otto, Prof. 225, 226. 
Gladstone. 449. 
Glynn, Mc. Dr. 254, 255, 340, 
343, 349. 
Godwin. 19. 
Goethe, Wolfgang. 65, 124, 264. 
Gottheil, Rabbiner. 349. 
Gracchus, Gajus. 241, 292, 295, 
297—305, 307, 308, 310.