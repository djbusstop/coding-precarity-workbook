136 
in höherem Maße überdeckt, als bei der Grundstückshypothek, bei der 
zum Sachwerte der Bodenwert hinzutritt. 
Die Verkäuflichkeit des Erbbauhauses ist leichter, als diejenige 
des Grundstückshauses, da der Bodenwert bei dem Kaufpreise aus 
scheidet. Nur die Bodenrente, der Erbbauzins, wird vom Käufer 
übernommen; der Kapitalaufwand beim Kaufe wird wesentlich ver 
ringert." 
Das Deutsche Reich hat bis zum 1. Januar 1909 in 
12 Orten: Berck bei Ruhrort, Brunsbüttel, Danzig, Dresden, 
Duisburg, Holtenau, Kiel, Metz-Sablón, Neuende bei Wil 
helmshaven, Pries bei Eckernförde und Rendsburg 556408 
qm in Erbbaurecht abgegeben. 
Über die Bedeutung dieses Vorgehens hat der damalige 
Staatssekretär des Innern, Graf Posadowsky, am 
10. Februar 1903 im Reichstage dem Abgeordneten Jäger, 
einem Vorstandsmitglied des Bundes Deutscher Boden 
reformer, gegenüber ausgeführt: 
„Es hat mich namentlich gefreut, daß der Abgeordnete Jäger 
in seinen Ausführungen auch zu der Überzeugung gekommen zu sein 
scheint, daß der geeignetste Weg sowohl für das Reich, wie für Staat 
und Kommune, den unbemittelten Bevölkerungsklassen billige Woh 
nungen zu schaffen, in der Tat in der Anwendung des Erbbaurechts 
liegt. Nur wenn Reich, Staat und Kommune in dieser Weise dauernde 
Eigentümer des Grund und Bodens bleiben, auf dem im Wege 
der Genossenschaftsbildung billige Wohnstätten für die unbemittelten 
Klassen errichtet werden, wird es möglich sein, der Grundstücksspeku 
lation in der Umgebung der Städte, namentlich der Großstädte, wirk 
sam und dauernd entgegenzutreten. Werden aber die Preise für Grund 
und Boden in der Umgebung der sich entwickelnden Städte weiter ge 
trieben, in die Höhe getrieben, so wird es auf die Länge der Zeit 
geradezu unmöglich werden, für die ärmeren Bevölkerungsklassen noch 
billige Wohnstätten in erreichbarer Nähe ihrer Arbeitsstätte zu schaffen 
(sehr richtig!); denn nicht in den Kosten des Baues der Wohnstätten, 
sondern im steigenden Preise des Bodens liegt dann eine solche Er-