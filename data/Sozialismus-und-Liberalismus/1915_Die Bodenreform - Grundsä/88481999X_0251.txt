231 
2. Der Gedanke der Theokratie. 
1 tms Jahr 1500 vor unserer Zeitrechnung, also etwa 
neunhundert Jahre früher als Solon in Athen, mehr 
als elfhundert Jahre früher als Licinius in Rom boden- 
reformerische Grundsätze zum Siege führten, finden wir in 
Israel jene gewaltige, durch die Zeiten ragende Gestalt, 
auf die alle wichtigen Einrichtungen und Gesetze des jüdischen 
Volkslebens zurückgeführt werden: Moses. 
Wer will in wenigen Worten die Persönlichkeit des 
Mannes schildern, der am prunkvollen Pharaonen-Hofe als 
ein Sohn der Königstochter in aller kriegerischen Tugend 
ritterlicher Prinzen und in aller Weisheit hoher Priester 
schulen erzogen wurde, und der doch alle Macht der Mächtigen 
und alle Weisheit der Weisen verließ, um einem armen und 
verachteten Volke zu dienen? 
Man übersieht häufig, daß die ägyptische Kultur zur 
Zeit des Moses schon so alt war, wie heute etwa die Kul 
tur des deutschen Volkes. Die Pyramiden, auf denen die 
Augen Moses' ruhten, waren schon in jenen Tagen mehr als 
1000 Jahre alt, und die Staatsweisheit und die Volkswirt 
schaft, die in den Priesterschulen von Theben gelehrt wurden, 
konnten sich auf lange Zeiten der Blüte, des Verfalles und 
der Wiedergeburt des eigenen Volkes stützen. 
Welche Bedeutung hat dieser gewaltigste Gesetzgeber 
aller Zeiten der Bodenfrage beigemessen? 
Das volkswirtschaftliche Ziel seiner Gesetzgebung ist ein 
mal so zusammengefaßt: sie solle dahin führen, „daß das 
Volk genug zu essen habe und im Lande sicher wohne". 
(3. Mos. 25, 19.)