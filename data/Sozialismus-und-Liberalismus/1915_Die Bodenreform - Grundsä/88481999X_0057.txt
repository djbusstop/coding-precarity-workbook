37 
kann? Wie kommt es, daß trotz der ungeheuren, stetig stei 
genden Güterproduktion doch die große Mehrzahl des Volkes 
diese Güter nicht erwerben kann, obwohl sie danach strebt? 
Es ist im Grunde das ganze soziale Problem, das in dem 
Worte „Überproduktion" aufgerollt wird. Aber in ihm eine 
Erklärung oder gar Antwort finden zu wollen, erscheint 
töricht. 
3. Der Kommunismus. 
^şus der Hoffnungslosigkeit der pseudo-liberalen mam- 
monistischen Auffassung ist der moderne „wissenschaft 
liche" Kommunismus erwachsen, der seine Meister in Ferdinand 
La ss all e und Karl Marr verehrt. In der Stiftungsur- 
kunde der deutschen Sozialdemokratie, im „Offenen Antwort 
schreiben" vom 1. März 1863, steht das „eherne ökonomische 
Gesetz" im Mittelpunkt, nach dem unter der gegenwärtigen 
Herrschaft von Angebot und Nachfrage der durchschnittliche 
Arbeitslohn stets auf den notwendigsten Lebensunterhalt 
beschränkt bleiben muß. Siegessicher konnte Lassalle seiner 
Darlegung hinzufügen: 
„Dieses Gesetz kann von niemand bestritten werden. Ich könnte 
Ihnen für dasselbe ebensoviele Gewährsmänner anführen, als cs 
große und berühmte Namen in der nationalökonomischen Wissen 
schaft gibt, und zwar aus der liberalen Schule selbst; denn gerade 
die liberale ökonomische Schule ist es, welche selbst dieses Gesetz 
entdeckt und nachgewiesen hat. 
Dieses eherne und grausame Gesetz müssen Sie sich vor allem 
tief, tief in die Seele prägen und bei allem Ihrem Denken von ihm 
ausgehen!" — 
Und wenn Marr auch Malthus und das eherne Lohn 
gesetz Lassalles nicht anerkannte, so hat doch auch das von