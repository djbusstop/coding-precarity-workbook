338 
kauft. Heute ist „Fortschritt und Armut" ins Französische, 
Russische, Portugiesische, Schwedische, Ungarische, Polnische, 
Böhmische, Dänische, Japanische, Chinesische und Arabische 
übersetzt. In Deutschland haben wir die musterhafte Über 
setzung von einem Freunde Henry Georges, F. Gütschow, 
der schon 1880 in San Franzisko das Buch in seine Mutter 
sprache übertrug. 
Der Erfolg seines Werkes riß George aus der Stille 
seines Beamtentums und führte ihn mitten in die größten 
Kämpfe unserer Zeit. 
Im Jahre 1881 ging er als Vertreter der „Irish World" 
zum ersten Male nach Irland und England. Überall hielt 
er Vorträge und verkündete die neue Wahrheit, und überall 
weckte er heftige Gegnerschaft, aber auch begeisterte Zu 
stimmung. Sein Name wurde ein Feldzeichen, das die 
Geister schied. Im Jahre 1883, als er zum zweiten Male 
nach England kam, war feine Lehre bereits eine Macht ge 
worden. „Ich sprach", berichtete George selbst, „in den wich 
tigsten Städten Englands. Ich hatte überall viele Zuhörer. 
Zuerst standen sie mir meist feindlich gegenüber,- aber ich 
eroberte sie alle leicht mit Ausnahme derer in Orford, dem 
Sitze der Gelehrsamkeit. Die Orforder Studenten waren 
entschlossen, die Versammlung zu stören, obgleich ich Gast 
des berühmten Professors Mar Müller war, der auch den 
Vorsitz in dieser Versammlung führte". 
Neben dieser anstrengenden Werbetätigkeit schuf George 
sein zweites großes Werk: „Schutzzoll oder Freihandel". 
Im Jahre 1884 folgte er einer Einladung seiner An 
hänger in Schottland, um auch hier für seine Gedanken zu 
wirken. Seine große Rede im Stadtsaale von Glasgow über