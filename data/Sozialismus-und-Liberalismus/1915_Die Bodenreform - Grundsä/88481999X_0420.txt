400 
erkennen, wie schnell die Begriffe von Recht und Unrecht 
wechseln können. 
Aber die Erkenntnis, daß der Boden keine bloße Speku 
lationsware fein darf, wächst, zumal seitdem in dem Bund 
Deutscher Bodenreformer sich immer mehr pflichtbewußte 
Männer und Frauen aus allen politischen und religiösen 
Parteien vereinen, die den Ausschluß des Mißbrauchs mit 
dem vaterländischen Boden als die erste Voraussetzung jeder 
gesunden sozialen Entwicklung erkennen, dafür eine plan 
mäßige Aufklärungsarbeit leisten und sich auch durch alle 
Angriffe von interessierter Seite nicht irre machen lassen. 
Es ist ein Anknüpfen an die besten Überlieferungen 
Hohenzollernscher Sozialpolitik, wenn z. B. unter unserem 
jetzigen preußischen Könige Erlasse der Minister der Finanzen, 
des Innern, des Kultus und des Handels in die Welt gehen, 
wie die vom 19. März 1901: 
„von durchgreifenoer Bedeutung für eine bessere Gestaltung 
der Wohnungsverhältnisse ist endlich eine zweckmäßige Bodenpolitik 
der Gemeinden. Die heute herrschenden Mißstände haben ihre Haupt- 
quelle in der ungesunden Bodenspekulation, die sich freilich zum 
Teil mit Erfolg nur nach Abänderung der Gesetzgebung bekämpfen 
lassen wird. Lin wirksames Mittel, um sie iu Schranken zu halten, 
bietet sich aber auch gegenwärtig schon in der Erwerbung tunlichst 
vieler Grundstücke durch diejenigen Gemeinden, deren stetiges An 
wachsen das umliegende Acker- und Gartenland in immer zu 
nehmendem Maße in Bauland verwandelt." 
Hierher gehören auch die Versuche in den Ostprovinzen 
auf dem Gebiet der Innenkolonisation, der Besitzfestigung, 
der Entschuldung (Siehe Seite 184 u. ff.). Es ist kein Zu 
fall, daß dort, wo die volle Bedeutung der Bodenfrage für 
das nationale Leben am klarsten zutage tritt, auch das