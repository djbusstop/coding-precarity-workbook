348 
Am Sonntag, den 31. Oktober, wurde Henry George 
begraben. In dem größten Saal des Grand Central Palace 
war sein schmuckloser Sarg aufgestellt, aus dessen oberem 
glasbedeckten Teil das stille Antlitz des Toten schaute. Hier 
versammelten sich die Mitglieder der Familie, die nächsten 
Freunde, die Vertreter der Städte New Pork und Brooklyn; 
hier hinein strömte das Volk, soweit es der Raum gestattete. 
Auf den Wunsch der Witwe begann die Feier mit dem Ge 
sänge des englischen Kirchenliedes: „Komm, liebliches Licht!" 
Dann trat Dr Herbert Newton an das schwarz bekleidete 
Rednerpult und segnete nach dem Ritus der protestantischen 
Hochkirche Englands die Leiche ein. Ein Geistlicher dieser 
Kirche, Nyman Ablott, schilderte dann des Verstorbenen 
Tugenden und stellte seine Selbstlosigkeit und seine Treue 
als leuchtendes Beispiel hin. 
Nach dem protestantischen Geistlichen bestieg der Rab 
biner Gottheil das Rednerpult. Er legte seinen Ausfüh 
rungen das alte hebräische Wort zugrunde: „Der wahrhaft 
Weise wird größer sein im Tode als im Leben." 
Nach dem Rabbi ergriff der katholische Priester Ed 
ward Mac Glynn das Wort: „Ich glaube mich keiner 
Profanation der Schrift schuldig zu machen, wenn ich sage: 
Uns ward ein Mann von Gott gesandt, des Name war Henry 
George .... Sein Buch ist nicht nur das Werk eines National 
ökonomen, sondern das eines Sehers, eines Propheten. Es 
ist ein religiöses Gedicht . . . Wenn man die Namen von 
Bürgermeistern und Präsidenten nur noch in verstaubten 
Katalogen wird finden körmen, so wird noch im Bewußtsein 
der Menschen leben und glänzen der Name Henry George." 
Zuletzt sprach John Crosby : „Man sagt George nach,