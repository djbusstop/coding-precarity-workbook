54 
er in das andere Ertrem verfällt: er will auch die Arbeits 
produkte behandeln, wie man den Erdboden behandeln sollte. 
Die volkswirtschaftliche Wahrheit aber wird nur eine 
Anschauung bringen, die die volkswirtschaftliche Natur der 
Dinge zu ihrem Rechte kommen läßt: Die Kontrolle der 
Gesamtheit für alles, was seiner Natur nach M o - 
n o p o l ist, d. h. neben den Verkehrswegen in erster Reihe 
für den Boden und seine Schätze, damit jedem der Zu 
tritt zu den Rohstoffen gesichert sei! Die Freiheit des 
Einzelnen aber auf dem Gebiet, auf dem sich allein 
Persönlichkeiten entwickeln können, auf dem seiner Arbeit 
und der Verwertung ihrer Produkte! 
îî^elche volkswirtschaftliche Eesamtauffassung entspricht 
der psychischen Natur des Menschen? Wie müssen sich 
die Zustände des menschlichen Zusammenseins gestalten, wenn 
sie der Menschennatur entsprechen sollen? 
Die Vertreter des Mammonismus sprechen: Der Mensch 
ist ein Individuum. Er ist ein Einzelwesen, das seinen Vor 
teil schaffen und sein Glück gestalten soll, so gut es irgend 
geht. Die Tüchtigen werden dann von selbst stark werden, 
und aus Einzelvorteilen wird sich die Summe Eesellschafts- 
glück ergeben, die auf dieser Welt erreichbar ist. Es ist das 
eine Art Raubtier-Anschauung. So ungefähr könnte ein 
Tiger auch sprechen, zumal wenn er satt ist. Freilich, die 
Vertreter dieser Art von Individualismus sind natürlich zum 
Teil gute Leute, die wissen, daß alle Dinge dieser Erde re 
lativ sind, und die gerne durch Almosen und Wohlfahrts 
einrichtungen lindern und mildern, aber doch nur, so weit 
das Wesen der Eesamtauffassung nicht berührt wird.