Es konnte ihm auch nicht zweifelhaft fein, in welcher 
Weife allein eine glückliche Lösung dieser Frage erhofft 
werden konnte. Deshalb berief er eine Reihe befreundeter 
Organisationen und gründete mit ihnen am 20. März 1915 
einen Hauptausschutz für Kriegerheimstätten. 
Welchen Widerhall der Gedanke dieses Ausschusses 
weckte, zeigt feine Entwicklung. Die Zahl der ihm angeschlos 
senen Organisationen, die am 20. März 28 betrug, ist in 
kaum sechs Monaten auf über 1700 gestiegen. Unter diesen 
Mitgliedern sei nur eines hervorgehoben: der „Reichs 
verband deutscher Städt e", der 761 deutsche 
Städte unter 26 000 Einwohnern umfaßt. Er begründete 
seinen Beitritt: 
„Das Heimatbewußtsein, die Vaterlandsliebe soll dem 
Krieger und seiner Familie durch den Besitz eines Stückes Heimatboden 
gestärkt werden, seine Kinder sollen unter besseren Bedingungen als 
bisher aufwachsen können, zum Segen der Volksgesundheit, der 
Wehrkraft und der Volkssittlichkeit. Die Heimstätten sollen zu 
gleich dazu dienen, das Deutschtum an den Grenzen zu sichern 
und zu stärken, sowie den Zug vom Lande und den kleineren 
Städten nach den Großstädten zu hemmen. 
Es handelt sich um eine nationale Angelegenheit im höchsten 
Sinne und zugleich um eine solche, die auch den Interessen der 
kleineren Städte dienen kann." 
Als Richtlinien stellte der Hauptausschutz folgende 
„Grundsätze" auf: 
1. Das Reich dankt seinen Verteidigern, indem es jedem deutschen 
Kriegsteilnehmer oder seiner Witwe die Möglichkeit eröffnet, auf dem 
vaterländischen Boden ein Familienheim ans eigener Scholle (Krieger 
heimstätte) zu erringen. 
Die Kriegerheimstätten sollen, gemäß den Lehren dieses Lüuternngs- 
krieges, das deutsche Boden- und Siedlungswesen auf das Ziel hin 
lenken, einen körperlich und sittlich gesunden Volksnachwuchs zu sichern