228 
Aber 11% Verzinsung! Das bedeutet eine so hohe 
Rentabilität der Anlagen, daß man schon für größere Sied 
lungsflächen mit vielleicht ungünstigeren Durchschnitts 
bedingungen einen hohen Satz abrechnen kann, um doch noch 
eine planmäßige, wirklich große Kulturarbeit, die für das Ge 
meinwohl dringend notwendig ist, auch wirtschaftlich als ge 
rechtfertigt erscheinen zu lassen. 
Wenn zu solcher Kultivierung Strafgefangene ver 
wendet würden, so ließe sich dagegen wenig einwenden. 
Hier würden sie nicht den Lohn ehrlicher Arbeit drücken. 
Hier fertigten sie nicht Waren, die durch ihre Billigkeit freien 
Gewerbetreibenden schädlichen Wettbewerb bereiten. Die Ge 
fangenen selbst wären hier gesundheitlich gewiß besser daran 
als in der naturgemäß oft schlechten Anstaltsluft. 
Wichtiger noch ist die sittliche Seite. Die große Mehr 
zahl der Vergehen stammt von Menschen, die wurzellos ge 
worden sind, im eigentlichen Sinne „elend", d. h. ohne Land. 
Können sie sich nun durch die Verwendung bei der Feldarbeit 
landwirtschaftliche Kenntnisse erwerben, so wird eine ver 
ständige Gesetzgebung hier Wege finden, um ihnen den Er 
werb von Heimstätten zu ermöglichen, die den Besten unter 
ihnen den Weg erschließen würde, wieder wurzelfeste, 
nützliche Mitglieder unseres Volkstums zu werden. Gerade bei 
der Gewinnung neuen Landes, wie es in anderer Weise an 
der holsteinischen Küste bei der Gewinnung der „Kooge" vor 
sich geht, sollten überall Rechtsformen gefunden werden, die 
die Schäden ausschließen, an denen heute unsere Landwirt 
schaft krankt. Für diese neuen Stellen muß von vornherein 
Bodenreformrecht maßgebend sein, das die Spekulation mit 
dem Boden und seine Überschuldung unmöglich macht.