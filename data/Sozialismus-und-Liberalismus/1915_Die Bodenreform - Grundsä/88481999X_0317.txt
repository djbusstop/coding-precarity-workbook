297 
(132), Publius Popillius, obwohl selbst ein Gegner der 
Gracchen, rühmte auf einem öffentlichen Denkmal: ich bin 
der erste gewesen, der bewirkt hat, daß statt Sklavenhirten 
auf dem Staatsland Bauern angesiedelt wurden. Von 
welchem Segen dieses Stück Bodenreform war, enthüllt 
die eine Tatsache, daß die Zahl der waffenfähigen Bürger, 
die 131 nur noch 318 823 betrug, schon 125 auf 394 736 ge 
stiegen war, also um etwa 76 000 zunahm. 
Daß bei dem Reformwerk einzelne Rechte verletzt, ein 
zelne Härten nicht vermieden wurden, mag zugegeben werden. 
Aber wo ist eine große Reform jemals ohne Härten durch 
geführt worden? Der Widerstand wuchs natürlich, je mehr 
das Teilungsgeschäft fortschritt. Von den Optimalen wurde 
Aug jeder etwaige Fehlgriff des Bodenreformausschusses 
übertrieben, neue Hindernisse künstlich hervorgerufen und 
die öffentliche Meinung planmäßig beeinflußt. 
Es war Scipio Africanus, der Schwager der 
Gracchen,der Führer der Mittelpartei, bei dem die Entscheidung 
lag. Der gefeierte Feldherr neigte seiner ganzen aristokratischen 
Persönlichkeit nach zu den Optimalen. Ihn verletzte schon 
jeder Formfehler, der von der Gegenseite gemacht wurde 
er erkannte nicht, um welche Entscheidungen es sich han 
delte. Genug, er setzte es durch, daß der Teilungskommission 
die richterliche Befugnis genommen wurde, zu entscheiden, 
was Staatsland und was Privateigentum sei. Damit war 
die Bodenreformkommission zur Untätigkeit verdammt. 
Eine tiefe Erbitterung ergriff die Volkspartei. In 
diesen Tagen kündigte Scipio eine größere Rede an. Am 
Vorabend zog er sich früher als sonst in sein Schlafgemach 
Zurück. Was sich in dem Dunkel dieser Nacht abgespielt hat,