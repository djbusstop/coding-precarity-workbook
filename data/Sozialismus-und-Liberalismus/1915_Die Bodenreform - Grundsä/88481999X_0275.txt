Seite mit Henry George in Amerika für den Gedanken der 
Bodenreform kämpfte. Daraufhin beauftragte Papst 
Leo XIII. seinen Legaten, den Erzbischof Satolli, die 
Bodenreformlehre zu untersuchen. Dieser berief vier Pro 
fessoren der katholischen Universität Washington als Gut 
achter. Nach sorgfältiger Prüfung erklärten sie einstimmig, daß 
in der Lehre der Bodenreform nichts enthalten sei, was 
gegen das Dogma und die Moral der katholischen Kirche 
verstoße. Der Papst bestätigte das, als er in Rom Mc. Glynn 
persönlich freundlich empfing. 
^^as eine soll bleiben: Jede ernste Beschäftigung mit der 
Gesetzgebung des Moses, in dem drei Weltreligionen: 
das Judentum, das Christentum und der Islam den Dol 
metscher des Ewigen sehen, führt zu der Erkenntnis, daß 
in ihr der Bodenwucher nicht milder aufgefaßt wird als Mord 
und Raub und Gotteslästerung. Dieselben Verheißungen 
und dieselben Strafen gelten hier und dort. 
Wahrlich, die Bodenreformer haben ein Recht, sich als 
die Vertreter der höchsten Sittengesetze anzusehen, wenn sie 
nach Reformen ringen, die es ermöglichen sollen, „daß 
unser Volk genug zu essen habe und in dem Lande, das der 
Herr ihm gegeben, sicher wohne", wenn sie in unserer Zeit 
ihre Pflicht erfüllen im Kampfe um die deutsche Boden 
reform !