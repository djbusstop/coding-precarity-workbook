190 
fesiigungsgesetz vorn 26. Juni 1912 überwies dann weitere 
100 Millionen JÍ zu Entschuldungszwecken. 
Die Entschuldung geht auf dem Wege der Rentenguts 
bildung vor sich. Will ein Besitzer sein Grundstück „regulieren 
lassen", so erteilt er der zuständigen Besitzfestigungsbank 
Vollmacht, sein Grundstück an den Staat aufzulassen. Dieser 
überlätzt es wiederum dem Eigentümer, aber als Rentengut, 
und zwar wird in der Regel eine dauernde, d. h. nur mit 
Zustimmung beider Teile ablösbare Rente in Höhe von einer 
Mark vereinbart. Mit der 1 Mark-Rente wird für den Staat 
ein dingliches Wiederkaufsrecht begründet, damit er jeden 
Mißbrauch mit dem entschuldeten Besitz verhindern kann. 
Mit dieser Umwandlung wird nun eine Ordnung der Hypo 
thekarverhältnisse nach folgender Richtung bewirkt: mäßige 
Verzinsung des Kapitals, seine jährliche Tilgung und Unkünd 
barkeit seitens des Gläubigers. Zur Durchführung der Ent 
schuldung wird zunächst der Kredit der gemeinwirtschaft 
lichen Institute in Anspruch genommen: der Landschaften, 
der Provinzialhilfskassen, der Provinzialfeuersozietäten 
usw. An zweiter Stelle gewährt der Staat aus dem oben 
genannten Fonds ein mit 3% % verzinsliches Kapital, das 
bei bäuerlichen Gütern mit % %, bei größeren Gütern 
mit iy 2 % getilgt werden muß. 
Vorbildlich erscheint die Verbindung der öffentlich- 
rechtlichen Institute mit den freien Organisationen genossen 
schaftlicher Selbsthilfe. Die landwirtschaftlichen Genossen 
schaften werden zur Abschätzung des Wertes des Grundstücks, 
das bis zu 75% beliehen wird, herangezogen, und sie über 
nehmen die selbstschuldnerische Bürgschaft für die dem Staat 
zu zahlende Rente. Diese Genossenschaften und Spar- und