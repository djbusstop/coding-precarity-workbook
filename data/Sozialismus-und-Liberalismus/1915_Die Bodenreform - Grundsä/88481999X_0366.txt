346 
auf sein Herzleiden hinwies. Aber George war entschlossen. 
Als die Abgeordneten der ihm befreundeten Organisationen 
vor ihm standen, erklärte er: „Ich folge diesem Ruf, und 
wenn ich dafür sterben sollte". 
Es war ein Kampf von höchster Bedeutung, der nun 
begann. Selbst große deutsche Tageszeitungen, die sonst 
den einzelnen Wahlen in Amerika wenig Beachtung zu 
schenken pflegen, verfolgten diesen Wahlkampf mit größter 
Aufmerksamkeit. Die Berliner „Kreuzzeitung" wies darauf 
hin, daß ein Sieg Henry Georges in Groß-New-Pork Folgen 
mit sich führen müßte, deren Tragweite gar nicht abzusehen 
wäre. George verschmähte es auch in diesem Kampf durchaus, 
besonderen Interessen zu schmeicheln. So wurde er einmal 
einer Versammlung von 1200 Arbeitern als Arbeiterfreund 
vorgestellt. Er aber sagte: „Ich habe nie beansprucht, ein 
besonderer Freund des Arbeiters zu heißen. Auch jetzt be 
anspruche ich es nicht!" Totenstille trat ein. „Ich habe nie 
besondere Arbeiterinteressen vertreten und werde sie nie ver 
treten." Lautlose Stille. Henry George schritt die Bühne 
ab und, sich an die Versammlung wendend, rief er: „Ich 
trete ein für die Rechte aller Menschen — für gleiche 
Rechte für alle. Laßt uns hinfort keine Sonderrechte fordern, 
weder für Kapitalisten noch für Arbeiter!" — Die Menge 
brach in solchen Jubel aus, daß das Gebäude erzitterte. 
Am 2. November 1897 sollte die Entscheidung fallen. 
In den Tagen vorher häuften sich die Vorträge. Am 28. Ok 
tober sprach George in vier großen Versammlungen. Die 
letzte fand im Central Opera House statt, wo etwa 5000 
Menschen versammelt waren. Er konnte hier aus den anderen 
Versammlungen erst nach 10 Uhr eintreffen. Vor seiner An-