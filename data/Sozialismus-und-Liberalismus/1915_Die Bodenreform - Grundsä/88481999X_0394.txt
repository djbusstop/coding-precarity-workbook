für die zweckmäßigste Stadterweiterung ein. Welche Wichtig 
keit er dieser Aufgabe beilegte, ergeben die Namen der Aus 
schußmitglieder: Dankelmann, Grumbkow und der 
Erbauer des Zeughauses, Neh ring. Diese entwarfen den 
Plan zu dem Stadtteil, der des Königs Namen noch heute trägt, 
der Friedrichstadt. Seine Anlage und Bebauung schritt schnell 
vorwärts. Bereits in den ersten Jahren des 18. Jahrhunderts, 
also vor etwa 200 Jahren, wurden die ersten kleinen Häuschen 
in der Leipziger Straße errichtet. Die Anlegung dieses neuen 
großen Stadtteils in Verbindung mit dem Ausbau der 
drei unter dem Großen Kurfürsten angelegten Stadtteile 
ermöglichte ein ganz außerordentliches Wachstum der Stadt, 
ohne daß sich Mißstände bemerkbar machten. Als im Jahre 
1709 Friedrich I. die bisher von besonderen Magistraten 
verwalteten Stadtteile Berlin, Kölln, Friedrichswerder, 
Dorotheenstadt und Friedrichstadt zu einem Ganzen ver 
einigte und einem Magistrat unterstellte, hatte sich die Zahl 
der Häuser, die 1648 etwa 1000 betragen hatte, auf 4000 ver 
mehrt. Die Einwohnerzahl war von 1688—1709 von 18 000 
auf 55 000 gestiegen, d. h. sie hatte sich in diesen zwanzig 
Jahren um mehr als 300 % vermehrt — ein Wachstum, 
das auch in der Entwicklung des 19. Jahrhunderts seines 
gleichen nicht hat. 
Und dabei blieben die Mieten sehr gering. Der 
erste Prediger an der Georgenkirche erhielt z. B. jährlich 
20 Taler Mietsentschädigung. Die Ursache dieser gesunden 
Verhältnisse lag in der Regelung der Bodenfrage. Das Ent 
eignungsrecht zum Ackerwerte ließ die Preise des Bodens 
stets in den allermäßigsten Grenzen bleiben. Als die St- 
Georgengemeinde 1693 den fünf Morgen großen Kirchhofe