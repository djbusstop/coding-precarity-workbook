ì'ķMWWMMMiWWWDMMWWWMâŞààŞŞWWDW 
— 185 — 
dem Wesen dieser Hypothek, daß im wesentlichen nur die 
Gemeinschaft als Geldgeber in Betracht kommen 
kann. Ob Reich, Staat oder Provinz, Kreis oder Gemeinde 
oder ein Zusammenarbeiten dieser Faktoren in einer Ge 
nossenschaft von öffentlich-rechtlichem Charakter diese Orga 
nisation des Realkredits am besten bewirken, wird zuletzt 
die Praris allein entscheiden können. 
Die allmähliche Abtragung der bestehenden Schulden 
wird aber nur von vorübergehender Wirkung sein, wenn 
nicht auch Vorkehrungen getroffen werden, einer abermaligen 
Verschuldung vorzubeugen. In solcher Erkenntnis beschloß 
im Dezember 1903 der österreichische Landwirtschaftsrat, 
die offizielle Vertretung der Landwirtschaft: 
„Aus der Erwägung heraus, daß die kündbare Kapitalhypothek 
der Eigenart des landwirtschaftlichen Betriebes überhaupt widerspricht, 
daß namentlich die kündbare Nachhypothek eine stetige Bedrohung des 
Besitzes bedeutet, daß endlich die hohe Verschuldung überhand nimmt, 
solange man nicht die Landwirte zur Tilgung ihrer Hypotheken 
verpflichtet, ist man nach reiflicher Überlegung zu der Überzeugung 
gekommen, daß alle Bemühungen, durch eine planmäßige Ent 
schuldungsaktion der landwirtschaftlichen Schuldennot abzuhelfen, nur 
dann zu dauernden Erfolgen führen können, wenn es gelingt, eine 
übermäßige Neubelastnng des Bodens zu verhüten. Dieses Ziel 
glaubt man ohne kreditrechtliche Beschränkung der Verschul 
dungsfreiheit nicht erreichen zu können." 
wichtigste Voraussetzung dazu ist die Schaffung 
einer wirksamen Verschuldungsgrenze. Am 
einfachsten und klarsten würde diese nach einer Neueinschätzung 
der gesamten Bodenwerte zu finden sein. Wertvolle Vor 
arbeiten dazu bilden die regelmäßigen Veranlagungen, die 
3- B. in Preußen zum Zweck der Vermögenssteuer vorge-