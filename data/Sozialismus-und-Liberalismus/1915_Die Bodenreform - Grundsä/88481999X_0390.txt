370 
aber erhöhte sich der Ertrag der Akzise jährlich um 12 000 
Taler. 
Um „der Städte Nahrung, Wohlstand, Handel und 
Wandel zu heben", mußte man natürlich möglichst viel Men 
schen in die Städte ziehen. 
Der Große Kurfürst versuchte es. — 1661 ließ er ein 
Edikt ergehen, das aus „Landesväterlicher Liebe allen den 
jenigen, so wüste Stellen anzunehmen, zu bebauen und also 
sich in diesen Unseren Landen häußlich nieder zu lassen 
vorhabend seyn sollten, einige empfindliche Ergötzlichkeit", 
nämlich sechsjährige Freiheit von allen Lasten (Kontribution, 
Schoß, Einquartierung, Servies usw.), sowie die Gewährung 
von Bauholz, in Aussicht stellte. Aber diese „empfindlichen 
Ergötzlichkeiten" waren nicht genügend, um eine wesentliche 
Erhöhung der Einwohnerzahl zu bewirken. Nach einer alten 
Bodenreformwahrheit dienten sie lediglich dazu, die Preise 
der „wüsten Stellen", d. h. der Bauplätze, zu erhöhen. 
Der Große Kurfürst erkannte das bald. Er war nicht 
der Mann, der sich durch den Mißbrauch mit dem Boden in 
seinen Plänen beirren ließ, und so erließ er 1667 das be 
deutendste Bodenreformedikt jener Zeit. Er versprach noch 
einmal Freiheit von allen Lasten, unentgeltliche Verleihung 
des Bürgerrechts, und fuhr dann fort: 
„Weil wir vernehmen, daß viele . . . darüber abgeschrecket 
werden, weil ihnen die wüsten Stellen nicht umbsonst gegeben, 
sondern theuer angeschlagen, auch wohl gar die Schöße- und Aon- 
tributionsrechte gefordert werden sollen, also verordnen wir hier 
mit allen und jeden, so aufbauen wollen, die wüsten Stellen freV 
umbsonst und ohne einiges Entgelt zu geben und anzuweisen, auch 
ihnen wegen der alten restierenden Schöße und Aontributionen . - " 
nichts abzufordern ... (Es wäre denn, daß etwann noch Leute