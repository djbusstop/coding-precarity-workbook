276 
entscheiden. Da endlich gelang es den Oligarchen, einen 
Schergen zu bewegen, die Schlinge um den Hals des Königs 
zu ziehen. „Höre auf zu weinen", waren seine letzten Worte 
zu einem Gerichtsdiener, der laut schluchzte, „erleide ich auch 
wider Recht und Gesetz den Tod, so bin ich doch besser als 
meine Mörder!" 
Jetzt eilten die Führer zur Pforte und beruhigten die 
Volksmenge: es solle dem Könige kein Leid widerfahren. 
Als die Frauen forderten, den geliebten Gefangenen zu 
sehen, wurde ihnen sofort der Eintritt gestattet. Die Volks 
menge, durch dieses Zugeständnis versöhnt, zerstreute sich. 
Da führte man zuerst die greise Großmutter Archimadia in 
das Totengemach und erwürgte sie. 
In froher Hoffnung, nun den Sohn wiederzusehen, 
betrat Agesistrata die Totenkammer. Wie entsetzlich der An 
blick war, sie ertrug ihn ohne eine Träne, ohne ein Wort 
der Klage Sie löste den Leib der Mutter von der Schlinge 
des Henkers, bettete die Mutter neben den Sohn, verhüllte 
die Toten und drückte der Mutter die Augen zu. Dann küßte 
sie des Königs Antlitz: „Deine Milde und Nachsicht, mein 
Sohn, haben dich und uns in das Verderben gestürzt!" 
Auch sie mußte sterben. Stolz faßte sie die Schlinge und legte 
sie sich selbst um den Hals mit den Worten: „Möge es Sparta 
zum Heile gereichen!" 
Noch war die letzte von den drei Frauen übrig, die den 
Gedanken der Bodenreform mit dem Könige durchzuführen 
versucht hatten. Es war die junge Witwe des Königs, die 
schöne Agiatis. Um das Erbe des Gemordeten mit einem 
Scheine des Rechts an sein Haus zu bringen, verlangte 
König Leonidas, daß sie seinem kaum dem Knabenalter ent-