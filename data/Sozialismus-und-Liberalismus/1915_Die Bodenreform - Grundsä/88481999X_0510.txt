490 
Hheognis. 258, 259. 
Thüngen, v. 201. 
Thurn und Taxis, Prinz v. 114. 
Tiele-Winckler, Graf. 396. 
Tirpitz, v. Staatss. 414. 
Tolstoj. Leo, Graf. 351, 352, 424. 
Trevelyan, Minister. 354, 438, 
439. 
Turgot. 389, 390, 393. 
Ajest, Herzog v. 404. 
Marro. 287. 
Victor, I. K. 407. 
Völker, Heinrich, Steinmetzmstr. 
156. 
Wagner, Adolph, Exz. 94,119— 
121, 145, 458. 
Wagner, Heinrich, v., Oberbgmstr. 
137—139. 
Waldstein, Abg. 122. 
Wallace, Alfred Ruffel. 353. 
Wedell, v. General. 380. 
Wegener, Physikus. 8. 
Weiskopf, H. 9. 
Wellington. 437. 
Wermuth, Exz., Reichsschatzsekr. 
120, 122. 
Wilhelm I-, Kaiser. 462. 
Wilms, Oberbgmstr. 128. 
Wißmann, Hermann v. 407, 408. 
Wolf, Julius, Prof. 15. 
Zeller, H. v. 180. 
Zobel, Stadtv. 108. 
Zollmann, Pastor. 245, 247.