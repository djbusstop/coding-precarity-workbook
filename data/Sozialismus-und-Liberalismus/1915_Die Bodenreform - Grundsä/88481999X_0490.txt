470 
Die „Berliner Volkszeitung" 1914, Nr. 464: 
„Die Hausbesitzerin Redepenning in Stettin, die auf der Ober 
wieck mehrere Häuser besitzt, schickte an die in ihren Häusern wohn 
haften Mietparteien einen Brief, worin sie, wie man uns mitteilt, 
eine Mietsteigerung von durchschnittlich 4 Ji im Monat ankündigte: 
Stettin, den 1. September 1914. 
Herr und Frau N. N.! 
Die gewaltige Wendung, die die Gnade des Allmächtigen Gottes, 
unsere durch seine Macht und Kraft bewaffneten Truppen uns er 
rungen haben, lassen uns in eine große gesegnete kommende Zeit 
blicken. Möchte unser Volk soviel Gnade nie vergessen, nie den alten 
Gott, der Staat und Volk vor allem Übel bewahrt. Ihre Wohnung 
kostet vom 1. Oktober ab 30 Ji. 
Achtungsvoll Frau Redepenning." 
Die „Kieler Neuesten Nachrichten" 1914, Nr. 257 ent 
halten folgende Anzeige: 
„Spekukationsterrain! 
Ich will einen kleinen Betrag anlegen in 
Terrain, welches nach dem Kriege bald ver 
wertet werden kann. Nur billige Angebote 
haben Zweck. Angebote erb. unter ..." 
Die „Allgemeine Zeitung" in Chemnitz bringt in Nr. 4 
von 1915 diese Anzeige: 
„Neue Gründerzeit! Für aussichtsreiches Unternehmen 
werden sofort 100000 Jl gesucht. Es handelt sich um in 
dustrielle und Grundstücksspekulation. Risiko ausgeschlossen. 
Stille oder tätige Beteiligung oder feste Dividende nach 
Wunsch. Großzügig veranlagte Reflektanten werden . . 
Was die gewerbsmäßige Terrainspekulation von einem 
glücklichen Kriege erhofft, lassen solche Anzeigen deutlich er 
kennen. Das sprechen aber auch die hervorragendsten Ver 
treter des Bodenhandels bei aller gebotenen Zurückhaltung 
offen aus. Als die führenden Vertreter der Berliner Terrain-