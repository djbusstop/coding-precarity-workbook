V%Aas Malthus dazu führt, fein Bevölkerungsgesetz für so 
fest begründet anzusehen, wie eine „unbezwingbare 
Festung", das ist seine Anschauung von dem „Gesetz der Produk 
tion auf Land" oder dem „Gesetz der abnehmenden Erträge". 
Nach ihm wird überall der beste Boden zuerst in Angriff ge 
nommen. Die Kultur des minder guten Bodens, zu dem 
die Menschen fortschreiten müssen, erfordere mehr Kapital 
und Arbeit und doch müsse „im Verhältnis, wie die Kultur 
sich ausdehnt, die Zunahme der früheren Durchschnittspro 
duktion allmählich und regelmäßig abnehmen". Der Ertrag 
einer Fläche sei nicht nur endlich beschränkt, sondern es müsse 
auch, jede Steigerung des Ertrages durch Aufwendung 
von verhältnismäßig mehr Arbeit und Kapital erkauft 
werden. 
Dieses „Gesetz der Produktion auf Land" ist wohl richtig 
— unter einer Bedingung: Arbeit und Kapital werden stets 
in unveränderter Weise angewandt. Sobald aber die Arbeit- 
und Kapitalverwendung eine andere wird, tritt dadurch eine 
Tendenz in Wirksamkeit, die der Tendenz der sinkenden Er 
träge widerstreitet. Das spricht Malthus selbst einmal 
im 10. Kapitel des ITI. Buches aus: 
„Ein verbessertes Kultursystem kaun beim Gebrauch besserer 
Geräte eine lange Zeit die Tendenz einer ausgedehnten Kultur und 
einer großen Kapitalzunahme, geringere Verhältniserträge zu liefern, 
mehr als aufwiegen." 
Er ist aber diesem Gedanken nicht nachgegangen und hat 
namentlich nicht erwogen, ob eine Wechselbeziehung zwischen 
der Vermehrung der Bevölkerung und der Verbesserung des 
Kultursystems bestehe. Das aber ist in vielfacher Beziehung 
zweifellos der Fall.