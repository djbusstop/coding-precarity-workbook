﻿5

über das Maß dessen hinausgegangen sei, was dieselbe Anzahl
von Arbeitern früher bei 12- und isstündiger Arbeitszeit ge-
leistet hätte. Er führte das Resultat auf die größere Arbeits-
energie zurück.

Nachdem die Verfasser eine weitere Anzahl ähnlicher Bei-
spiele gegeben haben, kommen sie auf Grund der günstigen Er-
fahrungen zu dem allgemeinen Schluß: Die allmählichen Verkür-
zungen der Arbeitsstunden, welche in diesem Jahrhundert durch-
geführt wurden, sind nach einem sehr kurzen Zwischenraum von
einer positiven allgemeinen Vermehrung der Produktivität be-
gleitet gewesen1). Der Achtstundentag sei ökonomisch möglich,
weil in keiner Weise eine Einbuße am nationalen Produktions-
ertrag nachzuweisen sei; »Wir haben gezeigt, daß kein Grund
ist, den Verlust unseres auswärtigen Handels zu befürchten oder
einen Rückgang in den Löhnen der Arbeiter oder ein Steigen der
Preise. Kurz, daß wir ruhig das Experiment des Achtstunden-
tags ohne die geringste Befürchtung eines nationalen Verlustes
ins Auge fassen können.« Auch Rae kommt zu ähnlichen Er-
gebnissen in seiner Schrift über den Achtstundentag2).

2. Vertreter der zweiten Gruppe: Abbe und v. Buch.

Während in den Werken von Webb und Cox und Rae
über die Erfahrungen berichtet wird, die in einzelnen Industrie-
betrieben mit dem Achtstundentag gemacht wurden und auf
Grund dieser Erfahrungen der Schluß gezogen wird, daß der
Achtstundentag auch generell ohne Schädigung der Unternehmer
durchgeführt werden könnte, haben zwei andere Autoren ver-
sucht, den Achtstundentag theoretisch zu begründen und eine
gewisse Gesetzmäßigkeit festzustellen, kraft welcher der
Achtstundentag das Optimum der Leistungsfähigkeit dar-
stellen soll.

Der eine Autor ist Abbe, der als Leiter der Zeißschen
Werke in Jena den Versuch praktisch durchgeführt hat und auf
Grund der hierbei gemachten Erfahrungen und Beobachtungen
diese Gesetzmäßigkeit zu konstatieren glaubte. Der andere Autor
ist von Buch, der als nationalökonomischer Theoretiker auf
Grund einer Arbeitstheorie ein Optimum der Arbeitsintensität
feststellte.

*) a. a. O., S. 103.

2) Light hours for work. Übersetzt von Borchardt. Weimar 1897.