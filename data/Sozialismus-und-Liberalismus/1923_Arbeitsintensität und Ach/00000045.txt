﻿37

III.	Über die internationale Verschiedenheit der Arbeitsintensität.

Die Befürworter des Achtstundentags erblicken eine Schwierig-
keit häufig in den Rücksichten auf die internationale Konkurrenz.
Seit Jahrzehnten schon war in immer weiteren Kreisen die Über-
zeugung durchgedrungen, daß nur auf dem Wege einer inter-
nationalen Arbeiterschutzgesetzgebung der Achtstundentag durch-
geführt werden könne. Die Argumentation lautet etwa so: Wenn
ein Land für sich den achtstündigen Maximalarbeitstag einführt,
dann wird es in der Konkurrenz auf dem Weltmarkt von den
Ländern geschlagen, die den neun-, zehn- oder elfstündigen Ar-
beitstag haben. Würden dagegen alle Länder den Achtstunden-
tag einführen, dann wären auch die Konkurrenzbedingungen der
Länder untereinander gleiche.

Georg Adler, der in energischer Weise für internatio-
nalen Arbeiterschutz eintrat, argumentiert folgendermaßen: »Heute
wird in jedem Kulturlande nicht bloß für den eigenen Be-
darf, sondern auch für denjenigen fremder Länder produziert.
Außerdem müssen die Industrien jedes Landes, soweit es nicht
Schutzzölle hat, auf dessen eigenem Gebiete mit den ausländischen
Industrien konkurrieren. Gesetzt nun den Fall, der Staat A habe
eine sehr entwickelte Arbeiterschutzgesetzgebung, etwa den zehn-
stündigen Maximalarbeitstag und das Verbot der Kinderarbeit,
während der Staat B der Ausnützung seiner Arbeiter in Fabriken
und Werkstätten keine oder nur geringe Schranken setzt, so kon-
kurrieren die Industrien des Staates A mit denjenigen des Staates B
unter Bedingungen, die rein infolge der Gesetzgebung von A für
dessen Gewerbezweige ungünstig sind. Denn jeder Unternehmer
des Landes A ist, infolge des Verbots zu langer Arbeitszeit, nicht •
in der Lage, sich bei Vergrößerung seiner Produktion alle jene
bedeutenden, früher angeführten pekuniären Vorteile zu verschaffen,
welche eine Folge gerade der durch Verlängerung der Arbeits-
zeit bewirkten Vergrößerung des produzierten Warenquantums
sind1).« Er schlägt vor, in dem Programm einer internationalen
Vereinigung für Arbeiterschutzgesetzgebung einen effektiven Maxi-
raalarbeitstag von zehn Stunden für alle erwachsenen Fabrikarbeiter
und Fabrikarbeiterinnen aufzunehmen2).

Ein allgemeiner schweizerischer Arbeitertag, der im Jahre 1883 * S.

*) Georg Adler, Die Frage des internationalen Arbeiterschutzes

S. 22.

*) a- a- O., S. 53-