﻿i5

können, sind eigentlich immer nur die körperlichen Begleiterschei-
nungen seelischer Vorgänge1).«

Aus diesem Grunde dürfen wir auch nicht allzuviel Hoff-
nung auf die neuere Wissenschaft der sogenannten »industriellen
Physiologie« setzen, die in letzter Zeit, besonders in England und
Amerika gepflegt wird. Namentlich der Amerikaner Lee hat in
seiner Abhandlung »The New Science of Industrial Physiology«
(Public Health Reports 1919) auf die große Bedeutung dieser
neuen Wissenschaft hingewiesen. Unter industrieller Physiologie
will er »die Summe an Kenntnissen verstehen, die wir über das
Wirken des menschlichen Mechanismus bei der industriellen Tätig-
keit erhalten können«. Diese Wissenschaft soll zwei Aufgaben
haben, 1. zu erforschen, wie der industrielle Arbeiter tatsächlich
seine Arbeit verrichtet und welches die Bedingungen sind, unter
denen er seine Arbeit am intensivsten verrichten und dabei seinen
Körper gesund erhalten kann; 2. die mehr praktische Aufgabe,
für die Fabriken die Bedingungen festzustellen, unter denen das
Maximum an Produktionserfolg und Arbeitsleistung des Arbeiters
zu erreichen sei. Exakte Messungen des Arbeitsresultats und
exakte Angaben über die physiologischen Wirkungen der Arbeit
müßten erzielt werden. Die Methoden, deren sich diese neue
Wissenschaft bedienen soll, sollen sein: die Beobachtung und das
Experiment.

Bestenfalls können aber solche Beobachtungen und Experi-
mente einzelne physiologische Daten des Arbeitsprozesses
aufklären, über die wichtigen psychologischen und sozialen Mo-
mente, die dabei mitspielen, aber nichts ermitteln. Deshalb haben
neuere Untersuchungen, die man in England nach dieser Methode
angestellt hat und ausdrücklich »auf physiologischer Basis vor-
genommen« bezeichnet hat, ein so wenig ermutigendes Ergebnis ge-
habt. Es handelt sich besonders um zwei Arbeiten: 1. The second
Interim Report on an Investigation of Industrial Fatigue by Physio-
logical Methods, made by Professor Kent under the auspices of the
Home Office (1916). 2. The Interim Report of the Health of Mu-
nition Workers Committee on Industrial Efficiency and Fatigue
issued last year (x 91 y)2)- Die Ergebnisse der Untersuchungen sind
nur negative, da irgendwelche positive Angaben über Arbeits-

') Erich Stern, Angewandte Psychologie. Aus Natur- und Geistes-
welt. 1921. S. 79.

2) Vgl. Lord Henry Bentinck, Industrial Fatigue, London 1918. S. 8.