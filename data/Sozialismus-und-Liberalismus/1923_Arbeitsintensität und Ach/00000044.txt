﻿36

Arbeitsstunden, im Tagelohn vier. Vielfach wird der Produktions-
rückgang seit der Einführung des Achtstundentags auf 30— 40%
der vorher erreichten Arbeitsleistung beziffert.

Alle die zuletzt angeführten Berichte aus den verschiedenen
Ländern kommen zu dem Resultat, daß es eine allgemeine Ge-
setzmäßigkeit über den Zusammenhang zwischen Arbeitsinten-
sität und Arbeitszeit nicht gibt und daß man eine Normalarbeits-
zeit nicht bestimmen kann. Mögen auch die Untersuchungen im
einzelnen nicht nach der strengsten wissenschaftlichen Methode
vorgenommen sein — die Schwierigkeit einer solchen haben wir
oben nachgewiesen — das negative Ergebnis ist lehrreich genug
und sollte uns vor Generalisierungen auf diesem Gebiete hüten.
Daß solche Verallgemeinerungen immer noch Vorkommen, beweist
die Antwort, die Herr Loucheur, damals Minister für Wieder-
aufbau der französischen Industrie, im April 1919 einer Deputation
von Arbeitgebern gab, die gegen den Achtstundentag sprachen.
Er sagte, er habe den Besuch dreier (!!) großer Industrieller emp-
fangen, die der Textil-, der Maschinen- und der Schwerindustrie
angehörten, und diese hätten ihm aus freien Stücken versichert,
daß der Achtstundentag mit der Erhaltung und selbst mit der
Steigerung der Produktion durchaus vereinbar sei1).

Das Material, das den Untersuchungen zugrunde liegt, ist meist
zur Feststellung exakter Ergebnisse und zu Vergleichungen mit
früheren Zeitperioden oder mit anderen Ländern in keiner Weise
ausreichend. So erklärt der neueste Bericht des Chefinspektors
der Fabriken und Werkstätten in Großbritannien für das Jahr
1920, daß die Berichte über die Wirkungen des Achtstundentags
auf die Produktion aus dem Grunde widerspruchsvoll seien, weil
Änderungen in den Produktionsmethoden, in den Anlagen, Roh-
stoffmangel, Transportverzögerungen und andere Faktoren die
Arbeitsbedingungen ungemein verändert haben und die Vergleich-
barkeit mit früheren Perioden beeinträchtigen2). Solche vage und
allgemeine Behauptungen, wie sie sich z. B. in der neuesten Ver-
öffentlichung der internationalen Vereinigung für gesetzlichen Ar-
beiterschutz finden: »Die Verfechter des Achtstundentags haben
die Erfahrungen der Produktionssteigerung in der Friedensindustrie
für sich3),« sollten in der wissenschaftlichen Literatur nicht mehr
Platz finden.

*) Die Zukunft der Arbeit. Jena 1922. S. 13.

2)	a, a. O., S. 5.

3)	a. a. O., S. 6.