﻿Leistung = i Sekundenmeterkilogramm. Das 7 5 fache dieser Leistung
nennt man Pferdestärke.

Der Nationalökonom muß sich aber bewußt sein, daß, wenn
er diese Arbeitseinheit Erg verwendet, er rein im Gebiet des
Technischen bleibt und für die volkswirtschaftliche Betrachtung der
Arbeit keinen brauchbaren Begriff hat. Der Techniker kann mit
einem einheitlichen Arbeitsbegriff operieren, weil er hierbei nur
mit einer eng begrenzten Art von unqualifizierter Arbeit operiert
und bei ihrer Messung diese Leistung zugrunde legt, wie sie sich
in einer rein äußerlichen Kräftewirkung zeigt. Für den National-
ökonomen ist die »Arbeit« etwas ganz verschiedenes, je nach der
sozialen Stellung des Arbeiters, und danach, ob der Arbeiter mehr
oder minder frei ist, ob er unqualifizierte oder qualifizierte Arbeit,
ob er Handarbeit oder Maschinenarbeit zu leisten hat, ob er im
Zeitlohn oder Stücklohn arbeitet. Alle diese Momente sind zu
berücksichtigen, wenn man den wirtschaftlichen Arbeitsprozeß und
die Arbeitsintensität im einzelnen Falle richtig beurteilen soll. An
der Individualität der einzelnen Arbeitsleistung scheitert daher
auch die Möglichkeit, eine exakte Messung der Arbeitsintensität
vorzunehmen. — Alle neueren Versuche der Physiologie und der
Psychologie, zu einer Messung der Arbeitsintensität zu gelangen,
sind hieran gescheitert.

Auf der Ende des Jahres 1919 abgehaltenen Internationalen
Arbeits-Konferenz in Washington1) erklärte ein Redner (M. Calvo,
Panama): »Das Prinzip des Achtstundentags und der 48 ständigen
Arbeitswoche ist nicht ein vom Verstände geschaffenes Dogma,
sondern das Resultat von vieler Erfahrung und die Frucht wissen-
schaftlicher Beobachtungen, die vermittels des »Ergographen« vor-
genommen worden sind, eines Apparats, der den Grad der Er-
müdung und der Erschöpfung der Arbeiter mißt gemäß den
Kalorien, die sie durch eine genügende Nahrung aufgenommen
haben.« Dies alles muß in der Phantasie des Redners be-
stehen: mir ist keine einzige solche Untersuchung bekannt ge-
worden. Wenn man weiß, was der Ergograph leisten kann, so
weiß man auch, daß er niemals messen könnte, ob wirklich der
achtstündige Arbeitstag die beste Arbeitsintensität darstellt. Der
Ergograph ist ein von dem Italiener Mosso 1890 erfundener Ap-
parat; der den Zweck hat, die Leistung der Muskeln von lebenden
Menschen zu untersuchen. Er wurde auch dazu benutzt, um Er-

1)	Conference Internationale du travail. Washington 1920. S. 65.