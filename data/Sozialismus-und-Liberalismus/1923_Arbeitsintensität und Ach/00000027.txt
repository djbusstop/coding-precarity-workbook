﻿19

schnell generalisierte und übersah, daß diese Versuche unter ex-
zeptionellen Bedingungen gelungen waren und daß sie von Ar-
beitgebern ausgingen, die von starkem sozialpolitischen Eifer an-
getrieben, sich mit Begeisterung für ein soziales Experiment ein-
gesetzt haben.

3.	Der dritte und vielleicht wichtigste Irrtum hängt mit einer
gewissen methodologischen Richtung in der nationalökonomischen
Wissenschaft zusammen. Die Theoretiker des Achtstundentags be-
gehen denselben Fehler, der auch in der klassischen National-
ökonomie so oft begangen wurde. Es ist das Bestreben, ver-
wickelte volkswirtschaftliche Probleme auf möglichst einfache
glatte Formeln zu bringen. Es ist derselbe Irrweg, der bei so
manchen Gesetzen der klassischen Ökonomie begangen wurde, und
der zur Aufstellung des ehernen Lohngesetzes, der Lohn-
fondtheorie, des Produktionskostengesetzes und anderer
derartiger Gesetze geführt hat.

Der Vielgestaltigkeit und Kompliziertheit der wirtschaftlichen
Vorgänge können derartige »Gesetze« niemals gerecht werden.
Auf kaum einem andern Gebiete des Wirtschaftslebens liegen
die Verhältnisse im einzelnen so verschieden -wie auf dem der
Arbeiterfragen. Speziell die Frage der Arbeitsleistung im Ver-
hältnis zur Arbeitszeit zählt zu diesen Problemen. Von irgend-
einem Optimum der Arbeitsintensität in einer bestimmten Stunden-
zahl kann gar keine Rede sein. Zweifellos ist bei zahllosen Be-
trieben bei der Arbeitszeitverkürzung auf acht Stunden eine Ver-
minderung der Produktivität eingetreten.

Ich habe bereits auf die Faktoren hingewiesen, die in den
einzelnen Industriezweigen und bei einzelnen Arbeitergruppen
durchaus verschieden liegen und bei der Frage der Arbeitsinten-
sität beachtet werden müssen. Ich möchte hier namentlich auf
zwei besonders hinweisen.

a) Zeitlohn oder Stücklohn? Die Arbeitsintensität wird
erfahrungsgemäß weit eher gesteigert, wenn der Arbeiter im
Stücklohn arbeitet, als wenn er im Zeitlohn beschäftigt ist. Dies
hängt mit den obenerwähnten psychologischen Momenten zu-
sammen, daß der Willensantrieb, der persönliche Arbeits-
eifer, die Arbeitsfreude und das Arbeitsinteresse eine ent-
scheidende Rolle bei der Arbeitsintensität spielen. Der Arbeiter,
der weiß, daß er in einer bestimmten Zeit ein gewisses Arbeits-
quantum geleistet haben muß, um einen festgesetzten Lohn zu er-
halten, wird alle Kraft daran setzen, auch in kürzerer Arbeitszeit

2*