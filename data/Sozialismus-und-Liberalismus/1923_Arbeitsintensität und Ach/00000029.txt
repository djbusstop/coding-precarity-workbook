﻿auf dem Evangelisch-sozialen Kongreß in Jena1). Seiner Be-
hauptung, daß bei den automatischen Maschinen der Arbeiter viel
weniger von Einfluß auf das Arbeitsresultat sei, wurde lebhaft
widersprochen von Ludwig Bernhard, der erklärte: »Es ist
umgekehrt! Wenn bei der gewöhnlichen Maschine die Verkürzung
der Arbeitszeit die Intensität um 20—30°/0 steigert, so steigert die-
selbe Verkürzung der Arbeitszeit bei der automatischen Maschine
die Intensität um 50—100%. Denn bei der automatischen Maschine
heißt das Problem nicht: der Arbeiter soll schneller arbeiten, son-
dern es heißt: er soll nicht ei ne Maschine bedienen, sondern wenn
er ausgeschlafen hat, kann er zwei oder drei bedienen2).«

Diese Behauptung Bernhards läßt sich nicht aufrechter-
halten. In allen modernen Fabrikbetrieben ist bereits die Organi-
sation so durchgeführt, daß der Arbeiter die Zahl von Maschinen
zu bedienen hat, die technisch möglich ist. Auch die neusten
Erfahrungen über die Arbeitszeitverkürzung haben ergeben, daß
bei den automatischen Maschinen der Ausfall an Arbeitszeit kaum
einzuholen ist. Das zeigt sich sogar innerhalb einzelner großer
Industriezweige, z. B. der Textilindustrie, wo in den Branchen,
wo automatische Maschinen über wiegen, die Arbeitszeitverkürzung
nicht eingeholt wurde, während dies bei den nicht-automatischen
Maschinen der Fall war. Dies ergibt auch eine Vergleichung der
Wollindustrie und der Baum Wollindustrie, worauf ich später zu
sprechen komme. —

3. Vertreter der 3. Gruppe. (Schülers Untersuchung über den
Normalarbeitstag. Bericht des amerikanischen National Industrial
Conference Board über die Arbeitszeit in der Wollindustrie und

andere Berichte.)

In methodisch viel besserer Weise sind eine Reihe anderer
Untersuchungen vorgenommen worden. Sie gehen nicht von den
Erfahrungen einzelner Unternehmungen aus, sondern haben eine
viel breitere Basis für ihre Ermittlungen genommen. Ganze In-
dustriezweige sind in exakter Weise auf die Resultate der Arbeits-
zeitverkürzung untersucht worden. Ich möchte hier zunächst auf
zwei dieser Arbeiten hinweisen:

a) Auf die Untersuchung des Schweizer Fabrikinspektors

*) Verhandlungen, des 17. Evangelisch-sozialen Kongresses in Jena. Göt-
tingen 1906.

2) Ebenda. S. 95.