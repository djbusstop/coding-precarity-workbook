﻿22

Schüler1) und b) auf die vom amerikanischen National Industrial
Conference Board veranstalteten Enquete im Jahre 19182).

a) Schüler. Nach Einführung des elfstündigen Maximal-
arbeitstags in der Schweiz 1878 hatten die Schweizer Fabrik-
inspektoren den Auftrag erhalten, genaue Erhebungen darüber
anzustellen, wie die Einführung des Elfstundenarbeitstags gegen-
über dem früher üblichen zwölfstündigen Arbeitstag auf die Pro-
duktion gewirkt habe. Schüler hat in besonders sorgfältiger Weise
seine Untersuchung durchgeführt. Er hatte sich schon vorher
für die Baumwollspinnerei des Kantons Glarus, in dem bereits
1872 der Elfstundentag eingeführt wurde, an verschiedene In-
dustrielle und Arbeiter gewandt, um bestimmte Zahlenangaben zu
erlangen. In seinem Aufsatz: »Der Norraalarbeitstag in seinen
Wirkungen auf die Produktion« hat er die Plauptergebnisse seiner
eigenen Untersuchungen und die anderer Fabrikinspektoren über
die Schweizer Textilindustrie zusammengestellt. Aus den Ermitt-
lungen teile ich einige von prinzipiell wichtiger Art mit. Aus
den Zahltagheften mehrerer beliebig herausgegriffener Arbeiter
einer gut geleiteten Spinnerei ermittelte Schüler, wie groß die
Spinnleistungen mehrerer Arbeiter in den zwei verglichenen Pe-
rioden 1871 (Zwölfstundentag) und 1872 (Elfstundentag) waren.
Als Resultat ergab sich, daß die Leistungen der einzelnen sehr
verschieden ausfielen. Es spann z. B.

im Rhre 1871/72 (bei 12 Std.) im Jahre 1872/73 (bei 11 Std.)

39 485 = 100 : 98,2
41 504 = 100 ; 99,0
41 187 = 100 ; 97,5

A.	40 208 Pfund

B.	41 929	„

C.	42 245

Die ganze Spinnerei lieferte, in Prozenten ausgedrückt, jähr-
lich 1871/72= ioo°/0, 1872/73 = 99,15°/0 (S. 88). Schüler weist
auf eine Äußerung der Geschäftsleitung hin, daß die Produktion
auch um deswillen gesteigert werden konnte, weil der Gang der
Maschinen verbessert wurde. Ferner ist von Bedeutung, ob die
Arbeiter nach Zeit oder nach Stück bezahlt wurden. Bei ge-
nauer Prüfung vieler Fälle aus anderen Spinnereien stellte
Schüler fest, daß die Einholung des Produktionsausfalles in-
folge der Arbeitszeitverkürzung dadurch bewirkt wurde, daß

*) Schüler, Der Normalarbeitstag in seinen Wirkungen auf die Pro-
duktion. — Archiv für soziale Gesetzgebung und Statistik. 4. Bd.

2) Hours of work as related to output and health of workers. Wool Manu-
facturing Research, Number 12/December 1918. — National Industrial Conference
Board, 1918.