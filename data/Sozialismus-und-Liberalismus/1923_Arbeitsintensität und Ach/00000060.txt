﻿52

Intensivierung ihrer Betriebe beeilten, zu einer starken Produktions-
verringerung führen. . . . Man kann daher wohl sagen, daß eine
entsprechende Verlängerung der Arbeitszeit die Voraussetzung für
die Rationalisierung in vielen Gewerben ist1).«

Es ist zu hoffen und zu wünschen, daß solche Erkenntnisse
in den weitesten Kreisen der Arbeiterschaft immer mehr Gemein-
gut werden. Dann könnten Arbeitnehmer und Arbeitgeber und
Sozialpolitiker in gemeinsamer Einheitsfront für eine gesetzliche
Regelung der Arbeitszeit kämpfen, die zugleich den Anforde-
rungen der Sozialpolitiker gerecht wird, aber auch die so dringen-
den Erfordernisse der Produktivität der deutschen Wirtschaft nicht
außer acht läßt.

Max Cohen-Reuß, Deutsche Arbeit, Deutsches Schicksal. Berlin
1922. S. 15 u, 16.