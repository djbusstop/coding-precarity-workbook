﻿Intensität auf diese Weise nicht zu gewinnen waren. In dem
Bericht des an zweiter Stelle genannten Untersuchungskomitees
heißt es: »Unser industrielles Leben muß in Zukunft in den Einzel-
heiten seiner Durchführung durch die Anwendung der physiolo-
gischen Wissenschaft geleitet werden.« Auf Grund seiner Unter-
suchungen über die Arbeit der englischen Munitionsarbeiter war
dieses Komitee zuerst zum Schlüsse gekommen, daß 65—67 Stunden
pro Woche für Männer, 60 Stunden für die Frauen das Optimum
des Arbeitserfolgs ergäben. In einem zweiten Memorandum
erklärt dasselbe Komitee, daß diese Stundenzahl zu groß sei, daß
es jetzt dem Munitionsminister kein Optimum mehr angeben
werde wegen der Unzulänglichkeit und der Schwierig-
keiten der Untersuchungsmethoden. Im Hinblick auf die Ver-
schiedenheiten in den Arbeitsverrichtungen und in Anbetracht
der Tatsache, daß eine ganze Anzahl anderer Faktoren mit in
Rücksicht zu ziehen seien, wie z. B. die häuslichen Verhältnisse
der Arbeiter und die Wegstrecke zur Fabrik, könne das Komitee
keine feste Stundenzahl dem Munitionsminister empfehlen. Hier-
bei ist zu beachten, daß diesem Komitee im Gegensatz zu pri-
vaten Forschern das denkbar beste Material zur Verfügung stand.
Es hatte das Recht, alle Bücher und Statistiken der Munitions-
fabriken einzusehen, alle Arbeiter und Leiter der Fabriken zu
befragen usw.

So können wir nach dem heutigen Stande der Wissenschaft
sagen, daß ein irgendwie sicheres Ergebnis über den Umfang der
Arbeitsintensität innerhalb einer gewissen Arbeitszeit nicht er-
mittelt werden kann, und daß die von Abbe und Buch auf-
gestellten Gesetze über das Optimum der Arbeitsintensität mit
ganz unzulänglicher Methode gewonnen wurden.

Wie steht es aber mit den angeblich so reichen Er-
fahrungen, die mit dem Achtstundentag gemacht seien, und
die erfahrungsmäßig den Satz beweisen sollen, daß das Optimum
der Arbeitsintensität bei der achtstündigen Arbeitszeit liegt? Da
muß darauf hingewiesen werden, daß die wirkliche Zahl der Fälle,
bei denen Arbeitgeber und Arbeitnehmer übereinstimmend be-
kunden, daß die Reduktion auf acht Stunden keine Einbuße im
Produktionsresultat ergeben habe, eine ganz geringe ist. Es ist
auffallend, daß in der Literatur über den Achtstundentag, sowohl
in der deutschen wie in der ausländischen, es immer ein paar
Dutzend berühmte Fälle sind, die als Paradebeispiele vorgeführt
werden. Wir finden da immer wieder die bekannten Namen der