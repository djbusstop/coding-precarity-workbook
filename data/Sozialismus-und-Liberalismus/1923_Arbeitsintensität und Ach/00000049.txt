﻿4i

energisch aufgegriffen. Im 13. Teil des Versailler Friedensver-
trags ist auch »die Organisation der Arbeit« als eine der Auf-
gaben verzeichnet, welche der Völkerbund in Angriff zu nehmen
habe, und speziell die Festlegung eines Maximalarbeitstags und
einer Maximalarbeitswoche wurde dabei als Ziel ins Auge gefaßt.
Die ursprünglichen Mitglieder des Völkerbunds sollen die ersten
Mitglieder dieser Organisation sein und die allgemeine Konferenz
der Vertreter der Mitglieder soll wenigstens einmal im Jahre zu-
sammentreten. Außerdem soll ein internationales Arbeitsamt am
Sitze des Völkerbunds errichtet werden und einen Bestandteil des
Bundes bilden. Die Vorbereitung der internationalen Arbeitsver-
träge war damit auf das internationale Arbeitsamt in Genf über-
gegangen. Die erste Sitzung der internationalen Arbeitskonferenz
fand 1919 in Washington statt und hatte als ersten Punkt der
Tagesordnung die Anwendung des Grundsatzes des Achtstunden-
tags oder der 48-Stundenwoche festgesetzt. Unter den allgemeinen
Grundsätzen, die von den vertragschließenden Parteien als be-
sonders wichtig und dringend erachtet wurden, findet sich im
Friedensvertrag die Annahme des Achtstundentags oder der 48-
Stundenwoche als Ziel angegeben, das überall angestrebt werden
soll, wo es noch nicht erreicht wurde. Nach eingehenden Ver-
handlungen nahm diese Konferenz einen Vertragsentwurf an, wo-
nach in allen öffentlichen und privaten industriellen Anstalten oder
in ihren Nebenbetrieben, welcher Art sie auch sein mögen, die
Arbeitszeit des Personals acht Stunden im Tag oder 48 Stunden
in der Woche nicht übersteigen sollte. Die Regierungen aller
Staaten, die an dieser Organisation beteiligt sind, hatten die Pflicht,
diesen Entwurf den zuständigen Stellen nach Ablauf von längstens
18 Monaten zur Ratifikation vorzulegen. Bemerkenswert ist, daß
schon in diesem Vertragsentwurf wichtige Ausnahmen vom Acht-
stundentag für einzelne Länder zugelassen waren. Für Japan war
die Maximalarbeitszeit auf 57—60 Stunden pro Woche, für China,
Persien, Siam sollten die Bestimmungen überhaupt keine Anwen-
dung finden, und für Griechenland und Rumänien erst zu einem
späteren Termin als für die anderen Staaten. Die Ratifikation
dieses Beschlusses stieß aber auf erhebliche Schwierigkeiten, und
bis zum 1. Oktober 1921, dem Zeitpunkt der Ratifikation, hatten
überhaupt nur Griechenland, Indien, Rumänien und die Tschecho-
slowakei ratifiziert, alle übrigen Länder die Ratifikation nicht
vorgenommen. Von diesen vier Staaten sind drei solche, denen
schon besondere Ausnahmen zugebilligt waren, und die Tscheche-