﻿2 9

Methode zu verfeinern und die zahllosen Fehlerquellen in einem
erträglichen Grade auszuschalten1). Trotz dieser Erklärungen
sind die in der »Wirtschaftskurve« veröffentlichten Resultate in
der Tagespresse vielfach kritiklos, und zwar meistens zugunsten
des Achtstundenarbeitstags verwandt worden. Wie sehr hier
Vorsicht am Platze ist, ergibt sich, wenn man die einzelnen
mitgeteilten Statistiken über die Arbeitsintensität kritisch prüft.
Was will es z. B. heißen, wenn eine große Maschinenfabrik be-
richtet, daß die Arbeitsintensität vom Jahr 1919 gleich 100 auf 154
im Jahr 1921 heraufgegangen sei, wenn zu diesen Zahlen das
Unternehmen selbst berichtet, daß einerseits die in die Augen
springende Hebung in erheblichem Umfang aus organisatori-
schen Maßnahmen sich erkläre, andrerseits aber die Leistung
im Jahr 1921 durch verschlechterten Geschäftsgang ungünstig be-
einflußt sei. Oder wenn umgekehrt ein bedeutendes Werk der
Metallwarenindustrie das Ergebnis berichtet, daß gegenüber einer
Friedensleistung von ioo°/0 die Leistungen am 1. April 1922 nur
73°/o betragen haben, wenn uns mitgeteilt wird, daß diese Tabelle
das Resultat von acht Einzelrechnungen sei, die voneinander
nicht unwesentlich abweichen2). Richtiger ist im Fall 4 vor-
gegangen. Hier berichtet ein Konfektionsbetrieb über Leistungen
bestimmter einzelner Arbeiter. Hier werden also die Leistungen
einzelner Arbeiter in den verschiedenen Zeitperioden geprüft.
Aber gerade das Schlußresultat zeigt deutlich, wie man hier nicht
generalisieren darf. Denn dieser Bericht zeigt, wie bei den ein-
zelnen Arbeitern sehr verschiedene individuelle Resultate sich er-
geben. So z. B. hat der Arbeiter A, wenn man die Leistung im
ersten Quartal 191g gleich 100 setzt, im ersten Quartal 1921 144
geleistet. Der Arbeiter B hat unter denselben Voraussetzungen
nur 64 geleistet, dagegen ist der Arbeiter C wiederum auf 154
gekommen. Wie man sieht, eine durchaus verschiedene Gestaltung
der Arbeitsintensität.

Wie sehr man sich hüten muß, aus allen diesen Mittei-
lungen irgendwelche allgemeine Tendenzen abzuleiten, ergibt sich
besonders aus der im Heft 3 mitgeteilten Statistik einer Ziga-
rettenfabrik. Aus der Tabelle geht hervor, daß die Anzahl der
Stück pro Arbeiter und Stunde bei der Handarbeit im ersten
Quartal 1914 ns war; dagegen im vierten Quartal 1921 150,

x) a. a. O., 3. Heft, S. 63.

2) a. a. O., 2. Heft, S. 56.