﻿12

müdungserscheinungen bei Schülern festzustellen. Auch hat man
gewisse Versuche mit dem Ergographen angestellt, um den
Zusammenhang von Nahrungsaufnahme und Arbeitsleistung klar-
zustellen und hat dann z. B. gefunden, daß nach der Mahlzeit die
Muskelleistung höher ist als am Vormittag1). Aber niemals kann
der Ergograph mehr leisten, als daß er über gewisse, ganz eng
begrenzte physiologische Vorgänge einigermaßen exakte Beobach-
tungen aufstellt. Gerade, wenn man bedenkt, in wie mühsamer
Untersuchung einzelne Feststellungen mit dem Ergographen ge-
macht wurden, wie z. B. die Muskel Vorgänge bei Fingerbiegung
und Fingerstreckung beschaffen sind und wenn man weiter be-
denkt, daß selbst hierbei, wie Fachleute behaupten, der Apparat
nicht einmal imstande ist, diesen scheinbar so einfachen Vorgang
richtig zu ermitteln, muß man staunen, daß Nationalökonomen
allen Ernstes annehmen, man könne exakt feststellen, daß die
Achtstundenarbeit das Optimum der Leistungsfähigkeit der Arbeit
darstellt. So sagt Müller in einer Abhandlung »Über Mossos
Ergographen«: »Zur Zeit fehlen die aus der Erfahrung geschöpften
Anhaltspunkte, aus denen irgendwelche Relationen zwischen der
zentralen und der im Ergogramm zum Ausdruck kommenden peri-
pheren Ermüdung ableitbar wären. Es fehlt also eine verbindende
Theorie, welche das Instrument in der Hand des Psychologen
brauchbar machen könnte, wenn dieser nicht gerade darauf aus-
geht, die Ermüdung von Muskelgruppen in toto, wie sie eben
physiologisch verläuft, zu bearbeiten* 2).«

Der Ergograph ist überhaupt nur konstruiert für die »phy-
siologische« Ermüdung, welche auf Stoffwechselvorgängen des
Muskels und eventuell der Nervensubstanz beruht; gar nicht kommt
er in Betracht für die »subjektive« Ermüdung, die von der phy-
siologischen zu trennen ist.

Auch Kraepelin hat in seiner bekannten Untersuchung
über die »Arbeitskurve« und ebenso haben die Untersuchungen
seiner Schüler über die Arbeit immer nur die physiologische Seite
des Arbeitsprozesses zum Gegenstand3).

*) Römer, Versuche über Nahrungsaufnahme und geistige Leistungs-
fähigkeit. Psychologische Arb., herausgeg. von Kraepelin, II, S. 695, feraer
III, S. 689. Zitiert bei: Bernhard, Höhere Arbeitsintensität bei kürzerer
Arbeitszeit. (Staats- und sozialwissenschaftliche Forschungen, herausgeg. von
Schmolle r und S e r i n g.) Heft 138. Leipzig 1909.

2)	Robert Müller, Über Mossos Ergographen. Philosophische Studien.
17. Band. 1901.

3)	Emil Kraepelin, Die Arbeitskurve. Philosophische Studien, 19, Bd. 190z.