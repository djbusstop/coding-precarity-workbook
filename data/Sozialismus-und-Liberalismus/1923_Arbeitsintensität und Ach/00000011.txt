﻿3

soll, wird die Wirtschaftlichkeit des Betriebes eher günstig
beeinflußt. Denn durch die Verkürzung der Arbeitszeit wird
gleichzeitig eine gewisse Ersparnis an Generalkosten des Unter-
nehmens erzielt.

2. Der Maximalarbeitstag. Um etwas gänzlich verschie-
denes handelt es sich beim Maximalarbeitstag. Hier soll eine
bestimmte Maximalarbeitszeit pro Tag gesetzlich eingeführt werden,
ganz unabhängig von der Frage, ob mit dieser Festsetzung des
maximalen Arbeitstages eine Schädigung der privatwirtschaft-
lichen und Rentabilitätsinteressen der Unternehmer verknüpft
ist. Gerade wie bei der Einführung der Sonntagsruhe oder bei
der Abschaffung der Nachtarbeit der Frauen soll die Frage der
Rentabilität der Unternehmung in den Hintergrund treten um
höherer Interessen willen. Dieser Maximalarbeitstag wird aus
zweierlei Gesichtspunkten gefordert. Einmal kann es sich um
den hygienischen oder sanitären Maximalarbeitstag
handeln. Dann liegt eine Maßnahme des Arbeiterschutzes vor;
es wird gesagt, daß es im Interesse auch der erwachsenen
männlichen Arbeiter Hegen müsse, daß die Arbeitszeit nicht
ein gewisses Maß übersteige, um nicht die Gesundheit der
lebenden und der kommenden Generationen zu gefährden. Von
diesem Gesichtspunkt aus hat man schon vor längerer Zeit den
gesetzlichen Maximalarbeitstag eingeführt in Frankreich, in der
Schweiz, in Österreich. In Deutschland hatte man bereits vor
dem Kriege den Maximalarbeitstag in anderer Weise durch-
geführt. Ein gesetzlicher Maximalarbeitstag wurde nicht einge-
führt, aber die Reichsgewerbeordnung enthielt die Bestimmung,
daß der Bundesrat in allen den Betrieben, wo eine zu große
Arbeitszeit gesundheitsschädliche Wirkungen hervorbringt, eine
Herabsetzung der Arbeitszeit verfügen kann.

Neben diesem sanitären oder hygienischen Arbeitstag gibt
es noch den kulturellen Maximalarbeitstag. Er wird im Inter-
esse der Kultur verlangt, damit die große Masse der Arbeiter-
schaft die nötige Muße habe, um neben der Berufsarbeit ein wirk-
liches Kulturleben führen zu können. Auch hier soll wiederum
die Rentabilitätsfrage vollkommen ausschalten, man verlangt unbe-
schadet der privatwirtschaftlichen Interessen der Unternehmer,
daß der Arbeiter nicht länger als acht Stunden für die tägliche
Lebensnotdurft arbeiten soll. In dem Sinne, wie es schon das
alte, englische Arbeitersprichwort sagt: Fight hours to work, eight
hours to rest, eight hours to sleep.

1*