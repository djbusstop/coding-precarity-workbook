﻿6

a) Abbe1). In den Zeißschen Werken wurde im Jahr 1891
die neunstündige Arbeitszeit eingeführt, und im Frühjahr 1900
durch die achtstündige Arbeitszeit ersetzt. Man ging zur acht-
stündigen Arbeitszeit über mit der Maßgabe, daß bei allen Zeit-
lohnarbeitern in Zukunft für acht Stunden dasselbe bezahlt werden
soll, als vorher für neun Stunden, und daß alle Akkordlöhne
unverändert bleiben sollen in der ausgesprochenen Erwartung,
alle Arbeiter würden es fertig bringen, in diesen acht Stunden
dasselbe zu leisten, wie bisher in neun Stunden. Beachtenswert
ist, daß der Entschließung zum Übergang zur achtstündigen
Arbeitszeit eine Abstimmung in der Arbeiterschaft vorausging.
Die Abstimmung geschah so, daß gefragt wurde; wer traut sich
zu und ist gewillt, in acht Stunden dasselbe zu leisten, wie bisher
in neun? Die Abstimmung ergab ein 7/s-Majorität mit »ja«, und
die ganze Einrichtung sollte nur für ein Jahr gelten. Stellte sich
heraus, daß ein merklicher Arbeitsausfall einträte, so sollte der
Achtstundentag wieder aufgehoben werden. Abbe hatte seine
Beobachtungen so angestellt, daß er die Leistungen von 250 Arbei-
tern genau auf die Wirkung des Achtstundentags untersuchte,
und zwar wurden nur Leute genommen, die in den beiden Unter-
suchungsjahren unter völlig konstanten Bedingungen gearbeitet
hatten. Auch wurden nur Leute untersucht, die zur Zeit des
Wechsels schon mindestens 22 Jahre alt und vier Jahre im Be-
trieb tätig waren. Alle Leute wurden ausgeschieden bei der
Untersuchung, die nicht mindestens die Hälfte der ganzen Zeit
in Stücklohn gearbeitet hatten. Die Endziffer der Untersuchung
ergab, daß sich der Stundenverdienst im Verhältnis von 100 zu
116,2 erhöht hatte. Das Verhältnis von 8 zu 9 wäre aber ge-
wesen von xoo zu 112,5. Die Tagesleistung beim Achtstunden-
tag hatte sich also gegenüber der Neunstundenarbeit um 1/30 der
früheren Tagesleistung erhöht. Dieser Nachweis betraf aus-
schließlich solche Leute, die im Stücklohn gearbeitet hatten. Aber
auch bei den Leuten, die im Zeitlohn an Maschinen beschäftigt
waren, hat Abbe infolge einer besonderen Messungsmethode ge-
funden, daß eine Steigerung im Durchschnitt von 100 zu 112 zu
konstatieren war, daß also auch hier annähernd der Zeitausfall
ausgeglichen wurde.

Abbe behauptet, daß das, was er in seinem Unternehmen

') Die volkswirtschaftliche Bedeutung des industriellen Arbeitstags. Ab-
gedruckt in Abbe, Sozialpolitische Schriften. Jena 1906. Gesammelte Ab-
handlungen. III. Band.