﻿28

gesetzt wurde: Wenn die Arbeiterin geschickt ist und ihre Arbeit
genau übersieht, so kann sie einen großen Teil von Störungen
vermeiden, welche das Abstoppen des Webstuhls erforderlich
machen und kann daher die Produktion vergrößern. Eine ähn-
liche Meinung wurde von einem Weber ausgedrückt, der sagte:
Unter gewöhnlichen Umständen kann eine Reduktion von 56
auf 54 Stunden ausgeglichen werden durch besondere Sorgfalt
der Weber1).«

c) Weitere Berichte und Untersuchungen. Gegenüber
der Schul ersehen und der amerikanischen Untersuchung be-
deutet die in der »Wirtschaftskurve« der Frankfurter Zeitun^2!
angewendete Art der Behandlung des Problems einen Rückschritt.
Hier werden regelmäßig Mitteilungen über die Arbeitsintensität
veröffentlicht. Diese Berichte sind aber Einzelberichte, teils von
Unternehmern, teils von Gewerkschaftsführern. Wenn auch die
Fragen, die von dem Herausgeber an die Gewährsmänner gerichtet
sind, die gleichen sein mögen, so ist doch die Art und Weise
der Beantwortung der Fragen keine einheitliche. Nicht nach
der exakten Methode von Schüler werden die Fragen untersucht,
sondern die einzelnen Unternehmungen bzw. Arbeiterführer senden
Berichte über ihre Einzelerfahrungen ein, die aber nicht methodisch
einwandfrei sein können. In anerkennenswerter Werse hat der
Herausgeber die Mängel dieser Berichte selbst zugegeben. Da
diese Mängel sich auch bei anderen Berichten von sogenannten
Erfahrungstatsachen finden, ist dieses Eingeständnis besonders
wichtig. Der Berichterstatter sagt; »Viel schwieriger aber ist
der geradezu erstaunliche Mangel an guten Betriebsstatistiken.
Und hier liegt, wie wir gestehen müssen, für uns das über-
raschendste und einstweilen ^wesentlichste, wenn auch negative
Resultat unserer Vorarbeiten: man kennt sich im eigenen Betriebe
nicht aus. Das mag häufig ein Ergebnis der derzeitigen immer
noch chaotischen, allenthalben auf Umstellung und Provisorien ein-
gestellten Fabrikation sein — immerhin läßt diese Versicherung
auch Gutwilliger, man wisse nicht, allerhand Schlüsse zu3).« Der
Herausgeber bemerkt selbst ferner, es werde sehr langer Arbeit
und Erfahrung bedürfen, um die einstweilen noch sehr rohe

x) a. a. O., S. 24/25.

2)	Die Wirtschaftskurve mit Indexzahlen der Frankfurter Zeitung. Nach
den Methoden und unter Mitwirkung von ErnstKahn. 4 Hefte. 1922. Frank-
furt a. M.

3)	a. a. O., 2. Heft, S. 55.