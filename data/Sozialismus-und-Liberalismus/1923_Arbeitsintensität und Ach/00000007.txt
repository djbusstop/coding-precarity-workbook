﻿IV

vielmehr in meiner Schrift eine nationalökonomisch-theoretische
Frage klären. In der Literatur über den Achtstundentag kehrt
sehr häufig der Gedanke wieder, daß die Arbeitsintensität die
günstigste bei achtstündiger Arbeitszeit wäre, so daß unbeschadet
der Produktivität und Rentabilität der Betriebe der Achtstunden-
tag aus volkswirtschaftlichen Gründen gefordert werden müßte.
Hier liegen offenbare Irrtümer der Theorie der Arbeit und der
Arbeitsintensität vor, gegen die sich meine Schrift in der Haupt-
sache wendet.

Die Hauptvertreter der historisch-ethischen Schule der National-
ökonomie in Deutschland, Schmoller und Brentano, haben sich
unzweifelhaft die größten Verdienste um unsere Wissenschaft er-
worben; aber die große Anzahl von Nationalökonomen und Sozial-
politikern, die aus dieser Schule hervorgegangen sind, zeigen viel-
fach einen bedauerlichen Mangel an Interesse und Verständnis für
grundlegende Fragen der Theorie der Volkswirtschaftslehre. Das
zeigt sich besonders häufig bei der Art und Weise, wie Fragen
der Sozialpolitik behandelt werden. Ich habe auf diesen Mangel
an theoretischer Vertiefung vor kurzem an anderer Stelle bereits
hingewiesen. (Zur Frage der Beteiligung der Arbeiter am Unter-
nehmergewinn. Weltwirtschaftliches Archiv. 1922.) Noch mehr
als bei der Frage der Gewinnbeteiligung tritt dies in der Literatur
über den Achtstundentag hervor, wo allzu oft ein gewisses soziales
Empfinden die notwendige Berücksichtigung ökonomischer Tat-
sachen und Zusammenhänge zurücktreten läßt. Ich hoffe, durch
meine Schrift dazu beizutragen, daß die Diskussion über dieses
wichtige Problem mehr von dem maßgeblichen volkswirtschaft-
lichen Standpunkt als von dem parteimäßigen oder gefühlsmäßigen
aus beurteilt werde.

Freiburg i. B., März 1923.

K. Diehl.