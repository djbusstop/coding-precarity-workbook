﻿£ cc

9

Die Freiliandelslehre.

Der wissenschaftliche Sozialismus.

§ 4.

Der Begriff Gut.

Die entschieden privatwirtschaftliche Auffassung der
Freihandelslehre unterscheidet im „weiteren Sinne“ gei-
stige und materielle Güter. In der Volkswirt-
schaftslehre aber versteht man unter dem Worte „Gut“
oder „Güter“ schlechtweg „materielle Güter,
eiche Werte verkörpern“. So Friedrich
a s t i a t, Max W i r t h u. a.

Marx ist bei der Definition des Begriffes „Gut“ in
der freihändlerischen Auffassung hängen geblieben. E i n
Warenkörper, wie Eisen, Weizen, Diamant usw„
der einen Gebrauchswert hat, ist nach ihm
ein „Q u t“.

Sozialdemokratische Lehrbücher von heute sagen
deshalb: „Das Gut ist ein Gegenstand, der
menschlichen Bedürfnissen dien t“. So Dr.
Herrn. Dune k er 1908.

\