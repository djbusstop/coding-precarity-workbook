﻿15

Der KathedersoziaHsmus.

Die organische Mittelstandsauffass iing.

§ 10.

Der Begriff Kapital.

Auch bei diesem Begriff bewahrt diese Schule ihren
eklektischen Charakter. Conrad bezeichnet als
„Kapital“ jenen Teil des Vermögens, wel-
cher, selbst Produkt menschlicher Ar-
beit, wieder zur Produktion bestimmt
ist. Die Grundlage der Kapitalbildung
sei das Produzieren von Produktions-
mitteln, statt von Genussgütern und das
Aufsparen der letzteren durch Beschrän-
kung des Verbrauchs, um einen Ueber-
schuss der Produktion zu erzielen. Phi-
lip p o v 1 c h unterscheidet Produktivkapital und
Erwerbskapital. Schmoller erblickt im „Ka-
pital“ eine Wertsteigerung oder -Minde-
rung, die nicht der aufgewendeten Arbeit
entspricht, und neigt im allgemeinen mehr der
Auffassung als „Erwerbsvermögen“ zu, um
schliesslich C. Menger beizupflichten, welcher dar-
unter werbende Geldsummen versteht. Ro-
scher hat sogar Tiere, die für ihren Winterschlaf
Vorräte und Wohnungen haben, zu Kapitalisten ge-
macht. Es kann also nicht überraschen, dass heute fast
jeder Mensch nach wissenschaftlicher Auffassung Kapi-
talist ist. Viele rechnen auch den Grund undBoden,
Häuser, Leihbibliotheken usw. zu dem Begriff
„Kapital“. Weiter unterscheidet man „stehendes“
und „umlaufendes“ „Betriebskapital“ und pflegt für
beide Arten einen verschieden hohen Zinssatz dem
Unternehmergewinn gegenüber aufzurechnen.

Unter dem Worte „Kapitalismus“ aber versteht
auch Böhm-Bawerk jene Produktionsweise, welche
unter der Herrschaft und Leitung der
Eigentümer des Kapitals vor sich geht.
Nachdem diese Kapitalisten als Regel vom Qeldbesitz
ausgingen und jederzeit bemüht sind die Rückkehr zum
Qeldbesitz sich offen zu halten, werden die vorstehend
angeführten Definitionen wohl durchweg zu weit sein.

Zur Definition dieses Begriffes bieten sich die Worte
„Kapitalismus“ und „K a p 11 a 1 g e w i n n“ als
Hilfsbegriffe. So kann man zunächst sagen: Kapital
ist eine Geld- oder Gütermenge, welche
Gewinn bringen soll. Die Grundrente ist
nur eine Unterart der Kapitalgewinne.
Und Kapitalgewinn wird hier im Gegensatz
zum Arbeitsgewinn im Sinne des § 3 gebraucht.
Für Kapitalgewinn ist deshalb auch das Wort
„müheloser jewinn“ üblich. Kapital kann aber
auch verloren gehen, ohne Gewinn zu
bringen. Deshalb gehört vielfach das Risiko als
wesentliches Moment zu dem Begriff Kapital — aber
nicht immer und darf also der Definition nicht ein-
verleibt werden. Eine Geld- oder Gütermenge kann jetzt
als „Wucherkapital“ sich betätigen und plötz-
lich in eine „milde Stiftung“ verwandelt
werden. Kapital ist deshalb wesentlich ein psycho-
logischer Begriff, dem die auf nicht erarbeite-
ten Gewinn gerichtete egoistische Absicht zugrunde liegt.
Und dieser Gewinn wird vertragsmässig mit Hilfe
des geltenden Kapitalistenrechtes gesichert. Insofern
ihrer Entstehung nach alle Güter volkswirtschaftliche
Arbeitsprodukte sind (§ 4) und unter normalen
o r g a n i s c h e n Verhältnissen das Kapital keine
Werte erzeugt, sondern sie nur überträgt,
scheint es gerechtfertigt, Kapital als „geronnene
Arbeit“ zu charakterisieren. Indes entsteht der Kapi-
talgewinn doch recht vielfach auch aus einer
Uebertragung von Kapitalgewinnen an-
derer. Und wo nach Zersetzung des Mittelstandes
Qrosskapitalisten und Proletarier sich zusammen
der Güterproduktion widmen, kann auch der
Kapitalgewinn einen berechtigten Produktionsanteil
bedeuten. In beiden Fällen bleibt die Bezeichnung
„geronnene Arbeit“ unzutreffend. Weiter
führt hier der Begriff „K a p i t a 1 i s m u s“. Er fügt zum
Streben nach Gewinn, und zwar einer Gewinn-
sucht, die sich nicht schämt — im Gegensatz zum
Wertbegriff der organischen Mittelstandsauffassung,
die das Moment der besseren Gerechtigkeit
mit dem Schamgefühl an der Stirne trägt — das
weitere Moment der Herrschaft und Herrsch-
sucht — im Gegensätze zu dem „Sich-dienend-
anschliessen“ des Begriffes Arbeit im Sinne des
§ 3 der organischen Mittelstandsauffassung — in diesen
Kreis der Definitionserwägungen ein. Also: das Geld
der Witwe, welches irgendwo zu üblichem Zins an-
gelegt ist, die Spargelder des Volkes, welche im
Kredit der öffentlichen Körperschaften Verwendung fin-
den, sindkeinKapital, weil hier die beiden wesent-
lichen Begriffsmerkmale der Habsucht, die sich
nicht schämt, und der Herrschsucht über
die volkswirtschaftliche Arbeit fehlen. In
all diesen Fällen handelt es sich um den landesüblichen
Preis der Qeldleihe, die sich dienend ans Ganze an-
schliesst. Der Kapitalismus aber bedeutet
die volkswirtschaftliche Herrschaft der
Kapitalisten, die nur historisch verstanden werden
kann.

Der Freihandel hatte die Millionen von Einzelwirt-
schaften einfach „frei“ gemacht. Jede Art von orga-
nischer Gliederung zum Ganzen fehlte. Daraus