﻿10

Die Freihandelslehre.

Der wissenschaftliche Sozialismus.

§ 5.

Begriffe: Preis und Wert.

Die individualistische Auffassung der Freihändler mit
dem Glauben an die Harmonie der egoistischen Interessen
bei Freiheit des Verkehrs lässt die Begriffe „Preis“ und
„Wert“ ineinander überfliessen. Ausserdem werden
beide Begriffe überwiegend auf „Waren“ bezogen. Der
Tauschwert einer Ware wird nach Adam
S m i t h am besten an der aufgewendeten Arbeit
gemessen. Der Marktpreis aber wird durch Ange-
bot und Nachfrage bestimmt und bewegt sich
um einen „natürlichen Preis“ als Mittelpunkt,
f ü r' d e n wieder die Produktionskosten be-
stimmend sind.

Nach Friedrich Bastiat schafft der Tausch
erst den Wertbegriff. Der Wert bestimmt sich
nach dem Verhältnis zweier ausgetauschter Dienst-
leistungen.

J. B. S a y sieht in dem Wert mehr den Ausdruck
einer Schätzung, lässt aber auch wieder den Wert
durch Angebot und Nachfrage bestimmt werden,
wobei durch den Qeldausdruck der Preis ent-
steht, dessen Schwankungen sich wieder um den Wert
bewegen.

Max Wirth sagt: Während der Wert (Wert-
schätzung) schon bei dem einzelnen Menschen zur Gel-
tung gelangt, erscheint der Preis erst in der Gesell-
schaft mehrerer durch das Tauschgeschäft. Die Tendenz
der Preisbewegung richtet sich nach den Herstellungs-
kosten der Ware. Der Marktpreis ist ein genaueres Wert-
bestimmungsmittel als die Wertschätzung. Der domi-
nierende Begriff ist nicht der Wert, sondern der
Preis, der sich im freien Markte bildet. Tritt
ein Konflikt zwischen Preis und Wert ein, so ist es Sache
des Wertes, sich nach dem Preise zu richten. Die auf-
gewendete Arbeit, an welcher der Tauschwert einer
Ware gemessen werden kann, bezieht sich nur auf den
privatwirtschaftlichen Begriff der Arbeit.

Karl Marx geht auch hier vor allem von den
englischen Fabrikverhältnissen seiner Zeit aus. Der
Glaube an die soziale Harmonie bei freier Konkurrenz
ist bei ihm verschwunden. Preis und Wert sind ihm
zwei ganz verschiedene selbständige Er-
scheinungen, die in ihrem geld massigen Aus-
druck keineswegs zusammenfall.en. Speziell
bei der Ware „menschliche Arbeitskraft“, wie sie
im freien Markte von dem kapitalistischen Unternehmer „ge-
kauft“ wird, sieht Marx eine mehr oder minder
wesentliche Differenz zwischen dem Preis
(Tauschwert, Lohn) und dem Gebrauchswert
(Wert des erzeugten Arbeitsproduktes). In dieser
Differenz zwischen Preis und Wert der
„Ware“ Arbeitskraft liegt für Karl Marx die
soziale Frage. Und die Lösung dieser Frage muss
offenbar bewirken, dass sich der Preis nach dem
Werte richtet, nicht umgekehrt. Darin unter-
scheidet sich Marx wesentlich von den Freihändlern.
Marx ist ferner zur gesellschaftlichen Be-
trachtung dieser Erscheinungen übergegangen. Aber
Marx reduziert, ähnlich wie Adam Smith und an-
dere Freihändler, den Tauschwert aller Waren als Ar-
beitsprodukte auf die gleiche, einfache,
menschliche Arbeit, ja selbst auf die gleiche,
notwend i ge,menschlicheArbeitszeit, wo-
bei der Marxsche Arbeitsbegriff nicht mit unserem
volkswirtschaftlichen Arbeitsbegriff im Sinne des § 3
identisch ist. Andere „soziale Fragen“ neben der
Lohnarbeiterfrage kennt Marx nicht.