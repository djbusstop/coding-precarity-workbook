﻿Inhaltsangabe.

§	1.	Die philosophisch-psychologische Auffassung als Konsequenz der jeweiligen Zeitverhältnisse 6

§	2.	Begriffe: Wirtschaft und Volkswirtschaft . i...................................7

§	3.	Der Begriff Arbeit.............................................................8

§	4.	Der Begriff Gut................................................................9

§	5.	Begriffe: Preis und Wert......................................................10

§	6.	Begriffe; Reichtum und Vermögen...............................................11

§	7.	Begriffe: Produktion und Konsumtion...........................................12

§	8.	Begriffe: Geld und Währung....................................................13

§	9.	Begriffe: Arbeiter und Arbeitslohn............................................14

§	10.	Der Begriff Kapital ..........................................................15