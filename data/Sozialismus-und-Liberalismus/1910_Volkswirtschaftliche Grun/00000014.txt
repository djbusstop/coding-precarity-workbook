﻿8

Der Kathedersozialismus.

Die organische Mittelstandsauffassung.

§ 3.

Der Begriff Arbeit.

Gemeinsam ist fast all diesen kathedersozialistischen
Definitionen das Merkmal der Tätigkeit der ein-
zelnen Menschen, die auf ein äusseres
Ziel g e r i c h t e t i s t. Aber während Roscher dabei
zunächst noch an das Merkmal der „Mühe“ denkt und
Schönberg von „Anstrengungen“ spricht, wäh-
rend Böhm-Bawerk zwei Voraussetzungen kennt, auf
welchen die Möglichkeit beruht, einen Wert durch
Arbeitsplage bemessen zu können, und F. v. W i e s e r
von der „A r b e i t s p I a g e“ und den mit der Arbeit ver-
bundenen „Unannehmlichkeiten“ spricht, ver-
tritt Conrad die Auffassung, dass eine physisch
anstrengende einförmige Tätigkeit für
den daran gewöhnten Arbeiter keine Last,
sondern ein „Bedürfnis“ ist. Bei P h i 1 i p p o v i c h
ist die Arbeit ferner das,, schöpf erischeElement
der Produktion“. Und S c h m o 11 e r vertritt die
Ansicht: „Esgibt keine grössere Freude für
den Menschen, als die Lust tätigen
Schaffens und Wirkens“.

So sind also bei dem Kathedersozialismus von dem
Unlustgefühl der (Mühe bis zur Schaffens-
lust alle Empfindungen in dem Begriff „Arbeit“ ver-
treten, wenn auch durch verschiedene Personen.

Durchweg bleiben diese Definitionen in den Bahnen
der rein subjektiven individuaüstischen Auffassung. Bei
den modernen sozialpolitischen Betrachtungen aber
wird die „A r b e i t“ als „W a r e“ vielfach in geradezu
römisch-rechtlicher Auffassung gedacht. Die Arbeit ist hier
mit der Person des Arbeiters so untrennbar verbunden, dass
ihr Kauf eine Verfügung über die Person des Verkäufers
bedeutet.

Das konstituierende Merkmal des Be-
griffes „menschliche Arbeit“ ist die Tatsache der
sozialen Arbeitsgemeinschaft, zunächst
innerhalb der nationalen Volkswirtschaft,
dann aber auch innerhalb derWeltwirtschaft, und
zwar nicht nur innerhalb der Gegenwart, sondern
auch zurück in die ganze historische
Vergangenheit. Die soziale Arbeitsgemeinschaft in
diesem Sinne deckt sich mit dem uralten p.hilo-
sophischenBegriffdes„ganzenMensche n“
von P 1 a t o bis auf Trendelenburg,Schaeffle,
JohnRuskin undT homasCarlyle. Unushomo—
nullus homo! Ein Mensch ist kein Mensch! Der Be-
griff „ganzer Mensch“ ist gleich dem „der gan-
zen Menschheit“. Jede Art wirtschaft-
licher Erwerbstätigkeit, sei es in der Fabrik
oder auf Feld und Flur, sei es im Handel oder in der
Schiffahrt, zeigt jeden einzelnen Menschen in
engster Verbindung mit dieser sozialen
Arbeitsgemeinschaft, die bis zur fernsten Ur-
zeit zurückreicht und den ganzen Erdball umschliesst.
Diese soziale Arbeitsgemeinschaft ist .eine solche des
Geistes und der Materie.

Arbeit in objektivem Sinne ist deshalb
der fortschreitende gewaltige Vereini-
gungsprozess von Materie und Geist, wo-
bei die jetzt tätigen Menschen die an-
wesenden lebenden Bindeglieder sind
zwischen den von einer Jahrtausende
alten Arbeitsgemeinschaft vorbereite-
ten Stoffen und Kräften.

Arbeiten im subjektiven Sinne heisst, an
diese soziale Arbeitsgemeinschaft in irgend welcher
Weise sich dienend anschliessen, sei es als
Hilfsarbeiter, sei es als selbständiger Unternehmer, sei es
als Lehrer, der die Jugend bildet, sei es als Priester,
welcher die ethischen Kräfte des Volkes stärkt, sei es als
Soldat, Richter oder Verwaltungsbeamter zur Aufrecht-
erhaltung der öffentlichen Ordnung. Selbst der König ist
ein Arbeiter in diesem volkswirtschaftlichen Sinne. Die
Devise des heutigen Königs von England lautet mit
Recht: „Ich dien!“ In diesem Sinne ist jede
volkswirtschaftliche Arbeit ein quasi —
Amt. Ob die Arbeit des einzelnen überwiegend ein
Unlustgefühl oder das Gefühl der Arbeits-
lust und Arbeitsfreudigkeit begleitet, das wird
im wesentlichen abhängen von der Erziehung und Bil-
dung des Volkes, von dem sozialen Rechtszustand und
von der Gesundheitspflege des Menschen. „Arbeit
ist die Mission des Menschen auf Erde n.“
(Th. Carlyle.) Wer die Arbeit als körperliche Anstren-
gung meidet, muss sie als Sport aller Art pflegen oder
alsUebungenindenmedico-mechanischen
Anstalten und als Massage nachzuholen ver-
suchen.

Im scharfen Gegensätze zu dieser im Prinzip „dienen-
den“ Arbeit steht die Arbeit der reinenEgoisten
Sie fördern die soziale Gemeinschaft nicht, sie zerstören sie.
Sie schaffen keine neuen Werte, sie übertragen nur solche
aus dem Besitz anderer in ihren Besitz. An dieser Ar-
beit der reinen Egoisten gehen die Völker zu
Grunde.