15 
Zins und Grundrente als arbeitsloser, auf Ausbeutung 
beruhender Einkommen vollständig klar und hat die So 
phismen, mit denen der zinsliebende Amerikaner seine 
Ansichten zu stützen suchte, gründlich vernichtet. Aber an 
Stelle einer Illusion hat er schließlich auch nur eine andere 
gesetzt. Denn nicht zufrieden, die Wesensgleichheit von 
Zins und Grundrente als arbeitsloser Einkommen zu kon- 
statiren, geht er weiter, indem er den Zins als eine Folge 
der Grundrente, welche mit dieser zugleich fallen müßte, 
hinstellt. Die Bodenreform, so verheißt er, wird beide 
Parasiten, deren soziale Gefährlichkeit dieselbe ist, gleich 
mäßig ausrotten. Er bezeichnet sogar die Beseitigung 
d^es Zinses als die einzige Bedingung, unter 
welcher die staatliche Konfiskation der Grund 
rente der arbeitenden Mehrheit von Nutzen sein 
kann. 
Hermann Bahr hatte in den „Deutschen Worten" 
die Bestrebungen der Landliga vom proletarisch-sozialistischen 
Standptlnkt aus beleuchtet. Nehme der Staat den Boden 
für sich, so würde damit eben nur ein Landverpächter an 
die Stelle der vielen treten; der jetzt bestehende Inter 
essengegensatz zwischen agrarischem und industriellem Kapital 
würde zu Ungunsten des Volkes, das aus dem Streit der 
Herrschendelt Klassen bisher manchen Nutzen zog, fortfallen, 
ohne daß irgend ein anderer Vortheil die Massen dafür 
entschädigte. Denn wenn der Staat auch, mit den Ein 
nahmen aus der Bodenrente ausgestattet, die Steuern 
erlassen könnte, so würden auf Grund des ehernen Lohn- 
gesetzes die Löhne bald um den Betrag, um welchen sie 
der Steuererlaß real erhöhte, wiederum fallen. Die Boden 
reformbewegung liege darum gänzlich abseits vom Wege 
der Arbeiterpartei, sie charakterisire sich als „Versuch einer 
einseitigen Ausbeutung der sozialen Ideen jit Gunsten des 
beweglichen Kapitals." 
Gegen diesen gründlichen, zielbewußten Angriff ver 
theidigte sich Flürscheim in seiner Monatsschrift „Deutsch 
Land."') Alles was Bahr einwende, sei vom Standpunkte 
) Ebenso äußert sich Flürscheim auch in dem diesjährigen 
Jumheft der „Neuen Zeit". Der aus seiner Feder dort veröffent 
lichte Aufsatz giebt ein übersichtliches Bild seiner Theorie.