10 — 
Nur in diesem Falle wird dem Gesetze der Konkurrenz, 
welches auf allen Gebieten die Profite ausgleicht, genug, 
gethan. Denn wenn der bloße Boden mit dem kapitali- 
sirten Betrage der Grundrente bezahlt wird, so bezieht der 
neue Käufer der besseren Bodenklassen mit seiner Grund 
rente nur den üblichen Gewinnsatz des beim Kauf des 
Landes ausgelegten Kapitals. Sein Extragewinn hört 
auf (ausgenommen den Fall, daß die Grundrente nach 
dem Kaufe, während er das Gut noch besitzt, wider Er 
warten steigt) und fließt dem spekulativen Landverkäufer zu. 
Bei dem Boden, der nicht zur Beackerung, sondern 
zum Hausbau verwendet wird, verhält es sich ähnlich. 
Die Miethe stellt nicht nur den Prosit des beim Bau ver 
brauchten Kapitals dar, sondern umfaßt überdies das Kauf 
geld, welches dem Hausbesitzer für die mehr oder weniger 
günstige Lage seines Grundstücks jährlich von dem Miether 
ausgezählt wird. Dieser Theil der Miethe, kapitalisirt, 
ergiebt den so außerordentlich schwankenden, oft enorm 
hohen Preis der Grundstücke. 
Wäre der Boden gleich den Waaren der Industrie 
beliebig vermehrbar, so würde zwischen den Grundbesitzern 
und den Fabrikherren kein größerer Interessengegensatz be 
stehen als zwischen den Unternehmern zweier verschiedenen 
\ Jndustriebranchen. So aber befinden sich die Grundbe 
sitzer, insbesondere die langjährigen, womöglich erblichen 
Eigenthümer der besseren Bodenklassen, gegenüber den In 
dustriellen in einer Monopolistenstellung. Sie haben das 
Land in den Kulturstaaten tune, und kein Industrieller 
vermag daselbst neues zum alten hinzuzuproduziren. Diese 
Monopolistenstellung ist es, welche die Rente 
schafft und so die gesammle Industrie durch die 
Vertheuerung der Bodenprodukte und Miethe dem 
Grundbesitze tributpflichtig macht. 
III. 
Die polemif gegen die Grundrente. 
Der Klassengegensatz zwischen den Eigen 
thümern des Bodens nnb denen des mobilen Ka 
pitals spiegelt sich naturgemäß in der praktischen