16 — 
des Henry George aus unwiederleglich, das eherne Lohn 
gesetz und damit die Ausbeutung und das Elend der Ar 
beiter würde durch eine solche Reform ganz und gar nicht 
berührr, nur das mobile Kapital, welches die drückende 
Steuerlast dann los wäre, hätte den Prosit davon. Aber, 
fügt Flürscheim gleich hinzu, mir selbst ist es gelungen 
die Bodenreform auf einer „ganz neuen Theorie 
zu begründen." Diese Reform muß, meiner Theorie 
zu Folge, ökonomische Wirkung hervorbringen, 
an die weder George noch die ihn bekämpfenden 
Sozialisten gedacht haben, sie muß den Zins und 
mit ihm die Quelle der Ueberproduktion ver 
nichten. Bezieht ferner das Leihkapital keinen 
Zins mehr, so werden auch Assoziationen tüchtiger 
Arbeiter im Stande sein, das zur Produktion 
nothwendige Geld selbst leihweise zu erhalten, 
die Uebrigen aber, welche fortfahren, bei Kapi 
talisten zu arbeiten, werden den vollen Werth 
ihrer Tagesarbeit im Lohn erhalten. Mit der 
Beseitigung der Exploitation der Arbeitskraft 
und der Ueberproduktion wäre indeß die soziale 
Frage ohne Umwälzung unseres ganzen Wirth 
schaftssystems gelöst. Richt die Verstaatlichung 
allerProduktionsmittel, die bloße Verstaatlichung 
des Bodens reicht dazu aus. 
Ich bin auf die Polemik Flürscheim's gegen Bahr 
zurückgegangen wegen des unumwundenen Geständnisses, 
welches der Leiter der deutschen Reformbewegung hier ab 
legt, daß sein Parteiprogramm nur dann Berechtigung 
hätte, wenn die Erfüllung desselben nachweislich die Ueber 
produktion und die Ausbeutung unmöglich machen würde. 
Ohne das, so räumt er ein, wären die Bodenverstaatlicher 
eine reine Bourgeoisgruppe. 
Flürscheim meint, den Nachweis erbracht zu haben. 
Wir werden ihn im Folgenden eingehend prüfen. Das 
Haupt der deutschen Bodenreform soll nicht behaupten 
dürfen, daß die sozialistische Presse einer prinzipiellen Aus 
einandersetzung» mit seinem Parteistandpunkte aus dem 
Wege gegangen sei. 
Ich lasse die Frage, inwieweit die heutige Ueber 
produktion dem Zinseinkommen unserer großen Bankiers