Unternehmer auf allen Gebieten wird entbehren uub durch 
gesellschaftliche Institutionen ersetzen können. 
Natürlich würde mit dem Theile der Expropriation 
begonnen werden, welcher sich verhältnißmäßig am leichtesten 
durchsetzen ließe, und das wäre vielleicht die des Grund 
besitzes. Der Staat kann den gesammten städtischen 
wie ländlichen Boden als sein Eigenthum erklären, ohne 
baß baburch bag wi##aftíi4e Mea irgenb míe in8 
Stocken geräth. Er kann, bis sich der neue Zustand 
der Dinge befestigt hat, Land unb Wohnungen zu ähnlich 
abgestuften Preisen vermiethen wie bisher, wodllrch ihm, 
wie die Bodenreformer mit Recht hervorheben, außer 
ordentliche Einnahmen zufließen würden, ohne daß er die 
2aß ber gangen Şrobii%ion8Ìeitung, weicher ec guerft #mer= 
lich gewachsen sein mächte, gu übernehmen brauchte. 
@8 märe mög#, ba& bicfe maßreßd allein #on 
Anlaß zu gewissen Erleichterungen des konsumirenden 
8oHe8 gäbe^ inbem man ißa# nnb ^ieth8breife be. 
deutend ermäßigt, sodaß die Hauptausgaben des Arbeiters, 
die für Nahrung und Wohnung, sich vermindern. Ob die 
Maßregel ihm aber für längere #eit helfen mirb, ist, ba 
bie be8 9Irbeiter8 nnoeränbert bleibt, nicht 
im Voraus zu bestimmen. Wenn die Marktlage für die 
Unternehmer gut, für die Arbeiter dagegen schlecht ist, 
kann es leicht geschehen, daß der Lohn um soviel, als die 
Weiter bar# iene8 Eingreifen be8 6taate8 gewannen, 
mieber gnrnctgeht. 3n Engíanb, wo am Anfange biefe8 
Jahrhunderts in den ländlichen Distrikten massenhaft 
Armenunterstützungen verabfolgt wurden, _ entblödeten sich 
z. B. die Herren Unternehmer nicht, daraufhin die jämmer 
lichen Hungerlöhne noch weiter zu verkürzen. Freilich hat 
seitdem die Arbeiterklasse unendlich an Kraft und Organi 
sation gewonnen. So leicht würden die Fabrikanten es 
mit ihren Lohndrückereien reicht mehr haben. — Dasselbe 
gilt von den Steuererleichterungen der Arbeiter, die bei 
einer Konfiskation der Grundrente eintreten würden. Es 
könnte immerhin geschehen, daß ihre gute Wirkung durch 
Herabgehen der Löhne gleichfalls ausgeglichen würde. 
Dagegen hat ber Staat, im Besitz ber sämmtlichen 
aus dem Boden fließenden Geldmittel, eine andere Mög 
lichkeit, um in einschneidender Weise das Loos des ar-