7 
bessert hieraus, auf beit theoretischen Kampf gegen die 
Grundrente, eingehen, erscheint es nothwendig, uns das 
Wesen derselben kurz zu veranschaulichen. 
Daß die Grundrente so wie die übrigen arbeits 
freien Einkomnten aus dem Mehrwerth fließt und als 
Tribut von dem Arbeiter, dem ursprünglichen Mehrwerth 
erzeuger, wie vom Unternehmer, dem ursprünglichen Mehr- 
werthaneigner, erhoben wird, war schon gesagt. 
Es liegt hier aber der Einwurf nahe, daß der Land 
wirth ebensogut wie der Fabrikant Unternehmer sei und 
als solcher aus den von ihm angestellten Arbeitern direkt 
Mehrwerth Herauspresse, daß sein arbeitsfreies Einkommen 
mithin nicht abgeleitet sei, sondern genau so wie der 
Prosit des Fabrikanten aus der unmittelbaren „Ver 
werthung der proletarischen Arbeitskraft" entspringe. Und 
das trifft in der That für einen großen Theil des durch 
Bodenbearbeitung gewonnenen Einkommens zu. Nicht 
aber für das ganze. 
Nimmt man die Summe aller im Laufe eines Jahres her 
gestellten Jndustrieprodukte und zieht von dem Gesammt- 
preis derselben die Summe der aufgewandten Produktions 
kosten ab, so ergiebt der Rest den Ueberschuß der Preise 
über dieKosten, das Profiteinkommen, welches der Stand 
der Industriellen in die Tasche steckt. Der durchschnittliche 
Gewinnsatz, d. h. der Bruchtheil, um welchen sich ein in 
der Industrie vorgeschossenes Kapital durchschnittlich im 
Jahr vermehrt, wird gefunden, indem man den Gesammt- 
profit durch das vorgeschossene industrielle Gesammtkapital 
dividirt. Beides, Profit wie Kapitalvorschuß, stellen ja 
nichts anderes als Verkörperungen allgemein menschlicher 
Arbeitszeit, als Werthe dar, und dieser ihrer Gleichartigkeit 
wegen sind sie auch durcheinander theilbar. 
Nun betrachte nlan im Gegensatze dazu das auf 
Bodenkultur und Anbau verwandte Kapital. In den 
Stallungen, den Geräthschaften, der Aussaat, in allen Be- 
ackerungen, Kanalisationett, Häuserbauten u. s. w. steckt 
unzweifelhaft ebenso wie in den Maschinen, Fabriken und 
Rohstoffen des industriellen Kapitals menschliche Arbeit, 
und der Werth jener agrarischen wie der industriellen 
Produktionsmittel kann sich allein nach der Menge mensch 
licher Arbeit, die in ihnen verkörpert ist, richten. Die