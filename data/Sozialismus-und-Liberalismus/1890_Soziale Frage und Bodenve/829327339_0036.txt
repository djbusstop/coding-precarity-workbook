30 
immer, daß der Staat genug finanzielle Mittel in der 
Hand hat und im wirklichen Arbeiterinteresse geleitet wird. 
Die Industrie könnte ans dieser zwangsweis einge 
führten und garantirteli Lohnerhöhung übrigens großen 
Nutzen ziehen. Demi wenn es wahr ist, daß eine Hanpt- 
qnelle der Ueberproduktion im Unterkonsum zu suchen ist, 
und daß dieser allein durch eine allgemeine Einkommen- 
steigerung der breiten, nur verzehrenden, nicht kapitalistisch 
aufhäufenden Volksschichten gemindert werden kann, dami 
muß offenbar der staatliche Minimallohn, der gerade diese 
Einkommen steigert, der Ueberproduktion äußerst wirksam 
entgegenarbeiten. Die Vermehrung der Produktionskosten, 
welcher ben Unternehmern durch die Lohnzahlung erwächst, 
würde durch die Erhöhung des Waarennmsatzes, die leich 
tere und bessere Verkaufsgelegenheit, wahrscheinlich sehr 
bald überholt werden. Stati zu sinken, würden die Ge 
winne steigen. Die Versicherung gegen Arbeitslosigkeit und 
der Minimallohn würden also nach Durchführung der 
Bodenverstaatlichung auch im Rahmen der kapitalistischen 
Wirthschaftsordilung durchführbar sein. 
Aber daulit wäre der Nutzen, den die Expropriation 
des privaten Grundbesitzes schaffen könnte, bei weitem nicht 
erschöpft. Das staatliche Bodeueigenthum könnte der Aus 
gangspunkt für die allmähliche Sozialisirnng der ganzen 
Wirthschaft werden. Was hindert z. B. den Staat, auf 
Kredit einen großen Theil des Ackerbodens ait agrarische 
Arbeiterassoziationen zu verpachten, die ohne jede kapita 
listische Vermittlung zu eigenem Vortheil produzirm? 
Was hindert ihn, zwischett den verschiedenetl agrarischen 
Assoziationen, die verschiedene Arten des Anbaus kultiviren, 
ein halbsozialistisches Vertheilungsspstem der gemeinsam 
produzirteit Ernte, oder vielmehr eines Theiles derselben, 
einztlrichteu. Ein anderer Theil muß verkauft werden, 
damit die Arbeiter für den Erlös sich Jndtlstriewaaren 
verschaffen können. 
Was hindert aber endlich auch den Staat, noch 
weiter ztl gehen und allmählig ein industrielles Etablisse 
ment nach dem andern in Betrieb gn nehmen? Arbeits 
kräfte hat er daztl in der von ihm finanziell unterstützten 
industriellen Reservearmee in reichlichstem Maaße. Die 
industriellen und agrarischen Arbeiterassoziationen, die er