17 
zur Last zu schreiben ist, ob also eine Aufhebung dieses 
Zinseinkommens die Ueberproduktion aus der Welt schaffen 
würde, einstweilen bei Seite und wende mich zu dem Kern 
punkt der Flürscheimschen Theorie, daß der Zins mit 
der Grundrente stehe und falle. 
Wenn der Boden sich im Privatbesitze besindet, so 
wirft er — warum, sahen wir oben — Grundrente ab 
und erhält darum in der kapitalistischen Gesellschaft einen 
Preis. Jeder Kapitalist kann sich also durch Bodenkauf 
in den Besitz eines arbeitslosen Einkommens setzen. Er 
braucht dazu nur das angekaufte Land bebauen zu lasten 
und die Produkte desselben mit dem bewußten Preisauf 
schlage zu verkaufen. 
Wenn jedoch eine Art der Kapitalanlage solchen Ge 
winn bietet, müffen es alle übrigen gleichfalls, weil sonst 
Niemand sein Geld für sie hergeben würde. Daher, sagt 
Flürscheim, kommt es, daß das industrielle Kapital einen 
Unternehmerprofit und das Leihkapital Zins abwirft. 
Führt man indessen den Boden in das Staatseigenthum 
über, so vermag das Privatkapital nicht mehr durch Land 
kauf sich die Grundrente zu annektiren. Und da das ar 
beitslose Renteueinkommen des agrarischen Kapitalisten die 
letzte Ursache für Unternehmerprofit unb Leihzins, das 
arbeitslose Einkommen der anderen Kapitalistengruppen, 
ist, so wäre mit staatlicher Konfiskation der Bodenrente 
zugleich dem Zinse und Profit ihre materielle Grundlage 
entzogen. Sie müßten gleichfalls verschwinden. 
Dieses die Beweisführung Flürscheins, die offenbar 
auf vollständiger Verkennung des ökonomischen Mecha 
nismus beruht. Der Preis der Bodenprodukte, der 
die Grundrente besserer Bodenklassen zur Folge hat, 
wird, wie schon oben bemerkt, normirt durch die Pro 
duktionskosten, welche auf der ungünstigsten noch ange 
bauten Landsorte zu ihrer Herstellung nothwendig sind. 
Denn die Anbauer dieser Ländereien müssen ihre um 
den landesüblichen Profit vermehrten Auslagen beim 
Verkauf ihrer Waare zurückerhalten, weil sie sonst ihr Ka 
pital aus dieser Anlage zurückziehen würden. Worin be 
stehen aber ihre Auslagen? In dem Preise, den sie für 
die von ihnen verbrauchte Arbeitskraft und Produktions 
mittel zahlen müssen. Wovon hängt dieser Preis ab?