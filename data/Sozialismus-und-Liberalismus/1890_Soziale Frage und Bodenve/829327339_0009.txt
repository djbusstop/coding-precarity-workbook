3ak 2338 
Weltwirtschaft 
Kiel 
2. 7.69. 
Soziale Frage and Kodenaerstaatlichung. 
I. 
Der Alehrwerth. 
Die moderne Gesellschaft batti sich auf dem Klassen 
gegensätze zwischen Kapital und Arbeit auf. Der eigen- 
lhumlose Proletar, unfähig irgettd eine selbständige Pro 
duktion zu unternehmen, wird gettöthigt, Tag für Tag 
seine Kraft an den Besitzer der Arbeitsinstrumente zu ver 
kaufen. Der Preis, den er für seine Thätigkeit erhält, 
normirt sich wie der Preis aller anbeten Waaren nach 
den Produktionskosten. Die Produktionskosten der Waare 
Arbeitskraft, welche fast stets in überreicher Menge auf 
dem Markte angeboten wird, stub aber nichts Anderes als 
die Kosten, die das Ausziehen und der nothdürftige Lebens 
unterhalt der Arbeiterindividuen erheischt. 
Die Thatsache, daß der Lohn sich niemals längere 
Zeit hindurch liber das Existenzminimum erhoben hat, 
ist also nací; dieser von Marx vertretenen Auffassttng 
die Folge der kapitalistischen Wirthschaft tmd des in 
ihr allmächtig herrschenden Werthgesetzes selbst. Nur 
wenn die Produktion attfhört, zum Verkauf bestimmte 
Waarenproduktion ztt sein, wird auch die Arbeit auf 
hören, Waare ztt sein und nach ben Erzeugungskosten 
gezahlt ztt werden. Nur allein eine sozialistische Gesell 
schaft, in welcher die Güter, den Bedürfnissen der Gesammt 
heit entsprechend, plattmäßig hergestellt und vertheilt werden, 
vermag das Proletariat aus dem Elend, welches die „freie 
Konkurrenz" über dasselbe verhängt, zu erlösen. Darum 
ist auch die Arbeiterklasse das Heer, auf welches die sozia 
listischen Ideen allein mit Sicherheit rechnen können. So 
bald sie ihre Lage klar erkennt, muß sie sozialistisch denken,