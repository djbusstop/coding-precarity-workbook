21 
Angenommen, die Grundherren erhielten Nichts, Je 
wäre die gesummte Bodenwaare aus ihren Händen in die 
des Staates übergegangen, ohne daß die Kapitalmasse, 
mek%e ben be8 SBobenö reyräfentirt, an bie%eft$er 
zurückströmt. Sie verliereu das Geld, welches ste für 
Hingabe ihrer Waare zu beanspruchen hätten, und smd 
damit der Verlegenheit überhoben, es irgendwo von Neuem 
qewinntragend unterzubringen. Auf dem Geld- und Waaren- 
marft bliebe im ^efentU^^en abe8 nnbetãnbert, e8 gäbe 
nur ein gnt 3:beil Slanierotteure rne^r nnb barum einen 
gemiffen 9In8fab im %Baatenïonfnm. ^ ^ ^ 
Würde dagegen der Marktwerth des Landes bei der 
Expropriation an die Besitzer in Form verzinslicher 
6taat8f#Ibf#ine 01189%#, so märe bet ©tont ge= 
rabem ^nfpeftor unb %ctmaltet bet beengen ®rnnb= 
Herren geworden. Wenn im ersten Falle die Bodenbesttzer 
ni(bt in ^eriegenbeit iamen, ma8 sie mit ihrem (gelbe 
anfangen fobten, meii be nberbanyt (ein (Mb er= 
hielten, so wird ihnen bei dieser zweiten Regulirungsart 
diese Unruhe ebenfalls, und zwar auf sehr viel angeneh- 
mere Weise erspart. Sie bekommen ihr Geld von vorn 
herein in Form von schöii bemalteii Zettelchen, die kraft 
ihrer geheimnißvollen inneren Natur jahraus jahrein Zins 
abwerfen. Die Suche nach neuer Anlage ist nicht 
nothwendig. 
&er %BaatcMmar;t etieibet a# so (eine Sßeranbcrung. 
@ine btitte Wöglicbfeit ist, basi ber 6taat bei bet 
Expropriation statt verzinslicher Schuldscheine einfaches 
Metall- oder Papiergeld ausgiebt. Dann gilt es für die 
Enipfäiiger, die neuen Schätze nnn schnell und zwar 
mogli# ptofitiieb nntergnbringen. SDic Waffe be8 
deplazirten Kapitals wird überdies, nach Flürscheim, noch 
vermehrt, durch die allmählige Zurückzahlung der ganzen 
6taat8f^^nIb, mo;n bie nene SBobententeneinnabme be8 
6taate8 bie Wittei bergeben fob, nnb bureb bie ^feiti, 
gung der hypothekarischen Bodenbelastung. Dies ist der 
§alï, mit bem telnet, an8 melerem er 
sein neues Argument herholt. 
Man höre: Das durch die Bodenverstaatlichung direkt 
oder indirekt auf den Markt geworfene Kapital wird An 
lage suchen als Leihkapital. Das Angebot von Leihkapital