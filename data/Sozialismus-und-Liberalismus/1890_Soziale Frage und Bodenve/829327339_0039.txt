Inhaltsübersicht 
über die Hefte der ï. Serie. 
Heft 1: Ei» sozialistischer Noma». Ein Rückblick. 
2000—I88Î', Nach dem Amerikanischen des Edward Bellamy. 
(32 (Seiten; SßreiS 15 %if.) t 
Heft 2: Die Gewerkschaften, ihr Nutze» und ihre 
Bedeutung für die Arbeiterbewegung. Nach der Rede vor 
den Berliner Maurern von Mar'Schippet. (32 Seiten: 
Preis 15 Pf.) 
Heft 3. Die Arbeiterinnen- und Franenfrage der 
Gegenwart. Bon Clara Zetkin «Paris.) (40Seiten: Preis20Pf.) 
Heft 4: Der Sozialismus in Frankreich seit der 
Pariser Kommune. Von Ossip Zetkin-Paris f. (48 Seiten 
Preis 20 Pf.) 
Heft 5: Charakterköpfe ans der französischen Arbeiter 
bewegung. Bon Ossip Zetkin-Paris f. (48 Seiten; Preis 20 Pf.) 
Heft 0: Die Hausindustrie in Deutschland. Von Paul 
Kampffmeyer-Genf. (32 Seiten : Preis 15 Pf.) 
Heft 7 : Junker und Bauer. Von Paul Kampffmeyer- 
Genf. (32 Seiten: Preis 15 Pf.) 
Heft 8: Die wirthschaftlicheu Nmwalzuugen unserer 
Zeit und die Entwicklung der Sozialdemokratie. Ein 
Vortrag von Max Schippe!. (32 Seiten: Preis 15 Pf.) 
Heft 0 Die Marx'sche Werththeorie. Von Paul 
Fischer-Loi in. (52 Seiten: Preis 20 Pf.) 
Heft I n Die Sozialdemokratie und der deutsche 
Reichstag. Statistisches und Historisches.' (36 Seiten: 
Preis 15 Pf.) 
Heft 1.1: Die soziale Frage auf dem Laude. I. Die 
Lage der ländlichen Lohnarbeiter in Preußen. Von Paul 
Kampffmeyer-Genf. II. Der Ruin des ländlichen Kleinbetriebes 
durch die landwirthschaftliche Großproduktion. Von * * * 
(40 Seiten: Preis 20 Pf.) 
Heft 12: Internationale Arbeitsschutzgesetzgebung. 
Von Paul Ernst-Berlin. (36 Seiten: Preis 15 Pf.) 
Bon der II. Serie 
erschien bisher: 
à, . Heft 1: Der Mythus von der Gründung des Deutschen 
Reiches. Von Hans Müller-Rostock. (40 Seiten: Preis 15 Pf.) 
Heft 2 : Zur Naturgeschichte der antisemitischen Be 
wegung in Deutschland. Von Gerhard Krause. (32 Seiten: 
Preis 15 Pf.)