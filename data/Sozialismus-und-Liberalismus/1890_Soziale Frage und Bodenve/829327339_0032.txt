— 2G — 
überhaupt nicht mehr Waaren saufe. Nein, die 100 von Roth 
schild nicht konsumirten Millionen treten, neu verliehen, in der 
Hand seiner Schuldner sofort wieder als Waarenkaufer 
auf.') Ihre Eigenschaft, Zins zu sein und Zinseszins zn 
schaffen, entzieht sie durchaus nicht dem Waarenmarkt. 
Ob in Profit- oder Zinsform angeeignet, die Waaren- 
nachfrage bleibt dieselbe; das Mißverhältniß zwischen 
Angebot und Nachfrage, die Ueberproduktion, nicht 
arrs dem Kapital als solchem, sondern dem Leih 
kapitale abzuleiten, ist ähnlich unvernünftig wie 
das Treiben der Antisemiten, welche die Volks 
misere nicht auf das Konto des Kapitals sondern 
des jüdischen Kapitals setzen wollen. 
Der Gedanke, welcher Flürscheim bei seiner Theorie 
vorschwebte, war offenbar der, daß die Ueberproduktion 
von dem Nichtkonsum des Mehrwerrhs, d. h. von 
seiner Akkumulation herrühre. Eine alte Idee, die schon im 
Anfange dieses Jahrhunderts von Sismondi vertreten amb 
speziell in Deutschland durch Rodbertus populär geworden 
ist! Aber ebenso berechtigt, wie diese Erklärungsweise, 
ebenso ans der Luft gegriffen ist die Zuthat, welche Flnr- 
scheim ihr aus eigener.Erfindung beigemischt, die Zllthat: 
nur der Nichtkonsum des in der Form von Leihkapital 
angeeigneten Mehrwerths trage die Schuld an dem Elend 
der Absatzkrisen. Diese lassen sich im Nahmen der Kapi 
tal-Gesellschaft, soweit es überhaupt möglich, nur dadurch 
heben , daß die kaufkräftige Nachfrage nach Konsum - 
art ik e ln steigt. Eine solche Steigerung setzt voraus, daß 
der zur Vermehrung der Produktion verwandte Theil des 
Mehrwerths kleiner, der gesammte Volkskonsum aber 
größer wird. Es müßte also, um den Absatzkrisen ent- 
i) Der Einwurf liegt nahe: Die Produzenten müssen an 
Rothschild die 120 Millionen zahlen, ehe dieser sein Geld neu aus 
leiht. Dies neu auszuleihende Geld kann darum noch keine Nach 
frage nach den von den Produzenten feilgebotenen Waaren, deren 
Erlös an Rothschild kommen soll, erheben. Folglich tritt durch die 
Zwischenkunft des Rothschild'schen Leihkapitals doch eine Absatzkrisc 
ein. — Man vergißt dabei aber die Organisation des Kreditwesens 
welche Kauf und Verkauf von dem augenblicklichen Geldbesitze 
unabhängig macht. An Stelle der Zahlung tritt das gleichw^rthige 
Zahlungsversprechen.