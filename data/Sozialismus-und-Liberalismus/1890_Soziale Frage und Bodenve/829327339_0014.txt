8 
Größe des vorgeschossenen Kapitalwerthes muß in beiden 
Fällen aus gleiche Weise berechnet werden. Zieht man 
aber von dem Gesammtpreis, den die Bodenbesitzer beim 
SBerfauf i^et Şrobube unb bw^ bag 2^011^ ron 
Gebäuden jährlich erzielen, die Produktionskosten, d. h. 
bag %ur &erßeHun8 ^tet SBaaten etfotbetIi($e 9kkiig= 
ober Werthguantum ab, und dividirt den dann verbleiben- 
bea me^er bag atbeitgfteie ßinbrnmen ber 
Grundherren reprüsentirt, durch den von ihnen vorgeschoffe- 
nem Kapitalwerth, so stellt sich heraus, daß die auf Bo 
denkultur verwandte Arbeit nicht nur den in der 
Industrie üblichen Gewinnsatz, sondern darüber 
hinaus einen gewaltigen Ueberschuß, die sog. 
Grundrente, abwirft. 
Der berühmte englische Nationalökonom Ricardo 
gtebt bte Erklärung für diese Thatsache. Während in der 
Industrie, sobald die Nachfrage nach Waare irgend einer 
Ärt annimmt, ber neue Bebatf mit bem gieren Meitg= 
aufmanb %etgefteíít merben sann, bet gut Şrobnftion bet= 
selben Waarenrnenge auch früher nothwendig war, trifft 
das, wie er hervorhebt, für den Land bau nicht in dem 
selben Maaße zu, uno zwar deßhalb nicht, weil in den 
Kulturländern die besten Bodensorten bereits seit lange in 
Beachtung finb. &ebt # nun bie m^ftage nach %aL 
rungsmitteln — und das muß sie dauernd, da die Be- 
röüetung R# 3# für ^a^ retme# — fr mirb ^(6^= 
leres Land in Angriff genommen. Die natürliche Folge 
ist, daß zur Herstellung derselben Metlge von Bodenpro 
dukten nunmehr auf diesem neu beackerten Lande ein 
größeres Quantum Arbeit erfordert wird, als auf dem 
bessern, früher bebauten Boden nothwendig war. Im 
Laufe der Entwicklung wird also immer mehr und immer 
geringwerthigeres Land in Kultur genommen. Das Arbeits 
quantum, das aus der schlechtesten der angebauten Boden 
sorten zur Produktion einer gegebenen Menge von Nah- 
runggmittein 110^^ iß, beßnbet # fottmä%renb im 
Wachsen. 
Dementsprechend muß aber auch der Preis der Bo- 
denprodukte dauernd zunehmen, denn dieser muß immer 
so hoch stehen, daß die Anbauer der schlechtesten aller in 
Angriff genommenen Landsorten die Produktionskosten ver-