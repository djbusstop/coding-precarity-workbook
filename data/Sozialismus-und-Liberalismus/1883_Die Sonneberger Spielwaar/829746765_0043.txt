39 
DerfäHt, baß alíe ßanbelßartlfel um so früher ereilt, ie begetter sie finb 
und je leichter ihre Herstellung ist. 
Ein willkürliches Herabdrücken der Preise Seitens der 
Kaufleute kann überhaupt da nicht stattfinden, wo wie in 
Sonneberg und in dem nahen Neustadt, über sechszig Groß 
handlungshäuser ihre Waaren von unabhängigen, selbst 
ständigen Fabrikanten kaufen müssen, um Handel treiben 
zu können, wo jeder geschickte Fabrikant, der für die mittleren und 
höheren Gesellschaftselassen originelle, fein oder künst- 
lerifcf) gearbeitete Äßaaren verfertigt, bie außerhalb ber 
Eoneurrenz stehen, allezeit die Preise erhält, die er vorschreibt. 
Seber ^abrifant l)at Wegezeit, unter beu fe^ig ®ro#aublungä= 
Ijänfern in @onneberg unb %euftabt fid) baßfenige außgumä^leu, mel^eß bie 
^õ^^^^eu ^reife für feine ^aaren i# #lt, eine ^e^â^berfe#^^ 
lei^termig, bie amar bem .Kaufmann abge#, meil feine gibue^ner über aüe 
Känbcr vcrtíjeilt stub, bacìi) ^at and) er eelegei^ett, unter beu Şrobuaenten 
bicfenigeu ¡id) außaumä^len, meld)e für einen, bur^^ bie Goncurrena be= 
stimmten Preis die besten Waaren liefern. 
Dem Handel können nur Waaren-Preise bienen, um welche der 
Fabrikant jederzeit lieferfähig ist, denn der Kaufmann darf nicht zu billigeren 
Preisen offerirei:, als die, zu denen er liefern kann. Er liefe sonst Gefahr, 
von seiner Kundschaft als leistungsunfähig angesehen und für die Folge 
von ihr gemieden zu werden. In diesem Umstand, der Liefer- 
beß ^anbelß, welche bie 8ei^^ungßfa^igfeit ber 
Production bedingt, liegt die Bürgschaft, daß letztere überhaupt nicht 
unter einem Preis arbeitet, der keinen Nutzen ihr gewährt. 
Weil die Angebote der verschiedenen Fabrikanten ausschließlich an die 
Kaufleute ergehen, so sind auch nur sie ausschließlich in den Stand gesetzt, 
Vergleiche der Leistungsfähigkeit unter den Fabrikanten anzustellen und nach 
bem (»rabe ber Dualität ber Maaren bie ^eife au noriniren. Su ben 
illngen Handelsunkundiger mögen solche Preisnormirungen a lu bau Resultat 
beß Drudeß ber .Kaufleute e^clueu, in ber %l)at finb sic aber nur baß beß 
S)nideß ber Goncurrena in ber ^0^1^011 selbst, melier in ben üerfd)iebenen 
Angeboten sich äußert. 
Wie die Erfolge großartiger Erfindungen erst dann allseitig die Menschen 
beglücken, der Production, dem Handel und Verkehr Millionen eintragen, wenn