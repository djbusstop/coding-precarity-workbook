eine Menge Industriezweige, die unter günstigen Verhältnissen auf freiem 
Felde selbstständig erwachsen, unabhängig wie sie sind, ebendeshalb keiner 
besonderen Gunst und Pflege seitens conservativer Regierungen sich erfreuen, 
weil sie zu dem Gedeihen bürgerlicher Selbstständigkeit der Atmosphäre 
des politischen Liberalismus bedürfen, die jenen nicht behagt, — 
nichtsdestoweniger aber Erwerbszweige sind, welche je nach dem Grad 
der in ihnen entwickelten persönlichen Thatkraft und Energie rascher zu 
sicheren Existenzen führen, als die, welche der Staat auf seinen überfüllten, 
erschöpften Berufsfeldern zumeist nach langjährigem Harren unter höchst 
bescheidenen Verhältnissen dem Volk zu bieten vermag. 
So ist das Oberland*) des Herzogthums Sachsen-Meiningen (Vorort 
Sonneberg), kaum 9 ^Meilen umfassend, eine für verschiedenartigste Industrie 
zweige von Natur gesegnete Gegend wie keine zweite in Europa, bewohnt 
von einer Bevölkerung, deren Fleiß, Rührigkeit und schöpferischer Arbeits 
geist unter selbstgestellter Organisation ihres Schaffens weltbekannt ist, wie 
nicht minder wegen des dort herrschenden gerechten Verhältnisses zwischen 
der selbstständigen Haus- und Kleinindustrie und dem lokalen Großhandel. 
Gerade die Sonneberger Spielwaaren-Manufaktur bietet für alles 
selbstständige plastische Schaffen, das vom mechanischen bis hinauf zum rein 
künstlerischen reicht, eine Stufenleiter, wie keine zweite Industrie, weil 
sie in einer selbstständigen Hausindustrie, dergleichen keine zweite existirt, 
ihren Stützpunkt hat. Eben dadurch, daß die Sonneberger Industrie zur 
untersten Staffel die Hausindustrie, zur nächsten die Kleinindustrie, 
zur folgenden die Fabrikin du stri e hat, bietet sie jedem sie betretenden Zög 
ling, sobald er als selbstständiger Fabrikant in das praktische Leben eintritt, auf 
jeder ihrer Staffeln, darauf er stehen geblieben, genau nach 
dem Erfolg seines materiellen oder nach dem Grad seines künstlerischen, durch 
geistigten Schaffens, den gerechten Lohn durch den lokalen Großhandel. 
Hier ist der Ort, wo man arbeiten sieht und wo arbeiten gelehrt 
wird, physisch und geistig, wo Jeder, der ein weltoffenes Auge hat, wahre 
National-Oekonomie praktisch studiren kann, faßlich und leicht. Was hier ge 
schaffen wird, geht über alle Meere, weiter, viel weiter, als gar manches 
*) Die Schätze daselbst sind: seltener Holzreichthnm; bester Schiefer 
zur Dachdeckung und zu Schiefertafeln; weicher Schieferstein, einzig seiner 
Art zu Griffeln und zu Wetzsteinen, Kalksteine (Marmor) zu Kinder 
schussern; Erdfarben, darunter der berühmte Goldoker; Kaolin und Eisen 
erze; Steinkohlen; feuerfester Thon in der Nähe. 
2*