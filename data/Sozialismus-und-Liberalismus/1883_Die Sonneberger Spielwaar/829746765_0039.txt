35 
ihn zu leiten haben, muß er das Resultat seiner gewonnenen Ueberzeugung 
gestreng zu seiner Richtung nehmen, denn bei massenhaft begehrten Handels 
artikeln handelt es sich häufig um wenige Pfennige Preisdifferenz sür's 
Dutzend, ob das eine oder das andere Großhandlungshaus den Auftrag 
empfängt. Kauft das eine um diese Differenz zu theuer von der Production, 
so geht es dafür der Bestellung verlustig, wo nicht gar der Kundschaft selbst. 
Denn auch diese, die Imp or teure und Detail li st en arbeiten unter 
C o neurrenz. Um sich ihr Absatzgebiet nicht schmälern zu lassen, müssen auch 
sie allezeit auf der Luge sein, für ihren Waarenbedars die billigsten Bezugs 
quellen zu erspähen. Sie verlangen und erwarten von ihren Vertretern, den 
Kaufleuten am Orte der Production, daß sie der letzteren gegenüber ihr Inte 
resse wahrnehmen, sowohl in Bezug auf die Qualität der zu vereinbarten 
Preisen zu besorgenden Waaren, wie namentlich auf die Preisreductionen 
correnter Sorten, wie solche, durch die Concurrenz herbeigeführt, in der Pro 
duction oft unvermuthet vorkommen, auf daß auch sie ihren Abnehmern, 
den Konsumenten solche sofort theilhaftig werden lassen, sobald ihre 
Concurrenz es thut. 
Daß dies gewissenhaft geschieht, dafür sorgt am sichersten der gefürchtete 
strenge Popanz, die Concurrenz unter den Kaufleuten. 
Jedes industrielle Geschäft, auch das des kleinsten Hausindustriellen, 
ist auf Massenproduktion und folglich auch auf Massenabsatz an 
gewiesen. Letzteren möglichst ohne Unterbrechung herbeizuführen, ist die Auf 
gabe und das Interesse eines den Sitz der Produktion theilenden Export- 
tz and eisstand es. Dieser sammelt die von verschiedenen Fabrikanten ge 
kauften Waaren, bis alle zur Completirung der Aufträge eingegangen sind. Um 
solche schneller ausführen zu können, kauft die Mehrzahl der Sonneberger 
Handelshäuser Waaren auf Lager, die nicht jederzeit in genügender Menge 
von den Fabrikanten zu beschaffen sind, oder solche, die man allein vom 
Fabrikanten beziehen will, um sie im Handel der Concurrenz zu entziehen. 
Oder auch die Kaufleute alle benützen den Tiefgang der 
Preise correnter Waaren, indem sie auf Lager kaufen. Dies 
zu thun, fordert das Interesse eines jeden Großhändlers, der unter Con 
currenz arbeitet, sowohl dem Fabrikantenstand gegenüber, welcher beschäftigt 
sein will, und den an sich zu fesseln in seinem Interesse liegt, als auch seiner 
Kundschaft gegenüber, welche nur so lange ihm treu bleibt, als er sie gleich 
3 *