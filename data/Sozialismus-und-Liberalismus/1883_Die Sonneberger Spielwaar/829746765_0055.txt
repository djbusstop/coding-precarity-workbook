51 
renden Nothstand in der Sonneberger Hausindustrie", als einer Production, 
die jeder höheren, sittlichen Organisation ermangelt, von „abhängigen" Fabri 
kanten und Arbeitern, die ohne innern Zusammenhang, ohne gemeinsame 
Institution alle Mittel entbehren zu einem Aufschwung in bessere Verhält 
nisse*). Und inmitten seines tiefschwarzen Gemäldes stellt er den nackten, kauf 
männischen Egoismus in rother Höllengluth dar, als den Urheber und Nährer 
des grausigen Elendes unter einem „Proletariat der schlimmsten Sorte."**) 
Der Universitäts-Philosoph sieht den Wohlstand nicht, der in Sonne 
berg und Umgegend unter allen Klassen vorherrscht, die direct und indirect 
von der Blüthe des Handels leben. Er sieht nur wohlhabende Kaufherren, 
aber keine wohlhabenden Fabrikanten und Gewerbtreibende, aber um so 
größere Schaaren eines Arbeiter-Proletariates, welche unparteiische Be 
obachter hier nicht sehen, vielmehr auswärts in einer viel traurigeren 
Lage finden, da, wo keine Industrie und kein Handel ihren Segen spenden. 
Man fragt daher erstaunt, ja empört, w o, in welchen anderen Regionen 
zwischen Production und Handel für erstere günstigere, humanere Verhältnisse 
existiren, als hier, wo jeder einzelne Fabrikant dem lokalen Großhandels 
stand so unabhängig und selbstständig gegenübersteht, wie der Detaillist 
und der Handwerker dem Publikum. Genau so wie dieses***) und 
nicht anders verfährt die Sonneberger Kaufmannschaft bei ihren Waaren 
einkäufen gegenüber den Fabrikanten,-^) nur daß im Großhandel, weil 
fortwährend unter Production und Consum eine Fluctuation gleich Ebbe 
und Fluth stattfindet, beide Factoren dem Gesetz des Angebotes 
und der Nachfrage unterworfen sind.şş) 
*) Wie ein Rezensent im Interesse des Sax'schen Werks in der Straßburger 
W (23. Äug. 1882) fWM. 
**) Wie ein Rezensent in der Weser-Zeitung (vom 27. September 1882) sich ver 
lauten läßt. 
***) Der fahrende Schüler der National - Oekonomie kauft seine Brödchen bei dem 
Bäcker, der die größten und besten liefert, imb der Herr Professor erspäht dasjenige 
Geschäft, in welchem er seine Bedürfnisse bester Qualität zugleich billigst empfängt. 
Beide erachten es als selbstverständliche Aufgabe der Concnrrenz, daß die Consú 
mente» der Willkür der Produzenten nicht preisgegeben werden, daß sie vielmehr die 
Preise der Erzeugnisse fortwährend erniedrige durch wirksamen D r u ck auf Arbeit, Fort 
schritt und Erfindung. 
t) Glefic Gelte 23, 29. 
tt) Gl# Güte 34, 39.