83 
Antheil von der Bestellung überbringst welche dem Kaufmann aus dem Lande 
der Antipoden zugekommen ist. 
Das Arb ei 1s leb en der selbstständigen Hausindustriellen ist ein ge 
sellschaftliches. Meister und Gehülfen haben einander in die Hände zu 
arbeiten. Eine Unterhaltung und ein Ideenaustausch kann zwischen ihnen statt 
finden, unbeschadet der nöthigen Flinkheit und Aufmerksamkeit bei der Arbeits 
verrichtung. Daher verbannt auch der Meister den Humor und Mutterwitz und 
auch den Gesang eines Liedes nicht vom Arbeitstische, vielmehr begrüßt er sie 
als willkommene Gäste, zur Arbeit anzufrischen. Ja, er zieht dazu noch andere 
liebe Gäste bei. Vögel, die er im Herbst auf dem nahen bewaldeten Berg 
rücken selbst gefangen, sollen durch ihren Gesang bei der Arbeit ihn ergötzen. 
Er macht so die Thierchen zu Schicksalsgefährten seiner und der Seinigen, 
die Jahr aus Jahr ein an den Werktagen von frühem Morgen bis spät in 
die Nacht in die Werkstube gebannt, die allbelebende Frische der freien 
Waldesluft entbehren müssen. 
Solche „Poesie in der Arbeitsstube" des Hausindnstriellen herrscht 
schon weniger in der des kleinen städtischen Fabrikanten und sie ist gänzlich 
verpönt in den Fabriken, wo die Arbeit in strenger Ordnung geschäfts 
mäßig betrieben wird. 
Die Segnungen des Welthandels bestehen für die Sonneberger 
Industrie in dem fast ununterbrochenen, gleichmäßigen Bedarf von Waaren, 
wie solcher nur durch einen über alle Länder der Erde verbreiteten Consum 
herbeigeführt werden kann. Während Bestellungen auf Waaren, etwa für 
Weihnachten, aus entferntesten Gegenden wie Indien und Australien, schon 
zu Anfang des Jahres am Produetionsorte eintreffen und ausgeführt werden 
müssen, sind für minder entfernte Länder wie Amerika, die Aufträge erst 
gegen Mitte des Jahres auszuführen, für nähere Gegenden noch später, für 
den Bedarf des Heimathlandes zuletzt. 
Durch die Transportzeit, bedingt durch die verschiedenen Entfernungen 
der consumirenden Länder vom Orte der Production, vertheilt sich der Welt- 
markts-Waarenbedarf fast auf alle Monate des Jahres, somit auch die Be 
schäftigung und der Verdienst der Fabrikanten. 
Weiter führt der Welthandel für die Industrie den Vortheil mit sich, 
daß er die für neue Handelsartikel gezahlten höheren Preise auf 
längere Zeit hinaus stabil erhält. Denn, ist ein deutsches Fabrikat neu 
in Deutschland in den ersten Monaten des Jahres, so kommt es in Amerika 
3