Fürstlich prid. Hosbuchdruckerci (g. stliitzlaff), Rudolstadt. 
d" Larve der Philosophie das Volk irre führt oder gar anlügt? Wer 
kennt cm Land, wohin deutsches Wissenschafts-Proletariat auswandern 
? effer - "Ş es ausgewandert werden sollte, das unter de», 
Brustschilde der Germania gebrütet, ja gezüchtet und genährt, in den 
Aalten des Gewandes sicherlich aufwärts, hinauf bis zu Krone und Schwert 
sich versteigt, wenn es den Körper mit seinem Gift überzogen haben wird? 
■ Í Ķbert muß wohl sich gedulden, bis das Rad der 
Geschichte Uber die Köpfe hinweggegangen sein wird, deren Scharfsinn 
Drohnen züchtet, stracks entgegen dem Instinkt kluger Thi-rchen, deren 
Arbcitsstaat den Menschen zum Vorbild dienen sollte. 
, î» ^ ert Zf af ’ ia§ Mn "ns-itigster Beurtheilung und grenzen 
los leichtfertiger Auffassung der Verhältnisse in Stadt uiid Kreis Sonncberg 
zeugt von Verdächtigungen und Entstellungen strotzt, während es alle 
wohbeta,inten Lichtseiten verschweigt,*) entbehrt jedes handelswissen 
schaftlichen Werthes und in Folge dessen auch des industriellen, indem 
Handel und Industrie hier unzertrennlich in einander verflochten sind 
Einen praktisch-industriellen Werth aber kann das Buch empfanaen 
ms rjl blC , in bcn Ştaatsarchiocil aufzubewahrenden Exemplare,' 
der Rest derselben den Rückweg so vieler gelehrten Werke gewandelt ,-in 
wird - den zur Sonneb-rg-r Papiermüchö-Spielwaaren- 
F a o r t ï a 11 o n. 
) Vergleiche unsere Beschreibung der Sonneberger Hausindustrie 
Seite 16, 19, 22, 23, 26, 29, 32, 39, 42. 
und ihres Handels